<?php

$inputString = file_get_contents('php://input');
$inputJSON = json_decode($inputString, TRUE);

// Copyright 2009, FedEx Corporation. All rights reserved.
// Version 12.0.0

require_once('fedex-common.php5');

//The WSDL is not included with the sample code.
//Please include and reference in $path_to_wsdl variable.
$path_to_wsdl = 'ShipService_v26.wsdl';
$date_time_now = date("dmYHis");
//$inputJSON['RequestedShipment']['CustomerReferences']['0']['Value']
//echo("Fedex_".$inputJSON['RequestedShipment']['CustomerReferences']['0']['Value']."_".$date_time_now.".pdf");

//"Fedex_TC007_07_PT1_ST01_PK01_SNDUS_RCPCA_POS_26072020"
define('SHIP_LABEL', "Fedex_".$inputJSON['RequestedShipment']['CustomerReferences']['0']['Value'].".pdf");  // PDF label file. Change to file-extension .pdf for creating a PDF label (e.g. shiplabel.pdf)

ini_set("soap.wsdl_cache_enabled", "0");

$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information


$request['WebAuthenticationDetail'] = array(
//	'ParentCredential' => array(
//		'Key' => getProperty('parentkey'),
//		'Password' => getProperty('parentpassword')
//	),
	'UserCredential' => array(
		'Key' => getProperty('key'),
		'Password' => getProperty('password')
	)
);

$request['ClientDetail'] = array(
	'AccountNumber' => getProperty('shipaccount'),
	'MeterNumber' => getProperty('meter')
);

$request['TransactionDetail'] = array('CustomerTransactionId' => '*** Express International Shipping Request using PHP ***');
$request['Version'] = array(
	'ServiceId' => 'ship',
	'Major' => '26',
	'Intermediate' => '0',
	'Minor' => '0'
);
$request['RequestedShipment'] = array(
	'ShipTimestamp' => date('c'),
	'DropoffType' => $inputJSON['RequestedShipment']['DropoffType'], // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
	'ServiceType' => $inputJSON['RequestedShipment']['ServiceType'], // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
	'PackagingType' => $inputJSON['RequestedShipment']['PackagingType'], // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	'Shipper' => addShipper(),
	'Recipient' => addRecipient(),
	'ShippingChargesPayment' => addShippingChargesPayment(),
	'CustomsClearanceDetail' => addCustomClearanceDetail(),
	'LabelSpecification' => addLabelSpecification(),
	'CustomerSpecifiedDetail' => array(
		'MaskedData'=> $inputJSON['RequestedShipment']['CustomerSpecifiedDetail']['MaskedData']
	),
	'PackageCount' => 1,
		'RequestedPackageLineItems' => array(
		'0' => addPackageLineItem1()
	),
	'CustomerReferences' => $inputJSON['RequestedShipment']['CustomerReferences']
	/*array(
		'0' => array(
			'CustomerReferenceType' => 'CUSTOMER_REFERENCE',
			'Value' => 'TC007_07_PT1_ST01_PK01_SNDUS_RCPCA_POS'
		)
	)*/
);



try{
	if(setEndpoint('changeEndpoint')){
		$newLocation = $client->__setLocation(setEndpoint('endpoint'));
	}

	$response = $client->processShipment($request); // FedEx web service invocation

    if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR'){
        printSuccess($client, $response);

        // Create PNG or PDF label
        // Set LabelSpecification.ImageType to 'PDF' for generating a PDF label
        $fp = fopen('../../uploads/labels/'.SHIP_LABEL, 'wb');
        fwrite($fp, ($response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image));
        fclose($fp);
        //echo 'Label <a href="../../uploads/labels/'.SHIP_LABEL.'">'.SHIP_LABEL.'</a> was generated.';
    }else{
		var_dump(http_response_code(500));

		// Get the new response code
		var_dump(http_response_code());
		printError($client, $response);
    }

	//writeToLog($client);    // Write to log file
} catch (SoapFault $exception) {
	var_dump(http_response_code(500));

	// Get the new response code
	var_dump(http_response_code());
	printFault($exception, $client);

    exit;
}



function addShipper(){
	$inputString = file_get_contents('php://input');
	$inputJSON = json_decode($inputString, TRUE);
	//echo($input['Contact']['PersonName']);
	$shipper = array(
		'Contact' => array(
			'PersonName' => $inputJSON['Contact']['PersonName'],
			'CompanyName' => $inputJSON['Contact']['CompanyName'],
			'PhoneNumber' => $inputJSON['Contact']['PhoneNumber']
		),
		'Address' => array(
			'StreetLines' => $inputJSON['Address']['StreetLines'],
			'City' => $inputJSON['Address']['City'],
			'StateOrProvinceCode' => $inputJSON['Address']['StateOrProvinceCode'],
			'PostalCode' => $inputJSON['Address']['PostalCode'],
			'CountryCode' => $inputJSON['Address']['CountryCode']
		)
	);
	return $shipper;
}
function addRecipient(){
	$inputString = file_get_contents('php://input');
	$inputJSON = json_decode($inputString, TRUE);

	$recipient = array(
		'Contact' => array(
			'PersonName' => $inputJSON['ContactRecipient']['PersonName'],
			'CompanyName' => $inputJSON['ContactRecipient']['CompanyName'],
			'PhoneNumber' => $inputJSON['ContactRecipient']['PhoneNumber']
		),
		'Address' => array(
			'StreetLines' => $inputJSON['AddressRecipient']['StreetLines'],
			'City' => $inputJSON['AddressRecipient']['City'],
			'StateOrProvinceCode' => $inputJSON['AddressRecipient']['StateOrProvinceCode'],
			'PostalCode' => $inputJSON['AddressRecipient']['PostalCode'],
			'CountryCode' => $inputJSON['AddressRecipient']['CountryCode'],
			'Residential' => $inputJSON['AddressRecipient']['Residential']
		)
	);
	return $recipient;
}
function addShippingChargesPayment(){
	$inputString = file_get_contents('php://input');
	$inputJSON = json_decode($inputString, TRUE);

	$shippingChargesPayment = array(
		'PaymentType' => $inputJSON['shippingChargesPayment']['PaymentType'],
        'Payor' => array(
			'ResponsibleParty' => array(
				'AccountNumber' => getProperty('billaccount'),
				'Contact' => $inputJSON['shippingChargesPayment']['Payor']['ResponsibleParty']['Contact'],
				'Address' => array(
					'CountryCode' => $inputJSON['shippingChargesPayment']['Payor']['ResponsibleParty']['Address']['CountryCode']
				)
			)
		)
	);
	return $shippingChargesPayment;
}
function addLabelSpecification(){
	$inputString = file_get_contents('php://input');
	$inputJSON = json_decode($inputString, TRUE);

	$labelSpecification = array(
		'LabelFormatType' => $inputJSON['labelSpecification']['LabelFormatType'], // valid values COMMON2D, LABEL_DATA_ONLY
		'ImageType' => $inputJSON['labelSpecification']['ImageType'],  // valid values DPL, EPL2, PDF, ZPLII and PNG
		'LabelStockType' => $inputJSON['labelSpecification']['LabelStockType']
	);
	return $labelSpecification;
}
function addSpecialServices(){
	$inputString = file_get_contents('php://input');
	$inputJSON = json_decode($inputString, TRUE);

	$specialServices = array(
		'SpecialServiceTypes' => ['specialServices']['SpecialServiceTypes'],
		'CodDetail' => array(
			'CodCollectionAmount' => array(
				'Currency' => ['specialServices']['CodDetail']['CodCollectionAmount']['Currency'],
				'Amount' => ['specialServices']['CodDetail']['CodCollectionAmount']['Amount']
			),
			'CollectionType' => ['specialServices']['CodDetail']['CollectionType'] // ANY, GUARANTEED_FUNDS
		)
	);
	return $specialServices;
}
function addCustomClearanceDetail(){
	$inputString = file_get_contents('php://input');
	$inputJSON = json_decode($inputString, TRUE);
	$customerClearanceDetail = array(
		'DutiesPayment' => array(
			'PaymentType' => $inputJSON['customerClearanceDetail']['DutiesPayment']['PaymentType'], // valid values RECIPIENT, SENDER and THIRD_PARTY
			'Payor' => array(
				'ResponsibleParty' => array(
					'AccountNumber' => getProperty('dutyaccount'),
					'Contact' => $inputJSON['customerClearanceDetail']['DutiesPayment']['Payor']['ResponsibleParty']['Contact'],
					'Address' => array(
						'CountryCode' => $inputJSON['customerClearanceDetail']['DutiesPayment']['Payor']['ResponsibleParty']['Address']['CountryCode']
					)
				)
			)
		),
		'DocumentContent' => $inputJSON['customerClearanceDetail']['DocumentContent'],
		'CustomsValue' => array(
			'Currency' => $inputJSON['customerClearanceDetail']['CustomsValue']['Currency'],
			'Amount' => $inputJSON['customerClearanceDetail']['CustomsValue']['Amount']
		),
		'Commodities' => $inputJSON['customerClearanceDetail']['Commodities'],
		'ExportDetail' => array(
			'B13AFilingOption' => $inputJSON['customerClearanceDetail']['ExportDetail']['B13AFilingOption']
		)
	);
	return $customerClearanceDetail;
}
function addPackageLineItem1(){
	$inputString = file_get_contents('php://input');
	$inputJSON = json_decode($inputString, TRUE);

	$packageLineItem = array(
		'SequenceNumber'=>$inputJSON['packageLineItem']['SequenceNumber'],
		'GroupPackageCount'=>$inputJSON['packageLineItem']['GroupPackageCount'],
		'Weight' => array(
			'Value' => $inputJSON['packageLineItem']['Weight']['Value'],
			'Units' => $inputJSON['packageLineItem']['Weight']['Units']
		),
		'InsuredValue' => array(
			'Currency' => $inputJSON['packageLineItem']['InsuredValue']['Currency'],
			'Amount' => $inputJSON['packageLineItem']['InsuredValue']['Amount']
		),
		'Dimensions' => array(
			'Length' => $inputJSON['packageLineItem']['Dimensions']['Length'],
			'Width' => $inputJSON['packageLineItem']['Dimensions']['Width'],
			'Height' => $inputJSON['packageLineItem']['Dimensions']['Height'],
			'Units' => $inputJSON['packageLineItem']['Dimensions']['Units']
		)
	);
	return $packageLineItem;
}
?>
