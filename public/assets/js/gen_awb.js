function gen_dhlexpress(obj) {
    var tr = $(obj).parent().parent().parent().parent();
    var id = tr.find('.sh_id').attr('set');
    $("#loading").show();
    $.ajax({
        type: "GET",
        url: '/create_dhlexpress_awb/' + id,
        processData: false,
        contentType: false,
        success: function (result) {
            try {
                alert(result.Response.Status.Condition.ConditionData);
                console.log(result.Response.Status.Condition.ConditionData);
            }
            catch (error) {
                try {
                    tr.find('.sh_awb').attr('set', result.AirwayBillNumber);
                    tr.find('.sh_awb').html(result.AirwayBillNumber);
                    tr.find('.sh_awb_download').html('<a href="/uploads/labels/' + result.Note.ActionNote + '" target="_blank"><i class="mdi mdi-file-pdf"></i></a>');
                    tr.find('#gen_awb').hide();
                    window.open('/uploads/labels/' + result.Note.ActionNote, '_blank');
                    console.log(result);
                }
                catch (error) {
                    alert('เกิดข้อผิดพลาด ปัญหาที่ไม่คาดคิด กรุณาติดต่อเจ้าหน้าที่ผู้ดูแลระบบ');
                }

            }
            $("#loading").hide();

        },
        error: function (e) {
            console.log("ERROR : ", e);
            if (e.status == 500) {
                alert('เกิดข้อผิดพลาด ปัญหาอาจมาจากการไม่แนบไฟล์ Invoice หรือ ระบบของ DHL ล่ม กรุณาติดต่อเจ้าหน้าที่ผู้ดูแลระบบ');
            }
            $("#loading").hide();
        }
    });
    return true;
}

function gen_fedexInternational(obj) {
    var tr = $(obj).parent().parent().parent().parent();
    var id = tr.find('.sh_id').attr('set');
    var sh_tracking_code = tr.find('.sh_tracking_code').attr('set');
    $("#loading").show();
    $.ajax({
        type: "GET",
        url: '/create_fedexInternational_awb/' + id,
        processData: false,
        contentType: false,
        success: function (result) {
            if (result != "error") {
                tr.find('.sh_awb').attr('set', result);
                tr.find('.sh_awb').html(result);
                tr.find('.sh_awb_download').html('<a href="/uploads/labels/Fedex_SME_SHIPPING_TRACKING_' + sh_tracking_code + '" target="_blank"><i class="mdi mdi-file-pdf"></i></a>');
                tr.find('#gen_awb').hide();
                window.open('/uploads/labels/Fedex_SME_SHIPPING_TRACKING_' + sh_tracking_code + '.pdf', '_blank');
                $("#loading").hide();
            } else {
                $("#loading").hide();
                alert("เกิดข้อผิดพลาด โปรดตรวจสอบข้อมูลให้ระเอียดอีกครั้ง หรือติดต่อเจ้าหน้าที่ผู้ดูแลระบบ");
            }


        },
        error: function (e) {
            console.log("ERROR : ", e);
            if (e.status == 500) {
                alert('เกิดข้อผิดพลาด โปรดตรวจสอบข้อมูลให้ระเอียดอีกครั้ง หรือติดต่อเจ้าหน้าที่ผู้ดูแลระบบ');
            }
            $("#loading").hide();
        }
    });
    return true;
}