var this_sh_select = null;
$("#add_box").click(function () {
    var box_num = $("#box tbody tr").length;
    var box = $("#box tbody").append(
        "<tr>" +
        "<td>" + (box_num + 1) + "</td>" +
        '<td><input name="weight" class="weight form-control" onkeypress="return isNumberKey(event)" required/></td>' +
        '<td><input name="width" class="width form-control" onkeypress="return isNumberKey(event)" /></td>' +
        '<td><input name="length" class="length form-control" onkeypress="return isNumberKey(event)" /></td>' +
        '<td><input name="height" class="height form-control" onkeypress="return isNumberKey(event)" /></td>' +
        '<td style="padding:unset"><button type="button" class="btn btn-sm btn-danger" onclick="box_remove(this)"> <span class="ti-trash"></span></button> <button type="button" class="btn btn-sm btn-success" onclick="box_copy(this)"> <span class="ti-plus"></span></button></td>' +
        "</tr>"
    );
});

$("#sh_form").submit(async function (e) {
    $('#sh_postcode').removeClass('is-invalid');
    $('#sh_postcode').removeClass('is-valid');
    e.preventDefault();
    $('#message-notdata').remove();
    var row_no = $("#shipment_detail tbody tr").length + 1;
    //start row
    var row = '';
    row += '<td><label class="checkbox_select"><input type="checkbox" class="check_all" onclick="check_select(this)"></label></td>';
    row += "<td>" + row_no + "</td>";
    //Information----------------
    var service = $("#sh_type_of_service option:selected");
    var sh_so = $("#sh_so").val();
    var sh_co = $("#sh_co").val();
    var sh_s_company = $("#sh_s_company").val();
    var sh_s_address1 = $("#sh_s_address1").val();
    var sh_s_address2 = $("#sh_s_address2").val();
    var sh_s_city = $("#sh_s_city").val();
    var sh_s_postalcode = $("#sh_s_postalcode").val();
    var sh_s_phone = $("#sh_s_phone").val();
    var sh_insurance = $("#sh_insurance").val();
    var sh_account_number = $("#sh_account_number option:selected");
    var sh_id = $("#sh_id").val();
    var sh_awb = $("#sh_awb").val();
    var sh_tracking_DHL = $("#sh_tracking_DHL").val();
    var sh_tracking_local = $("#sh_tracking_local").val();
    var sh_tracking_code = $("#sh_tracking_code").val();
    var cal = service.attr('cal');
    row +=
        "<td>" +
        '<span class="b-title sh_id" set="' + sh_id + '">Service:</span>' +
        '<span class="sh_type_of_service" set="' + service.val() + '">' + service.text() + "</span><br>" +
        '<span class="b-title">Insurance:</span> <span class="sh_insurance" set="' + sh_insurance + '">' + sh_insurance + "</span><br>" +
        '<span class="b-title">Account Number:</span> <span class="sh_account_number" set="' + sh_account_number.val() + '">' + sh_account_number.val() + "</span><br>" +
        '<span class="b-title">SO:</span> <span class="sh_so" set="' + sh_so + '">' + sh_so + "</span><br>" +
        '<span class="b-title">C/O:</span> <span class="sh_co" set="' + sh_co + '">' + sh_co + "</span><br>" +
        '<span class="b-title">Company:</span> <span class="sh_s_company" set="' + sh_s_company + '">' + sh_s_company + "</span><br>" +
        '<span class="b-title">Address 1:</span> <span class="sh_s_address1" set="' + sh_s_address1 + '">' + sh_s_address1 + "</span><br>" +
        '<span class="b-title">Address 2:</span> <span class="sh_s_address2" set="' + sh_s_address2 + '">' + sh_s_address2 + "</span><br>" +
        '<span class="b-title">City:</span> <span class="sh_s_city" set="' + sh_s_city + '">' + sh_s_city + "</span><br>" +
        '<span class="b-title">Postal Code:</span> <span class="sh_s_postalcode" set="' + sh_s_postalcode + '">' + sh_s_postalcode + "</span><br>" +
        '<span class="b-title">Phone Number:</span> <span class="sh_s_phone" set="' + sh_s_phone + '">' + sh_s_phone + "</span><br>" +
        '<span class="b-title">AWB:</span> <span class="sh_awb" set="' + sh_awb + '">' + sh_awb + '</span><br>' +
        '<span class="b-title">Routing:</span>' +
        '<span class="sh_routing_id" set="' + service.attr('routing') + '">' + service.attr('routing') + '</span><br>' +
        '<span class="b-title">Cal:</span>' +
        '<span class="sh_cal" set="' + cal + '">' + (cal == 'up' ? 'Up' : 'Normal') + '</span><br>' +
        '<span class="b-title">Tracking DHL:</span>' +
        '<span class="sh_tracking_DHL" set="' + sh_tracking_DHL + '">' + sh_tracking_DHL + '</span>' +
        "<br>" +
        '<span class="b-title">Warehouse:</span>' +
        '<span class="sh_tracking_local" set="' + sh_tracking_local + '">' + $("#sh_tracking_local option:selected").text() + '</span>' +
        "<br>" +
        '<span class="b-title">Tracking SME:</span>' +
        '<span class="sh_tracking_code" set="' + sh_tracking_code + '"> ' + smecode(sh_tracking_code) + '</span>' +
        "</td>";
    //Shipment----------------
    var sh_shipment_type = $('#sh_shipment_type option:selected').val();
    var sh_currency = $('#sh_currency option:selected').val();
    var sh_declared_value = $('#sh_declared_value').val();
    var sh_content_quantity = $('#sh_content_quantity').val();
    var sh_description = $('#sh_description').val();
    var sh_note = $('#sh_note').val();
    var sh_packge_type = $('#sh_packge_type option:selected');
    row += "<td>" +
        '<span class="b-title">Shipping Type:</span>' +
        '<span class="sh_shipping_type" set="' + sh_shipment_type + '">' + sh_shipment_type + '</span><br>' +
        '<span class="b-title">Currency:</span>' +
        '<span class="sh_currency" set="' + sh_currency + '">' + sh_currency + '</span><br>' +
        '<span class="b-title">Declared Value:</span>' +
        '<span class="sh_declared_value" set="' + sh_declared_value + '">' + sh_declared_value + '</span><br>' +
        '<span class="b-title">Description:</span>' +
        '<span class="sh_description" set="' + sh_description + '">' + sh_description + '</span><br>' +
        '<span class="b-title">Content Quantity:</span>' +
        '<span class="sh_content_quantity" set="' + sh_content_quantity + '">' + sh_content_quantity + '</span><br>' +
        '<span class="b-title">Note:</span>' +
        '<span class="sh_note" set="' + sh_note + '">' + sh_note + '</span><br>' +
        '<span class="b-title">Packge Type:</span>' +
        '<span class="sh_packge_type" set="' + sh_packge_type.val() + '">' + sh_packge_type.html() + '</span><br>' +
        "</td>";
    //Recipient----------------
    var sh_recipient = $('#sh_recipient').val();
    var sh_company = $('#sh_company').val();
    var sh_email = $('#sh_email').val();
    var sh_address1 = $('#sh_address1').val();
    var sh_address2 = $('#sh_address2').val();
    var sh_address3 = $('#sh_address3').val();
    var sh_phone = $('#sh_phone').val();
    var sh_country = $('#sh_country option:selected');
    var sh_city = $('#sh_city').val();
    var sh_state = $('#sh_state').val();
    var sh_postcode = $('#sh_postcode').val();
    var sh_suburb = $('#sh_suburb').val();
    row += "<td>" +
        '<span class="b-title">Name:</span>' +
        '<span class="sh_recipient" set="' + sh_recipient + '">' + sh_recipient + '</span><br>' +
        '<span class="b-title">Company:</span>' +
        '<span class="sh_company" set="' + sh_company + '">' + sh_company + '</span><br>' +
        '<span class="b-title">E-Mail:</span>' +
        '<span class="sh_email" set="' + sh_email + '">' + sh_email + '</span><br>' +
        '<span class="b-title">Address1:</span>' +
        '<span class="sh_address1" set="' + sh_address1 + '">' + sh_address1 + '</span><br>' +
        '<span class="b-title">Address2:</span>' +
        '<span class="sh_address2" set="' + sh_address2 + '">' + sh_address2 + '</span><br>' +
        '<span class="b-title">Address3:</span>' +
        '<span class="sh_address3" set="' + sh_address3 + '">' + sh_address3 + '</span><br>' +
        '<span class="b-title">Phone Number:</span>' +
        '<span class="sh_phone" set="' + sh_phone + '">' + sh_phone + '</span><br>' +
        '<span class="b-title">Country:</span>' +
        '<span class="sh_country" set="' + sh_country.val() + '"> ' + sh_country.val() + ' </span><br>' +
        '<span class="b-title">Country Code:</span>' +
        '<span class="sh_country_code" set="' + sh_country.attr('code') + '">' + sh_country.attr('code') + '</span><br>' +
        '<span class="b-title">City:</span>' +
        '<span class="sh_city" set="' + sh_city + '">' + sh_city + '</span><br>' +
        '<span class="b-title">Suburb:</span>' +
        '<span class="sh_suburb" set="' + sh_suburb + '">' + sh_suburb + '</span><br>' +
        '<span class="b-title">State:</span>' +
        '<span class="sh_state" set="' + sh_state + '">' + sh_state + '</span><br>' +
        '<span class="b-title">Postal Code:</span>' +
        '<span class="sh_postcode" set="' + sh_postcode + '">' + sh_postcode + '</span><br>' +
        "</td>";
    //Other Service----------------
    var sh_pickup_fee = parseFloat(convert2number($('#sh_pickup_fee').val()));
    var sh_time_guaranty = parseFloat(convert2number($('#sh_time_guaranty').val()));
    var sh_remote_area = parseFloat(convert2number($('#sh_remote_area').val()));
    var sh_insurance = parseFloat(convert2number($('#sh_insurance').val()));
    var sh_export_doc = parseFloat(convert2number($('#sh_export_doc').val()));
    var sh_co_from = parseFloat(convert2number($('#sh_co_from').val()));
    var sh_over_size = parseFloat(convert2number($('#sh_over_size').val()));
    var sh_over_weight = parseFloat(convert2number($('#sh_over_weight').val()));
    var sh_packing_paperbox = parseFloat(convert2number($('#sh_packing_paperbox').val()));
    var sh_wood_packing = parseFloat(convert2number($('#sh_wood_packing').val()));
    var sh_other_service_charge = parseFloat(convert2number($('#sh_other_service_charge').val()));
    var total_other_service = 0;
    row += "<td>";
    var other_service_list = $('.other_service_ck');
    other_service_list.each(function (i, e) {
        if ($(e).is(':checked')) {
            var service_name = $(e).parent().parent().find('.other_service_name').val();
            var service_value = $(e).parent().parent().find('.other_service_value').val();
            total_other_service = total_other_service + parseFloat(convert2number(service_value));
            row += '<span class="b-title">' + service_name + ': </span><span class="sh_other_service" set="' + service_name + '" price="' + service_value + '">' + service_value + '</span><hr class="line-npd">';
        }
    });
    row += '<div class="sum-bottom total_other_service" set="' + total_other_service + '">Total Amount:' + total_other_service + ' ฿' +
        '</div>' +
        '</td>';
    //Volume(KG)----------------
    var box = $('#box tbody tr');
    var totalweight = 0;
    row += "<td>";
    box.each(function (i, e) {
        var weight = parseFloat(convert2number($(e).find(".weight").val()));
        var width = parseFloat(convert2number($(e).find(".width").val()));
        var length = parseFloat(convert2number($(e).find(".length").val()));
        var height = parseFloat(convert2number($(e).find(".height").val()));
        var sum = width * length * height / 5000;
        sum = parseFloat(convert2number(sum.toFixed(10)));
        var weightcal = parseFloat(convert2number(calup(weight)));
        var sumcal = parseFloat(convert2number(calup(sum)));
        totalweight += (weightcal > sumcal ? weightcal : sumcal);
        row += '<span class="box b-title" weight="' + weight + '" width="' + width + '" length="' + length + '" height="' + height + '" series="' + (i + 1) + '">' + (i + 1) + ' :</span>' +
            '<span>' + weight + ' KG // ' +
            '' + width + 'x' + length + 'x' + height + ' = ' + (weight > sum ? calup(weight) : calup(sum)) + ' KG' +
            '<hr class="line-npd">';
    });
    row += '<div class="sum-bottom">Total Weight : <span class="total_weight" set="' + totalweight.toFixed(1) + '">' + totalweight.toFixed(1) + '</span> KG.</div>' +
        "</td>";

    //CHARGE----------------    
    var url = '/getprice/' + service.val() + '/' + sh_country.attr('code') + '/' + totalweight.toFixed(2);
    const total_regular = await $.ajax({
        type: "GET",
        url: url,
        success: function (result) {
            return result;
        },
        error: function (e) {
            console.log("ERROR : ", e);
            $('#loading').hide();
            alert('พบปัญหาในการขอราคา โปรดตรวจสอบให้แน่ใจว่าเลือกประเทศปลายทาง กรอกขนาดและน้ำหนัก และเลือกบริการเรียบร้อย? หากยังพบปัญหาโปรดแจ้งผู้ดูแลระบบ');
            return 0;
        }
    });
    var total_charge = total_other_service + parseFloat(total_regular);
    row += "<td>" +
        '<span class="b-title">Regular:</span><span class="total_regular_item" set="' + total_regular + '">' + total_regular + '</span><br>' +
        '<span class="b-title">Other Service:</span><span class="total_other_service_item" set="' + total_other_service + '"> ' + total_other_service + ' ฿</span><br>' +
        '<div class="sum-bottom total_charge_item" set="' + total_charge + '"> Total Charge: ' + total_charge + ' ฿ </div > ' +
        "</td>";

    //Action----------------
    row += "<td>";
    row += '<div class="dropdown d-inline-block float-right">';
    row += '    <a class="nav-link dropdown-toggle arrow-none" id="dLabel8"  data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">';
    row += '        <i class="fas fa-ellipsis-v font-20 text-muted"></i>';
    row += "    </a>";
    row += '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel8">';
    row += '<a class="dropdown-item" onclick="copy_sh($(this))">Copy</a>';
    row += '<a class="dropdown-item" onclick="edit_sh($(this))">Edit</a>';
    row += '<a class="dropdown-item" onclick="delete_sh($(this))">Delete</a>';
    row += "</div>";
    row += "</div>";
    row += "</td>";
    //end row

    if (sh_id != '') {
        $(this_sh_select).html(row);
    } else {
        row = "<tr>" + row + "</tr>"
        $("#shipment_detail tbody").append(row);
    }

    $("#close_modal_all").click();
    $("#sh_reset").click();
    $('#sh_country option:contains("UNITED STATES OF AMERICA")').attr('selected', 'selected').change();
    binding();
});

function getprice(url) {
    var total_regular = 0;
    $.ajax({
        type: "GET",
        url: url,
        success: function (result) {
            total_regular = result;
        },
        error: function (e) {
            console.log("ERROR : ", e);
            $('#loading').hide();
        }
    });
    return total_regular;
}

function box_remove(obj) {
    $(obj).parent().parent().remove();

    $box = $("#box tbody tr");
    $($box).each(function (i, n) {
        $(this).find('td').eq(0).html(i + 1);
    });

}

function box_copy(obj) {
    var newitem = $(obj).parent().parent().html();
    console.log(newitem);
    $("#box tbody").append('<tr>' + newitem + '</tr>');
    $box = $("#box tbody tr");
    $($box).each(function (i, n) {
        $(this).find('td').eq(0).html(i + 1);
    });

}

function convert2number(value) {
    var result = value;
    if (value === undefined || isNaN(value) || value === "") {
        result = 0;
    }
    return result;
}

function calup(value) {
    var sum_digit = value.toFixed(10);
    var sum_notdigit = Math.floor(value);
    var digit = sum_digit - sum_notdigit;
    result = sum_digit;
    if (digit > 0) {
        if (digit < 0.5) {
            result = sum_notdigit + 0.5;
        } else if (digit > 0.5) {
            result = sum_notdigit + 1;
        } else {
            result = sum_digit;
        }
    }
    return result;
}

function binding() {
    window.onbeforeunload = function () {
        return 'Are you sure that you want to leave this page?';
    }
    $('#save_shipment').show();
    var tr = $("#shipment_detail tbody tr");
    var total_charge = 0
    var package = $('.box').length;
    var total_weight = 0;
    $('.total_charge_item').each(function () {
        total_charge += parseFloat($(this).attr('set'));
    });
    $('.total_weight').each(function () {
        total_weight += parseFloat($(this).attr('set'));
    });
    var bind = $('#data_binding');
    var str = '';
    tr.each(function (i) {
        $(this).find('td').eq(1).html(i + 1);
        var sh_id = $(this).find('.sh_id').attr('set');
        str += '<input type="hidden" name="sh_id[' + i + ']" value="' + sh_id + '" />'
        var sh_type_of_service = $(this).find('.sh_type_of_service').attr('set');
        str += '<input type="hidden" name="sh_type_of_service[' + i + ']" value="' + sh_type_of_service + '" />'
        var sh_insurance = $(this).find('.sh_insurance').attr('set');
        str += '<input type="hidden" name="sh_insurance[' + i + ']" value="' + sh_insurance + '" />'
        var sh_account_number = $(this).find('.sh_account_number').attr('set');
        str += '<input type="hidden" name="sh_account_number[' + i + ']" value="' + sh_account_number + '" />'
        var sh_so = $(this).find('.sh_so').attr('set');
        str += '<input type="hidden" name="sh_so[' + i + ']" value="' + sh_so + '" />'
        var sh_co = $(this).find('.sh_co').attr('set');
        str += '<input type="hidden" name="sh_co[' + i + ']" value="' + sh_co + '" />'

        var sh_s_company = $(this).find('.sh_s_company').attr('set');
        str += '<input type="hidden" name="sh_s_company[' + i + ']" value="' + sh_s_company + '" />'
        var sh_s_address1 = $(this).find('.sh_s_address1').attr('set');
        str += '<input type="hidden" name="sh_s_address1[' + i + ']" value="' + sh_s_address1 + '" />'
        var sh_s_address2 = $(this).find('.sh_s_address2').attr('set');
        str += '<input type="hidden" name="sh_s_address2[' + i + ']" value="' + sh_s_address2 + '" />'
        var sh_s_city = $(this).find('.sh_s_city').attr('set');
        str += '<input type="hidden" name="sh_s_city[' + i + ']" value="' + sh_s_city + '" />'
        var sh_s_postalcode = $(this).find('.sh_s_postalcode').attr('set');
        str += '<input type="hidden" name="sh_s_postalcode[' + i + ']" value="' + sh_s_postalcode + '" />'
        var sh_s_phone = $(this).find('.sh_s_phone').attr('set');
        str += '<input type="hidden" name="sh_s_phone[' + i + ']" value="' + sh_s_phone + '" />'


        var sh_awb = $(this).find('.sh_awb').attr('set');
        str += '<input type="hidden" name="sh_awb[' + i + ']" value="' + sh_awb + '" />'
        var sh_tracking_code = $(this).find('.sh_tracking_code').attr('set');
        str += '<input type="hidden" name="sh_tracking_code[' + i + ']" value="' + sh_tracking_code + '" />'
        var sh_tracking_DHL = $(this).find('.sh_tracking_DHL').attr('set');
        str += '<input type="hidden" name="sh_tracking_DHL[' + i + ']" value="' + sh_tracking_DHL + '" />'
        var sh_tracking_local = $(this).find('.sh_tracking_local').attr('set');
        str += '<input type="hidden" name="sh_tracking_local[' + i + ']" value="' + sh_tracking_local + '" />'

        var sh_shipping_type = $(this).find('.sh_shipping_type').attr('set');
        str += '<input type="hidden" name="sh_shipping_type[' + i + ']" value="' + sh_shipping_type + '" />'
        var sh_currency = $(this).find('.sh_currency').attr('set');
        str += '<input type="hidden" name="sh_currency[' + i + ']" value="' + sh_currency + '" />'
        var sh_declared_value = $(this).find('.sh_declared_value').attr('set');
        str += '<input type="hidden" name="sh_declared_value[' + i + ']" value="' + sh_declared_value + '" />'
        var sh_description = $(this).find('.sh_description').attr('set');
        str += '<input type="hidden" name="sh_description[' + i + ']" value="' + sh_description + '" />'
        var sh_content_quantity = $(this).find('.sh_content_quantity').attr('set');
        str += '<input type="hidden" name="sh_content_quantity[' + i + ']" value="' + sh_content_quantity + '" />'
        var sh_note = $(this).find('.sh_note').attr('set');
        str += '<input type="hidden" name="sh_note[' + i + ']" value="' + sh_note + '" />'
        var sh_packge_type = $(this).find('.sh_packge_type').attr('set');
        str += '<input type="hidden" name="sh_packge_type[' + i + ']" value="' + sh_packge_type + '" />'

        var sh_recipient = $(this).find('.sh_recipient').attr('set');
        str += '<input type="hidden" name="sh_recipient[' + i + ']" value="' + sh_recipient + '" />'
        var sh_company = $(this).find('.sh_company').attr('set');
        str += '<input type="hidden" name="sh_company[' + i + ']" value="' + sh_company + '" />'
        var sh_email = $(this).find('.sh_email').attr('set');
        str += '<input type="hidden" name="sh_email[' + i + ']" value="' + sh_email + '" />'
        var sh_address1 = $(this).find('.sh_address1').attr('set');
        str += '<input type="hidden" name="sh_address1[' + i + ']" value="' + sh_address1 + '" />'
        var sh_address2 = $(this).find('.sh_address2').attr('set');
        str += '<input type="hidden" name="sh_address2[' + i + ']" value="' + sh_address2 + '" />'
        var sh_address3 = $(this).find('.sh_address3').attr('set');
        str += '<input type="hidden" name="sh_address3[' + i + ']" value="' + sh_address3 + '" />'
        var sh_phone = $(this).find('.sh_phone').attr('set');
        str += '<input type="hidden" name="sh_phone[' + i + ']" value="' + sh_phone + '" />'
        var sh_country = $(this).find('.sh_country').attr('set');
        str += '<input type="hidden" name="sh_country[' + i + ']" value="' + sh_country + '" />'
        var sh_country_code = $(this).find('.sh_country_code').attr('set');
        str += '<input type="hidden" name="sh_country_code[' + i + ']" value="' + sh_country_code + '" />'
        var sh_city = $(this).find('.sh_city').attr('set');
        str += '<input type="hidden" name="sh_city[' + i + ']" value="' + sh_city + '" />'
        var sh_state = $(this).find('.sh_state').attr('set');
        str += '<input type="hidden" name="sh_state[' + i + ']" value="' + sh_state + '" />'
        var sh_suburb = $(this).find('.sh_suburb').attr('set');
        str += '<input type="hidden" name="sh_suburb[' + i + ']" value="' + sh_suburb + '" />'
        var sh_postcode = $(this).find('.sh_postcode').attr('set');
        str += '<input type="hidden" name="sh_postcode[' + i + ']" value="' + sh_postcode + '" />'
        var total_regular_item = $(this).find('.total_regular_item').attr('set');
        str += '<input type="hidden" name="total_regular_item[' + i + ']" value="' + total_regular_item + '" />'
        //var sh_other = $(this).find('.sh_other');//list
        //sh_other.each(function (n) {
        //    str += '<input type="text" name="sh_other_name[' + i + '][' + n + ']" value="' + $(this).attr('set') + '" />'
        //    str += '<input type="text" name="sh_other_value[' + i + '][' + n + ']" value="' + $(this).attr('price') + '" />'
        //});
        var sh_otherservice = $(this).find('.sh_other_service'); //list
        sh_otherservice.each(function (n) {
            str += '<input type="hidden" name="sh_other_service_name[' + i + '][' + n + ']" value="' + $(this).attr('set') + '" />'
            str += '<input type="hidden" name="sh_other_service_value[' + i + '][' + n + ']" value="' + $(this).attr('price') + '" />'
        });
        var box = $(this).find('.box'); //list
        box.each(function (n) {
            str += '<input type="hidden" name="series[' + i + '][' + n + ']" value="' + $(this).attr('series') + '" />'
            str += '<input type="hidden" name="weight[' + i + '][' + n + ']" value="' + $(this).attr('weight') + '" />'
            str += '<input type="hidden" name="width[' + i + '][' + n + ']" value="' + $(this).attr('width') + '" />'
            str += '<input type="hidden" name="length[' + i + '][' + n + ']" value="' + $(this).attr('length') + '" />'
            str += '<input type="hidden" name="height[' + i + '][' + n + ']" value="' + $(this).attr('height') + '" />'
        });

        var total_charge_item = $(this).find('.total_charge_item').attr('set'); // ราคารวมบริการอื่นๆแล้ว
        str += '<input type="hidden" name="total_charge_item[' + i + ']" value="' + total_charge_item + '" />'
        var total_weight_item = $(this).find('.total_weight').attr('set'); //น้ำหนักรวม
        str += '<input type="hidden" name="total_weight_item[' + i + ']" value="' + total_weight_item + '" />'

    });
    str += '<input type="hidden" name="total_price" value="' + total_charge + '" />'
    str += '<input type="hidden" name="total_weight" value="' + total_weight + '" />'
    bind.html(str);


    var discount = $('#total_discount').html();
    $('#total_package').html(package);
    $('#total_pickup_fee').html(0);
    $('#grand_total').html(total_charge - discount);
    $('#total_shipment').html(tr.length);
    //$('#total_discount').html(0);
    $('#total_weight').html(total_weight);
    $('#total_amount').html(total_charge);

}

$('#sh_country option:contains("UNITED STATES OF AMERICA")').attr('selected', 'selected').change();

$('#save_shipment').click(function () {
    $("#loading").show();
    //binding();
    window.onbeforeunload = function () { };
    var url = $('#data_binding').attr("action");

    var data_bunding = new FormData();
    data_bunding.fromdata()
    
    $.ajax({
        type: "POST",
        data: $("#booking").serialize(),
        url: url,
        success: function (result) {
            $("#loading").hide();
            location.reload();
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert(
                "An error occurred on the server when processing the URL. Please contact the system administrator."
            );
            $("#loading").hide();
        }
    });
});

function delete_sh(e) {
    var tr = $(e).parent().parent().parent().parent();
    tr.remove();
    binding();
    console.log(tr);
}

function return_sh(e) {
    $("#loading").show();
    var tr = $(e).parent().parent().parent().parent();
    var id = tr.find('.sh_id').attr('set');
    $.ajax({
        type: "GET",
        url: '/set_return/' + id,
        success: function (result) {
            if (result >= 10) {
                tr.find('.box-status').html('<img src="/assets/images/return_box.webp" width="100%">');
            } else {
                tr.find('.box-status').html('');
            }
            //location.reload();
            $("#loading").hide();
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert(
                "An error occurred on the server when processing the URL. Please contact the system administrator."
            );
            $("#loading").hide();
        }
    });

}

function copy_sh(e) {

    $("#loading").show();
    $('#add_sh').click();
    var tr = $(e).parent().parent().parent().parent();
    this_sh_select = tr;
    $('#sh_id').val('');
    var sh_type_of_service = $(tr).find('.sh_type_of_service').attr('set');
    $('#sh_type_of_service').val(sh_type_of_service);
    var sh_insurance = $(tr).find('.sh_insurance').attr('set');
    $('#sh_insurance').val(sh_insurance);
    var sh_account_number = $(tr).find('.sh_account_number').attr('set');
    $('#sh_account_number').val(sh_account_number);
    var sh_so = $(tr).find('.sh_so').attr('set');
    $('#sh_so').val(sh_so);
    var sh_co = $(tr).find('.sh_co').attr('set');
    $('#sh_co').val(sh_co);
    var sh_awb = $(tr).find('.sh_awb').attr('set');
    $('#sh_awb').val(sh_awb);
    var sh_tracking_code = $(tr).find('.sh_tracking_code').attr('set');
    $('#sh_tracking_code').val(sh_tracking_code);
    var sh_tracking_DHL = $(tr).find('.sh_tracking_DHL').attr('set');
    $('#sh_tracking_DHL').val(sh_tracking_DHL);
    var sh_tracking_local = $(tr).find('.sh_tracking_local').attr('set');
    $('#sh_tracking_local').val(sh_tracking_local);
    var sh_shipping_type = $(tr).find('.sh_shipping_type').attr('set');
    $('#sh_shipping_type').val(sh_shipping_type);
    var sh_currency = $(tr).find('.sh_currency').attr('set');
    $('#sh_currency').val(sh_currency);
    var sh_declared_value = $(tr).find('.sh_declared_value').attr('set');
    $('#sh_declared_value').val(sh_declared_value);
    var sh_description = $(tr).find('.sh_description').attr('set');
    $('#sh_description').val(sh_description);
    var sh_content_quantity = $(tr).find('.sh_content_quantity').attr('set');
    $('#sh_content_quantity').val(sh_content_quantity);
    var sh_note = $(tr).find('.sh_note').attr('set');
    $('#sh_note').val(sh_note);
    var sh_packge_type = $(tr).find('.sh_packge_type').attr('set');
    $('#sh_packge_type').val(sh_packge_type);
    var sh_recipient = $(tr).find('.sh_recipient').attr('set');
    $('#sh_recipient').val(sh_recipient);
    var sh_company = $(tr).find('.sh_company').attr('set');
    $('#sh_company').val(sh_company);
    var sh_email = $(tr).find('.sh_email').attr('set');
    $('#sh_email').val(sh_email);
    var sh_address1 = $(tr).find('.sh_address1').attr('set');
    $('#sh_address1').val(sh_address1);
    var sh_address2 = $(tr).find('.sh_address2').attr('set');
    $('#sh_address2').val(sh_address2);
    var sh_address3 = $(tr).find('.sh_address3').attr('set');
    $('#sh_address3').val(sh_address3);
    var sh_phone = $(tr).find('.sh_phone').attr('set');
    $('#sh_phone').val(sh_phone);
    var sh_country = $(tr).find('.sh_country').attr('set');
    $('#sh_country').val(sh_country);
    $('#select2-sh_country-container').html(sh_country);
    var sh_country_code = $(tr).find('.sh_country_code').attr('set');
    $('#sh_country_code').val(sh_country_code);
    var sh_city = $(tr).find('.sh_city').attr('set');
    $('#sh_city').val(sh_city);
    var sh_suburb = $(tr).find('.sh_suburb').attr('set');
    $('#sh_suburb').val(sh_suburb);
    var sh_state = $(tr).find('.sh_state').attr('set');
    $('#sh_state').val(sh_state);
    var sh_postcode = $(tr).find('.sh_postcode').attr('set');
    $('#sh_postcode').val(sh_postcode);

    var sh_otherservice = $(tr).find('.sh_other_service'); //list
    console.log(sh_otherservice);
    $('.new_other_service').remove();
    sh_otherservice.each(function (i, n) {
        if ($(n).attr('set') == 'Pickup Fee') {
            var this_other = $('.ck1');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Time Guaranty') {
            var this_other = $('.ck2');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Remote Area') {
            var this_other = $('.ck3');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Insurance') {
            var this_other = $('.ck4');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Export Doc') {
            var this_other = $('.ck5');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'CO From') {
            var this_other = $('.ck6');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Over Size') {
            var this_other = $('.ck7');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Over Weight') {
            var this_other = $('.ck8');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Packing Paperbox') {
            var this_other = $('.ck9');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Wood Packing') {
            var this_other = $('.ck10');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else {
            $('#other_service_footer').before('<tr class="new_other_service">' +
                '<td><input type="checkbox" class="other_service_ck" onclick="other_ck($(this))" checked /></td>' +
                '<td style="padding: unset;"><input type="text" class="form-control other_service_name" value="' + $(n).attr('set') + '" required/></td>' +
                '<td style="padding: unset;"><input name="other"' +
                '        id="sh_wood_packing" class="form-control other_service_value"' +
                '        onkeypress="return isNumberKey(event)" value="' + $(n).attr('price') + '" disabled />' +
                '</td>' +
                '</tr>')
        }

        //str += '<input type="hidden" name="sh_otherservice_name[' + i + '][' + n + ']" value="' + $(tr).attr('set') + '" />'
        //str += '<input type="hidden" name="sh_otherservice_price[' + i + '][' + n + ']" value="' + $(tr).attr('price') + '" />'
    });
    var box = $(tr).find('.box'); //list
    $("#box tbody").html('');
    box.each(function (box_num, n) {
        $("#box tbody").append(
            "<tr>" +
            "<td>" + (box_num + 1) + "</td>" +
            '<td><input name="weight" class="weight form-control" onkeypress="return isNumberKey(event)" value="' + $(this).attr('weight') + '" required/></td>' +
            '<td><input name="width" class="width form-control" onkeypress="return isNumberKey(event)" value="' + $(this).attr('width') + '" /></td>' +
            '<td><input name="length" class="length form-control" onkeypress="return isNumberKey(event)" value="' + $(this).attr('length') + '" /></td>' +
            '<td><input name="height" class="height form-control" onkeypress="return isNumberKey(event)" value="' + $(this).attr('height') + '" /></td>' +
            '<td style="padding:unset"><button type="button" class="btn btn-sm btn-danger" onclick="box_remove(this)"> <span class="ti-trash"></span></button> <button type="button" class="btn btn-sm btn-success" onclick="box_copy(this)"> <span class="ti-plus"></span></button></td>' +
            "</tr>"
        );
    });
    $("#loading").hide();
}

function edit_sh(e) {
    $("#loading").show();
    $('#add_sh').click();
    var tr = $(e).parent().parent().parent().parent();
    this_sh_select = tr;
    var sh_id = $(tr).find('.sh_id').attr('set');
    $('#sh_id').val(sh_id);
    var sh_type_of_service = $(tr).find('.sh_type_of_service').attr('set');
    $('#sh_type_of_service').val(sh_type_of_service);
    var sh_insurance = $(tr).find('.sh_insurance').attr('set');
    $('#sh_insurance').val(sh_insurance);
    var sh_account_number = $(tr).find('.sh_account_number').attr('set');
    $('#sh_account_number').val(sh_account_number);
    var sh_so = $(tr).find('.sh_so').attr('set');
    $('#sh_so').val(sh_so);
    var sh_co = $(tr).find('.sh_co').attr('set');
    $('#sh_co').val(sh_co);

    var sh_s_company = $(tr).find('.sh_s_company').attr('set');
    $('#sh_s_company').val(sh_s_company);
    var sh_s_address1 = $(tr).find('.sh_s_address1').attr('set');
    $('#sh_s_address1').val(sh_s_address1);
    var sh_s_address2 = $(tr).find('.sh_s_address2').attr('set');
    $('#sh_s_address2').val(sh_s_address2);
    var sh_s_city = $(tr).find('.sh_s_city').attr('set');
    $('#sh_s_city').val(sh_s_city);
    var sh_s_postalcode = $(tr).find('.sh_s_postalcode').attr('set');
    $('#sh_s_postalcode').val(sh_s_postalcode);
    var sh_s_phone = $(tr).find('.sh_s_phone').attr('set');
    $('#sh_s_phone').val(sh_s_phone);

    var sh_awb = $(tr).find('.sh_awb').attr('set');
    $('#sh_awb').val(sh_awb);
    var sh_tracking_code = $(tr).find('.sh_tracking_code').attr('set');
    $('#sh_tracking_code').val(sh_tracking_code);
    var sh_tracking_DHL = $(tr).find('.sh_tracking_DHL').attr('set');
    $('#sh_tracking_DHL').val(sh_tracking_DHL);
    var sh_tracking_local = $(tr).find('.sh_tracking_local').attr('set');
    $('#sh_tracking_local').val(sh_tracking_local);
    var sh_shipping_type = $(tr).find('.sh_shipping_type').attr('set');
    $('#sh_shipping_type').val(sh_shipping_type);
    var sh_currency = $(tr).find('.sh_currency').attr('set');
    $('#sh_currency').val(sh_currency);
    var sh_declared_value = $(tr).find('.sh_declared_value').attr('set');
    $('#sh_declared_value').val(sh_declared_value);
    var sh_description = $(tr).find('.sh_description').attr('set');
    $('#sh_description').val(sh_description);
    var sh_content_quantity = $(tr).find('.sh_content_quantity').attr('set');
    $('#sh_content_quantity').val(sh_content_quantity);
    var sh_note = $(tr).find('.sh_note').attr('set');
    $('#sh_note').val(sh_note);
    var sh_packge_type = $(tr).find('.sh_packge_type').attr('set');
    $('#sh_packge_type').val(sh_packge_type);
    var sh_recipient = $(tr).find('.sh_recipient').attr('set');
    $('#sh_recipient').val(sh_recipient);
    var sh_company = $(tr).find('.sh_company').attr('set');
    $('#sh_company').val(sh_company);
    var sh_email = $(tr).find('.sh_email').attr('set');
    $('#sh_email').val(sh_email);
    var sh_address1 = $(tr).find('.sh_address1').attr('set');
    $('#sh_address1').val(sh_address1);
    var sh_address2 = $(tr).find('.sh_address2').attr('set');
    $('#sh_address2').val(sh_address2);
    var sh_address3 = $(tr).find('.sh_address3').attr('set');
    $('#sh_address3').val(sh_address3);
    var sh_phone = $(tr).find('.sh_phone').attr('set');
    $('#sh_phone').val(sh_phone);
    var sh_country = $(tr).find('.sh_country').attr('set');
    $('#sh_country').val(sh_country);
    $('#select2-sh_country-container').html(sh_country);
    var sh_country_code = $(tr).find('.sh_country_code').attr('set');
    $('#sh_country_code').val(sh_country_code);
    var sh_city = $(tr).find('.sh_city').attr('set');
    $('#sh_city').val(sh_city);
    var sh_suburb = $(tr).find('.sh_suburb').attr('set');
    $('#sh_suburb').val(sh_suburb);
    var sh_state = $(tr).find('.sh_state').attr('set');
    $('#sh_state').val(sh_state);
    var sh_postcode = $(tr).find('.sh_postcode').attr('set');
    $('#sh_postcode').val(sh_postcode);

    var sh_otherservice = $(tr).find('.sh_other_service'); //list
    console.log(sh_otherservice);
    $('.new_other_service').remove();
    sh_otherservice.each(function (i, n) {
        if ($(n).attr('set') == 'Pickup Fee') {
            var this_other = $('.ck1');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Time Guaranty') {
            var this_other = $('.ck2');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Remote Area') {
            var this_other = $('.ck3');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Insurance') {
            var this_other = $('.ck4');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Export Doc') {
            var this_other = $('.ck5');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'CO From') {
            var this_other = $('.ck6');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Over Size') {
            var this_other = $('.ck7');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Over Weight') {
            var this_other = $('.ck8');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Packing Paperbox') {
            var this_other = $('.ck9');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else if ($(n).attr('set') == 'Wood Packing') {
            var this_other = $('.ck10');
            this_other.prop("checked", true);
            this_other.parent().parent().find('.other_service_value').prop('disabled', false);
            this_other.parent().parent().find('.other_service_value').val($(n).attr('price'))
        } else {
            $('#other_service_footer').before('<tr class="new_other_service">' +
                '<td><input type="checkbox" class="other_service_ck" onclick="other_ck($(this))" checked /></td>' +
                '<td style="padding: unset;"><input type="text" class="form-control other_service_name" value="' + $(n).attr('set') + '" required/></td>' +
                '<td style="padding: unset;"><input name="other"' +
                '        id="sh_wood_packing" class="form-control other_service_value"' +
                '        onkeypress="return isNumberKey(event)" value="' + $(n).attr('price') + '" disabled />' +
                '</td>' +
                '</tr>')
        }

        //str += '<input type="hidden" name="sh_otherservice_name[' + i + '][' + n + ']" value="' + $(tr).attr('set') + '" />'
        //str += '<input type="hidden" name="sh_otherservice_price[' + i + '][' + n + ']" value="' + $(tr).attr('price') + '" />'
    });
    var box = $(tr).find('.box'); //list
    $("#box tbody").html('');
    box.each(function (box_num, n) {
        $("#box tbody").append(
            "<tr>" +
            "<td>" + (box_num + 1) + "</td>" +
            '<td><input name="weight" class="weight form-control" onkeypress="return isNumberKey(event)" value="' + $(this).attr('weight') + '" required/></td>' +
            '<td><input name="width" class="width form-control" onkeypress="return isNumberKey(event)" value="' + $(this).attr('width') + '" /></td>' +
            '<td><input name="length" class="length form-control" onkeypress="return isNumberKey(event)" value="' + $(this).attr('length') + '" /></td>' +
            '<td><input name="height" class="height form-control" onkeypress="return isNumberKey(event)" value="' + $(this).attr('height') + '" /></td>' +
            '<td style="padding:unset"><button type="button" class="btn btn-sm btn-danger" onclick="box_remove(this)"> <span class="ti-trash"></span></button> <button type="button" class="btn btn-sm btn-success" onclick="box_copy(this)"> <span class="ti-plus"></span></button></td>' +
            "</tr>"
        );
    });
    $("#loading").hide();
}
$('#add_sh').click(function () {
    $('#sh_postcode').removeClass('is-invalid');
    $('#sh_postcode').removeClass('is-valid');
    $('#sh_id').val('');
    $('#sh_tracking_code').val('');
    $('#select2-sh_country-container').html('UNITED STATES OF AMERICA');
    var sh_id = $('#sh_id').val();
    if (sh_id == '') {
        $("#sh_reset").click();
        $('.new_other_service').remove();
        $('.other_service_value').prop('disabled', true);
        var box = $("#box tbody").html(
            "<tr>" +
            "<td>1</td>" +
            '<td><input name="weight" class="weight form-control" onkeypress="return isNumberKey(event)" required/></td>' +
            '<td><input name="width" class="width form-control" onkeypress="return isNumberKey(event)" /></td>' +
            '<td><input name="length" class="length form-control" onkeypress="return isNumberKey(event)" /></td>' +
            '<td><input name="height" class="height form-control" onkeypress="return isNumberKey(event)" /></td>' +
            '<td style="padding:unset"><button type="button" class="btn btn-sm btn-danger" onclick="box_remove(this)"> <span class="ti-trash"></span></button> <button type="button" class="btn btn-sm btn-success" onclick="box_copy(this)"> <span class="ti-plus"></span></button></td>' +
            "</tr>"
        );
    }
});

function smecode(code) {
    var result = code;
    if (code.length == 6) {
        result = code.substr(0, 2) + ' ' + code.substr(2, 2) + ' ' + code.substr(4, 2);
    }
    return result;
}

function printsmelabel(obj) {
    //var tr = $(obj).parent().parent();
    window.open($(obj).attr('action'), '_blank');
    console.log(obj);
}

$('#sh_type_of_service').change(function () {
    var x = $('#sh_id').val();
    var s_id = $("#sh_type_of_service option:selected").val();
    if (true) {
        $.ajax({
            type: "get",
            url: "/getOtherService" + '/' + s_id,
            success: function (result) {
                $("#loading").hide();
                var data = $.parseJSON(result);
                if (data.pickup_fee > 0) {
                    $('.ck1').prop('checked', true);
                    $('#sh_pickup_fee').val(data.pickup_fee).prop('disabled', false);
                } else {
                    $('.ck1').prop('checked', false);
                    $('#sh_pickup_fee').prop('disabled', true);
                }
                if (data.time_guaranty > 0) {
                    $('.ck2').prop('checked', true);
                    $('#sh_time_guaranty').val(data.time_guaranty).prop('disabled', false);
                } else {
                    $('.ck2').prop('checked', false);
                    $('#sh_time_guaranty').prop('disabled', true);
                }
                if (data.remote_area > 0) {
                    $('.ck3').prop('checked', true);
                    $('#sh_remote_area').val(data.remote_area).prop('disabled', false);
                } else {
                    $('.ck3').prop('checked', false);
                    $('#sh_remote_area').prop('disabled', true);
                }
                if (data.insurance > 0) {
                    $('.ck4').prop('checked', true);
                    $('#sh_insurance').val(data.insurance).prop('disabled', false);
                } else {
                    $('.ck4').prop('checked', false);
                    $('#sh_insurance').prop('disabled', true);
                }
                if (data.export_doc > 0) {
                    $('.ck5').prop('checked', true);
                    $('#sh_export_doc').val(data.export_doc).prop('disabled', false);
                } else {
                    $('.ck5').prop('checked', false);
                    $('#sh_export_doc').prop('disabled', true);
                }
                if (data.co_from > 0) {
                    $('.ck6').prop('checked', true);
                    $('#sh_co_from').val(data.co_from).prop('disabled', false);
                } else {
                    $('.ck6').prop('checked', false);
                    $('#sh_co_from').prop('disabled', true);
                }
                if (data.over_size > 0) {
                    $('.ck7').prop('checked', true);
                    $('#sh_over_size').val(data.over_size).prop('disabled', false);
                } else {
                    $('.ck7').prop('checked', false);
                    $('#sh_over_size').prop('disabled', true);
                }
                if (data.over_weight > 0) {
                    $('.ck8').prop('checked', true);
                    $('#sh_over_weight').val(data.over_weight).prop('disabled', false);
                } else {
                    $('.ck8').prop('checked', false);
                    $('#sh_over_weight').prop('disabled', true);
                }
                if (data.packing_paperbox > 0) {
                    $('.ck9').prop('checked', true);
                    $('#sh_packing_paperbox').val(data.packing_paperbox).prop('disabled', false);
                } else {
                    $('.ck9').prop('checked', false);
                    $('#sh_packing_paperbox').prop('disabled', true);
                }
                if (data.wood_packing > 0) {
                    $('.ck10').prop('checked', true);
                    $('#sh_wood_packing').val(data.wood_packing).prop('disabled', false);
                } else {
                    $('.ck10').prop('checked', false);
                    $('#sh_wood_packing').prop('disabled', true);
                }

                console.log(data);
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    }
});

function upload_inv(id) {

    var form_data = new FormData();
    form_data.append("file", $('#file-input' + id)[0].files[0]);
    // /console.log(form_data);
    var size_file = ($('#file-input' + id)[0].files[0].size / 1024) / 1024;
    if (size_file <= 1) {
        $("#loading").show();
        $.ajax({
            type: "POST",
            data: form_data,
            url: '/upload_invoice/' + id,
            processData: false,
            contentType: false,
            success: function (result) {
                $("#loading").hide();
                $('.inv_' + id).html('<a href="/uploads/invoice/' + result + '" target="_blank"> ' +
                    '<i class="mdi mdi-file" style="font-size:18px"></i>View Invoice</a><span aria-hidden="true" class="remove_inv" onclick="reset_inv(' + id + ')">×</span>');

                console.log(result);
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    } else {
        alert('ไฟล์ที่คุณเลือกมีขนาดเกินกำหนด 1 MB.')
    }

    return true;
}

function reset_inv(id) {
    var x = confirm("Are you sure you want to delete invoice?");
    if (x) {
        $("#loading").show();
        $.ajax({
            type: "GET",
            url: '/upload_resetinvoice/' + id,
            processData: false,
            contentType: false,
            success: function (result) {
                $("#loading").hide();
                $('.inv_' + id).html(' <label for="file-input' + id + '" class="btn btn-outline-danger waves-effect waves-light">' +
                    '<i class="mdi mdi-file" style="font-size:18px"></i>Commercial Invoice</label>' +
                    '<input id="file-input' + id + '" type="file" accept="application/pdf" style="display:none" onchange="upload_inv(' + id + ')">');

                console.log(result);
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    }
    return true;
}

function upload_payment(id) {
    $("#loading").show();
    var form_data = new FormData();
    var totalfiles = document.getElementById('payment_file').files.length;
    for (var index = 0; index < totalfiles; index++) {
        form_data.append("files[]", document.getElementById('payment_file').files[index]);
    }
    // /console.log(form_data);
    $.ajax({
        type: "POST",
        data: form_data,
        url: '/upload_payment/' + id,
        processData: false,
        contentType: false,
        success: function (result) {
            $("#loading").hide();

            var data = JSON.parse(result);
            console.log(result);
            var str = '';
            $(data).each(function (i, e) {
                str += '<div class="f_left"><a href="/uploads/payment/' + e.filename + '" target="_blank"><img src="/uploads/payment/' + e.filename + '" width="50px" height="50px" class="img-thumbnail" /></a>' +
                    '<span aria-hidden="true" class="remove_inv" onclick="reset_payment(' + e.id + ',$(this))" style="padding-right: 10px;">×</span></div>';
            });
            $('#viewpayment').append(str);
            checkpayment();
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert(
                "An error occurred on the server when processing the URL. Please contact the system administrator."
            );
            $("#loading").hide();
        }
    });

    return true;
}
function upload_so(id) {
    $("#loading").show();
    var form_data = new FormData();
    var totalfiles = document.getElementById('so_file').files.length;
    for (var index = 0; index < totalfiles; index++) {
        form_data.append("files[]", document.getElementById('so_file').files[index]);
    }
    // /console.log(form_data);
    $.ajax({
        type: "POST",
        data: form_data,
        url: '/upload_so/' + id,
        processData: false,
        contentType: false,
        success: function (result) {
            $("#loading").hide();
            var data = JSON.parse(result);
            console.log(result);
            var str = '';
            $(data).each(function (i, e) {
                str += '<div class="row text-left bg-light p-3 mb-3"><div class="col-sm-8">ไม่ได้ระบุข้อมูล</div><div class="col-sm-4"><a href="/uploads/so/' + e.so_image + '" target="_blank"><img src="/uploads/so/' + e.so_image + '" width="50" class="img-thumbnail"></a></div></div>';
            });
            $('#viewattachment').append(str);
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert(
                "An error occurred on the server when processing the URL. Please contact the system administrator."
            );
            $("#loading").hide();
        }
    });

    return true;
}


function reset_payment(id, obj) {
    var x = confirm("Are you sure you want to delete payment file?");
    if (x) {
        $("#loading").show();
        $.ajax({
            type: "GET",
            url: '/upload_resetpayment/' + id,
            processData: false,
            contentType: false,
            success: function (result) {
                $("#loading").hide();
                console.log(obj);
                $(obj).parent().remove();
                checkpayment();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    }

    return true;
}
$('#payment_value').change(function () {
    checkpayment();
});
$('#payment_value').change();

$('#payment_type').change(function () {
    checkpayment();
});

function checkpayment() {
    var type = $('#payment_type option:selected').val();
    if (type == "Credit") {
        $('#payment_status').val(1);
    } else if (type == "โอนเงิน") {
        var viewpayment = $('#viewpayment').find('.f_left').length;
        var thisvalue = parseFloat($('#payment_value').val());
        var thisprice = parseFloat($('#grand_total').html().replace(/,/g, ''));
        var percent3 = thisprice * 3 / 100;
        if (thisvalue >= thisprice - percent3 && viewpayment > 0) {
            $('#payment_status').val(1);
        } else {
            $('#payment_status').val(0);
        }
    } else if (type == "2C2P") {
        var viewpayment = $('#viewpayment').find('.f_left').length;
        var thisvalue = parseFloat($('#payment_value').val());
        var thisprice = parseFloat($('#grand_total').html().replace(/,/g, ''));
        var percent3 = thisprice * 3 / 100;
        if (thisvalue >= thisprice - percent3 && viewpayment > 0) {
            $('#payment_status').val(1);
        } else {
            $('#payment_status').val(0);
        }
    } else {
        var thisvalue = parseFloat($('#payment_value').val());
        var thisprice = parseFloat($('#grand_total').html().replace(/,/g, ''));
        var percent3 = thisprice * 3 / 100;
        if (thisvalue >= thisprice - percent3) {
            $('#payment_status').val(1);
        } else {
            $('#payment_status').val(0);
        }
    }
}
$(document).ready(function ($) {
    $('#sh_postcode').keyup(function () {
        $('#sh_postcode').unmask();
        $('#sh_postcode').addClass('is-invalid');
        $('#sh_postcode').removeClass('is-valid');
        this.value = this.value.toUpperCase();
    });
    $('#sh_postcode').change(function () {
        var sh_country = $('#sh_country option:selected');
        var sh_country_code = sh_country.attr('code');
        var sh_zipcode = $('#sh_postcode').val();
        $('#sh_city').val('');
        if (sh_zipcode != "") {
            $.ajax({
                type: "GET",
                url: '/checkformat/' + sh_country_code + '/' + sh_zipcode,
                processData: false,
                contentType: false,
                success: function (result) {
                    console.log(result);
                    if (result.status) {
                        if (result.res.length == 1) {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_city').val(result.res[0].city_name);
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $('#select_city').append(new Option(result.res[0].city_name, result.res[0].city_name));
                        } else {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_city').val('');
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $(result.res).each(function (i, e) {
                                $('#select_city').append(new Option(e.city_name, e.city_name));
                            })

                        }

                    } else {
                        $('#sh_postcode').addClass('is-invalid');
                    }
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    $('#sh_postcode').addClass('is-invalid');
                    $('#sh_postcode').removeClass('is-valid');
                    $("#loading").hide();
                }
            });
        } else {
            $("#loading").hide();
            $('#sh_postcode').removeClass('is-invalid');
        }
    });

    $('#sh_country').change(function () {
        var sh_country_code = $('#sh_country option:selected').attr('code');
        $.ajax({
            type: "GET",
            url: '/admin/getcitybycountrycode/' + sh_country_code,
            processData: false,
            contentType: false,
            success: function (result) {
                result = JSON.parse(result);
                $('#sh_city').val('');
                $('#select_city').html('');
                $(result).each(function (i, e) {
                    $('#select_city').append(new Option(e.city_name, e.city_name));
                });
            },
            error: function (e) {
                console.log("ERROR : ", e);
                $('#sh_postcode').addClass('is-invalid');
                $('#sh_postcode').removeClass('is-valid');
                $("#loading").hide();
            }
        });
    });

    $('#sh_city, #sh_postcode').change(function () {
        var sh_country_code = $('#sh_country option:selected').attr('code');
        var sh_city = $('#sh_city').val();
        $.ajax({
            type: "GET",
            url: '/admin/getsuburb/' + sh_country_code + '/' + sh_city,
            processData: false,
            contentType: false,
            success: function (result) {
                result = JSON.parse(result);
                console.log(result);
                if (result.length > 0) {
                    $('.subrub_area').show();
                    $(result).each(function (i, e) {
                        $('#select_subrub').append(new Option(e.suburb, e.suburb));
                    });
                } else {
                    $('#sh_suburb').val('');
                    $('.subrub_area').hide();
                }

            },
            error: function (e) {
                console.log("ERROR : ", e);
                $('#sh_postcode').addClass('is-invalid');
                $('#sh_postcode').removeClass('is-valid');
                $("#loading").hide();
            }
        });
    });

    $('#sh_postcode').keyup(function () {
        var sh_country = $('#sh_country option:selected');
        var sh_country_code = sh_country.attr('code');
        var sh_zipcode = $('#sh_postcode').val();
        $('#sh_city').val('');
        if (sh_zipcode != "") {
            $.ajax({
                type: "GET",
                url: '/checkformat/' + sh_country_code + '/' + sh_zipcode,
                processData: false,
                contentType: false,
                success: function (result) {
                    console.log(result);
                    if (result.status) {
                        if (result.res.length == 1) {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_city').val(result.res[0].city_name);
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $('#select_city').append(new Option(result.res[0].city_name, result.res[0].city_name));
                        } else {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_city').val('');
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $(result.res).each(function (i, e) {
                                $('#select_city').append(new Option(e.city_name, e.city_name));
                            })

                        }

                    } else {
                        $('#sh_postcode').addClass('is-invalid');
                    }
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    $('#sh_postcode').addClass('is-invalid');
                    $('#sh_postcode').removeClass('is-valid');
                    $("#loading").hide();
                }
            });
        } else {
            $("#loading").hide();
            $('#sh_postcode').removeClass('is-invalid');
        }
    });
});

// Get discount
$("#code_discount").keyup(function discount() {
    debugger;
    var index = 0;
    var service_id = "";
    var country_code = "";
    var code = $('#code_discount').val();
    var weight = $('#total_weight').text();
    var total_service = $('#grand_total').text();
    var str = ''

    $('.desc').each(function(i, e) {
        if (index == 0) {
            var sh_country_code = $(e).find(".sh_country_code_0").text();
            country_code += sh_country_code;
            var sh_service = $(e).find(".sh_service").text();
            service_id += sh_service;
            index++;
        } else {
            var sh_country_code = $(e).find(".sh_country_code_0").text();
            country_code += "," + sh_country_code;
            var sh_service = $(e).find(".sh_service").text();
            service_id += "," + sh_service;
            index++;
        }
    });
    $.ajax({
        type: "GET",
        url: '/getdiscount/' + code + '/' + weight + '/' + service_id + '/' + country_code,
        success: function(result) {
            if (result.status == "True") {
                if (result.type == "THB") {
                    $("#input_discount").val(result.number);
                    str = '<span style="color:green;">รหัสส่วนลดนี้มีอยู่ในระบบ</span>';
                    $("#Error_discount").html(str);
                } else {
                    disconut_percent = (total_service * result.number) / 100
                    $("#input_discount").val(disconut_percent);
                    $("#Error_discount").append(str);
                }
            } else {
                str = '<span style="color:red;">' + result.error + '</span>';
                if ($('#code_discount').val() == "" || $('#code_discount').val() == null) {
                    $("#Error_discount").hide();
                } else
                    $("#Error_discount").show();
                $("#Error_discount").html(str);
            }
        },
        error: function(e) {
            console.log("ERROR : ", e);
            $('#loading').hide();
        }
    });
});
// end 


//------------read excel --------------------

$(function () {
    oFileIn = document.getElementById('my_file_input');
    if (oFileIn.addEventListener) {
        oFileIn.addEventListener('change', filePicked, false);
    }
});

function filePicked(oEvent) {
    // Get The File From The Input
    var oFile = oEvent.target.files[0];
    var sFilename = oFile.name;
    var s = sFilename;
    var lastIndex = s.lastIndexOf(".");
    var nametype = s.substring(lastIndex + 1);
    if (nametype == 'csv') {
        var reader = new FileReader();

        // Ready The Event For When A File Gets Selected
        reader.onload = function (e) {
            var data = e.target.result;
            var allTextLines = data.split(/\r\n|\n/);
            $('#message-notdata').remove();
            $(allTextLines).each(async function (indexR, valueR) {
                var valueR = valueR.split(',');
                if (valueR.length > 11 && indexR > 0) {
                    var row_no = $("#shipment_detail tbody tr").length + 1;
                    //start row
                    var row = '';
                    row += '<td><label class="checkbox_select"><input type="checkbox" class="check_all" onclick="check_select(this)"></label></td>';
                    row += "<td>" + (indexR) + "</td>";
                    //Information----------------
                    var service = $("#sh_type_of_service_excel option:selected");
                    var sh_so = valueR[0];
                    var sh_co = 'SME SHIPPING';
                    var sh_awb = valueR[1] != undefined ? valueR[1] : '';
                    var sh_tracking_DHL = valueR[10] != undefined ? valueR[10] : '';
                    var sh_tracking_local = '';
                    var sh_tracking_code = '';
                    var cal = service.attr('cal');

                    row +=
                        "<td>" +
                        '<span class="b-title sh_id" set="">Service:</span>' +
                        '<span class="sh_type_of_service" set="' + service.val() + '">' + service.text() + "</span><br>" +
                        '<span class="b-title">Insurance:</span> <span class="sh_insurance" set=""></span><br>' +
                        '<span class="b-title">Account Number:</span> <span class="sh_account_number" set=""></span><br>' +
                        '<span class="b-title">SO:</span> <span class="sh_so" set="' + sh_so + '">' + sh_so + "</span><br>" +
                        '<span class="b-title">C/O:</span> <span class="sh_co" set="' + sh_co + '">' + sh_co + "</span><br>" +
                        '<span class="b-title">Company:</span> <span class="sh_s_company" set=""></span><br>' +
                        '<span class="b-title">Address1:</span> <span class="sh_s_address1" set=""></span><br>' +
                        '<span class="b-title">Address2:</span> <span class="sh_s_address2" set=""></span><br>' +
                        '<span class="b-title">City:</span> <span class="sh_s_city" set=""></span><br>' +
                        '<span class="b-title">Postal Code:</span> <span class="sh_s_postalcode" set=""></span><br>' +
                        '<span class="b-title">Phone Number:</span> <span class="sh_s_phone" set=""></span><br>' +
                        '<span class="b-title">AWB:</span> <span class="sh_awb" set="' + sh_awb + '">' + sh_awb + '</span><br>' +
                        '<span class="b-title">Routing:</span>' +
                        '<span class="sh_routing_id" set="' + service.attr('routing') + '">' + service.attr('routing') + '</span><br>' +
                        '<span class="b-title">Cal:</span>' +
                        '<span class="sh_cal" set="' + cal + '">' + (cal == 'up' ? 'Up' : 'Normal') + '</span><br>' +
                        '<span class="b-title">Tracking DHL:</span>' +
                        '<span class="sh_tracking_DHL" set="' + sh_tracking_DHL + '">' + sh_tracking_DHL + '</span>' +
                        "<br>" +
                        '<span class="b-title">Warehouse:</span>' +
                        '<span class="sh_tracking_local" set="' + sh_tracking_local + '">' + sh_tracking_local + '</span>' +
                        "<br>" +
                        '<span class="b-title">Tracking SME:</span>' +
                        '<span class="sh_tracking_code" set="' + sh_tracking_code + '"> ' + smecode(sh_tracking_code) + '</span>' +
                        "</td>";
                    //Shipment----------------
                    var sh_shipment_type = '';
                    var sh_currency = 'THB';
                    var sh_declared_value = '';
                    var sh_content_quantity = '';
                    var sh_description = '';
                    var sh_note = '';
                    var sh_packge_type = $('#sh_packge_type option:selected');
                    row += "<td>" +
                        '<span class="b-title">Shipping Type:</span>' +
                        '<span class="sh_shipping_type" set="' + sh_shipment_type + '">' + sh_shipment_type + '</span><br>' +
                        '<span class="b-title">Currency:</span>' +
                        '<span class="sh_currency" set="' + sh_currency + '">' + sh_currency + '</span><br>' +
                        '<span class="b-title">Declared Value:</span>' +
                        '<span class="sh_declared_value" set="' + sh_declared_value + '">' + sh_declared_value + '</span><br>' +
                        '<span class="b-title">Description:</span>' +
                        '<span class="sh_description" set="' + sh_description + '">' + sh_description + '</span><br>' +
                        '<span class="b-title">Content Quantity:</span>' +
                        '<span class="sh_content_quantity" set="' + sh_content_quantity + '">' + sh_content_quantity + '</span><br>' +
                        '<span class="b-title">Note:</span>' +
                        '<span class="sh_note" set="' + sh_note + '">' + sh_note + '</span><br>' +
                        '<span class="b-title">Packge Type:</span>' +
                        '<span class="sh_packge_type" set="' + sh_packge_type.val() + '">' + sh_packge_type.html() + '</span><br>' +
                        "</td>";
                    //Recipient----------------
                    var sh_recipient = valueR[2];
                    var sh_company = '';
                    var sh_email = "";
                    var sh_address1 = valueR[11] != undefined && valueR[11] != '' ? valueR[11] : '-';
                    var sh_address2 = '';
                    var sh_address3 = '';
                    var sh_phone = '';
                    var sh_country = '';
                    var sh_country_code = valueR[7] != undefined ? valueR[7] : '';
                    if (sh_country == '') {
                        sh_country = $('#sh_country option[code=' + sh_country_code + ']').val();
                    }
                    var sh_city = valueR[8] != undefined ? valueR[8] : '';
                    var sh_state = '';
                    var sh_postcode = valueR[9] != undefined ? valueR[9] : '';
                    var sh_suburb = '';
                    row += "<td>" +
                        '<span class="b-title">Name:</span>' +
                        '<span class="sh_recipient" set="' + sh_recipient + '">' + sh_recipient + '</span><br>' +
                        '<span class="b-title">Company:</span>' +
                        '<span class="sh_company" set="' + sh_company + '">' + sh_company + '</span><br>' +
                        '<span class="b-title">E-Mail:</span>' +
                        '<span class="sh_email" set="' + sh_email + '">' + sh_email + '</span><br>' +
                        '<span class="b-title">Address1:</span>' +
                        '<span class="sh_address1" set="' + sh_address1 + '">' + sh_address1 + '</span><br>' +
                        '<span class="b-title">Address2:</span>' +
                        '<span class="sh_address2" set="' + sh_address2 + '">' + sh_address2 + '</span><br>' +
                        '<span class="b-title">Address3:</span>' +
                        '<span class="sh_address3" set="' + sh_address3 + '">' + sh_address3 + '</span><br>' +
                        '<span class="b-title">Phone Number:</span>' +
                        '<span class="sh_phone" set="' + sh_phone + '">' + sh_phone + '</span><br>' +
                        '<span class="b-title">Country:</span>' +
                        '<span class="sh_country" set="' + sh_country + '"> ' + sh_country + ' </span><br>' +
                        '<span class="b-title">Country Code:</span>' +
                        '<span class="sh_country_code" set="' + sh_country_code + '">' + sh_country_code + '</span><br>' +
                        '<span class="b-title">City:</span>' +
                        '<span class="sh_city" set="' + sh_city + '">' + sh_city + '</span><br>' +
                        '<span class="b-title">Suburb:</span>' +
                        '<span class="sh_suburb" set="' + sh_suburb + '">' + sh_suburb + '</span><br>' +
                        '<span class="b-title">State:</span>' +
                        '<span class="sh_state" set="' + sh_state + '">' + sh_state + '</span><br>' +
                        '<span class="b-title">Postal Code:</span>' +
                        '<span class="sh_postcode" set="' + sh_postcode + '">' + sh_postcode + '</span><br>' +
                        "</td>";
                    //Other Service----------------
                    var total_other_service = 0;
                    row += '<td><div class="sum-bottom total_other_service" set="' + total_other_service + '">Total Amount:' + total_other_service + ' ฿' +
                        '</div>' +
                        '</td>';
                    //Volume(KG)----------------
                    var totalweight = 0;
                    row += "<td>";

                    var weight = valueR[3] != undefined && valueR[3] != '' ? valueR[3] : 0;
                    weight = parseFloat(weight);
                    var width = valueR[4] != undefined && valueR[4] != '' ? valueR[4] : 0;
                    var length = valueR[5] != undefined && valueR[5] != '' ? valueR[5] : 0;
                    var height = valueR[6] != undefined && valueR[6] != '' ? valueR[6] : 0;
                    var sum = width * length * height / 5000;
                    // sum = parseFloat(convert2number((cal == 'up' ? calup(sum) : sum)));
                    totalweight += (weight > sum ? weight : sum);
                    row += '<span class="box b-title" weight="' + weight + '" width="' + width + '" length="' + length + '" height="' + height + '" series="1">1 :</span>' +
                        '<span class="' + (weight > sum ? "badge badge-boxed badge-soft-success" : "") + '">' + weight + '</span><span>' +
                        'KG // ' + width + ' x ' + length + ' x ' + height + ' = </span><span class="' + (weight < sum ? "badge badge-boxed badge-soft-success" : "0") + '">' + totalweight + '</span>' +
                        '<hr class="line-npd">';

                    row += '<div class="sum-bottom">Total Weight : <span class="total_weight" set="' + totalweight + '">' + totalweight.toFixed(2) + '</span> KG.</div>' +
                        "</td>";

                    //CHARGE----------------    
                    var url = '/getprice/' + service.val() + '/' + sh_country_code + '/' + totalweight;
                    const total_regular = await $.ajax({
                        type: "GET",
                        url: url,
                        success: function (result) {
                            return result;
                        },
                        error: function (e) {
                            console.log("ERROR : ", e);
                            $('#loading').hide();
                            //alert('พบปัญหาในการขอราคา โปรดตรวจสอบให้แน่ใจว่าเลือกประเทศปลายทาง กรอกขนาดและน้ำหนัก และเลือกบริการเรียบร้อย? หากยังพบปัญหาโปรดแจ้งผู้ดูแลระบบ');
                            return 0;
                        }
                    });
                    var total_charge = total_other_service + parseFloat(total_regular);
                    row += "<td>" +
                        '<span class="b-title">Regular:</span><span class="total_regular_item" set="' + total_regular + '">' + total_regular + '</span><br>' +
                        '<span class="b-title">Other Service:</span><span class="total_other_service_item" set="' + total_other_service + '"> ' + total_other_service + ' ฿</span><br>' +
                        '<div class="sum-bottom total_charge_item" set="' + total_charge + '"> Total Charge: ' + total_charge + ' ฿ </div > ' +
                        "</td>";

                    //Action----------------
                    row += "<td>";
                    row += '<div class="dropdown d-inline-block float-right">';
                    row += '    <a class="nav-link dropdown-toggle arrow-none" id="dLabel8"  data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">';
                    row += '        <i class="fas fa-ellipsis-v font-20 text-muted"></i>';
                    row += "    </a>";
                    row += '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel8">';
                    row += '<a class="dropdown-item" onclick="copy_sh($(this))">Copy</a>';
                    row += '<a class="dropdown-item" onclick="edit_sh($(this))">Edit</a>';
                    row += '<a class="dropdown-item" onclick="delete_sh($(this))">Delete</a>';
                    row += "</div>";
                    row += "</div>";
                    row += "</td>";
                    //end row
                    row = "<tr>" + row + "</tr>"
                    $("#shipment_detail tbody").append(row);
                    sortTable();
                    binding();
                    $('#save_shipment').show();
                }
            });
        }
    } else {
        // Create A File Reader HTML5
        var reader = new FileReader();

        // Ready The Event For When A File Gets Selected
        reader.onload = function (e) {
            var data = e.target.result;
            var cfb = XLS.CFB.read(data, { type: 'binary' });
            var wb = XLS.parse_xlscfb(cfb);
            // Loop Over Each Sheet
            // Obtain The Current Row As CSV
            var sheetName = "Sheet1";
            var sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);
            var data = XLS.utils.sheet_to_json(wb.Sheets[sheetName], { header: 1 });
            $('#message-notdata').remove();
            $(data).each(async function (indexR, valueR) {
                if (valueR.length > 20 && indexR > 0) {
                    debugger
                    var row_no = $("#shipment_detail tbody tr").length + 1;
                    //start row
                    var row = '';
                    row += '<td><label class="checkbox_select"><input type="checkbox" class="check_all" onclick="check_select(this)"></label></td>';
                    row += "<td>" + (indexR) + "</td>";
                    //Information----------------
                    var service = $("#sh_type_of_service_excel option:selected");
                    var sh_so = valueR[2];
                    var sh_co = 'SME SHIPPING';
                    var sh_awb = valueR[3] != undefined ? valueR[3] : '';
                    var sh_tracking_DHL = '';
                    var sh_tracking_local = '';
                    var sh_tracking_code = '';
                    var cal = service.attr('cal');

                    row +=
                        "<td>" +
                        '<span class="b-title sh_id" set="">Service:</span>' +
                        '<span class="sh_type_of_service" set="' + service.val() + '">' + service.text() + "</span><br>" +
                        '<span class="b-title">Insurance:</span> <span class="sh_insurance" set=""></span><br>' +
                        '<span class="b-title">Account Number:</span> <span class="sh_account_number" set=""></span><br>' +
                        '<span class="b-title">SO:</span> <span class="sh_so" set="' + sh_so + '">' + sh_so + "</span><br>" +
                        '<span class="b-title">C/O:</span> <span class="sh_co" set="' + sh_co + '">' + sh_co + "</span><br>" +
                        '<span class="b-title">Company:</span> <span class="sh_s_company" set=""></span><br>' +
                        '<span class="b-title">Address1:</span> <span class="sh_s_address1" set=""></span><br>' +
                        '<span class="b-title">Address2:</span> <span class="sh_s_address2" set=""></span><br>' +
                        '<span class="b-title">City:</span> <span class="sh_s_city" set=""></span><br>' +
                        '<span class="b-title">Postal Code:</span> <span class="sh_s_postalcode" set=""></span><br>' +
                        '<span class="b-title">Phone Number:</span> <span class="sh_s_phone" set=""></span><br>' +
                        '<span class="b-title">AWB:</span> <span class="sh_awb" set="' + sh_awb + '">' + sh_awb + '</span><br>' +
                        '<span class="b-title">Routing:</span>' +
                        '<span class="sh_routing_id" set="' + service.attr('routing') + '">' + service.attr('routing') + '</span><br>' +
                        '<span class="b-title">Cal:</span>' +
                        '<span class="sh_cal" set="' + cal + '">' + (cal == 'up' ? 'Up' : 'Normal') + '</span><br>' +
                        '<span class="b-title">Tracking DHL:</span>' +
                        '<span class="sh_tracking_DHL" set="' + sh_tracking_DHL + '">' + sh_tracking_DHL + '</span>' +
                        "<br>" +
                        '<span class="b-title">Warehouse:</span>' +
                        '<span class="sh_tracking_local" set="' + sh_tracking_local + '">' + sh_tracking_local + '</span>' +
                        "<br>" +
                        '<span class="b-title">Tracking SME:</span>' +
                        '<span class="sh_tracking_code" set="' + sh_tracking_code + '"> ' + smecode(sh_tracking_code) + '</span>' +
                        "</td>";
                    //Shipment----------------
                    var sh_shipment_type = valueR[4] != undefined ? valueR[4] : '';;
                    var sh_currency = valueR[20] != undefined ? valueR[20] : '';;
                    var sh_declared_value = valueR[21] != undefined ? valueR[21] : '';;
                    var sh_content_quantity = valueR[42] != undefined ? valueR[42] : '';;
                    var sh_description = valueR[33] != undefined ? valueR[33] : '';;
                    var sh_note = valueR[47] != undefined ? valueR[47] : '';
                    var sh_packge_type = $('#sh_packge_type option:selected');
                    row += "<td>" +
                        '<span class="b-title">Shipping Type:</span>' +
                        '<span class="sh_shipping_type" set="' + sh_shipment_type + '">' + sh_shipment_type + '</span><br>' +
                        '<span class="b-title">Currency:</span>' +
                        '<span class="sh_currency" set="' + sh_currency + '">' + sh_currency + '</span><br>' +
                        '<span class="b-title">Declared Value:</span>' +
                        '<span class="sh_declared_value" set="' + sh_declared_value + '">' + sh_declared_value + '</span><br>' +
                        '<span class="b-title">Description:</span>' +
                        '<span class="sh_description" set="' + sh_description + '">' + sh_description + '</span><br>' +
                        '<span class="b-title">Content Quantity:</span>' +
                        '<span class="sh_content_quantity" set="' + sh_content_quantity + '">' + sh_content_quantity + '</span><br>' +
                        '<span class="b-title">Note:</span>' +
                        '<span class="sh_note" set="' + sh_note + '">' + sh_note + '</span><br>' +
                        '<span class="b-title">Packge Type:</span>' +
                        '<span class="sh_packge_type" set="' + sh_packge_type.val() + '">' + sh_packge_type.html() + '</span><br>' +
                        "</td>";
                    //Recipient----------------
                    var sh_recipient = valueR[6];
                    var sh_company = valueR[5] != undefined ? valueR[5] : '';
                    var sh_email = "";
                    var sh_address1 = valueR[7] != undefined ? valueR[7] : '';
                    var sh_address2 = valueR[8] != undefined ? valueR[8] : '';
                    var sh_address3 = valueR[9] != undefined ? valueR[9] : '';
                    var sh_phone = valueR[14] != undefined ? valueR[14] : '';
                    var sh_country = valueR[15] != undefined ? valueR[15] : '';
                    var sh_country_code = valueR[13] != undefined ? valueR[13] : '';
                    if (sh_country == '') {
                        sh_country = $('#sh_country option[code=' + sh_country_code + ']').val();
                    }
                    var sh_city = valueR[10] != undefined ? valueR[10] : '';
                    var sh_state = valueR[11] != undefined ? valueR[11] : '';
                    var sh_postcode = valueR[12] != undefined ? valueR[12] : '';
                    var sh_suburb = '';
                    row += "<td>" +
                        '<span class="b-title">Name:</span>' +
                        '<span class="sh_recipient" set="' + sh_recipient + '">' + sh_recipient + '</span><br>' +
                        '<span class="b-title">Company:</span>' +
                        '<span class="sh_company" set="' + sh_company + '">' + sh_company + '</span><br>' +
                        '<span class="b-title">E-Mail:</span>' +
                        '<span class="sh_email" set="' + sh_email + '">' + sh_email + '</span><br>' +
                        '<span class="b-title">Address1:</span>' +
                        '<span class="sh_address1" set="' + sh_address1 + '">' + sh_address1 + '</span><br>' +
                        '<span class="b-title">Address2:</span>' +
                        '<span class="sh_address2" set="' + sh_address2 + '">' + sh_address2 + '</span><br>' +
                        '<span class="b-title">Address3:</span>' +
                        '<span class="sh_address3" set="' + sh_address3 + '">' + sh_address3 + '</span><br>' +
                        '<span class="b-title">Phone Number:</span>' +
                        '<span class="sh_phone" set="' + sh_phone + '">' + sh_phone + '</span><br>' +
                        '<span class="b-title">Country:</span>' +
                        '<span class="sh_country" set="' + sh_country + '"> ' + sh_country + ' </span><br>' +
                        '<span class="b-title">Country Code:</span>' +
                        '<span class="sh_country_code" set="' + sh_country_code + '">' + sh_country_code + '</span><br>' +
                        '<span class="b-title">City:</span>' +
                        '<span class="sh_city" set="' + sh_city + '">' + sh_city + '</span><br>' +
                        '<span class="b-title">Suburb:</span>' +
                        '<span class="sh_suburb" set="' + sh_suburb + '">' + sh_suburb + '</span><br>' +
                        '<span class="b-title">State:</span>' +
                        '<span class="sh_state" set="' + sh_state + '">' + sh_state + '</span><br>' +
                        '<span class="b-title">Postal Code:</span>' +
                        '<span class="sh_postcode" set="' + sh_postcode + '">' + sh_postcode + '</span><br>' +
                        "</td>";
                    //Other Service----------------
                    var sh_pickup_fee = parseFloat(convert2number($('#sh_pickup_fee').val()));
                    var sh_time_guaranty = parseFloat(convert2number($('#sh_time_guaranty').val()));
                    var sh_remote_area = parseFloat(convert2number($('#sh_remote_area').val()));
                    var sh_insurance = parseFloat(convert2number($('#sh_insurance').val()));
                    var sh_export_doc = parseFloat(convert2number($('#sh_export_doc').val()));
                    var sh_co_from = parseFloat(convert2number($('#sh_co_from').val()));
                    var sh_over_size = parseFloat(convert2number($('#sh_over_size').val()));
                    var sh_over_weight = parseFloat(convert2number($('#sh_over_weight').val()));
                    var sh_packing_paperbox = parseFloat(convert2number($('#sh_packing_paperbox').val()));
                    var sh_wood_packing = parseFloat(convert2number($('#sh_wood_packing').val()));
                    var sh_other_service_charge = parseFloat(convert2number($('#sh_other_service_charge').val()));
                    var total_other_service = 0;
                    row += "<td>";
                    var other_service_list = $('.other_service_ck');
                    other_service_list.each(function (i, e) {
                        if ($(e).is(':checked')) {
                            var service_name = $(e).parent().parent().find('.other_service_name').val();
                            var service_value = $(e).parent().parent().find('.other_service_value').val();
                            total_other_service = total_other_service + parseFloat(convert2number(service_value));
                            row += '<span class="b-title">' + service_name + ': </span><span class="sh_other_service" set="' + service_name + '" price="' + service_value + '">' + service_value + '</span><hr class="line-npd">';
                        }
                    });
                    row += '<div class="sum-bottom total_other_service" set="' + total_other_service + '">Total Amount:' + total_other_service + ' ฿' +
                        '</div>' +
                        '</td>';
                    //Volume(KG)----------------
                    var box = $('#box tbody tr');
                    var totalweight = 0;
                    row += "<td>";
                    box.each(function (i, e) {
                        var weight = valueR[16] != undefined ? valueR[16] : 1;
                        weight = parseFloat(weight);
                        weight = weight / 1000;
                        var width = valueR[17] != undefined ? valueR[17] : 1;
                        var length = valueR[18] != undefined ? valueR[18] : 1;
                        var height = valueR[19] != undefined ? valueR[19] : 1;
                        var sum = width * length * height / 5000;
                        // sum = parseFloat(convert2number((cal == 'up' ? calup(sum) : sum)));
                        totalweight += (weight > sum ? weight : sum);
                        row += '<span class="box b-title" weight="' + weight + '" width="' + width + '" length="' + length + '" height="' + height + '" series="' + (i + 1) + '">' + (i + 1) + ' :</span>' +
                            '<span class="' + (weight > sum ? "badge badge-boxed badge-soft-success" : "") + '">' + weight + '</span><span>' +
                            'KG // ' + width + ' x ' + length + ' x ' + height + ' = </span><span class="' + (weight < sum ? "badge badge-boxed badge-soft-success" : "") + '">' + totalweight + '</span>' +
                            '<hr class="line-npd">';
                    });
                    row += '<div class="sum-bottom">Total Weight : <span class="total_weight" set="' + totalweight + '">' + totalweight.toFixed(2) + '</span> KG.</div>' +
                        "</td>";

                    //CHARGE----------------    
                    var url = '/getprice/' + service.val() + '/' + sh_country_code + '/' + totalweight;
                    const total_regular = await $.ajax({
                        type: "GET",
                        url: url,
                        success: function (result) {
                            return result;
                        },
                        error: function (e) {
                            console.log("ERROR : ", e);
                            $('#loading').hide();
                            alert('พบปัญหาในการขอราคา โปรดตรวจสอบให้แน่ใจว่าเลือกประเทศปลายทาง กรอกขนาดและน้ำหนัก และเลือกบริการเรียบร้อย? หากยังพบปัญหาโปรดแจ้งผู้ดูแลระบบ');
                            return 0;
                        }
                    });
                    var total_charge = total_other_service + parseFloat(total_regular);
                    row += "<td>" +
                        '<span class="b-title">Regular:</span><span class="total_regular_item" set="' + total_regular + '">' + total_regular + '</span><br>' +
                        '<span class="b-title">Other Service:</span><span class="total_other_service_item" set="' + total_other_service + '"> ' + total_other_service + ' ฿</span><br>' +
                        '<div class="sum-bottom total_charge_item" set="' + total_charge + '"> Total Charge: ' + total_charge + ' ฿ </div > ' +
                        "</td>";

                    //Action----------------
                    row += "<td>";
                    row += '<div class="dropdown d-inline-block float-right">';
                    row += '    <a class="nav-link dropdown-toggle arrow-none" id="dLabel8"  data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">';
                    row += '        <i class="fas fa-ellipsis-v font-20 text-muted"></i>';
                    row += "    </a>";
                    row += '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel8">';
                    row += '<a class="dropdown-item" onclick="copy_sh($(this))">Copy</a>';
                    row += '<a class="dropdown-item" onclick="edit_sh($(this))">Edit</a>';
                    row += '<a class="dropdown-item" onclick="delete_sh($(this))">Delete</a>';
                    row += "</div>";
                    row += "</div>";
                    row += "</td>";
                    //end row
                    row = "<tr>" + row + "</tr>"
                    await $("#shipment_detail tbody").append(row);
                    sortTable();
                    binding();
                    $('#save_shipment').show();

                }
            });
        };
        // Tell JS To Start Reading The File.. You could delay this if desired
    }
    reader.readAsBinaryString(oFile);
}

//------------read excel --------------------
function sortTable() {
    var sorted = $('#shipment_detail tbody tr').sort(function (a, b) {
        var a = $(a).find('td:first').text(),
            b = $(b).find('td:first').text();
        return a.localeCompare(b, false, { numeric: true })
    })

    $('#shipment_detail tbody').html(sorted)
}
// --------------------------------------------------

function disconut_code() {
    $("#loading").show();
    var code_discount = $('#code_discount').val();
    var discount = $('#discount').val();
    var customer_id = $('#customer_id').val();
    var customer_name = $('#customer_name').val();
    var manifest_id = $('#manifest_id').val();
    var form_data = new FormData();

    form_data.append("code_discount", code_discount);
    form_data.append("discount", discount);
    form_data.append("customer_id", customer_id);
    form_data.append("customer_name", customer_name);
    console.log(form_data);
    console.log(code_discount);
    console.log(discount);
    console.log(customer_id);
    console.log(customer_name);
    $.ajax({
        type: "POST",
        data: form_data,
        url: '/admin/discount/' + manifest_id + '/' + discount,
        processData: false,
        contentType: false,
        success: function(result) {
            console.log(result);
            window.location.reload();
            $("#loading").hide();
        },
        error: function(e) {
            console.log("ERROR : ", e);
            alert(
                "An error occurred on the server when processing the URL. Please contact the system administrator."
            );
            $("#loading").hide();
        }
    });
}

// ------------------------------------------------

// Get discount
$("#code_discount").keyup(function discount() {
    debugger;
    var index = 0;
    var service_id = "";
    var country_code = "";
    var code = $('#code_discount').val();
    var weight = $('#total_weight').text();
    var total_service = $('.total_amount').val();
    var str = ''
    debugger;
    $('.desc').each(function(i, e) {
        if (index == 0) {
            var sh_country_code = $(e).find(".sh_country_code_0").text();
            country_code += sh_country_code;
            var sh_service = $(e).find(".sh_service").text();
            service_id += sh_service;
            index++;
        } else {
            var sh_country_code = $(e).find(".sh_country_code_0").text();
            country_code += "," + sh_country_code;
            var sh_service = $(e).find(".sh_service").text();
            service_id += "," + sh_service;
            index++;
        }
    });
    $.ajax({
        type: "GET",
        url: '/getdiscount/' + code + '/' + weight + '/' + service_id + '/' + country_code,
        success: function(result) {
            if (result.status == "True") {
                if (result.type == "THB") {
                    $("#discount").val(result.number);
                    str = '<span style="color:green;">รหัสส่วนลดนี้มีอยู่ในระบบ</span>';
                    $("#Error_discount").html(str);
                } else {
                    var disconut = 0;
                    var number = result.number;
                    disconut = (total_service * number) / 100;
                    str = '<span style="color:green;">รหัสส่วนลดนี้มีอยู่ในระบบ</span>';
                    $("#discount").val(disconut);
                    $("#Error_discount").html(str);
                }
            } else {
                str = '<span style="color:red;">' + result.error + '</span>';
                if ($('#code_discount').val() == "" || $('#code_discount').val() == null) {
                    $("#discount").val(result.number);
                    $("#Error_discount").hide();
                } else
                    $("#Error_discount").show();
                $("#discount").val(result.number);
                $("#Error_discount").html(str);
            }
        },
        error: function(e) {
            console.log("ERROR : ", e);
            $('#loading').hide();
        }
    });
});
// end

// ------------------------------------------------------------------------------

// CheckBox
$("#checkAll").click(function(){
    debugger;
    $('.check_all').not(this).prop('checked', this.checked);
    var check_ = $('.check_all').is("input:checked");
    if(!check_){
        $('#delete_shipment').hide();
    }else{ 
        $('#delete_shipment').show();
    }
    });

    function check_select(e){
        var check = $(e).is(":checked");
        var check_all = $('.check_all').is("input:checked");
        if(!check_all){
            $('#delete_shipment').hide();
            $('#checkAll').prop('checked', false);
        }else{ 
            $('#delete_shipment').show();
        }
    }

    $("#delete_shipment").click(function() {
        $(".checkbox_select input:checked").parent().parent().parent().remove();
        binding();
        $('#delete_shipment').hide();
    });

// end CheckBox