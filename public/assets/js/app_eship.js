$(document).ready(function($) {
    $('#sh_postcode').keyup(function() {
        $('#sh_postcode').unmask();
        $('#sh_postcode').addClass('is-invalid');
        $('#sh_postcode').removeClass('is-valid');
        this.value = this.value.toUpperCase();
    });
    $('#sh_country').change(function() {
        $('#sh_show_country').val($('#sh_country').val());
    });
    $('#sh_postcode').change(function() {
        var sh_country = $('#sh_country option:selected');
        var sh_country_code = sh_country.attr('code');
        var sh_zipcode = $('#sh_postcode').val();
        if (sh_zipcode != "") {
            $('#process_postcode').show();
            $.ajax({
                type: "GET",
                url: '/checkformat/' + sh_country_code + '/' + sh_zipcode,
                processData: false,
                contentType: false,
                success: function(result) {
                    console.log(result);
                    if (result.status) {
                        if (result.res.length == 1) {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_city').val(result.res[0].city_name);
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $('#select_city').append(new Option(result.res[0].city_name, result.res[0].city_name));
                        } else {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $(result.res).each(function(i, e) {
                                $('#select_city').append(new Option(e.city_name, e.city_name));
                            })
                            var option = $('#select_city option').filter(function() {
                                return this.value === $("#sh_city").val();
                            }).val();

                            if (!option) {
                                $('#sh_city').val('');
                            }
                        }
                    } else {
                        $('#sh_postcode').addClass('is-invalid');
                    }
                    $('#process_postcode').hide();
                },
                error: function(e) {
                    console.log("ERROR : ", e);
                    $('#sh_postcode').addClass('is-invalid');
                    $('#sh_postcode').removeClass('is-valid');
                    $("#loading").hide();
                }
            });
        } else {
            $('#process_postcode').hide();
            $("#loading").hide();
            $('#sh_postcode').removeClass('is-invalid');
        }
    });

    $('#sh_postcode').keyup(function() {
        $('#process_postcode').show();
        var sh_country = $('#sh_country option:selected');
        var sh_country_code = sh_country.attr('code');
        var sh_zipcode = $('#sh_postcode').val();
        $('#sh_city').val('');
        if (sh_zipcode != "") {
            $.ajax({
                type: "GET",
                url: '/checkformat/' + sh_country_code + '/' + sh_zipcode,
                processData: false,
                contentType: false,
                success: function(result) {
                    console.log(result);
                    $('#process_postcode').hide();
                    if (result.status) {
                        if (result.res.length == 1) {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_city').val(result.res[0].city_name);
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $('#select_city').append(new Option(result.res[0].city_name, result.res[0].city_name));
                        } else {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_city').val('');
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $(result.res).each(function(i, e) {
                                $('#select_city').append(new Option(e.city_name, e.city_name));
                            })
                        }
                    } else {
                        $('#sh_postcode').addClass('is-invalid');
                    }
                },
                error: function(e) {
                    console.log("ERROR : ", e);
                    $('#sh_postcode').addClass('is-invalid');
                    $('#sh_postcode').removeClass('is-valid');
                    $("#loading").hide();
                }
            });
        } else {
            $('#process_postcode').hide();
            $("#loading").hide();
            $('#sh_postcode').removeClass('is-invalid');
        }
    });

    $('#sh_city, #sh_postcode').change(function() {
        var sh_country_code = $('#sh_country option:selected').attr('code');
        var sh_city = $('#sh_city').val();
        $.ajax({
            type: "GET",
            url: '/admin/getsuburb/' + sh_country_code + '/' + sh_city,
            processData: false,
            contentType: false,
            success: function(result) {
                result = JSON.parse(result);
                console.log(result);
                if (result.length > 0) {
                    $('.subrub_area').show();
                    $(result).each(function(i, e) {
                        $('#select_subrub').append(new Option(e.suburb, e.suburb));
                    });
                } else {
                    $('#sh_suburb').val('');
                    $('.subrub_area').hide();
                }

            },
            error: function(e) {
                console.log("ERROR : ", e);
                $('#sh_postcode').addClass('is-invalid');
                $('#sh_postcode').removeClass('is-valid');
                $("#loading").hide();
            }
        });
    });
    $('#sh_country').change(function() {
        var sh_country_code = $('#sh_country option:selected').attr('code');
        $.ajax({
            type: "GET",
            url: '/admin/getcitybycountrycode/' + sh_country_code,
            processData: false,
            contentType: false,
            success: function(result) {
                result = JSON.parse(result);
                //$('#sh_city').val('');
                $('#select_city').html('');
                $(result).each(function(i, e) {
                    $('#select_city').append(new Option(e.city_name, e.city_name));
                });
                var option = $('#select_city option').filter(function() {
                    return this.value === $("#sh_city").val();
                }).val();

                if (!option) {
                    $('#sh_city').val('');
                }
            },
            error: function(e) {
                console.log("ERROR : ", e);
                $('#sh_postcode').addClass('is-invalid');
                $('#sh_postcode').removeClass('is-valid');
                $("#loading").hide();
            }
        });
    });
});
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('input[type=radio][name=time]').change(function() {
        if (this.value == 'พร้อมรับ') {
            $('#delivery_time').prop('disabled', true);
        } else {
            $('#delivery_time').prop('disabled', false);
        }
    });
});

function isNumberKey(evt) {
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function nextstep(step) {
    $('#step_1,#step1').hide();
    $('#step_2,#step2').hide();
    $('#step_3,#step3').hide();
    $('#step_4,#step4').hide();
    $('#step_5,#step5').hide();


    $('#step_' + step).show();
    $('#step' + step).show();

}


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
    if (sParam != 'width' && sParam != 'length' && sParam != 'height') {
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    }
};

var sh_country = getUrlParameter('sh_country') == null ? "" : getUrlParameter('sh_country');
var sh_weight = getUrlParameter('sh_weight') == null ? "" : getUrlParameter('sh_weight');

var x = sh_country.split('+');
var data = "";
for (var i = 0; i < x.length; i++) {
    if (i == (x.length - 1)) {
        data += x[i];
    } else {
        data += x[i] + " ";
    }
}
var volumnWeight = getUrlParameter('volumnWeight') == null ? "" : getUrlParameter('volumnWeight');
var service_id = getUrlParameter('service') == null ? "" : getUrlParameter('service');
if (sh_country != "" && sh_weight != "") {
    document.getElementById("sh_country").value = data;
    $("#sh_weight").val(sh_weight);
    $("#volumnWeight").val(volumnWeight);
    get_service(service_id);
    nextstep(2);
}


$("#form_step_1").submit(function(e) {

    var form = $(this)[0];
    var service = $("input[name='service']:checked");
    if (service.length > 0) {
        if (form.checkValidity() === false) {
            e.preventDefault();
            e.stopPropagation();
            form.classList.add('was-validated');
        }else{
            e.preventDefault();
            nextstep(2);
        }
    } else {
        alert('โปรดเลือกบริการและราคาที่ต้องการ');
        e.preventDefault();
    }
});

$("#form_step_2").submit(function(e) {
    var form = $(this)[0];
    if (form.checkValidity() === false) {
        e.preventDefault();
        e.stopPropagation();
    }else{
        e.preventDefault();
        nextstep(3);
    }
    form.classList.add('was-validated');
});
$("#form_step_3").submit(function(e) {
    var form = $(this)[0];
    if (form.checkValidity() === false) {
        e.preventDefault();
        e.stopPropagation();
    }else{
        e.preventDefault();
        nextstep(4);
    }
    form.classList.add('was-validated');
});

$("#form_step_4").submit(function(e) { //data binding
    e.preventDefault();
    var form = $(this)[0];
    var service = $("input[name='service']:checked");
    if (service.length > 0) {

        var addr_name = $('#addr_name').val() != "" ? $('#addr_name').val() : "-";
        var addr_address = $('#addr_address').val() != "" ? $('#addr_address').val() : "-";
        var addr_phone = $('#addr_phone').val() != "" ? $('#addr_phone').val() : "-";
        var sh_recipient = $('#sh_recipient').val() != "" ? $('#sh_recipient').val() : "-";
        var sh_address1 = $('#sh_address1').val() != "" ? $('#sh_address1').val() : "-";
        var sh_country = $('#sh_country option:selected').val() != "" ? $('#sh_country option:selected').val() : "-";
        var sh_postcode = $('#sh_postcode').val() != "" ? $('#sh_postcode').val() : "-";
        var sh_phone = $('#sh_phone').val() != "" ? $('#sh_phone').val() : $('#sh_phone').val("-");
        var sh_email = $('#sh_email').val() != "" ? $('#sh_email').val() : "";
        var sh_weight = $('#sh_weight').val() != "" ? $('#sh_weight').val() : "-";
        var sh_service = $(service).parent().find(".cut-text").text();
        var sh_declared = $('#sh_declared').val() != "" ? $('#sh_declared').val() : "-";
        var sh_quantity = $('#sh_quantity').val() != "" ? $('#sh_quantity').val() : "-";
        var sh_description = $('#sh_description').val() != "" ? $('#sh_description').val() : "-";

        $("#sr_name").text(addr_name);
        $("#sr_addr_adderss").text(addr_address);
        $("#sr_phone").text(addr_phone);
        $("#sr_recipient").text(sh_recipient);
        $("#sr_sh_address").text(sh_address1);
        $("#sr_country").text(sh_country);
        $("#sr_postcode").text(sh_postcode);
        $("#sr_sent_phone").text(sh_phone);
        $("#sr_email").text(sh_email);
        $("#sr_weight").text(sh_weight);
        $("#t_country").text(sh_country);
        $("#sr_service").text(sh_service);
        $("#total_service").text(numberWithCommas(service.attr('s_price')));
        $("#price_service").text(numberWithCommas(service.attr('s_price')));
        $("#totle_pirce").text(numberWithCommas(service.attr('s_price')));
        $("#sr_product_baht").text(sh_declared);
        $("#sr_product_quality").text(sh_quantity);
        $("#sr_product_detail").text(sh_description);
        $("#bindingdata").html(binding());
        temp_summary();
        var edit_mode = $('#edit_number').val();
        var i = edit_mode != '' ? edit_mode : $('.sumary_data').length;
        if (form.checkValidity() === false) {
            e.preventDefault();
            e.stopPropagation();
        }else{
            $("#edit_step").show();
            nextstep(5);
        }
        form.classList.add('was-validated');
    } else {
        alert('Error');
    }
    var shipment_ = $('#temp_sumary').html();
    $.ajax({
        type: "POST",
        url: '/app/save_temp',
        data: { temp: shipment_ },
        success: function(result) {
            console.log('Save temp success.');
        },
        error: function(e) {
            console.log("ERROR : ", e);
        }
    });
});

// next step 1
$('#edit_step').click(function(e){
    e.stopPropagation();
    $('#edit_step').hide();
    nextstep(1);
});

//  Check duscount
$("#check_discount").click(function discount() {
    var total = 0;
    var total_service = $(".temp_total_regular_item").val();
    if ($('#discount_code').val() != "") {
        var code = $('#discount_code').val();
        var weight = $('.temp_volumnWeight').val();
        var service_id = $('.temp_sh_type_of_service').val();
        var country_code = $('.temp_sh_country_code').val();

        $.ajax({
            type: "GET",
            url: '/getdiscount/' + code + '/' + weight + '/' + service_id + '/' + country_code,
            success: function(result) {
                if (result.status == 'True') {
                    if (result.type == 'THB') {
                        total = total_service - result.number;
                        if (total < 0) {
                            total = 0;
                            $('#totle_pirce').text(total);
                        } else {
                            $('#totle_pirce').text(total);
                        }
                        $('#discount_price').text(result.number);
                        $('.discount_price').val(result.number);
                        $('#check_code').val(result.status);
                        alert('ส่วนลดราคา ' + result.number + ' บาท');
                    } else {
                        disconut_percent = (total_service * result.number) / 100;
                        total = total_service - disconut_percent;
                        if (total < 0) {
                            total = 0;
                            $('#totle_pirce').text(total);
                        } else {
                            $('#totle_pirce').text(total);
                        }
                        $('#discount_price').text(disconut_percent);
                        $('.discount_price').val(result.number);
                        $('#check_code').val(result.status);
                        alert('ส่วนลดราคา ' + disconut_percent + ' บาท');
                    }
                } else {
                    alert(result.error);
                    $("#discount_code").val("");
                    $('#loading').hide();
                }
            },
            error: function(e) {
                console.log("ERROR : ", e);
                $('#loading').hide();
            }
        });
    } else {
        $('#totle_pirce').text(total_service);
        $('#discount_price').text(total);
        $('.discount_price').val(total);
        alert('กรุณากรอก Code ส่วนลดก่อนครับ');
    }
});
// end Check


$("#form_step_5").submit(function(e) {

    if ($('#discount_code').val() == "") {
        e.preventDefault();
        $("#loading").show();
        $.ajax({
            type: "POST",
            data: $("#form_step_2,#form_step_4,#form_step_5").serialize(),
            url: "/app/booking",
            success: function(res) {
                $("#loading").hide();
                $("#step5").hide();
                gettracking(res);
                $("#finished").modal("show");

            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    } else if ($('#check_code').val() == "True") {
        e.preventDefault();
        $("#loading").show();
        $.ajax({
            type: "POST",
            data: $("#form_step_2,#form_step_4,#form_step_5").serialize(),
            url: "/app/booking",
            success: function(res) {
                $("#loading").hide();
                $("#step5").hide();
                gettracking(res);
                $("#finished").modal("show");

            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    } else {
        alert("กรุณากดตรวจสอบ Code ส่วนลดก่อนครับ");
        return false;
    }
});



function gettracking(id) {

    $.ajax({
        type: "GET",
        url: "/app/gettracking/" + id,
        success: function(res) {
            if ($('#select_type4').is(':checked')) {
                $("#printer").html('<a href="/dropoff/' + id + '" target="_blank" class="button_style">' +
                    '<button type="button"' +
                    'class="btn btn-danger btn-square waves-effect waves-light eship_space"' +
                    'style="width: 100%;"><span style="font-size: 16px;">พิมพ์เอกสาร</span></button></a>');
                $("#print_label").html('<a href="/dropoff/' + id + '" target="_blank">' +
                    '<button type="button" class="btn" style="width: 72px;color: #ffffff ;background-color: #f22727;font-weight: bold;">พิมพ์</button></a>');
            } else if ($('#select_type3').is(':checked')) {
                $("#printer").html('<a href="/address_customer/' + id + '" target="_blank" class="button_style">' +
                    '<button type="button"' +
                    'class="btn btn-danger btn-square waves-effect waves-light eship_space"' +
                    'style="width: 100%;"><span style="font-size: 16px;">พิมพ์เอกสาร</span></button></a>');
                $("#print_label").html('<a href="/address_customer/' + id + '" target="_blank">' +
                    '<button type="button" class="btn" style="width: 72px;color: #ffffff ;background-color: #f22727;font-weight: bold;">พิมพ์</button></a>');
            } else {
                $("#printer").html('<a href="/smelabel/' + id + '" target="_blank" class="button_style">' +
                    '<button type="button"' +
                    'class="btn btn-danger btn-square waves-effect waves-light eship_space"' +
                    'style="width: 100%;"><span style="font-size: 16px;">พิมพ์เอกสาร</span></button></a>');
                $("#print_label").html('<a href="/smelabel/' + id + '" target="_blank">' +
                    '<button type="button" class="btn" style="width: 72px;color: #ffffff ;background-color: #f22727;font-weight: bold;">พิมพ์</button></a>');
            }
            $("#tracking").html('<div style="text-align: center; color: red; margin-bottom: 10px;font-size: 19px;">' +
                '<span>พิมพ์เอกสารการส่ง ติดบนกล่องหรือเขียนรหัส "' + res.substr(0,2) + ' '+ res.substr(2,2) +' '+ res.substr(4,2) +'" บนกล่อง</span></div>');
            $("#modal_tracking").html(
                '<div class="number_tracking">เลขพัสดุของท่าน</div>'+
                '<div class="gettracking">' + res.substr(0,2) + ' '+ res.substr(2,2) +' '+ res.substr(4,2) +'</div>'+
                '<div style="text-align: center; color: red; margin-top: 16px; margin-bottom: 10px; font-size: 92%;">' +
                '<span>กรุณาพิมพ์เอกสารการส่ง <br/>และนำไปติดลงบนกล่องพัสดุ <br/>หรือเขียนเลขพัสดุลงบนกล่อง</span></div>');
        },
        error: function(e) {
            console.log("ERROR : ", e);
            alert(
                "An error occurred on the server when processing the URL. Please contact the system administrator."
            );
        }
    });
}

// Add Box
function addbox() {
    var box_number = $('.box').length + 1;
    var html = '<div class="row box"><div class="col-12" style="padding-bottom:10px;">ขนาดพัสดุกล่องที่ <span class="box_number">' + box_number +
        '</span><span class="box_del_add badge badge-danger" onclick="box_del($(this))">X</span><br></div>' +
        '<div class="col-4"><input type="number" class="form-control width" style="text-align:center" placeholder="กว้าง (CM)"></div>' +
        '<div class="col-4"><input type="number" class="form-control length" style="text-align:center" placeholder="ยาว (CM)"></div>' +
        '<div class="col-4"><input type="number" class="form-control height" style="text-align:center" placeholder="สูง (CM)"></div></div>';
    $('#box_area').append(html);
    $('#box_total').val(box_number);
}

function box_del(obj) {
    $(obj).parent().parent().remove();
    $('.box').each(function(i, e) {
        $(e).find(".box_number").html(i + 1);
    });
    $('#box_total').val($('.box').length);
    cal_box();

}

function cal_box() {
    // console.log('cal_box');
    var volumnWeight = 0;
    var weight = parseFloat(convert2number($("#sh_weight").val()));

    $('.box').each(function(i, e) {
        var width = parseFloat(convert2number($(e).find(".width").val()));
        var length = parseFloat(convert2number($(e).find(".length").val()));
        var height = parseFloat(convert2number($(e).find(".height").val()));
        var sum = width * length * height / 5000;
        volumnWeight = volumnWeight + sum;
    });
    if (volumnWeight < weight) {
        volumnWeight = weight;
    }
    $('#volumnWeight').val(volumnWeight);
    // $('#volumnWeight').change();
    get_service()
}

// Add Description
function adddesc() {
    var desc_number = $('.desc').length;
    var html = '<div class="desc">' +
        '<div style="text-align: right;padding-right: 20px;"> <span class="box_del badge badge-danger" onclick = "desc_del($(this))">X</span></div>' +
        '<div class="row">' +
        '<div class="col-md-6" style="padding-left: 27px;">' +
        '<div class="col-md-12" style="padding-top: 20px;">คำอธิบายสินค้า* ' + (desc_number + 1) + '</div>' +
        '<div style="color: red;font-size: 13px;padding-top: 12px;">'+
        'โปรดอธิบายโดยละเอียด สินค้าคืออะไร ใช้เพื่ออะไร ทำจากอะไรเช่น Men black polo shirt Cotton 100%. หรือ Document for Visa.</div>'+
        '<div class="col-md-12"><textarea class="d_description form-control sh-input textarea" rows="3"' +
        'style="height: 82px;" placeholder="" required onchange="cal_desc()"></textarea>'+
        '<div class="invalid-feedback">กรุณากรอกจำนวน(ชิ้น)*</div></div>'+
        '</div>' +
        '<div class="col-md-6" style="padding-left: 27px;">' +
        '<div class="col-md-12" style="padding-top: 20px;">จำนวน(ชิ้น)*</div>' +
        '<div class="col-md-12"><input type="text" class="d_number form-control sh-input" required onchange="cal_desc()" onkeypress="return isNumberKey(event)">' +
        '<div class="invalid-feedback">กรุณากรอกจำนวน(ชิ้น)*</div></div>'+
        '<br/><div class="col-md-12">มูลค่ารวม(บาท)*</div>' +
        '<div class="col-md-12"><input type="text" class="d_amount form-control sh-input" required onchange="cal_desc()" onkeypress="return isNumberKey(event)">' +
        '<div class="invalid-feedback">กรุณากรอกมูลค่ารวม(บาท)*</div></div>'+
        '</div></div>' +
        '</div>';
    $('#desc_area').append(html);
}

function desc_del(obj) {
    $(obj).parent().parent().remove();
    $('.desc').each(function(i, e) {
        $(e).find(".desc_number").html(i + 1);
    });
    cal_desc();
}

function cal_desc() {
    // console.log('cal_box');

    var description = "";
    var amount = 0;
    var number = 0;

    $('.desc').each(function(i, e) {
        var d_description = $(e).find(".d_description").val();
        var d_amount = parseFloat(convert2number($(e).find(".d_amount").val()));
        var d_number = parseFloat(convert2number($(e).find(".d_number").val()));
        description += d_description + ",";
        amount += d_amount;
        number += d_number;
    });

    $('#sh_description').val(description);
    $('#sh_quantity').val(number);
    $('#sh_declared').val(amount);
    $('#sh_declared').change();

    //$('.volumnWeight').html(volumnWeight);
}

function convert2number(value) {
    var result = value;
    if (value === undefined || isNaN(value) || value === "") {
        result = 0;
    }
    return result;
}

function get_service(service_id) {
    var x_country = $("#sh_country").val();
    $("#s_country").val(x_country);
    $('#service_select').html('');
    var sh_country = $('#sh_country option:selected');
    var totalweight = $('#volumnWeight').val();
    var url = '/getprice/' + sh_country.attr('code') + '/' + totalweight;
    $('#sercive_price').show();
    if ($('#volumnWeight').val() != '' && $('#volumnWeight').val() != 0) {
        $("#loading").show();

        $.ajax({
            type: "GET",
            url: url,
            success: function(result) {
                var data = $.parseJSON(result);
                // console.log(data);
                var html = '';
                var ismatch = false;
                $.each(data, function(i, e) {
                    e.logo = e.logo != null ? e.logo : 'sme.jpg';
                    if (e.price.result != undefined && e.price.result != "" && service_id == e.id) {
                        html += '<div class="col-md-12" onclick="show_service_modal()"  data-toggle="modal" data-target="#select_one_service">' +
                            '<input type="radio" name="service" class="radio_service_item" id="s_' +
                            service_id + '" s_id="' + service_id + '" s_price="' + e.price.result +
                            '" s_logo="' + e.logo + '" >' +
                            '<label class="row service_card" for="s_' + service_id + '">' +
                            '<div class="col-4 col-md-3" style="float: left;padding: 10px;text-align:center;">' +
                            '<img src="/uploads/service/' + e.logo + '" alt="user" width="70px" height="70px"></div>' +
                            '<div class="col-8 col-md-9 font_service">' +
                            '<div><span class="cut-text">' + e.name + '</span></div>( ' + e.day.day +' วันทำการ )' +
                            '<div style="display: flex;justify-content:space-between;margin-bottom: 9px;"><span>' + numberWithCommas(e.price.result) +' ฿</span><span class="select_sevice">เลือก</span></div></div>' + '</label></div>';
                        ismatch = true;
                    } else
                    if (e.price.result != undefined && e.price.result != "") {
                        html += '<div class="col-md-12" onclick="show_service_modal()"  data-toggle="modal" data-target="#select_one_service">' +
                            '<input type="radio" name="service" class="radio_service_item" id="s_' +
                            e.id + '" s_id="' + e.id + '" s_price="' + e.price
                            .result + '" s_logo="' + e.logo + '">' +
                            '<label class="row service_card" for="s_' + e.id + '">' +
                            '<div class="col-4 col-md-3" style="float: left;padding: 10px;text-align:center;">' +
                            '<img src="/uploads/service/' + e.logo +
                            '" alt="user" width="70px" height="70px"></div>' +
                            '<div class="col-8 col-md-9 font_service">' +
                            '<div> <span class="cut-text">' + e.name + '</span></div>( ' + e.day.day + ' วันทำการ )' +
                            '<div style="display: flex;justify-content:space-between;margin-bottom: 9px;"><span>' + numberWithCommas(e.price.result) +' ฿</span><span class="select_sevice">เลือก</span></div></div>' + '</label></div>';
                    }
                });
                if (!ismatch) {
                    nextstep(1);
                }
                $('#service_select').html(html);
                var select_service = $('#service_number').val();
                if (select_service != null) {
                    $("#s_" + select_service).prop('checked', true);
                }
                $("#loading").hide();
            },
            error: function(e) {
                console.log("ERROR : ", e);
            }
        });
        $('#service_select').show();
        $('#service_wait_data').hide();
    } else {
        $('#service_select').hide();
        $('#service_wait_data').show();
        $('#sercive_price').hide();
        $("#loading").hide();
    }
}

function show_service_modal(){
    var show_service = $("input[name='service']:checked");
    var sh_service = $(show_service).parent().find(".cut-text").text();
    $("#name_service").text(sh_service);
    };

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

$('#sh_country').on('change', function(e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
});

function binding() {
    var i = 0;
    var service = $("input[name='service']:checked");
    var volumnWeight = $('#volumnWeight').val() == "" ? "-" : $('#volumnWeight').val();
    var sh_description = $('#sh_description').val() == "" ? "-" : $('#sh_description').val();
    var sh_quantity = $('#sh_quantity').val() == "" ? "-" : $('#sh_quantity').val();
    var sh_declared = $('#sh_declared').val() == "" ? "-" : $('#sh_declared').val();
    var sh_recipient = $('#sh_recipient').val() == "" ? "-" : $('#sh_recipient').val();
    var sh_company = $('#sh_company').val() == "" ? "-" : $('#sh_company').val();
    var sh_email = $('#sh_email').val() == "" ? "" : $('#sh_email').val();
    var sh_address1 = $('#sh_address1').val() == "" ? "-" : $('#sh_address1').val();
    var sh_address2 = $('#sh_address2').val() == "" ? "-" : $('#sh_address2').val();
    var sh_address3 = $('#sh_address3').val() == "" ? "-" : $('#sh_address3').val();
    var sh_phone = $('#sh_phone').val() == "" ? "" : $('#sh_phone').val();
    var sh_country = $('#sh_country option:selected') == "" ? "-" : $('#sh_country option:selected');
    var sh_currency = $('#sh_currency option:selected') == "" ? "-" : $('#sh_currency option:selected');
    var sh_city = $('#sh_city').val() == "" ? "-" : $('#sh_city').val();
    var sh_state = $('#sh_state').val() == "" ? "-" : $('#sh_state').val();
    var sh_postcode = $('#sh_postcode').val() == "" ? "-" : $('#sh_postcode').val();
    var sh_suburb = $('#sh_suburb').val() == "" ? "-" : $('#sh_suburb').val();
    var sh_Insurance = ($('#Insurance').is(':checked') ? 'yes' : 'no');
    var sh_ExportDoc = ($('#ExportDoc').is(':checked') ? 'yes' : 'no');
    var sh_Pack = ($('#Pack').is(':checked') ? 'yes' : 'no');
    var sh_Note = $('#Note').val();
    data = '<input type="hidden" name="sh_type_of_service[' + i + ']" value="' + service.attr('s_id') + '">' +
        //'<input type="hidden" name="sh_currency[' + i + ']" value="USD">' +
        '<input type="hidden" name="sh_declared_value[' + i + ']" value="' + sh_declared + '">' +
        '<input type="hidden" name="sh_description[' + i + ']" value="' + sh_description + '">' +
        '<input type="hidden" name="sh_content_quantity[' + i + ']" value="' + sh_quantity + '">' +
        '<input type="hidden" name="sh_packge_type[' + i + ']" value="PA">' +
        '<input type="hidden" name="sh_recipient[' + i + ']" value="' + sh_recipient + '">' +
        '<input type="hidden" name="sh_company[' + i + ']" value="-">' +
        '<input type="hidden" name="sh_email[' + i + ']" value="' + sh_email + ' ">' +
        '<input type="hidden" name="sh_address1[' + i + ']" value="' + sh_address1 + '">' +
        '<input type="hidden" name="sh_address2[' + i + ']" value="-">' +
        '<input type="hidden" name="sh_address3[' + i + ']" value="-">' +
        '<input type="hidden" name="sh_phone[' + i + ']" value="' + sh_phone + '">' +
        '<input type="hidden" name="sh_country[' + i + ']" value="' + sh_country.val() + '">' +
        '<input type="hidden" name="sh_currency[' + i + ']" value="THB">' +
        '<input type="hidden" name="sh_country_code[' + i + ']" value="' + sh_country.attr('code') + '">' +
        '<input type="hidden" name="sh_city[' + i + ']" value="' + sh_city + '">' +
        '<input type="hidden" name="sh_suburb[' + i + ']" value="-">' +
        '<input type="hidden" name="sh_state[' + i + ']" value="-">' +
        '<input type="hidden" name="sh_postcode[' + i + ']" value="' + sh_postcode + '">' +
        '<input type="hidden" name="sh_Insurance[' + i + ']" value="' + sh_Insurance + '">' +
        '<input type="hidden" name="sh_ExportDoc[' + i + ']" value="' + sh_ExportDoc + '">' +
        '<input type="hidden" name="sh_Pack[' + i + ']" value="' + sh_Pack + '">' +
        '<input type="hidden" name="sh_Note[' + i + ']" value="-">' +

        '<input type="hidden" name="total_regular_item[' + i + ']" value="' + service.attr('s_price') + '">';


    var box_total = $('#box_total').val();
    var weight = $('#sh_weight').val() / box_total;
    var box_auto_create = box_total - $('.box').length;
    $('.box').each(function(a, e) {
        var width = parseFloat(convert2number($(e).find(".width").val()));
        var length = parseFloat(convert2number($(e).find(".length").val()));
        var height = parseFloat(convert2number($(e).find(".height").val()));
        data += '<input type="hidden" name="series[' + i + '][' + a + ']" value="' + (a + 1) + '">' +
            '<input type="hidden" name="weight[' + i + '][' + a + ']" value="' + weight + '">' +
            '<input type="hidden" name="width[' + i + '][' + a + ']" value="' + width + '">' +
            '<input type="hidden" name="length[' + i + '][' + a + ']" value="' + length + '">' +
            '<input type="hidden" name="height[' + i + '][' + a + ']" value="' + height + '">';
    });
    if (box_auto_create > 0) {
        var a = $('.box').length;
        var z;
        for (z = 0; z < box_auto_create; z++) {
            data += '<input type="hidden" name="series[' + i + '][' + (a + z) + ']" value="' + ((a + z) + 1) + '">' +
                '<input type="hidden" name="weight[' + i + '][' + (a + z) + ']" value="' + weight + '">' +
                '<input type="hidden" name="width[' + i + '][' + (a + z) + ']" value="0">' +
                '<input type="hidden" name="length[' + i + '][' + (a + z) + ']" value="0">' +
                '<input type="hidden" name="height[' + i + '][' + (a + z) + ']" value="0">';
        }
    }
    $('.desc').each(function(a, e) {
        var d_description = $(e).find(".d_description").val();
        var d_amount = parseFloat(convert2number($(e).find(".d_amount").val()));
        var d_number = parseFloat(convert2number($(e).find(".d_number").val()));
        data += '<input type="hidden" name="d_description[' + i + '][' + a + ']" value="' + d_description + '">' +
            '<input type="hidden" name="d_amount[' + i + '][' + a + ']" value="' + d_amount + '">' +
            '<input type="hidden" name="d_number[' + i + '][' + a + ']" value="' + d_number + '">';
    });
    data += '<input type="hidden" name="total_weight_item[' + i + ']" value="' + volumnWeight + '">';
    return data;
}

function addshipping() {
    $('#form_step_2').trigger("reset");
    $('#form_step_3').trigger("reset");
    $('#service_wait_data').show();
    $('#service_select').hide();
    $('#sh_suburb').val('');
    $('#sh_suburb').hide();
    $('.box_del').each(function(i, e) {
        $(e).click();
    });
    $('#Insurance').prop('checked', false);
    $('#ExportDoc').prop('checked', false);
    $('#Pack').prop('checked', false);
    $('#customCheck03').prop('checked', false);
    $('#Note').val('');
    $('#Note_area').hide();
    $('#edit_number').val('');
    $('#service_number').val('');
    nextstep(2);
}


$.ajax({
    type: "GET",
    url: '/admin/getaddress_thailand',
    success: function(result) {
        var province = JSON.parse(result);
        $(province).each(function(index, i) {
            $('#province').append('<option value="' + i.PROVINCE_ID + '">' + i.PROVINCE_NAME + '</option>');
            $('#a_province').append('<option value="' + i.PROVINCE_ID + '">' + i.PROVINCE_NAME + '</option>');
        });
    },
    error: function(e) {
        console.log("ERROR : ", e);
        $('#loading').hide();
    }
});


$('#province').change(function(e) {
    var provinceid = $('#province').val();
    $('#amphur').html('');
    $.ajax({
        type: "GET",
        url: '/admin/getaddress_thailand/' + provinceid,
        success: function(result) {
            var amphur = JSON.parse(result);
            $(amphur).each(function(index, i) {
                $('#amphur').append('<option value="' + i.id + '" postcode="' + i.postcode + '">' + i.name + '</option>');
            });
        },
        error: function(e) {
            console.log("ERROR : ", e);
            $('#loading').hide();
        }
    });
});

$('#amphur').change(function(e) {
    var provinceid = $('#province').val();
    var amphurid = $('#amphur').val();
    $('#postcode').val($('#amphur option:selected').attr('postcode'));
    $('#tumbon').html('');
    $.ajax({
        type: "GET",
        url: '/admin/getaddress_thailand/' + provinceid + '/' + amphurid,
        success: function(result) {
            var tumbon = JSON.parse(result);
            $(tumbon).each(function(index, i) {
                $('#tumbon').append('<option value="' + i.id + '">' + i.name + '</option>');
            });
        },
        error: function(e) {
            console.log("ERROR : ", e);
            $('#loading').hide();
        }
    });
});

$('#a_province').change(function(e) {
    var provinceid = $('#a_province').val();
    $('#a_amphur').html('');
    $.ajax({
        type: "GET",
        url: '/admin/getaddress_thailand/' + provinceid,
        success: function(result) {
            var amphur = JSON.parse(result);
            $(amphur).each(function(index, i) {
                $('#a_amphur').append('<option value="' + i.id + '" postcode="' + i.postcode + '">' + i.name + '</option>');
            });
        },
        error: function(e) {
            console.log("ERROR : ", e);
            $('#loading').hide();
        }
    });
});

function set_sender(id, name, address, tumbon, amphur, province, postcode, phone, email) {
    $('.modal').modal('hide');
    $('#addr_name').val(name);
    $('#addr_phone').val(phone);
    $('#email').val(email);
    $('#addr_address').val(address);
    $('#province').val(province);
    $('#amphur').val(amphur);
    $('#tumbon').val(tumbon);
    $('#postcode').val(postcode);
    $('#address_id').val(id);
}

function set_recipient(id) {
    $('.modal').modal('hide');
    var name = $('#re_' + id).attr('name');
    var company = $('#re_' + id).attr('company');
    var email = $('#re_' + id).attr('email');
    var address1 = $('#re_' + id).attr('address1');
    var address2 = $('#re_' + id).attr('address2');
    var address3 = $('#re_' + id).attr('address3');
    var phone = $('#re_' + id).attr('phone');
    var city = $('#re_' + id).attr('city');
    var zipcode = $('#re_' + id).attr('zipcode');
    var state = $('#re_' + id).attr('state');
    var suburb = $('#re_' + id).attr('suburb');
    var country = $('#re_' + id).attr('country');
    var country_code = $('#re_' + id).attr('country_code');
    if (country != null) {
        $('#sh_country').val(country).change();
    }
    $('#sh_recipient').val(name);
    $('#sh_company').val(company);
    $('#sh_email').val(email);
    $('#sh_address1').val(address1);
    $('#sh_address2').val(address2);
    $('#sh_address3').val(address3);
    $('#sh_phone').val(phone);
    $('#sh_city').val(city);
    $('#sh_postcode').val(zipcode);
    $('#sh_state').val(state);
    $('#sh_suburb').val(suburb);
    $('#sh_city').val(city);
}

$('#recipient_list').click(function() {
    $('#find_recipient').val('');
    $("#loading").show();
    var country = $('#sh_country option:selected').attr('code');
    // var country = 'all';
    $('#recipient_area').html('');
    $.ajax({
        type: "GET",
        url: '/app/recipient/' + country,
        success: function(result) {
            var data = JSON.parse(result);
            $(data).each(function(index, i) {
                $('#recipient_area').append('<tr onclick="set_recipient(' + (index + 1) + ')" style="cursor:pointer">' +
                    '<td id="re_' + (index + 1) + '" name="' + (i.recipient != null ? i.recipient : '') +
                    '" company="' + (i.company != null ? i.company : '') +
                    '" email="' + (i.email != null ? i.email : '') +
                    '" address1="' + (i.address != null ? i.address : '') +
                    // '" address2="' + (i.address2 != null ? i.address2 : '') +
                    // '" address3="' + (i.address3 != null ? i.address3 : '') +
                    '" phone="' + (i.phone != null ? i.phone : '') +
                    // '" city="' + (i.city != null ? i.city : '') +
                    // '" suburb="' + (i.suburb != null ? i.suburb : '') +
                    // '" zipcode="' + (i.zipcode != null ? i.zipcode : '') +
                    // '" state="' + (i.state != null ? i.state : '') +
                    // '" country_code="' + (i.country_code != null ? i.country_code : '') +
                    // '" country="' + (i.country != null ? i.country : '') +
                    '">' +
                    i.recipient + '</td>' +
                    '<td>' + i.address + '</td > ' +
                    '<td>' + i.phone + '</td>' +
                    '</tr>');
            });
        },
        error: function(e) {
            console.log("ERROR : ", e);
        }
    });
    $('#loading').hide();
});

$('#find_recipient').keyup(function() {
    $("#loading").show();
    var country = $('#sh_country option:selected').attr('code');
    // var country = 'all';
    var keyword = $('#find_recipient').val();
    $.ajax({
        type: "GET",
        url: '/app/recipient/' + country + '/' + keyword,
        success: function(result) {
            var data = JSON.parse(result);
            $('#recipient_area').html('');
            $(data).each(function(index, i) {
                $('#recipient_area').append('<tr onclick="set_recipient(' + (index + 1) + ')" style="cursor:pointer">' +
                    '<th id="re_' + (index + 1) + '" name="' + (i.recipient != null ? i.recipient : '') + '" company="' + (i.company != null ? i.company : '') + '" email="' + (i.email != null ? i.email : '') + '" address1="' + (i.address != null ? i.address : '') + '" address2="' + (i.address2 != null ? i.address2 : '') + '" address3="' + (i.address3 != null ? i.address3 : '') + '" phone="' + (i.phone != null ? i.phone : '') + '" city="' + (i.city != null ? i.city : '') + '" zipcode="' + (i.zipcode != null ? i.zipcode : '') + '" state="' + (i.state != null ? i.state : '') + '">' +
                    '' + (index + 1) + '</th>' +
                    '<td>' + i.recipient + '</td>' +
                    '<td>' + i.country + '</td > ' +
                    '<td>' + i.phone + '</td>' +
                    '</tr>');
            });
        },
        error: function(e) {
            console.log("ERROR : ", e);
        }
    });
    $('#loading').hide();
});

$('#customCheck03').change(function() {
    if ($('#customCheck03').is(':checked')) {
        $('#Note_area').show();
    } else {
        $('#Note_area').hide();
    }
});

$('#img_type1').click(function() {
    $('#img_type1').html('<img class="pointer" src="/assets/images/red1.png">');
    $('#img_type2').html('<img class="pointer" src="/assets/images/red2.png">');
    $('#img_type3').html('<img class="pointer" src="/assets/images/red2.png">');
    $('#img_type4').html('<img class="pointer" src="/assets/images/red2.png">');
});
$('#img_type2').click(function() {
    $('#img_type1').html('<img class="pointer" src="/assets/images/red2.png">');
    $('#img_type2').html('<img class="pointer" src="/assets/images/red1.png">');
    $('#img_type3').html('<img class="pointer" src="/assets/images/red2.png">');
    $('#img_type4').html('<img class="pointer" src="/assets/images/red2.png">');
});
$('#img_type3').click(function() {
    $('#img_type1').html('<img class="pointer" src="/assets/images/red2.png">');
    $('#img_type2').html('<img class="pointer" src="/assets/images/red2.png">');
    $('#img_type3').html('<img class="pointer" src="/assets/images/red1.png">');
    $('#img_type4').html('<img class="pointer" src="/assets/images/red2.png">');
});
$('#img_type4').click(function() {
    $('#img_type1').html('<img class="pointer" src="/assets/images/red2.png">');
    $('#img_type2').html('<img class="pointer" src="/assets/images/red2.png">');
    $('#img_type3').html('<img class="pointer" src="/assets/images/red2.png">');
    $('#img_type4').html('<img class="pointer" src="/assets/images/red1.png">');
});

function temp_summary() {
    i = 0;
    var service = $("input[name='service']:checked");
    var addr_name = $('#addr_name').val() != "" ? $('#addr_name').val() : "-";
    var addr_address = $('#addr_address').val() != "" ? $('#addr_address').val() : "-";
    var addr_phone = $('#addr_phone').val() != "" ? $('#addr_phone').val() : "-";
    var addr_email = $('#email').val() != "" ? $('#email').val() : "-";
    var addr_province = $('#province').val() != "" ? $('#province').val() : "-";
    var addr_postcode = $('#postcode').val() != "" ? $('#postcode').val() : "-";
    var sh_weight = $('#sh_weight').val() != "" ? $('#sh_weight').val() : "-";
    var sh_service = $(service).parent().find(".cut-text").text();
    var volumnWeight = $('#volumnWeight').val() == "" ? "" : $('#volumnWeight').val();
    var sh_quantity = $('#sh_quantity').val() == "" ? "" : $('#sh_quantity').val();
    var sh_declared = $('#sh_declared').val() == "" ? "" : $('#sh_declared').val();
    var sh_description = $('#sh_description').val() == "" ? "-" : $('#sh_description').val();
    var sh_recipient = $('#sh_recipient').val() == "" ? "" : $('#sh_recipient').val();
    var sh_email = $('#sh_email').val() == "" ? "" : $('#sh_email').val();
    var sh_address1 = $('#sh_address1').val() == "" ? "" : $('#sh_address1').val();
    var sh_phone = $('#sh_phone').val() == "" ? "" : $('#sh_phone').val();
    var sh_country = $('#sh_country option:selected') == "" ? "" : $('#sh_country option:selected');
    var sh_city = $('#sh_city').val() == "" ? " " : $('#sh_city').val();
    var sh_postcode = $('#sh_postcode').val() == "" ? " " : $('#sh_postcode').val();
    var sh_Insurance = ($('#Insurance').is(':checked') ? 'yes' : 'no');
    var sh_ExportDoc = ($('#ExportDoc').is(':checked') ? 'yes' : 'no');
    var sh_Pack = ($('#Pack').is(':checked') ? 'yes' : 'no');

    html =
        '<div class="col-2 mobile_hide" style="text-align:center">ส่งถึง</div>' +
        '<div class="col-3 mobile_hide" style="text-align:center">ปลายทาง</div>' +
        '<div class="col-1 mobile_hide" style="text-align:center">น้ำหนัก</div>' +
        '<div class="col-3 mobile_hide" style="text-align:center">บริการ</div>' +
        '<div class="col-2 mobile_hide">ค่าบริการ</div>' +
        '<div class="col-1 mobile_hide"></div>' +
        '<div class="col-md-12 sumary_data">' +

        // Customer
        '<input type="hidden" class="temp_addr_name" name="addr_name" value="' + addr_name + '">' +
        '<input type="hidden" class="temp_addr_address" name="addr_address" value="' + addr_address + '">' +
        '<input type="hidden" class="temp_addr_phone" name="addr_phone" value="' + addr_phone + '">' +
        '<input type="hidden" class="temp_postcode" name="postcode" value="' + addr_postcode + '">' +
        '<input type="hidden" class="temp_province" name="province" value="' + addr_province + '">' +
        '<input type="hidden" class="temp_email" name="email" value="' + addr_email + '">' +
        '<input type="hidden" class="temp_sh_weight" name="sh_weight" value="' + sh_weight + '">' +
        '<input type="hidden" class="temp_sh_service" name="sh_service" value="' + sh_service + '">' +
        '<input type="hidden" class="temp_volumnWeight" name="volumnWeight" value="' + volumnWeight + '">' +
        // end

        // binding_temp
        '<input type="hidden" class="temp_sh_type_of_service" name="sh_type_of_service" value="' + service.attr('s_id') + '">' +
        '<input type="hidden" class="temp_sh_declared_value" name="sh_declared_value" value="' + sh_declared + '">' +
        '<input type="hidden" class="temp_sh_description" name="sh_description" value="' + sh_description + '">' +
        '<input type="hidden" class="temp_sh_content_quantity" name="sh_content_quantity" value="' + sh_quantity + '">' +
        '<input type="hidden" class="temp_sh_packge_type" name="sh_packge_type" value="PA">' +
        '<input type="hidden" class="temp_sh_recipient" name="sh_recipient" value="' + sh_recipient + '">' +
        '<input type="hidden" class="temp_sh_company" name="sh_company" value="-">' +
        '<input type="hidden" class="temp_sh_email" name="sh_email" value="' + sh_email + ' ">' +
        '<input type="hidden" class="temp_sh_address1" name="sh_address1" value="' + sh_address1 + '">' +
        '<input type="hidden" class="temp_sh_address2" name="sh_address2" value="-">' +
        '<input type="hidden" class="temp_sh_address3" name="sh_address3" value="-">' +
        '<input type="hidden" class="temp_sh_phone" name="sh_phone" value="' + sh_phone + '">' +
        '<input type="hidden" class="temp_sh_country" name="sh_country" value="' + sh_country.val() + '">' +
        '<input type="hidden" class="temp_sh_currency" name="sh_currency" value="THB">' +
        '<input type="hidden" class="temp_sh_country_code" name="sh_country_code" value="' + sh_country.attr('code') + '">' +
        '<input type="hidden" class="temp_sh_city" name="sh_city" value="' + sh_city + '">' +
        '<input type="hidden" class="temp_sh_suburb" name="sh_suburb" value="-">' +
        '<input type="hidden" class="temp_sh_state" name="sh_state" value="-">' +
        '<input type="hidden" class="temp_sh_postcode" name="sh_postcode" value="' + sh_postcode + '">' +
        '<input type="hidden" class="temp_sh_Insurance" name="sh_Insurance" value="' + sh_Insurance + '">' +
        '<input type="hidden" class="temp_sh_ExportDoc" name="sh_ExportDoc" value="' + sh_ExportDoc + '">' +
        '<input type="hidden" class="temp_sh_Pack" name="sh_Pack" value="' + sh_Pack + '">' +
        '<input type="hidden" class="temp_sh_Note" name="sh_Note" value="-">' +
        '<input type="hidden" class="temp_total_regular_item" name="total_regular_item" value="' + service.attr('s_price') + '">' +
        '<div class="temp_arr" style="display:none;">';
    // end

    var box_total = $('#box_total').val();
    var weight = $('#sh_weight').val() / box_total;
    var box_auto_create = box_total - $('.box').length;
    $('.box').each(function(a, e) {
        var width = parseFloat(convert2number($(e).find(".width").val()));
        var length = parseFloat(convert2number($(e).find(".length").val()));
        var height = parseFloat(convert2number($(e).find(".height").val()));
        html +=
            '<input type="hidden" name="series[' + i + '][' + a + ']" value="' + (a + 1) + '">' +
            '<input type="hidden" name="weight[' + i + '][' + a + ']" value="' + weight + '">' +
            '<input type="hidden" name="width[' + i + '][' + a + ']" value="' + width + '">' +
            '<input type="hidden" name="length[' + i + '][' + a + ']" value="' + length + '">' +
            '<input type="hidden" name="height[' + i + '][' + a + ']" value="' + height + '">';
    });
    if (box_auto_create > 0) {
        var a = $('.box').length;
        var z;
        for (z = 0; z < box_auto_create; z++) {
            data += '<input type="hidden" name="series[' + i + '][' + (a + z) + ']" value="' + ((a + z) + 1) + '">' +
                '<input type="hidden" name="weight[' + i + '][' + (a + z) + ']" value="' + weight + '">' +
                '<input type="hidden" name="width[' + i + '][' + (a + z) + ']" value="0">' +
                '<input type="hidden" name="length[' + i + '][' + (a + z) + ']" value="0">' +
                '<input type="hidden" name="height[' + i + '][' + (a + z) + ']" value="0">';
        }
    }
    $('.desc').each(function(a, e) {
        var d_description = $(e).find(".d_description").val();
        var d_amount = parseFloat(convert2number($(e).find(".d_amount").val()));
        var d_number = parseFloat(convert2number($(e).find(".d_number").val()));
        html += '<input type="hidden" name="d_description[' + i + '][' + a + ']" value="' + d_description + '">' +
            '<input type="hidden" name="d_amount[' + i + '][' + a + ']" value="' + d_amount + '">' +
            '<input type="hidden" name="d_number[' + i + '][' + a + ']" value="' + d_number + '">';
    });
    html += '<input type="hidden" name="total_weight_item[' + i + ']" value="' + volumnWeight + '">';

    html += '</div>' +
        '<div class="card client-card mobile_hide">' +
        '<div class="row" style="padding-bottom:unset">' +
        '<div class="col-2" style="text-align:center"><b class="cut-text">' + sh_recipient + '</b></div>' +
        '<div class="col-3" style="text-align:center"><span class="cut-text">' + sh_country.val() + '</span></div>' +
        '<div class="col-1" style="text-align:center">' + volumnWeight + 'KG</div>' +
        '<div class="col-3" style="text-align:center"><span class="cut-text">' + sh_service + '</span></div>' +
        '<div class="col-2">' + numberWithCommas(service.attr('s_price')) + ' บาท</div>' +
        '</div></div>' +
        '<div class="card client-card mobile_show">' +
        '<div class="row">' +
        '<div class="col-4">ชื่อผู้รับ</div>' +
        '<div class="col-8"><b class="cut-text">' + sh_recipient + '</b></div>' +
        '<div class="col-4">ปลายทาง</div>' +
        '<div class="col-8"><span class="cut-text">' + sh_country.val() + '</span></div>' +
        '<div class="col-4">น้ำหนัก</div>' +
        '<div class="col-8">' + volumnWeight + 'KG</div>' +
        '<div class="col-4">บริการ</div>' +
        '<div class="col-8"><span class="cut-text">' + sh_service + '</span></div>' +
        '<div class="col-4">ค่าบริการ</div>' +
        '<div class="col-8">' + numberWithCommas(service.attr('s_price')) + '</div>' +
        '<div class="col-12" onclick=""><h4 style="color:red;text-align:center;margin-bottom:unset">แก้ไข</h4></div>' +
        '</div></div></div>';

    $("#temp_sumary").html(html);
}
