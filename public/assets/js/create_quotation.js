$('#ifram_add').click(function () {
    var number_ = parseInt($('#number_').val());
    var service = $('iframe[name=select_frame]').contents().find("input[name='service']:checked");
    var r = $("#tbform tbody .irow").length;
    var country = $('iframe[name=select_frame]').contents().find('#sh_country').val();
    var risk = $('iframe[name=select_frame]').contents().find('#risk').val();
    var volumnWeight = $('iframe[name=select_frame]').contents().find('#volumnWeight').val();
    var width = $('iframe[name=select_frame]').contents().find('input[name^="width"]');
    var weight = $('iframe[name=select_frame]').contents().find('input[name^="weight"]');
    var length = $('iframe[name=select_frame]').contents().find('input[name^="length"]');
    var height = $('iframe[name=select_frame]').contents().find('input[name^="height"]');
    var total_box = $('iframe[name=select_frame]').contents().find('input[name^="total_box"]');
    var true_volumnWeight = $('iframe[name=select_frame]').contents().find('#box_volumnWeight').text();
    var total_volumnWeight = $('iframe[name=select_frame]').contents().find('#total_volumnWeight').text();
    var boxnumber = width.length;
    var netWeight = volumnWeight / boxnumber;
    var number_unit = number_ + 1;
    var handing_unit = 0;
    var th_en = $('#translate').val();
    var sercive_price = 0;
    var service_name_dhl = $(service).parent().find(".cut-text").text().substr(0, 3);
    if (service_name_dhl != 'DHL' || service_name_dhl != 'Fed'){
        sercive_price = service.attr('s_price')
    }else{
        sercive_price = service.attr('s_price')-100;
    }
    if (service.length > 0) {
        var esc = service.attr('p_esc');
        var row = '<tr class="irow">' +
            '<td>' + (r + 1) + '</td>' +
            '<td colspan="2" style="text-align:left;"><strong style="font-size: 1.1em !important;"><input type="hidden" name="s_id[' + (r) + ']" value="' +
            service.attr('s_id') + '"><input type="hidden"  name="s_name[' + (r) + ']" value="' +
            $(service).parent().find(".cut-text").text() + '"><span class="lang" key="shipping_cost">ค่าขนส่ง</span> ' + $(service).parent().find(".cut-text").text() + '</strong><br>';

        for (var i = 0; i < boxnumber; i++) {
            row +=
                //'<span style="color:#C00; font-size:16px;">('+(i+1)+'). </span>'+
                //'<span style=" font-size:16px;">'+weight.eq(i).val()+' เทียบกับ '+width.eq(i).val()+' x '+length.eq(i).val()+' x '+height.eq(i).val()+' = </span>'+
                //'<span style="color:#006; font-size:16px;">'+volumnWeight+'</span><br>'+
                '<span style="padding-left:5%; font-size:16px;"><span class="lang" key="country">ประเทศ</span> ' + country + ' </span><br>' +
                '<span style="font-size:16px; padding-left:5%;"><span class="lang" key="real_weight">น้ำหนักจริง</span> ' + weight.eq(i).val() + ' <span class="lang" key="kg">กก.</span></span><br>' +
                '<span style="font-size:16px; padding-left:5%;"><span class="lang" key="volume_weight">น้ำหนักปริมาตร</span> ' + total_box.eq(i).val() + ' <span class="lang" key="kg">กก.</span></span><br>' +
                '<input type="hidden" name="width[' + (r) + '][' + (i) + ']" value="' + width.eq(i).val() + '">' +
                '<input type="hidden" name="weight[' + (r) + '][' + (i) + ']" value="' + weight.eq(i).val() + '">' +
                '<input type="hidden" name="length[' + (r) + '][' + (i) + ']" value="' + length.eq(i).val() + '">' +
                '<input type="hidden" name="height[' + (r) + '][' + (i) + ']" value="' + height.eq(i).val() + '">' +
                '<input type="hidden" name="volume_width[' + (r) + '][' + (i) + ']" value="' + total_box.eq(i).val() + '">';
                // '<input type="hidden" name="country['+(r)+']['+(i)+']" value="'+country+'">'+
            // '<input type="hidden" name="volumnWeight['+(r)+']['+(i)+']" value="'+service.attr('s_id')+'">';
        }

        row += '</td><td>' + total_volumnWeight + ' <span class="lang" key="kg">กก.</span></td>' +
            '<td><input type="text" class="center_input" onkeypress="return isNumberKey(event)" onkeyup="cal(' + number_unit + ',' + (r) + ')" name="s_price[' + (r) + ']" value="' + sercive_price + '"><br></td>' +
            '<td><span id="show_vat_s' + number_unit + '">0.00</td>' +
            '<td>'+
            '<input type="hidden" name="wht_one[' + (r) + ']" class="center_input" id="wht_one' + number_unit + '" '+
            'onkeypress="return isNumberKey(event)" onkeyup="" value="0"><span>0.00</span>'+
            '</td>' +
            '<td>'+'<input type="hidden" name="wht_tree[' + (r) + ']" class="center_input" id="wht_tree' + number_unit + '" '+
            'onkeypress="return isNumberKey(event)" onkeyup="" value="0"><span>0.00</span>'+
            '</td>'+
            '<td><span id="show_s_price' + number_unit + '"></span><br>' +
            '<input type="hidden" name="total_s_price[' + (r) + ']" id="total_s_price' + number_unit + '" value="">'+
            '<input type="hidden" name="vat_serven[' + (r) + ']" id="vat_serven' + number_unit + '" value="">'+
            '<input type="hidden" name="unit[' + (r) + ']" value="' + total_volumnWeight + '">' +
            '<input type="hidden" name="risk[' + (r) + ']" value="' + risk + '">' +
            '<input type="hidden" name="country[' + (r) + ']" value="' + country + '">' +
            '</td>' +
            '</tr>';
            if(service_name_dhl == 'DHL' ||  service_name_dhl == 'Fed'){
                row += '<tr>' +
                    '<td>&nbsp;</td>' +
                    '<td colspan="2" style="text-align: revert;padding-left:1%;font-size:16px;">' + '<span class="lang" key="handing_fee">ค่าเอกสารจัดส่ง</span>' + '</td>' +
                    '<td><span>1</span></td>' +
                    '<td><span>93.46</span></td>' +
                    '<td><span>6.54</span></td>' +
                    '<td><span>0.00</span></td>' +
                    '<td><input type="text" class="center_input" name="wht_handingfee[' + (r) + ']" onkeyup="cal_handing(' + r + ')" id="handing_fee' + r + '" onkeypress="return isNumberKey(event)" value="0"></td>' +
                    '<td><input type="hidden" id="total_handing' + (r) + '" name="total_handingfee[' + (r) + ']" value="100"><span class="total_handing' + (r) + '">100.00</span></td>' +
                    '<tr/>';

            }else{
                row += '<input type="hidden"  name="wht_handingfee[' + (r) + ']" value="">'+
                    '<input type="hidden" id="total_handing' + (r) + '" name="total_handingfee[' + (r) + ']" value="">';
            }

        row += '<tr id="add_other' + (r) + '">' +
            '<td class="end_service">&nbsp;</td>' +
            '<td class="end_service" colspan="2" style="text-align: revert;padding-left:1%;font-size:16px;">' + '<span class="select_tr lang" key="other_service" onclick="modal_other(' + r + ',' + esc + ')">+ บริการเสริม</span>' + '</td>' +
            '<td class="end_service"></td>' +
            '<td class="end_service"></td>' +
            '<td class="end_service"></td>' +
            '<td class="end_service"></td>' +
            '<td class="end_service"></td>' +
            '<td class="end_service"></td>' +
            '</tr>';
        $("#tbform tbody").append(row);
        cal(number_unit, r);
        $('#close_modal_discount').click();
        $('#number_').val(number_unit);
        $('#' + th_en).click();
    }
    else {
        alert('กรุณาเลือกบริการก่อนครับ')
    }
});

function isNumberKey(evt) {
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function cal(number_unit, r) {
    var s_name = $('input[name^="s_name[' + r + ']"]').val();
    var s_price = $('input[name^="s_price"]');
    var total_s_price = $('input[name^="total_s_price"]');
    var risk = $('input[name^="risk"]');
    var totle_other = $('input[name^="total_other_service"]');
    var total_handing = $('input[name^="total_handingfee"]');
    // var wht = parseFloat(convert2number($('input[name="withholding"]').val()));
    // var wht_vat = parseFloat(convert2number($('input[name="withholding_vat"]').val()));
    var discount = parseFloat(convert2number($('input[name="discount"]').val()));
    var document_charge = parseFloat(convert2number($('input[name="document_charge"]').val()));
    var total_price = 0;
    
    if (number_unit !== undefined) {
        var unit_price = $('input[name^="s_price[' + r + ']"]').val();
        var show_vat_s = unit_price * 7 / 100;
        if (s_name.substr(0,3) == "DHL" || s_name.substr(0,3) == "Fed"){
            show_vat_s = 0;
        }
        var total_unit = parseFloat(unit_price) + parseFloat(show_vat_s);
        $('#show_s_price' + number_unit).text((total_unit.toFixed(2)));
        $('#total_s_price' + number_unit).val((total_unit.toFixed(2)));
        $("#show_vat_s"+number_unit).text(show_vat_s.toFixed(2));
        $('#vat_serven' + number_unit).val(show_vat_s.toFixed(2));
    }

    $(total_s_price).each(function (i, e) {
        total_price = total_price + parseFloat(convert2number(($(e).val())));
    });
    $(risk).each(function (i, e) {
        total_price = total_price + parseFloat(convert2number(($(e).val())));
    });
    $(totle_other).each(function (i, e) {
        total_price = total_price + parseFloat(convert2number(($(e).val())));
    });
    $(total_handing).each(function (i, e) {
        total_price = total_price + parseFloat(convert2number(($(e).val())));
    });

    var wht_valule = (total_price * 1 / 100).toFixed(2);
    var wht_val = (total_price * 1.5 / 100).toFixed(2);

    $('#total_price').html((total_price).toFixed(2));
    $('.total_price').val((total_price).toFixed(2));
    $('#wht_value').html(wht_valule);
    $('#wht_val').html(wht_val);
    var total = (total_price - discount - document_charge - wht_valule - wht_val).toFixed(2);
    $('#grand_total').html(total);
};

function cal_other_service(ii,n) {
    var service_value = $("#service_value"+ii+n).val();
    var other_name = $("#other_name"+ii+n).val()
    var vat_o = ['International Freight', 'Permium', 'Remote area Pickup', 'Remote area Delivery', 'Shipment Redirect', 'Over Weight Piece', 'Over Sized Piece', 'Elevated Risk', 'Restricted Destination'];
    var vat_t = ['Change of billing', 'Address correction', 'Shipment Insurance', 'All custom services'];
    var vat_esc = ['Emergency surcharge'];
    var vat_1 = $("#other_vat_1_"+ii+n).val()!=""?$("#other_vat_1_"+ii+n).val():0;
    var vat_3 = $("#other_vat_3_"+ii+n).val()!=""?$("#other_vat_3_"+ii+n).val():0;
    var total = 0;
    var vat_ = 0;
    if (vat_o.includes(other_name)) {
        total = + (parseFloat(service_value) + vat_) - parseFloat(vat_1);
        $("#other_vat_3_" + ii + n).prop('readonly', true);
        $("#other_vat_3_" + ii + n).css('background-color', '#ddd');
    }
    if (vat_t.includes(other_name)) {
        vat_ =+ parseFloat(service_value) * 7 / 100;
        total =+ (parseFloat(service_value) + vat_) - parseFloat(vat_3);
        $("#other_vat_1_" + ii + n).prop('readonly', true);
        $("#other_vat_1_" + ii + n).css('background-color', '#ddd');
    }
    if (vat_esc.includes(other_name)) {
        total = + parseFloat(service_value)-(parseFloat(vat_1)+parseFloat(vat_3));
    }
    $("#vat_other_service" + ii + n).val((convert2number(vat_)).toFixed(2));
    $(".vat_other_service" + ii + n).text((convert2number(vat_)).toFixed(2));
    $("#total_other_service" + ii + n).val(total);
    $("#show_other_service" + ii + n).text((total).toFixed(2));
    cal();
};
function cal_handing(n) {
    var handing_fee = $("#handing_fee" + n).val();
    if (handing_fee == "") {
        handing_fee = 0;
    }
    var total_hand = 100 - parseFloat(handing_fee);
    $("#total_handing" + n).val((total_hand).toFixed(2));
    $(".total_handing" + n).text((total_hand).toFixed(2));
    cal();
};

function convert2number(value) {
    var result = value;
    if (value === undefined || isNaN(value) || value === "") {
        result = 0;
    }
    return result;
}

$(".switch_toppic").click(function () {
    var check = $("#switch_toppic").val()
    var th_en = $('#translate').val();
    if (check == 0) {
        if (th_en == "th") {
            $(".switch_text").text("ใบแจ้งหนี้");
            $(".switch_").text("ใบเสนอราคา");
            $(".quotation_toppic").text("ใบแจ้งหนี้");
            $(".quotation_toppic").val("ใบแจ้งหนี้");
        } else if (th_en == "en") {
            $(".switch_text").text("Invoice");
            $(".switch_").text("Quotation");
            $(".quotation_toppic").text("Invoice");
            $(".quotation_toppic").val("Invoice");
        }
        $("#switch_toppic").val("1");
    } else {
        if (th_en == "th") {
            $(".switch_text").text("ใบเสนอราคา");
            $(".switch_").text("ใบแจ้งหนี้");
            $(".quotation_toppic").text("ใบเสนอราคา");
            $(".quotation_toppic").val("ใบเสนอราคา");
        } else if (th_en == "en") {
            $(".switch_text").text("Quotation");
            $(".switch_").text("Invoice");
            $(".quotation_toppic").text("Quotation");
            $(".quotation_toppic").val("Quotation");
        }
        $("#switch_toppic").val("0");
    }
});

// $('#add_other_service').click(function () {
//     $('#other_service_footer').before('<tr class="new_other_service">' +
//         '<td><input type="checkbox" class="other_service_ck" onclick="other_ck($(this))" /></td>' +
//         '<td style="padding: unset;"><input type="text" class="form-control other_service_name" disabled required/></td>' +
//         '<td style="padding: unset;"><input name="other"' +
//         'id="sh_wood_packing" class="form-control other_service_value"' +
//         'onkeypress="return isNumberKey(event)" value="0" disabled />' +
//         '</td>' +
//         '</tr>')
// });

function other_ck(e) {
    if ($(e).is(':checked')) {
        //console.log($(this).parent().parent());
        $(e).parent().parent().find('.other_service_name').prop('disabled', false);
        $(e).parent().parent().find('.other_service_value').prop('disabled', false);
    }
    else {
        $(e).parent().parent().find('.other_service_name').prop('disabled', true);
        $(e).parent().parent().find('.other_service_value').prop('disabled', true);
    }
};

function add_ck() {
    var r = $("#tbform tbody .irow").length;
    var ii = $(".num_other").val();
    var total_other_service = 0;
    var num_other = $('.num_other').val();
    var other_service_list = $('.other_service_ck');
    var vat_o_ = ['International Freight', 'Permium', 'Remote area Pickup', 'Remote area Delivery', 'Shipment Redirect', 'Over Weight Piece', 'Over Sized Piece', 'Elevated Risk', 'Restricted Destination'];
    var vat_t_ = ['Change of billing', 'Address correction', 'Shipment Insurance', 'All custom services'];
    other_service_list.each(function (i, e) {
        if ($(e).is(':checked')) {
            var n = $(".n"+ii).length;
            var service_name = $(e).parent().parent().find('.other_service_name').val();
            var service_value = $(e).parent().parent().find('.other_service_value').val();
            var vat_other_service = 0;
            var vat_other = 0;
            var vat_other_ = 0;
            if (vat_o_.includes(service_name)) {
                vat_other_ = service_value * 1 / 100;
                vat_other = 0;
            }
            if (vat_t_.includes(service_name)) {
                vat_other = service_value * 1.5 / 100;
                vat_other_ = 0;
            }
            total_other_service = parseFloat(convert2number(service_value)) - vat_other_service;
            row = '<tr class="n'+ii+'"><td></td>' +
                '<td colspan="2" style="text-align:left"><span>' + service_name + ' </span></td>' +
                '<td><span class="unit_other_service' + n + '">1</span></td>' +
                '<td><span class="sh_other_service" id="sh_other_service' + n + '">' + parseFloat(service_value).toFixed(2) + '</span>' +
                '</td>' +
                '<td><span class="vat_other_service'  + ii + n +  '">' + vat_other_service + '</span></td>' +

                '<td>' +
                '<input type="text" class="center_input" id="other_vat_1_' + ii + n + '" name="other_wht_one[' + ii + '][' + n + ']" '+
                'value="'+vat_other_+'" onkeypress="return isNumberKey(event)" onkeyup="cal_other_service('+ii+','+n+')">' +
                '</td>' +
                '<td>' +
                '<input type="text" class="center_input" id="other_vat_3_' + ii + n + '" name=other_wht_tree[' + ii + '][' + n + ']" '+
                'value="' + vat_other + '" onkeypress="return isNumberKey(event)" onkeyup="cal_other_service('+ii+','+n+')">' +
                '</td>' +

                '<td><span id="show_other_service' + ii + n +  '">' + total_other_service + '</span>' +
                '<input type="hidden" id="other_name' +ii+n+ '"name="other_name[' + ii + '][' + n + ']" value="' + service_name + '">' +
                '<input type="hidden" id="vat_other_service'  + ii + n +  '" name="vat_other_service[' + ii + '][' + n + ']" value="' + vat_other_service + '">' +
                '<input type="hidden" id="unit_other_service' +ii+n+ '" name="unit_other_service[' + ii + '][' + n + ']" value="1">' +
                '<input type="hidden" id="service_value' +ii+n+ '" name="service_value[' + ii + '][' + n + ']" value="' + service_value + '">' +
                '<input type="hidden" id="total_other_service' + ii + n + '" name="total_other_service[' + ii + '][' + n + ']" value="' + total_other_service + '"></td></tr>';
            $("#add_other" + num_other).before(row);
            cal_other_service(ii,n);
            $(e).prop('checked', false);
            $(e).parent().parent().find('.other_service_value').val(0);
            $(e).parent().parent().find('.other_service_name').prop('disabled', true);
            $(e).parent().parent().find('.other_service_value').prop('disabled', true);
        }
    });
    $('#close_supplement').click();
};

$('.other_service_ck').change(function () {
    if ($(this).is(':checked')) {
        //console.log($(this).parent().parent());
        $(this).parent().parent().find('.other_service_name').prop('disabled', false);
        $(this).parent().parent().find('.other_service_value').prop('disabled', false);
    }
    else {
        $(this).parent().parent().find('.other_service_name').prop('disabled', true);
        $(this).parent().parent().find('.other_service_value').prop('disabled', true);
    }
});

var translate = {
    "en": {
        "company_address_1": "SME Shipping Co., Ltd.",
        "company_address_2": "Rachadapisek 16 Alley, Lane 2,",
        "company_address_3": "Wat Tha Pra, Bangkok Yai, Bangkok 10600",
        "company_address_4": "Text : 02-105-7777 Fax : 02-105-7778",
        "quotation": "Quotation",
        "invoice": "Invoice",
        "taxpayer_number": "TAXPAYER IDENTIFICATION NUMEBER",
        "company_name": "COMPANY NAME",
        "customer_name": "CUSTOMER NAME",
        "address": "Address",
        "telephone": "Telephone",
        "email": "Email",
        "date": "DATE",
        "quote_no": "QUOTE NO.",
        "payment_terms": "PAYMENT TERMS",
        "valid_time_day": "VALID TIME DAY",
        "refer": "REFER",
        "detail_condition": "We are please to submit you the following quotation and offer to sell shipping rate described here in at price. Item and term stated.",
        "add_service": "+ Add Service",
        "no": "No.",
        "description": "Description",
        "unit": "Unit",
        "amount": "AMOUNT(THB)",
        "serven_vat": "Vat 7%",
        "total_amount": "Total Amount(THB)",
        "country": "Country",
        "real_weight": "Real Weight",
        "volume_weight": "Volume Weight",
        "emergency": "Emergency surcharge",
        "kg": "KG.",
        "other_service_charge": "Other Service Charge",
        "condition": "Condition",
        "condition_1": "- Service term Door to Door, Delivered Duties Unpaid*",
        "condition_2": "- Money Bock Guarantee (MBG) does not apply to this shipment.",
        "condition_3": "- All other conditions of Contact on SHIPPING ORDER and SME SHIPPING Standard Conditions of Carriage shall apply to the shipment.",
        "condition_4": "- Remote area and surcharges and other surcharges my be charged in additional to this rate.",
        "condition_5": "- SME SHIPPING reserves the right at any time to amend, modify, or discontinue the rate and the terms and conditions herein without prior notice.",
        "condition_6": "- Duties and taxes and/or GST (goods and services tax) may be assessed on the contents. Any such charge will be billed to the recipient, unless instructed otherwise by the shipper. Notwithstanding the aforesaid, will aways be primarily responsible for all charges relating to this shipment.",
        "total": "Total",
        "wht_1": "WHT 1%",
        "wht_3": "WHT 3%",
        "approve": "Approve to order items descrived here in at price and terms states.",
        "approved_by": "Approved by __________________________________",
        "position": "Position __________________________________",
        "employee": "Employee",
        "sincerely": "Sincerely Yours.",
        "sales": "Sales Manager __________________________________",
        "handing_fee":"Handing Fee",
        "shipping_cost":"Shipping cost",
        "other_service":"+ Other Service"
    },
    "th": {
        "company_address_1": "บริษัทเอสเอ็มอี ชิปปิ้ง จำกัด",
        "company_address_2": "46-48 ชอยรัชดาภิเษก 16 ถนนรัชดาภิเษก แขวงวัดท่าพระ",
        "company_address_3": "เขตบางกออกใหญ่ กรุงเทพฯ 10600",
        "company_address_4": "โทรศัพท์ : 02-105-7777 โทรสาร : 02-105-7778",
        "quotation": "ใบเสนอราคา",
        "invoice": "ใบแจ้งหนี้",
        "taxpayer_number": "เลขประจำตัวผู้เสียภาษีอากร",
        "company_name": "บริษัท",
        "customer_name": "ลูกค้า",
        "address": "ที่อยู่",
        "telephone": "โทรศัพท์",
        "email": "อีเมล์",
        "date": "วันที่ ",
        "quote_no": "เลขที่",
        "payment_terms": "เงื่อนไขการชำระ",
        "valid_time_day": "กำหนดวันยืนยันราคา",
        "refer": "อ้างอิง",
        "detail_condition": "บริษัทฯมีความยินดีที่จะเสนอราคาขนส่งแด่ท่านตามราคาและเงื่อนไขที่ระบุไว้ในใบเสนอราคานี้",
        "add_service": "+ เพิ่มบริการ",
        "no": "ลำดับ",
        "description": "รายละเอียด",
        "unit": "หน่วย",
        "amount": "จำนวนเงิน (บาท)",
        "serven_vat": "ภาษีมูลค่าเพิ่ม 7%",
        "total_amount": "จำนวนเงินรวม (บาท)",
        "country": "ประเทศ",
        "real_weight": "น้ำหนักจริง",
        "volume_weight": "น้ำหนักปริมาตร",
        "emergency": "ค่าระวางพิเศษ",
        "kg": "กก.",
        "other_service_charge": "บริการเสริม",
        "condition": "ข้อกำหนด เงื่อนไขการส่งพัสดุ",
        "condition_1": "- การจัดส่งเป็นการบริการแบบส่งถึงที่อยู่ผู้รับปลายทาง (Door To Door) ไม่รวมการชำระค่าภาษีที่เกิดขึ้นจากศุลกากร",
        "condition_2": "- ไม่มีนโยบายรับประกันความพึงพอใจ ยินดีคืนเงิน สำหรับการส่งพัสดุประเภทนี้",
        "condition_3": "- เงื่อนไข และสัญญาอื่นๆ ทั้งหมดบนแบบฟอร์มการส่งสินค้า และข้อกำหนดพื้นฐานของผู้ให้บริการมีผล บังคับใช้กับการส่งสินค้าชิ้นนี้",
        "condition_4": "- พื้นที่นอกเหนือจากการให้บริการ อาจมีการค่าบริการบริการเพิ่มเติม ขึ้นอยู่กับระยะทาง",
        "condition_5": "- บริษัท SME SHIPPING จำกัด ขอสงวนสิทธิ์ในการปรับปรุง แก้ไข หรือเปลี่ยนแปลงราคารวมถึง ข้อกำหนด และเงื่อนไขที่แนบมาในที่นี้ได้ตลอดเวลาโดยไม่แจ้งให้ทราบล่วงหน้า",
        "condition_6": "- ค่าภาษีศุลกากร และภาษีมูลค่าเพิ่ม หรือ ค่าภาษีสินค้าและบริการขึ้นอยู่กับดุลยพินิจของหน่วยงานที่เกี่ยวข้อง ค่าภาษีใดๆก็ตามที่เกี่ยวข้องจะเป็นหน้าที่ของผู้รับพัสดุในการชำระค่าใช้จ่ายดังกล่าว นอกจากได้รับคำแนะนำ/ เงื่อนไข/ ข้อแนะนำอื่นๆ จากผู้ส่ง",
        "total": "จำนวนเงิน",
        "wht_1": "หัก ณ ที่จ่าย 1%",
        "wht_3": "หัก ณ ที่จ่าย 3%",
        "approve": "อนุมัติสั่งซื้อบริการตามที่ได้เสนอราคา",
        "approved_by ": "ผู้สั่งซื้อ __________________________________",
        "position": "ตำแหน่ง __________________________________",
        "employee": "พนักงานขาย",
        "sincerely": "ขอแสดงความนับถือ",
        "sales": "หัวหน้าฝ่ายขาย __________________________________",
        "handing_fee":"ค่าเอกสารจัดส่ง",
        "shipping_cost":"ค่าขนส่ง",
        "other_service":"+ บริการเสริม"
    }
};

$('.translate').click(function () {
    var lang = $(this).attr('id');
    var quotation_toppic = $("#switch_toppic").val();
    $('.lang').each(function (index, element) {
        $(this).text(translate[lang][$(this).attr('key')]);
    });
    $("#translate").val(lang);
    if (lang == "th") {
        $("#th").addClass("th_select");
        $("#en").removeClass("en_select");
        if (quotation_toppic == "1") {
            $(".switch_text").text("ใบแจ้งหนี้");
            $(".switch_").text("ใบเสนอราคา");
            $(".quotation_toppic").text("ใบแจ้งหนี้");
            $(".quotation_toppic").val("ใบแจ้งหนี้");
        }
        if (quotation_toppic == "0") {
            $(".switch_text").text("ใบเสนอราคา");
            $(".switch_").text("ใบแจ้งหนี้");
            $(".quotation_toppic").text("ใบเสนอราคา");
            $(".quotation_toppic").val("ใบเสนอราคา");
        }
    } else if (lang == "en") {
        $("#en").addClass("en_select");
        $("#th").removeClass("th_select");
        if (quotation_toppic == "1") {
            $(".switch_text").text("Invoice");
            $(".switch_").text("Quotation");
            $(".quotation_toppic").text("Invoice");
            $(".quotation_toppic").val("Invoice");
        }
        if (quotation_toppic == "0") {
            $(".switch_text").text("Quotation");
            $(".switch_").text("Invoice");
            $(".quotation_toppic").text("Quotation");
            $(".quotation_toppic").val("Quotation");
        }
    }
});

function translate_(n) {
    var lang = $("#" + n).attr('id');
    $('.lang').each(function () {
        $(n).text(translate[lang][$(n).attr('key')]);
    });
    $("#translate").val(lang);
    if (lang == "th") {
        $("#th").addClass("th_select");
        $("#en").removeClass("en_select");
    } else if (lang == "en") {
        $("#en").addClass("en_select");
        $("#th").removeClass("th_select");
    }
}

function modal_other(i, n) {
    $('#supplement').modal();
    $('.num_other').val(i);
    $("#esc").val(n);
}

function td_delete(e,index) {
    var delete_ = $('.delete_'+index);
    delete_.remove();
    num = 0;
    $("#tbform tbody .irow").each(function (i, e) {
        $(e).find('td').eq(0).text(num+=1);
        $(e).find('input[name^="s_id"]').each(function (ii, ee) {
            var s_id = "s_id[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/s_id\[[0-9]+\]/g, s_id);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="s_name"]').each(function (ii, ee) {
            var s_name = "s_name[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/s_name\[[0-9]+\]/g, s_name);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="width"]').each(function (ii, ee) {
            var width = "width[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/width\[[0-9]+\]/g, width);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="weight"]').each(function (ii, ee) {
            var weight = "weight[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/weight\[[0-9]+\]/g, weight);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="length"]').each(function (ii, ee) {
            var length = "length[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/length\[[0-9]+\]/g, length);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="height"]').each(function (ii, ee) {
            var height = "height[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/height\[[0-9]+\]/g, height);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="country"]').each(function (ii, ee) {
            var country = "country[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/country\[[0-9]+\]/g, country);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="volumnWeight"]').each(function (ii, ee) {
            var volumnWeight = "volumnWeight[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/volumnWeight\[[0-9]+\]/g, volumnWeight);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="s_price"]').each(function (ii, ee) {
            var s_price = "s_price[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/s_price\[[0-9]+\]/g, s_price);
            $(this).attr('name', name);
        });
        // เพิ่มเตืม Service
        $(e).find('input[name^="wht_handingfee"]').each(function (ii, ee) {
            var wht_handingfee = "wht_handingfee[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/wht_handingfee\[[0-9]+\]/g, wht_handingfee);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="wht_one"]').each(function (ii, ee) {
            var wht_one = "wht_one[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/wht_one\[[0-9]+\]/g, wht_one);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="wht_tree"]').each(function (ii, ee) {
            var wht_tree = "wht_tree[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/wht_tree\[[0-9]+\]/g, wht_tree);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="wht_handingfee"]').each(function (ii, ee) {
            var unit = "unit[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/unit\[[0-9]+\]/g, unit);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="vat_serven"]').each(function (ii, ee) {
            var vat_serven = "vat_serven[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/vat_serven\[[0-9]+\]/g, vat_serven);
            $(this).attr('name', name);
        });

        // บริการเสริม (other service)
        $(e).find('input[name^="other_wht_one"]').each(function (ii, ee) {
            var other_wht_one = "other_wht_one[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/other_wht_one\[[0-9]+\]/g, other_wht_one);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="other_wht_one"]').each(function (ii, ee) {
            var other_wht_one = "other_wht_one[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/other_wht_one\[[0-9]+\]/g, other_wht_one);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="other_name"]').each(function (ii, ee) {
            var other_name = "other_name[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/other_name\[[0-9]+\]/g, other_name);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="vat_other_service"]').each(function (ii, ee) {
            var vat_other_service = "vat_other_service[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/vat_other_service\[[0-9]+\]/g, vat_other_service);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="unit_other_service"]').each(function (ii, ee) {
            var unit_other_service = "unit_other_service[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/unit_other_service\[[0-9]+\]/g, unit_other_service);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="unit_other_service"]').each(function (ii, ee) {
            var unit_other_service = "unit_other_service[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/unit_other_service\[[0-9]+\]/g, unit_other_service);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="service_value"]').each(function (ii, ee) {
            var service_value = "service_value[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/service_value\[[0-9]+\]/g, service_value);
            $(this).attr('name', name);
        });
        $(e).find('input[name^="total_other_service"]').each(function (ii, ee) {
            var total_other_service = "total_other_service[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/total_other_service\[[0-9]+\]/g, total_other_service);
            $(this).attr('name', name);
        });

        var thisindex = $(e).index();
        $("#tbform tbody tr").eq(thisindex + 1).find('input[name^="risk"]').each(function (ii, ee) {
            var risk = "risk[" + i + "]";
            var name = $(this).attr('name');
            name = name.replace(/risk\[[0-9]+\]/g, risk);
            $(this).attr('name', name);
        });
    });
    cal();
}
