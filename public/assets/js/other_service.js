$('.other_service_ck').change(function () {
    if ($(this).is(':checked')) {
        //console.log($(this).parent().parent());
        $(this).parent().parent().find('.other_service_name').prop('disabled', false);
        $(this).parent().parent().find('.other_service_value').prop('disabled', false);
    }
    else {
        $(this).parent().parent().find('.other_service_name').prop('disabled', true);
        $(this).parent().parent().find('.other_service_value').prop('disabled', true);
    }
});
function other_ck(e) {
    if ($(e).is(':checked')) {
        //console.log($(this).parent().parent());
        $(e).parent().parent().find('.other_service_name').prop('disabled', false);
        $(e).parent().parent().find('.other_service_value').prop('disabled', false);
    }
    else {
        $(e).parent().parent().find('.other_service_name').prop('disabled', true);
        $(e).parent().parent().find('.other_service_value').prop('disabled', true);
    }
};
$('#add_other_service').click(function () {
    $('#other_service_footer').before('<tr class="new_other_service">' +
        '<td><input type="checkbox" class="other_service_ck" onclick="other_ck($(this))" /></td>' +
        '<td style="padding: unset;"><input type="text" class="form-control other_service_name" disabled required/></td>' +
        '<td style="padding: unset;"><input name="other"' +
        '        id="sh_wood_packing" class="form-control other_service_value"' +
        '        onkeypress="return isNumberKey(event)" value="0" disabled />' +
        '</td>' +
        '</tr>')
});