$(document).ready(function ($) {
    $('#sh_postcode').keyup(function () {
        $('#sh_postcode').unmask();
        $('#sh_postcode').addClass('is-invalid');
        $('#sh_postcode').removeClass('is-valid');
        this.value = this.value.toUpperCase();
    });
    $('#sh_country').change(function () {
        $('#sh_show_country').val($('#sh_country').val());
    });
    $('#sh_postcode').change(function () {
        var sh_country = $('#sh_country option:selected');
        var sh_country_code = sh_country.attr('code');
        var sh_zipcode = $('#sh_postcode').val();
        if (sh_zipcode != "") {
            $('#process_postcode').show();
            $.ajax({
                type: "GET",
                url: '/checkformat/' + sh_country_code + '/' + sh_zipcode,
                processData: false,
                contentType: false,
                success: function (result) {
                    console.log(result);
                    if (result.status) {
                        if (result.res.length == 1) {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_city').val(result.res[0].city_name);
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $('#select_city').append(new Option(result.res[0].city_name, result.res[0].city_name));
                        }
                        else {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $(result.res).each(function (i, e) {
                                $('#select_city').append(new Option(e.city_name, e.city_name));
                            })
                            var option = $('#select_city option').filter(function () {
                                return this.value === $("#sh_city").val();
                            }).val();

                            if (!option) {
                                $('#sh_city').val('');
                            }
                        }
                    }
                    else {
                        $('#sh_postcode').addClass('is-invalid');
                    }
                    $('#process_postcode').hide();
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    $('#sh_postcode').addClass('is-invalid');
                    $('#sh_postcode').removeClass('is-valid');
                    $("#loading").hide();
                }
            });
        }
        else {
            $('#process_postcode').hide();
            $("#loading").hide();
            $('#sh_postcode').removeClass('is-invalid');
        }
    });

    $('#sh_postcode').keyup(function () {
        $('#process_postcode').show();
        var sh_country = $('#sh_country option:selected');
        var sh_country_code = sh_country.attr('code');
        var sh_zipcode = $('#sh_postcode').val();
        $('#sh_city').val('');
        if (sh_zipcode != "") {
            $.ajax({
                type: "GET",
                url: '/checkformat/' + sh_country_code + '/' + sh_zipcode,
                processData: false,
                contentType: false,
                success: function (result) {
                    console.log(result);
                    $('#process_postcode').hide();
                    if (result.status) {
                        if (result.res.length == 1) {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_city').val(result.res[0].city_name);
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $('#select_city').append(new Option(result.res[0].city_name, result.res[0].city_name));
                        }
                        else {
                            $('#sh_postcode').mask(result.value);
                            $('#sh_city').val('');
                            $('#sh_state').val(result.res[0].state_code)
                            $('#sh_postcode').removeClass('is-invalid');
                            $('#sh_postcode').addClass('is-valid');
                            $('#select_city').html('');
                            $(result.res).each(function (i, e) {
                                $('#select_city').append(new Option(e.city_name, e.city_name));
                            })
                        }
                    }
                    else {
                        $('#sh_postcode').addClass('is-invalid');
                    }
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    $('#sh_postcode').addClass('is-invalid');
                    $('#sh_postcode').removeClass('is-valid');
                    $("#loading").hide();
                }
            });
        }
        else {
            $('#process_postcode').hide();
            $("#loading").hide();
            $('#sh_postcode').removeClass('is-invalid');
        }
    });

    $('#sh_city, #sh_postcode').change(function () {
        var sh_country_code = $('#sh_country option:selected').attr('code');
        var sh_city = $('#sh_city').val();
        $.ajax({
            type: "GET",
            url: '/admin/getsuburb/' + sh_country_code + '/' + sh_city,
            processData: false,
            contentType: false,
            success: function (result) {
                result = JSON.parse(result);
                console.log(result);
                if (result.length > 0) {
                    $('.subrub_area').show();
                    $(result).each(function (i, e) {
                        $('#select_subrub').append(new Option(e.suburb, e.suburb));
                    });
                }
                else {
                    $('#sh_suburb').val('');
                    $('.subrub_area').hide();
                }

            },
            error: function (e) {
                console.log("ERROR : ", e);
                $('#sh_postcode').addClass('is-invalid');
                $('#sh_postcode').removeClass('is-valid');
                $("#loading").hide();
            }
        });
    });
    $('#sh_country').change(function () {
        var sh_country_code = $('#sh_country option:selected').attr('code');
        $.ajax({
            type: "GET",
            url: '/admin/getcitybycountrycode/' + sh_country_code,
            processData: false,
            contentType: false,
            success: function (result) {
                result = JSON.parse(result);
                //$('#sh_city').val('');
                $('#select_city').html('');
                $(result).each(function (i, e) {
                    $('#select_city').append(new Option(e.city_name, e.city_name));
                });
                var option = $('#select_city option').filter(function () {
                    return this.value === $("#sh_city").val();
                }).val();

                if (!option) {
                    $('#sh_city').val('');
                }
            },
            error: function (e) {
                console.log("ERROR : ", e);
                $('#sh_postcode').addClass('is-invalid');
                $('#sh_postcode').removeClass('is-valid');
                $("#loading").hide();
            }
        });
    });
});
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('input[type=radio][name=time]').change(function () {
        if (this.value == 'พร้อมรับ') {
            $('#delivery_time').prop('disabled', true);
        }
        else {
            $('#delivery_time').prop('disabled', false);
        }
    });
});
function isNumberKey(evt) {
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function nextstep(step) {
    $('#step_1,#step1').hide();
    $('#step_2,#step2').hide();
    $('#step_3,#step3').hide();
    $('#step_4,#step4').hide();
    $('#step_5,#step5').hide();
    $('#step_2_progress').removeClass('active');
    $('#step_3_progress').removeClass('active');
    $('#step_4_progress').removeClass('active');

    $('#step_' + step).show();
    $('#step' + step).show();
    if (step == 2) {
        $('#step_2_progress').addClass('active');
    }
    else if (step == 3) {
        $('#step_2_progress').addClass('active');
        $('#step_3_progress').addClass('active');
    }

    else if (step == 4) {
        $('#step_2_progress').addClass('active');
        $('#step_3_progress').addClass('active');
        $('#step_4_progress').addClass('active');
    }
    else {
        $('#step_2_progress').addClass('active');
        $('#step_3_progress').addClass('active');
        $('#step_4_progress').addClass('active');
    }
}
$("#form_step_1").submit(function (e) {
    e.preventDefault();
    nextstep(2);
});
$("#form_step_2").submit(function (e) {
    e.preventDefault();
    nextstep(3);
});
$("#form_step_3").submit(function (e) { //data binding
    e.preventDefault();
    var service = $("input[name='service']:checked");
    if (service.length > 0) {
        var sh_recipient = $('#sh_recipient').val();
        var sh_country = $('#sh_country option:selected');
        var sh_phone = $('#sh_phone').val();
        var box_number = $('#box_total').val();
        var volumnWeight = $('#volumnWeight').val();
        var edit_mode = $('#edit_number').val();
        var i = edit_mode != '' ? edit_mode : $('.sumary_data').length;
        var html = '<div class="col-md-12 sumary_data">' + binding() +
            '<div class="card client-card mobile_hide">' +
            '<div class="row" style="padding-bottom:unset">' +
            '<div class="col-2" style="text-align:center"><b class="cut-text">' + sh_recipient + '</b></div>' +
            '<div class="col-3" style="text-align:center"><span class="cut-text">' + sh_country.val() + '</span></div>' +
            '<div class="col-1" style="text-align:center">' + volumnWeight + ' KG</div>' +
            '<div class="col-3" style="text-align:center"><span class="cut-text">' + $(service).parent().find(".cut-text").text() + '</span></div>' +
            '<div class="col-2">' + numberWithCommas(service.attr('s_price')) + ' บาท</div>' +
            '<div class="col-1" onclick="edit_shipment(' + i + ')"><span style="color:red">แก้ไข</span></div>' +
            '</div>' +
            '</div>' +
            '<div class="card client-card mobile_show">' +
            '<div class="row">' +
            '<div class="col-4">ชื่อผู้รับ</div>' +
            '<div class="col-8"><b class="cut-text">' + sh_recipient + '</b></div>' +
            '<div class="col-4">ปลายทาง</div>' +
            '<div class="col-8"><span class="cut-text">' + sh_country.val() + '</span></div>' +
            '<div class="col-4">น้ำหนัก</div>' +
            '<div class="col-8">' + volumnWeight + ' KG</div>' +
            '<div class="col-4">บริการ</div>' +
            '<div class="col-8"><span class="cut-text">' + $(service).parent().find(".cut-text").text() + '</span></div>' +
            '<div class="col-4">ค่าบริการ</div>' +
            '<div class="col-8">' + numberWithCommas(service.attr('s_price')) + ' บาท</div>' +
            '<div class="col-12" onclick="edit_shipment(' + i + ')">' +
            '<h4 style="color:red;text-align:center;margin-bottom:unset">แก้ไข</h4>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        if (edit_mode != '') {
            var area = $('#sumary_area').find('.sumary_data').eq(edit_mode);
            area.replaceWith(html);
        }
        else {
            $('#sumary_area').append(html);
        }
        $('#edit_number').val('');
        $('#service_number').val('');

        nextstep(4);
    }
    else {
        alert('โปรดเลือกบริการและราคาที่ต้องการ');
    }
    var shipment_ = $('#sumary_area').html();
    $.ajax({
        type: "POST",
        url: '/app/save_temp',
        data: { temp: shipment_ },
        success: function (result) {
            console.log('Save temp success.');
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });

});
$("#form_step_4").submit(function (e) {
    e.preventDefault();
    nextstep(5);
});
$("#form_step_5").submit(function (e) {
    e.preventDefault();
    var url = $('#form_step_4').attr("action");
    $.ajax({
        type: "POST",
        data: $("#form_step_1,#form_step_4,#form_step_5").serialize(),
        url: url,
        success: function (result) {
            $("#loading").hide();
            window.location = '/app/label/' + result;
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert(
                "An error occurred on the server when processing the URL. Please contact the system administrator."
            );
            $("#loading").hide();
        }
    });
});

//edit shipment

function edit_shipment(row) {
    addshipping();
    $('#edit_number').val(row);
    $('#service_number').val($("input[name='sh_type_of_service[" + row + "]']").val());

    var name = $("input[name='sh_recipient[" + row + "]']").val();
    var company = $("input[name='sh_company[" + row + "]']").val();
    var email = $("input[name='sh_email[" + row + "]']").val();
    var address1 = $("input[name='sh_address1[" + row + "]']").val();
    var address2 = $("input[name='sh_address2[" + row + "]']").val();
    var address3 = $("input[name='sh_address3[" + row + "]']").val();
    var phone = $("input[name='sh_phone[" + row + "]']").val();
    var city = $("input[name='sh_city[" + row + "]']").val();
    var zipcode = $("input[name='sh_postcode[" + row + "]']").val();
    var state = $("input[name='sh_state[" + row + "]']").val();
    var suburb = $("input[name='sh_suburb[" + row + "]']").val();
    var country = $("input[name='sh_country[" + row + "]']").val();
    var country_code = $("input[name='sh_country_code[" + row + "]']").val();
    if (country != null) {
        $('#sh_country').val(country).change();
    }
    $('#sh_recipient').val(name);
    $('#sh_company').val(company);
    $('#sh_email').val(email);
    $('#sh_address1').val(address1);
    $('#sh_address2').val(address2);
    $('#sh_address3').val(address3);
    $('#sh_phone').val(phone);
    //$('#sh_city').val(city);
    $('#sh_postcode').val(zipcode);
    $('#sh_state').val(state);
    $('#sh_suburb').val(suburb);
    $('#sh_city').val(city);

    var dimension = $("input[name^='weight[" + row + "]']");
    var description = $("input[name^='d_description[" + row + "]']");
    $(dimension).each(function (i, e) {
        if (i > 0) {
            addbox();
        }
    });
    var weight = 0;
    $(dimension).each(function (i, e) {
        weight = weight + parseFloat(convert2number($("input[name='weight[" + row + "][" + i + "]']").val()));
        $('.width').eq(i).val($("input[name='width[" + row + "][" + i + "]']").val());
        $('.length').eq(i).val($("input[name='length[" + row + "][" + i + "]']").val());
        $('.height').eq(i).val($("input[name='height[" + row + "][" + i + "]']").val());
    });
    $('#sh_weight').val(weight);
    $('#sh_weight').change();
    $('#box_total').val(dimension.length);
    $(description).each(function (i, e) {
        if (i > 0) {
            adddesc();
        }
        var desc = $("input[name='d_description[" + row + "][" + i + "]']").val();
        var number = $("input[name='d_number[" + row + "][" + i + "]']").val();
        var amount = $("input[name='d_amount[" + row + "][" + i + "]']").val();
        $('.d_description').eq(i).val(desc);
        $('.d_number').eq(i).val(number);
        $('.d_amount').eq(i).val(amount);
        cal_desc();
    });
    var sh_Insurance = $("input[name='sh_Insurance[" + row + "]']").val();
    var sh_ExportDoc = $("input[name='sh_ExportDoc[" + row + "]']").val();
    var sh_Pack = $("input[name='sh_Pack[" + row + "]']").val();
    var sh_Note = $("input[name='sh_Note[" + row + "]']").val();
    if (sh_Insurance == 'yes') {
        $('#Insurance').prop('checked', true);
    }
    if (sh_Pack == 'yes') {
        $('#Pack').prop('checked', true);
    }
    if (sh_ExportDoc == 'yes') {
        $('#ExportDoc').prop('checked', true);
    }
    if (sh_Note != '') {
        $('#customCheck03').prop('checked', true);
        $('#Note').val(sh_Note);
        $('#Note_area').show();
    }

}
// Add Box
function addbox() {
    var box_number = $('.box').length + 1;
    var html = '<div class="row box"><div class="col-12" style="padding-bottom:10px;">ขนาดพัสดุกล่องที่ <span class="box_number">' + box_number + '</span><span class="box_del badge badge-danger" onclick="box_del($(this))">X</span><br></div> <div class="col-4"><input type="number" class="form-control width" style="text-align:center" placeholder="กว้าง (CM)" onchange="cal_box()"></div> <div class="col-4"><input type="number" class="form-control length" style="text-align:center" placeholder="ยาว (CM)" onchange="cal_box()"></div><div class="col-4"><input type="number" class="form-control height" style="text-align:center" placeholder="สูง (CM)" onchange="cal_box()"></div></div>';
    $('#box_area').append(html);
    $('#box_total').val(box_number);
}
function box_del(obj) {
    $(obj).parent().parent().remove();
    $('.box').each(function (i, e) {
        $(e).find(".box_number").html(i + 1);
    });
    $('#box_total').val($('.box').length);
    cal_box();

}
function cal_box() {
    console.log('cal_box');
    var volumnWeight = 0;
    var weight = parseFloat(convert2number($("#sh_weight").val()));

    $('.box').each(function (i, e) {
        var width = parseFloat(convert2number($(e).find(".width").val()));
        var length = parseFloat(convert2number($(e).find(".length").val()));
        var height = parseFloat(convert2number($(e).find(".height").val()));
        var sum = width * length * height / 5000;
        volumnWeight = volumnWeight + sum;
    });
    if (volumnWeight < weight) {
        volumnWeight = weight;
    }
    $('#volumnWeight').val(volumnWeight);
    $('#volumnWeight').change();
    get_service()
}
// Add Description
function adddesc() {
    var desc_number = $('.desc').length;
    var html = '<div class="desc">' +
        '<div class="desc-topic"> รายละเอียดพัสดุ <span class="desc_number">' + (desc_number + 1) + '</span><span class="box_del badge badge-danger" onclick = "desc_del($(this))">X</span></div>' +
        '<div class="col-md-12">ประเภทสิ่งของ(ภาษาอังกฤษเท่านั้น)*</div>' +
        '<div class="col-md-12"><input type="text" class="d_description form-control sh-input" required  onchange="cal_desc()"></div>' +
        '<div class="col-md-12">จำนวน(ชิ้น)*</div>' +
        '<div class="col-md-12"><input type="text" class="d_number form-control sh-input" required onchange="cal_desc()" onkeypress="return isNumberKey(event)"></div>' +
        '<div class="col-md-12">มูลค่ารวม(บาท)*</div>' +
        '<div class="col-md-12"><input type="text" class="d_amount form-control sh-input" required onchange="cal_desc()" onkeypress="return isNumberKey(event)"></div>' +
        '</div>';
    $('#desc_area').append(html);
}
function desc_del(obj) {
    $(obj).parent().parent().remove();
    $('.desc').each(function (i, e) {
        $(e).find(".desc_number").html(i + 1);
    });
    cal_desc();
}
function cal_desc() {
    console.log('cal_box');
    var description = "";
    var amount = 0;
    var number = 0;
    $('.desc').each(function (i, e) {
        var d_description = $(e).find(".d_description").val();
        var d_amount = parseFloat(convert2number($(e).find(".d_amount").val()));
        var d_number = parseFloat(convert2number($(e).find(".d_number").val()));
        description += d_description + ',';
        amount += d_amount;
        number += d_number;
    });
    $('#sh_description').val(description);
    $('#sh_quantity').val(number);
    $('#sh_declared').val(amount);
    $('#sh_declared').change();
    //$('.volumnWeight').html(volumnWeight);
}
function convert2number(value) {
    var result = value;
    if (value === undefined || isNaN(value) || value === "") {
        result = 0;
    }
    return result;
}
function get_service() {
    $('#service_select').html('');
    var sh_country = $('#sh_country option:selected');
    var totalweight = $('#volumnWeight').val();
    var url = '/getprice/' + sh_country.attr('code') + '/' + totalweight;
    if ($('#volumnWeight').val() != '' && $('#volumnWeight').val() != 0) {
        $.ajax({
            type: "GET",
            url: url,
            success: function (result) {
                var data = $.parseJSON(result);
                console.log(data);
                var html = '';
                $(data).each(function (i, e) {
                    e.logo = e.logo != null ? e.logo : 'sme.jpg';
                    if (e.price.result != undefined) {
                        html += '<div class="col-md-12" title="' + e.name + '"><input type="radio" name="service" class="radio_service_item" id="s_' + e.id + '" s_id="' + e.id + '" s_price="' + e.price.result + '" s_logo="' + e.logo + '">' +
                            '<label class="row service_card" for="s_' + e.id + '">' +
                            '<div style="float: left;padding: 10px;">' +
                            '<img src="/uploads/service/' + e.logo + '" alt="user" height="70px"></div>' +
                            '<div  style="font-size:18px;padding-top: 10px;width:70%"><span class="cut-text">' + e.name + ' </span>( ' + e.day.day + ' วันทำการ )<span style="float:right">' + numberWithCommas(e.price.result) + ' ฿</span></div>' +
                            '</label></div>';
                    }

                });
                $('#service_select').html(html);
                var select_service = $('#service_number').val();
                if (select_service != null) {
                    $("#s_" + select_service).prop('checked', true);
                }
            },
            error: function (e) {
                console.log("ERROR : ", e);
            }
        });
        $('#service_select').show();
        $('#service_wait_data').hide();
    }
    else {
        $('#service_select').hide();
        $('#service_wait_data').show();
    }
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function binding() {
    var i = $('.sumary_data').length;
    var edit_mode = $('#edit_number').val();
    if (edit_mode != '') {
        i = edit_mode;
    }
    var service = $("input[name='service']:checked");
    var volumnWeight = $('#volumnWeight').val();
    var sh_description = $('#sh_description').val();
    var sh_quantity = $('#sh_quantity').val();
    var sh_declared = $('#sh_declared').val();
    var sh_recipient = $('#sh_recipient').val();
    var sh_company = $('#sh_company').val();
    var sh_email = $('#sh_email').val();
    var sh_address1 = $('#sh_address1').val();
    var sh_address2 = $('#sh_address2').val();
    var sh_address3 = $('#sh_address3').val();
    var sh_phone = $('#sh_phone').val();
    var sh_country = $('#sh_country option:selected');
    var sh_currency = $('#sh_currency option:selected');
    var sh_city = $('#sh_city').val();
    var sh_state = $('#sh_state').val();
    var sh_postcode = $('#sh_postcode').val();
    var sh_suburb = $('#sh_suburb').val();
    var sh_Insurance = ($('#Insurance').is(':checked') ? 'yes' : 'no');
    var sh_ExportDoc = ($('#ExportDoc').is(':checked') ? 'yes' : 'no');
    var sh_Pack = ($('#Pack').is(':checked') ? 'yes' : 'no');
    var sh_Note = $('#Note').val();
    data = '<input type="hidden" name="sh_type_of_service[' + i + ']" value="' + service.attr('s_id') + '">' +
        //'<input type="hidden" name="sh_currency[' + i + ']" value="USD">' +
        '<input type="hidden" name="sh_declared_value[' + i + ']" value="' + sh_declared + '">' +
        '<input type="hidden" name="sh_description[' + i + ']" value="' + sh_description + '">' +
        '<input type="hidden" name="sh_content_quantity[' + i + ']" value="' + sh_quantity + '">' +
        '<input type="hidden" name="sh_packge_type[' + i + ']" value="PA">' +
        '<input type="hidden" name="sh_recipient[' + i + ']" value="' + sh_recipient + '">' +
        '<input type="hidden" name="sh_company[' + i + ']" value="' + sh_company + '">' +
        '<input type="hidden" name="sh_email[' + i + ']" value="' + sh_email + ' ">' +
        '<input type="hidden" name="sh_address1[' + i + ']" value="' + sh_address1 + '">' +
        '<input type="hidden" name="sh_address2[' + i + ']" value="' + sh_address2 + '">' +
        '<input type="hidden" name="sh_address3[' + i + ']" value="' + sh_address3 + '">' +
        '<input type="hidden" name="sh_phone[' + i + ']" value="' + sh_phone + '">' +
        '<input type="hidden" name="sh_country[' + i + ']" value="' + sh_country.val() + '">' +
        '<input type="hidden" name="sh_currency[' + i + ']" value="' + sh_currency.val() + '">' +
        '<input type="hidden" name="sh_country_code[' + i + ']" value="' + sh_country.attr('code') + '">' +
        '<input type="hidden" name="sh_city[' + i + ']" value="' + sh_city + '">' +
        '<input type="hidden" name="sh_suburb[' + i + ']" value="' + sh_suburb + '">' +
        '<input type="hidden" name="sh_state[' + i + ']" value="' + sh_state + '">' +
        '<input type="hidden" name="sh_postcode[' + i + ']" value="' + sh_postcode + '">' +

        '<input type="hidden" name="sh_Insurance[' + i + ']" value="' + sh_Insurance + '">' +
        '<input type="hidden" name="sh_ExportDoc[' + i + ']" value="' + sh_ExportDoc + '">' +
        '<input type="hidden" name="sh_Pack[' + i + ']" value="' + sh_Pack + '">' +
        '<input type="hidden" name="sh_Note[' + i + ']" value="' + sh_Note + '">' +

        '<input type="hidden" name="total_regular_item[' + i + ']" value="' + service.attr('s_price') + '">';


    var box_total = $('#box_total').val();
    var weight = $('#sh_weight').val() / box_total;
    var box_auto_create = box_total - $('.box').length;
    $('.box').each(function (a, e) {
        var width = parseFloat(convert2number($(e).find(".width").val()));
        var length = parseFloat(convert2number($(e).find(".length").val()));
        var height = parseFloat(convert2number($(e).find(".height").val()));
        data += '<input type="hidden" name="series[' + i + '][' + a + ']" value="' + (a + 1) + '">' +
            '<input type="hidden" name="weight[' + i + '][' + a + ']" value="' + weight + '">' +
            '<input type="hidden" name="width[' + i + '][' + a + ']" value="' + width + '">' +
            '<input type="hidden" name="length[' + i + '][' + a + ']" value="' + length + '">' +
            '<input type="hidden" name="height[' + i + '][' + a + ']" value="' + height + '">';
    });
    if (box_auto_create > 0) {
        var a = $('.box').length;
        var z;
        for (z = 0; z < box_auto_create; z++) {
            data += '<input type="hidden" name="series[' + i + '][' + (a + z) + ']" value="' + ((a + z) + 1) + '">' +
                '<input type="hidden" name="weight[' + i + '][' + (a + z) + ']" value="' + weight + '">' +
                '<input type="hidden" name="width[' + i + '][' + (a + z) + ']" value="0">' +
                '<input type="hidden" name="length[' + i + '][' + (a + z) + ']" value="0">' +
                '<input type="hidden" name="height[' + i + '][' + (a + z) + ']" value="0">';
        }
    }
    $('.desc').each(function (a, e) {
        var d_description = $(e).find(".d_description").val();
        var d_amount = parseFloat(convert2number($(e).find(".d_amount").val()));
        var d_number = parseFloat(convert2number($(e).find(".d_number").val()));
        data += '<input type="hidden" name="d_description[' + i + '][' + a + ']" value="' + d_description + '">' +
            '<input type="hidden" name="d_amount[' + i + '][' + a + ']" value="' + d_amount + '">' +
            '<input type="hidden" name="d_number[' + i + '][' + a + ']" value="' + d_number + '">';
    });
    data += '<input type="hidden" name="total_weight_item[' + i + ']" value="' + volumnWeight + '">';
    return data;
}

function addshipping() {
    $('#form_step_2').trigger("reset");
    $('#form_step_3').trigger("reset");
    $('#service_wait_data').show();
    $('#service_select').hide();
    $('#sh_suburb').val('');
    $('#sh_suburb').hide();
    $('.box_del').each(function (i, e) {
        $(e).click();
    });
    $('#Insurance').prop('checked', false);
    $('#ExportDoc').prop('checked', false);
    $('#Pack').prop('checked', false);
    $('#customCheck03').prop('checked', false);
    $('#Note').val('');
    $('#Note_area').hide();

    $('#edit_number').val('');
    $('#service_number').val('');
    nextstep(2);
}


$.ajax({
    type: "GET",
    url: '/admin/getaddress_thailand',
    success: function (result) {
        var province = JSON.parse(result);
        $(province).each(function (index, i) {
            $('#province').append('<option value="' + i.PROVINCE_ID + '">' + i.PROVINCE_NAME + '</option>');
            $('#a_province').append('<option value="' + i.PROVINCE_ID + '">' + i.PROVINCE_NAME + '</option>');
        });
    },
    error: function (e) {
        console.log("ERROR : ", e);
        $('#loading').hide();
    }
});


$('#province').change(function (e) {
    var provinceid = $('#province').val();
    $('#amphur').html('');
    $.ajax({
        type: "GET",
        url: '/admin/getaddress_thailand/' + provinceid,
        success: function (result) {
            var amphur = JSON.parse(result);
            $(amphur).each(function (index, i) {
                $('#amphur').append('<option value="' + i.id + '" postcode="' + i.postcode + '">' + i.name + '</option>');
            });
        },
        error: function (e) {
            console.log("ERROR : ", e);
            $('#loading').hide();
        }
    });
});
$('#amphur').change(function (e) {
    var provinceid = $('#province').val();
    var amphurid = $('#amphur').val();
    $('#postcode').val($('#amphur option:selected').attr('postcode'));
    $('#tumbon').html('');
    $.ajax({
        type: "GET",
        url: '/admin/getaddress_thailand/' + provinceid + '/' + amphurid,
        success: function (result) {
            var tumbon = JSON.parse(result);
            $(tumbon).each(function (index, i) {
                $('#tumbon').append('<option value="' + i.id + '">' + i.name + '</option>');
            });
        },
        error: function (e) {
            console.log("ERROR : ", e);
            $('#loading').hide();
        }
    });
});

$('#a_province').change(function (e) {
    var provinceid = $('#a_province').val();
    $('#a_amphur').html('');
    $.ajax({
        type: "GET",
        url: '/admin/getaddress_thailand/' + provinceid,
        success: function (result) {
            var amphur = JSON.parse(result);
            $(amphur).each(function (index, i) {
                $('#a_amphur').append('<option value="' + i.id + '" postcode="' + i.postcode + '">' + i.name + '</option>');
            });
        },
        error: function (e) {
            console.log("ERROR : ", e);
            $('#loading').hide();
        }
    });
});
function set_sender(id, name, address, tumbon, amphur, province, postcode, phone, email) {
    $('.modal').modal('hide');
    $('#addr_name').val(name);
    $('#addr_phone').val(phone);
    $('#addr_address').val(address);
    $('#province').val(province);
    $('#amphur').val(amphur);
    $('#tumbon').val(tumbon);
    $('#postcode').val(postcode);
    $('#address_id').val(id);
}
function set_recipient(id) {
    $('.modal').modal('hide');
    var name = $('#re_' + id).attr('name');
    var company = $('#re_' + id).attr('company');
    var email = $('#re_' + id).attr('email');
    var address1 = $('#re_' + id).attr('address1');
    var address2 = $('#re_' + id).attr('address2');
    var address3 = $('#re_' + id).attr('address3');
    var phone = $('#re_' + id).attr('phone');
    var city = $('#re_' + id).attr('city');
    var zipcode = $('#re_' + id).attr('zipcode');
    var state = $('#re_' + id).attr('state');
    var suburb = $('#re_' + id).attr('suburb');
    var country = $('#re_' + id).attr('country');
    var country_code = $('#re_' + id).attr('country_code');
    if (country != null) {
        $('#sh_country').val(country).change();
    }
    $('#sh_recipient').val(name);
    $('#sh_company').val(company);
    $('#sh_email').val(email);
    $('#sh_address1').val(address1);
    $('#sh_address2').val(address2);
    $('#sh_address3').val(address3);
    $('#sh_phone').val(phone);
    //$('#sh_city').val(city);
    $('#sh_postcode').val(zipcode);
    $('#sh_state').val(state);
    $('#sh_suburb').val(suburb);
    $('#sh_city').val(city);
}

$('#recipient_list').click(function () {
    $('#find_recipient').val('');
    $("#loading").show();
    // var country = $('#sh_country option:selected').attr('code');
    // var country = $('#sh_country option:selected').attr('code');
    var country = 'all';
    $('#recipient_area').html('');
    $.ajax({
        type: "GET",
        url: '/app/recipient/' + country,
        success: function (result) {
            var data = JSON.parse(result);
            $(data).each(function (index, i) {
                $('#recipient_area').append('<tr onclick="set_recipient(' + (index + 1) + ')" style="cursor:pointer">' +
                    '<td id="re_' + (index + 1) + '" name="' + (i.recipient != null ? i.recipient : '') + '" company="' + (i.company != null ? i.company : '') + '" email="' + (i.email != null ? i.email : '') + '" address1="' + (i.address != null ? i.address : '') + '" address2="' + (i.address2 != null ? i.address2 : '') + '" address3="' + (i.address3 != null ? i.address3 : '') + '" phone="' + (i.phone != null ? i.phone : '') + '" city="' + (i.city != null ? i.city : '') + '" suburb="' + (i.suburb != null ? i.suburb : '') + '" zipcode="' + (i.zipcode != null ? i.zipcode : '') + '" state="' + (i.state != null ? i.state : '') + '" country_code="' + (i.country_code != null ? i.country_code : '') + '" country="' + (i.country != null ? i.country : '') + '">' +
                    i.recipient + '</td>' +
                    '<td>' + i.country + '</td > ' +
                    '<td>' + i.phone + '</td>' +
                    '</tr>');
            });
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });
    $('#loading').hide();
});

$('#find_recipient').keyup(function () {
    $("#loading").show();
    // var country = $('#sh_country option:selected').attr('code');
    var country = 'all';
    var keyword = $('#find_recipient').val();
    $.ajax({
        type: "GET",
        url: '/app/recipient/' + country + '/' + keyword,
        success: function (result) {
            var data = JSON.parse(result);
            $('#recipient_area').html('');
            $(data).each(function (index, i) {
                $('#recipient_area').append('<tr onclick="set_recipient(' + (index + 1) + ')" style="cursor:pointer">' +
                    '<th id="re_' + (index + 1) + '" name="' + (i.recipient != null ? i.recipient : '') + '" company="' + (i.company != null ? i.company : '') + '" email="' + (i.email != null ? i.email : '') + '" address1="' + (i.address != null ? i.address : '') + '" address2="' + (i.address2 != null ? i.address2 : '') + '" address3="' + (i.address3 != null ? i.address3 : '') + '" phone="' + (i.phone != null ? i.phone : '') + '" city="' + (i.city != null ? i.city : '') + '" zipcode="' + (i.zipcode != null ? i.zipcode : '') + '" state="' + (i.state != null ? i.state : '') + '">' +
                    '' + (index + 1) + '</th>' +
                    '<td>' + i.recipient + '</td>' +
                    '<td>' + i.country + '</td > ' +
                    '<td>' + i.phone + '</td>' +
                    '</tr>');
            });
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });
    $('#loading').hide();
});

$('#customCheck03').change(function () {
    if ($('#customCheck03').is(':checked')) {
        $('#Note_area').show();
    }
    else {
        $('#Note_area').hide();
    }
});

