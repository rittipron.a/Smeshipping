var data = null;
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });
    //------------ ดึงข้อมูลลูกค้า
    $("#search_customer").keypress(function (e) {
        $("#loading").show();
        $("#edit_customer_save").prop("disabled", true);
        var keywork = $("#search_customer").val();
        $.ajax({
            type: "GET",
            url: "/admin/getCustomer/" + keywork,
            success: function (result) {
                $("#m_id").val("");
                $("#customer_select tbody").html("");
                $("#loading").hide();
                $("#customer_detail").hide();
                data = JSON.parse(result);
                $(data).each(function (index, i) {
                    $("#customer_select").show();

                    $("#customer_select tbody").append(
                        '<tr onclick="setcustomer(' +
                        index +
                        ')" style="cursor: pointer;"><td>ชื่อ: ' +
                        i.firstname +
                        " " +
                        i.lastname +
                        " </td><td> Tel: " +
                        i.phone +
                        " </td><td>ที่อยู่: " +
                        i.address +
                        "</td></tr>"
                    );
                });
            },
            error: function (e) {
                console.log("ERROR : ", e);
                $("#loading").hide();
            }
        });
    });
    //--------- submit
    $("#booking").submit(function (e) {
        $("#loading").show();
        e.preventDefault();
        var thisform = $(this);
        var url = thisform.attr("action");
        var cus = $('.customer_data')
        var cus_id = cus.attr('cus_id');
        var cus_status = cus.attr('cus_status');
        if (cus_id == 9999999 && cus_status == 4) {
            alert('ข้อมูลของลูกค้าท่านนี้เป็นข้อมูลชั่วคราว โปรดนำเนินการแก้ไขให้เรียบร้อย!');
        }
        $.ajax({
            type: "PUT",
            data: $("#booking").serialize(),
            url: url,
            success: function (result) {
                $("#loading").hide();
                window.location.reload();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });

    });

    $("#form_edit_delivery").submit(function (e) {
        $("#loading").show();
        e.preventDefault();
        var thisform = $(this);
        var url = thisform.attr("action");

        $.ajax({
            type: "POST",
            data: $("#form_edit_delivery").serialize(),
            url: url,
            success: function (result) {
                $("#loading").hide();
                location.reload();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    });
    $("#form_edit_customer").submit(function (e) {
        $("#loading").show();
        e.preventDefault();
        var thisform = $(this);
        var url = thisform.attr("action");

        $.ajax({
            type: "POST",
            data: $("#form_edit_customer").serialize(),
            url: url,
            success: function (result) {
                $("#loading").hide();
                location.reload();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    });

    $("#hold").click(function (e) {
        $("#loading").show();
        var remark = $("#comment").val();
        var url = $(this).attr('action') + '/' + remark;
        $.ajax({
            type: "GET",
            url: url,
            success: function (result) {
                $("#loading").hide();
                location.reload();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    });
    $("#save").click(function (e) {
        $("#loading").show();
        var remark = $("#comment").val();
        var url = $(this).attr('action') + (remark != '' ? '/' + remark : '');
        $.ajax({
            type: "POST",
            data: $("#booking").serialize(),
            url: url,
            dataType: 'text',
            success: function (result) {
                $("#loading").hide();
                location.reload();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    });
    $("#complete").click(function (e) {
        $("#loading").show();
        var remark = $("#comment").val();
        var url = $(this).attr('action') + (remark != '' ? '/' + remark : '');
        $.ajax({
            type: "POST",
            data: $("#booking").serialize(),
            url: url,
            dataType: 'text',
            success: function (result) {
                $("#loading").hide();
                location.reload();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    });
    $("#cancel").click(function (e) {
        $("#loading").show();
        var remark = $("#comment").val();
        var url = $(this).attr('action') + '/' + remark;
        $.ajax({
            type: "GET",
            url: url,
            success: function (result) {
                $("#loading").hide();
                location.reload();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    });
    $("input[type=radio][name=time]").change(function () {
        if (this.value == "พร้อมรับ") {
            $("#delivery_time").prop("disabled", true);
        } else {
            $("#delivery_time").prop("disabled", false);
        }
    });
});
function setcustomer(index) {
    $("#customer_select").hide();
    $("#customer_detail").show();
    $("#edit_customer_save").prop("disabled", false);
    $(data).each(function (i, res) {
        if (index == i) {
            var address =
                (res.address != null ? res.address : "") +
                (res.tumbon != null ? ", " + res.tumbon : "") +
                (res.amphur != null ? ", " + res.amphur : "") +
                (res.province != null ? ", " + res.province : "") +
                (res.postcode != null ? " " + res.postcode : "");
            $("#m_id").val(res.id != null ? res.id : "");
            $("#address").val(address);
            $("#customer_name_show").text(
                (res.firstname != null ? res.firstname : "") +
                " " +
                (res.lastname != null ? res.lastname : "")
            );
            $("#customer_company_show").text(
                res.company != null ? res.company : "-"
            );
            $("#customer_Line_show").text(
                res.line != null ? res.line : "-"
            );
            $("#customer_phone_show").text(res.phone != null ? res.phone : "-");
            $("#customer_email_show").text(res.email != null ? res.email : "");
        }
    });
} //-------------- ห้ามใส่ตัวอักษร
function isNumberKey(evt) {
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function isNumberOnly(evt) {
        var vchar = String.fromCharCode(event.keyCode);
        if ((vchar<'0' || vchar>'9') && (vchar == '.' || vchar == '-')) 
        return false;
        evt.onKeyPress=vchar;
        }

$("#m_default").addClass("active");
$("#MetricaCRM").addClass("active");


$("#sh_city").on('keyup', function (e) {
    var option = $('#select_city option').filter(function () {
        return this.value === $("#sh_city").val();
    }).val();

    if (option) {
        $('#sh_city').removeClass('is-invalid');
        $('#sh_city').addClass('is-valid');
    } else {
        $('#sh_city').addClass('is-invalid');
    }
});


function dhl_ecommerce_toggle() {
    var shipment = $('#shipment_detail tbody tr');
    if ($('#airmail_off').is(":hidden")) {
        $(shipment).each(function (i, e) {
            var text = $(e).find("td").eq(0);
            $(text).html(i + 1);
        });
    }
    else {
        $(shipment).each(function (i, e) {
            var awb = $(e).find("td").eq(1).find(".sh_awb").attr('set');
            var text = $(e).find("td").eq(0);
            var id = $(e).find("td").eq(1).find(".sh_id").attr('set');
            if (awb == "") {
                $(text).html('<input type="checkbox" class="check_sh" value="' + id + '" checked />');
            }

        });
    }

    $('#airmail_off').toggle();
    $('#airmail_on').toggle();
}

function genarate_dhl_ecommerce() {
    $("#loading").show();
    var shipment = $('.check_sh');
    var str = "";
    $(shipment).each(function (i, e) {
        if ($(e).is(":checked")) {
            str += (i > 0 ? ',' : '') + $(e).val();
        }
    });

    var url = '/create_dhlecommerce_awb' + '/' + str;
    $.ajax({
        type: "GET",
        url: url,
        success: function (result) {
            $("#loading").hide();
            var res = result;
            console.log(res);
            if (res.hasOwnProperty("status")) {
                location.reload();
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
            alert(
                "An error occurred on the server when processing the URL. Please contact the system administrator."
            );
            $("#loading").hide();
        }
    });
}
