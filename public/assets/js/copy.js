function copy() {
    var addr_name = $(".addr_name").val();
    var addr_phone = $(".addr_phone").val();
    var addr_address = $(".addr_address").val();
    var email = $(".email").val() == "" ? "-" : $(".email").val();

    var sh_weight = $('.total_weight_item').val() != "" ? $('.total_weight_item').val() : "-";
    var sh_service = $('.sh_service').val() != "" ? $('.sh_service').val() : "-";
    var sh_quantity = $('.sh_content_quantity').val() == "" ? "-" : $('.sh_content_quantity').val();
    var sh_declared = $('.sh_declared_value').val() == "" ? "-" : $('.sh_declared_value').val();
    var sh_description = $('.sh_description').val() == "" ? "-" : $('.sh_description').val();
    var sh_recipient = $('.sh_recipient').val() == "" ? "-" : $('.sh_recipient').val();
    var sh_email = $('.sh_email').val() == "" ? "-" : $('.sh_email').val();
    var sh_address1 = $('.sh_address1').val() == "" ? "-" : $('.sh_address1').val();
    var sh_phone = $('.sh_phone').val() == "" ? "-" : $('.sh_phone').val();
    var sh_country = $('.sh_country').val() == "" ? "-" : $('.sh_country').val();
    var sh_postcode = $('.sh_postcode').val() == "" ? "-" : $('.sh_postcode').val();
    var total_regular_item = $('.total_regular_item').val() == "" ? "-" : $('.total_regular_item').val();

    var sh_content_quantity = $('.sh_content_quantity').val() != "" ? $('.sh_content_quantity').val() : "-";
    var sh_address3 = $('.sh_address3').val() != "" ? $('.sh_address3').val() : "-";
    var sh_address2 = $('.sh_address2').val() != "" ? $('.sh_address2').val() : "-";
    var sh_suburb = $('.sh_suburb').val() != "" ? $('.sh_suburb').val() : "-";
    var sh_Note = $('.sh_Note').val() != "" ? $('.sh_Note').val() : "-";

    $("#sr_name").text(addr_name);
    $("#sr_addr_adderss").text(addr_address);
    $("#sr_phone").text(addr_phone);
    $("#sr_recipient").text(sh_recipient);
    $("#sr_sh_address").text(sh_address1);
    $("#sr_country").text(sh_country);
    $("#sr_postcode").text(sh_postcode);
    $("#sr_sent_phone").text(sh_phone);
    $("#sr_email").text(sh_email);
    $("#sr_weight").text(sh_weight);
    $("#t_country").text(sh_country);
    $("#sr_service").text(sh_service);
    $("#total_service").text(total_regular_item);
    $("#price_service").text(total_regular_item);
    $("#totle_pirce").text(total_regular_item);
    $("#sr_product_baht").text(sh_declared);
    $("#sr_product_quality").text(sh_quantity);
    $("#sr_product_detail").text(sh_description);

    $(".sh_content_quantity").val(sh_content_quantity);
    $(".sh_address3").val(sh_address3);
    $(".sh_address2").val(sh_address2);
    $(".sh_suburb").val(sh_suburb);
    $(".sh_Note").val(sh_Note);
    $(".sh_email").val(sh_email);

    $("#email").val(email);

}
copy();