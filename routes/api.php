<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API')->group(function () {
    Route::post('/login', 'AuthController@login');
    Route::post('/logout', 'AuthController@login');

    Route::middleware('auth:api')->group(function () {
        Route::get('logout', 'AuthController@logout');

        Route::namespace('v1')->prefix('v1')->group(function () {
            Route::get('list/{model}', 'CRUDController@list');
            Route::post('list/{model}', 'CRUDController@list');
            Route::post('create/{model}', 'CRUDController@create');
            Route::get('read/{model}/{id}', 'CRUDController@read');
            Route::post('read/{model}/{id}', 'CRUDController@read');
            Route::put('update/{model}/{id}', 'CRUDController@update');
            Route::delete('delete/{model}/{id}', 'CRUDController@delete');
        });
    });
});
