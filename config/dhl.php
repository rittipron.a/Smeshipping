<?php

return [
    'mode' => env('FEDEX_API_MODE', 'development'),
    'key' => (env('FEDEX_API_MODE') == 'development') ? env('FEDEX_API_DEVELOPMENT_KEY') : env('FEDEX_API_PRODUCTION_KEY'),
    'password' => (env('FEDEX_API_MODE') == 'development') ? env('FEDEX_API_DEVELOPMENT_PASSWORD') : env('FEDEX_API_PRODUCTION_PASSWORD'),
    'account_number' => (env('FEDEX_API_MODE') == 'development') ? env('FEDEX_API_DEVELOPMENT_ACCOUNT_NUMBER') : env('FEDEX_API_PRODUCTION_ACCOUNT_NUMBER'),
    'meter_number' => (env('FEDEX_API_MODE') == 'development') ? env('FEDEX_API_DEVELOPMENT_METER_NUMBER') : env('FEDEX_API_PRODUCTION_METER_NUMBER'),
];
