<?php

use Illuminate\Database\Seeder;

class EMSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(27533000, 27537999) as $number) {
            $sum = 0;
            $sum += (int)substr($number, 0, 1) * 8;
            $sum += (int)substr($number, 1, 1) * 6;
            $sum += (int)substr($number, 2, 1) * 4;
            $sum += (int)substr($number, 3, 1) * 2;
            $sum += (int)substr($number, 4, 1) * 3;
            $sum += (int)substr($number, 5, 1) * 5;
            $sum += (int)substr($number, 6, 1) * 9;
            $sum += (int)substr($number, 7, 1) * 7;
            $sum = $sum % 11;

            switch ($sum) {
                case 0:
                    $sum = 5;
                    break;
                case 1:
                    $sum = 0;
                    break;
                default:
                    $sum = 11 - $sum;
                    break;
            }

            \App\ThaiPostEMSNumber::query()->create([
                'number' => "EA" . $number . $sum . 'TH',
            ]);
        }
    }
}
