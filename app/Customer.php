<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
 protected $table = 'app_customer';

 protected $primaryKey = 'id';

 public function addresses() {
     return $this->hasMany(CustomerAddress::class, 'customer_id', 'id');
 }
}
