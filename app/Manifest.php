<?php

namespace App;

use Cassandra\Custom;
use eloquentFilter\QueryFilter\ModelFilters\Filterable;
use Illuminate\Database\Eloquent\Model;

class Manifest extends Model
{
    use Filterable;

    private static $whiteListFilter = ['*'];

    protected $table = 'app_manifest';

    public function details()
    {
        return $this->hasMany(ManifestDetail::class, 'inv_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'm_id', 'id');
    }

    public function pickup()
    {
        return $this->hasOne(Pickup::class, 'id', 'p_id');
    }
}
