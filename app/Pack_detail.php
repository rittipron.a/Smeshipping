<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pack_detail extends Model
{
    protected $table = 'pack_detail';

    protected $primaryKey = 'id';
}
