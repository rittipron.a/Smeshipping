<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppBoxCheckin extends Model
{
    protected $table = 'app_box_checkin';

    protected $primaryKey = 'id';
}
