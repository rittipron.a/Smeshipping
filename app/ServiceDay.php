<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceDay extends Model
{
    protected $table = 'app_services_day';

    protected $primaryKey = 'id';
}
