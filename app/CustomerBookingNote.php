<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerBookingNote extends Model
{
    protected $table = 'customer_booking_note';

    protected $primaryKey = 'id';
}
