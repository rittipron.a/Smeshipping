<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'app_country';

    protected $primaryKey = 'id';
}
