<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentAttachmentFromCustomer extends Model
{
    protected $table = 'payment_attachment_from_customer';

    protected $primaryKey = 'id';
}
