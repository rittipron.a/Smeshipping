<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
 protected $table = 'app_services';

 protected $primaryKey = 'id';

}
