<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingLogEvent extends Model
{
    protected $table = 'tracking_log_events';

    protected $fillable = [
        'status_update', 'comment', 'op', 'awb'
    ];

    public function operator()
    {
        return $this->belongsTo(User::class, 'op', 'id');
    }
}
