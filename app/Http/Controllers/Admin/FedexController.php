<?php

namespace App\Http\Controllers\admin;

use App\Classes\API\FedEx;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
use Illuminate\Support\Facades\DB;
use App\ManifestDetail_box;
use App\ManifestDetail_Other_Service;
use App\ManifestDetail;
use App\Manifest;
use App\Customer;
use App\Pickup;
use App\ManifestHistory;
use Illuminate\Support\Collection;
use App\ServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FedexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create_awb($id)
    {
        $manifest_detail = ManifestDetail::where('id', $id)->first();
        if ($manifest_detail->awb != null) {
            throw new Exception('awb is not null.');
        }
        $manifest = Manifest::where('id', $manifest_detail->inv_id)->first();
        $box = ManifestDetail_box::where('shipment_id', $id)->where('status', '<', 10)->get();
        $sender = Customer::where('id', $manifest_detail->m_id)->first();
        $service = Service::where('id', $manifest_detail->services)->first();
        $servicetype = ServiceType::where('id', $service->cat)->first();
        $insurance = ManifestDetail_Other_Service::where('shipment_id', $id)->where('other_service_name', 'Insurance')->first();
        $checkPLT = DB::table('validate_country_plt')->where('CountryCode', $manifest_detail->country_code)->first();
        $insurance_price = 0;
        if ($manifest_detail->insurance != null) {
            $insurance_price = $manifest_detail->insurance;
        }
        $statename = '';
        if ($manifest_detail->country_code == 'US') {
            $statename = DB::table('validate_address')->where('country_cd', 'US')->where('state_code', $manifest_detail->state)->first();
        }
        error_reporting(0);
        $packingService = "";
        switch ($manifest_detail->packge_type) {
            case "PAK":
                $packingService = 'FEDEX_PAK';
                break;
            case "ENV":
                $packingService = 'FEDEX_ENVELOPE';
                break;
            default:
                $packingService = 'YOUR_PACKAGING';
                break;
        }
        $json = '{
        "SME_SO": "' . $manifest_detail->so . '",
        "RequestedShipment":{
           "DropoffType":"REGULAR_PICKUP",
           "ServiceType":"INTERNATIONAL_PRIORITY",
           "PackagingType":"' . $packingService . '",
           "CustomerSpecifiedDetail":{
              "MaskedData":"SHIPPER_ACCOUNT_NUMBER"
           },
           "PackageCount":' . count($box) . ',
           "CustomerReferences":{
              "0":{
                 "CustomerReferenceType":"CUSTOMER_REFERENCE",
                 "Value":"SME_SHIPPING_TRACKING_' . $manifest_detail->tracking_code . '"
              }
           }
        },
        "Contact":{
           "PersonName":"C/O ' . substr(($manifest_detail->co != null ? $manifest_detail->co : $sender->firstname), 0, 31) . '",
           "CompanyName":"SME SHIPPING C/O ' . substr(($manifest_detail->shipper_company != null ? $manifest_detail->shipper_company : ($manifest_detail->co != null ? $manifest_detail->co : $sender->firstname)), 0, 42) . '",
           "PhoneNumber":"' . ($manifest_detail->shipper_phone != null ? $manifest_detail->shipper_phone : '+6621057777') . '",
           "EmailAddress":"service@smeshipping.com"
        },
        "Address":{
           "StreetLines":[
              "' . ($manifest_detail->shipper_address1 != null ? $manifest_detail->shipper_address1 : '46 RATCHADAPISEK 16') . '",
              "' . ($manifest_detail->shipper_address2 != null ? $manifest_detail->shipper_address2 : 'RATCHADAPISEK ROAD THAPRA BANGKOK-Y') . '"
           ],
           "City":"' . ($manifest_detail->shipper_city != null ? $manifest_detail->shipper_city : 'BANGKOK') . '",
           "StateOrProvinceCode":"' . ($manifest_detail->shipper_city != null ? $manifest_detail->shipper_city : 'BANGKOK') . '",
           "PostalCode":"' . ($manifest_detail->shipper_postalcode != null ? $manifest_detail->shipper_postalcode : '10600') . '",
           "CountryCode":"TH"
        },
        "ContactRecipient":{
           "PersonName":"' . self::replacestring($manifest_detail->recipient) . '",
           "CompanyName":"' . self::replacestring($manifest_detail->company != '' ? $manifest_detail->company : $manifest_detail->recipient) . '",
           "PhoneNumber":"' . $manifest_detail->phone . '"
        },
        "AddressRecipient":{
           "StreetLines":[
              "' . self::replacestring($manifest_detail->address) . '",
              "' . self::replacestring($manifest_detail->address2) . '",
              "' . self::replacestring($manifest_detail->address3) . '"
           ],
           "City":"' . $manifest_detail->city . '",
           "StateOrProvinceCode":"' . $manifest_detail->state . '",
           "PostalCode":"' . $manifest_detail->zipcode . '",
           "CountryCode":"' . $manifest_detail->country_code . '",
           "Residential": "false"
        },
        "shippingChargesPayment":{
           "PaymentType":"SENDER",
           "Payor":{
              "ResponsibleParty":{
                 "AccountNumber":"billaccount",
                 "Contact":null,
                 "Address":{
                    "CountryCode":"TH"
                 }
              }
           }
        },
        "labelSpecification":{
           "LabelFormatType":"COMMON2D",
           "ImageType":"PDF",
           "LabelStockType":"STOCK_4X6"
        },
        "specialServices":{
           "SpecialServiceTypes":[
              "COD"
           ],
           "CodDetail":{
              "CodCollectionAmount":{
                 "Currency":"' . $manifest_detail->currency . '",
                 "Amount":0
              },
              "CollectionType":"ANY"
           }
        },
        "customerClearanceDetail":{
           "DutiesPayment":{
              "PaymentType":"RECIPIENT",
              "Payor":{
                 "ResponsibleParty":{
                    "AccountNumber":"dutyaccount",
                    "Contact":null,
                    "Address":{
                       "CountryCode":"TH"
                    }
                 }
              }
           },
           "DocumentContent":"NON_DOCUMENTS",
           "CustomsValue":{
              "Currency":"' . $manifest_detail->currency . '",
              "Amount":' . number_format($manifest_detail->declared_value, 2) . '
           },
           "Commodities":{';
        foreach ($box as $index => $boxitem) {
            $json = $json . ($index != 0 ? ',' : '') . '"' . $index . '":{
                    "NumberOfPieces":' . ($index + 1) . ',
                    "Description":"' . ($manifest_detail->description != null ? $manifest_detail->description : '-') . '",
                    "CountryOfManufacture":"TH",
                    "Weight":{
                       "Units":"KG",
                       "Value":' . number_format((float)$boxitem->weight, 2, '.', '') . '
                    },
                    "Quantity":1,
                    "QuantityUnits":"EA",
                    "UnitPrice":{
                       "Currency":"' . $manifest_detail->currency . '",
                       "Amount":' . $manifest_detail->declared_value / count($box) . '
                    },
                    "CustomsValue":{
                       "Currency":"' . $manifest_detail->currency . '",
                       "Amount":' . number_format($manifest_detail->declared_value / count($box), 2) . '
                    }
                 }';
        }

        $json = $json . '},
           "ExportDetail":{
              "B13AFilingOption":"NOT_REQUIRED"
           }
        },
        "packageLineItem":{
           "SequenceNumber":1,
           "GroupPackageCount":1,
           "Weight":{
              "Value":' . $manifest_detail->weight . ',
              "Units":"KG"
           },
           "InsuredValue":{
              "Currency":"' . $manifest_detail->currency . '",
              "Amount":' . $insurance_price . '
           },
           "Dimensions":{
              "Length":' . intval($boxitem->length) . ',
              "Width":' . intval($boxitem->width) . ',
              "Height":' . intval($boxitem->height) . ',
              "Units":"CM"
           }
        }
     }';
        //----------------send api

        $s = new FedEx();
        $array_data = $s->processShipment($json)->post()->toArray();

//   $curl = curl_init();

//    curl_setopt_array($curl, array(
//    CURLOPT_URL => "https://backend.smeshipping.com/fedex/International/ShipWebServiceClient.php",
//    CURLOPT_RETURNTRANSFER => true,
//    CURLOPT_ENCODING => "",
//    CURLOPT_MAXREDIRS => 10,
//    CURLOPT_TIMEOUT => 0,
//    CURLOPT_FOLLOWLOCATION => true,
//    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//    CURLOPT_CUSTOMREQUEST => "POST",
//    CURLOPT_POSTFIELDS =>$json,
//    CURLOPT_HTTPHEADER => array(
//        "Content-Type: application/json"
//    ),
//    ));

//    $data = curl_exec($curl);
//    curl_close($curl);
        //----------------stop api
//   $clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $data);
//   $xml = simplexml_load_string($clean_xml);
//   $array_data = json_decode(json_encode($xml), true);
//     dd($s);
        $result = "error";
        if (!empty($array_data["SOAP-ENV_Body"]["ProcessShipmentReply"]["CompletedShipmentDetail"]["MasterTrackingId"]["TrackingNumber"])) {

            Storage::disk('public_uploads_label')->put("Fedex_SME_SHIPPING_TRACKING_" . $manifest_detail->tracking_code . ".pdf", base64_decode($array_data["SOAP-ENV_Body"]["ProcessShipmentReply"]["CompletedShipmentDetail"]["CompletedPackageDetails"]["Label"]["Parts"]["Image"]));

            $manifest_detail->awb_file = "Fedex_SME_SHIPPING_TRACKING_" . $manifest_detail->tracking_code . ".pdf";
            $manifest_detail->awb = $array_data["SOAP-ENV_Body"]["ProcessShipmentReply"]["CompletedShipmentDetail"]["MasterTrackingId"]["TrackingNumber"];
            $manifest_detail->save();
            $result = $array_data["SOAP-ENV_Body"]["ProcessShipmentReply"]["CompletedShipmentDetail"]["MasterTrackingId"]["TrackingNumber"];

            $history = new ManifestHistory;
            $history->topic = 'Generate Fedex Label';
            $history->detail = Auth::user()->name . '/' . ($manifest_detail->so != null ? $manifest_detail->so : $manifest_detail->tracking_code) . ' /' . $manifest_detail->awb;
            $history->update_by = Auth::user()->id;
            $history->inv_id = $manifest_detail->inv_id;
            $history->save();
        } else {
            $history = new ManifestHistory;
            $history->topic = 'Generate Fedex Label';
            $history->detail = 'สร้าง Label ไม่สำเร็จ โปรดติดต่อผู้ดูแลระบบ';
            $history->update_by = Auth::user()->id;
            $history->inv_id = $manifest_detail->inv_id;
            $history->save();
        }
        return $result;
    }

    public function replacestring($string)
    {
        $string = str_replace('&', '&amp;', $string);
        $string = str_replace('<', '&lt;', $string);
        $string = str_replace('>', '&gt;', $string);
        return $string;
    }
}
