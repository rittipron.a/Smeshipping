<?php

namespace App\Http\Controllers\admin;

use App\Employee;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
 public function __construct()
 {
  $this->middleware('auth');
  $this->middleware('role:role_employee');
 }

 public function index(Request $request)
 {

  $employee = Employee::where('active', 1)->where('name', 'like', '%' . $request->keyword . '%')->paginate(50);

  return view('admin/employee/index', compact('employee'));
  //return $request->keyword;
 }
 public function create()
 {
  return "";
 }
 public function store(Request $request)
 {
  $result   = '0';
  $check_em = Employee::where('email', $request->email)->count();
  if ($check_em == 0) {
   $employee        = new Employee;
   $employee->name  = $request->name;
   $employee->email = $request->email;
   $employee->tel = $request->tel;
   if ($request->password != '*****' && $request->password != '') {
    $employee->password = Hash::make($request->password);
   }
   $employee->position = $request->position;
   $employee->active   = $request->active == 'on' ? 1 : 0;
   $employee->role_service   = $request->role_service == 'on' ? 1 : 0;
   $employee->role_customer   = $request->role_customer == 'on' ? 1 : 0;
   $employee->role_employee   = $request->role_employee == 'on' ? 1 : 0;
   $employee->save();
   $result = '1';
  } else {
   $result = '0';
  }

  return $result;
 }
 public function show($id)
 {
  return "";
 }
 public function edit($id)
 {
  return "";
 }
 public function update(Request $request, $id)
 {
  $result   = '0';
  $check_em = Employee::where('email', $request->email)->whereNotIn('id', [$id])->count();
  if ($check_em == 0) {
   $employee        = Employee::find($id);
   $employee->name  = $request->name;
   $employee->email = $request->email;
   $employee->tel = $request->tel;
   if ($request->password != '*****' && $request->password != '') {
    $employee->password = Hash::make($request->password);
   }
   $employee->position = $request->position;
   $employee->active   = $request->active == 'on' ? 1 : 0;
   $employee->role_service   = $request->role_service == 'on' ? 1 : 0;
   $employee->role_customer   = $request->role_customer == 'on' ? 1 : 0;
   $employee->role_employee   = $request->role_employee == 'on' ? 1 : 0;
   $employee->update_by = Auth::user()->id;
   $employee->save();
   $result = '1';
  } else {
   $result = '0';
  }

  return $result;
 }
 public function destroy($id)
 {
  return "";
 }
 
}
