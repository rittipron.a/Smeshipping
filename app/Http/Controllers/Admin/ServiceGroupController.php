<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Service;
use App\ServiceDay;
use App\ServiceType;
use App\ServiceZone;
use App\ServiceGroup;
use App\ServiceGroupMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ServiceGroupController extends Controller
{
 public function __construct()
 {
  $this->middleware('auth');
 }

 public function index()
 {
    $group = ServiceGroup::all();
    $service = DB::table('app_services')
    ->leftJoin('app_services_type', 'app_services.cat', '=', 'app_services_type.id')
    ->select('app_services_type.code as type_code', 'app_services_type.name as type_name', 'app_services.*')
    ->orderBy('active', 'DESC')
   // ->where('service_group',1)
    ->whereNull('app_services.del')
    ->paginate(20);
    return view('admin/servicegroup/index',compact('group','service'));
 }

 public function create()
 {

 }

 public function store(Request $request)
 {
  $group = new ServiceGroup;
  $group->name = $request->name;
  $group->save();
  return back()->withInput();
 }

 public function show($id)
 {
  $group = ServiceGroup::all();
  /*$service = DB::table('app_services')
  ->leftJoin('app_services_type', 'app_services.cat', '=', 'app_services_type.id')
  ->select('app_services_type.code as type_code', 'app_services_type.name as type_name', 'app_services.*')
  ->orderBy('active', 'DESC')
  ->where('service_group',$id)
  ->where('active',1)
  ->whereNull('app_services.del')
  ->paginate(20);*/
  $service_set = DB::table('app_services')
  ->leftJoin('app_services_group_map', 'app_services.id', '=', 'app_services_group_map.service_id')
  ->where('app_services_group_map.group_id',$id)
  ->whereNull('app_services.del')
  ->select('app_services.*',DB::raw('CONCAT("[",app_services.cat, "] ", app_services.name,"(",app_services.tracking,")") as name'))
  ->orderBy('app_services.id')
  ->get();
  $service_id = collect();
  foreach ($service_set as $key => $value) {
   $service_id->push($value->id);
  }
  $service_all = DB::table('app_services')
  ->select('id',DB::raw('CONCAT("[",id, "] ", description,"(",tracking,")") as text'))
  ->whereNotIn('id',$service_id)
  ->orderBy('id')
  ->whereNull('app_services.del')
  ->get()->toJson();
  return view('admin/servicegroup/show',compact('group','service_set','service_all','id'));
 }

 public function edit($id)
 {

 }

 public function update(Request $request, $id)
 {

 }
 public function setgroup(Request $request, $id){
   $data     = $request->json()->all();
   DB::table('app_services_group_map')->where('group_id', $id)->delete();
   $_count = count($data['service']);
   $service       = '';
   for ($i = 0; $i < $_count; $i++) {
    $service_arr = $data['service'][$i];
    $service     = $service_arr['id'];
    $newdata = new ServiceGroupMap;
    $newdata->group_id = $id;
    $newdata->service_id = $service;
    $newdata->save();
   }
    return 'true';
 }

}
