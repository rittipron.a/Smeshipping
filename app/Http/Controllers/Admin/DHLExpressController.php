<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
use Illuminate\Support\Facades\DB;
use App\ManifestDetail_box;
use App\ManifestDetail_Other_Service;
use App\ManifestDetail;
use App\Manifest;
use App\Customer;
use App\Pickup;
use App\ManifestHistory;
use App\ServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
class DHLExpressController extends Controller
{
 public function __construct()
 {
  $this->middleware('auth');
 }
 public function create_awb($id)
 {
     $manifest_detail = ManifestDetail::where('id',$id)->first();
     if($manifest_detail->awb!=null){
        throw new Exception('awb is not null.');
     }
     $manifest = Manifest::where('id',$manifest_detail->inv_id)->first();
     $box = ManifestDetail_box::where('shipment_id',$id)->where('status','<',10)->get();
     $sender = Customer::where('id',$manifest_detail->m_id)->first();
     $service = Service::where('id',$manifest_detail->services)->first();
     $servicetype = ServiceType::where('id',$service->cat)->first();
     $insurance = ManifestDetail_Other_Service::where('shipment_id',$id)->where('other_service_name','Insurance')->first();
     $checkPLT = DB::table('validate_country_plt')->where('CountryCode',$manifest_detail->country_code)->first();
     $insurance_price = 0;
     /*   if($insurance!=null){
            $insurance_price = $insurance->other_service_value;
        }
     */
    if($manifest_detail->insurance!=null){
        $insurance_price = $manifest_detail->insurance;
    }
     $statename = '';
     if($manifest_detail->country_code=='US'){
        $statename = DB::table('validate_address')->where('country_cd','US')->where('state_code',$manifest_detail->state)->first();
     }
     $path = public_path() . "/uploads/invoice/" . $manifest_detail->invoice_file;
     if($manifest_detail->packge_type!='DC'){
         $invoice_base64 = base64_encode(file_get_contents($path));
     }
     
    
    error_reporting(0);
    date_default_timezone_set("Asia/Bangkok");
    header('Content-Type: text/html; charset=utf-8');

    $url            = "https://xmlpi-ea.dhl.com/XMLShippingServlet";//"https://xmlpitest-ea.dhl.com/XMLShippingServlet";
    $site_id        = "v62_MiV8RRMg6e"; //'v62_zFgdyqm6PS';
    $password       = "OqevK2V9I1";//'kGYhSsjb4Q';
    $account_number = $manifest_detail->account_number=="Auto"||$manifest_detail->account_number==null?$servicetype->account_api:$manifest_detail->account_number ;
    $xml            = '
              <req:ShipmentRequest xmlns:req="http://www.dhl.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com ship-val-global-req.xsd" schemaVersion="6.2">
                  <Request>
                      <ServiceHeader>
                          <MessageTime>' . date('Y-m-d') . 'T' . date('H:i:s') . '</MessageTime>
                          <MessageReference>SME_SHIPPING_TRACKING_'.$manifest_detail->tracking_code.'</MessageReference>
                          <SiteID>'.$site_id.'</SiteID>
                          <Password>'.$password.'</Password>
                      </ServiceHeader>
                      <MetaData>
                          <SoftwareName>SME Shipping</SoftwareName>
                          <SoftwareVersion>1.0</SoftwareVersion>
                      </MetaData>
                  </Request>
                  <RegionCode>AP</RegionCode>
                  <LanguageCode>en</LanguageCode>
                  <PiecesEnabled>Y</PiecesEnabled>
                  <Billing>
                      <ShipperAccountNumber>'.$account_number.'</ShipperAccountNumber>
                      <ShippingPaymentType>S</ShippingPaymentType>
                      <DutyPaymentType>R</DutyPaymentType>
                  </Billing>
                    <Consignee>
                        <CompanyName>'.self::replacestring($manifest_detail->company!=''?$manifest_detail->company:$manifest_detail->recipient).'</CompanyName>
                        <AddressLine>'.self::replacestring($manifest_detail->address).'</AddressLine>
                        <AddressLine>'.self::replacestring($manifest_detail->address2).'</AddressLine>
                        <AddressLine>'.self::replacestring($manifest_detail->address3).'</AddressLine>
                        <City>'.$manifest_detail->city.'</City>'.
                        ($manifest_detail->country_code=='US'?'
                        <Division>'.$statename->state_name.'</Division>
                         <DivisionCode>'.$manifest_detail->state.'</DivisionCode> ':'')
                        .'
                        <PostalCode>'.$manifest_detail->zipcode.'</PostalCode>
                        <CountryCode>'.$manifest_detail->country_code.'</CountryCode>
                        <CountryName>'.$manifest_detail->country.'</CountryName>
                        <Contact>
                            <PersonName>'.self::replacestring($manifest_detail->recipient).'</PersonName>
                            <PhoneNumber>'.$manifest_detail->phone.'</PhoneNumber>
                        </Contact>'.
                        ($manifest_detail->suburb!=null?'
                        <Suburb>'.$manifest_detail->suburb.'</Suburb>':'')
                        .'
                    </Consignee>
                  <Commodity>
                      <CommodityCode>cc</CommodityCode>
                      <CommodityName>cm</CommodityName>
                  </Commodity>'.($manifest_detail->packge_type!='DC'?'
                  <Dutiable>
                  <DeclaredValue>'.$manifest_detail->declared_value.'</DeclaredValue>
                  <DeclaredCurrency>'.$manifest_detail->currency.'</DeclaredCurrency>
                  <ShipperEIN>ShipperEIN</ShipperEIN>
                  </Dutiable>':'').'
                  <Reference>
                      <ReferenceID>'.Auth::user()->name.'/'.($manifest_detail->so!=null?$manifest_detail->so:$manifest_detail->tracking_code).'</ReferenceID>
                      <ReferenceType>St</ReferenceType>
                  </Reference>
                  <ShipmentDetails>
                      <NumberOfPieces>'.count($box).'</NumberOfPieces>
                      <Pieces>';
                        foreach ($box as $index=>$boxitem){
                            $xml=$xml.' <Piece>
                              <PieceID>'.($index+1).'</PieceID>
                              <PackageType>CP</PackageType>
                              <Weight>'.number_format((float)$boxitem->weight, 2, '.', '').'</Weight>
                              <Width>'.intval($boxitem->width).'</Width>
                              <Height>'.intval($boxitem->height).'</Height>
                              <Depth>'.intval($boxitem->length).'</Depth>
                          </Piece>';
                        }
                    $xml=$xml.'</Pieces>
                      <Weight>'.$manifest_detail->weight.'</Weight>
                      <WeightUnit>K</WeightUnit>
                      <GlobalProductCode>'.($manifest_detail->packge_type=='DC'?'D':'P').'</GlobalProductCode>
                      <LocalProductCode>P</LocalProductCode>
                      <Date>'.date('Y-m-d').'</Date>
                      <Contents>'.($manifest_detail->description!=null?$manifest_detail->description:'-').'</Contents>
                      <DoorTo>DD</DoorTo>
                      <DimensionUnit>C</DimensionUnit>
                      <InsuredAmount>'.$insurance_price.'</InsuredAmount>
                      <PackageType>'.$manifest_detail->packge_type.'</PackageType>
                      <IsDutiable>'.($manifest_detail->packge_type=='DC'?'N':'Y').'</IsDutiable>
                      <CurrencyCode>'.$manifest_detail->currency.'</CurrencyCode>
                  </ShipmentDetails>
                  <Shipper>
                    <ShipperID>' . $account_number . '</ShipperID>
                    <CompanyName>SME SHIPPING C/O '.substr(($manifest_detail->shipper_company!=null?$manifest_detail->shipper_company:($manifest_detail->co!=null?$manifest_detail->co:$sender->firstname)),0,42).'</CompanyName>
                    <AddressLine>'.($manifest_detail->shipper_address1!=null?$manifest_detail->shipper_address1:'46 RATCHADAPISEK 16').'</AddressLine>
                    <AddressLine>'.($manifest_detail->shipper_address2!=null?$manifest_detail->shipper_address2:'RATCHADAPISEK ROAD THAPRA BANGKOK-Y').'</AddressLine>
                    <City>'.($manifest_detail->shipper_city!=null?$manifest_detail->shipper_city:'BANGKOK').'</City>
                    <DivisionCode>'.($manifest_detail->shipper_city!=null?$manifest_detail->shipper_city:'BANGKOK').'</DivisionCode>
                    <PostalCode>'.($manifest_detail->shipper_postalcode!=null?$manifest_detail->shipper_postalcode:'10600').'</PostalCode>
                    <CountryCode>TH</CountryCode>
                    <CountryName>THAILAND</CountryName>
                    <Contact>
                        <PersonName>C/O '.substr(($manifest_detail->co!=null?$manifest_detail->co:$sender->firstname),0,31).'</PersonName>
                        <PhoneNumber>'.($manifest_detail->shipper_phone!=null?$manifest_detail->shipper_phone:'+6621057777').'</PhoneNumber>
                        <Email>service@smeshipping.com</Email>
                    </Contact>
                </Shipper>'.
               ($checkPLT->PLTIn=='Y'&&$manifest_detail->packge_type!='DC'?'
               <SpecialService>
                    <SpecialServiceType>WY</SpecialServiceType>
                </SpecialService>  
                <DocImages>
		            <DocImage>
                        <Type>CIN</Type>
                        <Image>'.$invoice_base64.'</Image>
                        <ImageFormat>PDF</ImageFormat>
                    </DocImage>
                </DocImages>':'').'
                <LabelImageFormat>PDF</LabelImageFormat> 
                <Label><LabelTemplate>8X4_PDF</LabelTemplate></Label> 
              </req:ShipmentRequest>
              ';
    $headers = array(
        "Content-type: text/xml",
        "Content-length: " . strlen($xml),
        "Connection: close",
    );
 //return dd($xml);
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_MUTE, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 10);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

  $data       = curl_exec($ch);
  //libxml_use_internal_errors(true);
  $data = iconv('UTF-8', 'UTF-8//IGNORE', $data);
  $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
  if (false === simplexml_load_string($data)) {
    $errors = libxml_get_errors();
    echo 'Errors are '.var_export($errors, true);
    throw new \Exception('invalid XML');
  }
  if (!empty($array_data["LabelImage"]["OutputImage"])) {
   $image      = base64_decode($array_data["LabelImage"]["OutputImage"]);
   $image_name = $manifest_detail->inv_id.'_shipper_' . $array_data["Billing"]["ShipperAccountNumber"] . "_" . $array_data["Shipper"]["ShipperID"] . "_AirwayBillNumber_" . $array_data["AirwayBillNumber"] . '.pdf';
   $path       = public_path() . "/uploads/labels/" . $image_name;
   file_put_contents($path, $image);
   // Insert database success
   $array_data["Note"]["ActionNote"]=$image_name;
   $manifest_detail->awb_file = $image_name;
   $manifest_detail->awb = $array_data["AirwayBillNumber"];
   $manifest_detail->save();
   $result = $array_data;

    $history            = new ManifestHistory;
    $history->topic     = 'Generate AWB';
    $history->detail    = Auth::user()->name.'/'.($manifest_detail->so!=null?$manifest_detail->so:$manifest_detail->tracking_code).' /'.$manifest_detail->awb;
    $history->update_by = Auth::user()->id;
    $history->inv_id    = $manifest_detail->inv_id;
    $history->save();
  } else {
   // Insert database faile
   //var_dump($array_data);
   $result = $array_data;
    $history            = new ManifestHistory;
    $history->topic     = 'Generate AWB';
    $history->detail    = 'สร้าง AWB ไม่สำเร็จ '.$result['Response']['Status']['Condition']['ConditionData'];
    $history->update_by = Auth::user()->id;
    $history->inv_id    = $manifest_detail->inv_id;
    $history->save();
  }
  $result['Response']['ServiceHeader']['SiteID'] = 'DHL EXPRESS CALLBACK.';
  $result['Response']['ServiceHeader']['Password'] = 'DHL EXPRESS CALLBACK.';
  
  return $result;
  
 }

 public function replacestring($string)
 {
     $string = str_replace('&', '&amp;', $string);
     $string = str_replace('<', '&lt;', $string);
     $string = str_replace('>', '&gt;', $string); 
  return $string;
 }

}
