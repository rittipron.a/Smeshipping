<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Service;
use App\ServiceDay;
use App\ServiceType;
use App\ServiceZone;
use App\ServiceGroup;
use App\ServiceZoneESC;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $service = DB::table('app_services')
            ->leftJoin('app_services_type', 'app_services.cat', '=', 'app_services_type.id')
            ->select('app_services_type.code as type_code', 'app_services_type.name as type_name', 'app_services.*')
            ->orderBy('updated_at', 'DESC')
            ->whereNull('app_services.del')
            ->paginate(20);
        $servicetype = ServiceType::orderBy('code', 'ASC')->get();
        return view('admin/service/index')->with(compact('service', 'servicetype'));

    }

    public function create()
    {
        return "create";
    }

    public function store(Request $request)
    {
        $type = ServiceType::Where('id', $request->input('type_of_service'))->first();
        $service = new Service;
        $service->cat = $request->has('type_of_service') && $request->input('type_of_service') != '' ? $request->input('type_of_service') : '0';
        //$service->service_name     = $request->has('service_name') && $request->input('service_name') != '' ? $request->input('service_name') : '';
        $service->name = $request->has('product_name') && $request->input('product_name') != '' ? $request->input('product_name') : '';
        $service->description = $request->has('description') && $request->input('description') != '' ? $request->input('description') : '';
        // $service->routing_id       = $request->has('routing_id') && $request->input('routing_id') != '' ? $request->input('routing_id') : '';
        //$service->code             = $request->has('service_code') && $request->input('service_code') != '' ? $request->input('service_code') : '';
        $service->tracking = $type->tracking;
        //$service->website          = $request->has('website') && $request->input('website') != "" ? 'yes' : '';
        //$service->webapp           = $request->has('webapp') && $request->input('webapp') != "" ? 'yes' : '';
        //$service->webapp_detail    = $request->has('webapp_detail') && $request->input('webapp_detail') != '' ? $request->input('webapp_detail') : '';
        //$service->ba               = $request->has('ba') && $request->input('ba') != "" ? 'yes' : '';
        $service->active = 1;
        $service->type = $request->has('type') && $request->input('type') != '' ? $request->input('type') : '';
        $service->cal_weight = $request->has('cal_weight') && $request->input('cal_weight') != '' ? $request->input('cal_weight') : '';
        $service->cal = $request->has('cal') && $request->input('cal') != '' ? $request->input('cal') : '';
        //$service->prod_drop_point  = $request->has('prod_drop_point') && $request->input('prod_drop_point') != '' ? $request->input('prod_drop_point') : '';
        //$service->more_info_detail = $request->has('more_info_detail') && $request->input('more_info_detail') != '' ? $request->input('more_info_detail') : '';
        $result = 'create fail';
        if ($service->save()) {
            $result = 'success';
        }

        return $service->id;
    }

    public function show($id)
    {
        $group = ServiceGroup::all();
        $price = ServiceZone::where('s_id', $id)->orderBy('weight', 'ASC')->get();
        $surcharges = ServiceZoneESC::where('s_id', $id)->orderBy('weight', 'ASC')->get();
        $servicetype = ServiceType::orderBy('code', 'ASC')->get();
        $service = Service::where('id', $id)->first();
        $price_backup = DB::table('app_services_zone_backup')->where('s_id', $id)->orderBy('weight', 'ASC')->get();
        return view('admin/service/show')->with(compact('service', 'servicetype', 'price', 'price_backup', 'group', 'surcharges'));
    }

    public function edit($id)
    {
        return "edit";
    }

    public function update(Request $request, $id)
    {
        $service = Service::find($id);
        $service->cat = $request->has('type_of_service') && $request->input('type_of_service') != '' ? $request->input('type_of_service') : '0';
        //$service->service_group    = $request->has('group_of_service') && $request->input('group_of_service') != '' ? $request->input('group_of_service') : '';
        $service->name = $request->has('product_name') && $request->input('product_name') != '' ? $request->input('product_name') : '';
        $service->description = $request->has('description') && $request->input('description') != '' ? $request->input('description') : '';
        //$service->routing_id       = $request->has('routing_id') && $request->input('routing_id') != '' ? $request->input('routing_id') : '';
        //$service->code             = $request->has('service_code') && $request->input('service_code') != '' ? $request->input('service_code') : '';
        $service->tracking = $request->has('tracking') && $request->input('tracking') != "" ? $request->input('tracking') : '';
        //$service->website          = $request->has('website') && $request->input('website') != "" ? 'yes' : '';
        //$service->webapp           = $request->has('webapp') && $request->input('webapp') != "" ? 'yes' : '';
        //$service->webapp_detail    = $request->has('webapp_detail') && $request->input('webapp_detail') != '' ? $request->input('webapp_detail') : '';
        //$service->ba               = $request->has('ba') && $request->input('ba') != "" ? 'yes' : '';
        //$service->active           = $request->has('active') && $request->input('active') != "" ? '1' : '';
        $service->type = $request->has('type') && $request->input('type') != '' ? $request->input('type') : '';
        $service->cal_weight = $request->has('cal_weight') && $request->input('cal_weight') != '' ? $request->input('cal_weight') : '';
        $service->cal = $request->has('cal') && $request->input('cal') != '' ? $request->input('cal') : '';
        // $service->prod_drop_point  = $request->has('prod_drop_point') && $request->input('prod_drop_point') != '' ? $request->input('prod_drop_point') : '';
        //$service->more_info_detail = $request->has('more_info_detail') && $request->input('more_info_detail') != '' ? $request->input('more_info_detail') : '';
        //$service->del              = $request->active=='on'?null:Auth::user()->name;
        $result = 'fail';
        if ($request->hasFile('logo')) {
            $file = $request->file('logo')->getClientOriginalName();
            $fileName = pathinfo($file, PATHINFO_FILENAME);
            $imageName = time() . '_' . $fileName . '.' . $request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move(public_path('/uploads/service'), $imageName);
            $service->logo = $imageName;
        }

        if ($service->save()) {
            $result = 'success';
        }

        return redirect()->back();
    }

    public function setzone(Request $request, $id)
    {
        $data = $request->json()->all();
        $daycount = ServiceDay::where('s_id', $id)->count();
        $zone = $data['input_zone'];
        $result = '';
        if ($daycount == 1) {
            DB::table('app_services_day')
                ->where('s_id', $id)
                ->update(['zone_' . $zone => $data['input_day']]);
            $result = 'success';
        } else {
            DB::table('app_services_day')->insert(
                ['s_id' => $id, 'zone_' . $zone => $data['input_day']]
            );
            $result = 'success';
        }

        DB::table('app_country_zone')->where('s_id', $id)->where('zone', $zone)->delete();
        $country_count = count($data['country']);
        $country = '';
        for ($i = 0; $i < $country_count; $i++) {
            $country_arr = $data['country'][$i];
            $country = $country_arr['id'];
            DB::table('app_country_zone')->insert(
                ['s_id' => $id, 'zone' => $zone, 'country' => $country]
            );
        }

        return $result;
    }

    public function setprice(Request $request, $id)
    {
        DB::table('app_services_zone')->where('s_id', $id)->delete();
        DB::table('app_services_zone_backup')->where('s_id', $id)->delete();
        $data = $request->json()->all();
        $count = count($data['data']);
        for ($i = 0; $i < $count; $i++) {
            $value = $data['data'][$i];
            DB::table('app_services_zone')->insert(
                [
                    's_id' => $id,
                    'weight' => $value['weight'],
                    'zone_1' => $value['zone_1'],
                    'zone_2' => $value['zone_2'],
                    'zone_3' => $value['zone_3'],
                    'zone_4' => $value['zone_4'],
                    'zone_5' => $value['zone_5'],
                    'zone_6' => $value['zone_6'],
                    'zone_7' => $value['zone_7'],
                    'zone_8' => $value['zone_8'],
                    'zone_9' => $value['zone_9'],
                    'zone_10' => $value['zone_10'],
                    'zone_11' => $value['zone_11'],
                    'zone_12' => $value['zone_12'],
                    'zone_13' => $value['zone_13'],
                    'zone_14' => $value['zone_14'],
                    'zone_15' => $value['zone_15'],
                    'zone_16' => $value['zone_16'],
                    'zone_17' => $value['zone_17'],
                    'zone_18' => $value['zone_18'],
                    'zone_19' => $value['zone_19'],
                    'zone_20' => $value['zone_20'],
                    'zone_21' => $value['zone_21'],
                    'zone_22' => $value['zone_22'],
                    'zone_23' => $value['zone_23'],
                    'zone_24' => $value['zone_24'],
                    'zone_25' => $value['zone_25']
                ]
            );
        }

        // $tmp = $data['data']
        return 'success';
    }

    public function setesc(Request $request, $id)
    {
        DB::table('app_services_zone_esc')->where('s_id', $id)->delete();
        $data = $request->json()->all();
        $count = count($data['data']);
        for ($i = 0; $i < $count; $i++) {
            $value = $data['data'][$i];
            DB::table('app_services_zone_esc')->insert(
                [
                    's_id' => $id,
                    'weight' => $value['weight'],
                    'zone_1' => $value['zone_1'],
                    'zone_2' => $value['zone_2'],
                    'zone_3' => $value['zone_3'],
                    'zone_4' => $value['zone_4'],
                    'zone_5' => $value['zone_5'],
                    'zone_6' => $value['zone_6'],
                    'zone_7' => $value['zone_7'],
                    'zone_8' => $value['zone_8'],
                    'zone_9' => $value['zone_9'],
                    'zone_10' => $value['zone_10'],
                    'zone_11' => $value['zone_11'],
                    'zone_12' => $value['zone_12'],
                    'zone_13' => $value['zone_13'],
                    'zone_14' => $value['zone_14'],
                    'zone_15' => $value['zone_15'],
                    'zone_16' => $value['zone_16'],
                    'zone_17' => $value['zone_17'],
                    'zone_18' => $value['zone_18'],
                    'zone_19' => $value['zone_19'],
                    'zone_20' => $value['zone_20'],
                    'zone_21' => $value['zone_21'],
                    'zone_22' => $value['zone_22'],
                    'zone_23' => $value['zone_23'],
                    'zone_24' => $value['zone_24'],
                    'zone_25' => $value['zone_25']
                ]
            );
        }

        // $tmp = $data['data']
        return 'success';
    }

    public function price_set_date(Request $request, $id)
    {
        DB::table('app_services_zone_backup')->where('s_id', $id)->delete();
        $data = $request->json()->all();
        $count = count($data['data']);
        for ($i = 0; $i < $count; $i++) {
            $value = $data['data'][$i];
            DB::table('app_services_zone_backup')->insert(
                [
                    's_id' => $id,
                    'weight' => $value['weight'],
                    'zone_1' => $value['zone_1'],
                    'zone_2' => $value['zone_2'],
                    'zone_3' => $value['zone_3'],
                    'zone_4' => $value['zone_4'],
                    'zone_5' => $value['zone_5'],
                    'zone_6' => $value['zone_6'],
                    'zone_7' => $value['zone_7'],
                    'zone_8' => $value['zone_8'],
                    'zone_9' => $value['zone_9'],
                    'zone_10' => $value['zone_10'],
                    'zone_11' => $value['zone_11'],
                    'zone_12' => $value['zone_12'],
                    'zone_13' => $value['zone_13'],
                    'zone_14' => $value['zone_14'],
                    'zone_15' => $value['zone_15'],
                    'zone_16' => $value['zone_16'],
                    'zone_17' => $value['zone_17'],
                    'zone_18' => $value['zone_18'],
                    'zone_19' => $value['zone_19'],
                    'zone_20' => $value['zone_20'],
                    'zone_21' => $value['zone_21'],
                    'zone_22' => $value['zone_22'],
                    'zone_23' => $value['zone_23'],
                    'zone_24' => $value['zone_24'],
                    'zone_25' => $value['zone_25'],
                    'set_date' => $data['date']]
            );
        }

        // $tmp = $data['data']
        return $data['date'];
    }

    public function destroy($id)
    {
        $service = Service::find($id);
        $service->del = Auth::user()->name;
        $service->save();
        return "delete" . $id;
    }

    public function getZone($s_id, $zone)
    {
        $CountryinZone = DB::table('app_country_zone')
            ->join('app_country', 'app_country_zone.country', '=', 'app_country.id')
            ->select('app_country.id', 'app_country.country as text')
            ->where([
                ['app_country_zone.s_id', '=', $s_id],
                ['app_country_zone.zone', '=', $zone],
            ])
            ->whereNotNull('s_id')
            ->distinct()
            ->get();
        return $CountryinZone->toJson();
    }

    public function getCountryForZone($s_id)
    {
        $contry_used = DB::table('app_country_zone')->where('s_id', $s_id)->pluck('country');
        $country_empty = DB::table('app_country')
            ->whereNull('del')
            ->whereNotIn('id', $contry_used)
            ->select('id', 'country as text')->get();
        return $country_empty->toJson();
    }

    public function getBussinessDayByServiceZone($s_id, $zone)
    {
        $bussinessDay = DB::table('app_services_day')
            ->where('s_id', $s_id)
            ->select('id', 'zone_' . $zone . ' as day')->get();
        return $bussinessDay->toJson();
    }

    public function getPriceByWeight($weight, $country_id)
    {
        $result = DB::table('app_services')
            ->leftJoin('app_services_type', 'app_services.cat', '=', 'app_services_type.id')
            ->leftJoin('app_country_zone', 'app_services.cat', '=', 'app_services_type.id')
            ->select('app_services_type.code as type_code', 'app_services_type.name as type_name', 'app_services.*')
            ->whereNull('app_services.del')
            ->orderBy('website', 'DESC')
            ->orderBy('ba', 'DESC')
            ->orderBy('webapp', 'DESC')
            ->get();
        return $result->toJson;
    }

    public function copyService($service_id)
    {
        $current_date_time = Carbon::now()->toDateTimeString();
        $service = DB::table('app_services')->where('id', $service_id)->first();
        $id = DB::table('app_services')->insertGetId(
            [
                'cat' => $service->cat,
                'name' => $service->name,
                //'service_group'  => $service->service_group,
                'description' => $service->description,
                'routing_id' => $service->routing_id,
                'code' => $service->code,
                'tracking' => $service->tracking,
                'type' => $service->type,
                'cal_weight' => $service->cal_weight,
                'cal' => $service->cal,
                'unit' => $service->unit,
                'del' => $service->del,
                'comment' => $service->comment,
                'prod_drop_point' => $service->prod_drop_point,
                'more_info_detail' => $service->more_info_detail,
                'logo' => $service->logo,
                'active' => $service->active,
                'created_at' => $current_date_time,
                'updated_at' => $current_date_time
            ]
        );

        $old_service_zone = DB::table('app_services_zone')->where('s_id', $service_id)->get();
        foreach ($old_service_zone as $key => $value) {
            DB::table('app_services_zone')->insert(
                [
                    's_id' => $id,
                    'weight' => $value->weight,
                    'zone_1' => $value->zone_1,
                    'zone_2' => $value->zone_2,
                    'zone_3' => $value->zone_3,
                    'zone_4' => $value->zone_4,
                    'zone_5' => $value->zone_5,
                    'zone_6' => $value->zone_6,
                    'zone_7' => $value->zone_7,
                    'zone_8' => $value->zone_8,
                    'zone_9' => $value->zone_9,
                    'zone_10' => $value->zone_10,
                    'zone_11' => $value->zone_11,
                    'zone_12' => $value->zone_12,
                    'zone_13' => $value->zone_13,
                    'zone_14' => $value->zone_14,
                    'zone_15' => $value->zone_15,
                    'zone_16' => $value->zone_16,
                    'zone_17' => $value->zone_17,
                    'zone_18' => $value->zone_18,
                    'zone_19' => $value->zone_19,
                    'zone_20' => $value->zone_20,
                    'zone_21' => $value->zone_21,
                    'zone_22' => $value->zone_22,
                    'zone_23' => $value->zone_23,
                    'zone_24' => $value->zone_24,
                    'zone_25' => $value->zone_25,
                    'time' => $value->time,
                    'del' => $value->del,
                    'active' => $value->active,
                    'created_at' => $current_date_time,
                    'updated_at' => $current_date_time
                ]
            );
        }
        $old_country_zone = DB::table('app_country_zone')->where('s_id', $service_id)->get();
        foreach ($old_country_zone as $key => $value) {
            DB::table('app_country_zone')->insert(
                [
                    's_id' => $id,
                    'country' => $value->country,
                    'zone' => $value->zone,
                    'time' => $value->time,
                    'del' => $value->del,
                    'active' => $value->active,
                    'created_at' => $current_date_time,
                    'updated_at' => $current_date_time
                ]
            );
        }

        $services_day = DB::table('app_services_day')->where('s_id', $service_id)->first();
        DB::table('app_services_day')->insert(
            [
                's_id' => $id,
                'zone_1' => $services_day->zone_1,
                'zone_2' => $services_day->zone_2,
                'zone_3' => $services_day->zone_3,
                'zone_4' => $services_day->zone_4,
                'zone_5' => $services_day->zone_5,
                'zone_6' => $services_day->zone_6,
                'zone_7' => $services_day->zone_7,
                'zone_8' => $services_day->zone_8,
                'zone_9' => $services_day->zone_9,
                'zone_10' => $services_day->zone_10,
                'zone_11' => $services_day->zone_11,
                'zone_12' => $services_day->zone_12,
                'zone_13' => $services_day->zone_13,
                'zone_14' => $services_day->zone_14,
                'zone_15' => $services_day->zone_15,
                'zone_16' => $services_day->zone_16,
                'zone_17' => $services_day->zone_17,
                'zone_18' => $services_day->zone_18,
                'zone_19' => $services_day->zone_19,
                'zone_20' => $services_day->zone_20,
                'zone_21' => $services_day->zone_21,
                'zone_22' => $services_day->zone_22,
                'zone_23' => $services_day->zone_23,
                'zone_24' => $services_day->zone_24,
                'zone_25' => $services_day->zone_25,
                'created_at' => $current_date_time,
                'updated_at' => $current_date_time
            ]
        );
        return back()->withInput();
    }

}
