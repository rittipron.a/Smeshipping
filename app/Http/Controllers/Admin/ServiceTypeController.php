<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ServiceType;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceTypeController extends Controller
{
 public function __construct()
 {
  $this->middleware('auth');
 }

 public function index()
 {
  $serviceType = ServiceType::whereNull('del')->get();
  return view('admin/servicetype/index', compact('serviceType'));
 }

 public function create()
 {
  return "";
 }

 public function store(Request $request)
 {
    $serviceType = new ServiceType;
    $serviceType->code = $request->code;
    $serviceType->name = $request->name;
    $serviceType->account_api = $request->account_api;
    
    $serviceType->pickup_fee = $request->sh_pickup_fee;
    $serviceType->time_guaranty = $request->sh_time_guaranty;
    $serviceType->remote_area = $request->sh_remote_area;
    $serviceType->insurance = $request->sh_insurance;
    $serviceType->co_from = $request->sh_co_from;
    $serviceType->over_size = $request->sh_over_size;
    $serviceType->over_weight = $request->sh_over_weight;
    $serviceType->export_doc = $request->sh_export_doc;
    $serviceType->packing_paperbox = $request->sh_packing_paperbox;
    $serviceType->wood_packing = $request->sh_wood_packing;

    $serviceType->save();

    return redirect('admin/servicetype');
 }

 public function show($id)
 {
    $serviceType = ServiceType::find($id);
    return view('admin/servicetype/show', compact('serviceType'));
 }
 public function edit($id)
 {
  return "";
 }

 public function update(Request $request, $id)
 {
    $serviceType = ServiceType::find($id);
    $serviceType->code = $request->code;
    $serviceType->name = $request->name;
    $serviceType->account_api = $request->account_api;
    $serviceType->tracking = $request->tracking;
    
    $serviceType->pickup_fee = $request->sh_pickup_fee;
    $serviceType->time_guaranty = $request->sh_time_guaranty;
    $serviceType->remote_area = $request->sh_remote_area;
    $serviceType->insurance = $request->sh_insurance;
    $serviceType->co_from = $request->sh_co_from;
    $serviceType->over_size = $request->sh_over_size;
    $serviceType->over_weight = $request->sh_over_weight;
    $serviceType->export_doc = $request->sh_export_doc;
    $serviceType->packing_paperbox = $request->sh_packing_paperbox;
    $serviceType->wood_packing = $request->sh_wood_packing;

    $serviceType->save();


  return redirect('admin/servicetype');
 }

 public function destroy($id)
 {
    $serviceType = ServiceType::find($id);
    $serviceType->del = Auth::user()->name;
    $serviceType->save();

    return 'success';
 }

 public function getOtherService($id)
 {
    $service = Service::where('id',$id)->select('cat')->first();
    $serviceType = ServiceType::where('id',$service->cat)->first();
    return $serviceType->toJson();
 }
}
