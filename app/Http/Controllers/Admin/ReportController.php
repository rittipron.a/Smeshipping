<?php

namespace App\Http\Controllers\admin;

use App\Classes\API\ThaiPost;
use App\Http\Controllers\Controller;
use App\Pickup;
use App\ThaiPostEMSNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use mPDF;
use Mpdf\QrCode\QrCode;
use Mpdf\QrCode\Output;

class ReportController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function smelabel($id)
    {
        $shipment_all = DB::table('app_manifest_detail')
            ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
            ->leftJoin('pickup_manifest', 'app_manifest_detail.p_id', '=', 'pickup_manifest.id')
            ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
            ->where('app_manifest_detail.inv_id', $id)
            ->select('app_manifest_detail.m_id as customerid', 'pickup_manifest.name', 'pickup_manifest.delivery_date', 'app_services.name as servicename', 'app_country.country', 'app_manifest_detail.*')
            ->get();

        $mpdf = new \Mpdf\Mpdf();

        $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new \Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                __DIR__ . '/fonts',
            ]),
            'fontdata' => $fontData + [
                    'thsarabun' => [
                        'R' => 'THSarabunNew.ttf',
                        //'I' => 'THSarabunNew Italic.ttf',
                        //'B' => 'THSarabunNew Bold.ttf',
                    ],
                    'prompt' => [
                        'R' => 'Prompt-Regular.ttf',
                    ]
                ],
            'default_font' => 'prompt'
        ]);

        $html = '<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">

<style>
body {
	font-size: 9pt;
}
body {
    margin: 0px;
}

.wrapper {
    display: grid;
    grid-template-columns: 100px 100px 100px;
    grid-gap: 10px;
    background-color: #fff;
    color: #444;
}

table, th {
  border: 1px solid black;
  border-collapse: collapse;
  padding: 10px;
 }
.td_border{
  border: 1px solid black;
  border-collapse: collapse;
  padding: 10px;
}
.td_padding{
  border-top: 1px solid black;
  padding: 10px;
}
.center_text{
  text-align: center;
}
.bold_font{
  font-weight: bold;
}
.box-1{
  padding:5px;
  width:48%;
  float:left;
}
.box-2{
  width:8.8cm;
  float:right;
  text-align:right;
}
</style>
</head>
<body>';
        $line = 0;
        foreach ($shipment_all as $shipment) {
            $box = DB::table('app_manifest_box')->where('shipment_id', $shipment->id)->where('status', '<', 10)->count();
            $shipmentAll = DB::table('app_manifest_detail')->where('inv_id', $shipment->inv_id)->where('status', '<', 10)->count();
            $pickup = DB::table('pickup_manifest')->where('id', $shipment->p_id)->first();
            $number = $box;
            $code = $shipment->tracking_code;
            for ($i = 0; $i < $number; $i++) {
                $html .= '
        <div class="box-1">
          <table style="width:8cm; height:5cm;">
          <tr>
          <td class="center_text td_border"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATMAAABMCAYAAAABW1D+AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAFF9JREFUeNrsXe9x27gSRzR+c++bfRWYV4F1FYipwEwFViqIUoHpCiJXELqC0BUcVcHJFZxUwdnfbuZ9yOP6lgkMYxcLEFScZHeGY48kgviz+8NvFwvw1efPn01O+c9//1v2f+Ca91fRX2fMz/f9teuvrr+28Pd///xzb1RUVFQi5VUuMOtBrOr/rPvrdGRRt/3V9qDW6PCoqKgcBMx6AFsiC1v34LPFz06QlcHnVYCZcfKA4LhWtqaiojIJmBEs7A6Bp3F+C+C2xCsF2B6w3FqHS0VFJQuYITABWJ0zP4M4WO1zEzGeBqC0SKgrlLvsy+102FRUVJLBrAcicB0BSI4jwGfVg0+bGdSulKWpqKiMZWZDLKwMsDNbbhHUdp7yluiuHkfW+wbL1FiaioqKDMwQcDoXjNDlhNjZUsCwHtBFbBNdV59AjK5UQFNRUQmCWQ80ADIXFhuqCYZVoNt4kcqo+jJW/Z8PCmgqKipZwQwZ2UfPV1eGSJcQghoAUEWAIrivbaTbuenLKnUoVVQUzAwBKn+kuI3W/RALO2PuL4fcNOfe2IUGkOu+rJUOp4qKgpkNJhDD2gnB5BZB7Z4ANWBpl8z9b4kUjhRAe0OBq4qKyo8vM89nENSXxqAgaL/DJNpngikUv5t/0zR88hHdWfc+YGwlMjipNAjEKioqCmaPQNL0V4EgdM0A0SDAnj71QLImAA2AaY4sbkpAO0bXVkVFRd1MvyDzgphUKAVjY/4N7lNuJ7iUFxO7nL/5FhdUVFR+EmYG7Ki/7gmW1OKK4WsELEoA7DoEIB9Lg7LfJjC0mOB+rcOqovITM7MeSIDNDBvHIX1iRe2DFBz3Q65WDsBp/GkfIL8Tq5wAUpfKzlRUVEhmhqkUNjBBSsUfEAfzBdVx1RDY1zVR7nGAoTUMQ/Peh4sJG2G7XkyaxiEWJXThQ0WoJ8XP4GYuie/f9deWAJd7zO16bfxB+lRAg/tawkArI1sQqF6I8pRM3+ZUUF34UBFN8rjT5sd1MyFWZsIBdvK0CgSezviTZEMuJ4Cab1HAm9mPLu4nQdt+p54ZCUaljz1KjiLCFd6i/+0k4Gr1O6xAHxzQEEihf9wZX49Af5nMbItjNX+pYRi0OSBAPjLD6tWro19+gRtbIzvuOpQkSwFTCNDg+edSAGV+LwJfAUCskFFxfQIpK2sORFB5AOB/ncKwrX54BG7Bzg2x9OW9Cigc9G1odZs93SSwH/d1zNl1krIQfP+i6ooLVG78mJMHNDCDNtRyIBFYzX82mVsG3HATcyAG/Zgl4LR99BZAS7ef2QWmdsXY2xK9KemRYLdoe0/0YwadhA9/Y8IxqXN0HQvCAKBS72NdTmzMnefzSzQc3+9D7maZMEAlKs+lQJnh+w9Ufh0O0tlUbi8+F8bjwVL0MlPxt1Sb+qtDwJQo3gWO+0nCGMWyaklZ3G9ai21K32NxjP2wQCD9CxeqcujkUC6Eev4EIEzsx9bzm0WgnhJGTm1V7KRlILj/jX0Xc7Yh6P0fbhtmFhDZ6Rd3TEHQiC0TC1vHxsJw5qbiYc/uwd/XAmWIGaAajTT2hSzvCMC1P6syA9kSldxVnlzP6TzPhPHemfgDNUFf1pFGuElgslRZd1ZZlaDNYyeES1+KUSRIUhMD1Y+VoO2lp57zxLqUMbrjmRBrZIkXGfq6fQZmFlCATzpHQHpgZqSU4P4pNVMjPV8Sz2oI0NwLZhAJODRGnvbhkzow4GVGIJs7LkXnYYJZwSxxr+wTQ3THAss8HjO7C8tqIwEvx1j5sgBylHvhTpwR/eh7fnNIMMO6bkfa2jOWhvZrZhTdREAqDL0NKRXQyJkaUz6uiQpXQhCxpRAYwjLDDDEPDPgxtX81gd53AjdijNhuq/3M45HllkI2EQ1mEuMaafSxcuxpXy7WvExoO8UKzxLdTW5S2DF21pnxr6IkQX6GVG3rYzGYflExLE0CaNfEw1cMy/ExrsbjbjYmvHc0BA6hVUBwuW8C388FMYVqJJCdmOdnve0t5ckFZp1n9uaAbI/9Q7F4+PyNZ6saWd+El9ZIyhpj9ClSTMDMYsuVtD3K3Qz0UccA2ceICRF0ZhMIdz3Dh5kTB6sYUCoJ4AgB2ooAgw++WBPSfcrdXEeys5AC1UwHQ0fCToI5Lmz85mGpV/j9TvDcscrceABSwihgzF5HXCtLCctAjAxAClJPYIUbwPa9D+iJo5mocjcJfXMuKEti9NyE8x77BxbKrmIm0QArvLH6/iqh/RLXOTSRthGJ11EuZmCl1e0H0KdXoEsQv0fbemXCi5OPB76ao19++exca8g98139dyf9tfXcA9c9pHlE3gf3nBD3NMRzSk/Z98Rv60BbPjPX3HNPYdW7ZMpupGVKLhgToryloC1NyjOx3JYptybu6QR9XzLlriLrWErqyOhIJ2zvCdPW0LNXzO8qT7k183u7vvORbRfZfYROnDi/rQTPbSgMENrVl3b6zjOD1Tly1REXB24YhhazWnnMBCFXhOtSe8pOOZSRm61ufHk9yMBgligCrlApjHdIZnW4590IN6JLoYE4jueM+1Yz4/Y6kOOXs75cWa3FMMfEy+6I1VUufeR+RHt3zO93MXHHACt07b4c0d9P+gjd0SbgSr5BVi9auUYPaWOx7t9sPZsxtJ3MD8JCYwFtR3T+uS9+hg30uZULT6dTYLZNNIKK6dCW6/xATKGKBJQ5Q9HteFmVG8yMf1HDrhsVktgKYl6lZPFhJJhJ8+9iFwikfSQBSSoFpRhZrjRWGIxLR7jLMbHWIYk+hYQA9rxFN/QJ6M+Ym84SAY1breyIBYGaSKNYE+yscQGG+N19RCD1CSiP2MPGKc9pRLpIEQAiScrBfsS2lVAMJal/UJ8WGYF3cQCj74jxWTD9vo1khVIG30XGHWPA7NTwC2KiPkJvYhEAsqSthqDPvnMPQY4wkHkaADTv69wA0Prv4F83tQFWK3c+VwMWBHCAzzzuZumyM8x0v/SAQukwgM7jFo3ZQgQLFEXCi1JCylMFFIZauaQYBccEd0LXwTVsEYjglpYqEjC5+gBL/2zySDajdxmENT4kwxkRMG+Y8bwd+jowrl0imA222xKsSdpHXIihHrtnmpIj9MFPBQyNej/lCum2u9IGS77UhmxA7j89xrH0oO4an3HsiZ2VHJgFOm1rwtnsQxwhxmBDruTShNNBuDdbxcTLoH3SvZp7h61K2jusgi8jXIbSHEY6gdG3TmjF2y9OLlYZ0JsHZ3y555dW/eb42+OAXohBMsAKQ+5m4bH3MjQpICs7ZdzqyQ5EmAmp/Znhg/vQSF9OCLWQsCXczTWxdakhmEHBxMdCOSpS4/tisMIYV0h5zgJxidqEk3hzZ6w/6w8cI0n6wfAOCOkLZQ4BZna8rBoJeKfoGQxXaAKsHRDgfm+Xex7QnVuHGFRM2yUT3V1gTBtHL6XuMufJ1FMO+szIN/SGAG1p/KuVLdOwvef3dWBGouI225j4CxO/owb347BtIoOhVgSQLY1sq0c3ATh0CSzziXti+MMEcm+5kgJzKQC8XH14YzOPGBc/NHnZcbRAP0p1ozT8YQ3urhsJEyyYeu2lYQzot4SrkDIzG9AaAhy2xh+49O7QZzaLv3OD5Oji3REum12eGMyG+J3hs/t98YTUUyBYkEAQWMcAD95zOhWY4Zi+jdQPDtAO7WLmMPoYIFuOmAw4IHNDPNJ42YJx96jk9GfupiBe1gna2wiB7ARDI7FXNcNG3UZ07jnFUJi9lZfEabUN4crUws5w9zxuYsAM67CMBLQFU3YZ0YcnzgB20viGFZ/KNvNT6SbWHtuYV/5RgFYdEswyGL1UrjxAlmN8bj1AFhMvY5kr6tGt0N1cMHWMBdkpwhDdkdW484gbLzC43/jYjme1cgCjOQFcHz3l107QHer4gTCQ1jNLilcycVV2a/jtTW7cq7ZXayOSE+1Ba2OBzKQvu5tURcPD/bbGv6WKMgIYk0KoqHsTf4LDJePO7CKMPhVgh0D/2qdrI13qG4y97SINPipWaHk3O0b/FvYxO6kMN2KlPGU8Htt9ZCnrOtIYIYa0JVYMoYP+9ADAyl3NwGfXHnepdtxISDPwpZGUqZTWqccaB60RztRLh0HGDsIAwutIpZcoz+iTRAmXcx7xlqwn6TOhDcoxpwJjWZcj+kcSL6MAFsq/F6QXcP1/5flsi+V2AjfsLFPbh/SnleH3T54L9ZFLko2Z5JMm4yPrg7WJP2eo8y3h4hHOV57yasxh2SWyM6j0hcdo5tYAbRJOXbBjc6XgVXpGCKosmAlXLrl4WZbzwCL7qMYwgwT0S6HLF5sJLmFcnNFLFgi6lGPXJSx0onK/tCsC8GxCsUxwtx+c5GBKRIuMI04teWz3zAGzh8hCyNVKHDDpaiWVwb8UGuk8EG+LNdjhVXoxR5AsEvruMqFusfGgKQBth8zvfU4AylyWJCUjyugz1XFsuWNPy+0YTyPW/rvMtlFgGMV37UP1sI/NpvZCBivKHPC2JOJtpWMc1GZxN2clFEPoIl+CccIY7L3UdQzMSrFHumyE31UZ4hPJ/TO45gHAl6zA3WU+InsXYdCTTAg5zshPZHy71HbhvbFEoIvsk+BEjXsun12G3s2zGdo9E7ApiVCrlR1hnLXQ1XiyWskYaWHVX9q56xB4B7L+90KmsIqY8e4CdWoFs90mE5BVRrYLYBdyLwIpJF0CUEjKGmP0+5GvYsvpUudsO7uRHyenGP1phXpAEZsYXaQmh2b4Z5bxoU1EIxYediZ1NUe5eGBYuDL3zgSO1Q6sdHUCljQoj0SBH7CtwZMYIvblpSoPKPUnnEyKAHPjDGcKBiRxH6VGX+UGnAlYaJR7HREr5CZfiTwBfAH4r1LeqI4TYcP0Jw1mqIBXCR19RiTHQiNvhOzMp9jnjruzSWUj1jnkZxaoFoyhBlmSMO4iUaIlAp9kVp1q5reBXjKZ+PbM+uqRE8yyuI8pxz9nqONU5dpljzoOCnVMYv8d4VlQQr6dLUAmOkbHngDvEdGg4Uie2JU2cDcbD0o3nrIWwpMvhkFsGXevEsR+GqLs1t1Ej8yHW83cCxNXO6ESvYfyIo7H4Z5ZReRONdYpDCvjz7NbuQcGYD1rQx8a6TL1Q8XLJPtVY8ZtSlcwN+Pb5WoX2j/n2lFldYF74Lsd6lrLvBy6QqDiPK5rN+x0FJh15yY+8a8xz4/ygRSOjadyS6dTtoxr0RKuWSUwiJWhc2WgfX9j/Qy6nqeC8sSzJeby3BJ1sPfzSd2I0OblqJgHGuAHZkaFF67CrHsvdOtvrfwy6WvgpC6HJB2lGgF4k7uCqazZ5IsVSgRs88/ItqwDE9ygT5CG9RFt7h7tvowIGd35juaaMejMnYbBxq8IZuDzey9s2skE+EsCyKSHvEmUaHiDdAjIbpzjbiRuIUfLV5GGUJo88qV+qOShhZ8zq4/Ych3X9FAuZhsJeId2BQ8RL8vGCpmTbUhgZEJKnM2dm6+nkYiAjOqLWaBBA6DdRnb8mlA2SYDfB57uCa37CCAbQHKfAQDu7D14kXlKLQHGkvSF/QQnPITqlwSQnjblTCEpBWV963jZoRcVpBNdSrtqwm7agNfyYKYR36Z7GZgNgIbvzryOeOipe/4Xk0vmghk1e82tQZknnFZZj+zIG3yZi0Rxnw245+QPN053yBMefMpdj1RCaoLJmUJSCsr6FkYf6wZPxfhKqT4KSQB1skYnIEC5Ae0aXz1HsttZRMMAcd9EVLIWdsJZ4JDFJ2AGwckUuo5LuDcJnfjlLTIZ4iPDZ289Ri9hFDnPA2szKuG1b4LJmUKS+Q1L1CS0mcoVHJPInDlWGGs3ncduOoGLOjd5ch6hjNeS4+tnkQ0bTkKQsLRTz+msrYCa3xMNGu0GISC9FxosMA1Yoi6YI6HLmJiC+TdueE28kGHsNpUoFkXEPIZArDROCkoOr/taEcaSkylIwF662FAemD1tJir3S7smZoW26ygCRmvL29uE9j+gbr3GHQCi+r+Cl2cmzhYFsq+LgNEUzn1bD7u4G1w451QG6IQ61/Ycx6Wr8DpxgPQxyXWqly58LzK8+8A8T+LdonF0I1mMyvelD6ALn3AyXiXaXIn6VJinCeuD3T3+TbX3ZDBzQG2Fiu8Lqr6xmQ1ml/uWb3/FFIYaG9rkBjEVFZVRtt6iXbYvsX6jwczj21fm6RtsbnEBYfgNAJ+b0/R4xvnPzoZUVF44mJ2gC/kiGXlWMCNYW+FkjxdINaFD7hXAVF6Ikfr2xM4N/TJk11XipDD53tUA8mDkLyK6Z35Lfbf9HkMIk4KZisqBwMgGHReYfEC10F6LFjeIP8S4BulskPwWJEXBTOUlglPpASIXlBSQXrY87m45ZNxbwUzlW7hyA5NyPzvWnvrhBNK46kO4rQpmKrmBqnT+FiZvvEjl+xPI2VxOzdIUzFRiwGr4W+B1qDeUq/wYcjXyZS4KZioiwBrAymZWClYquQUWEqop3E4Fs58LsAZGVTosS91AmdhnuqXIduT9xsSlhFD3f+vxjjm+S8HsJwet0lL6AbB+RIblAxcqd4rLtzK628Q76fmEysmjPl8wgLYi9ikrmP1kCjcoz9wBre+BZblJnz6gccFlN/KNSSova7J9HPec7EzB7PtjWuULAi0XlGwA2hnr9WPKfFSmliPtghdJ78tv6B5uPGzpyf+6BU1FwUzFBi7bRRzAa6qk0b3FkjoPc9rqcT4qCmYqMcA1XDm34gxMalgp+wJS6tqpKJipvBTgujNPD64bgEpdPRUVBbOswFVYoFUmANfGcf06ZVQqKgpmhwCv0nyNb3EvZR1kiFVtbYalYKWiomB2aNZVWqzrLOAKdhZgaU6UisqBRfPMvoLXAFqlh3XZDGungKWiomD2El3GAbwgLWJjvgbZB8DSQLuKioLZiwIv2P6zMl+PshlAq1OWpaLy/cv/BRgAO/D21W38A0AAAAAASUVORK5CYII="
          height="25px" width:25px></td>
          <td class="td_border" rowspan="2">
          Form : <span class="bold_font">' . $shipment->name . ' (' . $shipment->customerid . ') </span><br/>
          Tel.  <span class="bold_font"> ' . $pickup->phone . '</span><br/>
          Ship to:<br/>
          Country :  <span class="bold_font">' . $shipment->country . '</span><br/>
          </td>
          </tr>

          <tr>
          <td class="center_text td_border"><div style="left:398px;padding-top:10px;"><barcode code="' . $code . '" type="EAN128C" style="color:black" /></div>
          <span style="pedding-left:10px;color:black">SME No. <b>' . (strlen($code) == 6 ? substr($code, 0, 2) . ' ' . substr($code, 2, 2) . ' ' . substr($code, 4, 2) : $code) . '<b></span><br/></td>
          </tr>

          <tr>
          <td colspan="2" class="center_text td_border"><span style="padding-left:10px" class="bold_font">' . $shipment->servicename . '</span></td>
          </tr>

          <tr>
          <td class="td_padding" style="padding-left:20px">
          <span style="padding-left:10px">Parcel No. <span class="bold_font">' . (str_pad(($i + 1), 3, '0', STR_PAD_LEFT)) . ' / ' . (str_pad($box, 3, '0', STR_PAD_LEFT)) . '</span></span> <br/>
          <span style="padding-left:10px">Date.<span class="bold_font">' . date('d/m/Y', $shipment->delivery_date) . '</span></span> <br/>
          </td>
          <td class="td_padding" style="padding-left:0px">
          <span style="padding-left:10px">Booking No.  <span class="bold_font">' . (str_pad($shipment->inv_id, 6, '0', STR_PAD_LEFT)) . '</span></span>  <br/>
          <span style="padding-left:10px">Total shipment:  <span class="bold_font">' . (str_pad($shipmentAll, 3, '0', STR_PAD_LEFT)) . '</span></span>  <br/>
          </td>
          </tr>
          </table>
        </div>
        ';
                $line++;
                if ($line == 2) {
                    $html .= '<div style="padding-top:5px;padding-bottom:5px;font-size: 14pt;width:100%"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKwAAACKCAYAAAA3xKHzAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAMVElEQVR42mL8//8/AzJgZGRkIAMYALEDECtA2bjABTQ8CkYBCkBPj+gAIIDACpAxCQCUQBcA8QeQPWTgB0A8AZrIR8EowJoe0TFAAJGTYEEJ9QCZiRQXPkCgZB4FowkWjAECiJQECyoJN1A5oaJjUIkrMBptowkWFwYIIGITbAAFVT+p+MJoaTuaYHFhgAAiJsEW0CmhIuMPo4l2NMFiwwABRCjBLhiAxDqaaEcTLE4MEED4EmzDACbW0UQ7mmCxYoAAwpVgEwZBYkUe/hrtiI0mWDAGCCBGLBMHCtCOD/8g8sdGaMdvFAw9ACpsHKDYAIo/QAsiED4AHX36AEuw+ABAAGErYQ9Q0LsvgDoMHThA5S5QUNI6jMb9kAIBDMQPg36ANkEFCJWwAAGEbgk5TYEDJCYmBzIT7oPRNDAkStMCaFyR22fBW5MCBBA6ItWiBgo8R06nLmE0TQzahNrAQL2xepzpCiCAKCldqZF4SLVzdMHM8E6oyHgBNgsBAggZXRigko7UiYnRYa7BAQoYaD/7iVHSAgQQDClQM+WTAUhZozBhNK0MKEigoI1KDkZp0wIEEKlV8wcG2oyJkpJhRpsFAwMcGKi/So/YNKcAcwRAAMHQAjp0sgiBBSR4YnQigb7t1AUMAzt5dADmGIAAIrX9qkDDgDFgGB2THYntVGIxyC0MAAEEQ4OlKiY2cBpG0xLNq/8LDINneh7e4QYIICYSPEGPBEusHQqjaYpm1T+oU7sfiPUHmdsegtwHEECkJNgHowl2WIMAaBznDzJ3fQTiRmicHwAIIJZBFvkfRtMN3YECtFNlPwjdthCpHQ0GAAHExDA6Rz+SAagvcH8QJtaDQGzIABluRSnEAAKIlE7XAjo49AC5wxyjgORO1YNB2Kl6wEBgFhUggAbbKMGD0QRL807VhkGYUOHLCwl5ACCASE0otGzvkjIOO5pgSQeDaUwVveYmOl0BBBCpVXEDDQN0wmiCpVlBMBjHVA8wkDEBBBBAyI3vgVxLIEBi7h9NsMSF6QSGIdxOxQYAAgi5ET5UVmuNJljCIGAQdqpIbqdiAwABhIxIKeESqBi4CQw0WNw7QoECw8CsqKJJOxUbAAggZLSAYfDvOBhdS4C/WTfYOlUUtVOxAYAAQs+d9Ew4DQw03vMzOqY6tNup2ABAAFHaliR31yyl1dZogh0c61Rp3k5FBwABRI1Sll7nEowm2ME9pkqTdio6AAggXIHxf5DjkZpgDQZhp4qm7VR0ABBA1Gwa0BNvGIHVfwPD4Kv+E+gdEAABhC+ALgziBPtgdEx1wGu4AdlXBxBAQznRKgzzhKowCGu6DQMd7gABRIvxWXrhhGGcWAfbmOoFhkGy8RMggEiplgZbr3Q4nk/gMMhqtQ+DrWAACKChPu7nMEwS6mBcqDIo26kAAURu22qwJNwDwyA8EwZZ7TWo26kAAURpqUDNyYCR1pZVGGRjqkOinQoQQNQM/IIB6tV+GGIlwmAbUx1S7VSAAKJVhARAmw0P6Fg6DIX2lsMgG1Mdcu1UgACiBzKgU9NhweiY6vBvpwIE0EBE3AQaljKDMdEOpoUqQ76dChBAA907fkCj0mMwVHODafPfsGmnAgTQcG3XDeQFy4NtTHVY3ZAOEEDDueociFJlMM0IHhiO7VSAABqMIwwbhmDEDaYx1QfDuZ0KEECDuX1L7dKWVkM4g2VM9QMD2inVwxEABNBgHw67QIPSh1rNhME0pjpi2qkAATQUmgi06GkfoKDaHEwHqo24dipAAI3Udi25G+cGy5jqiG2nAgTQUEK06tQQ074dLJv/Rnw7FSCARpsH+Nu3g2lMdbSdCgQAATQUEy2tq2RY+3awbP4bbaciAYAAYhpi7v1A47bbRyQ7YKMUHwfIr6BrfhyRRiNGARAABNBQRQ006HwFEGjDLhhtpw48AAigoYyo0Z4ltbdN6wUto+1UAgAggIYyMmAYmEXftOj8HWAYuMU6QwoABNBQR+RW05Ruq1GgUufvAYGmyChAAwABNFJHDRoGuB39gWH0BEayAEAADQfkQMbwEzXaiQoUdPBG26lkAoAAGokjBweoaCcpGWW0nUoFABBAw7EjRqhDRM2q+MBoO5W+ACCARmJpS68EO9pOpQEACCCmYegngUFU9T4YTWLUBQABNNwQMcv/6N0kGN1aTUUAEEDDqe1KTOKhdqeLlCG1BQyji1goBgABNByq/wYG0oeWBKiUSUbHX+kMAAJoOHeuaN0sWMAwOsNFdwAQQEO9dKVkepSSjpkDw+ha1wEBAAE0EktX5OqZnERrwED9ReSjq7SIBAABNFJLV3LXniYw0G7Hw+j5V0QAgAAaqmgBA/X3czXgKHEFoAmJXge7jQ6D4QEAATQUkQMD7Vf8b4DiBwwDt5drdBgMCwAIoJHaFCCmPanAMPAHZowOg6EBgAAaaoiW1fIBHE2CwbB7dnQYDAoAAmgkt1tJSQyD5XyCET8MBhBAIz2xNpA4nOTAMDhO1R6xw2AAATRSEyull1I0MIweWzQgACCABnsHixbHbVJryEiBYXCctzWihsEAAmgwD11RezSAVr3twXKi4Yi4yggggAZjqUrtJgA9OiqD5cxYWp40PigAQAANpoTawED9Y+Lp3cYbLAfIDdthMIAAGo4JFdauUxhAPw2WIzpBtcuw2qkLEEADhQJoOFQ1YZD4cTBdLDdshsEAAoiepU4CNJGOtNVODQyDo1M2LIbBAAKI1j39BjqVMuSubaUXGCxDYEN+GAwggIZaKToUEysySGAYPDclDslhMIAAolZJumAAI8JhCGbswXJt0pAbBgMIIEpLi4EewmkY4hn9wSBJuENmGAwggIZqQoUF8lAvGcjdpj5ih8EAAmioDtMM9dJ1sIftoB0GAwggUoZm/g8yPBw7DINlXcKgHQYDCCBiqqwLgzCxXhjGfYPBNAQ26IbBAAKIUDX1YBAmVtgGveEOAgZRaTtohsEAAghfYv0wSBPrcGu/EqrhFgyicB/wYTCAAMIVSA8GcWL9zzDyViI5DLI4GbBhMIAAwoao0X46gIZH+mTBcBwCG5BhMIAAwtZLJbeqmEDA8QZQNR9GEyzFzbUDgyzh0m0YDCCA0Hun5N55JUBiSTFhNMEOqyEwug2DAQQQMlrAQN9FJ5T0gkcTLKKQ2cAw+IYcaRY/AAGEXOoNxAopckcjEkbT6qAeAqPZMBhAAJHTdqX2cr6A0WEtqhU6EwZZoqX6MBhAAJEzMkCLdsqE0QQ7bIfAqDoMBhBAMDTQU6Kknko4mmAJg8E2BEaVYTCAAILlyMHQdmwg0eOjgLhO2YFBmHDJHgYDCCBS2q8f6BC4owl2ZAyBkT0MBhBApJRs9EgkD0YTLE07ZRsYBufKO6KHwQACiJS7ZumVYEcBbcAHaMcnEIgfDiJ36QPxfmKHwQACaLBdjjxactIebIB2fCYOMnf5Q0tbvMNgAAHENBp/I7a0BbUfHYH44iByFz8Q10MTLtYOPkAADbYEO3qcDv1rNFBp2wjEHweRu+SBeD4DlmEwgAAiJcEq0MGhoweXDQxogIb9wUHmLnsgPs8AWecCLswAAogBWvQOln1UxA69jLZ1aQcG4xAYfFsUQAAxMJA2cUDLUpaUNQUNo+mK5k2zBQyDa8wWXMICBBAMDYajLEkJoITRNEUXACrMHjAMoi1RAAGE3PgmNqXTopQ1INEDCqNpie5t3IFqJmxAdghAACG3WwZyWpSUsw8ujKafAQEKDPSfKXuA3hQACCDkNstAnQtAaltp9FDegW8m0OtwFYxRI4AAoiThLBiAxPqBYXSsdrCABBo3E7D2UwACCL3IJ6d9QU4CEmAgb9nb6OjA4BtNoEX7FmdiBQggbI1rWm6DoMSDD0ZL1xGRcPEmVoAAYsRiMah9Ik+mwzdC9R/A0u4BtUf8KQgUR4bRCYOhkHALoJifRL0PoYkVbxwDBBCuIabBNtMxYTQtDLmEm0Bk54ykjYoAAYSvQT2YtguPgqELDKAJ8gADYhLiADReE0hNqAABNNgT7YXRdusoQAYAATSYE+2B0cQ6CtABQAANhvG2kXpg8SggAwAEECntEHrdaJgwGtyjABcACCBSe34NDEP0LKZRMDwAQACRgxQYqLtWEpRQHUaDdRQQAwACiNISN4GBvBU8oOZFwWiJOgpIBQABxEhFsxygCRBXIvzAgH0WbBSMAqIBQIABANixgsk5riwIAAAAAElFTkSuQmCC" width="9pt">---------------------------------------------------------------------------------------</div>';
                    $line = 0;
                }

            }
        }

        $html .= '

</body>
</html>
';

        $mpdf->WriteHTML($html);
//$mpdf->Output();
        return $mpdf->Output();
    }

    public function address_customer($id)
    {
        $shipment_all = DB::table('app_manifest_detail')
            ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
            ->leftJoin('pickup_manifest', 'app_manifest_detail.p_id', '=', 'pickup_manifest.id')
            ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
            ->where('app_manifest_detail.inv_id', $id)
            ->select('pickup_manifest.type_delivery', 'app_manifest_detail.m_id as customerid', 'pickup_manifest.name', 'pickup_manifest.delivery_date', 'app_services.name as servicename', 'app_country.country', 'app_manifest_detail.*')
            ->get();

        $mpdf = new \Mpdf\Mpdf();

        $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new \Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                __DIR__ . '/fonts',
            ]),
            'fontdata' => $fontData + [
                    'thsarabun' => [
                        'R' => 'THSarabunNew.ttf',
                        //'I' => 'THSarabunNew Italic.ttf',
                        //'B' => 'THSarabunNew Bold.ttf',
                    ],
                    'prompt' => [
                        'R' => 'Prompt-Regular.ttf',
                    ]
                ],
            'default_font' => 'prompt',
            'mode' => 'utf-8',
            'format' => 'A4-L',
        ]);

        $html = '<html>
<head>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">

<style>
body {
  font-size: 9pt;
  margin: 0px;
}
.center_text{
  text-align: center;
}
.bold_font{
  font-weight: bold;
}
.size_paper{
  padding: 30px;
  padding-bottom: 40px;
  width: 12cm;
  border: 1px dashed;
}

.td_padding{
  padding: 0px 10px 1px 30px;
}
.td_width{
  width: 45%;
}
.sme_no{
  padding: 0px 0px 0px 20px;
}
.box-1{
  padding:5px;
  width:48%;
  float:left;
}
</style>
</head>
<body>';
        $line = 0;
        foreach ($shipment_all as $shipment) {
            $box = DB::table('app_manifest_box')->where('shipment_id', $shipment->id)->where('status', '<', 10)->count();
            $shipmentAll = DB::table('app_manifest_detail')->where('inv_id', $shipment->inv_id)->where('status', '<', 10)->count();
            $pickup = DB::table('pickup_manifest')->where('id', $shipment->p_id)->first();
            $number = $box;
            $code = $shipment->tracking_code;
            for ($i = 0; $i < $number; $i++) {
                $html .= '
    <div class="box-1">
      <table class="size_paper" >
        <tr>
          <td  class="td_padding td_width">
                <span class="bold_font">FROM</span> <span>' . $shipment->name . '</span><br/>
          </td>
          <td></td>
        </tr>
        <tr><td class="td_padding td_width"><span>' . $pickup->address . '</span></td><td></td></tr>
        <tr><td class="td_padding td_width"><span>Phone <span>' . $pickup->phone . '</span></span></td><td></td></tr>
        <tr><td class="center_text td_width"><barcode code="' . $code . '" type="EAN128C" style="color:black" /></td><td></td></tr>
        <tr><td class="center_text td_width"><span>SME No. <b>' . (strlen($code) == 6 ? substr($code, 0, 2) . ' ' . substr($code, 2, 2) . ' ' . substr($code, 4, 2) : $code) . '<b/></span></td><td></td></tr>
        <tr>
          <td></td>
          <td  style="padding-top: 30px;">
                <span class="bold_font">TO </span> <span> บริษัท เอสเอ็มอี ชิปปิ้ง จำกัด</span><br/>
                <span>(ส่งของต่างประเทศ)</span><br/>
                <span>46-48 ซอยรัชดาภิเษก 16</span><br/>
                <span>ถนนรัชดาภิเษก</span><br/>
                <span>แขวงวัดท่าพระ เขตบางกอกใหญ่</span><br/>
                <span>กรุงเทพ 106000</span>
          </td>
        </tr>
      </table>
    </div>
    ';
                $line++;
                if ($line == 2) {
                    $html .= '<div style="page-break-before:always">&nbsp;</div> ';
                    $line = 0;
                }

            }
        }

        $html .= '

</body>
</html>
';

        $mpdf->WriteHTML($html);
//$mpdf->Output();
        return $mpdf->Output();
// return $html;
    }

    public function dropOff($id)
    {
        $shipment_all = DB::table('app_manifest_detail')
            ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
            ->leftJoin('pickup_manifest', 'app_manifest_detail.p_id', '=', 'pickup_manifest.id')
            ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
            ->where('app_manifest_detail.inv_id', $id)
            ->select('pickup_manifest.type_delivery', 'app_manifest_detail.m_id as customerid', 'pickup_manifest.name', 'pickup_manifest.delivery_date', 'app_services.name as servicename', 'app_country.country', 'app_manifest_detail.*')
            ->get();

        foreach ($shipment_all as $shipment) {
            $box = DB::table('app_manifest_box')->where('shipment_id', $shipment->id)->where('status', '<', 10)->count();
            for ($i = 0; $i < $box; $i++) {
                if ($shipment->type_delivery == 'Drop off ไป') {
                    $dropoff = ThaiPostEMSNumber::query()->where('manifest_id', $shipment_all->first()->inv_id)->get();
                    if ($dropoff->count() > 0) {
                        $t = new ThaiPost();
                        $t->label($dropoff->pluck('number')->all());
                    };
                }
            }
        }
    }
}
