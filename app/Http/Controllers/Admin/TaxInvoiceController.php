<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\TaxInvoice;
use File;
use Image;
use Illuminate\Support\Facades\Input;

class TaxInvoiceController extends Controller
{
    public function show()
    {
        return view('customer/app/tax_invoice');
    }
    public function add_tax(Request $request)
    {
        $taxinvoice = new TaxInvoice;
        // $files = $request->file('files');
        // $filename = null;
        // if($request->hasFile('files'))
        //     {  
        //         foreach ($files as $index=>$fileitem) {
        //             $file = $fileitem->getClientOriginalName();
        //             $fileName = pathinfo($file,PATHINFO_FILENAME);
        //             $imageName = time().'_'.$fileName.'.'.$fileitem->getClientOriginalExtension();
        //             $fileitem->move(public_path('/uploads/taxinvoice'), $imageName);
        //             $img = Image::make(public_path('/uploads/taxinvoice/').$imageName);
        //             $img->resize(null, 1000, function ($constraint) {
        //                 $constraint->aspectRatio();
        //             });
        //             $img->save(public_path('/uploads/taxinvoice/').$imageName);
        //             $filename =$filename.($index!=0?',':'').$imageName;
        //         }
        //     }
        $taxinvoice->file_name	    = "";
        $taxinvoice->status	        = $request->status;
        if($request->status == "รับ"){
        $taxinvoice->status_name	= $request->status_name;
        $taxinvoice->wht            = $request->wht;
        if($request->name_branch != null || $request->name_branch != ""){
            $taxinvoice->status_subject = $request->status_subject.' '.$request->name_branch;
            }else{
            $taxinvoice->status_subject = $request->status_subject;
            }
        }else{
        $taxinvoice->status_name	= "";
        $taxinvoice->wht            = "";  
        $taxinvoice->status_subject = "";
        }
        $taxinvoice->name           = $request->name;
        $taxinvoice->tax_number     = $request->tax_number=="undefined"?"":$request->tax_number;
        $taxinvoice->phone          = $request->phone;
        $taxinvoice->status_address = $request->status_address;
        $taxinvoice->address        = $request->address;
        $taxinvoice->email          = $request->email=="undefined"?"":$request->email;
        $taxinvoice->save();
        return $request;
        // return redirect()->back()->with('alert', 'Deleted!');
    }

    public function index(Request $request)
    {
        if($request->keyword!=null){
            $customer = DB::table('tax_invoice')
            ->where('phone', 'like', '%'.$request->keyword.'%')
            ->orWhere('name', 'like', '%'.$request->keyword.'%')
            ->orWhere('email', 'like', '%'.$request->keyword.'%')
            ->orWhere('status', $request->keyword)
            ->orderBy('id','desc')
            ->paginate(20);
           }
           else{
            $customer = DB::table('tax_invoice')
            ->orderBy('id','desc')
            ->paginate(20);
           }
        return view('admin/quotation/show_taxinvoice',compact('customer'));
    }

    public function edit(Request $request)
    {
        $taxinvoice = TaxInvoice::where('id', $request->id)->first();
        $taxinvoice->file_name	    = "";
        $taxinvoice->status	        = $request->status;
        $taxinvoice->name           = $request->name;
        if($request->status == "รับ"){
        $taxinvoice->status_name	= $request->status_name;
        $taxinvoice->wht            = $request->wht;
        if($request->name_branch != null || $request->name_branch != ""){
            $taxinvoice->status_subject = $request->status_subject.' '.$request->name_branch;
            }else{
            $taxinvoice->status_subject = $request->status_subject;
            }
        $taxinvoice->tax_number     = $request->tax_number=="undefined"?"":$request->tax_number;
        $taxinvoice->phone          = $request->phone;
        $taxinvoice->status_address = $request->status_address;
        $taxinvoice->address        = $request->address;
        $taxinvoice->email          = $request->email=="undefined"?"":$request->email;
        }else{
        $taxinvoice->file_name	    = "";
        $taxinvoice->status_name	= "";
        $taxinvoice->wht            = "";  
        $taxinvoice->status_subject = "";
        $taxinvoice->tax_number     = "";
        $taxinvoice->phone          = "";
        $taxinvoice->status_address = "";
        $taxinvoice->address        = "";
        $taxinvoice->email          = "";
        }
        $taxinvoice->save();
        return redirect()->back()->with('alert', 'Deleted!');
    }

    public function delete($id)
    {
        $taxinvoice = TaxInvoice::where('id', $id)->delete();
        return $taxinvoice;
    }

}
