<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ServiceType;
use Illuminate\Support\Facades\DB;
use App\ManifestDetail;
use App\ManifestDetail_box;
use App\Manifest;
use App\Pickup;
use App\Employee;
use App\Pickup_SO;
use App\Attachment_Payment;
use App\ManifestHistory;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Mail;
use App\Address;
use App\ManifestDetail_Other_Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function emailpayment($id)
    {
        $uid = uniqid();
        $data = DB::table('app_manifest')
            ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
            ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
            ->leftJoin('app_customer', 'app_customer.id', '=', 'pickup_manifest.m_id')
            ->where('app_manifest.id', '=', $id)
            ->select('pickup_manifest.*', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'users.name as salename', 'app_customer.service_group')
            ->first();
        $shipment = DB::table('app_manifest_detail')
            ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
            ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
            ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.description as service_name',
                'app_services.cal_weight', 'app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight')
            ->where('app_manifest_detail.inv_id', '=', $data->manifestid)
            ->orderBy('app_manifest_detail.id')
            ->get();
        $price = 0;
        foreach ($shipment as $shipitem) {
            $other_service_name = collect();
            $other_service_price = collect();
            //Express
            //Check price is 0
            $price = $price + $shipitem->price;
            //Other Service
            $other_service_total_price = 0;
            $other_service_list = ManifestDetail_Other_Service::where('shipment_id', $shipitem->id)->get();
            foreach ($other_service_list as $key => $value) {
                $other_service_name->add($value->other_service_name);
                $other_service_price->add($value->other_service_value);
                $other_service_total_price = $other_service_total_price + $value->other_service_value;
            }
            $shipitem->other_service_name = $other_service_name;
            $shipitem->other_service_price = $other_service_price;
            $shipitem->other_service_total_price = $other_service_total_price;
            $price = $price + $other_service_total_price;
        }
        $price = $price - $data->discount;

        //สร้าง token สำหรับลูกค้ากดดูรายละเอียด shipment จากเมล์ โดยไม่ต้องล็อกอิน
        DB::table('auth_token')->insert(
            [
                'token' => $uid,
                'user_id' => $data->m_id,
                'expiry_date' => date("Y-m-d H:i:s", strtotime("+1 years")),
                'program' => 'Email Customer',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'active' => 1
            ]
        );

        //สร้าง QR-Code สำหรับ Payment
//        $client = new \GuzzleHttp\Client();
//        $request = $client->post(env('SCB_API_URL') . "/v1/oauth/token", [
//            'headers' => [
//                'Content-Type' => 'application/json',
//                'resourceOwnerId' => env('SCB_API_KEY'),
//                'requestUId' => (string)Str::uuid(),
//                'accept-language' => 'EN'
//            ],
//            'body' => json_encode([
//                "applicationKey" => env('SCB_API_KEY'),
//                "applicationSecret" => env('SCB_SECRET_KEY')
//            ])
//        ]);
//        $authenToken = collect(json_decode((string)$request->getBody()));
//
//        $request = $client->post(env('SCB_API_URL') . "/v1/payment/qrcode/create", [
//            'headers' => [
//                'Content-Type' => 'application/json',
//                'Authorization' => implode(' ', [
//                    $authenToken->toArray()['data']->tokenType,
//                    $authenToken->toArray()['data']->accessToken
//                ]),
//                'resourceOwnerId' => env('SCB_API_KEY'),
//                'requestUId' => (string)Str::uuid(),
//                'Accept-Language' => 'EN'
//            ],
//            'body' => json_encode([
//                "qrType" => "PP",
//                "ppType" => "BILLERID",
//                "ppId" => env('SCB_MERCHANT_ID'),
//                "amount" => number_format((float) $price, 2, '.', ''),
//                "ref1" => "SME" . $id,
//                "ref2" => $id,
//                "ref3" => "SMEQR"
//            ])
//        ]);
//        $generatedQR = collect(json_decode((string)$request->getBody()));

        return view('admin/request/paymentemail')->with(compact('data', 'shipment', 'price', 'uid'));
    }

    public function sendemailpayment(Request $request)
    {
        $check = DB::table('auth_token')->where('token', $request->token)->count();
        $manifest = Manifest::where('id', $request->bookid)->first();
        $so = ManifestDetail::where('inv_id', $manifest->id)->pluck('so')->implode(', ');
        if ($check > 0) {
            $data = array(
                'email' => $to = explode(',', $request->email),
                'booking_id' => $request->bookid,
                'html' => $request->detail,
                'so' => $so
            );

            Mail::send('mails.mailpayment', $data, function ($message) use ($data) {
                $message->to($data['email'])
                    ->from('notification-no-reply@smeshipping.com', 'SME SHIPPING')
                    ->subject('SME SHIPPING แจ้งค่าบริการจัดส่งเลขที่ ' . $data['booking_id'] . ' / SO:' . $data['so']);
            });

            $history = new ManifestHistory;
            $history->topic = 'Send Email & SMS';
            $history->detail = 'ส่งเมล์ และ SMS แจ้งราคาลูกค้า';
            $history->update_by = Auth::user()->id;
            $history->inv_id = $request->bookid;
            $history->save();

            if ($request->sms != null) {
                $tellist = explode(',', $request->tel);
                $tellist_array = array_map('trim', $tellist);
                foreach ($tellist_array as $key => $value) {
                    if (strlen($value) == 10) { //เช็คว่าเบอร์ครบ 10 ตัวหรือไม่
                        $tel = preg_replace('/0/', '66', $value, 1);
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "https://api-service.ants.co.th/sms/send",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => '{"messages":[{"from":"SMESHIPPING","destinations":[{"to":"' . $tel . '"}],"text":"' . $request->sms . '"}]}',
                            CURLOPT_HTTPHEADER => array(
                                "Content-Type: application/json",
                                "Authorization: Basic U01FX1Byb2R1Y3Rpb246WEdlUTE1SnU="
                            ),
                        ));
                        $response = curl_exec($curl);
                        curl_close($curl);
                    }
                }
            }
            return redirect('/admin/request/' . $request->bookid);
        } else {
            return "ส่งเมล์ไม่สำเร็จ";
        }


    }
}
