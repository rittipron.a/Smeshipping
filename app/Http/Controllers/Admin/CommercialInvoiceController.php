<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Address;

class CommercialInvoiceController extends Controller
{

    public function create()
    {
        $country_all = Address::select('country_name', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        return view('admin/commercialinvoice/index', compact('country_all'));
    }

    public function list()
    {
        return view('admin/commercialinvoice/list_commercial');
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        
    }

    public function manage()
    {
        $country_all = Address::select('country_name', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
            
        return view('admin/commercialinvoice/manage', compact('country_all'));
    }

    public function manage_store(){
        
    }



    public function view_pdf()
    {
        return view('admin/commercialinvoice/pdf_commercial');
    }

    public function sendemail()
    {
        // $data = Quotation::where('id', $id)->first();
        // $data_array = array(
        //     'qot_no' => $data->quote_no,
        //     'name' => $data->name,
        //     'email' => $data->email,
        //     'qot_url' => 'https://backend.smeshipping.com/admin/quotation/' . $data->id
        // );

        // $backup = Mail::getSwiftMailer();

        // // Setup your gmail mailer
        // $transport = new Swift_SmtpTransport('smtp.gmail.com', 587, 'tls');
        // $transport->setUsername('ba1@smeshipping.com');
        // $transport->setPassword('');
        // // Any other mailer configuration stuff needed...
        // $gmail = new Swift_Mailer($transport);

        // // Set the mailer as gmail
        // Mail::setSwiftMailer($gmail);

        // Mail::send('mails.Quotation', $data_array, function ($message) use ($data_array) {
        //     $message->to($data_array['email']);
        //     $message->from('ba1@smeshipping.com', 'SME SHIPPING');
        //     $message->subject('SME SHIPPING แจ้งใบเสนอราคาเลขที่ ' . $data_array['qot_no'] . ' ของคุณ ' . $data_array['name'] . '');
        // });
        // Mail::setSwiftMailer($backup);
        // $now = strtotime(date('Y-m-d H:i:s'));
        // $data->send_email = $now;
        // $data->save();
        // return redirect()->back()->withErrors(['ส่งอีเมล์เสร็จสิ้น']);
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        
    }

    public function delete($id)
    {
        
    }

}
