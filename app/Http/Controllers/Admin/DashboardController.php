<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ServiceType;
use Illuminate\Support\Facades\DB;
use App\ManifestDetail_box;
use App\ManifestDetail;
use App\Manifest;
use App\Employee;
use App\Pickup;
use App\ManifestHistory;
use App\Pickup_SO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
 public function __construct()
 {
      $this->middleware('auth');
 }
 public function summary(){
     $ldate = strtotime(str_replace("/", "-",date('d/m/Y')));
     $waitforsale = DB::table('app_manifest')
     ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
     ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
     ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
     ->where('app_manifest.status', '=', 0)
     ->select('pickup_manifest.*','pickup_manifest.id as pickup_code', 'app_manifest.*',
     'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename')
     ->orderBy('pickup_manifest.delivery_date', 'DESC')
     ->get();
     $waitforpaytoday = DB::table('app_manifest')
     ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
     ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
     ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
     ->where('app_manifest.status', '=', 4)
     ->select('pickup_manifest.*','pickup_manifest.id as pickup_code', 'app_manifest.*',
     'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename')
     ->orderBy('pickup_manifest.delivery_date', 'DESC')
     ->get();
    
     $pendding = DB::table('app_manifest')
     ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
     ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
     ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
     ->where('pickup_manifest.delivery_date','<=', $ldate)
     ->whereIn('app_manifest.status', [1,2,3,5])
     ->select('pickup_manifest.*','pickup_manifest.id as pickup_code', 'app_manifest.*',
     'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename')
     ->orderBy('pickup_manifest.delivery_date', 'DESC')
     ->get();
     $success = DB::table('app_manifest')
     ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
     ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
     ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
     ->where('pickup_manifest.delivery_date', $ldate)
     ->where('app_manifest.status', '=',5)
     ->select('pickup_manifest.*','pickup_manifest.id as pickup_code', 'app_manifest.*',
     'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename')
     ->orderBy('pickup_manifest.delivery_date', 'DESC')
     ->get();

      return view("admin/dashboard/summary")->with(compact('waitforsale'))->with(compact('waitforpaytoday'))->with(compact('pendding'))->with(compact('success'));
      //return $data->toJson();
 }

 public function summaryall(){
     $ldate = strtotime(str_replace("/", "-",date('d/m/Y')));
     $new = DB::table('app_manifest')
     ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
     ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
     ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
     ->select('pickup_manifest.id as pickup_code', 'app_manifest.status','pickup_manifest.name','pickup_manifest.type_shipping','pickup_manifest.delivery_date','app_manifest.id',
     'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename')
     ->whereIn('app_manifest.status', [0, 1])
     ->orderBy('pickup_manifest.delivery_date', 'DESC')
     ->get();

     $pickup = DB::table('app_manifest')
     ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
     ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
     ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
     ->select('pickup_manifest.id as pickup_code', 'app_manifest.status','pickup_manifest.name','pickup_manifest.type_shipping','pickup_manifest.delivery_date','app_manifest.id',
     'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename','pickup_manifest.type_delivery_note')
     ->where('app_manifest.status', 2)
     ->orderBy('pickup_manifest.delivery_date', 'DESC')
     ->get();

     $cal = DB::table('app_manifest')
     ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
     ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
     ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
     ->select('pickup_manifest.id as pickup_code', 'app_manifest.status','pickup_manifest.name','pickup_manifest.type_shipping','pickup_manifest.delivery_date','app_manifest.id',
     'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename','pickup_manifest.type_delivery_note')
     ->where('app_manifest.status', 3)
     ->orderBy('pickup_manifest.delivery_date', 'DESC')
     ->get();

     $pay = DB::table('app_manifest')
     ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
     ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
     ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
     ->select('pickup_manifest.id as pickup_code', 'app_manifest.status','pickup_manifest.name','pickup_manifest.type_shipping','pickup_manifest.delivery_date','app_manifest.id',
     'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename','pickup_manifest.type_delivery_note',
     'app_manifest.payment_file_re','app_manifest.discount','app_manifest.price_total','app_manifest.price_pay')
     ->where('app_manifest.status', 4)
     ->orderBy('pickup_manifest.delivery_date', 'DESC')
     ->get();

     $release = DB::table('app_manifest')
     ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
     ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
     ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
     ->select('pickup_manifest.id as pickup_code', 'app_manifest.status','pickup_manifest.name','pickup_manifest.type_shipping','pickup_manifest.delivery_date','app_manifest.id',
     'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename','pickup_manifest.type_delivery_note',
     'app_manifest.payment_file_re','app_manifest.discount','app_manifest.price_total','app_manifest.price_pay')
     ->where('app_manifest.status', 5)
     ->orderBy('pickup_manifest.delivery_date', 'DESC')
     ->get();

     foreach ($release as $value) {
          $value->box = ManifestDetail_box::where('inv_id',$value->id)->count();
     }

     $pandding = DB::table('app_manifest_box')
     ->leftJoin('app_manifest', 'app_manifest_box.inv_id', '=', 'app_manifest.id')
     ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
     ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
     ->leftJoin('app_manifest_detail', 'app_manifest_detail.id', '=', 'app_manifest_box.shipment_id')
     ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
     ->select('pickup_manifest.id as pickup_code', 'app_manifest.status','pickup_manifest.name','app_manifest_detail.country as type_shipping','pickup_manifest.delivery_date','app_manifest.id',
     'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename','pickup_manifest.type_delivery_note',
     'app_manifest.payment_file_re','app_manifest.discount','app_manifest.price_total','app_manifest.price_pay','app_manifest_box.status as boxstatus')
     ->whereIn('app_manifest.status', [101,102,103,104,105,2,3,4,5])
     ->where('pickup_manifest.delivery_date','<', $ldate)
     ->orderBy('pickup_manifest.delivery_date', 'DESC')
     ->get();
     foreach ($pandding as $value) {
          $detail = DB::table('manifest_history')->where('inv_id',$value->id)->select('detail')->orderBy('manifest_history.created_at', 'DESC')->first();
          $value->description = $detail->detail;
     }
      return view("admin/dashboard/summaryall")->with(compact('new','pickup','cal','pay','release','pandding'));
 }
}

