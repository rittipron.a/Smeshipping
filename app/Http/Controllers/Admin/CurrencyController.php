<?php

namespace App\Http\Controllers\admin;

use App\Currency;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
 public function __construct()
 {
  $this->middleware('auth');
 }

 public function index()
 {
  $currency = Currency::all();
  return view('admin/currency/index', compact('currency'));
 }

 public function create()
 {
  return "";
 }

 public function store(Request $request)
 {  
     $old = Currency::where('code',$request->code)->count();
    if($old==0){
        $new = new Currency;
        $new->code = $request->code;
        $new->country = $request->country;
        $new->save();
    }
     
  return "add";
 }

 public function show($id)
 {
  return "";
 }
 public function edit($id)
 {
  return "";
 }

 public function update(Request $request, $id)
 {
    $update = Currency::where('id',$id)->first();
    $update->code = $request->code;
    $update->country = $request->country;
    $update->save();
  return "save";
 }

 public function destroy($id)
 {
    $update = Currency::where('id',$id)->first();
    $update->delete();
  return "";
 }
}
