<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Service;
use App\ServiceDay;
use App\ServiceType;
use App\ServiceZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ScuduleJobController extends Controller
{
 public function update_price(){
   //$data_old = DB::table('app_services_zone')->whereDate('set_date', '=', date('Y-m-d'))->get();
   $data_id = DB::table('app_services_zone_backup')->whereDate('set_date', '=', date('Y-m-d'))->select('s_id')->groupBy('s_id')->get();
   $data = DB::table('app_services_zone_backup')->whereDate('set_date', '=', date('Y-m-d'))->get();
   foreach ($data_id as $value) {
    DB::table('app_services_zone')->where('s_id', $value->s_id)->delete();
   }
   foreach ($data as $value) {
    DB::table('app_services_zone')->insert(
      [
       's_id'    => $value->s_id,
       'weight'  => $value->weight,
       'zone_1'  => $value->zone_1,
       'zone_2'  => $value->zone_2,
       'zone_3'  => $value->zone_3,
       'zone_4'  => $value->zone_4,
       'zone_5'  => $value->zone_5,
       'zone_6'  => $value->zone_6,
       'zone_7'  => $value->zone_7,
       'zone_8'  => $value->zone_8,
       'zone_9'  => $value->zone_9,
       'zone_10' => $value->zone_10,
       'zone_11' => $value->zone_11,
       'zone_12' => $value->zone_12,
       'zone_13' => $value->zone_13,
       'zone_14' => $value->zone_14,
       'zone_15' => $value->zone_15,
       'zone_16' => $value->zone_16,
       'zone_17' => $value->zone_17,
       'zone_18' => $value->zone_18,
       'zone_19' => $value->zone_19,
       'zone_20' => $value->zone_20
       ]
     );
   }

   DB::table('app_services_zone_backup')->whereDate('set_date', '=', date('Y-m-d'))->delete();
   
   return $data_id->toJson();

 }

}
