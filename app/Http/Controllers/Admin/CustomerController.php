<?php

namespace App\Http\Controllers\admin;

use App\Customer;
use App\Manifest;
use App\Pickup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\ServiceGroup;
use App\CustomerAddress;
use Mail;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $value = $request->keyword;
        $servicegroup = ServiceGroup::all();
        $customer = DB::table('app_customer')
            ->where(function ($query) use ($value) {
                $query->where('email', 'like', '%' . $value . '%')
                    ->orWhere('firstname', 'like', '%' . $value . '%')
                    ->orWhere('lastname', 'like', '%' . $value . '%')
                    ->orWhere('phone', 'like', '%' . $value . '%');
            })
            ->where('del', null)
            ->select('app_customer.id', 'app_customer.email', 'app_customer.company', 'app_customer.firstname', 'app_customer.lastname', 'app_customer.phone', 'app_customer.status', 'app_customer.del as del_cus')
            ->distinct()
            ->orderBy('app_customer.del')
            ->paginate(15);


        return view('admin/customer/index', compact('customer', 'servicegroup'));
    }

    public function store(Request $request)
    {
        $customer = new Customer;
        $customer->title = $request->title;
        $customer->status = $request->status;
        $customer->firstname = $request->firstname;
        $customer->lastname = $request->lastname;
        $customer->company = $request->company;
        $customer->type = $request->type;
        $customer->type_customer = $request->type_customer;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->username = $request->username;
        $customer->password = Hash::make($request->password);
        $customer->line = $request->line;
        $customer->credit_term = $request->credit_term;
        $customer->pickup_fee = $request->pickup_fee;
        $customer->note = $request->note;
        $customer->code = $request->code;
        $customer->save();
        return $customer->toJson();
    }

    public function update(Request $request, $id)
    {
        $result = 0; //กำหนดค่าเริ่มต้นว่าเมล์ซ้ำ
        $old_cus = Customer::where('email', $request->email)->whereNotIn('id', [$id])->whereNull('del')->count();
        $old_username = Customer::where('username', $request->username)->whereNotNull('username')->whereNotIn('id', [$id])->whereNull('del')->count();
        if ($old_username == 0) {
            $customer = Customer::where('id', $id)->first();
            $customer->title = $request->title;
            $customer->status = $request->status;
            $customer->firstname = $request->firstname;
            $customer->lastname = $request->lastname;
            $customer->company = $request->company;
            $customer->type = $request->type;
            $customer->type_customer = $request->type_customer;
            $customer->phone = $request->phone;
            $customer->email = $request->email;
            $customer->username = $request->username;
            if ($request->password != '******') {
                $customer->password = Hash::make($request->password);
            }
            $customer->line = $request->line;
            $customer->credit_term = $request->credit_term;
            $customer->pickup_fee = $request->pickup_fee;
            $customer->service_group = $request->service_group;
            $customer->note = $request->note;
            $customer->code = $request->code;
            $customer->del = ($request->active == 'on' ? null : Auth::user()->id);
            $customer->save();
            if ($request->has('invoice')) {
                $invoice = CustomerAddress::where('id', $request->invoice)->first();
                $update_invoice = CustomerAddress::where('customer_id', $invoice->customer_id)->update(['invoice' => 0]);
                $invoice = CustomerAddress::where('id', $request->invoice)->first();
                $invoice->invoice = 1;
                $invoice->save();
            }
            if ($request->has('pickup')) {
                $pickup = CustomerAddress::where('id', $request->pickup)->first();
                $update_pickup = CustomerAddress::where('customer_id', $pickup->customer_id)->update(['pickup' => 0]);
                $pickup = CustomerAddress::where('id', $request->pickup)->first();
                $pickup->pickup = 1;
                $pickup->save();
            }

            $result = 3;//กรณีอีเมล์ไม่ซ้ำ
        } elseif ($old_username > 0) {
            $result = 1;//กรณีusernameซ้ำ
        }

        return $result;
    }

    public function getCustomer($value)
    {
        $customer = DB::table('app_customer')
            ->leftJoin('customer_address', 'customer_address.customer_id', '=', 'app_customer.id')
            ->where(function ($query) use ($value) {
                $query->Where('customer_address.phone', 'like', '%' . $value . '%')
                    ->orWhere('customer_address.customer_name', 'like', '%' . $value . '%')
                    ->orWhere('customer_address.email', 'like', '%' . $value . '%');
            })
            ->where('app_customer.del', null)
            ->select('app_customer.*', 'app_customer.del as del_cus', 'customer_address.*', 'app_customer.id as id')
            ->distinct()
            ->limit(15)
            ->get();
        return $customer->toJson();
    }

    public function checkEmailDup($email)
    {
        $customer = DB::table('app_customer')
            ->leftJoin('customer_address', 'customer_address.customer_id', '=', 'app_customer.id')
            ->where(function ($query) use ($email) {
                $query->Where('app_customer.email', $email)
                    ->orWhere('customer_address.email', $email);
            })
            ->where('app_customer.del', null)
            ->select('customer_address.customer_name', 'customer_address.email', 'customer_address.phone')
            ->distinct()
            ->get();
        if (count($customer) > 0) {
            return response()->json(['data' => $customer]);
        } else {
            return 'true';
        }
    }

    public function getCustomer_by_id($id)
    {
        $result = collect();
        $customer = DB::table('app_customer')
            ->where('app_customer.id', $id)
            ->select('app_customer.*', 'app_customer.del as del_cus', 'app_customer.id')
            ->first();

        $address = DB::table('customer_address')
            ->where('customer_address.customer_id', $id)
            ->select('customer_address.*')
            ->get();
        $result->add($customer);
        $result->add($address);
        return $result->toJson();
    }

    public function destroy($id)
    {
        $update = Customer::where('id', $id)->first();
        $update->del = Auth::user()->id;
        $update->save();
        return "";
    }

    public function add_customer_address(Request $request)
    {  //update address customer
        if ($request->address_id != '') {
            $address = CustomerAddress::where('id', $request->address_id)->first();
        } else {
            $address = new CustomerAddress;
        }
        $address->customer_name = $request->name;
        $address->email = $request->email;
        $address->customer_id = $request->customer_id;
        $address->phone = $request->phone;
        $address->address = $request->address;
        $address->tumbon = $request->tumbon;
        $address->amphur = $request->amphur;
        $address->province = $request->province;
        $address->postcode = $request->postcode;
        $address->update_by = Auth::user()->id;
        $address->save();

        $get_address = DB::table('customer_address')
            ->where('customer_address.id', $address->id)
            ->select('customer_address.*')
            ->get();
        return $get_address->toJson();
    }

    public function del_customer_address($id)
    {
        $address = CustomerAddress::where('id', $id)->delete();
        return $address;
    }

    public function add_address(Request $request)
    {
        if ($request->customer_id == null) {

            $count = Customer::where('phone', $request->phone)->whereNull('del')->count();
            if ($count > 0) {
                $old_id = Customer::where('phone', $request->phone)->whereNull('del')->first();
                $addresscount = CustomerAddress::where('customer_id', $old_id->id)->count();
                if ($addresscount > 0) {
                    throw new Exception('พบเบอร์โทร หรือ อีเมล์ ซ้ำกับของเดิมในระบบ โปรดเช็คว่ามีการสมัครสมาชิกอยู่ก่อนแล้วหรือไม่');
                    return "พบเบอร์โทร หรือ อีเมล์ ซ้ำกับของเดิมในระบบ โปรดเช็คว่ามีการสมัครสมาชิกอยู่ก่อนแล้วหรือไม่";
                } else {
                    $old_id->del = '1';
                    $old_id->save();

                    $customer = new Customer;
                    $customer->firstname = $request->name;
                    $customer->lastname = '';
                    $customer->email = $request->email;
                    $customer->phone = $request->phone;
                    $customer->note = $request->note;
                    $customer->save();
                    $m_id = $customer->id;
                }
            } else {
                $customer = new Customer;
                $customer->firstname = $request->name;
                $customer->lastname = '';
                $customer->email = $request->email;
                $customer->phone = $request->phone;
                $customer->note = $request->note;
                $customer->save();
                $m_id = $customer->id;
            }
        } else {
            $m_id = $request->customer_id;
        }
        $address = new CustomerAddress;
        $address->customer_name = $request->name;
        $address->email = $request->email;
        $address->customer_id = $m_id;
        $address->phone = $request->phone;
        $address->address = $request->address;
        $address->tumbon = $request->tumbon;
        $address->amphur = $request->amphur;
        $address->province = $request->province;
        $address->postcode = $request->postcode;
        $address->pickup = 1;
        $address->invoice = $request->customer_id == null ? 1 : 0;
        $address->save();
        $get_address = DB::table('customer_address')
            ->leftjoin('app_customer', 'customer_address.customer_id', 'app_customer.id')
            ->select('customer_address.*', 'app_customer.note')
            ->where('customer_address.id', $address->id)
            ->get();
        return $get_address->toJson();
    }

    public function email()
    {
        /*
        $manifest_detail = DB::table('app_manifest_detail')->leftJoin('app_services', 'app_services.id', '=', 'app_manifest_detail.services')->select('app_manifest_detail.*','app_services.tracking')->where('inv_id', 2)->where('status','<',10)->get();
        $data = array(
          'name' => 'test',
          'email' => 'test',
          'detail' => $manifest_detail,
          'url_check' => 'https://www.dhl.com/en/express/tracking.shtml?AWB=',
          'date' => date_format(now(),"d/m/Y"));
        return view('mails.shipmentdetail',$data);
        */
        $data = array(
            'url' => 'https://www.dhl.com/en/express/tracking.shtml?AWB=');
        return view('mails.forgetpassword', $data);
    }

    public function sendsms($tel)
    {
        //------ ส่ง SMS แจ้งชำระค่าบริการ
        /*$tel = preg_replace('/0/', '66', $tel, 1);
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://api.ants.co.th/sms/1/text/single",
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{"from":"SMESHIPPING","to": "'.$tel.'","text":"ติดต่อบริการ  SME SHIPPING ผ่านช่องทาง Line Official วันนี้ สะดวกกว่า ส่งง่ายและรอรับโปรโมชั่นกันเลยที่ https://lin.ee/dMdavAv"}' ,
        CURLOPT_HTTPHEADER => array(
        "Authorization: Basic U01FX1Byb2R1Y3Rpb246WEdlUTE1SnU=",
        "Content-Type: application/json"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $sms_send="Error!! ส่ง SMS ไม่สำเร็จ";
        if ($err) {
           throw new Exception('Error.');
        } else {
           $sms_send= "ส่ง SMS สำเร็จ";
        }
        return $sms_send;*/

        $manifest = Manifest::where('id', $tel)->first();
        $manifest_detail = DB::table('app_manifest_detail')->leftJoin('app_services', 'app_services.id', '=', 'app_manifest_detail.services')->select('app_manifest_detail.*', 'app_services.tracking')->where('inv_id', $tel)->where('status', '<', 10)->get();
        $pickup = Pickup::where('id', $manifest->p_id)->first();

        //------ ส่ง SMS แจ้งชำระค่าบริการ
        $tellist = explode(',', $pickup->phone);
        $tellist_array = array_map('trim', $tellist);
        $textforsms = "Your have " . count($manifest_detail) . " new shipment with SME SHIPPING\n";
        foreach ($manifest_detail as $key => $item) {
            $textforsms = $textforsms . ($key + 1) . ". " . $item->tracking . " ";
            $textforsms = $textforsms . $item->awb . " to " . $item->country . "\n";
            /*$domain_data["fullName"] = "rebrand.ly";
            $post_data["destination"] = "https://backend.smeshipping.com/tracking/".$item->awb;
            $post_data["domain"] = $domain_data;
            $ch = curl_init("https://api.rebrandly.com/v1/links");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "apikey: 963f6f60dfa440159a842699a4a62ca2",
                "Content-Type: application/json"
            ));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
            $result = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($result, true);
            dd($response);
            $textforsms = $textforsms.'http://'.$response["shortUrl"]."\n";
              */
            //สร้าง shortURL
            $curl_shorturl = curl_init();
            curl_setopt_array($curl_shorturl, array(
                CURLOPT_URL => "https://is.gd/create.php?format=simple&url=" . "backend.smeshipping.com/tracking/" . $item->awb,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Cookie: __cfduid=d5d0336382e8f80c883bc40ef0c597ace1585972753"
                ),
            ));

            $res_shorturl = curl_exec($curl_shorturl);

            curl_close($curl_shorturl);
            if (strpos($res_shorturl, 'Error') === false) {
                $textforsms = $textforsms . $res_shorturl . "\n";
            } else {
                $textforsms = $textforsms . "http://backend.smeshipping.com/tracking/" . $item->awb . "\n";
            }

            echo $textforsms;
            //
        }

        /*foreach ($tellist_array as $key => $value) {
         if(strlen($value)==10){ //เช็คว่าเบอร์ครบ 10 ตัวหรือไม่
            $tel = preg_replace('/0/', '66', $value, 1);
            echo $tel."<br/><hr/>";
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.ants.co.th/sms/1/text/single",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>"<request> \n<from>SMESHIPPING</from>\n<to> \n<to>'.$tel.'</to> \n</to> \n<text>$textforsms</text> \n</request>",
            CURLOPT_HTTPHEADER => array(
              "Content-Type: application/xml",
              "Authorization: Basic U01FX1Byb2R1Y3Rpb246WEdlUTE1SnU="
            ),
            ));
           $response = curl_exec($curl);
           curl_close($curl);
          }
        }  */
    }


// Send SMS Passwork

    public function sendre_password(Request $request, $id)
    {
        $result = 0; //กำหนดค่าเริ่มต้นว่าเมล์ซ้ำ
        $old_cus = Customer::where('email', $request->email)->whereNotIn('id', [$id])->whereNull('del')->count();
        $old_username = Customer::where('username', $request->username)->whereNotNull('username')->whereNotIn('id', [$id])->whereNull('del')->count();
        if ($old_username == 0) {
            $customer = Customer::where('id', $id)->first();
            $customer->title = $request->title;
            $customer->status = $request->status;
            $customer->firstname = $request->firstname;
            $customer->lastname = $request->lastname;
            $customer->company = $request->company;
            $customer->type = $request->type;
            $customer->type_customer = $request->type_customer;
            $customer->phone = $request->phone;
            $customer->email = $request->email;
            $customer->username = $request->username;
            if ($request->password != '******') {
                $customer->password = Hash::make($request->password);
            }
            $customer->line = $request->line;
            $customer->credit_term = $request->credit_term;
            $customer->pickup_fee = $request->pickup_fee;
            $customer->service_group = $request->service_group;
            $customer->note = $request->note;
            $customer->code = $request->code;
            $customer->del = ($request->active == 'on' ? null : Auth::user()->id);
            $customer->save();
            if ($request->has('invoice')) {
                $invoice = CustomerAddress::where('id', $request->invoice)->first();
                $update_invoice = CustomerAddress::where('customer_id', $invoice->customer_id)->update(['invoice' => 0]);
                $invoice = CustomerAddress::where('id', $request->invoice)->first();
                $invoice->invoice = 1;
                $invoice->save();
            }
            if ($request->has('pickup')) {
                $pickup = CustomerAddress::where('id', $request->pickup)->first();
                $update_pickup = CustomerAddress::where('customer_id', $pickup->customer_id)->update(['pickup' => 0]);
                $pickup = CustomerAddress::where('id', $request->pickup)->first();
                $pickup->pickup = 1;
                $pickup->save();
            }

            //  Send Email
            $data = array(
                'email' => $to = explode(',', $request->email),
                'password' => $request->password,
                'name' => $request->firstname,
                'username' => $request->username,
                'url_login' => 'https://backend.smeshipping.com/public/app/login'
            );
            Mail::send('mails.resetpassword', $data, function ($message) use ($data) {
                $message->to($data['email'])
                    ->from('notification-no-reply@smeshipping.com', 'SME SHIPPING')
                    ->subject('SME SHIPPING Reset Password Username: ' . $data['username']);
            });
            // end

            //------ Send Sms Re_Password
            $tellist = explode(',', $request->phone);
            $tellist_array = array_map('trim', $tellist);
            $textforsms = "Your " . $request->username . " have new password " . $request->password . " with SME SHIPPING\n";

            foreach ($tellist_array as $key => $value) {
                if (strlen($value) == 10) { //เช็คว่าเบอร์ครบ 10 ตัวหรือไม่
                    $tel = preg_replace('/0/', '66', $value, 1);
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api-service.ants.co.th/sms/send",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => '{"messages":[{"from":"SMESHIPPING","destinations":[{"to":"' . $tel . '"}],"text":"' . $textforsms . '"}]}',
                        CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/json",
                            "Authorization: Basic U01FX1Byb2R1Y3Rpb246WEdlUTE1SnU="
                        ),
                    ));
                    $response = curl_exec($curl);
                    curl_close($curl);
                }
            }
            // end

            $result = 3;//กรณีอีเมล์ไม่ซ้ำ
        } elseif ($old_username > 0) {
            $result = 1;//กรณีusernameซ้ำ
        }
        return $result;
    }

}
