<?php

namespace App\Http\Controllers\admin;

use App\Classes\API\FedEx;
use App\TrackingError;
use App\TrackingErrorGroup;
use App\TrackingLog;
use App\Http\Controllers\Controller;
use App\ManifestDetail;
use App\TrackingLogEvent;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Mail;
use App\Teamplete_mail_sms;

class LogTrackingController extends Controller
{

    public function index()
    {
        $message = TrackingError::all();
        $groups = TrackingErrorGroup::all();
        return view('admin/logtracking/manage_message', compact('message', 'groups'));
    }

    public function show(Request $request)
    {
        $TrackingError = TrackingError::all();
        $officers = User::query()->where('position', 'Sale')->orWhere('position', 'Booking Agent')->get();
        $data = Teamplete_mail_sms::select('*')->get();
        $groups = TrackingErrorGroup::query()->with('filters')->with('filters.errors')->get();
        $group_id = false;
        return view('admin/logtracking/index', compact("TrackingError", "officers", "data", 'groups', 'group_id'));
    }

    public function show_by_group(Request $request, $group_id)
    {
        $TrackingError = TrackingError::all();
        $officers = User::query()->where('position', 'Sale')->orWhere('position', 'Booking Agent')->get();
        $data = Teamplete_mail_sms::select('*')->get();
        $groups = TrackingErrorGroup::query()->with('filters')->with('filters.errors')->get();

        return view('admin/logtracking/index', compact("TrackingError", "officers", "data", 'groups', 'group_id'));
    }

    public function mail_tracking(Request $request)
    {
        switch ($request->method()) {
            case "GET":
                $data = Teamplete_mail_sms::query()->whereNotNull('group_name')->get();
                return view('admin/logtracking/manage_mail', compact('data'));
                break;
            case "POST":
                $newMailTemplate = new Teamplete_mail_sms();
                $newMailTemplate->teamplate_name = $request->toppic;
                $newMailTemplate->email_topic = $request->toppic;
                $newMailTemplate->teamplate_mail = $request->email_detail;
                $newMailTemplate->cc = $request->cc;
                $newMailTemplate->group_name = $request->group_name;
                $newMailTemplate->save();
                return "Save";
                break;
            case "PUT":
                $newMailTemplate = Teamplete_mail_sms::query()->find($request->id);
                $newMailTemplate->teamplate_name = $request->toppic;
                $newMailTemplate->email_topic = $request->toppic;
                $newMailTemplate->teamplate_mail = $request->email_detail;
                $newMailTemplate->cc = $request->cc;
                $newMailTemplate->group_name = $request->group_name;
                $newMailTemplate->save();
                return "Save";
                break;
            case "DELETE":
                $mailTemplate = Teamplete_mail_sms::query()->find($request->id);
                $mailTemplate->delete();
                break;
            default:
                break;
        }
    }

    public function getTracing(Request $request)
    {
        if ($request->awb) {
            $data = TrackingLog::query()->with('assignee')->with('error')->with('manifestDetail')->with('manifestDetail.payments')->with('manifestDetail.sos')->with('manifestDetail.manifest')->with('manifestDetail.manifest.customer')->with('logs')->with('logs.operator')->where('awb', $request->awb)->first();
        } else {
            $data = TrackingLog::query()->with('assignee')->with('error')->with('manifestDetail')->with('manifestDetail.payments')->with('manifestDetail.sos')->with('manifestDetail.manifest')->with('manifestDetail.manifest.customer')->with('logs')->with('logs.operator')->get();
        }

        return response()->json(['data' => $data->toArray()]);
    }

    public function getTracingBy(Request $request, $group_id)
    {
        $data = TrackingLog::query()->with('assignee')->with('error')->with('manifestDetail')->with('manifestDetail.payments')->with('manifestDetail.sos')->with('manifestDetail.manifest')->with('manifestDetail.manifest.customer')->with('logs')->with('logs.operator')->get();

        $data = $data->filter(function ($value, $key) use ($group_id) {
            return collect(collect($value)->get('error'))->get('group_id') == $group_id;
        })->toArray();

        return response()->json(['data' => array_values($data)]);
    }

    public function updateTracing(Request $request)
    {
        if (!$request->awb) return redirect()->back();

        if ($request->has('awb_return')) {
            TrackingLog::query()->where('awb', $request->awb_return)->update([
                'awb_return' => $request->awb
            ]);

            TrackingLogEvent::query()->create([
                'status_update' => 'Returned AirWayBill has been changed !',
                'comment' => "Changed AirWayBill from {$request->awb_return} to {$request->awb}.",
                'op' => Auth::user()->id,
                'awb' => $request->awb_return
            ]);

            return redirect()->route('tracing_all');
        }

        $data = TrackingLog::query()->where('awb', $request->awb)->first();

        if ($request->has('complete')) {
            $data->delete();

            TrackingLogEvent::query()->create([
                'status_update' => 'Status has been changed !',
                'comment' => 'Changed status to completed.',
                'op' => Auth::user()->id,
                'awb' => $request->awb
            ]);
        } else {
            $data->update([
                'deadline' => $request->deadline,
                'note' => $request->note
            ]);

            if ($request->has('deadline')) {
                TrackingLogEvent::query()->create([
                    'status_update' => 'Deadline has been updated !',
                    'comment' => "Changed deadline to {$request->deadline}.",
                    'op' => Auth::user()->id,
                    'awb' => $request->awb
                ]);
            }

            if ($request->has('note')) {
                TrackingLogEvent::query()->create([
                    'status_update' => 'Note has been changed !',
                    'comment' => "Changed note to {$request->note}.",
                    'op' => Auth::user()->id,
                    'awb' => $request->awb
                ]);
            }
        }

        return redirect()->route('tracing_all');
    }

    public function generateLog(Request $request)
    {
        $log = new TrackingLogEvent();
        $log->fill($request->post());
        $log->save();

        $l = TrackingLogEvent::query()->where('id', $log->id)->with('operator')->first();

        return response()->json($l);
    }

    public function getLog(Request $request)
    {
        if (!$request->awb) return false;

        $log = TrackingLogEvent::query()->where('awb', $request->awb)->orderBy('created_at', 'DESC')->first();

        return response()->json($log);
    }

    public function assign(Request $request)
    {
        foreach ($request->post('tracking_log_id') as $tracking_id) {
            TrackingLog::query()->where('id', $tracking_id)->update(['assignee_id' => $request->post('officer')]);

            $t = TrackingLog::query()->where('id', $tracking_id)->first();
            $userName = Auth::user()->name;

            TrackingLogEvent::query()->create([
                'status_update' => 'Tracing has been assigned !',
                'comment' => "The tracing with AWB number {$t->awb} has been assigned to {$userName}.",
                'op' => Auth::user()->id,
                'awb' => $t->awb
            ]);
        }

        return redirect()->route('tracing_all');
    }

    public function log_tracking_cf($id)
    {
        $log = TrackingLog::where('id', $id)->first();
        $log->process = 'ดำเนินการแล้ว';
        $log->update_by = Auth::user()->id;
        $log->save();
        return redirect()->back()->withErrors(['บันทึกสำเร็จ']);
    }

    public function update(Request $request, $id)
    {
        $update = TrackingError::where('id', $id)->first();
        $update->priority = $request->priority;
        $update->message = $request->message;
        $update->group_id = $request->group;
        $update->save();
        return "save";
    }

    public function store(Request $request)
    {
        $update = new TrackingError();
        $update->priority = $request->priority;
        $update->message = $request->message;
        $update->group_id = $request->group;
        $update->save();
        return "save";
    }

    public function storeGroup(Request $request)
    {
        if ($request->method() === "POST") {
            $update = new TrackingErrorGroup();
            $update->name = $request->name;
            $update->isComplete = $request->has('isComplete');
            $update->save();
            return "save";
        } else if ($request->method() === "PUT") {
            $update = TrackingErrorGroup::find($request->group_id);
            $update->name = $request->name;
            $update->isComplete = $request->has('isComplete');
            $update->save();
            return "save";
        } else if ($request->method() === "DELETE") {
            $group = TrackingErrorGroup::find($request->group_id);
            $group->filters()->delete();
            $group->delete();
            return "save";
        }
    }

    public function destroy($id)
    {
        $update = TrackingError::where('id', $id)->first();
        $update->delete();
        return "";
    }

    public function checklog()
    {
        $error_message = TrackingError::all();
        $allshipment = DB::table('app_manifest')
            ->leftJoin('app_manifest_detail', 'app_manifest_detail.inv_id', '=', 'app_manifest.id')
            ->leftJoin('app_services', 'app_services.id', '=', 'app_manifest_detail.services')
            ->select('app_manifest_detail.awb', 'app_services.tracking')
//            ->where('app_services.tracking', 'DHL')
            ->where('app_manifest.status', '<>', 0)
            ->where('app_manifest.status', '<', 10)
            ->whereNotNull('app_manifest_detail.awb')
            ->where('app_manifest_detail.tracking_status', '0')
            ->get();

        $countdata = count($allshipment);
        $awblist = collect();
        foreach ($allshipment as $index => $item) {
            $awblist->add($item->awb);
        }

        $awblist->filter(function ($value, $key) {
            return strlen($value) === 10;
        })->chunk(10)->each(function ($item, $key) use ($error_message) {
            self::tracking_check_process_dhl($item, $error_message);
        });

        $awblist->filter(function ($value, $key) {
            return strlen($value) === 12;
        })->chunk(10)->each(function ($item, $key) use ($error_message) {
            self::tracking_check_process_fedex($item, $error_message);
        });

        return 'OK';
    }

    function tracking_check_process_dhl($awblist, $error_message)
    {
        $url = "https://xmlpi-ea.dhl.com/XMLShippingServlet";//"https://xmlpitest-ea.dhl.com/XMLShippingServlet";
        $site_id = "v62_MiV8RRMg6e"; //'v62_zFgdyqm6PS';
        $password = "OqevK2V9I1";//'kGYhSsjb4Q';
        error_reporting(0);
        date_default_timezone_set("Asia/Bangkok");
        header('Content-Type: text/html; charset=utf-8');
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
  <req:KnownTrackingRequest xmlns:req="http://www.dhl.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com TrackingRequestKnown.xsd" schemaVersion="1.0">
      <Request>
          <ServiceHeader>
          <MessageTime>' . date('Y-m-d') . 'T' . date('H:i:s') . '</MessageTime>
          <MessageReference>TrackingRequest_Multiple_AWB</MessageReference>
          <SiteID>' . $site_id . '</SiteID>
          <Password>' . $password . '</Password>
          </ServiceHeader>
      </Request>
      <LanguageCode>en</LanguageCode>';
        foreach ($awblist as $awb) {
            $xml .= '<AWBNumber>' . $awb . '</AWBNumber>';
        }
        $xml .= '<LevelOfDetails>ALL_CHECK_POINTS</LevelOfDetails>
      <PiecesEnabled>B</PiecesEnabled>
  </req:KnownTrackingRequest>';

        $headers = array(
            "Content-type: text/xml",
            "Content-length: " . strlen($xml),
            "Connection: close",
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_MUTE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $data = curl_exec($ch);
        $data = iconv('UTF-8', 'UTF-8//IGNORE', $data);
        $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
        $count_data = count($array_data["AWBInfo"]);
        foreach ($array_data["AWBInfo"] as $value) {
            $AWBNumber = $value["AWBNumber"];
            $shipmentEvent = $value["AWBNumber"]["ShipmentInfo"]["ShipmentEvent"];
            $evenCode = $value["ShipmentInfo"]["ShipmentEvent"][count($value["ShipmentInfo"]["ShipmentEvent"]) - 1]["ServiceEvent"]["EventCode"];
            $description = $value["ShipmentInfo"]["ShipmentEvent"][count($value["ShipmentInfo"]["ShipmentEvent"]) - 1]["ServiceEvent"]["Description"];
            if ($evenCode == '') {
                $evenCode = $value["ShipmentInfo"]["ShipmentEvent"]["ServiceEvent"]["EventCode"];
                $description = $value["ShipmentInfo"]["ShipmentEvent"]["ServiceEvent"]["Description"];
            }
            if (preg_match("/{$error_message->implode('message', '|')}/i", $description, $matches)) {
                $manifestdetail = ManifestDetail::where('awb', $AWBNumber)->first();
                if ($manifestdetail) {
                    $error = TrackingError::query()->where('message', collect($matches)->first())->first();
                    $manifestdetail->tracking_status = 0;
                    $manifestdetail->save();
                    $newlog = TrackingLog::withTrashed()->where('awb', $AWBNumber)->first();
                    if ($newlog == null) {
                        $newlog = new TrackingLog;
                        $newlog->awb = $AWBNumber;
                        $newlog->manifest_id = $manifestdetail->id;
                        $newlog->message = $description;
                        $newlog->process = '-';
                        $newlog->error_id = $error->id;
                        $newlog->save();
                    } else {
                        if (!$newlog->trashed() && $newlog->message != $description) {
                            $newlog->message = $description;
                            $newlog->process = '-';
                            $newlog->error_id = $error->id;
                            $newlog->save();
                        }
                    }

                    if ($evenCode == 'OK') $newlog->delete();
                }
            }

            if ($evenCode == 'OK') {
                $manifestdetail = ManifestDetail::where('awb', $AWBNumber)->first();
                if ($manifestdetail) {
                    $manifestdetail->tracking_status = 1;
                    $manifestdetail->save();
                }

            }
        }
    }

    function tracking_check_process_fedex($awblist, $error_message)
    {
        $fedex = new FedEx();
        $traces = $fedex->track(collect($awblist)->toArray())->get();
        if ($traces->isNotEmpty()) {
            if (Arr::has($traces->toArray(), 'SOAP-ENV_Body.TrackReply.CompletedTrackDetails')) {
                $traceList = collect(Arr::get($traces->toArray(), 'SOAP-ENV_Body.TrackReply.CompletedTrackDetails'));
                $traceList->each(function ($item, $key) use ($error_message) {
                    if (Arr::has($item, 'TrackDetails.StatusDetail.Description')) {
                        if (preg_match("/{$error_message->implode('message', '|')}/i", Arr::get($item, 'TrackDetails.StatusDetail.Description'), $matches)) {
                            $error = TrackingError::query()->where('message', collect($matches)->first())->first();
                            $trace = Arr::get($item, 'TrackDetails');
                            $manifestdetail = ManifestDetail::where('awb', Arr::get($trace, 'TrackingNumber'))->first();
                            if ($manifestdetail) {
                                $manifestdetail->tracking_status = 0;
                                $manifestdetail->save();
                                $newlog = TrackingLog::withTrashed()->where('awb', Arr::get($trace, 'TrackingNumber'))->first();
                                if ($newlog == null) {
                                    $newlog = new TrackingLog;
                                    $newlog->awb = Arr::get($trace, 'TrackingNumber');
                                    $newlog->manifest_id = $manifestdetail->id;
                                    $newlog->message = Arr::get($trace, 'StatusDetail.Description');
                                    $newlog->process = '-';
                                    $newlog->error_id = $error->id;
                                    if (Arr::has($trace, 'StatusDetail.AncillaryDetails.Action')) $newlog->action  = Arr::get($trace, 'StatusDetail.AncillaryDetails.Action');
                                    if (Arr::has($trace, 'StatusDetail.AncillaryDetails.ActionDescription')) $newlog->action_description = Arr::get($trace, 'StatusDetail.AncillaryDetails.ActionDescription');
                                    $newlog->save();
                                } else {
                                    if (!$newlog->trashed() && $newlog->message != Arr::get($trace, 'StatusDetail.Description')) {
                                        $newlog->message = Arr::get($trace, 'StatusDetail.Description');
                                        $newlog->process = '-';
                                        $newlog->error_id = $error->id;
                                        $newlog->save();
                                    }
                                }

                                if (Arr::get($trace, 'StatusDetail') == 'DL') $newlog->delete();
                            }

                            if (Arr::get($trace, 'StatusDetail') == 'DL') {
                                $manifestdetail = ManifestDetail::where('awb', Arr::get($trace, 'TrackingNumber'))->first();
                                if ($manifestdetail) {
                                    $manifestdetail->tracking_status = 1;
                                    $manifestdetail->save();
                                }
                            }
                        }
                    }
                });
            }
        }
    }
}
