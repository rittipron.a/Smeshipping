<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ServiceType;
use Illuminate\Support\Facades\DB;
use App\ManifestDetail_box;
use App\ManifestDetail;
use App\Manifest;
use App\CustomerBookingNote;
use App\Employee;
use App\PaymentAttachmentFromCustomer;
use App\Pickup;
use App\Country;
use App\Address;
use App\Pack_detail;
use App\Attachment_Payment;
use File;
use Image;
use App\ManifestHistory;
use App\Pickup_SO;
use App\AppBoxCheckin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckController extends Controller
{
 public function __construct()
 {
  $this->middleware('auth');
 }
 public function mess()
 {
   $ldate = strtotime(str_replace("/", "-",date('d/m/Y')));
   $data = DB::table('app_manifest')
   ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
   ->leftJoin('app_services_type', 'app_services_type.id', '=', 'pickup_manifest.type_of_service')
   ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
   ->where('app_manifest.status', '=', 2)
   ->where('pickup_manifest.status', '=', 2)
   ->where('pickup_manifest.driver', '=', Auth::user()->id)
   ->where('pickup_manifest.delivery_date',$ldate)
   ->select('app_manifest.id as manifestid','pickup_manifest.*','app_services_type.name as type_of_service_name',
   'users.name as sale_name','users.tel as sale_tel','app_manifest.type_pay','app_manifest.pay','pickup_manifest.status as pickup_status')
   ->orderBy('pickup_manifest.delivery_date')
   ->get();

   foreach ($data as $key => $value) {
      $contact = DB::table('customer_address')->where('customer_id',$value->m_id)->select('phone')->distinct()->get();
      $value->contact = $contact;
      $value->so_image = DB::table('pickup_manifest_so')->where('pickup_manifest_so.inv_id', '=',$value->manifestid)->get();
      $value->pack = Pack_detail::where('m_id',$value->manifestid)->get();
   }
   
   $mode = 'check';
   $mess = Employee::whereIn('position', ['Messenger','Supervisor Messenger'])->get();
  return view('admin/check/mess', compact('data','mess','mode'));
  //return $data->toJson();
 }
 public function mess_history()
 {
   $ldate = strtotime(str_replace("/", "-",date('d/m/Y')));
   $data = DB::table('app_manifest')
   ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
   ->leftJoin('app_services_type', 'app_services_type.id', '=', 'pickup_manifest.type_of_service')
   ->leftJoin('users', 'users.id', '=', 'app_manifest.user_create')
   ->where('app_manifest.status', '=', 2)
   ->where('pickup_manifest.status', '=', 3)
   ->orwhere(function ($query) use($ldate) {
      $query->where('app_manifest.status', '>', 2)
      ->where('pickup_manifest.delivery_date',$ldate);
     }) 
   ->where('pickup_manifest.delivery_date',$ldate)
   ->where('pickup_manifest.driver', '=', Auth::user()->id)
   ->select('app_manifest.id as manifestid','pickup_manifest.*','app_services_type.name as type_of_service_name',
   'users.name as sale_name','users.tel as sale_tel','app_manifest.type_pay','app_manifest.pay','pickup_manifest.status as pickup_status')
   ->orderBy('pickup_manifest.delivery_date')
   ->get();
   foreach ($data as $key => $value) {
      $contact = DB::table('customer_address')->where('customer_id',$value->m_id)->select('phone')->distinct()->get();
      $value->contact = $contact;
      $value->so_image = DB::table('pickup_manifest_so')->where('pickup_manifest_so.inv_id', '=',$value->manifestid)->get();
      $value->pack = Pack_detail::where('m_id',$value->manifestid)->get();
   }
      
   $mode = 'history';
   $mess = Employee::whereIn('position', ['Messenger','Supervisor Messenger'])->get();
   
  return view('admin/check/mess', compact('data','mess','mode'));
 }

 public function mess_search(Request $request)
 {
   $ldate = strtotime(str_replace("/", "-",date('d/m/Y')));
   $data = DB::table('app_manifest')
   ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
   ->leftJoin('app_services_type', 'app_services_type.id', '=', 'pickup_manifest.type_of_service')
   ->leftJoin('users', 'users.id', '=', 'app_manifest.user_create')
   ->where('pickup_manifest.status', '>', 2);
   if($request->mess!=null){
      $data = $data->where('pickup_manifest.driver', '=', $request->mess);
   }
   if($request->date!=null){
      $data = $data->where('pickup_manifest.delivery_date', '=', $request->date);
   }
   if($request->so!=null){
      $data = $data->where('pickup_manifest.so', 'like', '%'.$request->so.'%');
   }
   $data = $data->select('app_manifest.id as manifestid','pickup_manifest.*','app_services_type.name as type_of_service_name',
   'users.name as sale_name','users.tel as sale_tel','app_manifest.type_pay','app_manifest.pay','pickup_manifest.status as pickup_status')
   ->orderBy('pickup_manifest.delivery_date')
   ->paginate(20);
   foreach ($data as $key => $value) {
      $contact = DB::table('customer_address')->where('customer_id',$value->m_id)->select('phone')->distinct()->get();
      $value->contact = $contact;
      $value->so_image = DB::table('pickup_manifest_so')->where('pickup_manifest_so.inv_id', '=',$value->manifestid)->get();
      $value->pack = Pack_detail::where('m_id',$value->manifestid)->get();
   }
      
   $mode = 'search';
   $mess = Employee::whereIn('position', ['Messenger','Supervisor Messenger'])->get();
   
  return view('admin/check/mess', compact('data','mess','mode'));
 }

 public function in()
 {
   $data = DB::table('app_manifest')
   ->leftJoin('app_manifest_detail', 'app_manifest_detail.inv_id', '=', 'app_manifest.id')
   ->Join('app_manifest_box', 'app_manifest_box.shipment_id', '=', 'app_manifest_detail.id')
   ->where('app_manifest.status', '=', 2)
   ->where('app_manifest_box.status', '=', 1)
   ->select('app_manifest.id as manifestid', 'app_manifest_detail.tracking_code','app_manifest_box.series')
   ->get();
  return view('admin/check/in', compact('data'));
  //return $data->toJson();
 }
 
 public function out()
 {
   $data = DB::table('app_manifest')
   ->leftJoin('app_manifest_detail', 'app_manifest_detail.inv_id', '=', 'app_manifest.id')
   ->Join('app_manifest_box', 'app_manifest_box.shipment_id', '=', 'app_manifest_detail.id')
   ->where('app_manifest.status', '=', 5)
   ->where('app_manifest_box.status', '<=', 2)
   ->select('app_manifest.id as manifestid', 'app_manifest_detail.tracking_code','app_manifest_detail.awb','app_manifest_box.series')
   ->get();
   return view('admin/check/out', compact('data'));
 }

   public function getDetailByShipmentCode_mess($id) 
   {
      //ค้นหากล่องของรหัสที่ได้ ที่สถานะเป็น 0 แล้วอัพเดท 
      $box_update = ManifestDetail_box::where('id',$id)->first();
      if($box_update){
         if($box_update->status==0){
            $box_update->status = 1;
            $box_update->save();
         }
         
      }
      $manifest              = Manifest::where('id', $box_update->inv_id)->first();
      $box_check_status1 = ManifestDetail_box::where('inv_id',$box_update->inv_id)->where('status',1)->count();
      $box_check_all = ManifestDetail_box::where('inv_id',$box_update->inv_id)->Where('status','<',10)->count();
      /*if($box_check_all==$box_check_status1){
         $pickup             = Pickup::where('id', $manifest->p_id)->first();
         $pickup->status     = 3;
         $pickup->save();

         $history            = new ManifestHistory;
         $history->topic     = 'Messenger Check-In';
         $history->detail    = 'เปลี่ยนสถานะจาก Pickup เป็น Calculate ด้วยการ Check in ในบริษัท';
         $history->update_by = Auth::user()->id;
         $history->inv_id    = $box_update->inv_id;
         $history->save();
      }
      */
      return response()->json(['status' => true]);
   }

 public function getDetailByShipmentCode($code)
 {
    $code = preg_replace('/\s+/', '', $code);

   $now      = strtotime(date('Y-m-d H:i:s'));
   if(strlen($code)==6){
      $shipment = DB::table('app_manifest_detail')
      ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
      ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
      ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.description as service_name',
       'app_services.cal_weight','app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight')
      ->where('app_manifest_detail.tracking_code', '=', $code)
      ->whereYear('app_manifest_detail.created_at', '=', now()->year)
      ->orderBy('app_manifest_detail.id')
      ->get();
   }
   else{
      $shipment = DB::table('app_manifest_detail')
      ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
      ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
      ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.description as service_name',
       'app_services.cal_weight','app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight')
      ->where('app_manifest_detail.awb', '=', $code)
      ->whereYear('app_manifest_detail.created_at', '=', now()->year)
      ->orderBy('app_manifest_detail.id')
      ->limit(2)
      ->get();
   }
   
   //ค้นหากล่องของรหัสที่ได้ ที่สถานะเป็น 0 แล้วอัพเดท 
   $box_update = ManifestDetail_box::where('shipment_id',$shipment[0]->id)->where('status',1)->first();
   if($box_update){
      $box_update->status = 2;
      $box_update->save();
   }
   $box_check_status2 = ManifestDetail_box::where('inv_id',$shipment[0]->inv_id)->where('status',2)->count();
   $box_check_all = ManifestDetail_box::where('inv_id',$shipment[0]->inv_id)->Where('status','<',10)->count();
   if($box_check_all==$box_check_status2){
      $status                = 3; // update Pickup to Calculate
      $manifest              = Manifest::where('id', $shipment[0]->inv_id)->first();
      $manifest->user_update = Auth::user()->name;
      $pickup             = Pickup::where('id', $manifest->p_id)->first();
      $pickup->status     = $status;

      $history            = new ManifestHistory;
      $history->topic     = 'Check In';
      $history->detail    = 'เปลี่ยนสถานะจาก Pickup เป็น Calculate ด้วยการ Check in ในบริษัท';
      $history->update_by = Auth::user()->id;
      $history->inv_id    = $manifest->id;

      if($manifest->status==2){ //ต้องเป็นสถานะ pickup ถึงจะบันทึก
         $manifest->status      = $status;
         $manifest->save();
         $pickup->save();
         $history->save();
      }
      
   }
   if(count($shipment)>0){
      $box = DB::table('app_manifest_box')
   ->where('shipment_id', '=', $shipment[0]->id)
   ->select('app_manifest_box.*', DB::raw('ROUND((width*length*height)/5000,2) as sum_weight'))
   ->orderBy('series','DESC')
   ->get();
   }
   else{
      $box = [];
   }
    return response()->json(['shipment' => $shipment,'box'=>$box]);
 }
 
public function getDetailByAWBCode($code) // check-out ของออกจาก SMESHIPPING 
 {
   //$now      = strtotime(date('Y-m-d H:i:s'));
    $shipment = DB::table('app_manifest_detail')
   ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
   ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
   ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.description as service_name',
    'app_services.cal_weight','app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight')
   ->where('app_manifest_detail.awb', $code)
   ->where('app_manifest_detail.status', 1)
  // ->whereYear('app_manifest_detail.created_at', '=', now()->year)
   ->orderBy('app_manifest_detail.id')
   ->get();
   //ค้นหากล่องของรหัสที่ได้ ที่สถานะเป็น 2 แล้วอัพเดท เป็น 3
   
   $box_update = ManifestDetail_box::where('shipment_id',$shipment[0]->id)->where('status','<=',2)->first();
   if($box_update){
      $box_update->status = 3;
      $box_update->save();
   }

   $box_check_status3 = ManifestDetail_box::where('inv_id',$shipment[0]->inv_id)->where('status',3)->count();
   $box_check_all = ManifestDetail_box::where('inv_id',$shipment[0]->inv_id)->Where('status','<',10)->count();
   if($box_check_all==$box_check_status3){
      $status                = 6; // update to Complete
      $manifest              = Manifest::where('id', $shipment[0]->inv_id)->first();
      $manifest->user_update = Auth::user()->name;
      $pickup             = Pickup::where('id', $manifest->p_id)->first();
      $pickup->status     = $status;

      $detail = ManifestDetail::where('inv_id', '=', $manifest->id)->where('status', '<', 10)->update(['status' => '2']);

      $history            = new ManifestHistory;
      $history->topic     = 'Complete';
      $history->detail    = 'เปลี่ยนสถานะจาก Wait for Release เป็น Complete  ด้วยการ Check Out';
      $history->update_by = Auth::user()->id;
      $history->inv_id    = $manifest->id;

      if($manifest->status==5){ //ต้องเป็นสถานะ Release ถึงจะบันทึก
         $manifest->status      = $status; 
         $manifest->save();
         $pickup->save();
         $history->save();
      }
      
   }
   if(count($shipment)>0){
      $box = DB::table('app_manifest_box')
   ->where('shipment_id', '=', $shipment[0]->id)
   ->select('app_manifest_box.*', DB::raw('ROUND((width*length*height)/5000,2) as sum_weight'))
   ->orderBy('series','DESC')
   ->get();
   }
   else{
      $box = [];
   }
    return response()->json(['shipment' => $shipment,'box'=>$box]);
    
    //return response()->json(['shipment' => $shipment]);
 }

 public function checkinbox(Request $request){
    if($request->keyword==null){
        $data = AppBoxCheckin::leftJoin('users', 'app_box_checkin.update_by', 'users.id')
         ->select('app_box_checkin.*','users.name as editname')
         ->whereNull('del')->orderBy('status', 'asc')
         ->orderBy('created_at', 'desc')
         ->paginate(20);
    }else{
      $data = AppBoxCheckin::leftJoin('users', 'app_box_checkin.update_by', 'users.id')
      ->select('app_box_checkin.*','users.name as editname')
      
      ->where('app_box_checkin.tracking_no','like','%'.$request->keyword.'%')
      ->orwhere('app_box_checkin.name','like','%'.$request->keyword.'%')
      ->whereNull('del')->orderBy('status', 'asc')
      ->orderBy('created_at', 'desc')
      ->paginate(20);
    }
   
    return view('admin/check/checkinbox', compact('data'));
 }
 
 public function updatecheckinbox(Request $request){
   $ch_box = new AppBoxCheckin;
   $files = $request->file('files');
   $filename = null;
   if($request->hasFile('files'))
      {  
         foreach ($files as $index=>$fileitem) {
            $file = $fileitem->getClientOriginalName();
            $fileName = pathinfo($file,PATHINFO_FILENAME);
            $imageName = time().'_'.$fileName.'.'.$fileitem->getClientOriginalExtension();
            $fileitem->move(public_path('/uploads/inbox'), $imageName);
            $img = Image::make(public_path('/uploads/inbox/').$imageName);
            $img->resize(null, 1000, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(public_path('/uploads/inbox/').$imageName);
            $filename =$filename.($index!=0?',':'').$imageName;
        }
      }
   $ch_box->image = $filename;
   $ch_box->name = $request->name;
   $ch_box->delivery_by = $request->delivery_by;
   $ch_box->note = $request->note;
   $ch_box->tracking_no = $request->tracking_no;
   $ch_box->created_by = Auth::user()->id;
   $ch_box->save();
    return back()->withInput();
 }

 public function getinbox(Request $request, $id){
   $ch_box = AppBoxCheckin::find($id);
   $ch_box->update_by = Auth::user()->id;
   $ch_box->status = 2;
   $ch_box->note = $ch_box->note.' '.$request->remark;
   $ch_box->save();
   return redirect('/admin/request/create');
 }
 public function delinbox($id){
   $ch_box = AppBoxCheckin::find($id);
   $ch_box->del = Auth::user()->id;
   $ch_box->save();
    return back()->withInput();
 }

 public function getcountrycontent($country_code){
   $country = Country::where('code',$country_code)->get();
   return $country->ToJson();
 }
 
 public function inboxforlink(Request $request){
   
   if($request->keyword!=null){
      $data = CustomerBookingNote::leftJoin('users', 'customer_booking_note.created_by', 'users.id')
      ->select('customer_booking_note.*','users.name as editname')
      ->where('tracking', 'like', '%'.$request->keyword.'%')
      ->orWhere('sender_name', 'like', '%'.$request->keyword.'%')
      ->orWhere('sender_tel', 'like', '%'.$request->keyword.'%')
      ->orderBy('created_at', 'desc')->paginate(20);
     }
     else{
      $data = CustomerBookingNote::leftJoin('users', 'customer_booking_note.created_by', 'users.id')
      ->select('customer_booking_note.*','users.name as editname')
      ->orderBy('created_at', 'desc')
      ->paginate(20);
     }

   return view('admin/check/inboxforlink', compact('data'));
 }
 
 public function getlink(){
   $data = new CustomerBookingNote;
   $data->uuid = uniqid();
   $data->tracking = self::checkrandom(4);
   $data->created_by = Auth::user()->id;
   $data->save();

   $dataresult = CustomerBookingNote::leftJoin('users', 'customer_booking_note.created_by', 'users.id')
   ->where('customer_booking_note.id',$data->id)->select('customer_booking_note.*','users.name as editname')->first();
   return $dataresult->toJson();
 }

 public function checkrandom($length=4){

   $possible = "1234567890"; //ตัวอักษรที่ต้องการสุ่ม
   $loop = true;
   while($loop){
     $str = "";
     $sum = 0;
     while(strlen($str)<$length){
         $thisstr = substr($possible,(rand()%strlen($possible)),1);
         $str.=$thisstr;
         $sum = $sum+$thisstr;
     }
     $str.= substr($sum,-1);
     $tracking_code = CustomerBookingNote::where('tracking',$str)->whereNull('del')->count();
     if($tracking_code==0){
       $loop = false;
     }
   }
   
     return $str;
  }
 
  public function inboxforlink_del($id){
   $data = CustomerBookingNote::find($id);
   $data->del = Auth::user()->id;
   $data->save();
   return 'success';
  }

   public function check_payment_attachment(Request $request){
      if($request->keyword==null){
            $data = PaymentAttachmentFromCustomer::orderBy('created_at', 'desc')
               ->paginate(20);
      }else{
            $data = PaymentAttachmentFromCustomer::where('so','like','%'.$request->keyword.'%')
               ->orwhere('tel','like','%'.$request->keyword.'%')
               ->orderBy('created_at', 'desc')
               ->paginate(20);
      }
      return view('admin/check/check_payment_attachment', compact('data'));
   }
   public function check_payment_attachment_search(Request $request){
      $data = null;
      if ($request->so != null) {
         $data  = DB::table('pickup_manifest')
         ->leftJoin('app_manifest_detail', 'app_manifest_detail.p_id', '=', 'pickup_manifest.id')
         ->where('app_manifest_detail.inv_id', $request->so)
         ->select('pickup_manifest.name','pickup_manifest.phone', 'pickup_manifest.created_at', 'app_manifest_detail.inv_id as manifestid','app_manifest_detail.so')
         ->orderBy('pickup_manifest.updated_at', 'desc')
         ->distinct()
         ->paginate(10); 
      }
      if ($data==null&&$request->so != null) {
         $data  = DB::table('pickup_manifest')
         ->leftJoin('app_manifest_detail', 'app_manifest_detail.p_id', '=', 'pickup_manifest.id')
         ->where('app_manifest_detail.tracking_code', $request->so)
         ->select('pickup_manifest.name','pickup_manifest.phone', 'pickup_manifest.created_at', 'app_manifest_detail.inv_id as manifestid','app_manifest_detail.so')
         ->orderBy('pickup_manifest.updated_at', 'desc')
         ->distinct()
         ->paginate(10); 
      }else if ($data==null&&$request->so != null) {
            $data  = DB::table('pickup_manifest')
            ->leftJoin('app_manifest_detail', 'app_manifest_detail.p_id', '=', 'pickup_manifest.id')
            ->where('app_manifest_detail.so', $request->so)
            ->select('pickup_manifest.name','pickup_manifest.phone', 'pickup_manifest.created_at', 'app_manifest_detail.inv_id as manifestid','app_manifest_detail.so')
            ->orderBy('pickup_manifest.updated_at', 'desc')
            ->distinct()
            ->paginate(10); 
      }else if($data==null){
         $data  = DB::table('pickup_manifest')
         ->leftJoin('app_manifest_detail', 'app_manifest_detail.p_id', '=', 'pickup_manifest.id')
         ->where('pickup_manifest.phone', $request->tel)
         ->select('pickup_manifest.name','pickup_manifest.phone', 'pickup_manifest.created_at', 'app_manifest_detail.inv_id as manifestid','app_manifest_detail.so')
         ->orderBy('pickup_manifest.updated_at', 'desc')
         ->distinct()
         ->paginate(10); 
      }
      return $data->toJson();
   }
   public function check_payment_attachment_save(Request $request){
      
      if($request->att_id!=null){
         $data = PaymentAttachmentFromCustomer::where('id',$request->att_id)->first();
         if($data->update_by==null){
            $data->update_by = Auth::user()->id;
            $inv_id = ManifestDetail::Where('so', 'like', '%' . $data->so . '%')->first();
            if($inv_id!=null){
               $oldPath = public_path('/uploads/so/'.$data->filename);
               $newPath = public_path('/uploads/payment/'.$data->filename);
               File::copy($oldPath , $newPath);
               $attach = new Attachment_Payment;
               $attach->inv_id = $inv_id->inv_id;
               $attach->filename = $data->filename;
               $attach->save();
            }
            
         }
         else{
            $data->update_by = null;
            $attach = Attachment_Payment::Where('filename',$data->filename)->delete();
            File::delete(public_path('/uploads/payment/'.$data->filename));
         }
         $data->save();
      }
      return back()->withInput();
   }

   public function check_verify(Request $request,$id){
      Pack_detail::where('m_id',$id)
      ->update(['status' => 0]);
      if($request->verify_id!=null){
         Pack_detail::whereIn('id',$request->verify_id)
         ->where('m_id',$id)
         ->update(['status' => 1]);
      }
      $check_success = Pack_detail::where('m_id',$id)->where('status',0)->count();
      $check_all = Pack_detail::where('m_id',$id)->count();
      if($check_success==0 && $check_all!=0){
         Manifest::where('id',$id)->update(['ispackable' => 1]);
      }else{
         Manifest::where('id',$id)->update(['ispackable' => 0]);
      }
      return back()->withInput();
   }
}
