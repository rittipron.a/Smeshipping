<?php

namespace App\Http\Controllers\admin;

use App\DiscountCode;
use App\DiscountUsed;
use App\ServiceGroup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
 public function __construct()
 {
  $this->middleware('auth');
 }

 public function index()
 {
  $discount = DiscountCode::all();
  $serviceGroup = ServiceGroup::all();
  return view('admin/discount/index', compact('discount','serviceGroup'));
 }

 public function create()
 {
  return "";
 }

 public function store(Request $request)
 {  
    //return dd($request);
     $old = DiscountCode::where('id',$request->id)->count();
    if($old==0){
        $new = new DiscountCode;
        $new->discount_code = $request->discount_code;
        if($request->type=='price'){
          $new->price = $request->number;
          $new->percent = null;
        }
        else{
          $new->price = Null;
          $new->percent = $request->number;
        }
        $new->ignore_weight = $request->ignore_weight;
        $new->focus_country_code = $request->focus_country_code;
        $new->focus_service_group_id = implode(",",$request->focus_service_group_id);
        $new->effective_date = $request->effective_date;
        $new->expired_date = $request->expired_date;
        $new->used_limit = $request->used_limit;
        $new->description = $request->description;
        $new->save();
    }
  return "add";
 }

 public function show($id)
 {
  return "";
 }
 public function edit($id)
 {
  return "";
 }

 public function update(Request $request, $id)
 {
    $new = DiscountCode::where('id',$id)->first();
    $new->discount_code = $request->discount_code;
        if($request->type=='price'){
          $new->price = $request->number;
          $new->percent = null;
        }
        else{
          $new->price = Null;
          $new->percent = $request->number;
        }
        $new->ignore_weight = $request->ignore_weight;
        $new->focus_country_code = $request->focus_country_code;
        $new->focus_service_group_id = implode(",",$request->focus_service_group_id);
        $new->effective_date = $request->effective_date;
        $new->expired_date = $request->expired_date;
        $new->used_limit = $request->used_limit;
        $new->description = $request->description;
    $new->save();
  return "save";
 }

 public function destroy($id)
 {
    $update = DiscountCode::where('id',$id)->first();
    $update->delete();
    return "";
 }
}
