<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ServiceType;
use Illuminate\Support\Facades\DB;
use App\ManifestDetail;
use App\ManifestDetail_box;
use App\Manifest;
use App\Pickup;
use App\ManifestHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReturnController extends Controller
{
 public function __construct()
 {
  $this->middleware('auth');
 }

 public function set_return($id)
 {
  $shipment = ManifestDetail::where('id',$id)->first();
  $shipment->status = ($shipment->status>=10?$shipment->status-10:$shipment->status+10);
  $shipment->save(); 

   $box = ManifestDetail_box::where('shipment_id',$id)->get();
   foreach ($box as $value) {
      $value->status = ($value->status>=10?$value->status-10:$value->status+10);
      $value->save();
   }

  $history            = new ManifestHistory;
  $history->topic     = 'Return';
  $history->detail    = 'กด Return shipment ID:'.$id;
  $history->update_by = Auth::user()->name;
  $history->inv_id    = $shipment->inv_id;
  $history->save();
  return $shipment->status;
  
 }

}