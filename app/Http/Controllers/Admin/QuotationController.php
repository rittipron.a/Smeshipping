<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ServiceType;
use App\ServiceZone;
use App\ServiceDay;
use App\CountryZone;
use Illuminate\Support\Facades\DB;
use App\Quotation_detail;
use App\Quotation_box;
use App\Quotation_other;
use App\Quotation;
use App\Employee;
use App\User;
use App\Country;
use App\Address;
use Mail;
use Swift_Mailer;
use Swift_SmtpTransport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Teamplete_mail_sms;

class QuotationController extends Controller
{
    public function __construct()
    {

    }

    public function create()
    {
        return view('admin/quotation/index');
    }

    public function store(Request $request)
    {

        $now = strtotime(date('Y-m-d H:i:s'));
        $Quotation = new Quotation;
        $Quotation->customer_id = $request->customer_id;
        $Quotation->company = $request->company;
        $Quotation->name = $request->name;
        $Quotation->address = $request->address;
        $Quotation->tel = $request->tel;
        $Quotation->email = $request->email;
        $Quotation->quote_no = 'QT' . $now;
        $Quotation->payment = $request->payment;
        $Quotation->valid_day = $request->valid_day;
        $Quotation->reference = $request->reference;
        $Quotation->discount = $request->discount;
        //  $Quotation->document_charge = $request->document_charge;
        $Quotation->withholding = $request->withholding;
        $Quotation->quotation_toppic = $request->quotation_toppic;
        $Quotation->translate = $request->translate;
        $Quotation->total_price = $request->total_price;
        $Quotation->sale_id = Auth::user()->id;
        $Quotation->save();

        $shipcount = count($request->s_name);
        for ($i = 0; $i < $shipcount; $i++) {
            $Quotation_detail = new Quotation_detail;
            $Quotation_detail->quota_id = $Quotation->id;
            $Quotation_detail->s_id = $request->s_id[$i];
            $Quotation_detail->s_name = $request->s_name[$i];
            $Quotation_detail->s_price = $request->s_price[$i];
            $Quotation_detail->risk = $request->risk[$i];
            $Quotation_detail->country_name = $request->country[$i];
            // ส่วนเพิ่มเติมของบริการ
            $Quotation_detail->wht_one = $request->wht_one[$i];
            $Quotation_detail->wht_tree = $request->wht_tree[$i];
            $Quotation_detail->unit = $request->unit[$i];
            $Quotation_detail->vat_serven = $request->vat_serven[$i];
            // ค่าเอกสาร
            $Quotation_detail->wht_handingfee = $request->wht_handingfee[$i];
            $Quotation_detail->save();

            $boxcount = count($request->width[$i]);
            for ($ii = 0; $ii < $boxcount; $ii++) {
                $Quotation_box = new Quotation_box;
                $Quotation_box->quota_detail_id = $Quotation_detail->id;
                $Quotation_box->weight = $request->weight[$i][$ii];
                $Quotation_box->length = $request->length[$i][$ii];
                $Quotation_box->height = $request->height[$i][$ii];
                $Quotation_box->width = $request->width[$i][$ii];
                $Quotation_box->total_weight = $request->weight[$i][$ii];
                $Quotation_box->true_width = $request->width[$i][$ii];
                $Quotation_box->volume_width = $request->volume_width[$i][$ii];
                $Quotation_box->series = $ii;
                $Quotation_box->save();
            }
            // ส่วนของบริการเสริม
            if(isset($request->other_name[$i])) {
                $other_service = count($request->other_name[$i]);
                for ($ii = 0; $ii < $other_service; $ii++) {
                    $Quotation_other = new Quotation_other;
                    $Quotation_other->quota_detail_id = $Quotation_detail->id;
                    $Quotation_other->other_name = $request->other_name[$i][$ii];
                    $Quotation_other->other_wht_one = $request->other_wht_one[$i][$ii];
                    $Quotation_other->other_wht_tree = $request->other_wht_tree[$i][$ii];
                    $Quotation_other->vat_other_service = $request->vat_other_service[$i][$ii];
                    $Quotation_other->unit_other_service = $request->unit_other_service[$i][$ii];
                    $Quotation_other->service_value = $request->service_value[$i][$ii];
                    $Quotation_other->total_other_service = $request->total_other_service[$i][$ii];
                    $Quotation_other->save();
                }
            }
        }
        return redirect('admin/quotation/' . $Quotation->id . '/' . $request->translate);
    }

    public function show($id, $locale)
    {
        $data = Quotation::where('id', $id)->first();
        $shipment = Quotation_detail::where('quota_id', $id)->get();
        foreach ($shipment as $key => $value) {
            $value->box = Quotation_box::where('quota_detail_id', $value->id)->get();
            $value->other = Quotation_other::where('quota_detail_id', $value->id)->get();
        }

        $sale = User::where('id', $data->sale_id)->first();
        $data->salename = $sale->name;
        if ($locale == "th") {
            $header = $data->quotation_toppic;
            return view('admin/quotation/show_th', compact('data', 'shipment', 'header'));
        }
        if ($locale == "en") {
            $header = ($data->quotation_toppic == 'ใบเสนอราคา') ? 'Quotation' : 'Invoice';
            return view('admin/quotation/show_en', compact('data', 'shipment', 'header'));
        }
    }

    public function view($id)
    {
        $data = Quotation::where('id', $id)->first();
        $shipment = Quotation_detail::where('quota_id', $id)->get();
        foreach ($shipment as $key => $value) {
            $value->box = Quotation_box::where('quota_detail_id', $value->id)->get();
            $value->other = Quotation_other::where('quota_detail_id', $value->id)->get();
        }

        $sale = User::where('id', $data->sale_id)->first();
        $data->salename = $sale->name;
        return view('admin/quotation/view', compact('data', 'shipment'));
    }

    public function sendemail($id)
    {

        $data = Quotation::where('id', $id)->first();
        $data_array = array(
            'qot_no' => $data->quote_no,
            'name' => $data->name,
            'email' => $data->email,
            'qot_url' => 'https://backend.smeshipping.com/admin/quotation/' . $data->id
        );

        $backup = Mail::getSwiftMailer();

        // Setup your gmail mailer
        $transport = new Swift_SmtpTransport('smtp.gmail.com', 587, 'tls');
        $transport->setUsername('ba1@smeshipping.com');
        $transport->setPassword('kik@1234');
        // Any other mailer configuration stuff needed...
        $gmail = new Swift_Mailer($transport);

        // Set the mailer as gmail
        Mail::setSwiftMailer($gmail);

        Mail::send('mails.Quotation', $data_array, function ($message) use ($data_array) {
            $message->to($data_array['email']);
            $message->from('ba1@smeshipping.com', 'SME SHIPPING');
            $message->subject('SME SHIPPING แจ้งใบเสนอราคาเลขที่ ' . $data_array['qot_no'] . ' ของคุณ ' . $data_array['name'] . '');
        });
        Mail::setSwiftMailer($backup);
        $now = strtotime(date('Y-m-d H:i:s'));
        $data->send_email = $now;
        $data->save();
        return redirect()->back()->withErrors(['ส่งอีเมล์เสร็จสิ้น']);
    }

    public function quotationlist()
    {

        $data = Quotation::leftJoin('users', 'app_quotation.sale_id', '=', 'users.id')->select('app_quotation.*', 'users.name as sale')->orderBy('app_quotation.id', 'desc')->paginate(20);

        return view('admin/quotation/listquotation', compact('data'));
    }

    public function mainpickupcost()
    {

        $country_all = Address::select('country_name', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        return view('/admin/quotation/mainpickupcost', compact('country_all'));
    }

    public function mainpickupcost_ifram()
    {

        $country_all = Address::select('country_name', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        return view('/admin/quotation/mainpickupcost_ifram', compact('country_all'));
    }

    public function edit($id)
    {

        $data = Quotation::where('id', $id)->first();
        $shipment = Quotation_detail::where('quota_id', $id)->get();
        foreach ($shipment as $key => $value) {
            $value->box = Quotation_box::where('quota_detail_id', $value->id)->get();
            $value->other = Quotation_other::where('quota_detail_id', $value->id)->get();
        }

        $sale = User::where('id', $data->sale_id)->first();
        $data->salename = $sale->name;
        return view('admin/quotation/edit', compact('data', 'shipment'));
    }

    public function update(Request $request, $id)
    {
        $now = strtotime(date('Y-m-d H:i:s'));
        $Quotation = Quotation::where('id', $id)->first();
        $Quotation->customer_id = $request->customer_id;
        $Quotation->company = $request->company;
        $Quotation->name = $request->name;
        $Quotation->address = $request->address;
        $Quotation->tel = $request->tel;
        $Quotation->email = $request->email;
        $Quotation->quote_no = 'QT' . $now;
        $Quotation->payment = $request->payment;
        $Quotation->valid_day = $request->valid_day;
        $Quotation->reference = $request->reference;
        $Quotation->discount = $request->discount;
        $Quotation->withholding = $request->withholding;
        $Quotation->quotation_toppic = $request->quotation_toppic;
        $Quotation->translate = $request->translate;
        $Quotation->total_price = $request->total_price;
        $Quotation->sale_id = Auth::user()->id;
        $Quotation->save();

        $Quotation_detail_list = Quotation_detail::where('quota_id', $id)->pluck('id')->all();
        Quotation_other::whereIn('quota_detail_id', $Quotation_detail_list)->delete();
        Quotation_box::whereIn('quota_detail_id', $Quotation_detail_list)->delete();
        Quotation_detail::where('quota_id', $id)->delete();

        $shipcount = count($request->s_name);
        for ($i = 0; $i < $shipcount; $i++) {
            $Quotation_detail = new Quotation_detail;
            $Quotation_detail->quota_id = $Quotation->id;
            $Quotation_detail->s_id = $request->s_id[$i];
            $Quotation_detail->s_name = $request->s_name[$i];
            $Quotation_detail->s_price = $request->s_price[$i];
            $Quotation_detail->risk = $request->risk[$i];
            $Quotation_detail->country_name = $request->country[$i];
            // ส่วนเพิ่มเติมของบริการ
            $Quotation_detail->wht_one = $request->wht_one[$i];
            $Quotation_detail->wht_tree = $request->wht_tree[$i];
            $Quotation_detail->unit = $request->unit[$i];
            $Quotation_detail->vat_serven = $request->vat_serven[$i];
            // ค่าเอกสาร
            $Quotation_detail->wht_handingfee = $request->wht_handingfee[$i];
            $Quotation_detail->save();

            $boxcount = count($request->width[$i]);
            for ($ii = 0; $ii < $boxcount; $ii++) {
                $Quotation_box = new Quotation_box;
                $Quotation_box->quota_detail_id = $Quotation_detail->id;
                $Quotation_box->weight = $request->weight[$i][$ii];
                $Quotation_box->length = $request->length[$i][$ii];
                $Quotation_box->height = $request->height[$i][$ii];
                $Quotation_box->width = $request->width[$i][$ii];
                $Quotation_box->total_weight = $request->weight[$i][$ii];
                $Quotation_box->true_width = $request->width[$i][$ii];
                $Quotation_box->volume_width = $request->volume_width[$i][$ii];
                $Quotation_box->series = $ii;
                $Quotation_box->save();
            }

            // ส่วนของบริการเสริม
            if(isset($request->other_name[$i])) {
                $other_service = count($request->other_name[$i]);
                for ($ii = 0; $ii < $other_service; $ii++) {
                    $Quotation_other = new Quotation_other;
                    $Quotation_other->quota_detail_id = $Quotation_detail->id;
                    $Quotation_other->other_name = $request->other_name[$i][$ii];
                    $Quotation_other->other_wht_one = $request->other_wht_one[$i][$ii];
                    $Quotation_other->other_wht_tree = $request->other_wht_tree[$i][$ii];
                    $Quotation_other->vat_other_service = $request->vat_other_service[$i][$ii];
                    $Quotation_other->unit_other_service = $request->unit_other_service[$i][$ii];
                    $Quotation_other->service_value = $request->service_value[$i][$ii];
                    $Quotation_other->total_other_service = $request->total_other_service[$i][$ii];
                    $Quotation_other->save();
                }
            }
        }
        return redirect('admin/quotation/view/' . $Quotation->id);
    }

    public function introduce()
    {
        $data = Teamplete_mail_sms::query()->whereNull('group_name')->get();
        return view('admin/quotation/introduce', compact('data'));
    }

    public function save_teamplete(Request $request)
    {
        $teamplete_mail_sms = Teamplete_mail_sms::where("id", $request->id)->first();
        if ($teamplete_mail_sms != null) {
            $teamplete_mail_sms->teamplate_name = $request->teamplate_name;
            $teamplete_mail_sms->email_topic = $request->email_topic;
            $teamplete_mail_sms->teamplate_mail = $request->teamplate_mail;
            $teamplete_mail_sms->teamplate_sms = $request->teamplate_sms;
            $teamplete_mail_sms->group_name = $request->group_name;
            $teamplete_mail_sms->cc = $request->cc;
            $teamplete_mail_sms->bcc = $request->bcc;
            $teamplete_mail_sms->save();
        } else {
            $teamplete = new Teamplete_mail_sms;
            $teamplete->teamplate_name = $request->teamplate_name;
            $teamplete->email_topic = $request->email_topic;
            $teamplete->teamplate_mail = $request->teamplate_mail;
            $teamplete->teamplate_sms = $request->teamplate_sms;
            $teamplete->group_name = $request->group_name;
            $teamplete->cc = $request->cc;
            $teamplete->bcc = $request->bcc;
            $teamplete->save();
        }
        return $request;
    }

    public function delete_templete($id)
    {
        $teamplete_mail_sms = Teamplete_mail_sms::where('id', $id)->delete();
        return $id;
    }

    public function send_introduce(Request $request)
    {
        // return dd($request);
        // ส่ง Email
        $data_array = array(
            'name' => $request->name,
            'tel' => $request->tel,
            'cc' => $request->cc,
            // 'bcc' => $request->bcc,
            'email_detail' => $request->email_detail,
            'email_topic' => $request->email_topic,
            'sms_detail' => $request->sms_detail,
            'email' => explode(',', $request->email)
        );
        /* Mail::send('mails.introduce', $data_array, function ($message) use ($data_array) {
           $message->to($data_array['email']);
           $message->from('SERVICE@SMESHIPPING.COM', 'SME SHIPPING');
           $message->subject($data_array['email_topic']);
         });
         */
        // Backup your default mailer
        $backup = Mail::getSwiftMailer();

        // Setup your gmail mailer
        $transport = new Swift_SmtpTransport('smtp.gmail.com', 587, 'tls');
        $transport->setUsername('ba1@smeshipping.com');
        $transport->setPassword('');
        // Any other mailer configuration stuff needed...
        $gmail = new Swift_Mailer($transport);

        // Set the mailer as gmail
        Mail::setSwiftMailer($gmail);

        // Send your message
        Mail::send('mails.introduce', $data_array, function ($message) use ($data_array) {
            $message->to($data_array['email']);
            $message->from('ba1@smeshipping.com', 'SME SHIPPING');
            $message->cc($data_array['cc']);
            // $message->bcc($data_array['bcc']);
            $message->subject($data_array['email_topic']);
        });
        // Restore your original mailer
        Mail::setSwiftMailer($backup);

        $result = 'ส่งอีเมล์เสร็จสิ้น';
        // ส่ง SMS
        if ($request->sms_detail != null && $request->tel != null) {
            $textforsms = $data_array['sms_detail'];
            $tellist = explode(',', $request->tel);
            $tellist_array = array_map('trim', $tellist);
            foreach ($tellist_array as $key => $value) {
                if (strlen($value) == 10) { //เช็คว่าเบอร์ครบ 10 ตัวหรือไม่
                    $tel = preg_replace('/0/', '66', $value, 1);
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api-service.ants.co.th/sms/send",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => '{"messages":[{"from":"SMESHIPPING","destinations":[{"to":"' . $tel . '"}],"text":"' . $textforsms . '"}]}',
                        CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/json",
                            "Authorization: Basic U01FX1Byb2R1Y3Rpb246WEdlUTE1SnU="
                        ),
                    ));
                    $response = curl_exec($curl);
                    curl_close($curl);
                }
            }
            $result = 'ส่งอีเมล์ และ SMS เสร็จสิ้น';
        }

        return redirect()->back();
    }
}
