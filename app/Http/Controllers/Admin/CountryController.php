<?php

namespace App\Http\Controllers\admin;

use App\Address;
use App\Country;
use App\Postcode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CountryController extends Controller
{
 public function __construct()
 {
  //$this->middleware('auth');
 }
 public function address_verify($country = null, $cityname = null, $zipcode = null)
 {
   $zipcode = str_replace("-","",$zipcode);
   $zipcode = str_replace(" ","",$zipcode);
  if ($country != null && $cityname != null && $zipcode != null) {
   //กรณีใส่ข้อมูลมาครบ ตรวจสอบว่าข้อมูลนั้นถูกต้องหรือไม่ return True/False , State
   $result_ = Address::where('country_name', $country)
    ->where('city_name', $cityname)
    ->select('zip_low', 'zip_high')
    ->distinct()
    ->get();
   $check_zip = Address::where('country_name', $country)
    ->where('city_name', $cityname)
    ->where(function ($query) use ($zipcode) {
     $query->where('zip_low', $zipcode)
      ->orWhere('zip_high', $zipcode);
    })
    ->distinct()
    ->count();
   $check_res = '0';
   $result    = collect();
   if ($check_zip > 0) {
    $result->add(self::checkformat($country,$zipcode));
   } else {
    foreach ($result_ as $value) {
     if ($value->zip_low <= $zipcode && $zipcode <= $value->zip_high) {
        $check_res = self::checkformat($country,$zipcode);
     } else if (strpos($zipcode, $value->zip_low) === 0) {
        $check_res = self::checkformat($country,$zipcode);
     }
    }
    
    if ($check_res != '0') {
     $result->add($check_res);
    } else {
     $result->add('false');
    }
   }

  } else if ($country != null && $cityname != null && $zipcode == null) {
   // กรณีใส่ประเทศและจังหวัดมาแล้ว return zipcode
   $result = Address::where('country_name', $country)
    ->where('city_name', $cityname)
    ->select('zip_low', 'zip_high')
    ->distinct()
    ->get();
  } else if ($country != null && $zipcode == null) {
   // กรณีใส่แต่ประเทศมา return จังหวัด
   $result = Address::where('country_name', $country)
    ->select('city_name')
    ->distinct()
    ->get();
  } else {
   // กรณีไม่ใส่อะไรมา return Country Name
   $result = Address::select('country_name')
    ->orderBy('country_name')
    ->distinct()
    ->get();
  }
  return $result->toJson();
 }

 public function checkformat($country,$zipcode){
    $formate = Postcode::where('region_code',$country)->get();
    $_zipcode  = str_replace("-","",$zipcode.'');
    $_zipcode = str_replace(" ","",$_zipcode.'');
    $_zipcode_ = preg_replace("/[A-Za-z]/", 'A', $_zipcode);
    $_zipcode_ = preg_replace("/[0-9]/", '9', $_zipcode_);
    $result = $formate[0]->postcode_format;
    $status = false;
    $result_ = collect();
    foreach ($formate as $match) {
     $_match  = str_replace("-","",$match->postcode_format);
     $_match = str_replace(" ","",$_match);
     if(($_match == $_zipcode_||strpos($_match,$_zipcode_)===0)&&strlen($zipcode)+1>=$match->significant_figures){
        $result = $match->postcode_format;
        $status = true;
        if(is_numeric($_zipcode)) {
          $result_ = Address::where('country_cd', $country)
          ->where([['zip_low','<=',$_zipcode],['zip_high','>=',$_zipcode]])
          ->get();
        } else {
          $result_tmb = Address::where('country_cd', $country)
          ->where('zip_low','LIKE',substr($_zipcode,0,2).'%')
          ->get();
          foreach ($result_tmb as $value) {
            if(strpos($_zipcode,$value->zip_low)===0){
              $result_->push($value);
            }
          }
        }
     }
    }
    
   return response()->json(['status' => $status, 'value' => $result ,'res'=>$result_]);
 }
 public function index(Request $request)
 {
   if($request->keyword!=null){
    $country = Country::where('country', 'like', '%'.$request->keyword.'%')->paginate(20);
   }
   else{
    $country = Country::paginate(20);
   }
  
  return view('admin/country/index', compact('country'));
 }

 public function getaddress_thailand($province = null, $amphur = null, $tumbon = null)
 {
  if ($province == null) {
   $result = DB::table('province')->get();
  } else if ($province != null && $amphur == null) {
   $result = DB::table('amphur')->where('provinceID', $province)->get();
  } else {
   $result = DB::table('tumbon')->where('amphurID', $amphur)->get();
  }

  return $result->toJson();
 }

 public function create()
 {
  return "";
 }

 public function store(Request $request)
 {
   $now      = strtotime(date('Y-m-d H:i:s'));
   $country = new Country;
   $country->iata = $request->iata;
   $country->code = $request->code;
   $country->country = $request->country;
   $country->risk_price = $request->risk_price;
   $country->time = $now;
   $country->del = '';
   $country->manual = '2';
   $country->landing = 'yes';
   $country->note = $request->note;
   $country->save();
  return "save";
 }

 public function show($id)
 {
  return "";
 }
 public function edit($id)
 {
  return "";
 }

 public function update(Request $request, $id)
 {
  $now      = strtotime(date('Y-m-d H:i:s'));
   $country = Country::where('id',$id)->first();
   $country->iata = $request->iata;
   $country->code = $request->code;
   $country->country = $request->country;
   $country->risk_price = $request->risk_price;
   $country->time = $now;
   $country->note = $request->note;
   $country->save();
  return "update";
 }

 public function city_verify($country_code)
 {
  $result = DB::table('validate_address')->Where('country_cd',$country_code)->select('city_name')->groupBy('city_name')->get();
  return $result->toJson();
 }

 public function getsuburb($country=null,$city=null)
 {
     $result = DB::table('validate_suburb')->Where('country_cd',$country)->Where('city',$city)->select('suburb')->groupBy('suburb')->orderBy('suburb')->get();
  return $result->toJson();
 }
}
