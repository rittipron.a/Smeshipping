<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ManifestDetail;
use File;
use Image;
use App\Pickup_SO;
use App\Pack_detail;
use App\Attachment_Payment;
use App\ManifestHistory;
use App\Manifest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UploadController extends Controller
{
 public function __construct()
 {
  $this->middleware('auth');
 }

 public function upload_invoice(Request $request, $id){

  $file = $request->file('file')->getClientOriginalName();
  $fileName = pathinfo($file,PATHINFO_FILENAME);
  $imageName = time().'_'.$fileName.'.'.$request->file('file')->getClientOriginalExtension();
  $request->file('file')->move(public_path('/uploads/invoice'), $imageName);

  $update = ManifestDetail::where('id',$id)->first();
  $update->invoice_file = $imageName;
  $update->save();
  $history            = new ManifestHistory;
  $history->topic     = 'แนบไฟล์ Commercial invoice';
  $history->detail    = 'SO '.$update->so;
  $history->update_by = Auth::user()->id;
  $history->inv_id    = $update->inv_id;
  $history->save();
  return $imageName;
 }
 public function upload_resetinvoice($id){
  $update = ManifestDetail::where('id',$id)->first();
  File::delete(public_path('/uploads/invoice/').$update->invoice_file);
  $update->invoice_file = null;
  $update->save();

  $history            = new ManifestHistory;
  $history->topic     = 'ลบไฟล์ Commercial invoice';
  $history->detail    = 'SO '.$update->so;
  $history->update_by = Auth::user()->id;
  $history->inv_id    = $update->inv_id;
  $history->save();
  return 'true';
 }

   public function upload_resetpayment($id){
    $update = Attachment_Payment::where('id',$id)->first();
    File::delete(public_path('/uploads/payment/').$update->filename);
    $inv_id = $update->inv_id;
    
    $update->delete();
    $count = Attachment_Payment::where('inv_id',$inv_id)->count();
    if($count>0){
        $update_m = Manifest::where('id',$inv_id)->first();
        $update_m->payment_file_re = 'true';
        $update_m->save();
    }
    else{
        $update_m = Manifest::where('id',$inv_id)->first();
        $update_m->payment_file_re = null;
        $update_m->save();
    }
    $history            = new ManifestHistory;
    $history->topic     = 'ลบหลักฐานการโอนเงิน';
    $history->detail    = '';
    $history->update_by = Auth::user()->id;
    $history->inv_id    = $inv_id;
    $history->save();
    return 'true';
   }
   
   public function upload_so(Request $request, $id){
    $data = collect();
    $files = $request->file('files');

    if($request->hasFile('files'))
    {
        foreach ($files as $fileitem) {
            $file = $fileitem->getClientOriginalName();
            $fileName = pathinfo($file,PATHINFO_FILENAME);
            $imageName = time().'_'.$fileName.'.'.$fileitem->getClientOriginalExtension();
            $fileitem->move(public_path('/uploads/so'), $imageName);
            $img = Image::make(public_path('/uploads/so/').$imageName);
            $img->resize(null, 1000, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(public_path('/uploads/so/').$imageName);
            $attach = new Pickup_SO;
            $attach->inv_id = $id;
            $attach->so_image = $imageName;
            $attach->so_note = $request->so;
            $attach->save();
            $data->push($attach);
        }
    }
    return $data->toJson();
   }

   public function upload_pack(Request $request, $id){
    $files = $request->file('files');
    $filename = null;
    if($request->hasFile('files'))
    {
        foreach ($files as $index=>$fileitem) {
            $file = $fileitem->getClientOriginalName();
            $fileName = pathinfo($file,PATHINFO_FILENAME);
            $imageName = time().'_'.$fileName.'.'.$fileitem->getClientOriginalExtension();
            $fileitem->move(public_path('/uploads/pack'), $imageName);
            $img = Image::make(public_path('/uploads/pack/').$imageName);
            $img->resize(null, 1000, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(public_path('/uploads/pack/').$imageName);
            $filename =$filename.($index!=0?',':'').$imageName;
        }
        $attach = new Pack_detail;
        $attach->m_id = $id;
        $attach->ref = $request->so;
        $attach->note = $request->note;
        $attach->image = $filename;
        $attach->status = "0";
        $attach->type = $request->type;
        $attach->save();
    }
    $history            = new ManifestHistory;
    $history->topic     = 'Update Pack Detail';
    $history->detail    = 'ลงรายละเอียดพัสดุที่รับมาจากลูกค้า';
    $history->update_by = Auth::user()->id;
    $history->inv_id    = $id;
    $history->save();
    return $attach->toJson();
   }

   public function upload_payment(Request $request, $id){
    $data = collect();
    $files = $request->file('files');

    if($request->hasFile('files'))
    {
        foreach ($files as $fileitem) {
            $file = $fileitem->getClientOriginalName();
            $fileName = pathinfo($file,PATHINFO_FILENAME);
            $imageName = time().'_'.$fileName.'.'.$fileitem->getClientOriginalExtension();
            $fileitem->move(public_path('/uploads/payment'), $imageName);
            $img = Image::make(public_path('/uploads/payment/').$imageName);
            $img->resize(null, 1000, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(public_path('/uploads/payment/').$imageName);
            $attach = new Attachment_Payment;
            $attach->inv_id = $id;
            $attach->filename = $imageName;
            $attach->save();
            $data->push($attach);
        }
    }
    $count = Attachment_Payment::where('inv_id',$id)->count();
    if($count>0){
        $update_m = Manifest::where('id',$id)->first();
        $update_m->payment_file_re = 'true';
        $update_m->save();
    }
    else{
        $update_m = Manifest::where('id',$id)->first();
        $update_m->payment_file_re = null;
        $update_m->save();
    }
    
    $history            = new ManifestHistory;
    $history->topic     = 'แนบหลักฐานการโอน';
    $history->detail    = '';
    $history->update_by = Auth::user()->id;
    $history->inv_id    = $id;
    $history->save();
    return $data->toJson();
   }

   public function del_so($id){
    $update = Pickup_SO::where('id',$id)->first();
    File::delete(public_path('/uploads/so/').$update->so_image);
    $update->delete();
    return 'true';
   }

   public function del_pack($id){
    $update = Pack_detail::where('id',$id)->first();
    File::delete(public_path('/uploads/pack/').$update->image);
    $update->delete();
    return 'true';
   }
}

