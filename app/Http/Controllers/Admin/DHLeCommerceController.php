<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
use Illuminate\Support\Facades\DB;
use App\ManifestDetail_box;
use App\ManifestDetail_Other_Service;
use App\ManifestDetail;
use App\Manifest;
use App\Customer;
use App\Pickup;
use App\ServiceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
class DHLeCommerceController extends Controller
{
 public function __construct()
 {
  $this->middleware('auth');
 }
 public function create_awb($id)
 {
     $arr_id = explode(",",$id);
     $manifest_detail = ManifestDetail::whereIn('id',$arr_id)->get();   
     $collection = collect();
     //-- ข้อมูล API --------
     /*
     $clientId = "MTY2MTI2NzczNw==";
     $clientps = "MjAzMDI5MTU";
     $SoldToAcct = "5999991600";
     $PickupAcct = "5999991600";
     $prefix = 'THBQP';
     $api_url = "https://sandbox.dhlecommerce.asia";
     */
     //-- ข้อมูล API --------
     
     $clientId = "MTM0Njg4MTA5Nw==";
     $clientps = "MjAzMDI5MTU";
     $SoldToAcct = "5249536745";
     $PickupAcct = "5249536745";
     $prefix = 'THBQP';
     $api_url = "https://api.dhlecommerce.dhl.com";
     
     $curl_token = curl_init();

     curl_setopt_array($curl_token, array(
       CURLOPT_URL => $api_url."/rest/v1/OAuth/AccessToken?clientId=".$clientId."&password=".$clientps."&returnFormat=json",
       CURLOPT_RETURNTRANSFER => true,
       CURLOPT_ENCODING => "",
       CURLOPT_MAXREDIRS => 10,
       CURLOPT_TIMEOUT => 0,
       CURLOPT_FOLLOWLOCATION => true,
       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
       CURLOPT_CUSTOMREQUEST => "GET",
     ));
     
     $response_token = curl_exec($curl_token);
     curl_close($curl_token);
     $data_token = iconv('UTF-8', 'UTF-8//IGNORE', $response_token);
     $array_data = json_decode($data_token, true);
     //return dd($manifest_detail);

     $curl = curl_init();
     $shipmentitem = "";
     foreach ($manifest_detail as $key => $value) {
      $manifest = Manifest::where('id',$value->inv_id)->first();
      $box = ManifestDetail_box::where('shipment_id',$id)->where('status','<',10)->get();
      $sender = Customer::where('id',$value->m_id)->first();
      $service = Service::where('id',$value->services)->first();
      $servicetype = ServiceType::where('id',$service->cat)->first();

      $shipmentitem .= ($key>0?",":"")."{\r\n          
        \"consigneeAddress\": {\r\n            
          \"name\": \"".$value->recipient."\",\r\n            
          \"address1\": \"".$value->address."\",\r\n            
          \"address2\": \"".$value->address2."\",\r\n            
          \"address3\": \"".$value->address3."\",\r\n            
          \"city\": \"".$value->city."\",\r\n           
          \"state\": ".($value->state!=null?'"'.$value->state.'"':"null").",\r\n            
          \"district\": null,\r\n             
          \"country\": \"".$value->country_code."\",\r\n            
          \"postCode\": ".($value->zipcode!=null?'"'.$value->zipcode.'"':"null").",\r\n            
          \"phone\": ".($value->phone!=null?'"'.$value->phone.'"':"null").",\r\n            
          \"email\": ".($value->email!=null?'"'.$value->email.'"':"null").",\r\n            
          \"idNumber\": null,\r\n            
          \"idType\": null\r\n          
        },\r\n          
        \"shipmentID\": \"".$prefix.$value->so."\",\r\n          
        \"deliveryConfirmationNo\": null,\r\n          
        \"packageDesc\": \"".$value->description."\",\r\n          
        \"totalWeight\": ".($value->weight*1000).",\r\n          
        \"totalWeightUOM\": \"G\",\r\n          
        \"dimensionUOM\": \"CM\",\r\n          
        \"height\": null,\r\n          
        \"length\": null,\r\n          
        \"width\": null,\r\n          
        \"customerReference1\": null,\r\n          
        \"customerReference2\": null,\r\n          
        \"productCode\": \"".$value->shipping_type."\",\r\n          
        \"incoterm\":  null,\r\n          
        \"contentIndicator\": null,\r\n          
        \"codValue\": null,\r\n          
        \"insuranceValue\": ".($value->insurance==null?"null":$value->insurance).",\r\n          
        \"freightCharge\": null,\r\n          
        \"totalValue\": ".$value->declared_value.",\r\n          
        \"currency\": \"".$value->currency."\",\r\n          
        \"remarks\": ".($value->note!=null?'"'.$value->note.'"':"null").",\r\n          
        \"billingReference1\": null,\r\n          
        \"billingReference2\": null,\r\n          
        \"isMult\": \"false\",\r\n          
        \"deliveryOption\": \"C\",\r\n          
        \"shipmentContents\": [\r\n            
        {\r\n              
          \"skuNumber\":\"".($value->content_quantity!=null?$value->content_quantity:0)."\",\r\n              
          \"description\": \"".$value->description."\",\r\n              
          \"itemValue\": ".$value->declared_value.",\r\n              
          \"itemQuantity\": ".($value->content_quantity!=null?$value->content_quantity:0).",\r\n              
          \"grossWeight\": ".($value->weight*1000).",\r\n              
          \"netWeight\": ".($value->weight*1000).",\r\n              
          \"weightUOM\": \"G\",\r\n              
          \"hsCode\": null\r\n            
        }\r\n          
        ]\r\n        
      }\r\n ";
     }
     $str = "{\r\n
      \t\"labelRequest\": {\r\n
        \t\t\"hdr\": {\r\n
          \t\t\t\"messageType\": \"LABEL\",\r\n
          \t\t\t\"messageDateTime\": \"" . date('Y-m-d') . 'T' . date('H:i:s') . "\",\r\n
          \t\t\t\"accessToken\": \"".$array_data["accessTokenResponse"]["token"]."\",\r\n
          \t\t\t\"messageVersion\": \"1.4\",\r\n
          \t\t\t\"messageLanguage\": \"th_TH\"\r\n
          \t\t},\r\n
              \"bd\": {\r\n
                      \"inlineLabelReturn\": \"U\",\r\n
                      \"customerAccountId\": null,\r\n
                      \"pickupAccountId\": \"".$PickupAcct."\",\r\n
                      \"soldToAccountId\": \"".$SoldToAcct."\",\r\n
                      \"handoverMethod\": null,\r\n
                      \"pickupDateTime\": null,\r\n
                      \"pickupAddress\": {\r\n        
                        \"name\": \"SME SHIPPING CO.LTD\",\r\n        
                        \"address1\": \"46 RATCHADAPISEK 16 RATCHADAPISEK ROAD THAPRA -Y\",\r\n        
                        \"city\": \"BANGKOK\",\r\n        
                        \"state\": \"BANGKOK\",\r\n        
                        \"district\": \"Bangkok\",\r\n        
                        \"country\": \"TH\",\r\n        
                        \"postCode\": \"10600\",\r\n        
                        \"email\": \"service@smeshipping.com\"\r\n      
                      },\r\n      
                      \"shipmentItems\": [".$shipmentitem."],\r\n      
                    \"label\": {\r\n        
                      \"pageSize\": \"400x600\",\r\n        
                      \"format\": \"PDF\",\r\n       
                      \"layout\": \"1x1\"\r\n      
                    }\r\n    
                  }\r\n  
                }\r\n}" ;

     curl_setopt_array($curl, array(
       CURLOPT_URL => $api_url."/rest/v2/Label",
       CURLOPT_RETURNTRANSFER => true,
       CURLOPT_ENCODING => "",
       CURLOPT_MAXREDIRS => 10,
       CURLOPT_TIMEOUT => 0,
       CURLOPT_FOLLOWLOCATION => true,
       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
       CURLOPT_CUSTOMREQUEST => "POST",
       CURLOPT_POSTFIELDS => $str,
       CURLOPT_HTTPHEADER => array(
         "Content-Type: application/json"
       ),
     ));
     
     $response = curl_exec($curl);
      
     curl_close($curl);

     $data = iconv('UTF-8', 'UTF-8//IGNORE', $response);
     $result= json_decode($data, true);
     $collection->push($result);
    if (!empty($result["labelResponse"]["bd"]["responseStatus"]["code"])) {
      if($result["labelResponse"]["bd"]["responseStatus"]["code"] == "200" ){
        $res = $result["labelResponse"]["bd"]["labels"];
        $shipmentID_arr = "";
        foreach ($res as $key => $value) {
          $shipmentID_arr .= ($key>0?",":"")."{\r\n
            \t\"shipmentID\":\"".$value["shipmentID"]."\"\r\n
          \t}\r\n";
        }

        $curl_closeOut = curl_init();

        curl_setopt_array($curl_closeOut, array(
          CURLOPT_URL => $api_url."/rest/v2/Order/Shipment/CloseOut",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>"{\"closeOutRequest\":\r\n
            \t{\"hdr\":\r\n
              \t\t{\r\n
                \t\"messageType\":\"CLOSEOUT\",\r\n
                \t\"messageDateTime\":\"" . date('Y-m-d') . 'T' . date('H:i:s') . "\",\r\n
                \t\"accessToken\":\"".$array_data["accessTokenResponse"]["token"]."\",\r\n
                \t\"messageLanguage\":\"th_TH\",\r\n
                \t\"messageVersion\":\"1.3\"},\r\n
                \t\"bd\":{\r\n
                  \t\"customerAccountId\":null,\r\n
                  \t\"pickupAccountId\":\"".$PickupAcct."\",\r\n
                  \t\"soldToAccountId\":\"".$SoldToAcct."\",\r\n
                  \t\"handoverID\":null,\r\n
                  \t\"generateHandover\":\"Y\",\r\n
                  \t\"handoverMethod\":2,\r\n
                  \t\"shipmentItems\":[\r\n
                  \t".$shipmentID_arr."
                \t]\r\n
                \t}\r\n
                \t}\r\n
              \t}",
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));

        $response_closeOut = curl_exec($curl_closeOut);
        $data_closeOut = iconv('UTF-8', 'UTF-8//IGNORE', $response_closeOut);
        $result_closeOut= json_decode($data_closeOut, true);
        $collection->push($result_closeOut);
        curl_close($curl_closeOut);
        if (!empty($result_closeOut["closeOutResponse"]["bd"]["responseStatus"]["code"])) {
          if($result_closeOut["closeOutResponse"]["bd"]["responseStatus"]["code"] == "200"){
            $closeOut_base64      = base64_decode($result_closeOut["closeOutResponse"]["bd"]["handoverNote"]);
            $closeOut_name = $result_closeOut["closeOutResponse"]["bd"]["handoverID"].'.pdf';
            $path_closeOut      = public_path() . "/uploads/labels/" . $closeOut_name;
            file_put_contents($path_closeOut, $closeOut_base64);

            foreach ($res as $key => $value) {
              $code = str_replace($prefix,"",$value["shipmentID"]);
              
              $file_url =  $value["labelURL"];
              $path = public_path('uploads/labels');
              $url = $value["labelURL"];
              $fileName = $value["shipmentID"].'.zip';
              file_put_contents($path .'/'. $fileName, file_get_contents($url));
              $this_manifest = ManifestDetail::whereIn('id',$arr_id)->where('so',$code)->first();
              if($this_manifest!=null){
                $this_manifest->awb_file = $fileName;
                $this_manifest->awb = $value["shipmentID"];
                $this_manifest->handover_note = $closeOut_name;
                $this_manifest->save();
                return response()->json(['status' => 'yes']);     
              }   
              else{
                return response()->json(['status' => 'no']);
              }  
            }
            
          }
        }
        
      } else {
        return $collection;
      }
    }else{
      return $collection;
    }
    return $collection;
 }

 public function replacestring($string)
 {
     $string = str_replace('&', '&amp;', $string);
     $string = str_replace('<', '&lt;', $string);
     $string = str_replace('>', '&gt;', $string); 
  return $string;
 }

}
