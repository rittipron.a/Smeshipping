<?php

namespace App\Http\Controllers\admin;

use App\Employee;
use App\Http\Controllers\Controller;
use App\Manifest;
use App\ManifestHistory;
use App\Pickup;
use App\Pickup_SO;
use App\ManifestDetail;
use App\ManifestDetail_box;
use App\ManifestDetail_Other_Service;
use App\CustomerBookingNote;
use App\Address;
use App\Customer;
use App\AppBoxCheckin;
use App\Attachment_Payment;
use App\DiscountUsed;
use App\PaymentAttachmentFromCustomer;
use App\Pack_detail;
use Mail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $data = self::show_query($request);
        $data = $data->where('app_manifest.status', 1);
        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.created_at', 'desc')
            ->distinct()
            ->paginate(20);

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        return view('admin/request/inbox', compact('data'))->with('title', 'Search History')->with(compact('mess', 'sale'))->with('title', 'Request');
    }

    public function hold(Request $request)
    {
        $data = self::show_query($request);
        $data = $data->where('app_manifest.status', '>', 100);
        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.created_at', 'desc')
            ->distinct()
            ->paginate(20);

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        return view('admin/request/inbox', compact('data'))->with('title', 'Search History')->with(compact('mess', 'sale'))->with('title', 'Request');
    }

    public function cancel(Request $request)
    {
        $data = self::show_query($request);
        $data = $data->where('app_manifest.status', 99);
        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.created_at', 'desc')
            ->distinct()
            ->paginate(20);

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        return view('admin/request/inbox', compact('data'))->with('title', 'Search History')->with(compact('mess', 'sale'))->with('title', 'Request');
    }

    public function waitforsale(Request $request)
    {
        $data = self::show_query($request);
        $data = $data->where('app_manifest.status', 0);
        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.created_at', 'desc')
            ->distinct()
            ->paginate(20);

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        return view('admin/request/inbox', compact('data'))->with('title', 'Search History')->with(compact('mess', 'sale'))->with('title', 'Request');
    }

    public function waitforpickup(Request $request)
    {
        $data = self::show_query($request);
        $data = $data->where('app_manifest.status', 2);
        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.created_at', 'desc')
            ->distinct()
            ->paginate(20);

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        return view('admin/request/inbox', compact('data'))->with('title', 'Search History')->with(compact('mess', 'sale'))->with('title', 'Request');
    }

    public function waitforcalculate(Request $request)
    {
        $data = self::show_query($request);
        $data = $data->where('app_manifest.status', 3);

        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.created_at', 'desc')
            ->distinct()
            ->paginate(20);

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        return view('admin/request/inbox', compact('data'))->with('title', 'Search History')->with(compact('mess', 'sale'))->with('title', 'Request');
    }

    public function waitforpayment(Request $request)
    {
        $data = self::show_query($request);
        $data = $data->where('app_manifest.status', 4)->whereNull('app_manifest.payment_file_re');
        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.created_at', 'desc')
            ->distinct()
            ->paginate(20);

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        return view('admin/request/inbox', compact('data'))->with('title', 'Search History')->with(compact('mess', 'sale'))->with('title', 'Request');
    }

    public function waitforproof(Request $request)
    {
        $data = self::show_query($request);
        $data = $data->where('app_manifest.status', 4)->whereNotNull('app_manifest.payment_file_re');

        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.created_at', 'desc')
            ->distinct()
            ->paginate(20);

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        return view('admin/request/inbox', compact('data'))->with('title', 'Search History')->with(compact('mess', 'sale'))->with('title', 'Request');
    }

    public function waitforrelease(Request $request)
    {
        $data = self::show_query($request);
        $data = $data->where('app_manifest.status', 5);
        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.created_at', 'desc')
            ->distinct()
            ->paginate(20);

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        return view('admin/request/inbox', compact('data'))->with('title', 'Search History')->with(compact('mess', 'sale'))->with('title', 'Request');
    }

    public function search_history(Request $request)
    {
        $data = self::show_query($request);//return SQL only not result
        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.updated_at', 'desc')
            ->distinct()
            ->paginate(20);
        if ($request->so != null && count($data) == 0) {
            $data = self::show_query_so($request);//return result
        }

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        return view('admin/request/search_history', compact('data'))->with('title', 'Search History')->with(compact('mess', 'sale'))->with('title', 'Request');
    }

    public function show_query($request)
    {
        $value = $request->keyword;
        $date = strtotime(str_replace("/", "-", $request->delivery_date));
        $data = DB::table('app_manifest')
            ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
            ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
            ->leftJoin('app_services_type', 'app_services_type.id', '=', 'pickup_manifest.type_of_service');
        if ($request->status != null) {
            $data = $data->where('app_manifest.status', $request->status);
        }
        if ($request->driver != null) {
            $data = $data->where('pickup_manifest.driver', '' . $request->driver);
        }
        if ($request->sale != null) {
            $data = $data->where('pickup_manifest.sales', '' . $request->sale);
        }
        if ($request->delivery_date != null) {
            $data = $data->where('pickup_manifest.delivery_date', '=', $date);
        }
        if ($request->payment_value != null) {
            $data = $data->where('app_manifest.price_pay', '=', $request->payment_value);
        }
        if ($request->keyword != null || $request->so != null) {
            $data->leftJoin('app_manifest_detail', 'app_manifest_detail.inv_id', '=', 'app_manifest.id');
        }
        if ($request->keyword != null && $request->keyis == 'tel') {
            $data = $data->Where(function ($query) use ($value) {
                $query->Where('pickup_manifest.phone', 'like', '%' . $value . '%');
            });
        }
        if ($request->type_delivery != null) {
            $data = $data->Where('pickup_manifest.type_delivery', $request->type_delivery);
        }
        if ($request->keyword != null && $request->keyis == 'name') {
            $data = $data->Where(function ($query) use ($value) {
                $query->Where('pickup_manifest.name', 'like', '%' . $value . '%')
                    ->orWhere('pickup_manifest.company', 'like', '%' . $value . '%');
            });
        }
        if ($request->keyword != null && $request->keyis == 'awb') {
            $data = $data->Where(function ($query) use ($value) {
                $query->Where('app_manifest_detail.awb', '=', $value)
                    ->orWhere('app_manifest_detail.tracking_code', '=', $value)
                    ->orWhere('app_manifest_detail.tracking_dhl', '=', $value);
            });
        }
        if ($request->so != null) {
            $sonumber = $request->so;
            $data = $data->Where(function ($query) use ($sonumber) {
                $query->orWhere('app_manifest_detail.so', $sonumber)
                    ->orWhere('app_manifest_detail.tracking_code', $sonumber)
                    ->orWhere('app_manifest_detail.inv_id', $sonumber);
                //->orWhere('pickup_manifest.so','like', '%' . $sonumber. '%');
            });
        }

        return $data;
    }

    public function show_query_so($request)
    {
        $data = DB::table('app_manifest')
            ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
            ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
            ->leftJoin('app_services_type', 'app_services_type.id', '=', 'pickup_manifest.type_of_service')
            ->orWhere('pickup_manifest.so', 'like', '%' . $request->so . '%')
            ->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.updated_at', 'desc')
            ->distinct()
            ->paginate(20);
        return $data;
    }

    public function print(Request $request)
    {
        dd($request->so);
//        $value = $request->keyword;
//        $date = strtotime(str_replace("/", "-", $request->delivery_date));
//        $data = DB::table('app_manifest')
//            ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
//            ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
//            ->leftJoin('app_services_type', 'app_services_type.id', '=', 'pickup_manifest.type_of_service')
//            ->leftJoin('pickup_manifest_so', 'pickup_manifest_so.inv_id', '=', 'app_manifest.id')
//            ->leftJoin('app_manifest_detail', 'app_manifest_detail.inv_id', '=', 'app_manifest.id')
//            ->where('app_manifest.status', '<>', 0)
//            ->where('app_manifest.status', '<', 10);
//        if ($request->keyword != null) {
//            //$data->leftJoin('app_manifest_detail', 'app_manifest_detail.inv_id', '=', 'app_manifest.id');
//            $data->Where(function ($query) use ($value) {
//                $query->Where('pickup_manifest.email', 'like', '%' . $value . '%')
//                    ->orWhere('pickup_manifest.name', 'like', '%' . $value . '%')
//                    ->orWhere('pickup_manifest.company', 'like', '%' . $value . '%')
//                    ->orWhere('pickup_manifest.type_delivery_note', 'like', '%' . $value . '%')
//                    ->orWhere('pickup_manifest.line', 'like', '%' . $value . '%')
//                    ->orWhere('pickup_manifest.phone', 'like', '%' . $value . '%')
//                    ->orWhere('app_manifest.id', '=', $value)
//                    ->orWhere('app_manifest_detail.awb', '=', $value)
//                    ->orWhere('app_manifest_detail.tracking_code', '=', $value);
//            });
//        }
//        if ($request->so != null) {
//            $sonumber = $request->so;
//            $data->Where(function ($query) use ($sonumber) {
//                $query->orWhere('pickup_manifest.so', 'like', '%' . $sonumber . '%')
//                    ->orWhere('app_manifest_detail.so', 'like', '%' . $sonumber . '%');
//            });
//        }
//
//        $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
//            ->orderBy('pickup_manifest.updated_at', 'desc')
//            ->distinct()
//            ->paginate(200);
//
//        foreach ($data as $value) {
//            if ($value->driver != null) {
//                $mess = Employee::where('id', $value->driver)->first();
//                $value->driver = $mess->name;
//            }
//            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
//            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
//            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
//            $value->phonelist = $phonelist;
//            $value->maillist = $maillist;
//            $value->solist = $solist;
//        }
//
//        if ($request->driver != null) {
//            $thisdata = $data->first();
//            $this_mess = $thisdata->driver;
//        }

        $data = self::show_query($request);//return SQL only not result
        $data = $data->select('pickup_manifest.*', 'pickup_manifest.id as pickup_code', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services_type.name as service_name', 'users.name as salename')
            ->orderBy('pickup_manifest.updated_at', 'desc')
            ->distinct()
            ->limit(300)
            ->get();
        if ($request->so != null && count($data) == 0) {
            $data = self::show_query_so($request);//return result
        }

        foreach ($data as $value) {
            if ($value->driver != null) {
                $mess = Employee::where('id', $value->driver)->first();
                $value->driver = $mess->name;
            }
            $phonelist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('phone')->distinct()->get();
            $maillist = DB::table('customer_address')->where('customer_id', $value->m_id)->select('email')->distinct()->get();
            $solist = DB::table('app_manifest_detail')->where('inv_id', $value->manifestid)->select('so', 'awb', 'tracking_local', 'invoice_file')->orderBy('id', 'asc')->distinct()->get();
            $value->phonelist = $phonelist;
            $value->maillist = $maillist;
            $value->solist = $solist;
        }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();

//        $data = Manifest::query()
//            ->where('app_manifest.status', '<>', 0)
//            ->where('app_manifest.status', '<', 10);
//
//        $this_mess = 'ทุกคน';
//        $date = 'ไม่ระบุ';
//
//        if ($request->status) {
//            $data->where('app_manifest.status', $request->status);
//        }
//        if ($request->driver) {
//            $data->where('pickup_manifest.driver', '' . $request->driver);
//        }
//        if ($request->sale) {
//            $data->where('pickup_manifest.sales', '' . $request->sale);
//        }
//        if ($request->delivery_date) {
//            $data->where('pickup_manifest.delivery_date', '=', $date);
//            $date = $request->delivery_date;
//        }

//        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
//        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();

        return view('admin/request/print', compact('data'))->with('title', (($mess) ? $mess->first()->name : "ทุกคน"))->with('date', $request->delivery_date)->with(compact('mess', 'sale'));
    }

    public function create()
    {
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $service_type = DB::table('app_services_type')->orderBy('code')->get();
        return view('admin/request/create', compact('service_type'))->with(compact('mess', 'sale'));
    }

    public function store(Request $request)
    {
        $status = 1; // ตั้งค่าเริ่มต้นเป็น 1 Create
        if ($request->type_delivery == 'walkin' ||
            $request->type_delivery == 'TNT' ||
            $request->type_delivery == 'ไปรษณียไทย' ||
            $request->type_delivery == 'kerry' ||
            $request->type_delivery == 'Lalamove' ||
            $request->type_delivery == 'อื่นๆ') {
            //กรณีเลือก walkin จะไปสถานะ Calculate ทันที นอกนั้นอยู่ที่ Pickup
            $status = 3;
        } else if ($request->driver != '') {
            //กรณีเลือกผู้เข้ารับแล้วจะเป็น 2 Pickup
            $status = 2;
        }
        $customer = Customer::where('id', $request->m_id)->first();

        $pickup = new Pickup;
        $pickup->insighly = $request->insighly == 'on' ? 'yes' : '';
        $pickup->address = $request->address;
        $pickup->type_work = $request->type_work;
        $pickup->type_of_service = $request->type_of_service;
        $pickup->type_delivery = $request->type_delivery;
        $pickup->type_delivery_note = $request->type_delivery_note;
        $pickup->delivery_date = strtotime(str_replace("/", "-", $request->delivery_date));
        $pickup->delivery_time = $request->time . " " . $request->delivery_time;
        $pickup->shipment = $request->shipment;
        $pickup->driver = $request->driver;
        $pickup->m_note = $request->m_note;
        $pickup->type_shipping = $request->type_shipping;
        $pickup->m_id = $request->m_id;
        $pickup->name = $request->name;
        $pickup->company = $request->company;
        $pickup->phone = $request->phone;
        $pickup->line = $request->line;
        $pickup->email = $request->email;
        $pickup->status = $status;
        $pickup->sales = $request->sale;
        $pickup->save();


        $manifest = new Manifest;
        $manifest->m_id = $request->m_id;
        $manifest->user_create = Auth::user()->id;
        $manifest->status = $status;
        $manifest->price_pay = $request->payment_value != '' ? $request->payment_value : 0;
        $manifest->pay = $request->payment_value_formess != '' ? $request->payment_value_formess : 0;
        $manifest->type_pay = $request->payment_type;
        $manifest->discount = 0.00;
        $manifest->p_id = $pickup->id;
        $manifest->save();

        $history = new ManifestHistory;
        $history->topic = 'Create';
        $history->detail = 'Booking.';
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();
        return $manifest->id;
    }

    public function show($id)
    {
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $sale = Employee::whereNotIn('position', ['Messenger', 'Supervisor Messenger'])->where('active', 1)->get();
        $data = DB::table('app_manifest')
            ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
            ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
            ->leftJoin('app_customer', 'app_customer.id', '=', 'pickup_manifest.m_id')
            ->where('app_manifest.id', '=', $id)
            ->select('pickup_manifest.*', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'users.name as salename', 'app_customer.service_group', 'app_customer.note')
            ->first();
        $so = Pickup_SO::where('inv_id', $id)->get();
        $pack_detail = Pack_detail::where('m_id', $id)->get();
        if ($data->shipment_file != null) {
            $new_attach = new Pickup_SO;
            $new_attach->so_image = $data->shipment_file;
            $new_attach->so_note = 'ลูกค้าแนบไฟล์ Excel เข้ามา';
            $so->push($new_attach);
        }
        if ($data->m_id != 9999999) {
            $customer_db = DB::table('app_customer')->where('id', '=', $data->m_id)->first();
            $customer = collect();
            $customer->firstname = $data->name;
            $customer->lastname = '';
            $customer->company = $data->company;
            $customer->line = $data->line;
            $customer->note = $data->note;
            $customer->phone = $data->phone;
            $customer->email = $data->email;
            $customer->note = null;
        } else {
            $customer = collect();
            $customer->firstname = $data->name;
            $customer->lastname = '';
            $customer->company = $data->company;
            $customer->line = $data->line;
            $customer->note = $data->note;
            $customer->phone = $data->phone;
            $customer->email = $data->email;
            $customer->note = null;
        }

        $shipment = DB::table('app_manifest_detail')
            ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
            ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
            ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.description as service_name',
                'app_services.cal_weight', 'app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight')
            ->where('app_manifest_detail.inv_id', '=', $data->manifestid)
            ->orderBy('app_manifest_detail.id')
            ->get();
        $box = DB::table('app_manifest_box')
            ->where('inv_id', '=', $data->manifestid)
            ->select('app_manifest_box.*', DB::raw('TRUNCATE((width*length*height)/5000,10) as sum_weight'))
            ->orderBy('series')
            ->get();
        $service_type = DB::table('app_services_type')->where('id', $data->type_of_service)->get();
        $service_type_all = DB::table('app_services_type')->orderBy('code')->get();
        $service_all = DB::table('app_services')
            ->leftJoin('app_services_group_map', 'app_services.id', '=', 'app_services_group_map.service_id')
            ->where('app_services_group_map.group_id', $data->service_group)
            ->whereNull('app_services.del')
            ->where('app_services.active', '1')
            ->select('app_services.*')
            ->orderBy('app_services.code')
            ->get();
        $history = DB::table('manifest_history')
            ->leftJoin('users', 'users.id', '=', 'manifest_history.update_by')
            ->select('users.name', 'manifest_history.*')
            ->where('manifest_history.inv_id', $id)->orderBy('manifest_history.updated_at', 'desc')->get();
        $currency = DB::table('app_currency')->orderBy('code')->get();
        $country_all = Address::select('country_name', 'country_cd', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        if (count($service_type) == 0) {
            $service_type = $service_type_all;
        }
        $data->total_package = count($box); //TOTAL PACKAGE
        $data->total_shipment = count($shipment); //TOTAL SHIPMENT
        foreach ($shipment as $shipitem) {
            $other_service_name = collect();
            $other_service_price = collect();
            //Express

            //Check price is 0
            if ($shipitem->price == 0) {
                $shipitem->price = 100;
            }
            //Other Service
            $other_service_total_price = 0;


            $other_service_list = ManifestDetail_Other_Service::where('shipment_id', $shipitem->id)->get();
            foreach ($other_service_list as $key => $value) {
                $other_service_name->add($value->other_service_name);
                $other_service_price->add($value->other_service_value);
                $other_service_total_price = $other_service_total_price + $value->other_service_value;
            }
            $shipitem->other_service_name = $other_service_name;
            $shipitem->other_service_price = $other_service_price;
            $shipitem->other_service_total_price = $other_service_total_price;
        }

        $attach_payment = Attachment_Payment::where('inv_id', $id)->get();
        //return dd($data);
        return view('admin/request/show')->with(compact('sale', 'data', 'mess', 'customer', 'shipment', 'box', 'service_type', 'history', 'service_all', 'service_type_all', 'currency', 'country_all', 'so', 'attach_payment', 'pack_detail'));
    }

    public function update(Request $request, $id)
    {
        $manifest = Manifest::where('id', $id)->first();
        $now = strtotime(date('Y-m-d H:i:s'));
        if ($manifest->status == '0') {
            $status = 1; // update Draft to Create
            $manifest = Manifest::where('id', $id)->first();
            $manifest->user_update = Auth::user()->name;
            $manifest->status = $status;
            $manifest->price_pay = $request->payment_value != null ? $request->payment_value : 0;
            $manifest->pay = $request->payment_value_formess != null ? $request->payment_value_formess : 0;
            $manifest->type_pay = $request->payment_type;
            $manifest->save();

            $pickup = Pickup::where('id', $manifest->p_id)->first();
            $pickup->insighly = $request->insighly == 'on' ? 'yes' : '';
            $pickup->driver = $request->driver;
            $pickup->status = $status;
            $pickup->sales = Auth::user()->id;
            $pickup->save();

            $history = new ManifestHistory;
            $history->topic = 'Create';
            $history->detail = $request->comment != '' ? $request->comment : 'รับงานจากลูกค้า เปลี่ยนสถานะเป็น Create';
            $history->update_by = Auth::user()->id;
            $history->inv_id = $manifest->id;
            $history->save();
        } else if ($manifest->status == '1') {
            $status = 2; // update create to pickup
            $manifest = Manifest::where('id', $id)->first();
            $manifest->user_update = Auth::user()->name;
            $manifest->status = $status;
            $manifest->price_pay = $request->payment_value != null ? $request->payment_value : 0;
            $manifest->pay = $request->payment_value_formess != null ? $request->payment_value_formess : 0;
            $manifest->type_pay = $request->payment_type;
            $manifest->save();

            $pickup = Pickup::where('id', $manifest->p_id)->first();
            $pickup->insighly = $request->insighly == 'on' ? 'yes' : '';
            $pickup->driver = $request->driver;
            $pickup->status = $status;
            $pickup->save();

            $history = new ManifestHistory;
            $history->topic = 'Pickup';
            $history->detail = $request->comment != '' ? $request->comment : 'เปลี่ยนสถานะจาก Create เป็น Pickup';
            $history->update_by = Auth::user()->id;
            $history->inv_id = $manifest->id;
            $history->save();
        } else if ($manifest->status == '2') {

            $status = 3; // update Pickkup to Calculate
            $manifest = Manifest::where('id', $id)->first();
            $manifest->user_update = Auth::user()->name;
            $manifest->status = $status;
            if ($request->has('payment_type')) {
                $manifest->price_pay = $request->payment_value != null ? $request->payment_value : 0;
                $manifest->pay = $request->payment_value_formess != null ? $request->payment_value_formess : 0;
                $manifest->type_pay = $request->payment_type;
            }


            $manifest->save();

            $box = ManifestDetail_box::where('inv_id', '=', $manifest->id)->where('status', '<', 10)->update(['status' => '2']);

            $pickup = Pickup::where('id', $manifest->p_id)->first();
            $pickup->status = $status;
            if ($request->has('so')) {
                $pickup->so = $request->so;
            }
            if ($request->has('idcard')) {
                $pickup->idcard_note = $request->idcard;
            }
            $pickup->save();

            $history = new ManifestHistory;
            $history->topic = 'Calculate';
            $history->detail = $request->comment != '' ? $request->comment : 'เปลี่ยนสถานะจาก Pickup เป็น Calculate ด้วยการกดปุ่ม Submit';
            $history->update_by = Auth::user()->id;
            $history->inv_id = $manifest->id;
            $history->save();

        } else if ($manifest->status == '3') {
            $shipment = DB::table('app_manifest_detail')
                ->where('inv_id', $id)
                ->get();
            $check = true;
            foreach ($shipment as $value) {
                if ($value->awb == null) {
                    $check = false;
                }
            }
            if (count($shipment) > 0 && $check) { //เช็คว่าพร้อมเปลี่ยนสถานะไปรอชำระเงินมั้ยโดยจับจากมายเลข awb
                $status = 4; // update Calculate to Wait For Payment
                $manifest = Manifest::where('id', $id)->first();
                $manifest->user_update = Auth::user()->name;
                $manifest->status = $status;
                $manifest->price_pay = $request->payment_value != null ? $request->payment_value : 0;
                $manifest->pay = $request->payment_value_formess != null ? $request->payment_value_formess : 0;
                $manifest->type_pay = $request->payment_type;
                $manifest->save();

                $pickup = Pickup::where('id', $manifest->p_id)->first();
                $pickup->insighly = $request->insighly == 'on' ? 'yes' : '';
                $pickup->status = $status;
                $pickup->save();
                //-------  ส่งอีเมล์แจ้งชำระค่าบริการ
                /*
                $service_type = DB::table('app_services_type')->where('id',$pickup->type_of_service)->first();
                $net_charge = ManifestDetail::where('inv_id',$id)->where('status','<',10)->sum('price');
                $service_charge_amount = $net_charge-$manifest->price_pay;
                $data = array(
                  'name' => $pickup->name,
                  'email' => $pickup->email,
                  'service_id' => $id,
                  'service_name' => $service_type->name,
                  'shipment' => $pickup->shipment,
                  'date' => date_format($pickup->created_at,"d/m/Y"),
                  'service_charge_total'=> $net_charge,
                  'service_charge_paid'=> $manifest->price_pay,
                  'service_charge_amount'=> $service_charge_amount,
                  'service_charge_discount'=>$manifest->discount
                );

                Mail::send('mails.payment', $data, function ($message) use ($data) {
                  $message->to($data['email']);
                  $message->from('SERVICE@SMESHIPPING.COM', 'SME SHIPPING');
                  $message->subject('แจ้งยอดค้างชำระค่าบริการ SME SHIPPING');
                });
                    $manifest->email_date    = date("d-m-Y h:i:sa");
                    $manifest->email_send    = 'Yes';

                //------ ส่ง SMS แจ้งชำระค่าบริการ
                  $tel = preg_replace('/0/', '66', $pickup->phone, 1);
                  $curl = curl_init();
                  curl_setopt_array($curl, array(
                  CURLOPT_URL => "http://api.ants.co.th/sms/1/text/single",
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS =>'{"from":"SMESHIPPING","to": "'.$tel.'","text":"ท่านมียอดค้างชำระ กรุณาชำระค่าบริการ '.number_format($service_charge_amount).' บาท ขออภัยหากชำระแล้ว"}' ,
                  CURLOPT_HTTPHEADER => array(
                  "Authorization: Basic U01FX1Byb2R1Y3Rpb246WEdlUTE1SnU=",
                  "Content-Type: application/json"
                  ),
                  ));

                  $response = curl_exec($curl);
                  $err = curl_error($curl);
                  curl_close($curl);
                  if ($err) {
                    $manifest->sms_date    = date("d-m-Y h:i:sa");
                    $manifest->sms_error    = "cURL Error #:" . $err;
                    $manifest->sms_send    = 'No';
                  } else {
                    $manifest->sms_date    = date("d-m-Y h:i:sa");
                    $manifest->sms_send    = 'Yes';
                  }
                  */
                $manifest->save();


                $history = new ManifestHistory;
                $history->topic = 'Wait For Payment';
                $history->detail = $request->comment != '' ? $request->comment : 'เปลี่ยนสถานะจาก Calculate เป็น Wait For Payment ด้วยการกดปุ่ม Submit';
                $history->update_by = Auth::user()->id;
                $history->inv_id = $manifest->id;
                $history->save();
            } else {
                $manifest = Manifest::where('id', $id)->first();
                $manifest->user_update = Auth::user()->name;
                $manifest->price_pay = $request->payment_value != null ? $request->payment_value : 0;
                $manifest->pay = $request->payment_value_formess != null ? $request->payment_value_formess : 0;
                $manifest->type_pay = $request->payment_type;
                $manifest->save();
                $pickup = Pickup::where('id', $manifest->p_id)->first();
                $pickup->insighly = $request->insighly == 'on' ? 'yes' : '';
                $pickup->save();
            }
        } else if ($manifest->status == '4') {
            $check = $request->payment_status;
            if ($check && $manifest->m_id != 9999999) { //เช็คว่าพร้อมเปลี่ยนสถานะไปรอชำระเงินมั้ยโดยจับจากมายเลข awb
                $status = 5; // update Wait For Payment to Wait For Release
                $manifest = Manifest::where('id', $id)->first();
                $manifest->user_update = Auth::user()->name;
                $manifest->status = $status;
                $manifest->price_pay = $request->payment_value != null ? $request->payment_value : 0;
                $manifest->pay = $request->payment_value_formess != null ? $request->payment_value_formess : 0;
                $manifest->type_pay = $request->payment_type;
                $manifest->save();

                $pickup = Pickup::where('id', $manifest->p_id)->first();
                $pickup->insighly = $request->insighly == 'on' ? 'yes' : '';
                $pickup->status = $status;
                $pickup->save();

                $history = new ManifestHistory;
                $history->topic = 'Wait For Release';
                $history->detail = $request->comment != '' ? $request->comment : 'เปลี่ยนสถานะจาก Wait For Payment เป็น Wait For Release ด้วยการกดปุ่ม Submit';
                $history->update_by = Auth::user()->id;
                $history->inv_id = $manifest->id;
                $history->save();

                $manifest_detail = DB::table('app_manifest_detail')->leftJoin('app_services', 'app_services.id', '=', 'app_manifest_detail.services')->select('app_manifest_detail.*', 'app_services.tracking')->where('inv_id', $manifest->id)->where('status', '<', 10)->get();

                $to = explode(',', $pickup->email);
                $trimmed_array = array_map('trim', $to);
                if ($pickup->email != null) {
                    $data = array(
                        'name' => $pickup->name,
                        'email' => $trimmed_array,
                        'detail' => $manifest_detail,
                        'url_check' => 'https://www.dhl.com/en/express/tracking.shtml?AWB=',
                        'date' => date_format(now(), "d/m/Y"));
                    Mail::send('mails.shipmentdetail', $data, function ($message) use ($data) {
                        $message->to($data['email']);
                        //$message->cc('service@smeshipping.com');
                        $message->from('notification-no-reply@smeshipping.com', 'SME SHIPPING');
                        $message->subject('หมายเลขติดตามการจัดส่งของท่าน ในวันที่ ' . $data['date']);
                    });
                }
                //------ ส่ง SMS แจ้งชำระค่าบริการ
                $tellist = explode(',', $pickup->phone);
                $tellist_array = array_map('trim', $tellist);
                $textforsms = "Your have " . count($manifest_detail) . " new shipment with SME SHIPPING\n";
                foreach ($manifest_detail as $key => $item) {
                    $textforsms = $textforsms . ($key + 1) . ". " . $item->tracking . " ";
                    $textforsms = $textforsms . $item->awb . " to " . $item->country . "\n";
                    /*$domain_data["fullName"] = "rebrand.ly";
                    $post_data["destination"] = "https://backend.smeshipping.com/tracking/".$item->awb;
                    $post_data["domain"] = $domain_data;
                    $ch = curl_init("https://api.rebrandly.com/v1/links");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        "apikey: 963f6f60dfa440159a842699a4a62ca2",
                        "Content-Type: application/json"
                    ));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $response = json_decode($result, true);
                    $textforsms = $textforsms.'http://'.$response["shortUrl"]."\n";*/
                    //สร้าง shortURL
                    $curl_shorturl = curl_init();
                    curl_setopt_array($curl_shorturl, array(
                        CURLOPT_URL => "https://is.gd/create.php?format=simple&url=" . "backend.smeshipping.com/tracking/" . $item->awb,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "Cookie: __cfduid=d5d0336382e8f80c883bc40ef0c597ace1585972753"
                        ),
                    ));
                    $res_shorturl = curl_exec($curl_shorturl);
                    curl_close($curl_shorturl);
                    if (strpos($res_shorturl, 'Error') === false) {
                        $textforsms = $textforsms . $res_shorturl . "\n";
                    } else {
                        $textforsms = $textforsms . "http://backend.smeshipping.com/tracking/" . $item->awb . "\n";
                    }

                }
                foreach ($tellist_array as $key => $value) {
                    if (strlen($value) == 10) { //เช็คว่าเบอร์ครบ 10 ตัวหรือไม่
                        $tel = preg_replace('/0/', '66', $value, 1);
                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "https://api-service.ants.co.th/sms/send",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => "{\"messages\":[{\"form\":\"SMESHIPPING\",\"destinations\":[{\"to\":\"${tel}\"}],\"text\":\"${$textforsms}\"}]}",
                            CURLOPT_HTTPHEADER => array(
                                "Content-Type: application/xml",
                                "Authorization: Basic U01FX1Byb2R1Y3Rpb246WEdlUTE1SnU="
                            ),
                        ));
                        $response = curl_exec($curl);
                        curl_close($curl);
                    }
                }
                //End send SMS
            } else {
                $manifest = Manifest::where('id', $id)->first();
                $manifest->user_update = Auth::user()->name;
                $manifest->price_pay = $request->payment_value != null ? $request->payment_value : 0;
                $manifest->pay = $request->payment_value_formess != null ? $request->payment_value_formess : 0;
                $manifest->type_pay = $request->payment_type;
                $manifest->save();
                $pickup = Pickup::where('id', $manifest->p_id)->first();
                $pickup->insighly = $request->insighly == 'on' ? 'yes' : '';
                $pickup->save();
            }
        } else if ($manifest->status == '5') {
            $status = 6; // update Release to Complete
            $manifest = Manifest::where('id', $id)->first();
            $manifest->user_update = Auth::user()->name;
            $manifest->status = $status;
            $manifest->save();

            $box = ManifestDetail_box::where('inv_id', '=', $manifest->id)->where('status', '<', 10)->update(['status' => '3']);

            $detail = ManifestDetail::where('inv_id', '=', $manifest->id)->where('status', '<', 10)->update(['status' => '2']);

            $pickup = Pickup::where('id', $manifest->p_id)->first();
            $pickup->status = $status;
            $pickup->save();

            $history = new ManifestHistory;
            $history->topic = 'Complete';
            $history->detail = $request->comment != '' ? $request->comment : 'เปลี่ยนสถานะจาก Wait for Release เป็น Complete ด้วยการกดปุ่ม Submit';
            $history->update_by = Auth::user()->id;
            $history->inv_id = $manifest->id;
            $history->save();
        }

        return $manifest->id;

    }

    public function destroy($id)
    {
        return "";
    }

    public function count_request()
    {
        $inbox = Manifest::where('status', 1)->count();
        $cal = Manifest::where('status', 3)->count();
        $sale = Manifest::where('status', 0)->count();
        $pickup = Manifest::where('status', 2)->count();
        $payment = Manifest::where('status', 4)->whereNull('app_manifest.payment_file_re')->count();
        $proof = Manifest::where('status', 4)->whereNotNull('app_manifest.payment_file_re')->count();
        $checkin = AppBoxCheckin::where('status', 1)->whereNull('del')->count();
        $release = Manifest::where('status', 5)->count();
        $cancel = Manifest::where('status', 99)->count();
        $all = Manifest::where('status', '<>', 0)->count();
        $hold = Manifest::where('status', 101)->orWhere('status', 102)->orWhere('status', 103)->orWhere('status', 104)->orWhere('status', 105)->count();
        $checklink = CustomerBookingNote::whereNull('del')->count();
        $count_payment_attach = PaymentAttachmentFromCustomer::whereNull('update_by')->count();
        return response()->json([
            'sale' => $sale,
            'inbox' => $inbox,
            'pickup' => $pickup,
            'hold' => $hold,
            'payment' => $payment,
            'release' => $release,
            'all' => $all,
            'calculate' => $cal,
            'proof' => $proof,
            'checkin' => $checkin,
            'checklink' => $checklink,
            'cancel' => $cancel,
            'count_payment_attach' => $count_payment_attach
        ]);
    }

    public function cal_num($input)
    {
        $result = $input - floor($input);
        if ($result < 0.5 && $result != 0) {
            $num_1 = 0.5 - $result;
            $output = floor($input) + 0.5;
        } else if ($result == 0.5) {
            $output = $input;
        } else if ($result > 0.5) {
            $output = ceil($input);
        } else if ($result == 0) {
            $output = $input;
        }
        return $output;
    }

    public function edit_delivery(Request $request, $id)
    {
        $manifest = Manifest::where('id', $id)->first();
        if ($manifest->status == 1 && $request->driver != '' && $request->type_delivery != 'walkin') {
            $manifest->status = 2;
        } else if ($request->type_delivery == 'walkin' && ($manifest->status == 1 || $manifest->status == 2)) {
            $manifest->status = 3;
        }
        $manifest->save();

        $pickup = Pickup::where('id', $manifest->p_id)->first();
        $pickup->type_work = $request->type_work;
        $pickup->type_of_service = $request->type_of_service;
        $pickup->type_delivery = $request->type_delivery;
        $pickup->type_delivery_note = $request->type_delivery_note;
        $pickup->delivery_date = strtotime(str_replace("/", "-", $request->delivery_date));
        $pickup->delivery_time = $request->time . " " . $request->delivery_time;
        $pickup->shipment = $request->shipment;
        $pickup->driver = $request->driver;
        $pickup->m_note = $request->m_note;
        $pickup->type_shipping = $request->type_shipping;
        if ($pickup->status == 1 && $request->driver != '') {
            $pickup->status = 2;
        }
        $pickup->save();

        $history = new ManifestHistory;
        $history->topic = 'Edit';
        $history->detail = $request->comment != '' ? $request->comment : 'แก้ไขข้อมูลการเข้ารับสินค้า';
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();
        return 'success';
    }

    public function edit_customer(Request $request, $id)
    {
        $customer = DB::table('app_customer')->where('id', $request->m_id)->first();

        $manifest = Manifest::where('id', $id)->first();
        $manifest->m_id = $request->m_id;
        $manifest->save();

        $pickup = Pickup::where('id', $manifest->p_id)->first();
        $pickup->m_id = $request->m_id;
        $pickup->insighly = $request->edit_insighly == 'on' ? 'yes' : '';
        /*$pickup->name       = $customer->firstname . " " . $customer->lastname;
        $pickup->company    = $customer->company;
        $pickup->phone  = $customer->phone;
        $pickup->email      = $customer->email;*/
        $pickup->address = $request->address;
        $pickup->sales = $request->sale;
        $pickup->save();

        $history = new ManifestHistory;
        $history->topic = 'Edit';
        $history->detail = $request->comment != '' ? $request->comment : 'แก้ไขข้อมูลลูกค้า';
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();
        return 'success';
    }

    public function update_hold($id, $remark = null)
    {
        $now = strtotime(date('Y-m-d H:i:s'));
        $manifest = Manifest::where('id', $id)->first();
        $manifest->status = $manifest->status > 100 ? $manifest->status - 100 : $manifest->status + 100;
        $manifest->save();

        $pickup = Pickup::where('id', $manifest->p_id)->first();
        $pickup->status = $pickup->status > 100 ? $pickup->status - 100 : $pickup->status + 100;
        $pickup->save();

        $history = new ManifestHistory;
        $history->topic = $manifest->status > 100 ? 'Hold' : 'Unhold';
        $history->detail = $remark != '' ? $remark : ($manifest->status > 100 ? 'กด Hold' : 'กด Unhold');
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();
        return 'success';
    }

    public function update_save($id, $remark = null, Request $request)
    {
        $now = strtotime(date('Y-m-d H:i:s'));
        $manifest = Manifest::where('id', $id)->first();
        $manifest->user_update = Auth::user()->name;
        $manifest->price_pay = $request->payment_value != null ? $request->payment_value : 0;
        $manifest->pay = $request->payment_value_formess != null ? $request->payment_value_formess : 0;
        $manifest->type_pay = $request->payment_type;
        $manifest->save();

        $pickup = Pickup::where('id', $manifest->p_id)->first();
        $pickup->insighly = $request->insighly == 'on' ? 'yes' : '';
        $pickup->save();

        $history = new ManifestHistory;
        $history->topic = 'Save';
        $history->detail = $remark != '' ? $remark : 'ไม่ได้ระบุเหตุผล';
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();
        return 'success';
    }

    public function update_complete($id, $remark = null, Request $request)
    {
        $now = strtotime(date('Y-m-d H:i:s'));
        $manifest = Manifest::where('id', $id)->first();
        $manifest->user_update = Auth::user()->name;
        $manifest->price_pay = $request->payment_value != null ? $request->payment_value : 0;
        $manifest->pay = $request->payment_value_formess != null ? $request->payment_value_formess : 0;
        $manifest->type_pay = $request->payment_type;
        $manifest->status = 6;
        $manifest->save();

        $pickup = Pickup::where('id', $manifest->p_id)->first();
        $pickup->insighly = $request->insighly == 'on' ? 'yes' : '';
        $pickup->save();

        $history = new ManifestHistory;
        $pickup->status = 6;
        $history->topic = 'Complete';
        $history->detail = $remark != '' ? $remark : 'กดปุ่ม Complete ไม่ได้ระบุเหตุผล';
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();
        return 'success';
    }

    public function update_cancel($id, $remark = null)
    {
        $now = strtotime(date('Y-m-d H:i:s'));
        $manifest = Manifest::where('id', $id)->first();
        $manifest->status = $manifest->status == 99 ? 1 : 99;
        $manifest->save();

        $pickup = Pickup::where('id', $manifest->p_id)->first();
        $pickup->status = $pickup->status == 99 ? 1 : 99;
        $pickup->save();

        $history = new ManifestHistory;
        $history->topic = $manifest->status == 99 ? 'Cancel' : 'Restore';
        $history->detail = $remark != '' ? $remark : ($manifest->status == 99 ? 'กดยกเลิกงาน' : 'กดนำงานกลับมาทำ');
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();
        return 'success';
    }

    public function update_cancel_mess(Request $request, $id)
    {
        $remark = $request->remark;
        $now = strtotime(date('Y-m-d H:i:s'));
        $manifest = Manifest::where('id', $id)->first();
        $manifest->status = $manifest->status == 99 ? 1 : 99;
        $manifest->save();

        $pickup = Pickup::where('id', $manifest->p_id)->first();
        $pickup->status = $pickup->status == 99 ? 1 : 99;
        $pickup->save();

        $history = new ManifestHistory;
        $history->topic = $manifest->status == 99 ? 'Cancel' : 'Restore';
        $history->detail = $remark != '' ? $remark : ($manifest->status == 99 ? 'กดยกเลิกงาน' : 'กดนำงานกลับมาทำ');
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();
        return back()->withInput();
    }

    public function update_change_mess(Request $request, $id)
    {
        $manifest = Manifest::where('id', $id)->first();
        $pickup = Pickup::where('id', $manifest->p_id)->first();
        $pickup->driver = $request->driver;
        $pickup->save();

        $new_driver = Employee::where('id', $request->driver)->first();

        $history = new ManifestHistory;
        $history->topic = 'Transfer';
        $history->detail = 'เปลี่ยน Messenger เป็น ' . $new_driver->name;
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();
        return back()->withInput();
    }

    public function update_shipment(Request $request, $id)
    {
        $remark = $request->comment;
        $manifest = Manifest::where('id', $id)->first();
        $manifest->price_total = $request->total_price;
        $manifest->save();
        if (isset($request->sh_type_of_service)) {
            $count_shipment = count($request->sh_type_of_service);
        } else {
            $count_shipment = 0;
        }


        //$tracking_code = DB::table('app_manifest_detail')->select('tracking_code')->orderBy('tracking_code', 'desc')->first();
        //$sme_code = substr($tracking_code->tracking_code,3,-2);
        $collection = collect();
        for ($x = 0; $x < $count_shipment; $x++) {
            $sme_code = self::smerandom(5);
            if ($request->sh_id[$x] == null || $request->sh_id[$x] == "") {
                $detail = new ManifestDetail;
                $detail->tracking_code = $sme_code;
            } else {
                $detail = ManifestDetail::where('id', $request->sh_id[$x])->first();
            }
            $detail->inv_id = $id;
            $detail->co = $request->sh_co[$x];

            $detail->shipper_company = $request->sh_s_company[$x];
            $detail->shipper_address1 = $request->sh_s_address1[$x];
            $detail->shipper_address2 = $request->sh_s_address2[$x];
            $detail->shipper_city = $request->sh_s_city[$x];
            $detail->shipper_postalcode = $request->sh_s_postalcode[$x];
            $detail->shipper_phone = $request->sh_s_phone[$x];

            $detail->m_id = $manifest->m_id;
            $detail->p_id = $manifest->p_id;
            $detail->insurance = $request->sh_insurance[$x];
            $detail->account_number = $request->sh_account_number[$x];
            $detail->so = $request->sh_so[$x];
            $detail->awb = $request->sh_awb[$x];
            $detail->recipient = $request->sh_recipient[$x];
            $detail->company = $request->sh_company[$x];
            $detail->services = $request->sh_type_of_service[$x];
            $detail->price = $request->total_regular_item[$x];
            $detail->country = $request->sh_country[$x];
            $detail->country_code = $request->sh_country_code[$x];
            $detail->currency = $request->sh_currency[$x];
            $detail->city = $request->sh_city[$x];
            $detail->suburb = $request->sh_suburb[$x];
            $detail->address = $request->sh_address1[$x];
            $detail->description = $request->sh_description[$x];
            $detail->zipcode = $request->sh_postcode[$x];
            $detail->note = $request->sh_note[$x];
            $detail->comment = $request->sh_note[$x];
            $detail->tracking_dhl = $request->sh_tracking_DHL[$x];
            $detail->tracking_local = $request->sh_tracking_local[$x];
            $detail->awb_express = '';
            $detail->air_mail = '';
            $detail->shipping_type = $request->sh_shipping_type[$x];
            $detail->declared_value = $request->sh_declared_value[$x];
            $detail->content_quantity = $request->sh_content_quantity[$x];
            $detail->packge_type = $request->sh_packge_type[$x];
            $detail->phone = $request->sh_phone[$x];
            $detail->email = $request->sh_email[$x];
            $detail->state = $request->sh_state[$x];
            $detail->address2 = $request->sh_address2[$x];
            $detail->address3 = $request->sh_address3[$x];
            $detail->weight = $request->total_weight_item[$x];
            $detail->status = 1; //1 = process; 2 = success; 3 = return;
            $detail->save();
            $collection->push($detail->id);
            $count_box = count($request->weight[$x]);
            ManifestDetail_box::where('shipment_id', $detail->id)->delete();
            for ($i = 0; $i < $count_box; $i++) {
                ManifestDetail_box::where('shipment_id', $detail->id)
                    ->where('series', $request->series[$x][$i])
                    ->delete();

                $box = new ManifestDetail_box;
                $box->shipment_id = $detail->id;
                $box->inv_id = $id;
                $box->weight = $request->weight[$x][$i];
                $box->width = $request->width[$x][$i];
                $box->length = $request->length[$x][$i];
                $box->height = $request->height[$x][$i];
                $box->series = $request->series[$x][$i];
                $box->save();


            }
            ManifestDetail_Other_Service::where('shipment_id', $detail->id)->delete();
            if (isset($request->sh_other_service_name[$x])) {
                $count_other = count($request->sh_other_service_name[$x]);
                for ($i = 0; $i < $count_other; $i++) {
                    $other = new ManifestDetail_Other_Service;
                    $other->shipment_id = $detail->id;
                    $other->other_service_name = $request->sh_other_service_name[$x][$i];
                    $other->other_service_value = (float)$request->sh_other_service_value[$x][$i];
                    $other->save();
                }
            }

        }

        $detail = ManifestDetail::where('inv_id', $id)->where('m_id', $manifest->m_id)->whereNotIn('id', $collection)->delete(); //
        $detail = ManifestDetail_box::where('inv_id', $id)->whereNotIn('shipment_id', $collection)->delete(); //

        $history = new ManifestHistory;
        $history->topic = 'Edit';
        $history->detail = $remark != '' ? $remark : 'ปรับปรุงรายละเอียด Shipment';
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();
        return dd($collection);
    }

    public function removeShipment(Request $request)
    {
        try {
            $collection = collect($request->removeShipmentId);
            $detail = ManifestDetail::whereIn('id', $collection)->delete();
            $detail = ManifestDetail_box::whereIn('shipment_id', $collection)->delete();
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return "Success";
    }

    public function addHistoryShipment($id)
    {
        try {
            $history = new ManifestHistory;
            $history->topic = 'Edit';
            $history->detail = 'ปรับปรุงรายละเอียด Shipment';
            $history->update_by = Auth::user()->id;
            $history->inv_id = $id;
            $history->save();
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return "Success";
    }

    public function smerandom($length = 6)
    {

        $possible = "1234567890"; //ตัวอักษรที่ต้องการสุ่ม
        $loop = true;
        while ($loop) {
            $str = "";
            $sum = 0;
            while (strlen($str) < $length) {
                $thisstr = substr($possible, (rand() % strlen($possible)), 1);
                $str .= $thisstr;
                $sum = $sum + $thisstr;
            }
            $str .= substr($sum, -1);
            $tracking_code = DB::table('app_manifest_detail')->where('tracking_code', $str)->count();
            if ($tracking_code == 0) {
                $loop = false;
            }
        }

        return $str;
    }

    public function discount(Request $request, $id, $discount)
    {
        $manifest = Manifest::where('id', $id)->first();
        $manifest->discount = $discount;
        $manifest->save();

        $history = new ManifestHistory;
        $history->topic = 'Discount';
        $history->detail = 'กำหนดส่วนลดเป็นเงิน ' . $discount . 'บาท';
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();


        if ($request->code_discount != null) {
            $discount_ = DB::table('discount_code')->where('discount_code', $request->code_discount)->first();
            $discount_us = DiscountUsed::where('manifest_id', $manifest->id)->first();
            if ($discount_ != null) {
                if ($discount_us != null) {
                    DiscountUsed::where('manifest_id', $manifest->id)->update([
                        'discount_id' => $discount_->id,
                        'discount_code' => $request->code_discount,
                        'customer_id' => $request->customer_id,
                        'customer_name' => $request->customer_name,
                        'amount' => $discount,
                    ]);
                } else {
                    $new_discount = new DiscountUsed;
                    $new_discount->discount_id = $discount_->id;
                    $new_discount->discount_code = $request->code_discount;
                    $new_discount->customer_id = $request->customer_id;
                    $new_discount->customer_name = $request->customer_name;
                    $new_discount->amount = $discount;
                    $new_discount->manifest_id = $manifest->id;
                    $new_discount->save();
                }
            }
        }
        return $manifest;
    }

    public function copy($id)
    {
        $manifest_ = DB::table('app_manifest')->where('id', $id)->first();
        $pickup_ = Pickup::where('id', $manifest_->p_id)->first();
        $manifest_detail_ = ManifestDetail::where('inv_id', $id)->get();


        $pickup = $pickup_->replicate();
        $pickup->save();


        $manifest = new Manifest;
        $manifest->m_id = $manifest_->m_id;
        $manifest->user_create = Auth::user()->id;
        $manifest->status = $manifest_->status;
        $manifest->price_pay = $manifest_->price_pay;
        $manifest->pay = $manifest_->pay;
        $manifest->type_pay = $manifest_->type_pay;
        $manifest->discount = $manifest_->discount;
        $manifest->p_id = $pickup->id;
        $manifest->save();

        $history = new ManifestHistory;
        $history->topic = 'Create';
        $history->detail = 'Booking.';
        $history->update_by = Auth::user()->id;
        $history->inv_id = $manifest->id;
        $history->save();

        foreach ($manifest_detail_ as $key => $value) {
            //$detail = new Manifestdetail;
            $detail = $value->replicate();
            $detail->p_id = $pickup->id;
            $detail->so = null;
            $detail->awb = null;
            $detail->invoice_file = null;
            $detail->awb_file = null;
            $detail->tracking_code = self::smerandom(5);
            $detail->inv_id = $manifest->id;
            $detail->save();
            $box_ = ManifestDetail_box::where('shipment_id', $value->id)->get();
            foreach ($box_ as $value_) {
                $box = $value_->replicate();
                $box->shipment_id = $detail->id;
                $box->inv_id = $manifest->id;
                $box->save();
            }
        }
        return redirect('/admin/request/' . $manifest->id);
    }

    public function show_detail($id)
    {
        //  if(!session()->has('cus.id')){
        //      return redirect('/app/login');
        //  }
        $mess = Employee::whereIn('position', ['Messenger', 'Supervisor Messenger'])->get();
        $data = DB::table('app_manifest')
            ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
            ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
            ->where('app_manifest.id', '=', $id)
            ->select('pickup_manifest.*', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'users.name as salename')
            ->first();
        $so = Pickup_SO::where('inv_id', $id)->get();
        if ($data->m_id != 9999999) {
            /* $customer = DB::table('app_customer')
                ->where('id', '=', $data->m_id)
                ->first();*/
            $customer = collect();
            $customer->firstname = $data->name;
            $customer->lastname = '';
            $customer->company = $data->company;
            $customer->line = $data->line;
            $customer->phone = $data->phone;
            $customer->email = $data->email;
        } else {
            $customer = collect();
            $customer->firstname = $data->name;
            $customer->lastname = '';
            $customer->company = $data->company;
            $customer->line = $data->line;
            $customer->phone = $data->phone;
            $customer->email = $data->email;
        }

        $shipment = DB::table('app_manifest_detail')
            ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
            ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
            ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.name as service_name',
                'app_services.cal_weight', 'app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight')
            ->where('app_manifest_detail.inv_id', '=', $data->manifestid)
            ->orderBy('app_manifest_detail.id')
            ->get();
        $box = DB::table('app_manifest_box')
            ->where('inv_id', '=', $data->manifestid)
            ->select('app_manifest_box.*', DB::raw('ROUND((width*length*height)/5000,2) as sum_weight'))
            ->orderBy('series')
            ->get();
        $service_type = DB::table('app_services_type')->where('id', $data->type_of_service)->get();
        $service_type_all = DB::table('app_services_type')->orderBy('code')->get();
        $service_all = DB::table('app_services')->whereNull('del')->where('active', '1')->orderBy('code')->get();
        $history = DB::table('manifest_history')
            ->leftJoin('users', 'users.id', '=', 'manifest_history.update_by')
            ->select('users.name', 'manifest_history.*')
            ->where('manifest_history.inv_id', $id)->orderBy('manifest_history.updated_at', 'desc')->get();
        $currency = DB::table('app_currency')->orderBy('code')->get();
        $country_all = Address::select('country_name', 'country_cd', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        if (count($service_type) == 0) {
            $service_type = $service_type_all;
        }
        $data->total_package = count($box); //TOTAL PACKAGE
        $data->total_shipment = count($shipment); //TOTAL SHIPMENT
        foreach ($shipment as $shipitem) {
            $other_service_name = collect();
            $other_service_price = collect();
            //Express
            //Check price is 0
            if ($shipitem->price == 0) {
                $shipitem->price = 100;
            }
            //Other Service
            $other_service_total_price = 0;
            $other_service_list = ManifestDetail_Other_Service::where('shipment_id', $shipitem->id)->get();

            foreach ($other_service_list as $key => $value) {
                $other_service_name->add($value->other_service_name);
                $other_service_price->add($value->other_service_value);
                $other_service_total_price = $other_service_total_price + $value->other_service_value;
            }
            $shipitem->other_service_name = $other_service_name;
            $shipitem->other_service_price = $other_service_price;
            $shipitem->other_service_total_price = $other_service_total_price;
        }
        return view('admin/request/show_history')->with(compact('data', 'mess', 'customer', 'shipment', 'box', 'service_type', 'history', 'service_all', 'service_type_all', 'currency', 'country_all', 'so'));
    }

}
