<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class AuthController extends Controller
{
//    public function register(Request $request)
//    {
//        $validatedData = $request->validate([
//            'name' => 'required|max:55',
//            'email' => 'email|required|unique:users',
//            'password' => 'required|confirmed'
//        ]);
//
//        $validatedData['password'] = bcrypt($request->password);
//
//        $user = User::create($validatedData);
//
//        $accessToken = $user->createToken('authToken')->accessToken;
//
//        return response([ 'user' => $user, 'access_token' => $accessToken]);
//    }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($loginData)) {
            return responder()->error('invalid_credentials', 'Your credentials is invalid.');
        }

        $accessToken = auth()->user()->createToken('authToken');

        return responder()->success([
            'user' => auth()->user(),
            'access_token' => $accessToken->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $accessToken->token->expires_at
            )->toDateTimeString()
        ]);

    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return responder()->success();
    }
}
