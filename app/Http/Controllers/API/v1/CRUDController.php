<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class CRUDController extends Controller
{
    protected $ins = null;

    /**
     * Display a listing of the resource.
     *
     * @return \Flugg\Responder\Http\Responses\ErrorResponseBuilder|\Illuminate\Http\JsonResponse
     */
    public function list(Request $request, $model)
    {
        $data = null;
        $modelName = app("App\\" . Str::studly($model));

        if ($request->method() == "POST") {
            $data = $modelName::filter()->paginate(20);
        } else if ($request->method() == "GET") {
            $data = $modelName::paginate(20);
        } else {
            return responder()->error('method_not_allow', 'Your request method is not allowed.');
        }

        return responder()->success($data)->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder|\Illuminate\Http\Response
     */
    public function create(Request $request, $model)
    {
        $data = null;
        $modelName = app("App\\" . Str::studly($model));

        $modelName::query()->create($request->all());

        return responder()->success(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Flugg\Responder\Http\Responses\ErrorResponseBuilder|\Illuminate\Http\JsonResponse
     */
    public function read(Request $request, $model, $id)
    {
        $data = null;
        $modelName = app("App\\" . Str::studly($model));

        if ($request->method() == "POST") {
            $data = $modelName::filter()->where('id', $id)->firstOrFail();
        } else if ($request->method() == "GET") {
            $data = $modelName::query()->where('id', $id)->firstOrFail();
        } else {
            return responder()->error('method_not_allow', 'Your request method is not allowed.');
        }

        return responder()->success($data)->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder|\Illuminate\Http\Response
     */
    public function update(Request $request, $model, $id)
    {
        $data = null;
        $modelName = app("App\\" . Str::studly($model));

        $modelName::query()->where('id', $id)->update($request->all());

        return responder()->success(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder|\Illuminate\Http\Response
     */
    public function delete(Request $request, $model, $id)
    {
        $data = null;
        $modelName = app("App\\" . Str::studly($model));

        $modelName::query()->where('id', $id)->first()->delete();

        return responder()->success(true);
    }
}
