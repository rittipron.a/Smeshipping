<?php

namespace App\Http\Controllers;
use App\Customer;
use App\ServiceZoneESC;
use Illuminate\Http\Request;
use App\ServiceZone;
use App\Country;
use App\DiscountCode;
use App\DiscountUsed;
use App\CountryZone;
use App\ServiceGroup;
use App\ServiceGroupMap;
use App\Service;
use App\ServiceDay;
use Illuminate\Support\Facades\DB;
class PriceController extends Controller
{
    public function getprice($service_id,$country_code,$weight){

        $country_id = Country::where('code',$country_code)->first();
        $country_zone = CountryZone::where('country',$country_id->id)->where('s_id',$service_id)->first();
        $service = ServiceZone::where('s_id',$service_id)->orderBy('weight','ASC')->where('weight','>=',$weight)->select('zone_'.$country_zone->zone .' as result')->first();

        return $service->result;
    }
    public function getprice_by_app($country_code,$weight){
        $cus = Customer::where('id',session('cus.id'))->first();
        $country_id = Country::where('code',$country_code)->first();

        if($cus != null){
        $service = ServiceZone::join('app_services', 'app_services.id', '=', 'app_services_zone.s_id')
        ->leftJoin('app_services_group_map', 'app_services.id', '=', 'app_services_group_map.service_id')
        ->orderBy('app_services_zone.weight','ASC')
        ->where('app_services_group_map.group_id',$cus->service_group)
        ->where('app_services.active', 1)
        ->whereNull('app_services.del')
        ->select('app_services.*','app_services.cat as price','app_services.cat as day',DB::raw("'100000' as filtersort"))
        ->where('app_services_zone.weight', '>=',$weight)
        ->distinct()
        ->get();
        }else{
            $service = ServiceZone::join('app_services', 'app_services.id', '=', 'app_services_zone.s_id')
            ->leftJoin('app_services_group_map', 'app_services.id', '=', 'app_services_group_map.service_id')
            ->orderBy('app_services_zone.weight','ASC')
            ->where('app_services_group_map.group_id',1)
            ->where('app_services.active', 1)
            ->whereNull('app_services.del')
            ->select('app_services.*','app_services.cat as price','app_services.cat as day',DB::raw("'100000' as filtersort"))
            ->where('app_services_zone.weight', '>=',$weight)
            ->distinct()
            ->get();
        }
        foreach ($service as $value) {
            $country_zone = CountryZone::where('country',$country_id->id)->where('s_id',$value->id)->first();
            if($country_zone!=null){
                $service_price = ServiceZone::where('s_id',$value->id)->orderBy('weight','ASC')->where('weight','>=',$weight)->select('zone_'.$country_zone->zone .' as result')->first();
                $day = ServiceDay::where('s_id',$value->id)->select('zone_'.$country_zone->zone .' as day')->first();
                $value->price = $service_price;
                $value->day = ($day!=null?$day:"");
                $value->filtersort = $service_price->result;
            }
        }
        return $service->sortBy('filtersort')->values()->toJson();
    }
    public function getprice_by_sale($country_code,$weight){
        $country_id = Country::where('code',$country_code)->first();

        $service = ServiceZone::join('app_services', 'app_services.id', '=', 'app_services_zone.s_id')
        ->leftJoin('app_services_group_map', 'app_services.id', '=', 'app_services_group_map.service_id')
        ->orderBy('app_services_zone.weight','ASC')
        ->where('app_services_group_map.group_id',1)
        ->where('app_services.active', 1)
        ->whereNull('app_services.del')
        ->select('app_services.*','app_services.cat as price','app_services.cat as day',DB::raw("'100000' as filtersort"))
        ->where('app_services_zone.weight', '>=',$weight)
        ->distinct()
        ->get();
        foreach ($service as $value) {
            $country_zone = CountryZone::where('country',$country_id->id)->where('s_id',$value->id)->first();
            if($country_zone!=null){
                $service_price = ServiceZone::where('s_id',$value->id)->orderBy('weight','ASC')->where('weight','>=',$weight)->select('zone_'.$country_zone->zone .' as result')->first();
                $service_price_esc = ServiceZoneESC::where('s_id',$value->id)->orderBy('weight','ASC')->where('weight','>=',$weight)->select('zone_'.$country_zone->zone .' as result')->first();
                $day = ServiceDay::where('s_id',$value->id)->select('zone_'.$country_zone->zone .' as day')->first();
                $value->price = $service_price;
                $value->price_esc = $service_price_esc;
                $value->day = $day!=null?$day:"";
                $value->filtersort = $service_price->result;
                //$value->zone = $country_zone;
                //$value->country = $country_id;
            }
        }

        return $service->sortBy('filtersort')->values()->toJson();
    }
    public function index()
    {
        return view('home');
    }

    public function getDiscount($code,$weight,$service_id,$country_code)
    {
        $data=DiscountCode::where('discount_code',$code)->first();
        $count=DiscountUsed::where('discount_code',$code)->count();
        //------ เช็คกลุ่มบริการ
        $serviceGroup = false; // ผ่าน เพราะไม่ได้เช็ค
        if($data)
        {
             if($data->focus_service_group_id!=null){ //เช็คว่าถ้ามีการกำหนดประเภท ให้เช็คก่อนว่าผ่านมั้ย
                $SG_array = array_map('trim',explode( ',', $data->focus_service_group_id));
                $service = ServiceGroupMap::whereIn('group_id',$SG_array)->whereIn('service_id',array_map('trim',explode( ',', $service_id)))->count();
                if($service>0){ //ถ้ามากกว่า 0 คือเข้าเงื่อนไข สามารถใช้ส่วนลดได้
                    $serviceGroup = false; // เช็คแล้ว ผ่าน
                }
                else{
                    $serviceGroup = true; // เช็คแล้ว ไม่ผ่าน
                }
            }
        }




        //------
        if($data){
            $now = date("d/m/Y");
            //$data->effective_date = date('Y-m-d',strtotime($data->effective_date));
            //$data->expired_date = date('Y-m-d',strtotime($data->expired_date));
            //echo $now.'+'.$data->effective_date.'+'.$data->expired_date;
            if($now<$data->effective_date){
                $status = 'False';
                $error = 'ส่วนลดนี้ยังไม่เปิดใช้งาน';
                $type = 'THB';
                $number = '0';
            }
            elseif($now>$data->expired_date){
                $status = 'False';
                $error = 'ส่วนลดนี้หมดอายุแล้ว';
                $type = 'THB';
                $number = '0';
            }
            elseif($count>$data->used_limit && $data->used_limit != null){
                $status = 'False';
                $error = 'ส่วนลดถูกใช้ครบตามจำนวนครั้งที่กำหนดแล้ว';
                $type = 'THB';
                $number = '0';
            }
            elseif($data->ignore_weight!=null&&$data->ignore_weight!=''&&$weight>$data->ignore_weight){
                $status = 'False';
                $error = 'น้ำหนักรวมของคุณ เกินกำหนดของส่วนลดนี้';
                $type = 'THB';
                $number = '0';
            }
            elseif($data->focus_country_code!=null&&count(array_diff(array_map('trim', explode( ',', $country_code)), array_map('trim', explode( ',', $data->focus_country_code)))) != 0){
                $status = 'False';
                $error = 'ประเทศปลายทาง อยู่นอกส่วนลดนี้';
                $type = 'THB';
                $number = '0';
            }
            elseif($serviceGroup){
                $status = 'False';
                $error = 'บริการที่ท่านเลือกไม่สามารถใช้ส่วนลดนี้ได้';
                $type = 'THB';
                $number = '0';

            }
            else{
                //อันนี้คือสามารถใช้งานได้ ไม่ติดปัญหาอะไรแล้ว
                $status = 'True';
                $error = Null;
                if($data->price>0){
                    $type = 'THB';
                    $number = $data->price;
                }
                else{
                    $type = 'Percent';
                    $number = $data->percent;
                }
            }

        }else{
            $status = 'False';
            $error = 'ไม่พบส่วนลดรหัส '.$code.' อยุ่ในระบบในขณะนี้';
            $type = 'THB';
            $number = '0';
        }
        return response()->json(['code' => $code,'status' => $status, 'error' => $error,'type' => $type,'number' => $number]);
    }

}
