<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use App\Manifest;
use App\ManifestDetail;
use App\Pickup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
  public function sendMessage($text = 'SME SHIPPING',$url='https://backend.smeshipping.com',$status = 'create') // แจ้งพนักงานเป็น Push Notification
    {
      $icon = "https://backend.smeshipping.com/public/assets/email/favicon.png";
      if($status == 'payment'){
        $icon = "https://backend.smeshipping.com/public/assets/email/payment.jpg";
      }
        $content      = array(
            "en" => $text
        );
        $hashes_array = array();
        array_push($hashes_array, array(
            "id" => "like-button",
            "text" => "View",
            "icon" => $icon,
            "url" => $url
        ));
        $fields = array(
            'app_id' => "bea7fe0f-eeb0-41bd-aafc-b17393792e40",
            'included_segments' => array(
                'All'
            ),
            'data' => array(
                "foo" => "bar"
            ),
            'contents' => $content,
            'web_buttons' => $hashes_array
        );
        
        $fields = json_encode($fields);
        //print("\nJSON sent:\n");
        //print($fields);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic Y2EzMTllMGUtODUyNi00ZDIxLWJjZTEtNGI4MzZkYTYzMGEz'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        $response = curl_exec($ch);
        curl_close($ch);
        
        //return $response;
    }
}