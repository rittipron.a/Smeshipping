<?php

namespace App\Http\Controllers\Customer;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\CustomerAddress;
use App\ManifestDetail_box;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mail;
use QrCode;
class UserController extends Controller
{
    public function create(Request $request)
    {
        $old_cus = Customer::where('email', $request->email)->count();
        if($old_cus==0&&$request->email==$request->reemail){
            $cus = new Customer;
            $cus->title = $request->title;
            $cus->email = $request->email;
            $cus->username = $request->email;
            $cus->password = Hash::make($request->password);
            $cus->company = $request->company;
            $cus->firstname = $request->firstname;
            $cus->type_customer = $request->type_customer;
            $cus->lastname = $request->lastname;
            $cus->phone = $request->phone;
            $cus->code = $request->code;
            $cus->save();
            session(['cus' => $cus]);
            return redirect('/app');
        }
        else if($request->email!=$request->reemail){
            return back()->withErrors(['คุณยืนยันอีเมลล์ไม่ถูกต้อง กรุณาลองใหม่อีกครั้งค่ะ']);
        }
        // return back()->withErrors(['อีเมล์ที่ท่านกรอกเป็นสมาชิกอยู่แล้ว \nโปรดโทรมาที่ 02 105 7777 เพื่อขอ Username และ Password \nโดยไม่ต้องสมัครสมาชิกใหม่ครับ']);
        return back()->withErrors(['อีเมล์ที่ท่านกรอกเป็นสมาชิกอยู่แล้วครับ']);
    }

    public function login(Request $request)
    {
        $result = 0;
        $cus = Customer::where('username',$request->email)->whereNull('del')->first();
        if($cus){
            if(Hash::check($request->password, $cus->password)){
                session(['cus' => $cus]);
                $result = 1;
            }
            else{
                return back()->withErrors(['Password ไม่ถูกต้อง']);
            }
        }
        else{
            return back()->withErrors(['Username ไม่ถูกต้อง']);
        }

        return redirect('app');
    }

    public function forgetpassword(Request $request)
    {
        $cus = Customer::where('email',$request->email)->whereNull('del')->first();
        if($cus){
            $userId = $cus->id;
            DB::table('auth_token')->where('user_id', '=', $userId)->delete();
            DB::table('auth_token')->insert(
                [
                    'token' => uniqid(),
                    'user_id' => $userId,
                    'expiry_date' => date("Y-m-d H:i:s", strtotime("+24 hours")),
                    'program' => 'forget password',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'active' => 1
                ]
            );
            $token = DB::table('auth_token')->where('user_id',$userId)->select('token')->first();

            $data = array(
                'url' => url('app/pass/'.$token->token),
                'email' => $request->email
            );
            Mail::send('mails.forgetpassword', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->from('notification-no-reply@smeshipping.com', 'SME SHIPPING');
                $message->subject('Forget Password');
            });
            return back()->withErrors(['ลิงก์เข้าสู่ระบบได้ถูกจัดส่งให้คุณผ่านทางอีเมลเรียบร้อยแล้ว กรุณาตรวจสอบอีเมลของท่าน']);
        }
        else{
            return back()->withErrors(['E-Mail ที่ท่านกรอก ('.$request->email.') ไม่มีอยู่ในระบบ ']);
        }

        return back()->withErrors(['E-Mail ที่ท่านกรอก ('.$request->email.') ไม่มีอยู่ในระบบ ']);
    }
    public function home()
    {
        if(!session()->has('cus.id')){
            return redirect('/app/login');
        }

        $data = DB::table('shipment_temp')->where('m_id',session('cus.id'))->first();

        $checkpickup = CustomerAddress::where('customer_id',session('cus.id'))->where('pickup',1)->count();

        return view('customer/app/index',compact('data','checkpickup'));
    }

    public function booking()
    {
        if(!session()->has('cus.id')){
            return redirect('/app/login');
        }
        return view('customer/app/booking');
    }

    public function profile()
    {
        if(!session()->has('cus.id')){
            return redirect('/app/login');
        }
        $customer = DB::table('app_customer')
        ->where('app_customer.id', session('cus.id'))
        ->select('app_customer.*','app_customer.del as del_cus','app_customer.id')
        ->first();

        $address = DB::table('customer_address')
        ->where('customer_address.customer_id', session('cus.id'))
        ->select('customer_address.*')
        ->get();
        return view('customer/app/profile',compact('customer','address'));
    }

    public function updateprofile(Request $request){
        $old_cus = Customer::where('email', $request->email)->whereNull('del')->whereNotIn('id', [session('cus.id')])->count();
        if ($old_cus == 0) {
            $customer                  = Customer::where('id', session('cus.id'))->first();
            $customer->title           = $request->title;
            $customer->status          = $request->status;
            $customer->firstname       = $request->firstname;
            $customer->lastname        = $request->lastname;
            $customer->company         = $request->company;
            $customer->type            = $request->type;
            $customer->type_customer   = $request->type_customer;
            $customer->phone           = $request->phone;
            $customer->email           = $request->email;
            $customer->line            = $request->line;
            $customer->credit_term     = $request->credit_term;
            $customer->pickup_fee      = $request->pickup_fee;
            $customer->note            = $request->note;
            $customer->code            = $request->code;
            $customer->save();
            if ($request->has('invoice')) {
                $invoice = CustomerAddress::where('id',$request->invoice)->first();
                $update_invoice = CustomerAddress::where('customer_id',session('cus.id'))->update(['invoice' => 0]);
                $invoice = CustomerAddress::where('id',$request->invoice)->first();
                $invoice->invoice = 1;
                $invoice->save();
                }
            if ($request->has('pickup')) {
                $pickup = CustomerAddress::where('id',$request->pickup)->first();
                $update_pickup = CustomerAddress::where('customer_id',session('cus.id'))->update(['pickup' => 0]);
                $pickup = CustomerAddress::where('id',$request->pickup)->first();
                $pickup->pickup = 1;
                $pickup->save();
                }
            return back()->withInput();
        }
        return back()->withErrors([session('This email address is already registered.')]);
    }

    public function setpassword(Request $request){
        $old_cus = Customer::where('email', $request->email)->whereNull('del')->whereNotIn('id', [session('cus.id')])->count();
        if ($old_cus == 0) {
            $customer                  = Customer::where('id', session('cus.id'))->first();
            if($request->password!='******'){$customer->password = Hash::make($request->password);}
            $customer->save();
            // return back()->withInput();
        }
        dd($old_cus);
        // return back()->withErrors([session('This email address is already registered.')]);
    }


    public function add_address(Request $request){
        $address = new CustomerAddress;
        $address->customer_name = $request->name;
        $address->customer_id = session('cus.id');
        $address->phone = $request->phone;
        $address->address = $request->address;
        $address->tumbon = $request->tumbon;
        $address->amphur = $request->amphur;
        $address->province = $request->province;
        $address->postcode = $request->postcode;
        if($request->set_address_is=='pickup'){
            $update_pickup = CustomerAddress::where('customer_id',session('cus.id'))->update(['pickup' => 0]);
            $address->pickup = 1;
        }
        if($request->set_address_is=='invoice'){
            $update_invoice = CustomerAddress::where('customer_id',session('cus.id'))->update(['invoice' => 0]);
            $address->invoice = 1;
        }
        $address->save();

        return back();
    }
    public function set_address($id,$action){
        if ($action == 'invoice') {
            $invoice = CustomerAddress::where('id',$id)->first();
            $update_invoice = CustomerAddress::where('customer_id',session('cus.id'))->update(['invoice' => 0]);
            $invoice = CustomerAddress::where('id',$id)->first();
            $invoice->invoice = 1;
            $invoice->save();
        }
        if ($action == 'pickup') {
            $pickup = CustomerAddress::where('id',$id)->first();
            $update_pickup = CustomerAddress::where('customer_id',session('cus.id'))->update(['pickup' => 0]);
            $pickup = CustomerAddress::where('id',$id)->first();
            $pickup->pickup = 1;
            $pickup->save();
        }
        return back();
    }

    public function genQRcode(){
        $userId=session('cus.id');
        //DB::table('auth_token')->where('user_id', '=', $userId)->delete();
        DB::table('auth_token')->insert(
            [
                'token' => uniqid(),
                'user_id' => $userId,
                'expiry_date' => date("Y-m-d H:i:s", strtotime("+1 hours")),
                'program' => 'Dealer',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'active' => 1
            ]
        );
        $token = DB::table('auth_token')->where('user_id',$userId)->select('token')->first();
        //return QrCode::size(250)->backgroundColor(255,255,204)->generate('MyNotePaper');
        return view('customer/app/genqrcode',compact('token'));
    }

    public function pass($token){
        $getdata = DB::table('auth_token')->where('token', $token)->orderBy('created_at')->first();
        $cus = Customer::where('id',$getdata->user_id)->first();
        if($cus){
            if($getdata->expiry_date >= date("Y-m-d H:i:s") && $getdata->active==1){
                session(['cus' => $cus]);
                return redirect('/app/profile');
                //return dd(session('cus'));
            }
        }
        return "<div style='text-align:center'><br/><br/><img src='https://miro.medium.com/max/300/1*JY-JZfN8GW_OsJoVrI7wBg.png' width='120'><br/><br/>QR-Code ที่คุณได้รับมาหมดอายุแล้ว กรุณาติดต่อที่พนักงานของร้านค่ะ <br/> This QR-Code is expired,
        Please contact the staff.</div>";
    }

    public function add_customer_address(Request $request){  //update address customer
        if($request->address_id!=''){
          $address = CustomerAddress::where('id',$request->address_id)->first();
        }else{
          $address = new CustomerAddress;
        }
         $address->customer_name = $request->name;
         $address->email = $request->email;
         $address->customer_id = session('cus.id');
         $address->phone = $request->phone;
         $address->company = $request->company;
         $address->card_id = $request->card_id;
         $address->address = $request->address;
         $address->tumbon = $request->tumbon;
         $address->amphur = $request->amphur;
         $address->province = $request->province;
         $address->postcode = $request->postcode;
         $address->update_by = 'C';
         $address->save();
         $address_count = CustomerAddress::where('customer_id', session('cus.id'))->count();

         $customer = Customer::find(session('cus.id'));
         if($request->pickup == 'on'){
         $customer->addresses()->update(['pickup' => false]);
         $customer->addresses()->find($request->address_id)->update(['pickup' => true]);
         }else{
         $customer->addresses()->find($request->address_id)->update(['pickup' => false]);
         }
         if($request->invoice == 'on'){
         $customer->addresses()->update(['invoice' => false]);
         $customer->addresses()->find($request->address_id)->update(['invoice' => true]);
         }else{
         $customer->addresses()->find($request->address_id)->update(['invoice' => false]);
         }
         $get_address = DB::table('customer_address')
         ->where('customer_address.id', $address->id)
         ->select('customer_address.*')
         ->get();
         return $get_address->toJson();
       }

       public function del_customer_address($id){
        $address = CustomerAddress::where('id',$id)->delete();
        return $address;
       }

       public function check_session_member(){
        if(!session()->has('cus.id')){
            return 'false';
        }
        return 'true';
       }
}
