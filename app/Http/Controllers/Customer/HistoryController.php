<?php

namespace App\Http\Controllers\Customer;

use App\Customer;
use App\Manifest;
use App\Pickup_SO;
use App\Employee;
use App\Address;
use App\Country;
use App\ManifestDetail_Other_Service;
use Illuminate\Support\Facades\Session;
use App\ManifestDetail_box;
use App\Http\Controllers\Controller;
use App\CustomerAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;
use Image;

class HistoryController extends Controller
{
    public function create(Request $request)
    {
        return back()->withErrors(['This email address is already registered.']);
    }

    
    public function list()
    { 
        if(!session()->has('cus.id')){
            return redirect('/app/login');
        }
        /*$data = DB::table('app_manifest')
        ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
        ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
        ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
        ->Where('app_manifest.m_id', '=', session('cus.id'))
        ->select('pickup_manifest.*','pickup_manifest.id as pickup_code', 'app_manifest.*',
         'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename')
        ->orderBy('app_manifest.id', 'desc')
        ->paginate(10);
        */
        $data  = DB::table('app_manifest_detail')
        ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
        ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
        ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.name as service_name',
            'app_services.cal_weight','app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight')
        ->where('app_manifest_detail.m_id', '=', session('cus.id'))
        ->orderBy('app_manifest_detail.id','desc')
        ->paginate(10);
        
        return view('customer/app/history_list',compact('data'));
        //return $data->toJson();
    }

    public function detail($id)
    { 
        if(!session()->has('cus.id')){
            return redirect('/app/login');
        }
        $mess = Employee::whereIn('position', ['Messenger','Supervisor Messenger'])->get();
        $data = DB::table('app_manifest')
        ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
        ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
        ->where('app_manifest.id', '=', $id)
        ->select('pickup_manifest.*', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status' ,'users.name as salename')
        ->first();
        $so = Pickup_SO::where('inv_id',$id)->get();
        if($data->m_id!=9999999){
        /* $customer = DB::table('app_customer')
            ->where('id', '=', $data->m_id)
            ->first();*/
            $customer = collect();
            $customer->firstname = $data->name;
            $customer->lastname = '';
            $customer->company = $data->company;
            $customer->line =  $data->line;
            $customer->phone = $data->phone;
            $customer->email = $data->email;
        }
        else{
            $customer = collect();
            $customer->firstname = $data->name;
            $customer->lastname = '';
            $customer->company = $data->company;
            $customer->line =  $data->line;
            $customer->phone = $data->phone;
            $customer->email = $data->email;
        }
        
        $shipment = DB::table('app_manifest_detail')
        ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
        ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
        ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.description as service_name',
            'app_services.cal_weight','app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight')
        ->where('app_manifest_detail.inv_id', '=', $data->manifestid)
        ->orderBy('app_manifest_detail.id')
        ->get();
        $box = DB::table('app_manifest_box')
        ->where('inv_id', '=', $data->manifestid)
        ->select('app_manifest_box.*', DB::raw('ROUND((width*length*height)/5000,2) as sum_weight'))
        ->orderBy('series')
        ->get();
        $service_type     = DB::table('app_services_type')->where('id', $data->type_of_service)->get();
        $service_type_all = DB::table('app_services_type')->orderBy('code')->get();
        $service_all = DB::table('app_services')->whereNull('del')->where('active','1')->orderBy('code')->get();
        $history          = DB::table('manifest_history')
        ->leftJoin('users', 'users.id', '=', 'manifest_history.update_by')
        ->select('users.name','manifest_history.*')
        ->where('manifest_history.inv_id', $id)->orderBy('manifest_history.updated_at', 'desc')->get();
        $currency = DB::table('app_currency')->orderBy('code')->get();
        $country_all = Address::select('country_name','country_cd','country_cd')
        ->orderBy('country_name')
        ->distinct()
        ->get();
        if(count($service_type)==0){
            $service_type =$service_type_all; 
        }
        $data->total_package  = count($box); //TOTAL PACKAGE
        $data->total_shipment = count($shipment); //TOTAL SHIPMENT
        foreach ($shipment as $shipitem) {
            $other_service_name  = collect();
            $other_service_price = collect();
            //Express
            //Check price is 0
            if ($shipitem->price == 0) {
                $shipitem->price = 100;
            }
            //Other Service
            $other_service_total_price = 0;
            $other_service_list = ManifestDetail_Other_Service::where('shipment_id',$shipitem->id)->get();

            foreach ($other_service_list  as $key => $value) {
                $other_service_name->add($value->other_service_name);
                $other_service_price->add($value->other_service_value);
                $other_service_total_price = $other_service_total_price + $value->other_service_value;
            }
            $shipitem->other_service_name        = $other_service_name;
            $shipitem->other_service_price       = $other_service_price;
            $shipitem->other_service_total_price = $other_service_total_price;
        } 
    return view('customer/app/history')->with(compact('data','mess','customer','shipment','box','service_type','history','service_all','service_type_all','currency','country_all','so'));
    }
    
    public function detil($id)
    { 
        if(!session()->has('cus.id')){
            return redirect('/app/login');
        }
        $mess = Employee::whereIn('position', ['Messenger','Supervisor Messenger'])->get();
        $data = DB::table('app_manifest')
        ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
        ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
        ->where('app_manifest.id', '=', $id)
        ->select('pickup_manifest.*', 'app_manifest.*', 'app_manifest.id as manifestid', 'app_manifest.status as request_status' ,'users.name as salename')
        ->first();
        $so = Pickup_SO::where('inv_id',$id)->get();
        if($data->m_id!=9999999){
        /* $customer = DB::table('app_customer')
            ->where('id', '=', $data->m_id)
            ->first();*/
            $customer = collect();
            $customer->firstname = $data->name;
            $customer->lastname = '';
            $customer->company = $data->company;
            $customer->line =  $data->line;
            $customer->phone = $data->phone;
            $customer->email = $data->email;
        }
        else{
            $customer = collect();
            $customer->firstname = $data->name;
            $customer->lastname = '';
            $customer->company = $data->company;
            $customer->line =  $data->line;
            $customer->phone = $data->phone;
            $customer->email = $data->email;
        }
        
        $shipment = DB::table('app_manifest_detail')
        ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
        ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
        ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.name as service_name',
            'app_services.cal_weight','app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight')
        ->where('app_manifest_detail.inv_id', '=', $data->manifestid)
        ->orderBy('app_manifest_detail.id')
        ->get();
        $box = DB::table('app_manifest_box')
        ->where('inv_id', '=', $data->manifestid)
        ->select('app_manifest_box.*', DB::raw('ROUND((width*length*height)/5000,2) as sum_weight'))
        ->orderBy('series')
        ->get();
        $service_type     = DB::table('app_services_type')->where('id', $data->type_of_service)->get();
        $service_type_all = DB::table('app_services_type')->orderBy('code')->get();
        $service_all = DB::table('app_services')->whereNull('del')->where('active','1')->orderBy('code')->get();
        $history          = DB::table('manifest_history')
        ->leftJoin('users', 'users.id', '=', 'manifest_history.update_by')
        ->select('users.name','manifest_history.*')
        ->where('manifest_history.inv_id', $id)->orderBy('manifest_history.updated_at', 'desc')->get();
        $currency = DB::table('app_currency')->orderBy('code')->get();
        $country_all = Address::select('country_name','country_cd','country_cd')
        ->orderBy('country_name')
        ->distinct()
        ->get();
        if(count($service_type)==0){
            $service_type =$service_type_all; 
        }
        $data->total_package  = count($box); //TOTAL PACKAGE
        $data->total_shipment = count($shipment); //TOTAL SHIPMENT
        foreach ($shipment as $shipitem) {
            $other_service_name  = collect();
            $other_service_price = collect();
            //Express
            //Check price is 0
            if ($shipitem->price == 0) {
                $shipitem->price = 100;
            }
            //Other Service
            $other_service_total_price = 0;
            $other_service_list = ManifestDetail_Other_Service::where('shipment_id',$shipitem->id)->get();

            foreach ($other_service_list  as $key => $value) {
                $other_service_name->add($value->other_service_name);
                $other_service_price->add($value->other_service_value);
                $other_service_total_price = $other_service_total_price + $value->other_service_value;
            }
            $shipitem->other_service_name        = $other_service_name;
            $shipitem->other_service_price       = $other_service_price;
            $shipitem->other_service_total_price = $other_service_total_price;
        } 
    return view('customer/app/bookingdetail')->with(compact('data','mess','customer','shipment','box','service_type','history','service_all','service_type_all','currency','country_all','so'));
    }
    public function tracking($code){
        $shipment = DB::table('app_manifest_detail')
        ->leftJoin('app_manifest', 'app_manifest.id', '=', 'app_manifest_detail.inv_id')
        ->leftJoin('app_services', 'app_services.id', '=', 'app_manifest_detail.services')
        ->where('app_manifest_detail.awb', '=', $code)
        ->orWhere('app_manifest_detail.so', '=', $code)
        ->orWhere('app_manifest_detail.tracking_dhl', '=', $code)
        ->select('app_manifest_detail.awb','app_manifest_detail.so','app_manifest_detail.tracking_dhl','app_manifest.status','app_services.tracking','app_manifest_detail.recipient','app_manifest_detail.country','app_manifest.created_at','app_manifest.id')
        ->first();
        $history = DB::table('manifest_history')->where('inv_id',$shipment->id)->whereIn('topic',['Create','Calculate','Complete'])->get();
        $shipment->history = $history;
        $shipment->Date = date('Y-m-d H:i:s');
        $tracking = collect();
        $tracking->push($shipment);
        if($shipment->tracking=='DHL'){
                $url            = "https://xmlpi-ea.dhl.com/XMLShippingServlet";//"https://xmlpitest-ea.dhl.com/XMLShippingServlet";
                $site_id        = "v62_MiV8RRMg6e"; //'v62_zFgdyqm6PS';
                $password       = "OqevK2V9I1";//'kGYhSsjb4Q';
                error_reporting(0);
                date_default_timezone_set("Asia/Bangkok");
                header('Content-Type: text/html; charset=utf-8');
                $xml = '<?xml version="1.0" encoding="UTF-8"?>
                <req:KnownTrackingRequest xmlns:req="http://www.dhl.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com TrackingRequestKnown.xsd" schemaVersion="1.0">
                    <Request>
                        <ServiceHeader>
                        <MessageTime>'. date('Y-m-d') . 'T' . date('H:i:s') .'</MessageTime>
                        <MessageReference>TrackingRequest_Single_AWB__</MessageReference>
                        <SiteID>'.$site_id.'</SiteID>
                        <Password>'.$password.'</Password>
                        </ServiceHeader>
                    </Request>
                    <LanguageCode>en</LanguageCode>
                    <AWBNumber>'.$shipment->awb.'</AWBNumber>
                    <LevelOfDetails>ALL_CHECK_POINTS</LevelOfDetails>
                </req:KnownTrackingRequest>';
            
                $headers = array(
                    "Content-type: text/xml",
                    "Content-length: " . strlen($xml),
                    "Connection: close",
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_MUTE, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            
                $data       = curl_exec($ch);
                //libxml_use_internal_errors(true);
                $data = iconv('UTF-8', 'UTF-8//IGNORE', $data);
                $array_data = json_decode(json_encode(simplexml_load_string($data)), true);
                //return dd($array_data);
                $count_data = count($array_data["AWBInfo"]);
                    if($count_data==3){
                        $json = $array_data["AWBInfo"]["ShipmentInfo"]["ShipmentEvent"];
                    }else{
                        $json = $array_data["AWBInfo"][0]["ShipmentInfo"]["ShipmentEvent"];
                    }
                    
                    if($json[0]["Date"]==null){
                        if($value["Date"]!=null){
                            $value = $json;
                            $thisdata = null;
                            $thisdata->Date = $value["Date"];
                            $thisdata->Time = $value["Time"];
                            $thisdata->ServiceEvent = $value["ServiceEvent"]["Description"]." ".($value["Signatory"]!=null?$value["Signatory"]:'');
                            $thisdata->ServiceArea = $value["ServiceArea"]["Description"];
                            $tracking->push($thisdata);
                        }
                        
                    }else{
                        foreach ($json as $value) {
                            $thisdata = null;
                            $thisdata->Date = $value["Date"];
                            $thisdata->Time = $value["Time"];
                            $thisdata->ServiceEvent = $value["ServiceEvent"]["Description"]." ".($value["Signatory"]!=null?$value["Signatory"]:'');
                            $thisdata->ServiceArea = $value["ServiceArea"]["Description"];
                            $tracking->push($thisdata);
                        } 
                }
                
        } else if($shipment->tracking=='GMB'){
            $shipment->awb = $shipment->tracking_dhl;
            $clientId = "MTM0Njg4MTA5Nw==";
            $clientps = "MjAzMDI5MTU";
            $SoldToAcct = "5249536745";
            $PickupAcct = "5249536745";
            $prefix = 'THBQP';
            $api_url = "https://api.dhlecommerce.dhl.com";
            
            $curl_token = curl_init();

            curl_setopt_array($curl_token, array(
            CURLOPT_URL => $api_url."/rest/v1/OAuth/AccessToken?clientId=".$clientId."&password=".$clientps."&returnFormat=json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            ));
            
            $response_token = curl_exec($curl_token);
            curl_close($curl_token);
            $data_token = iconv('UTF-8', 'UTF-8//IGNORE', $response_token);
            $array_data = json_decode($data_token, true);
            
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => $api_url."/rest/v3/Tracking",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>"{\r\n
                    \"trackItemRequest\": {\r\n
                    \"hdr\": {\r\n
                    \"messageType\": \"TRACKITEM\",\r\n
                    \"accessToken\": \"".$array_data["accessTokenResponse"]["token"]."\",\r\n
                    \"messageDateTime\": \"" . date('Y-m-d') . 'T' . date('H:i:s') . "\",\r\n
                    \"messageVersion\": \"1.0\",\r\n
                    \"messageLanguage\": \"en\"\r\n
                    },\r\n
                    \"bd\": {\r\n
                    \"customerAccountId\": null,\r\n
                    \"soldToAccountId\": \"".$SoldToAcct."\",\r\n
                    \"pickupAccountId\": \"".$PickupAcct."\",\r\n
                    \t\t\t\"ePODRequired\": \"Y\", \r\n
                    \"trackingReferenceNumber\": [\r\n
                    \"".$shipment->tracking_dhl."\"\r\n
                    ]\r\n        }\r\n    }\r\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $data = iconv('UTF-8', 'UTF-8//IGNORE', $response);
            $array = json_decode($data, true);
            if (!empty($array["trackItemResponse"]["bd"]["shipmentItems"][0]["events"])) {
                $json = $array["trackItemResponse"]["bd"]["shipmentItems"][0]["events"];
                $json = array_sort($json, 'dateTime', SORT_ASC);
                foreach ($json as $value) {
                    $country = Country::where('code',$value["address"]["country"])->select('country')->first();
                    if($value["address"]["country"]=='TH'&&$country==null){
                        $country = new \stdClass();
                        $country->country = 'Thailand';
                    }
                    $thisdata = new \stdClass();
                    $thisdata->Date = $value["dateTime"];
                    $thisdata->Time = $value["dateTime"];
                    $thisdata->ServiceEvent = $value["description"];
                    $thisdata->ServiceArea = $country!=null?$country->country:$value["address"]["country"];
                    $tracking->push($thisdata);
                }
            }
            
        }
        
      return $tracking->ToJson();
     }
     public function trackingUI($code=null){
        return view('customer/app/tracking',compact('code'));
     }
     public function getawb($code=null){
        header("Access-Control-Allow-Origin: *");
         $value = $code;
        $shipment = DB::table('app_manifest_detail')
        ->select('app_manifest_detail.awb')
        ->Where(function ($query) use ($value) {
            $query->Where('app_manifest_detail.awb', $value )
             ->orWhere('app_manifest_detail.so', $value);
           })
        ->orderBy('app_manifest_detail.updated_at')
        ->first();
        return $shipment->awb;
     }
     public function detilbytoken($id,$token)
     {
       if(!session()->has('cus.id')){
          Session::forget('cus'); 
       }
       $getdata = DB::table('auth_token')->where('token', $token)->orderBy('created_at')->first();
       $cus = Customer::where('id',$getdata->user_id)->first();
       if($cus){
          if($getdata->expiry_date >= date("Y-m-d H:i:s") && $getdata->active==1){
             session(['cus' => $cus]);
          }
       } 
       return redirect('/app/historydetail/'.$id);
     }
}
