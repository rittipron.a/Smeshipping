<?php

namespace App\Http\Controllers\Customer;

use App\AppBoxCheckin;
use App\Classes\API\ThaiPost;
use App\Customer;
use App\DiscountUsed;
use App\CustomerBookingNote;
use App\Http\Controllers\Controller;
use App\ThaiPostEMSNumber;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\CustomerAddress;
use App\Address;
use App\Pickup;
use App\Manifest;
use App\ServiceZone;
use App\Country;
use App\Attachment_Payment;
use App\CountryZone;
use App\ManifestHistory;
use App\ManifestDetail;
use App\ManifestDetail_box;
use App\ManifestDetail_Other_Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    public function create(Request $request)
    {
        DB::table('shipment_temp')->where('m_id', session('cus.id'))->delete();
        $request->m_id = session('cus.id');
        $status = 0; // ตั้งค่าเริ่มต้นเป็น 0 Incomming
        $count_shipment = count($request->sh_type_of_service);
        $customer = DB::table('app_customer')->where('id', session('cus.id'))->first();
        $discount = DB::table('discount_code')->where('discount_code', $request->discount_code)->first();

        $addressold = DB::table('customer_address')
            ->where('customer_address.customer_id', $request->m_id)
            ->where('customer_address.pickup', 1)
            ->select('customer_address.*')
            ->first();

        //$update_pickup = CustomerAddress::where('customer_id',session('cus.id'))->update(['pickup' => 0]);

        if ($addressold == null) {

            $address_ = new CustomerAddress;
            $address_->customer_name = $request->name;
            $address_->customer_id = session('cus.id');
            $address_->phone = $request->phone;
            $address_->address = $request->address;
            $address_->tumbon = $request->tumbon;
            $address_->amphur = $request->amphur;
            $address_->province = $request->province;
            $address_->postcode = $request->postcode;
            $address_->pickup = 1;
            $address_->save();
        }

        if ($request->type_delivery == 'walkin' || $request->type_delivery == 'ส่งพัสดุมาที่ SME SHIPPING' || $request->type_delivery == 'Drop off ไปรษณีย์ไทย') {
            $request->delivery_time = '';

        } else {
            $request->delivery_time = ' เวลา' . $request->delivery_time;
        }

        $service_db = $address = DB::table('app_services')->where('id', $request->sh_type_of_service[0])->first();
        $pickup = new Pickup;
        $pickup->insighly = 'on';
        $pickup->type_work = 1;
        $pickup->type_of_service = $service_db->cat;
        $pickup->type_delivery = $request->type_delivery;
        //$pickup->type_delivery_note = $request->type_delivery_note;
        $pickup->delivery_date = strtotime(str_replace("/", "-", $request->delivery_date));
        $pickup->delivery_time = $request->time . " " . $request->delivery_time;
        $pickup->shipment = $count_shipment;
        //$pickup->driver             = $request->driver;
        $pickup->m_note = $request->m_note;
        $pickup->type_shipping = 'ดูที่รายละเอียดแต่ละ Shipment';
        $pickup->m_id = $customer->id;
        $pickup->name = $request->name;
        $pickup->address = $request->address . ', ' . $request->tumbon . ', ' . $request->amphur . ', ' . $request->province . ' ' . $request->postcode;
        $pickup->company = $customer->company;
        $pickup->phone = $request->phone;
        $pickup->line = $customer->line;
        $pickup->email = $customer->email;
        $pickup->status = $status;
        $pickup->save();

        $manifest = new Manifest;
        $manifest->m_id = $request->m_id;
        $manifest->user_create = 1;
        $manifest->status = $status;
        $manifest->price_pay = 0;
        $manifest->type_pay = 'โอนเงิน';
        $manifest->discount = $request->discount_price;
        $manifest->p_id = $pickup->id;
        $manifest->save();

        $price_total = 0; //ไว้เก็บราคารวมทั้งหมด

        $collection = collect();
        for ($x = 0; $x < $count_shipment; $x++) {
            $sme_code = self::smerandom(5);
            $detail = new ManifestDetail;
            $detail->tracking_code = $sme_code;
            $detail->inv_id = $manifest->id;
            $detail->m_id = $manifest->m_id;
            $detail->p_id = $manifest->p_id;
            //$detail->so = $request->sh_so[$x];
            //$detail->awb = $request->sh_awb[$x];
            $detail->recipient = $request->sh_recipient[$x];
            $detail->company = ($request->sh_company[$x] != null ? $request->sh_company[$x] : $request->sh_recipient[$x]);
            $detail->services = $request->sh_type_of_service[$x];
            $detail->price = $request->total_regular_item[$x];
            $detail->country = $request->sh_country[$x];
            $detail->country_code = $request->sh_country_code[$x];
            $detail->currency = $request->sh_currency[$x];
            $detail->city = $request->sh_city[$x];
            $detail->address = $request->sh_address1[$x];
            $detail->address2 = $request->sh_address2[$x];
            $detail->address3 = $request->sh_address3[$x];
            $detail->description = $request->sh_description[$x];
            $detail->zipcode = $request->sh_postcode[$x];
            $detail->note = $request->sh_Note[$x];
            //$detail->comment = $request->sh_note[$x];
            //$detail->tracking_dhl = $request->sh_tracking_DHL[$x];
            //$detail->tracking_local = $request->sh_tracking_local[$x];
            //$detail->awb_express = '';
            //$detail->air_mail = '';
            $detail->shipping_type = $request->sh_description[$x];
            $detail->declared_value = $request->sh_declared_value[$x];
            $detail->content_quantity = $request->sh_content_quantity[$x];
            $detail->packge_type = $request->sh_packge_type[$x];
            $detail->phone = $request->sh_phone[$x];
            $detail->email = $request->sh_email[$x];
            $detail->state = $request->sh_state[$x];
            $detail->suburb = $request->sh_suburb[$x];
            $detail->weight = $request->total_weight_item[$x];
            $detail->status = 1; //1 = process; 2 = success; 3 = return;
            $detail->save();
            $count_box = count($request->weight[$x]);
            for ($i = 0; $i < $count_box; $i++) {
                $box = new ManifestDetail_box;
                $box->shipment_id = $detail->id;
                $box->inv_id = $manifest->id;
                $box->weight = $request->weight[$x][$i];
                $box->width = $request->width[$x][$i];
                $box->length = $request->length[$x][$i];
                $box->height = $request->height[$x][$i];
                $box->series = $request->series[$x][$i];
                $box->save();

                if ($request->type_delivery == 'Drop off ไปรษณีย์ไทย') {
                    $thaiPostBarcode = ThaiPostEMSNumber::query()->whereNull('manifest_id')->where('used', false)->get()->random(1)->first();

                    $t = new ThaiPost();
                    $t->order()->params([
                        'orderId' => $manifest->id,
                        'invNo' => $detail->tracking_code,
                        'barcode' => $thaiPostBarcode->number,
                        'shipperName' => $request->name,
                        'shipperAddress' => $request->address . ' ' . $request->tumbon,
                        'shipperDistrict' => $request->amphur,
                        'shipperProvince' => $request->province,
                        'shipperZipcode' => $request->postcode,
                        'shipperEmail' => $customer->email,
                        'shipperMobile' => $request->phone,
                        'cusName' => 'บริษัท เอสเอ็มอี ชิปปิ้ง จำกัด',
                        'cusAdd' => '46-48 ซอยรัชดาภิเษก 16 ถนนรัชดาภิเษก',
                        'cusAmp' => 'บางกอกใหญ่',
                        'cusProv' => 'กรุงเทพฯ',
                        'cusZipcode' => '10600',
                        'cusTel' => '021057777',
                        'productPrice' => '0',
                        'productInbox' => '-',
                        'productWeight' => '0',
                        'orderType' => 'D',
                        'manifestNo' => $manifest->id,
                    ])->post();

                    $thaiPostBarcode->manifest_id = $manifest->id;
                    $thaiPostBarcode->save();

                    $receiveIn = new AppBoxCheckin();
                    $receiveIn->name = $request->name;
                    $receiveIn->delivery_by = "ไปรษณีย์ไทย";
                    $receiveIn->note = "พัสดุจากงาน " . $manifest->id;
                    $receiveIn->tracking_no = $thaiPostBarcode->number;
                    $receiveIn->status = 1;
                    $receiveIn->inv_id = $manifest->id;
                    $receiveIn->save();
                }
            }

            $count_desc = count($request->d_amount[$x]);
            for ($i = 0; $i < $count_desc; $i++) {
                DB::table('app_invoice')->insert(
                    ['description' => $request->d_description[$x][$i],
                        'value' => $request->d_amount[$x][$i],
                        'quantity' => $request->d_number[$x][$i],
                        's_id' => $detail->id,
                    ]
                );
            }

            if ($request->sh_Pack[$x] == 'yes') {
                $other = new ManifestDetail_Other_Service;
                $other->shipment_id = $detail->id;
                $other->other_service_name = 'RePack';
                $other->other_service_value = 0;
                $other->save();
            }
            if ($request->sh_Insurance[$x] == 'yes') {
                $other = new ManifestDetail_Other_Service;
                $other->shipment_id = $detail->id;
                $other->other_service_name = 'Insurance';
                $other->other_service_value = 0;
                $other->save();
            }
            if ($request->sh_ExportDoc[$x] == 'yes') {
                $other = new ManifestDetail_Other_Service;
                $other->shipment_id = $detail->id;
                $other->other_service_name = 'Export Doc';
                $other->other_service_value = 0;
                $other->save();
            }

            $price_total = $price_total + $request->total_regular_item[$x];
        }
        $manifest->price_total = $price_total;
        $manifest->save();
        /* app('App\Http\Controllers\Notification\NotificationController')
         ->sendMessage('คุณ'.$customer->firstname.' สร้างพัสดุเข้ามา','https://backend.smeshipping.com/admin/request/'.$manifest->id,'create');
         */
        if ($discount != null) {
            $new_discount = new DiscountUsed;
            $new_discount->discount_id = $discount->id;
            $new_discount->discount_code = $discount->discount_code;
            $new_discount->customer_id = session('cus.id');
            $new_discount->customer_name = $request->name;
            $new_discount->amount = $request->discount_price;
            $new_discount->manifest_id = $manifest->id;
            $new_discount->save();
        }

        return $manifest->id;

        //return dd($address);
    }

    public function gettracking($id)
    {
        $manifest = ManifestDetail:: select('tracking_code')
            ->where('inv_id', '=', $id)
            ->first();

        return $manifest->tracking_code;
    }

    public function smerandom($length = 5)
    {

        $possible = "1234567890"; //ตัวอักษรที่ต้องการสุ่ม
        $loop = true;
        while ($loop) {
            $str = "";
            $sum = 0;
            while (strlen($str) < $length) {
                $thisstr = substr($possible, (rand() % strlen($possible)), 1);
                $str .= $thisstr;
                $sum = $sum + $thisstr;
            }
            $str .= substr($sum, -1);
            $tracking_code = DB::table('app_manifest_detail')->where('tracking_code', $str)->count();
            if ($tracking_code == 0) {
                $loop = false;
            }
        }

        return $str;
    }

    public function booking()
    {
        if (!session()->has('cus.id')) {
            return redirect('/app/login');
        }
        $address = DB::table('customer_address')
            ->where('customer_address.customer_id', session('cus.id'))
            ->select('customer_address.*', 'customer_address.*')
            ->get();
        $country_all = Address::select('country_name', 'country_cd', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        $currency = DB::table('app_currency')->orderBy('code')->get();
        return view('customer/app/booking', compact('address', 'country_all', 'currency'));
    }

    public function bookingexcel()
    {
        if (!session()->has('cus.id')) {
            return redirect('/app/login');
        }
        $address = DB::table('customer_address')
            ->where('customer_address.customer_id', session('cus.id'))
            ->select('customer_address.*', 'customer_address.*')
            ->get();
        $country_all = Address::select('country_name', 'country_cd', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        $currency = DB::table('app_currency')->orderBy('code')->get();
        return view('customer/app/bookingexcel', compact('address', 'country_all', 'currency'));
    }

    public function eship()
    {
        if (!session()->has('cus.id')) {
            return redirect('/app/login');
        }
        $address = DB::table('customer_address')
            ->where('customer_address.customer_id', session('cus.id'))
            ->get();
        $country_all = Address::select('country_name', 'country_cd', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        $currency = DB::table('app_currency')->orderBy('code')->get();
        return view('customer/app/eshipment', compact('address', 'country_all', 'currency'));
    }

    public function copy_booking($id)
    {
        if (!session()->has('cus.id')) {
            return redirect('/app/login');
        }
        $shipment = DB::table('app_manifest_detail')
            ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
            ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
            ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.name as service_name',
                'app_services.cal_weight', 'app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight')
            ->where('app_manifest_detail.id', '=', $id)
            ->orderBy('app_manifest_detail.id')
            ->get();

        $address = DB::table('customer_address')
            ->where('customer_address.customer_id', session('cus.id'))
            ->select('customer_address.*', 'customer_address.*')
            ->get();
        $country_all = Address::select('country_name', 'country_cd', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        $currency = DB::table('app_currency')->orderBy('code')->get();
        //Other Service

        //box and description

        foreach ($shipment as $key => $value) {
            $value->pack = 'no';
            $value->insurance = 'no';

            $box = DB::table('app_manifest_box')
                ->where('shipment_id', '=', $value->id)
                ->select('app_manifest_box.*', DB::raw('TRUNCATE((width*length*height)/5000,10) as sum_weight'))
                ->orderBy('series')
                ->get();
            $desc = DB::table('app_invoice')
                ->where('s_id', '=', $value->id)
                ->get();

            $other_service_name = collect();
            $other_service_price = collect();
            $other_service_total_price = 0;
            $other_service_list = ManifestDetail_Other_Service::where('shipment_id', $value->id)->get();
            foreach ($other_service_list as $other_value) {
                if ($other_value->other_service_name == 'Insurance') {
                    $value->insurance = 'yes';
                }
                if ($other_value->other_service_name == 'RePack') {
                    $value->pack = 'yes';
                }
            }

            $value->box = $box;
            $value->desc = $desc;

            try {
                $country_id = Country::where('code', $value->country_code)->first();
                $country_zone = CountryZone::where('country', $country_id->id)->where('s_id', $value->services)->first();
                $service = ServiceZone::where('s_id', $value->services)->orderBy('weight', 'ASC')->where('weight', '>=', $value->weight)->select('zone_' . $country_zone->zone . ' as result')->first();
                $value->price = $service->result;
            } catch (Exception $e) {

            }
        }


        //return dd($shipment);
        return view('customer/app/eshipment', compact('shipment', 'address', 'country_all', 'currency'));
    }

    public function booking_label($id)
    {
        if (!session()->has('cus.id')) {
            return redirect('/app/login');
        }
        DB::table('shipment_temp')->where('m_id', session('cus.id'))->delete();
        $shipment = DB::table('app_manifest_detail')
            ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
            ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
            ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.name as service_name',
                'app_services.cal_weight', 'app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight', 'app_services.id as box', 'app_services.logo')
            ->where('app_manifest_detail.inv_id', '=', $id)
            ->where('app_manifest_detail.m_id', '=', session('cus.id'))
            ->orderBy('app_manifest_detail.id')
            ->get();
        foreach ($shipment as $value) {
            $box = ManifestDetail_box::where('shipment_id', $value->id)->count();
            $value->box = $box;
        }
        return view('customer/app/label', compact('shipment'));
        // return $shipment->toJson();
    }

    public function save_temp(Request $request)
    {
        DB::table('shipment_temp')->where('m_id', session('cus.id'))->delete();
        DB::table('shipment_temp')->insert(['m_id' => session('cus.id'), 'data' => $request->temp]);
        return 'ok';
    }

    public function temp_booking()
    {
        if (!session()->has('cus.id')) {
            return redirect('/app/login');
        }
        $temp = DB::table('shipment_temp')->where('m_id', session('cus.id'))->first();

        $address = DB::table('customer_address')
            ->where('customer_address.customer_id', session('cus.id'))
            ->select('customer_address.*', 'customer_address.*')
            ->get();
        $country_all = Address::select('country_name', 'country_cd', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        $currency = DB::table('app_currency')->orderBy('code')->get();
        //Other Service

        return view('customer/app/eshipment', compact('temp', 'address', 'country_all', 'currency'));
    }

    public function get_recipient($country_code, $keyword = null)
    {
        if ($country_code != 'all' && $keyword != null) {
            $recipient = ManifestDetail::where('m_id', session('cus.id'))
                ->where('country_code', $country_code)
                ->where(function ($query) use ($keyword) {
                    $query->where('recipient', 'like', '%' . $keyword . '%')
                        ->orWhere('company', 'like', '%' . $keyword . '%')
                        ->orWhere('phone', 'like', '%' . $keyword . '%');
                })
                ->select('recipient', 'email', 'company', 'country', 'country_code', 'city', 'address', 'address2', 'address3', 'zipcode', 'phone', 'state', 'suburb')
                ->distinct()
                ->limit(15)
                ->get();
        } else if ($country_code == 'all' && $keyword != null) {
            $recipient = ManifestDetail::where('m_id', session('cus.id'))
                ->where(function ($query) use ($keyword) {
                    $query->where('recipient', 'like', '%' . $keyword . '%')
                        ->orWhere('company', 'like', '%' . $keyword . '%')
                        ->orWhere('phone', 'like', '%' . $keyword . '%');
                })
                ->select('recipient', 'email', 'company', 'country', 'country_code', 'city', 'address', 'address2', 'address3', 'zipcode', 'phone', 'state', 'suburb')
                ->distinct()
                ->limit(15)
                ->get();
        } else {
            $recipient = ManifestDetail::where('m_id', session('cus.id'))
                ->select('recipient', 'email', 'company', 'country', 'country_code', 'city', 'address', 'address2', 'address3', 'zipcode', 'phone', 'state', 'suburb')
                ->distinct()
                ->limit(15)
                ->get();
        }

        return $recipient->toJson();
    }

    public function check_price()
    {
        // if(!session()->has('cus.id')){
        //   return redirect('/app/login');
        // }
        $country_all = Address::select('country_name', 'country_cd', 'country_cd')
            ->orderBy('country_name')
            ->distinct()
            ->get();
        return view('customer/app/check_price', compact('country_all'));
    }

    public function index_upload_booking()
    {
        if (!session()->has('cus.id')) {
            return redirect('/app/login');
        }
        $customer = DB::table('app_customer')->where('id', session('cus.id'))->first();
        $service_all = DB::table('app_services')
            ->leftJoin('app_services_group_map', 'app_services.id', '=', 'app_services_group_map.service_id')
            ->where('app_services_group_map.group_id', $customer->service_group)
            ->whereNull('app_services.del')
            ->where('app_services.active', '1')
            ->select('app_services.*')
            ->orderBy('app_services.code')
            ->get();
        $address = DB::table('customer_address')
            ->where('customer_address.customer_id', session('cus.id'))
            ->select('customer_address.*', 'customer_address.*')
            ->get();
        return view('customer/app/index_upload_booking', compact('service_all', 'address'));
    }

    public function store_upload_booking(Request $request)
    {
        $request->m_id = session('cus.id');
        $status = 0; // ตั้งค่าเริ่มต้นเป็น 0 Incomming
        $customer = DB::table('app_customer')->where('id', session('cus.id'))->first();

        $addressold = DB::table('customer_address')
            ->where('customer_address.customer_id', $request->m_id)
            ->where('customer_address.pickup', 1)
            ->select('customer_address.*')
            ->first();

        $file = $request->file('file')->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $excelName = $request->m_id . '_' . time() . '_' . $fileName . '.' . $request->file('file')->getClientOriginalExtension();
        $request->file('file')->move(public_path('/uploads/booking'), $excelName);

        if ($addressold == null) {
            $address_ = new CustomerAddress;
            $address_->customer_name = $request->name;
            $address_->customer_id = session('cus.id');
            $address_->phone = $request->phone;
            $address_->address = $request->address;
            $address_->tumbon = $request->tumbon;
            $address_->amphur = $request->amphur;
            $address_->province = $request->province;
            $address_->postcode = $request->postcode;
            $address_->pickup = 1;
            $address_->save();
        }

        $service_db = $address = DB::table('app_services')->where('id', $request->sh_type_of_service[0])->first();
        $pickup = new Pickup;
        $pickup->insighly = 'on';
        $pickup->type_work = 1;
        $pickup->type_of_service = $service_db->cat;
        $pickup->type_delivery = 'ไม่ระบุ';
        $pickup->delivery_date = strtotime(str_replace("/", "-", $request->delivery_date));
        $pickup->delivery_time = "พร้อมรับ";
        $pickup->shipment = 1;
        $pickup->m_note = $request->m_note . ' /// ต้องการใช้บริการ ' . $service_db->name;
        $pickup->type_shipping = 'ดูที่ไฟล์แนบมา';
        $pickup->m_id = $customer->id;
        $pickup->name = $request->name;
        $pickup->address = $request->address . ', ' . $request->tumbon . ', ' . $request->amphur . ', ' . $request->province . ' ' . $request->postcode;
        $pickup->company = $customer->company;
        $pickup->phone = $request->phone;
        $pickup->line = $customer->line;
        $pickup->email = $customer->email;
        $pickup->status = $status;
        $pickup->shipment_file = $excelName;
        $pickup->save();

        $manifest = new Manifest;
        $manifest->m_id = $request->m_id;
        $manifest->user_create = 1;
        $manifest->status = $status;
        $manifest->price_pay = 0;
        $manifest->type_pay = 'โอนเงิน';
        $manifest->discount = 0.00;
        $manifest->p_id = $pickup->id;
        $manifest->save();

        return redirect()->back()->withErrors(['บันทึกสำเร็จ']);
    }

    public function create_url(Request $request)
    {
        $customer_booking = CustomerBookingNote::where('uuid', $request->uuid)->first();

        if ($customer_booking->sender_name == null) {
            $customer_booking->uuid = $request->uuid;
            $customer_booking->tracking = $request->tracking;
            $customer_booking->sender_name = $request->sender_name;
            $customer_booking->sender_address = $request->sender_address;
            $customer_booking->sender_tel = $request->sender_tel;
            $customer_booking->sender_email = $request->sender_email;
            $customer_booking->receiver_name = $request->receiver_name;
            $customer_booking->receiver_address = $request->receiver_address;
            $customer_booking->receiver_tel = $request->receiver_tel;
            $customer_booking->receiver_email = $request->receiver_email;
            $customer_booking->commodity_description = $request->commodity_description;
            $customer_booking->quantity_unit = $request->quantity_unit;
            $customer_booking->currency = $request->currency;
            $customer_booking->total_packages = $request->total_packages;
            $customer_booking->note = $request->note;
            $customer_booking->value = $request->value;
            $customer_booking->shipdetail = $request->shipdetail;
            $customer_booking->updated_at = $request->updated_at;
            $customer_booking->save();
        } else {
            $customer_booking->dalivery = $request->dalivery;
            $customer_booking->dalivery_tracking = $request->dalivery_tracking;
            $customer_booking->save();
        }
        return redirect()->back()->withErrors(['บันทึกสำเร็จ']);

    }

    public function get_uuid()
    {
        $data = new CustomerBookingNote;
        $data->uuid = uniqid();
        $data->tracking = self::checkrandom(4);
        $data->created_by = User::query()->find(1)->id;
        $data->save();
        return view('customer/app/customer', compact('data'));
    }

    public function checkrandom($length=4){

        $possible = "1234567890"; //ตัวอักษรที่ต้องการสุ่ม
        $loop = true;
        while($loop){
            $str = "";
            $sum = 0;
            while(strlen($str)<$length){
                $thisstr = substr($possible,(rand()%strlen($possible)),1);
                $str.=$thisstr;
                $sum = $sum+$thisstr;
            }
            $str.= substr($sum,-1);
            $tracking_code = CustomerBookingNote::where('tracking',$str)->whereNull('del')->count();
            if($tracking_code==0){
                $loop = false;
            }
        }

        return $str;
    }
}
