<?php

namespace App\Http\Controllers\Customer;

use App\Customer;
use App\Manifest;
use App\ManifestDetail_box;
use App\Http\Controllers\Controller;
use App\CustomerAddress;
use App\PaymentAttachmentFromCustomer;
use App\Attachment_Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;
use Image;
class PaymentController extends Controller
{
    public function create(Request $request)
    {
        return back()->withErrors(['This email address is already registered.']);
    }

    
    public function list()
    { 
        if(!session()->has('cus.id')){
            return redirect('/app/login');
        }
        $data = DB::table('app_manifest')
        ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
        ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
        ->leftJoin('app_customer', 'app_manifest.m_id', '=', 'app_customer.id')
        ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
        ->WhereIn('app_manifest.status', [3,4])
        ->Where('app_manifest.m_id', '=', session('cus.id'))
        ->select('pickup_manifest.*','pickup_manifest.id as pickup_code', 'app_manifest.*',
         'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename','app_customer.credit_term')
        ->orderBy('pickup_manifest.delivery_date', 'desc')
        ->paginate(15);
        
        return view('customer/app/wait_payment_list',compact('data'));
        //return $data->toJson();
    }
    public function payment(Request $request){
        
        $listID = explode(',', $request->list);
        if(!session()->has('cus.id')){
            return redirect('/app/login');
        }
        $datalist = DB::table('app_manifest')
        ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
        ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
        ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
       // ->WhereIn('app_manifest.status',  [3,4])
        ->Where('app_manifest.m_id', '=', session('cus.id'))
        ->WhereIn('app_manifest.id', $listID)
        ->select('pickup_manifest.*','pickup_manifest.id as pickup_code', 'app_manifest.*',
         'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename')
         ->get();
        $shipment = DB::table('app_manifest_detail')
            ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
            ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
            ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.description as service_name',
                'app_services.cal_weight','app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight','app_services.id as box','app_services.logo')
            ->whereIn('app_manifest_detail.inv_id', $listID)
            ->where('app_manifest_detail.m_id','=',session('cus.id'))
            ->orderBy('app_manifest_detail.id')
            ->get();
        

        foreach ($shipment as $value) {
            $box = ManifestDetail_box::where('shipment_id', $value->id)->count();
            $value->box = $box;
        }

        $booking = count($datalist);
        $payment_file_re = null;
        $shipment = count($shipment);
        $amount_total = 0;
        $amount_pay = 0;
        $amount = 0;
        foreach ($datalist as $key => $value) {
            $amount_total += $value->price_total-$value->discount;
            $amount_pay += $value->price_pay;
            $amount = $amount_total - $amount_pay;
        }
        return view('customer/app/payment',compact('shipment','booking','payment_file_re','amount_total','amount_pay','amount'));
    }
    public function payment_pass(Request $request){
        $listID = explode(',', $request->list);
        $datalist = DB::table('app_manifest')
        ->leftJoin('pickup_manifest', 'pickup_manifest.id', '=', 'app_manifest.p_id')
        ->leftJoin('app_services', 'app_services.id', '=', 'pickup_manifest.type_of_service')
        ->leftJoin('users', 'users.id', '=', 'pickup_manifest.sales')
       // ->WhereIn('app_manifest.status',  [3,4])
        ->Where('app_manifest.m_id', '=', session('cus.id'))
        ->WhereIn('app_manifest.id', $listID)
        ->select('pickup_manifest.*','pickup_manifest.id as pickup_code', 'app_manifest.*',
         'app_manifest.id as manifestid', 'app_manifest.status as request_status', 'app_services.name as service_name','users.name as salename')
         ->get();
        $shipment = DB::table('app_manifest_detail')
            ->leftJoin('app_country', 'app_manifest_detail.country', '=', 'app_country.id')
            ->leftJoin('app_services', 'app_manifest_detail.services', '=', 'app_services.id')
            ->select('app_manifest_detail.*', 'app_country.country as country_name', 'app_country.code as c_code', 'app_services.description as service_name',
                'app_services.cal_weight','app_services.routing_id', 'app_manifest_detail.weight as total_sum_weight','app_services.id as box','app_services.logo')
            ->whereIn('app_manifest_detail.inv_id', $listID)
            ->where('app_manifest_detail.m_id','=',session('cus.id'))
            ->orderBy('app_manifest_detail.id')
            ->get();
        

        foreach ($shipment as $value) {
            $box = ManifestDetail_box::where('shipment_id', $value->id)->count();
            $value->box = $box;
        }

        $booking = count($datalist);
        $payment_file_re = Attachment_Payment::whereIn('inv_id',$listID)->count();
        $shipment = count($shipment);
        $amount_total = 0;
        $amount_pay = 0;
        $amount = 0;
        foreach ($datalist as $key => $value) {
            $amount_total += $value->price_total-$value->discount;
            $amount_pay += $value->price_pay;
            $amount = $amount_total - $amount_pay;
        }
        return view('customer/app/payment_pass',compact('shipment','booking','payment_file_re','amount_total','amount_pay','amount'));
    }
    public function upload_payment(Request $request){
        //$data = collect();
        $files = $request->file('files');
        $listID = explode(',', $request->list);
        
        if($request->hasFile('files'))
        {
            foreach ($files as $fileitem) {
                $file = $fileitem->getClientOriginalName();
                $fileName = pathinfo($file,PATHINFO_FILENAME);
                $imageName = time().'_'.$fileName.'.'.$fileitem->getClientOriginalExtension();
                $fileitem->move(public_path('/uploads/payment'), $imageName);
                foreach($listID as $_id){
                $img = Image::make(public_path('/uploads/payment/').$imageName);
                $img->resize(null, 1000, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(public_path('/uploads/payment/').$_id.'_'.$imageName);
                    $attach = new Attachment_Payment;
                    $attach->inv_id = $_id;
                    $attach->filename = $_id.'_'.$imageName;
                    if(count($listID)>1){
                        $attach->ref = $request->list;
                    }
                    $attach->save();
                }
                File::delete(public_path('/uploads/payment/').$imageName);
            }
        }
        foreach($listID as $_id){
            $count = Attachment_Payment::where('inv_id',$_id)->count();
            if($count>0){
                $update_m = Manifest::where('id',$_id)->first();
                $update_m->payment_file_re = 'true';
                $update_m->save();
            }
            else{
                $update_m = Manifest::where('id',$_id)->first();
                $update_m->payment_file_re = null;
                $update_m->save();
            }
        }
         return 'true';
    }
    
    public function payment_attachment(){
        return view('customer/app/payment_attachment');
    }

    public function upload_payment_attachment(Request $request){
        $attach = new PaymentAttachmentFromCustomer;
        $files = $request->file('files');
        $filename = null;
        if($request->hasFile('files'))
            {  
                foreach ($files as $index=>$fileitem) {
                    $file = $fileitem->getClientOriginalName();
                    $fileName = pathinfo($file,PATHINFO_FILENAME);
                    $imageName = time().'_'.$fileName.'.'.$fileitem->getClientOriginalExtension();
                    $fileitem->move(public_path('/uploads/so'), $imageName);
                    $img = Image::make(public_path('/uploads/so/').$imageName);
                    $img->resize(null, 1000, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save(public_path('/uploads/so/').$imageName);
                    $filename =$filename.($index!=0?',':'').$imageName;
                }
            }
        $attach->filename = $filename;
        $attach->customer_name	 = $request->name;
        $attach->so = $request->so;
        $attach->tel = $request->tel;
        $attach->save();
    return back()->withInput();
    }
}
