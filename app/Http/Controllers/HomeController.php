<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function connect()
    {
        $country = new Country;
        $country->iata = "thx";
        $country->code = "thx";
        $country->country = "thx";
        $country->save();
        return $country->toJson(JSON_PRETTY_PRINT);
    }

    public function work_redirect(){
        if(Auth::user()->position=='Administrator'){
           $url = 'admin/servicegroup';
        }
        else if(Auth::user()->position=='Messenger'){
           $url = 'admin/messenger';
        }
        else if(Auth::user()->position=='Accountant'){
           $url = 'admin/waitforpayment';
       }
       else if(Auth::user()->position=='Sale'||Auth::user()->position=='Booking Agent'){
           $url = 'admin/waitforsale';
       }
       else{
           $url = 'admin/request';
        }
        return redirect($url);
    }
}
