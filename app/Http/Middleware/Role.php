<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        if(Auth::user()->position=='Administrator'){
            //return $next($request);
        }
        else if ($role=='role_service'&&Auth::user()->role_service!=1) {
            return redirect()->back()->withErrors(['คุณไม่มีสิทธิ์ในการเข้าถึงข้อมูลส่วนนี้']);
        }
        else if($role=='role_customer'&&Auth::user()->role_customer!=1) {
            return redirect()->back()->withErrors(['คุณไม่มีสิทธิ์ในการเข้าถึงข้อมูลส่วนนี้']);
        }
        else if($role=='role_employee'&&Auth::user()->role_employee!=1) {
            return redirect()->back()->withErrors(['คุณไม่มีสิทธิ์ในการเข้าถึงข้อมูลส่วนนี้']);
        }

        return $next($request);
    }
}
