<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManifestDetail extends Model
{
    protected $table = 'app_manifest_detail';

    public function manifest()
    {
        return $this->belongsTo(Manifest::class, 'inv_id', 'id');
    }

    public function boxes()
    {
        return $this->hasMany(ManifestDetail_box::class, 'shipment_id', 'id');
    }

    public function otherServices()
    {
        return $this->hasMany(ManifestDetail_Other_Service::class, 'shipment_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(Attachment_Payment::class, 'inv_id', 'inv_id');
    }

    public function sos()
    {
        return $this->hasMany(Attachment_Payment::class, 'inv_id', 'inv_id');
    }

}
