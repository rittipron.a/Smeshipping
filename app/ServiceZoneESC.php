<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceZoneESC extends Model
{
    protected $table = 'app_services_zone_esc';

    protected $primaryKey = 'id';
}
