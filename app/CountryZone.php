<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryZone extends Model
{
    protected $table = 'app_country_zone';

    protected $primaryKey = 'id';
}
