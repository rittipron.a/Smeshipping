<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceGroup extends Model
{
    protected $table = 'app_services_group';

    protected $primaryKey = 'id';
}
