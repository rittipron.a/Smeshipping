<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceGroupMap extends Model
{
    protected $table = 'app_services_group_map';

    protected $primaryKey = 'id';
}
