<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingErrorGroup extends Model
{
    protected $table = 'tracking_error_message_group';

    protected $fillable = ['name', 'isComplete'];

    public function filters()
    {
        return $this->hasMany(TrackingError::class, 'group_id', 'id');
    }

}
