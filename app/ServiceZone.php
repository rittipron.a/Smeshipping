<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceZone extends Model
{
    protected $table = 'app_services_zone';

    protected $primaryKey = 'id';
}
