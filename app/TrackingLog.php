<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrackingLog extends Model
{
    use SoftDeletes;

    protected $table = 'tracking_logs';

    protected $fillable = [
        'deadline', 'note', 'error_id', 'awb', 'awb_return', 'manifest_id', 'message', 'action', 'process', 'assignee_id'
    ];

    public function error()
    {
        return $this->belongsTo(TrackingError::class, 'error_id', 'id');
    }

    public function manifestDetail()
    {
        return $this->belongsTo(ManifestDetail::class, 'awb', 'awb');
    }

    public function assignee()
    {
        return $this->belongsTo(User::class, 'assignee_id', 'id');
    }

    public function logs()
    {
        return $this->hasMany(TrackingLogEvent::class, 'awb', 'awb');
    }
}
