<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    protected $table = 'app_services_type';

    protected $primaryKey = 'id';
}
