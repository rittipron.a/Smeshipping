<?php

namespace App\Classes\API;

use DOMDocument;
use Illuminate\Support\Facades\Auth;
use SimpleXMLElement;

class FedEx
{
    private $url;
    private $curl;
    private $key;
    private $password;
    private $accountNumber;
    private $meterNumber;
    private $response;
    private $headers = [];

    public function __construct()
    {
        $this->url = (config('fedex.mode') == 'development') ? 'https://wsbeta.fedex.com:443/web-services' : 'https://ws.fedex.com:443/web-services';
        $this->key = config('fedex.key');
        $this->password = config('fedex.password');
        $this->accountNumber = config('fedex.account_number');
        $this->meterNumber = config('fedex.meter_number');

        array_push($this->headers, implode(" ", ["Content-Type:", "application/xml"]));

        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->curl, CURLOPT_URL, $this->url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($this->curl, CURLOPT_POST, true);
    }

    function mungXML($xml)
    {
        $obj = SimpleXML_Load_String($xml);
        if ($obj === FALSE) return $xml;

        // GET NAMESPACES, IF ANY
        $nss = $obj->getNamespaces(TRUE);
        if (empty($nss)) return $xml;

        // CHANGE ns: INTO ns_
        $nsm = array_keys($nss);
        foreach ($nsm as $key) {
            // A REGULAR EXPRESSION TO MUNG THE XML
            $rgx
                = '#'               // REGEX DELIMITER
                . '('               // GROUP PATTERN 1
                . '\<'              // LOCATE A LEFT WICKET
                . '/?'              // MAYBE FOLLOWED BY A SLASH
                . preg_quote($key)  // THE NAMESPACE
                . ')'               // END GROUP PATTERN
                . '('               // GROUP PATTERN 2
                . ':{1}'            // A COLON (EXACTLY ONE)
                . ')'               // END GROUP PATTERN
                . '#'               // REGEX DELIMITER
            ;
            // INSERT THE UNDERSCORE INTO THE TAG NAME
            $rep
                = '$1'          // BACKREFERENCE TO GROUP 1
                . '_'           // LITERAL UNDERSCORE IN PLACE OF GROUP 2
            ;
            // PERFORM THE REPLACEMENT
            $xml = preg_replace($rgx, $rep, $xml);
        }

        return $xml;
    }

    public function get()
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $this->headers
        ));

        $response = curl_exec($this->curl);
        $response = iconv('UTF-8', 'UTF-8//IGNORE', $response);

        $this->close();

        $this->response = collect(json_decode(json_encode(SimpleXML_Load_String($this->mungXML(trim($response)), 'SimpleXMLElement', LIBXML_NOCDATA)), true));

        return $this->response;
    }

    public function post()
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => $this->headers
        ));

        $response = curl_exec($this->curl);
        $response = iconv('UTF-8', 'UTF-8//IGNORE', $response);

        $this->close();

        $this->response = collect(json_decode(json_encode(SimpleXML_Load_String($this->mungXML(trim($response)), 'SimpleXMLElement', LIBXML_NOCDATA)), true));

        return $this->response;
    }

    protected function close()
    {
        curl_close($this->curl);
    }

    public function track(array $awbs)
    {
        $xml = '<?xml version="1.0" encoding="utf-8"?>';
        $xml .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://fedex.com/ws/track/v19">';
        $xml .= '<SOAP-ENV:Body>';
        $xml .= '<TrackRequest>';
        $xml .= '<WebAuthenticationDetail>';
        $xml .= '<UserCredential>';
        $xml .= '<Key>' . $this->key . '</Key>';
        $xml .= '<Password>' . $this->password . '</Password>';
        $xml .= '</UserCredential>';
        $xml .= '</WebAuthenticationDetail>';
        $xml .= '<ClientDetail>';
        $xml .= '<AccountNumber>' . $this->accountNumber . '</AccountNumber>';
        $xml .= '<MeterNumber>' . $this->meterNumber . '</MeterNumber>';
        $xml .= '</ClientDetail>';
        $xml .= '<Version>';
        $xml .= '<ServiceId>trck</ServiceId>';
        $xml .= '<Major>19</Major>';
        $xml .= '<Intermediate>0</Intermediate>';
        $xml .= '<Minor>0</Minor>';
        $xml .= '</Version>';

        foreach ($awbs as $awb) {
            $xml .= '<SelectionDetails>';
            $xml .= '<CarrierCode>FDXE</CarrierCode>';
            $xml .= '<PackageIdentifier>';
            $xml .= '<Type>TRACKING_NUMBER_OR_DOORTAG</Type>';
            $xml .= '<Value>' . $awb . '</Value>';
            $xml .= '</PackageIdentifier>';
            $xml .= '<ShipmentAccountNumber/>';
            $xml .= '<SecureSpodAccount/>';
            $xml .= '</SelectionDetails>';
        }

        $xml .= '</TrackRequest>';
        $xml .= '</SOAP-ENV:Body>';
        $xml .= '</SOAP-ENV:Envelope>';

        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $xml);

        return $this;
    }

    public function processShipment(string $json)
    {
        $inputJSON = json_decode($json, TRUE);

        $xml = '<?xml version="1.0" encoding="utf-8"?>';
        $xml .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://fedex.com/ws/ship/v26">';
        $xml .= '<SOAP-ENV:Body>';
        $xml .= '<ProcessShipmentRequest>';
        $xml .= '<WebAuthenticationDetail>';
        $xml .= '<UserCredential>';
        $xml .= '<Key>' . $this->key . '</Key>';
        $xml .= '<Password>' . $this->password . '</Password>';
        $xml .= '</UserCredential>';
        $xml .= '</WebAuthenticationDetail>';
        $xml .= '<ClientDetail>';
        $xml .= '<AccountNumber>' . $this->accountNumber . '</AccountNumber>';
        $xml .= '<MeterNumber>' . $this->meterNumber . '</MeterNumber>';
        $xml .= '</ClientDetail>';
        $xml .= '<TransactionDetail>';
        $xml .= '<CustomerTransactionId>*** Express International Shipping Request using PHP ***</CustomerTransactionId>';
        $xml .= '</TransactionDetail>';
        $xml .= '<Version>';
        $xml .= '<ServiceId>ship</ServiceId>';
        $xml .= '<Major>26</Major>';
        $xml .= '<Intermediate>0</Intermediate>';
        $xml .= '<Minor>0</Minor>';
        $xml .= '</Version>';
        $xml .= '<RequestedShipment>';
        $xml .= '<ShipTimestamp>' . date('c') . '</ShipTimestamp>';
        $xml .= '<DropoffType>' . $inputJSON['RequestedShipment']['DropoffType'] . '</DropoffType>';
        $xml .= '<ServiceType>' . $inputJSON['RequestedShipment']['ServiceType'] . '</ServiceType>';
        $xml .= '<PackagingType>' . $inputJSON['RequestedShipment']['PackagingType'] . '</PackagingType>';
        $xml .= '<Shipper>';
        $xml .= '<Contact>';
        $xml .= '<PersonName>' . $inputJSON['Contact']['PersonName'] . '</PersonName>';
        $xml .= '<CompanyName>' . $inputJSON['Contact']['CompanyName'] . '</CompanyName>';
        $xml .= '<PhoneNumber>' . $inputJSON['Contact']['PhoneNumber'] . '</PhoneNumber>';
        $xml .= '</Contact>';
        $xml .= '<Address>';

        foreach ($inputJSON['Address']['StreetLines'] as $streetLine) {
            $xml .= '<StreetLines>' . $streetLine . '</StreetLines>';
        }
        $xml .= '<City>' . $inputJSON['Address']['City'] . '</City>';
        $xml .= '<StateOrProvinceCode>' . $inputJSON['Address']['StateOrProvinceCode'] . '</StateOrProvinceCode>';
        $xml .= '<PostalCode>' . $inputJSON['Address']['PostalCode'] . '</PostalCode>';
        $xml .= '<CountryCode>' . $inputJSON['Address']['CountryCode'] . '</CountryCode>';
        $xml .= '</Address>';
        $xml .= '</Shipper>';
        $xml .= '<Recipient>';
        $xml .= '<Contact>';
        $xml .= '<PersonName>' . $inputJSON['ContactRecipient']['PersonName'] . '</PersonName>';
        $xml .= '<CompanyName>' . $inputJSON['ContactRecipient']['CompanyName'] . '</CompanyName>';
        $xml .= '<PhoneNumber>' . $inputJSON['ContactRecipient']['PhoneNumber'] . '</PhoneNumber>';
        $xml .= '</Contact>';
        $xml .= '<Address>';

        foreach ($inputJSON['AddressRecipient']['StreetLines'] as $streetLine) {
            $xml .= '<StreetLines>' . $streetLine . '</StreetLines>';
        }

        $xml .= '<City>' . $inputJSON['AddressRecipient']['City'] . '</City>';
        $xml .= '<StateOrProvinceCode>' . $inputJSON['AddressRecipient']['StateOrProvinceCode'] . '</StateOrProvinceCode>';
        $xml .= '<PostalCode>' . $inputJSON['AddressRecipient']['PostalCode'] . '</PostalCode>';
        $xml .= '<CountryCode>' . $inputJSON['AddressRecipient']['CountryCode'] . '</CountryCode>';
        $xml .= '<Residential>' . $inputJSON['AddressRecipient']['Residential'] . '</Residential>';
        $xml .= '</Address>';
        $xml .= '</Recipient>';
        $xml .= '<ShippingChargesPayment>';
        $xml .= '<PaymentType>' . $inputJSON['shippingChargesPayment']['PaymentType'] . '</PaymentType>';
        $xml .= '<Payor>';
        $xml .= '<ResponsibleParty>';
        $xml .= '<AccountNumber>' . $this->accountNumber . '</AccountNumber>';
        $xml .= '<Address>';
        $xml .= '<CountryCode>' . $inputJSON['shippingChargesPayment']['Payor']['ResponsibleParty']['Address']['CountryCode'] . '</CountryCode>';
        $xml .= '</Address>';
        $xml .= '</ResponsibleParty>';
        $xml .= '</Payor>';
        $xml .= '</ShippingChargesPayment>';
        $xml .= '<CustomsClearanceDetail>';
        $xml .= '<DutiesPayment>';
        $xml .= '<PaymentType>RECIPIENT</PaymentType>';
//        $xml .= '<Payor>';
//        $xml .= '<ResponsibleParty>';
//        $xml .= '<AccountNumber>' . $this->accountNumber . '</AccountNumber>';
//        $xml .= '<Address>';
//        $xml .= '<CountryCode>' . $inputJSON['customerClearanceDetail']['DutiesPayment']['Payor']['ResponsibleParty']['Address']['CountryCode'] . '</CountryCode>';
//        $xml .= '</Address>';
//        $xml .= '</ResponsibleParty>';
//        $xml .= '</Payor>';
        $xml .= '</DutiesPayment>';
        $xml .= '<DocumentContent>' . $inputJSON['customerClearanceDetail']['DocumentContent'] . '</DocumentContent>';
        $xml .= '<CustomsValue>';
        $xml .= '<Currency>' . $inputJSON['customerClearanceDetail']['CustomsValue']['Currency'] . '</Currency>';
        $xml .= '<Amount>' . $inputJSON['customerClearanceDetail']['CustomsValue']['Amount'] . '</Amount>';
        $xml .= '</CustomsValue>';

        foreach ($inputJSON['customerClearanceDetail']['Commodities'] as $commodity) {
            $xml .= '<Commodities>';
            $xml .= '<NumberOfPieces>' . $commodity['NumberOfPieces'] . '</NumberOfPieces>';
            $xml .= '<Description>' . $commodity['Description'] . '</Description>';
            $xml .= '<CountryOfManufacture>' . $commodity['CountryOfManufacture'] . '</CountryOfManufacture>';
            $xml .= '<Weight>';
            $xml .= '<Units>' . $commodity['Weight']['Units'] . '</Units>';
            $xml .= '<Value>' . $commodity['Weight']['Value'] . '</Value>';
            $xml .= '</Weight>';
            $xml .= '<Quantity>' . $commodity['Quantity'] . '</Quantity>';
            $xml .= '<QuantityUnits>' . $commodity['QuantityUnits'] . '</QuantityUnits>';
            $xml .= '<UnitPrice>';
            $xml .= '<Currency>' . $commodity['UnitPrice']['Currency'] . '</Currency>';
            $xml .= '<Amount>' . $commodity['UnitPrice']['Amount'] . '</Amount>';
            $xml .= '</UnitPrice>';
            $xml .= '<CustomsValue>';
            $xml .= '<Currency>' . $commodity['CustomsValue']['Currency'] . '</Currency>';
            $xml .= '<Amount>' . $commodity['CustomsValue']['Amount'] . '</Amount>';
            $xml .= '</CustomsValue>';
            $xml .= '</Commodities>';
        }

        $xml .= '<ExportDetail>';
        $xml .= '<B13AFilingOption>' . $inputJSON['customerClearanceDetail']['ExportDetail']['B13AFilingOption'] . '</B13AFilingOption>';
        $xml .= '</ExportDetail>';
        $xml .= '</CustomsClearanceDetail>';
        $xml .= '<LabelSpecification>';
        $xml .= '<LabelFormatType>' . $inputJSON['labelSpecification']['LabelFormatType'] . '</LabelFormatType>';
        $xml .= '<ImageType>' . $inputJSON['labelSpecification']['ImageType'] . '</ImageType>';
        $xml .= '<LabelStockType>' . $inputJSON['labelSpecification']['LabelStockType'] . '</LabelStockType>';
        $xml .= '</LabelSpecification>';
        $xml .= '<PackageCount>1</PackageCount>';

        $xml .= '<RequestedPackageLineItems>';
        $xml .= '<SequenceNumber>' . $inputJSON['packageLineItem']['SequenceNumber'] . '</SequenceNumber>';
        $xml .= '<GroupPackageCount>' . $inputJSON['packageLineItem']['GroupPackageCount'] . '</GroupPackageCount>';
        $xml .= '<InsuredValue>';
        $xml .= '<Currency>' . $inputJSON['packageLineItem']['InsuredValue']['Currency'] . '</Currency>';
        $xml .= '<Amount>' . $inputJSON['packageLineItem']['InsuredValue']['Amount'] . '</Amount>';
        $xml .= '</InsuredValue>';
        $xml .= '<Weight>';
        $xml .= '<Units>' . $inputJSON['packageLineItem']['Weight']['Units'] . '</Units>';
        $xml .= '<Value>' . $inputJSON['packageLineItem']['Weight']['Value'] . '</Value>';
        $xml .= '</Weight>';

        if ($inputJSON['RequestedShipment']['PackagingType'] == 'YOUR_PACKAGING') {
            $xml .= '<Dimensions>';
            $xml .= '<Length>' . $inputJSON['packageLineItem']['Dimensions']['Length'] . '</Length>';
            $xml .= '<Width>' . $inputJSON['packageLineItem']['Dimensions']['Width'] . '</Width>';
            $xml .= '<Height>' . $inputJSON['packageLineItem']['Dimensions']['Height'] . '</Height>';
            $xml .= '<Units>' . $inputJSON['packageLineItem']['Dimensions']['Units'] . '</Units>';
            $xml .= '</Dimensions>';
        }

        $xml .= '<CustomerReferences>';
        $xml .= '<CustomerReferenceType>CUSTOMER_REFERENCE</CustomerReferenceType>';
        $xml .= '<Value>' . Auth::user()->name . '/' . $inputJSON['SME_SO'] . '</Value>';
        $xml .= '</CustomerReferences>';
        $xml .= '</RequestedPackageLineItems>';
        $xml .= '</RequestedShipment>';
        $xml .= '</ProcessShipmentRequest>';
        $xml .= '</SOAP-ENV:Body>';
        $xml .= '</SOAP-ENV:Envelope>';

        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $xml);

//        dd($xml);

        return $this;
    }
}
