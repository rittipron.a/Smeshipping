<?php

namespace App\Classes\API;

class Insightly
{
    protected $token = null;
    protected $baseURL = 'https://api.insightly.com/v3.1';
    protected $url = null;
    protected $headers = array();
    protected $params = array();
    protected $curl = null;

    public function __construct()
    {
        $this->token = implode(" ", ["Basic", base64_encode(env('INSIGHTLY_KEY'))]);
        array_push($this->headers, implode(" ", ["Authorization:", $this->token]));

        $this->curl = curl_init();
    }

    public function contact()
    {
        $this->url = implode('/', [$this->baseURL, 'Contact']);
    }

    public function opportunities()
    {
        $this->url = implode('/', [$this->baseURL, 'Opportunities']);
    }

    public function task()
    {
        $this->url = implode('/', [$this->baseURL, 'Task']);
    }

    public function params(array $params)
    {
        $this->params = $params;
    }

    protected function init()
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => $this->headers
        ));
    }

    public function get()
    {
        $this->init();

        curl_setopt_array($this->curl, array(
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($this->curl);
        $this->close();

        return collect(json_decode((string) $response, true));
    }

    public function post()
    {
        array_push($this->headers, implode(" ", ["Content-Type:", "application/json"]));

        $this->init();

        curl_setopt_array($this->curl, array(
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode(collect($this->params)->toArray())
        ));

        $response = curl_exec($this->curl);
        $this->close();

        return collect(json_decode((string) $response, true));
    }

    protected function close()
    {
        curl_close($this->curl);
    }
}
