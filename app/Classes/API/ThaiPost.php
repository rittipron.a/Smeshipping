<?php

namespace App\Classes\API;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Mpdf\Mpdf;

class ThaiPost
{
    protected $token = null;
    protected $baseURL = 'https://r_dservice.thailandpost.com/webservice';
    protected $url = null;
    protected $headers = array();
    protected $params = array();
    protected $curl = null;

    public function __construct()
    {
        $this->token = implode(" ", ["Basic", base64_encode('smeship:12345')]);
        array_push($this->headers, implode(" ", ["Authorization:", $this->token]));
        array_push($this->headers, implode(" ", ["Content-Type:", "application/json"]));

        $this->curl = curl_init();
    }

    public function order()
    {
        $this->url = implode('/', [$this->baseURL, 'addItem']);

        return $this;
    }

    public function params(array $params)
    {
        $this->params = $params;

        return $this;
    }

    protected function init()
    {
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER => $this->headers
        ));
    }

    public function get()
    {
        $this->init();

        curl_setopt_array($this->curl, array(
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($this->curl);
        $this->close();

        return collect(json_decode((string)$response, true));
    }

    public function post()
    {
        $this->init();

        curl_setopt_array($this->curl, array(
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode(collect($this->params)->toArray())
        ));

        $response = curl_exec($this->curl);
        $this->close();

        return collect(json_decode((string)$response, true));
    }

    protected function close()
    {
        curl_close($this->curl);
    }

    public function label($barCode)
    {
        collect($barCode)->each(function ($item, $key) {
            $this->url = implode('/', [$this->baseURL, 'LabelPDF?barcode=' . $item . '&urlLogo=' . 'https://smeshipping.com/images/smelogo.png']);
            $this->init();

            curl_setopt_array($this->curl, array(
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => json_encode(collect($this->params)->toArray())
            ));

            $response = curl_exec($this->curl);

            Storage::put($item . '.pdf', $response);
        });

        return $this->getLabelPDF(collect($barCode));
    }

    protected function getLabelPDF($barCode)
    {
        $mpdf = new Mpdf(['mode' => 'utf-8', 'format' => [195, 159]]);

        $barCode->each(function ($item, $key) use ($mpdf) {
            $mpdf->AddPage();
            $pagecount = $mpdf->SetSourceFile(storage_path('app/' . $item . '.pdf'));
            $tplId = $mpdf->ImportPage($pagecount);
            $mpdf->UseTemplate($tplId);
        });

        return $mpdf->Output();
    }
}
