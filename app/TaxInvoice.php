<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxInvoice extends Model
{
    protected $table = 'tax_invoice';

    protected $primaryKey = 'id';
}
