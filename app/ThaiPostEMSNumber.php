<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThaiPostEMSNumber extends Model
{
    protected $table = 'thaipost_ems_number';

    protected $fillable = ['number', 'used', 'manifest_id'];
}
