<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManifestDetail_box extends Model
{
    protected $table = 'app_manifest_box';

    public function manifestDetail()
    {
        return $this->belongsTo(ManifestDetail::class, 'shipment_id', 'id');
    }

}
