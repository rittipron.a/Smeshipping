<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackingError extends Model
{
    protected $table = 'tracking_error_message';

    protected $fillable = ['priority', 'message'];

    public function group()
    {
        return $this->belongsTo(TrackingErrorGroup::class, 'group_id', 'id');
    }

    public function errors()
    {
        return $this->hasMany(TrackingLog::class, 'error_id', 'id');
    }

}
