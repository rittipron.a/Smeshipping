@extends('layouts.customer_app')

@section('content')
<link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/select2/select2.min.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/css/style.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
    rel="stylesheet" />

<div class="row">
    <div class="col-md-6">
        <h2>สร้างพัสดุ</h2>
    </div>
    <div class="col-md-12">
        <div class="progress-steps">
            <div class="step active" id="step_1_progress">
                <div class="step-circle">
                </div>
                <span class="label">ที่อยู่ผู้ส่ง/ใบกำกับภาษี</span>
            </div>
            <div class="step" id="step_2_progress">
                <div class="step-circle">
                </div>
                <span class="label">ข้อมูลพัสดุ</span>
            </div>
            <div class="step" id="step_3_progress">
                <div class="step-circle">
                </div>
                <span class="label">ข้อมูลผู้รับ</span>
            </div>
            <div class="step" id="step_4_progress">
                <div class="step-circle">
                </div>
                <span class="label">ส่งพัสดุ</span>
            </div>
        </div>
    </div>
</div>
<div class="row" id="step_1">
    <form name="form_step_1" id="form_step_1" action="#">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="ribbon-2">
                        <div class="card-box ribbon-box">
                            <div class="ribbon ribbon-danger">ที่อยู่ผู้ส่ง</div>
                            @foreach ($address->where('pickup',1) as $item)
                            <input type="hidden" name="m_id" value="{{$item->customer_id}}" />
                            <div>
                                <p style="padding-bottom: 13px;">
                                    <b>{{$item->customer_name}}</b><br />
                                    <i class="mdi mdi-cellphone"></i>{{$item->phone}}<br />
                                    <i class="mdi mdi-map-marker-check"></i>{{$item->address}}, {{$item->tumbon}},
                                    {{$item->amphur}}, {{$item->province}} {{$item->postcode}}<br />
                                </p>
                                <button type="button"
                                    class="btn btn-danger btn-square btn-outline-dashed waves-effect waves-light"
                                    style="width: 100%;" data-toggle="modal" data-animation="bounce"
                                    data-target=".bs-example-modal-lg" onclick="$('#set_address_is').val('pickup')"><i
                                        class="mdi mdi-check-all mr-2"></i>เปลี่ยน</button>
                            </div>
                            @endforeach
                            @if($address->where('pickup',1)->count()==0)
                            <p style="padding-bottom: 13px;"> กรุณาใส่ข้อมูลที่อยู่ โดยคลิกปุ่ม
                                <b>จัดการที่อยู่</b></p>
                            <button type="button"
                                class="btn btn-danger btn-square btn-outline-dashed waves-effect waves-light"
                                style="width: 100%;" data-toggle="modal" data-animation="bounce"
                                data-target=".bs-example-modal-lg" onclick="$('#set_address_is').val('pickup')"><i
                                    class="mdi mdi-check-all mr-2"></i>จัดการที่อยู่</button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ribbon-2">
                        <div class="card-box ribbon-box">
                            <div class="ribbon ribbon-danger">ที่อยู่ใบกำกับภาษี</div>
                            @foreach ($address->where('invoice',1) as $item)
                            <div>
                                <p style="padding-bottom: 13px;">
                                    <b>{{$item->customer_name}}</b><br />
                                    <i class="mdi mdi-cellphone"></i>{{$item->phone}}<br />
                                    <i class="mdi mdi-map-marker-check"></i>{{$item->address}}, {{$item->tumbon}},
                                    {{$item->amphur}}, {{$item->province}} {{$item->postcode}}<br />
                                </p>
                                <button type="button"
                                    class="btn btn-danger btn-square btn-outline-dashed waves-effect waves-light"
                                    style="width: 100%;" data-toggle="modal" data-animation="bounce"
                                    data-target=".bs-example-modal-lg" onclick="$('#set_address_is').val('invoice')"><i
                                        class="mdi mdi-check-all mr-2"></i>เปลี่ยน</button>
                            </div>
                            @endforeach
                            @if($address->where('invoice',1)->count()==0)
                            <p style="padding-bottom: 13px;"> กรุณาใส่ข้อมูลที่อยู่ โดยคลิกปุ่ม
                                <b>จัดการที่อยู่</b></p>
                            <button type="button"
                                class="btn btn-danger btn-square btn-outline-dashed waves-effect waves-light"
                                style="width: 100%;" data-toggle="modal" data-animation="bounce"
                                data-target=".bs-example-modal-lg" onclick="$('#set_address_is').val('invoice')"><i
                                    class="mdi mdi-check-all mr-2"></i>จัดการที่อยู่</button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ribbon-2">
                        <div class="card-box ribbon-box">
                            <div class="ribbon ribbon-danger">ข้อมูลเข้ารับเพิ่มเติม</div>
                            <div class="row">
                                <div class="col-sm-4">วันที่รับ :</div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" placeholder="โปรดระบุวันที่"
                                        name="delivery_date" id="mdate" required="" data-dtp="dtp_Qyted"
                                        value="{{date('d/m/Y')}}">
                                </div>
                                <div class="col-sm-4">
                                    <input type="radio" name="time" value="พร้อมรับ" checked="">
                                    พร้อมรับ <br>
                                    <input type="radio" name="time" value="ระบุเวลา"> ระบุเวลา
                                </div>
                                <div class="col-sm-8">
                                    <select id="delivery_time" name="delivery_time" class="form-control" disabled=""
                                        required="">
                                        <option value="">เลือกเวลาที่รับ</option>
                                        <option value="08:00">08:00</option>
                                        <option value="08:30">08:30</option>
                                        <option value="09:00">09:00</option>
                                        <option value="09:30">09:30</option>
                                        <option value="10:00">10:00</option>
                                        <option value="10:30">10:30</option>
                                        <option value="11:00">11:00</option>
                                        <option value="11:30">11:30</option>
                                        <option value="12:00">12:00</option>
                                        <option value="12:30">12:30</option>
                                        <option value="13:00">13:00</option>
                                        <option value="13:30">13:30</option>
                                        <option value="14:00">14:00</option>
                                        <option value="14:30">14:30</option>
                                        <option value="15:00">15:00</option>
                                        <option value="15:30">15:30</option>
                                        <option value="16:00">16:00</option>
                                        <option value="16:30">16:30</option>
                                        <option value="17:00">17:00</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">คำสั่งถึงพนักงาน :</div>
                                <div class="col-sm-8">
                                    <textarea name="m_note" class="form-control" maxlength="255"></textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-danger btn-square btn-outline-dashed waves-effect waves-light"
                    style="width: 100%;"
                    {{$address->where('pickup',1)->count()==0||$address->where('invoice',1)->count()==0?'disabled':''}}>{{$address->where('pickup',1)->count()==0||$address->where('invoice',1)->count()==0?'กรุณากรอกข้อมูลก่อน':''}}เลือกบริการจัดส่ง
                    <i class="mdi mdi-arrow-right"></i></button>
            </div>
        </div>
    </form>
</div>
<br />
<div class="row" id="step_2">
    <form name="form_step_2" id="form_step_2" action="#">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="ribbon-2">
                        <div class="card-box ribbon-box">
                            <div class="ribbon ribbon-danger">ข้อมูลพัสดุ</div>
                            <div class="row mb-0">
                                <div class="col-md-12" style="padding-top:15px;">
                                    <div id="box_area">
                                        <div class="box">
                                            <div><i class="fas fa-box"></i> ข้อมูลกล่องที่
                                                <span class="box_number">1</span>
                                            </div>
                                            <div style="text-align:center">
                                                <div class="w-col-3">
                                                    <span> น้ำหนัก</span>
                                                    <input type="text" class="weight"
                                                        onkeypress="return isNumberKey(event)" placeholder="0  "
                                                        required onchange="cal_box()">
                                                    <span> kg</span>
                                                </div>
                                                <div class="w-col-3">
                                                    <span> ยาว</span>
                                                    <input type="text" class="width"
                                                        onkeypress="return isNumberKey(event)" placeholder="0  "
                                                        onchange="cal_box()">
                                                    <span> cm</span>
                                                </div>
                                                <div class="w-col-3">
                                                    <span> กว้าง</span>
                                                    <input type="text" class="length"
                                                        onkeypress="return isNumberKey(event)" placeholder="0  "
                                                        onchange="cal_box()">
                                                    <span> cm</span>
                                                </div>
                                                <div class="w-col-3">
                                                    <span> สูง</span>
                                                    <input type="text" class="height"
                                                        onkeypress="return isNumberKey(event)" placeholder="0  "
                                                        onchange="cal_box()">
                                                    <span> cm</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="volumnWeight" onchange="get_service()">
                                    <button type="button"
                                        class="btn btn-info btn-square btn-outline-dashed waves-effect waves-light"
                                        style="width: 100%;margin-top:10px;" onclick="addbox()"><i
                                            class="mdi mdi-plus-circle-outline"></i>
                                        เพิ่มกล่อง</button>
                                </div>

                                <div class="col-md-12" style="padding-top:15px;">
                                    <div><i class="fas fa-info-circle"></i> รายละเอียดพัสดุ</div>
                                    <div class="w-col-4">
                                        <span> ประเภทสิ่งของ</span>
                                        <input type="text" class="" id="sh_description" placeholder="ภาษาอังกฤษเท่านั้น"
                                            required onchange="get_service()">
                                    </div>
                                    <div class="w-col-4">
                                        <span> จำนวน (ชิ้น)</span>
                                        <input type="text" id="sh_quantity" onkeypress="return isNumberKey(event)"
                                            placeholder="0 " required onchange="get_service()">
                                    </div>
                                    <div class="w-col-4">
                                        <span> มูลค่ารวม</span>
                                        <input type="text" id="sh_declared" onkeypress="return isNumberKey(event)"
                                            placeholder="0 " required onchange="get_service()">
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding:unset">
                                <div class="col-md-3 title-topic"><i class="fas fa-dollar-sign"></i> สกุลเงิน
                                </div>
                                <div class="col-md-9" style="padding-top:6px">
                                    <select class="select2 form-control mb-3 custom-select" id="sh_currency"
                                        name="sh_currency" required>
                                        @foreach ($currency as $cu_item)
                                        <option value="{{$cu_item->code}}" {{$cu_item->code=='THB'?'selected':''}}>
                                            {{$cu_item->code}} ({{$cu_item->country}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 title-topic"><i class="fas fa-map-marker-alt"></i> ประเทศปลายทาง
                                </div>
                                <div class="col-md-9" style="padding-top:6px">
                                    <select class="select2 form-control mb-3 custom-select" id="sh_country"
                                        name="sh_country" required onchange="get_service()">
                                        @foreach ($country_all as $c_item)
                                        <option value="{{$c_item->country_name}}" code="{{$c_item->country_cd}}"
                                            iata="{{$c_item->country_cd}}">
                                            {{$c_item->country_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ribbon-2">
                        <div class="card-box ribbon-box">
                            <div class="ribbon ribbon-danger">เลือกบริการ</div>
                            <div class="mb-0" id="service_wait_data">กรุณากรอกข้อมูลพัสดุก่อนเลือกบริการ</div>
                            <div class="row service_select" id="service_select" style="display:none">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" id="submit_step2" disabled
                        class="btn btn-danger btn-square btn-outline-dashed waves-effect waves-light"
                        style="width: 100%;">ใส่ข้อมูลผู้รับ <i class="mdi mdi-arrow-right"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="row" id="step_3">
    <form name="form_step_3" id="form_step_3" action="#">
        <div class="col-md-12">
            <div class="ribbon-2">
                <div class="card-box ribbon-box">
                    <div class="ribbon ribbon-danger">ข้อมูลผู้รับ (ภาษาอังกฤษเท่านั้น)</div>
                    <div class="row">
                        <div class="col-sm-2 title-topic">Receiver:</div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" id="sh_recipient" name="recipient" class="form-control" required
                                    maxlength="35">
                                <span class="input-group-append">
                                    <button class="btn btn-danger" type="button" data-toggle="modal"
                                        data-animation="bounce" data-target=".recipient_list" id="recipient_list"><i
                                            class="mdi mdi-format-list-bulleted"></i> เลือกผู้รับ</button>
                                </span>
                            </div>
                            <input type="hidden" name="sh_id" id="sh_id" value="3">
                        </div>
                        <div class="col-sm-2 title-topic">Company:</div>
                        <div class="col-sm-4">
                            <input type="text" id="sh_company" name="sh_company" class="form-control sh-input"
                                placeholder="" maxlength="35">
                        </div>
                        <div class="col-sm-2 title-topic">E-Mail:</div>
                        <div class="col-sm-4">
                            <input type="email" id="sh_email" name="sh_email" class="form-control sh-input"
                                placeholder="">
                        </div>
                        <div class="col-sm-2 title-topic">Address1:</div>
                        <div class="col-sm-10">
                            <input type="text" id="sh_address1" name="sh_address1" class="form-control sh-input"
                                placeholder="" required="" maxlength="45">
                        </div>
                        <div class="col-sm-2 title-topic">Address2:</div>
                        <div class="col-sm-10">
                            <input type="text" id="sh_address2" name="sh_address2" class="form-control sh-input"
                                placeholder="" maxlength="45">
                        </div>
                        <div class="col-sm-2 title-topic">Address3:</div>
                        <div class="col-sm-10">
                            <input type="text" id="sh_address3" name="sh_address3" class="form-control sh-input"
                                placeholder="" maxlength="45">
                        </div>
                        <div class="col-sm-2 title-topic">PhoneNumber:</div>
                        <div class="col-sm-4">
                            <input type="text" id="sh_phone" name="sh_phone" class="form-control sh-input"
                                placeholder="" required="" maxlength="25">
                        </div>
                        <div class="col-sm-2 title-topic">Country:</div>
                        <div class="col-sm-4" style="padding-top:6px">
                            <input type="text" id="sh_show_country" name="sh_show_country" class="form-control sh-input"
                                placeholder="" value="AFGHANISTAN" disabled>
                        </div>
                        <div class="col-sm-2 title-topic">City:</div>
                        <div class="col-sm-4">
                            <input list="select_city" id="sh_city" name="sh_city" class="form-control sh-input"
                                placeholder="โปรดระบุเมือง" required="" autocomplete="new-password">
                            <datalist class="form-control" id="select_city" style="display:none">
                            </datalist>
                        </div>
                        <div class="col-sm-2 title-topic">Post Code:</div>
                        <div class="col-sm-4">
                            <input type="text" id="sh_postcode" name="sh_postcode" class="form-control sh-input"
                                placeholder="">
                            <span id="process_postcode" style="display:none">กำลังตรวจสอบ<div
                                    class="spinner-border spinner-border-sm" role="status"></div></span>
                        </div>
                        <div class="col-sm-2 title-topic">State Code:</div>
                        <div class="col-sm-4">
                            <input type="text" name="sh_state" id="sh_state" class="form-control sh-input">
                        </div>
                        <div class="col-sm-2 title-topic subrub_area">Subrub:</div>
                        <div class="col-sm-4 subrub_area">
                            <input list="select_subrub" id="sh_suburb" name="sh_suburb" class="form-control sh-input"
                                placeholder="โปรดระบุชานเมือง" autocomplete="new-password">
                            <datalist class="form-control" id="select_subrub" style="display:none">
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-danger btn-square btn-outline-dashed waves-effect waves-light"
                style="width: 100%;">สรุปข้อมูลพัสดุ <i class="mdi mdi-arrow-right"></i></button>
        </div>
    </form>
</div>
<div class="row" id="step_4">
    <form name="form_step_4" id="form_step_4" action="{{url('/app/booking')}}">
        <div class="col-md-12">
            <div class="row" id="sumary_area">
                <div class="col-md-3">
                    <div class="card client-card" style="height:299px;cursor: pointer;" onclick="addshipping()">
                        <div class="card-body text-center" style="margin-top:25%">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQDk82upVfyYXmSuaaDckllIVSCGMf8bzcHaiJ0GEjfA1hQdgbCsg"
                                alt="user" class="rounded-circle thumb-xl">
                            <h5 class="client-name" style="color:darkgrey">เพิ่มพัสดุใหม่</h5>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-danger btn-square btn-outline-dashed waves-effect waves-light"
                    style="width: 100%;">ยืนยันส่งพัสดุ <i class="mdi mdi-truck-check"></i></button>
            </div>
        </div>
    </form>
</div>
</div>

<!--  Modal content for the above example -->
<div class="modal fade recipient_list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">เลือกผู้รับ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="background-color: white;">
                <div class="form-group">
                    <label for="example-input2-group1"></label>
                    <div class="input-group">
                        <input type="text" id="find_recipient" name="find_recipient" class="form-control"
                            placeholder="ค้นหาจากชื่อ หรือ เบอร์โทร">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="mdi mdi-account-search-outline"></i></span>
                        </div>
                    </div>
                </div>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อผู้รับ</th>
                            <th>ประเทศปลายทาง</th>
                            <th>เบอร์ติดต่อ</th>
                        </tr>
                    </thead>
                    <tbody id="recipient_area">

                    </tbody>
                </table>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->
<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">เลือกที่อยู่</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="background-color: white;">
                <div class="col-sm-12" id="add_address_area" style="display:{{$address->count()==0?'block':'none'}}">
                    <div class="card">
                        <div class="card-body">
                            <form name="add_address_form" id="add_address_form" action="{{url('/app/add_address')}}"
                                method="POST">
                                @csrf
                                <input type="hidden" name="set_address_is" id="set_address_is" value="pickup">
                                <div class="row">
                                    <div class="col-sm-2">ชื่อ - สกุล:</div>
                                    <div class="col-sm-4">
                                        <input type="text" name="name" id="addr_name" class="form-control"
                                            placeholder="ชื่อ-สกุล" required="" autocomplete="off">
                                        <input type="hidden" name="customer_id" id="addr_customer_id"
                                            class="form-control" value="{{session('cus.id')}}">
                                        <input type="hidden" name="address_id" id="address_id" class="form-control"
                                            value="">
                                    </div>
                                    <div class="col-sm-2">หมายเลขโทรศัพท์:</div>
                                    <div class="col-sm-4">
                                        <input type="text" name="phone" id="addr_phone" class="form-control"
                                            placeholder="หมายเลขโทรศัพท์" required="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">ที่อยู่:</div>
                                    <div class="col-sm-4">
                                        <input type="text" name="address" id="addr_address" class="form-control"
                                            placeholder="ที่อยู่" required="" autocomplete="off">
                                    </div>
                                    <div class="col-sm-2">จังหวัด:</div>
                                    <div class="col-sm-4">
                                        <select name="province" id="province" class="form-control" required=""
                                            autocomplete="off">
                                            <option selected="">--กรุณาเลือกจังหวัด--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">อำเภอ:</div>
                                    <div class="col-sm-4">
                                        <select name="amphur" id="amphur" class="form-control" required=""
                                            autocomplete="off"></select>
                                    </div>
                                    <div class="col-sm-2">แขวง/ตำบล:</div>
                                    <div class="col-sm-4">

                                        <select name="tumbon" id="tumbon" class="form-control" required=""
                                            autocomplete="off"></select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">รหัสไปรษณีย์:</div>
                                    <div class="col-sm-4">
                                        <input type="text" id="postcode" name="postcode" class="form-control"
                                            placeholder="กรุณาระบุรหัสไปรษณีย์" required="">
                                    </div>
                                    <div class="col-ms-6">
                                        <button type="submit" class="btn btn-sm btn-primary">บันทึก</button>
                                        <button type="button" class="btn btn-sm btn-danger"
                                            onclick="troggle_add_address()">ยกเลิก</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--end card-body-->
                    </div>

                </div>
                <div id="address_list" style="display:{{$address->count()==0?'none':'block'}}">
                    <div class="row">
                        <div class="col-md-4">
                            <button type="button"
                                class="btn btn-danger btn-square btn-outline-dashed waves-effect waves-light"
                                style="width: 100%;" onclick="troggle_add_address()"><i
                                    class="mdi mdi-home"></i>เพิ่มที่อยู่ใหม่</button>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($address as $item)
                        <div class="col-md-4">
                            <div class="ribbon-2">
                                <div class="card-box ribbon-box">
                                    <div>
                                        <b>{{$item->customer_name}}</b><br />
                                        <i class="mdi mdi-cellphone"></i>{{$item->phone}}<br />
                                        <i class="mdi mdi-map-marker-check"></i>{{$item->address}},
                                        {{$item->tumbon}},
                                        {{$item->amphur}}, {{$item->province}} {{$item->postcode}}<br />
                                        <button type="button"
                                            class="btn btn-danger btn-square btn-outline-dashed waves-effect waves-light"
                                            style="width: 100%;" onclick="set_address({{$item->id}})"><i
                                                class="mdi mdi-check-all mr-2"></i>เลือก</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}"></script>
<script src="{{ asset('assets/pages/jquery.forms-advanced.js?1' )}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script src="{{ asset('assets/js/app_booking.js?1.1' )}}"></script>

@endsection