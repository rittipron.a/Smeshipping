<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SME SHIPPING @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('description')" />
    <meta name="author" content="Mannatthemes" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('/assets/images/favicon.png') }}">
    <!-- App css -->
    <link href="{{ asset('/assets/css/bootstrap_app.css?1') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/metisMenu.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/style_app.css?1') }}" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
</head>
<style>
    body,
    p {
        color: rgba(101, 98, 105, 1);
        font-family: 'Prompt', sans-serif;
        font-size: 16px;
    }

    .row {
        padding-bottom: 10px;
    }

    .textcenter {
        text-align: center !important;
    }

    .teblet_show {
        display: none;
    }

    .dropdown-item {
        color: #656269 !important;
        font-family: 'Prompt', sans-serif;
    }

    @media screen and (max-width: 600px) {
        .mobile_hide {
            display: none;
        }
    }

    @media screen and (max-width: 767px) {
        .teblet_hide {
            display: none;
        }

        .teblet_show {
            display: block;
        }
    }


    #c_cookie {
        position: fixed;
        width: 44%;
        border-radius: 5px;
        height: auto;
        background-color: #000000c4;
        color: white;
        padding-left: 16px;
        left: 50%;
        transform: translateX(-50%);
        top: 87%;
        font-size: 80%;
    }

    #c_cookie button {
        width: 70%;
        font-size: 80%;
    }

    .pointer {
        cursor: pointer;
    }

    .modal-body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
    }

    .center_cookie {
        margin: auto;
    }

    #footer_cookie {
        display: none;
    }

    @media screen and (max-width: 960px) {
        #c_cookie {
            position: fixed;
            width: 87%;
            border-radius: 5px;
            height: auto;
            background-color: #000000c4;
            color: white;
            padding-left: 16px;
            left: 52%;
            transform: translateX(-50%);
            top: 87%;
            font-size: 50%;
        }

        #c_cookie button {
            width: 100%;
            padding: 9px 0px 10px 0px;
        }

        .cookie_picup {
            font-size: 80%;
        }

        .center_cookie {
            margin: auto;
        }

        .pointer {
            cursor: pointer;
        }

        .modal-body {
            max-height: calc(100vh - 210px);
            overflow-y: auto;
        }
    }
    .topbar{
        position: absolute !important;
    }
</style>

<body class="enlarge-menu">
    <div id="loading" style="
        width: 90px;
        height: 95px;
        position: fixed;
        z-index: 99999;
        text-align: center;
        top: 50%;
        left: 50%;
        color: white;
        padding: 10px;
        background-color: #f31414eb;
        -webkit-box-shadow: 0px 10px 13px -7px #000000, 5px 5px 15px 5px rgba(0,0,0,0);
        box-shadow: 0px 10px 13px -7px #000000, 5px 5px 15px 5px rgba(0,0,0,0);
        border: 5px solid #00000000;
        border-radius: 5px 5px 5px 5px;
        ">
        <div class="spinner-border thumb-md text-primary" style="color:white !important " role="status"></div>
        Loading...
    </div>

    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            <a href="{{url('/app')}}" class="logo">
                <span>
                    <img src="{{ asset('/assets/images/logo.png') }}" alt="sme shipping logo" class="logo-app">
                </span>
            </a>
        </div>
        <!--end logo-->
        <!-- Navbar -->
        <nav class="navbar-custom" style="margin-left: 0px !important;">
            <ul class="list-unstyled topbar-nav float-right mb-0">
                <li class="hidden-sm">
                    <a class="nav-link dropdown-toggle waves-effect waves-light menu_link"
                        href="{{url('app/check_price')}}">
                        เช็คราคา
                    </a>
                </li>
                <li class="hidden-sm">
                    <a class="nav-link dropdown-toggle waves-effect waves-light menu_link"
                        href="{{url('app/booking')}}">
                        สร้างพัสดุ
                    </a>
                </li>
                @if(Session::get('cus.service_group')==8)
                <li class="hidden-sm">
                    <a class="nav-link dropdown-toggle waves-effect waves-light menu_link"
                        href="{{url('app/upload_booking')}}">
                        สร้างพัสดุโดย Excel
                    </a>
                </li>
                @endif
                <li class="hidden-sm">
                    <div class="nav-link dropdown-toggle">
                        <i class="mdi mdi-account"
                            style="font-size:20px"></i><b>{{explode(' ', Session::get('cus.firstname'), 1)[0]}}</b>
                    </div>
                </li>
                <li class="dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown"
                        href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{asset('assets/images/17199.jpg')}}" alt="profile-user" class="" />
                        <span class="ml-1 nav-user-name hidden-sm">
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{url('app/booking')}}">สร้างพัสดุ</a>
                        <div class="dropdown-divider"></div>
                        <!--<a class="dropdown-item" href="{{url('/app/read_temp')}}">การส่งของฉัน</a>
                        <div class="dropdown-divider"></div>-->
                        <a class="dropdown-item" href="{{url('app/check_price')}}">เช็คราคา</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{url('/app/history')}}">ประวัติการส่ง</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{url('/app/waitforpayment')}}">แจ้งการชำระเงิน</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{url('/app/profile')}}">ข้อมูลของฉัน</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{url('/tracking')}}">ติดตามพัสดุ</a>
                        <div class="dropdown-divider"></div>
                        @if(Session::get('cus.service_group')==9)
                        <a class="dropdown-item" href="{{url('/app/genQRdealer')}}">สร้าง QR Code</a>
                        <div class="dropdown-divider"></div>
                        @endif
                        <a class="dropdown-item" href="#" data-toggle="modal" data-animation="bounce"
                            data-target=".view_map">ที่ตั้งจุดบริการ</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ asset('app/logout') }}">
                            ออกจากระบบ</a>
                    </div>
                </li>
            </ul>
            <!--end topbar-nav-->
        </nav>
        <!-- end navbar-->
        
    </div>

    
    <!-- Top Bar End -->

    <div class="container" style="padding-top: 100px;">
        @yield('content')
    </div>


    <!-- <footer class="footer text-center" style="display: block !important; border-top: 0px solid;">
     <div id="google_translate_element" style="text-align: right; padding: 7px 7px 0px 0px;"></div>
        <div class="row"  style="padding-bottom: 10px;padding-top: 10px;display:none;" id="c_cookie">
            <div class="col-9 center_cookie">
                SME SHIPPING ใช้คุกกี้บนเว็บไซต์นี้ เพื่อใช้งานเว็บไซต์ต่อไปท่านได้ตกลงใช้งานคุกกี้ <br />
                อ่านรายละเอียดเพิ่มเติมได้จาก <a class="pointer" ata-toggle="modal" id="rd_condition"
                    style="color: #f1646c;">นโยบายความเป็นส่วนตัว</a>
            </div>
            <div class="col-3 center_cookie">
                <button type="button" class="btn btn-danger" id="cookie_picup"><span
                        class="cookie_picup">ตกลง</span></button>
            </div>
        </div>
    </footer> -->

    <!-- Modal -->
    <div class="modal fade" id="condition" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">นโยบายคุกกี้</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span>
                        เมื่อท่านได้เข้าสู่เว็บไซต์นี้
                        ข้อมูลที่เกี่ยวข้องกับการเข้าสู่เว็บไซต์ของท่านจะถูกเก็บเอาไว้ในรูปแบบของคุกกี้
                        โดยนโยบายคุกกี้นี้จะอธิบายถึงความหมาย การทำงาน วัตถุประสงค์
                        รวมถึงการลบและการปฏิเสธการเก็บคุกกี้เพื่อความเป็นส่วนตัวของท่าน
                        โดยการเข้าสู่เว็บไซต์นี้ถือว่าท่านได้อนุญาตให้เราใช้คุกกี้ตามนโยบายคุกกี้ที่มีรายละเอียดดังต่อไปนี้
                    </span><br /><br />

                    <span>คุกกี้คืออะไร</span><br />
                    <span>คุกกี้ คือ ไฟล์เล็ก ๆ เพื่อจัดเก็บข้อมูลโดยจะบันทึกลงไปในอุปกรณ์คอมพิวเตอร์ และ/หรือ
                        เครื่องมือสื่อสารที่เข้าใช้งานของท่าน เช่น แท็บเล็ต, สมาร์ทโฟน
                        ผ่านทางเว็บเบราว์เซอร์ในขณะที่ท่านเข้าสู่เว็บไซต์ของเรา
                        โดยคุกกี้จะไม่ก่อให้เกิดอันตรายต่ออุปกรณ์คอมพิวเตอร์ และ/หรือ เครื่องมือสื่อสารของท่าน
                        ในกรณีดังต่อไปนี้
                        ข้อมูลส่วนบุคคลของท่านอาจถูกจัดเก็บเพื่อใช้เพิ่มประสบการณ์การใช้งานบริการของเราทางออนไลน์
                        โดยจะจำเอกลักษณ์ของภาษาและปรับแต่งข้อมูลการใช้งานตามความต้องการของท่าน
                        โดยการเก็บข้อมูลนี้เพื่อเป็นการยืนยันคุณลักษณะเฉพาะตัว ข้อมูลความปลอดภัยของท่าน
                        รวมถึงสินค้าและบริการที่ท่านสนใจ
                        นอกจากนี้คุกกี้ยังถูกใช้เพื่อวัดปริมาณการเข้าใช้งานบริการทางออนไลน์
                        การปรับเปลี่ยนเนื้อหาตามการใช้งานของท่านทั้งในก่อนหน้าและปัจจุบัน
                        หรือเพื่อวัตถุประสงค์ในการโฆษณาและประชาสัมพันธ์</span><br /><br />

                    <span>เราใช้คุกกี้อย่างไร</span><br />
                    <span>
                        เราใช้คุกกี้เพื่อเพิ่มประสบการณ์และความพึงพอใจของท่าน
                        โดยจะทำให้เราเข้าใจลักษณะการใช้งานเว็บไซต์ของท่านได้เร็ว และทำให้เว็บไซต์ของเราเข้าถึงได้ง่าย
                        สะดวกยิ่งขึ้น บางกรณีเราจำเป็นต้องให้บุคคลที่สามดำเนินการ ซึ่งอาจจะต้องใช้
                        อินเตอร์เน็ตโปรโตคอลแอดเดรส (IP Address) และคุกกี้เพื่อวิเคราะห์ทางสถิติ
                        ตลอดจนเชื่อมโยงข้อมูล และประมวลผลตามวัตถุประสงค์ทางการตลาด
                    </span><br /><br />

                    <span>เราใช้คุกกี้อย่างไร</span><br />
                    <span>ท่านสามารถลบและปฏิเสธการเก็บคุกกี้ได้โดยศึกษาตามวิธีการที่ระบุในแต่ละเว็บเบราว์เซอร์ที่ท่านใช้งานอยู่ตามลิงก์ดังนี้
                        (ข้อมูล ณ วันที่ เปลี่ยนตามวันที่ขึ้น)</span><br /><br />

                    <span>
                        Chrome<br />
                        Chrome for android<br />
                        Chrome for ios<br />
                        Firefox<br />
                        Internet Explorer<br />
                        Microsoft Edge<br />
                        Safari<br />
                        Safari for ios<br />
                    </span><br />

                    <span>การเปลี่ยนแปลงนโยบายคุกกี้</span><br />
                    <span>
                        นโยบายคุกกี้นี้อาจมีการปรับปรุงแก้ไขตามโอกาสเพื่อให้เป็นไปตามกฎระเบียบ
                        ดังนั้นเราขอแนะนำให้ท่านตรวจสอบให้แน่ใจว่าท่านได้เข้าใจการเปลี่ยนแปลงตามข้อกำหนดดังกล่าว
                    </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal -->

    <!-- jQuery   <script src="{{ asset('/assets/js/jquery.min.js') }}"></script> -->

    <script src="{{ asset('/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/assets/js/metisMenu.min.js') }}"></script>
    <script src="{{ asset('/assets/js/waves.min.js') }}"></script>
    <script src="{{ asset('/assets/js/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ asset('/assets/plugins/tiny-editable/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('/assets/plugins/tiny-editable/numeric-input-example.js') }}"></script>
    <script src="{{ asset('/assets/plugins/tabledit/jquery.tabledit.js') }}"></script>
    <script src="{{ asset('/assets/pages/jquery.tabledit.init.js') }}"></script>
    <!-- App js -->
    <script src="{{ asset('/assets/js/app.js?1') }}"></script>
    <script>
        $(document).ready(function () {
            if (document.cookie.indexOf("picup_cookie=") >= 0) {
                // They've been here before.
                console.log("hello again");
                $("#footer_cookie").hide();
            }else{
                $("#footer_cookie").show();
            }
        });
        $("#cookie_picup").click(function (){
           $("#footer_cookie").hide();
            // set a new cookie
            expiry = new Date();
            expiry.setTime(expiry.getTime()+((525600)*60*1000)); // 1 year

            // Date()'s toGMTSting() method will format the date correctly for a cookie
            document.cookie = "picup_cookie=ยอมรับการเก็บคุ้กกี้; expires=" + expiry.toGMTString();
            console.log("this is your first time");

        });
        $("#rd_condition").click(function (){
           $("#condition").modal('show');
        });

        $('#loading').hide();
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        var islinkactive = $('.menu-body').find('.active').length;
        if(islinkactive<=0){
            $('#m_default').click();
        }
        //เช็คว่าไม่ได้ล็อกเอาท์ไปแล้ว จากแท็บอื่น
        bodyOnload();
        function bodyOnload()
        {
             $.ajax({
                    type: "GET",
                    url: "{{url('/app/check_session_member')}}",
                    success: function(result){
                        if(result=='false'){
                            location.reload();
                        }
                    }
                });
            setTimeout("doLoop();",10000);
        }

        function doLoop()
        {
            bodyOnload();
        }
            
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'th'}, 'google_translate_element');
        }

        $("#onloadPopup").modal("show");
   </script>
   <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    <!--  Modal content for the above example -->
    <div class="modal fade view_map" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">แผนที่จุดบริการ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="background-color: white;text-align:center">
                    <img src="{{ asset('/assets/images/sme_address.jpg') }}" width="80%" /><iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.890475644994!2d100.47541231534696!3d13.72508020160875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2985a11c5db49%3A0xeb036104d5cc1b6d!2sSME%20Shipping%20Co.%2C%20Ltd!5e0!3m2!1sth!2sth!4v1580286391249!5m2!1sth!2sth"
                        width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div> <!-- /.modal -->
</body>

</html>