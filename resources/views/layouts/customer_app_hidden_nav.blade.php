<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SME SHIPPING @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('description')" />
    <meta name="author" content="Mannatthemes" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('/assets/images/favicon.png') }}">
    <!-- App css -->
    <link href="{{ asset('/assets/css/bootstrap_app.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/metisMenu.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/style_app.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">
</head>
<style>
    body,
    p {
        color: rgba(101, 98, 105, 1);
        font-family: 'Prompt', sans-serif;
        font-size: 16px;
    }

    .row {
        padding-bottom: 10px;
    }

    #topbar_title {
        font-size: 20px;
        color: black;
        font-family: 'Prompt', sans-serif;
        position: absolute;
        z-index: 1;
        top: 20px;
        right: 30px;
    }

    @media screen and (max-width: 600px) {
        .mobile_hide {
            display: none;
        }
    }

    #c_cookie {
        position: fixed;
        width: 44%;
        border-radius: 5px;
        height: auto;
        background-color: #000000c4;
        color: white;
        padding-left: 16px;
        left: 50%;
        transform: translateX(-50%);
        top: 87%;
        font-size: 80%;
    }

    #c_cookie button {
        width: 70%;
        font-size: 80%;
    }

    .pointer {
        cursor: pointer;
    }

    .modal-body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
    }

    .center_cookie {
        margin: auto;
    }

    #footer_cookie {
        /* display: none; */
    }

    @media screen and (max-width: 960px) {
        #c_cookie {
            position: fixed;
            width: 87%;
            border-radius: 5px;
            height: auto;
            background-color: #000000c4;
            color: white;
            padding-left: 16px;
            left: 52%;
            transform: translateX(-50%);
            top: 87%;
            font-size: 50%;
        }

        #c_cookie button {
            width: 100%;
            padding: 9px 0px 10px 0px;
        }

        .cookie_picup {
            font-size: 80%;
        }

        .center_cookie {
            margin: auto;
        }

        .pointer {
            cursor: pointer;
        }

        .modal-body {
            max-height: calc(100vh - 210px);
            overflow-y: auto;
        }
    }
    .topbar{
        position: absolute;
    }
    .goog-te-gadget,
    .goog-te-combo{
        margin: 0px 0px; 
    }
</style>

<body class="enlarge-menu">
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            <a href="https://smeshipping.com/" class="logo">
                <span>
                    <img src="{{ asset('/assets/images/logo.png') }}" alt="sme shipping logo" class="logo-app">
                </span>
            </a>
        </div>
        <div id="topbar_title" class="mobile_hide">ติดต่อเรา 02-105-7777</div>
        <!--end logo-->
        <!-- Navbar -->
        <nav class="navbar-custom" style="margin-left: 0px !important;">
        
        </nav>
        <!-- end navbar-->
        <div id="google_translate_element" style="text-align: right; padding: 7px 7px 0px 0px;"></div>
    </div>
    <!-- Top Bar End -->

    <div class="container" style="padding-top: 100px;">
        @yield('content')
    </div>
    <footer class="footer text-center" style="border-top: 0px solid;width: 0px;">

        <div class="row" style="padding-bottom: 10px;padding-top: 10px;display:none;"  id="c_cookie">
            <div class="col-9 center_cookie">
                SME SHIPPING ใช้คุกกี้บนเว็บไซต์นี้ เพื่อใช้งานเว็บไซต์ต่อไปท่านได้ตกลงใช้งานคุกกี้ <br />
                อ่านรายละเอียดเพิ่มเติมได้จาก <a class="pointer" ata-toggle="modal" id="rd_condition"
                    style="color: #f1646c;">นโยบายความเป็นส่วนตัว</a>
            </div>
            <div class="col-3 center_cookie">
                <button type="button" class="btn btn-danger" id="cookie_picup"><span
                        class="cookie_picup">ตกลง</span></button>
            </div>
        </div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="condition" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">นโยบายคุกกี้</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span>
                        เมื่อท่านได้เข้าสู่เว็บไซต์นี้
                        ข้อมูลที่เกี่ยวข้องกับการเข้าสู่เว็บไซต์ของท่านจะถูกเก็บเอาไว้ในรูปแบบของคุกกี้
                        โดยนโยบายคุกกี้นี้จะอธิบายถึงความหมาย การทำงาน วัตถุประสงค์
                        รวมถึงการลบและการปฏิเสธการเก็บคุกกี้เพื่อความเป็นส่วนตัวของท่าน
                        โดยการเข้าสู่เว็บไซต์นี้ถือว่าท่านได้อนุญาตให้เราใช้คุกกี้ตามนโยบายคุกกี้ที่มีรายละเอียดดังต่อไปนี้
                    </span><br /><br />

                    <span>คุกกี้คืออะไร</span><br />
                    <span>คุกกี้ คือ ไฟล์เล็ก ๆ เพื่อจัดเก็บข้อมูลโดยจะบันทึกลงไปในอุปกรณ์คอมพิวเตอร์ และ/หรือ
                        เครื่องมือสื่อสารที่เข้าใช้งานของท่าน เช่น แท็บเล็ต, สมาร์ทโฟน
                        ผ่านทางเว็บเบราว์เซอร์ในขณะที่ท่านเข้าสู่เว็บไซต์ของเรา
                        โดยคุกกี้จะไม่ก่อให้เกิดอันตรายต่ออุปกรณ์คอมพิวเตอร์ และ/หรือ เครื่องมือสื่อสารของท่าน
                        ในกรณีดังต่อไปนี้
                        ข้อมูลส่วนบุคคลของท่านอาจถูกจัดเก็บเพื่อใช้เพิ่มประสบการณ์การใช้งานบริการของเราทางออนไลน์
                        โดยจะจำเอกลักษณ์ของภาษาและปรับแต่งข้อมูลการใช้งานตามความต้องการของท่าน
                        โดยการเก็บข้อมูลนี้เพื่อเป็นการยืนยันคุณลักษณะเฉพาะตัว ข้อมูลความปลอดภัยของท่าน
                        รวมถึงสินค้าและบริการที่ท่านสนใจ
                        นอกจากนี้คุกกี้ยังถูกใช้เพื่อวัดปริมาณการเข้าใช้งานบริการทางออนไลน์
                        การปรับเปลี่ยนเนื้อหาตามการใช้งานของท่านทั้งในก่อนหน้าและปัจจุบัน
                        หรือเพื่อวัตถุประสงค์ในการโฆษณาและประชาสัมพันธ์</span><br /><br />

                    <span>เราใช้คุกกี้อย่างไร</span><br />
                    <span>
                        เราใช้คุกกี้เพื่อเพิ่มประสบการณ์และความพึงพอใจของท่าน
                        โดยจะทำให้เราเข้าใจลักษณะการใช้งานเว็บไซต์ของท่านได้เร็ว และทำให้เว็บไซต์ของเราเข้าถึงได้ง่าย
                        สะดวกยิ่งขึ้น บางกรณีเราจำเป็นต้องให้บุคคลที่สามดำเนินการ ซึ่งอาจจะต้องใช้
                        อินเตอร์เน็ตโปรโตคอลแอดเดรส (IP Address) และคุกกี้เพื่อวิเคราะห์ทางสถิติ
                        ตลอดจนเชื่อมโยงข้อมูล และประมวลผลตามวัตถุประสงค์ทางการตลาด
                    </span><br /><br />

                    <span>เราใช้คุกกี้อย่างไร</span><br />
                    <span>ท่านสามารถลบและปฏิเสธการเก็บคุกกี้ได้โดยศึกษาตามวิธีการที่ระบุในแต่ละเว็บเบราว์เซอร์ที่ท่านใช้งานอยู่ตามลิงก์ดังนี้
                        (ข้อมูล ณ วันที่ เปลี่ยนตามวันที่ขึ้น)</span><br /><br />

                    <span>
                        Chrome<br />
                        Chrome for android<br />
                        Chrome for ios<br />
                        Firefox<br />
                        Internet Explorer<br />
                        Microsoft Edge<br />
                        Safari<br />
                        Safari for ios<br />
                    </span><br />

                    <span>การเปลี่ยนแปลงนโยบายคุกกี้</span><br />
                    <span>
                        นโยบายคุกกี้นี้อาจมีการปรับปรุงแก้ไขตามโอกาสเพื่อให้เป็นไปตามกฎระเบียบ
                        ดังนั้นเราขอแนะนำให้ท่านตรวจสอบให้แน่ใจว่าท่านได้เข้าใจการเปลี่ยนแปลงตามข้อกำหนดดังกล่าว
                    </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal -->

    <!-- jQuery  -->
    <script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/assets/js/metisMenu.min.js') }}"></script>
    <script src="{{ asset('/assets/js/waves.min.js') }}"></script>
    <script src="{{ asset('/assets/js/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ asset('/assets/plugins/tiny-editable/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('/assets/plugins/tiny-editable/numeric-input-example.js') }}"></script>
    <script src="{{ asset('/assets/plugins/tabledit/jquery.tabledit.js') }}"></script>
    <script src="{{ asset('/assets/pages/jquery.tabledit.init.js') }}"></script>
    <!-- App js -->
    <script src="{{ asset('/assets/js/app.js') }}"></script>
    <script>
        $(document).ready(function () {
            if (document.cookie.indexOf("picup_cookie=") >= 0) {
                // They've been here before.
                console.log("hello again");
                $("#footer_cookie").hide();
            }else{
                $("#footer_cookie").show();
            }
        });
        $("#cookie_picup").click(function (){
           $("#footer_cookie").hide();
            // set a new cookie
            expiry = new Date();
            expiry.setTime(expiry.getTime()+((525600)*60*1000)); // 1 year

            // Date()'s toGMTSting() method will format the date correctly for a cookie
            document.cookie = "picup_cookie=ยอมรับการเก็บคุ้กกี้; expires=" + expiry.toGMTString();
            console.log("this is your first time");

        });
        $("#rd_condition").click(function (){
           $("#condition").modal('show');
        });

        $('#loading').hide();
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        var islinkactive = $('.menu-body').find('.active').length;
        if(islinkactive<=0){
            $('#m_default').click();
        }
        $("#onloadPopup").modal("show");

        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'th'}, 'google_translate_element');
        }
    </script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>

</html>