<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SMEShipping Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A premium admin dashboard template by Mannatthemes" name="description" />
    <meta content="Mannatthemes" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('/assets/images/favicon.png') }}">
    <!-- App css -->
    <link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/metisMenu.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/style.css?2') }}" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="loading" class="no-print">
        <div class="spinner-border thumb-md text-primary no-print" style="color:white !important " role="status"></div>
        Loading...
    </div>
    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="{{ asset('/admin/service') }}" class="logo">
                <span>
                    <img src="{{ asset('/assets/images/logo-sm.png') }}" alt="logo-small" class="logo-sm">
                </span>
                <span>
                    <img src="{{ asset('/assets/images/logo-dark.png') }}" alt="logo-large" class="logo-lg">
                </span>
            </a>
        </div>
        <!--end logo-->
        <!-- Navbar -->
        <nav class="navbar-custom">
            <ul class="list-unstyled topbar-nav float-right mb-0">
                <li class="dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown"
                        href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <img src="{{asset('assets/images/17198.jpg')}}" alt="profile-user" class="rounded-circle" />
                        <span class="ml-1 nav-user-name hidden-sm">{{Auth::user()->name}} <i
                                class="mdi mdi-chevron-down"></i> </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ asset('/logout') }}"><i
                                class="dripicons-exit text-muted mr-2"></i>
                            Logout</a>
                    </div>
                </li>
            </ul>
            <!--end topbar-nav-->

            <ul class="list-unstyled topbar-nav mb-0">
                <li>
                    <button class="button-menu-mobile nav-link waves-effect waves-light">
                        <i class="dripicons-menu nav-icon"></i>
                    </button>
                </li>
            </ul>
        </nav>
        <!-- end navbar-->
    </div>
    <!-- Top Bar End -->

    <div class="page-wrapper">
        <!-- Left Sidenav -->
        <div class="left-sidenav">
            <div class="main-icon-menu">
                <nav class="nav">
                    @if(Auth::user()->position=='Administrator')
                    <a href="#dashboard" class="nav-link" id="m_Dashboard" data-toggle="tooltip-custom"
                        data-placement="top" title="" data-original-title="Dashboard">
                        <svg class="nav-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                            style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <path class="svg-primary"
                                    d="M184,448h48c4.4,0,8-3.6,8-8V72c0-4.4-3.6-8-8-8h-48c-4.4,0-8,3.6-8,8v368C176,444.4,179.6,448,184,448z">
                                </path>
                                <path class="svg-primary"
                                    d="M88,448H136c4.4,0,8-3.6,8-8V296c0-4.4-3.6-8-8-8H88c-4.4,0-8,3.6-8,8V440C80,444.4,83.6,448,88,448z">
                                </path>
                                <path class="svg-primary" d="M280.1,448h47.8c4.5,0,8.1-3.6,8.1-8.1V232.1c0-4.5-3.6-8.1-8.1-8.1h-47.8c-4.5,0-8.1,3.6-8.1,8.1v207.8
                                    C272,444.4,275.6,448,280.1,448z"></path>
                                <path class="svg-primary" d="M368,136.1v303.8c0,4.5,3.6,8.1,8.1,8.1h47.8c4.5,0,8.1-3.6,8.1-8.1V136.1c0-4.5-3.6-8.1-8.1-8.1h-47.8
                                    C371.6,128,368,131.6,368,136.1z"></path>
                            </g>
                        </svg>
                    </a>
                    @endif
                    @if(Auth::user()->position!='Messenger')
                    <a href="#MetricaCRM" class="nav-link" id="m_default" data-toggle="tooltip-custom"
                        data-placement="top" title="" data-original-title="Manifests">
                        <svg class="nav-svg" version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                            style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <g>
                                    <path class="svg-primary"
                                        d="M276,68.1v219c0,3.7-2.5,6.8-6,7.7L81.1,343.4c-2.3,0.6-3.6,3.1-2.7,5.4C109.1,426,184.9,480.6,273.2,480
                                                C387.8,479.3,480,386.5,480,272c0-112.1-88.6-203.5-199.8-207.8C277.9,64.1,276,65.9,276,68.1z" />
                                </g>
                                <path class="svg-primary" d="M32,239.3c0,0,0.2,48.8,15.2,81.1c0.8,1.8,2.8,2.7,4.6,2.2l193.8-49.7c3.5-0.9,6.4-4.6,6.4-8.2V36c0-2.2-1.8-4-4-4
                                            C91,33.9,32,149,32,239.3z" />
                            </g>
                        </svg>
                    </a>
                    @endif
                    <a href="#check" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                        data-original-title="Check">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path class="svg-primary"
                                d="M168.531 215.469l-29.864 29.864 96 96L448 128l-29.864-29.864-183.469 182.395-66.136-65.062zm236.802 189.864H106.667V106.667H320V64H106.667C83.198 64 64 83.198 64 106.667v298.666C64 428.802 83.198 448 106.667 448h298.666C428.802 448 448 428.802 448 405.333V234.667h-42.667v170.666z">
                            </path>
                        </svg>
                    </a>
                    <!--end MetricaCRM-->
                    <a href="#quotation" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                        data-original-title="Quotation">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path class="svg-primary"
                                d="M410.5 279.2c-5-11.5-12.7-21.6-28.1-30.1-8.2-4.5-16.1-7.8-25.4-10 5.4-2.5 10-5.4 16.3-11 7.5-6.6 13.1-15.7 15.6-23.3 2.6-7.5 4.1-18 3.5-28.2-1.1-16.8-4.4-33.1-13.2-44.8-8.8-11.7-21.2-20.7-37.6-27-12.6-4.8-25.5-7.8-45.5-8.9V32h-40v64h-32V32h-41v64H96v48h27.9c8.7 0 14.6.8 17.6 2.3 3.1 1.5 5.3 3.5 6.5 6 1.3 2.5 1.9 8.4 1.9 17.5V343c0 9-.6 14.8-1.9 17.4-1.3 2.6-2 4.9-5.1 6.3-3.1 1.4-3.2 1.3-11.8 1.3h-26.4L96 416h87v64h41v-64h32v64h40v-64.4c26-1.3 44.5-4.7 59.4-10.3 19.3-7.2 34.1-17.7 44.7-31.5 10.6-13.8 14.9-34.9 15.8-51.2.7-14.5-.9-33.2-5.4-43.4zM224 150h32v74h-32v-74zm0 212v-90h32v90h-32zm72-208.1c6 2.5 9.9 7.5 13.8 12.7 4.3 5.7 6.5 13.3 6.5 21.4 0 7.8-2.9 14.5-7.5 20.5-3.8 4.9-6.8 8.3-12.8 11.1v-65.7zm28.8 186.7c-7.8 6.9-12.3 10.1-22.1 13.8-2 .8-4.7 1.4-6.7 1.9v-82.8c5 .8 7.6 1.8 11.3 3.4 7.8 3.3 15.2 6.9 19.8 13.2 4.6 6.3 8 15.6 8 24.7 0 10.9-2.8 19.2-10.3 25.8z">
                            </path>
                        </svg>
                    </a>

                    <!-- <a href="#commercialinvoice" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                        data-original-title="commercialinvoice">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path class="svg-primary"
                                d="M410.5 279.2c-5-11.5-12.7-21.6-28.1-30.1-8.2-4.5-16.1-7.8-25.4-10 5.4-2.5 10-5.4 16.3-11 7.5-6.6 13.1-15.7 15.6-23.3 2.6-7.5 4.1-18 3.5-28.2-1.1-16.8-4.4-33.1-13.2-44.8-8.8-11.7-21.2-20.7-37.6-27-12.6-4.8-25.5-7.8-45.5-8.9V32h-40v64h-32V32h-41v64H96v48h27.9c8.7 0 14.6.8 17.6 2.3 3.1 1.5 5.3 3.5 6.5 6 1.3 2.5 1.9 8.4 1.9 17.5V343c0 9-.6 14.8-1.9 17.4-1.3 2.6-2 4.9-5.1 6.3-3.1 1.4-3.2 1.3-11.8 1.3h-26.4L96 416h87v64h41v-64h32v64h40v-64.4c26-1.3 44.5-4.7 59.4-10.3 19.3-7.2 34.1-17.7 44.7-31.5 10.6-13.8 14.9-34.9 15.8-51.2.7-14.5-.9-33.2-5.4-43.4zM224 150h32v74h-32v-74zm0 212v-90h32v90h-32zm72-208.1c6 2.5 9.9 7.5 13.8 12.7 4.3 5.7 6.5 13.3 6.5 21.4 0 7.8-2.9 14.5-7.5 20.5-3.8 4.9-6.8 8.3-12.8 11.1v-65.7zm28.8 186.7c-7.8 6.9-12.3 10.1-22.1 13.8-2 .8-4.7 1.4-6.7 1.9v-82.8c5 .8 7.6 1.8 11.3 3.4 7.8 3.3 15.2 6.9 19.8 13.2 4.6 6.3 8 15.6 8 24.7 0 10.9-2.8 19.2-10.3 25.8z">
                            </path>
                        </svg>
                    </a> -->

                    <a href="#check_shipment_log" class="nav-link" data-toggle="tooltip-custom" data-placement="top"
                        title="" data-original-title="Tracing">
                        <svg class="nav-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path class="svg-primary"
                                d="M256 32C132.288 32 32 132.288 32 256s100.288 224 224 224 224-100.288 224-224S379.712 32 256 32zm135.765 359.765C355.5 428.028 307.285 448 256 448s-99.5-19.972-135.765-56.235C83.972 355.5 64 307.285 64 256s19.972-99.5 56.235-135.765C156.5 83.972 204.715 64 256 64s99.5 19.972 135.765 56.235C428.028 156.5 448 204.715 448 256s-19.972 99.5-56.235 135.765z">
                            </path>
                            <path
                                d="M200.043 106.067c-40.631 15.171-73.434 46.382-90.717 85.933H256l-55.957-85.933zM412.797 288A160.723 160.723 0 0 0 416 256c0-36.624-12.314-70.367-33.016-97.334L311 288h101.797zM359.973 134.395C332.007 110.461 295.694 96 256 96c-7.966 0-15.794.591-23.448 1.715L310.852 224l49.121-89.605zM99.204 224A160.65 160.65 0 0 0 96 256c0 36.639 12.324 70.394 33.041 97.366L201 224H99.204zM311.959 405.932c40.631-15.171 73.433-46.382 90.715-85.932H256l55.959 85.932zM152.046 377.621C180.009 401.545 216.314 416 256 416c7.969 0 15.799-.592 23.456-1.716L201.164 288l-49.118 89.621z">
                            </path>
                        </svg>
                    </a>

                    @if(Auth::user()->position=='Administrator')
                    <a href="#setting" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title=""
                        data-original-title="Settings">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path class="svg-primary"
                                d="M413.967 276.8c1.06-6.235 1.06-13.518 1.06-20.8s-1.06-13.518-1.06-20.8l44.667-34.318c4.26-3.118 5.319-8.317 2.13-13.518L418.215 115.6c-2.129-4.164-8.507-6.235-12.767-4.164l-53.186 20.801c-10.638-8.318-23.394-15.601-36.16-20.801l-7.448-55.117c-1.06-4.154-5.319-8.318-10.638-8.318h-85.098c-5.318 0-9.577 4.164-10.637 8.318l-8.508 55.117c-12.767 5.2-24.464 12.482-36.171 20.801l-53.186-20.801c-5.319-2.071-10.638 0-12.767 4.164L49.1 187.365c-2.119 4.153-1.061 10.399 2.129 13.518L96.97 235.2c0 7.282-1.06 13.518-1.06 20.8s1.06 13.518 1.06 20.8l-44.668 34.318c-4.26 3.118-5.318 8.317-2.13 13.518L92.721 396.4c2.13 4.164 8.508 6.235 12.767 4.164l53.187-20.801c10.637 8.318 23.394 15.601 36.16 20.801l8.508 55.117c1.069 5.2 5.318 8.318 10.637 8.318h85.098c5.319 0 9.578-4.164 10.638-8.318l8.518-55.117c12.757-5.2 24.464-12.482 36.16-20.801l53.187 20.801c5.318 2.071 10.637 0 12.767-4.164l42.549-71.765c2.129-4.153 1.06-10.399-2.13-13.518l-46.8-34.317zm-158.499 52c-41.489 0-74.46-32.235-74.46-72.8s32.971-72.8 74.46-72.8 74.461 32.235 74.461 72.8-32.972 72.8-74.461 72.8z">
                            </path>
                        </svg>
                    </a>
                    @endif

                    <a href="#MetricaAuthentication" class="nav-link" data-toggle="tooltip-custom" data-placement="top"
                        title="" data-original-title="User Management">
                        <svg class="nav-svg" version="1.1" id="Layer_5" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                            style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <path class="svg-primary"
                                    d="M256 256c52.805 0 96-43.201 96-96s-43.195-96-96-96-96 43.201-96 96 43.195 96 96 96zm0 48c-63.598 0-192 32.402-192 96v48h384v-48c0-63.598-128.402-96-192-96z">
                                </path>
                            </g>
                        </svg>
                    </a>
                    <!--end MetricaAuthentication-->

                </nav>
                <!--end nav-->
            </div>
            <!--end main-icon-menu-->

            <div class="main-menu-inner">
                <div class="menu-body slimscroll">
                    @if(Auth::user()->position=='Administrator')
                    <div id="setting" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Settings</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/currency') }}"><i
                                        class="mdi mdi-square-inc-cash"></i>Currency</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/country') }}"><i
                                        class="mdi mdi-earth"></i>Country</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/servicetype') }}"><i
                                        class="dripicons-view-list-large"></i>Service Type</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/service') }}"><i
                                        class="dripicons-article"></i>Service</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/servicegroup') }}"><i
                                        class="dripicons-article"></i>Service Group</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/discount') }}"><i
                                        class="mdi mdi-sale"></i>ส่วนลด</a></li>
                        </ul>
                    </div>
                    @endif
                    @if(Auth::user()->position=='Administrator')
                    <div id="dashboard" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Dashboard</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/dashboard/') }}"><i
                                        class="mdi mdi-file-table"></i>Today Report</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/dashboardall/') }}"><i
                                        class="mdi mdi-file-table"></i>All Report</a></li>
                        </ul>
                    </div>
                    @endif
                    <div id="check" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Check</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/messenger') }}"> <i
                                        class="mdi mdi-map-marker-check"></i>Messenger Check In</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/messenger_history') }}"> <i
                                        class="mdi mdi-map-marker-check"></i>Messenger History</a></li>
                            @if(Auth::user()->position!='Messenger')
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/messenger_search') }}"> <i
                                        class="mdi mdi-map-marker-check"></i>Messenger Search</a></li>
                            @endif
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/checkin') }}"> <i
                                        class="mdi mdi-home-map-marker"></i></i>Office Check In</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/checkout') }}"> <i
                                        class="mdi mdi-truck-check"></i>Check Out</a></li>
                        </ul>
                    </div><!-- end  Project-->
                    <div id="quotation" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Quotation</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/quotation') }}"><i
                                        class="dripicons-article"></i>Create Quotation</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/quotationlist') }}"><i
                                        class="dripicons-archive"></i>List Quotation</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/mainpickupcost') }}"><i
                                        class="dripicons-wallet"></i>เช็คราคา</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/introduce') }}"><i
                                        class="mdi mdi-account-card-details"></i>ส่งอีเมล และ SMS</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('admin/show_taxinvoice') }}"><i
                                        class="mdi mdi-account-card-details"></i>ใบกำกับภาษี</a></li>
                        </ul>
                    </div><!-- end  Project-->
                    <!-- <div id="commercialinvoice" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Commercial Invoice</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/commercial_invoice') }}"><i
                                        class="fas fa-file-invoice-dollar"></i>Commercial Invoice</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/commercial_invoicelist') }}"><i
                                        class="fas fa-list-alt"></i>List Commercial Invoice</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/manage_commercial') }}"><i
                                        class="fas fa-users"></i>Manage Commercial</a></li>
                        </ul>
                    </div> -->
                    <!-- end  Project-->
                    <div id="check_shipment_log" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">ชิปเม้นติดปัญหา</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="{{ url('/admin/log_tracking_all') }}"><i
                                        class="dripicons-article"></i>ปัญหาทั้งหมด</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ url('/admin/log_tracking') }}"><i
                                        class="dripicons-archive"></i>จัดการ Filter</a></li>
                             <li class="nav-item"><a class="nav-link" href="{{ url('/admin/log_tracking_mail') }}"><i
                                        class="dripicons-archive"></i>จัดการ อีเมล</a></li>
                        </ul>
                    </div><!-- end  Project-->

                    <div id="MetricaCRM" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">Manifests</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/request/create') }}">1.)
                                    สร้างงานใหม่</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/checkinbox') }}">2.)
                                    พัสดุรับเข้า (<span id="count_checkin">0</span>)</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/waitforsale') }}">3.)
                                    งานจากเว็บ (<span id="count_sale">0</span>)</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/inboxforlink') }}">4.)
                                    งานที่ส่งลิงค์ (<span id="count_checklink">0</span>)</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/request') }}">5.) รอลงงาน
                                    (<span id="count_inbox">0</span>)</a></li>
                            <!--<li class="nav-item"><a class="nav-link" href="{{ asset('/admin/hold') }}"><i
                                        class="mdi mdi-hand"></i>Hold (<span id="count_hold">0</span>)</a></li> -->
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/waitforpickup') }}">6.)
                                    กำลังไป (<span id="count_pickup">0</span>)</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/waitforcalculate') }}">7.)
                                    คิดราคา (<span id="count_calculate">0</span>)</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/waitforpayment') }}">8.)
                                    รอชำระ (<span id="count_payment">0</span>)</a></li>
                            <li class="nav-item"><a class="nav-link"
                                    href="{{ asset('/admin/check_payment_attachment') }}">9.)
                                    แนบหลักฐาน (<span id="count_payment_attach">0</span>)</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/waitforproof') }}">10.)
                                    จ่ายเงินแล้ว (<span id="count_proof">0</span>)</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/waitforrelease') }}">11.)
                                    รอปล่อย (<span id="count_release">0</span>)</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/cancel') }}">12.)
                                    ยกเลิกงาน (<span id="count_cancel">0</span>)</a></li>
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/search_history') }}">13.)
                                    งานทั้งหมด (<span id="count_all">0</span>)</a>
                            </li>

                        </ul>
                    </div><!-- end CRM -->
                    <div id="MetricaAuthentication" class="main-icon-menu-pane">
                        <div class="title-box">
                            <h6 class="menu-title">User Manage</h6>
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/customer') }}"><i
                                        class="dripicons-enter"></i>Customer</a></li>
                            @if(Auth::user()->position=='Administrator')
                            <li class="nav-item"><a class="nav-link" href="{{ asset('/admin/employee') }}"><i
                                        class="dripicons-lock"></i>Employee</a></li>
                            @endif
                        </ul>
                    </div><!-- end Authentication-->
                </div>
                <!--end menu-body-->
            </div><!-- end main-menu-inner-->
        </div>
        <!-- end left-sidenav-->

        <!-- Page Content-->
        <div class="page-content">
            <Br /><br />
            <div class="container-fluid">
                @yield('content')
            </div><!-- container -->

            <footer id="footer" class="footer text-center ">
                &copy; 2019 SMEShipping
            </footer>
            <!--end footer-->
        </div>
        <!-- end page content -->
    </div>
    <!-- jQuery   <script src="{{ asset('/assets/js/jquery.min.js') }}"></script>-->

    <script src="{{ asset('/assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/assets/js/metisMenu.min.js') }}"></script>
    <script src="{{ asset('/assets/js/waves.min.js') }}"></script>
    <script src="{{ asset('/assets/js/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ asset('/assets/plugins/tiny-editable/mindmup-editabletable.js') }}"></script>
    <script src="{{ asset('/assets/plugins/tiny-editable/numeric-input-example.js') }}"></script>
    <script src="{{ asset('/assets/plugins/tabledit/jquery.tabledit.js') }}"></script>
    <script src="{{ asset('/assets/pages/jquery.tabledit.init.js') }}"></script>
    <!-- App js -->
    <script src="{{ asset('/assets/js/app.js') }}"></script>
    <script>
        $('#loading').hide();
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            //------------ ดึงข้อมูลจังหวัด 
            $('#notification').html('');
            
            $.ajax({
                type: "GET",
                url: '/admin/count_request',
                success: function (result) {
                    console.log(result);
                    $('#count_sale').html(result.sale);
                    $('#count_inbox').html(result.inbox);
                    $('#count_pickup').html(result.pickup);
                    $('#count_hold').html(result.hold);
                    $('#count_payment').html(result.payment);
                    $('#count_all').html(result.all);
                    $('#count_release').html(result.release);
                    $('#count_calculate').html(result.calculate);
                    $('#count_proof').html(result.proof);
                    $('#count_cancel').html(result.cancel);
                    $('#count_checkin').html(result.checkin);
                    $('#count_checklink').html(result.checklink);
                    $('#count_payment_attach').html(result.count_payment_attach);

                    var checkin =  '<div class="alert alert-outline-danger alert-warning-shadow mb-0 alert-dismissible fade show" role="alert">'+
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                '<span aria-hidden="true"><i class="mdi mdi-close"></i></span>'+
                                '</button>'+
                                '<div class="spinner-grow text-danger" role="status"></div><a href="{{url("/admin/checkinbox")}}"><strong>มีพัสดุรอรับเข้าอยู่ '+result.checkin+' กล่อง</strong></a>'+
                                '</div>';
                    var sale =  '<div class="alert alert-outline-success alert-warning-shadow mb-0 alert-dismissible fade show" role="alert">'+
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                '<span aria-hidden="true"><i class="mdi mdi-close"></i></span>'+
                                '</button>'+
                                '<strong><div class="spinner-grow text-success" role="status"></div><a href="{{url("/admin/waitforsale")}}">มีลูกค้า Booking จากเว็บ '+result.sale+' รายการ</strong></a>'+
                                '</div>';           
                    if(result.checkin>0){
                        $('#notification').append(checkin);
                    }
                    if(result.sale>0){
                        $('#notification').append(sale);
                    }
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    $('#loading').hide();
                }
            });
        });
        var islinkactive = $('.menu-body').find('.active').length;
        if(islinkactive<=0){
            $('#m_default').click();
        }
    </script>
    <div id="notification">
    </div>
    @if($errors->any())
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        swal("Message", "{{$errors->first()}} ", "error");
    </script>

    @endif

</body>

</html>