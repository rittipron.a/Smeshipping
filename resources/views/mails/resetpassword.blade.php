<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:v="urn:schemas-microsoft-com:vml">

<head>
</head>

<body>
                            <p style="text-align:center"><img src="http://backend.smeshipping.com/public/assets/email/LOGOSMESHIP.png" width="208" height="51" /></p>
                            <p>เรียน คุณ{{$name}}</p>
                            <p>รหัสผ่านใหม่ของคุณคือ {{$password}}</p>
                            <p>คุณสามารถเข้าสู่ระบบได้ที่ <br/><a href="{{$url_login}}">https://backend.smeshipping.com/public/app/login</a></p><br/><br/>
                            <p>เอสเอ็มอี ชิปปิ้ง ขนส่งกับเราแบบเบาใจ<br/>ขอขอบพระคุณสำหรับการใช้บริการในครั้งนี้</p>
</body>
</html>