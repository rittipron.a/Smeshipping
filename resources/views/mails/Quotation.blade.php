<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>SME QUOTATION</title>
</head>

<body>
    <p style="text-align: center;"><b>SME SHIPPING :QUOTATION เลขที่ {{$qot_no}}</b></p>

    เรียน คุณ {{$name}}<br /><br />
    SME SHIPPING ขอแจ้งให้ท่านทราบว่า ใบเสนอราคาเลขที่ {{$qot_no}}<br />
    ท่านสามารถเข้าดูรายละเอียดใบเสนอราคาได้ <a href="{{$qot_url}}">จากที่นี่</a><br /><br /><br />
    หากท่านมีข้อสงสัยใดๆเกี่ยวกับค่าบริการ กรุณาติดต่อกลับมาที่ admin@smeshipping.com<br />
    หรือติดต่อผ่านแผนก Customer Service ที่หมายเลข <span style="color:red">02-105-7777</span><br /><br />
    <span style="color:#36f">เอสเอ็มอี ชิปปิ้ง ขนส่งกับเราแบบเบาใจ<br />
        ขอขอบพระคุณสำหรับการใช้บริการในครั้งนี้</span><br /><br /><br />
    Best Regards,<br />
    SME SHIPPING Billing Team<br />


</body>

</html>