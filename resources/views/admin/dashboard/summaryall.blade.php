@extends('layouts.app')
@section('content')
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<style>
    .card-body {
        overflow-x: auto;
        width: auto;
    }

    .frow {
        padding-right: unset !important;
    }

    .table-responsive {
        display: none;
    }
</style>
@php
$ldate = strtotime(str_replace("/", "-",date('d/m/Y')));
@endphp
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">All Report</a>
                    </li>
                </ol>
            </div>
            <h4 class="page-title">All Report</h4>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="card">
            <h5 class="card-header bg-{{count($new)==0?'success':'danger'}} text-white mt-0"
                onclick="$('.tablenew').toggle('slow');">งานใหม่ (
                จำนวน {{count($new)}} Booking)</h5>
            <div class="card-body">
                <div class="table-responsive tablenew">
                    <table class="table table-bordered mb-0 table-centered">
                        <thead>
                            <tr>
                                <th weight="10">ลำดับ</th>
                                <th>สถานะ</th>
                                <th>SALE</th>
                                <th>ชื่อลูกค้า</th>
                                <th>ประเทศปลายทาง</th>
                                <th>วันที่</th>
                                <th>เลข Booking</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($new as $index=>$item)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td>
                                    <span class="badge badge-soft-success">
                                        {!!$item->request_status==0?'<i class="mdi mdi-account-clock-outline"></i>
                                        รอติดต่อ':''!!}
                                        {!!$item->request_status==1?'<i class="mdi mdi-email-plus-outline"></i>
                                        รอเข้ารับ':''!!}
                                    </span>
                                </td>
                                <td>{{$item->salename}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->type_shipping}}</td>
                                <td>{{date('d/m/Y',$item->delivery_date)}}</td>
                                <td>{{str_pad($item->id,7,"0",STR_PAD_LEFT)}}</td>
                            </tr>
                            @endforeach
                            @if(count($new)<=0) <tr>
                                <td colspan="7" style="text-align:center">ไม่มีข้อมูล</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                    <!--end /table-->
                </div>
            </div>
            <!--end card-body-->
        </div>

        <div class="card">
            <h5 class="card-header bg-{{count($pickup)==0?'success':'danger'}} text-white mt-0"
                onclick="$('.tablepickup').toggle('slow');">รอของเข้า (
                จำนวน {{count($pickup)}} Booking)
            </h5>
            <div class="card-body">
                <div class="table-responsive tablepickup">
                    <table class="table table-bordered mb-0 table-centered">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>สถานะ</th>
                                <th>SALE</th>
                                <th>ชื่อลูกค้า</th>
                                <th>ประเทศปลายทาง</th>
                                <th>วันที่</th>
                                <th>เลข Booking</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pickup as $index=>$item)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td>
                                    <span class="badge badge-soft-success">
                                        {!!$item->request_status==2?'<i class="mdi mdi-car"></i>เตรียมเข้ารับ':''!!}
                                        {!!$item->request_status==102?'<i class="mdi mdi-car"></i> เตรียมเข้ารับ
                                        (Hold)':''!!}
                                    </span>
                                </td>
                                <td>{{$item->salename}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->type_shipping}}</td>
                                <td>{{date('d/m/Y',$item->delivery_date)}}</td>
                                <td>{{str_pad($item->id,7,"0",STR_PAD_LEFT)}}{{$item->type_delivery_note!=null?'/'.$item->type_delivery_note:''}}
                                </td>
                            </tr>
                            @endforeach
                            @if(count($pickup)<=0) <tr>
                                <td colspan="7" style="text-align:center">ไม่มีข้อมูล</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                    <!--end /table-->
                </div>
            </div>
            <!--end card-body-->
        </div>

        <div class="card">
            <h5 class="card-header bg-{{count($cal)==0?'success':'danger'}} text-white mt-0"
                onclick="$('.tablecal').toggle('slow');">รอดำเนินการ (
                จำนวน {{count($cal)}} Booking)</h5>
            <div class="card-body">
                <div class="table-responsive tablecal">
                    <table class="table table-bordered mb-0 table-centered">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>สถานะ</th>
                                <th>SALE</th>
                                <th>ชื่อลูกค้า</th>
                                <th>ประเทศปลายทาง</th>
                                <th>วันที่</th>
                                <th>เลขพัสดุ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cal as $index=>$item)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td>
                                    <span class="badge badge-soft-success">
                                        {!!$item->request_status==3?'<i class="mdi mdi-scale"></i>รอชั่งวัด':''!!}
                                        {!!$item->request_status==103?'<i class="mdi mdi-scale"></i> รอชั่งวัด
                                        (Hold)':''!!}
                                    </span>
                                </td>
                                <td>{{$item->salename}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->type_shipping}}</td>
                                <td>{{date('d/m/Y',$item->delivery_date)}}</td>
                                <td>{{str_pad($item->id,7,"0",STR_PAD_LEFT)}}{{$item->type_delivery_note!=null?'/'.$item->type_delivery_note:''}}
                                </td>
                            </tr>
                            @endforeach
                            @if(count($cal)<=0) <tr>
                                <td colspan="7" style="text-align:center">ไม่มีข้อมูล</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                    <!--end /table-->
                </div>
            </div>
            <!--end card-body-->
        </div>

        <div class="card">
            <h5 class="card-header bg-{{count($pay)==0?'success':'danger'}} text-white mt-0"
                onclick="$('.tablepay').toggle('slow');">รอการชำระ (
                จำนวน {{count($pay)}} Booking)</h5>
            <div class="card-body">
                <div class="table-responsive tablepay">
                    <table class="table table-bordered mb-0 table-centered">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>สถานะ</th>
                                <th>SALE</th>
                                <th>ชื่อลูกค้า</th>
                                <th>ประเทศปลายทาง</th>
                                <th>วันที่</th>
                                <th>เลขพัสดุ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pay as $index=>$item)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td>
                                    <span class="badge badge-soft-success">
                                        {!!$item->status==4&&$item->payment_file_re!=null?'<i class="mdi mdi-cash"></i>
                                        Proof':''!!}
                                        {!!$item->status==4&&$item->payment_file_re==null?'<i class="mdi mdi-cash"
                                            style="color:red"></i>
                                        <span style="color:red">Waiting</span>':''!!}
                                        @if ($item->status==4&&$item->payment_file_re!=null)
                                        <img src="{{asset('assets/images/coins/coin.gif')}}" width="15px" />
                                        @endif
                                    </span> /
                                    {{$item->price_total-$item->discount}} บาท
                                </td>
                                <td>{{$item->salename}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->type_shipping}}</td>
                                <td>{{date('d/m/Y',$item->delivery_date)}}</td>
                                <td>{{str_pad($item->id,7,"0",STR_PAD_LEFT)}}{{$item->type_delivery_note!=null?'/'.$item->type_delivery_note:''}}
                                </td>
                            </tr>
                            @endforeach
                            @if(count($pay)<=0) <tr>
                                <td colspan="7" style="text-align:center">ไม่มีข้อมูล</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                    <!--end /table-->
                </div>
            </div>
            <!--end card-body-->
        </div>

        <div class="card">
            <h5 class="card-header bg-{{count($release)==0?'success':'danger'}} text-white mt-0"
                onclick="$('.tablerelease').toggle('slow');">รอปล่อย (
                จำนวน {{count($release)}} Booking)</h5>
            <div class="card-body">
                <div class="table-responsive tablerelease">
                    <table class="table table-bordered mb-0 table-centered">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>สถานะ</th>
                                <th>SALE</th>
                                <th>ชื่อลูกค้า</th>
                                <th>ประเทศปลายทาง</th>
                                <th>วันที่</th>
                                <th>เลขพัสดุ</th>
                                <th>จำนวนพัสดุ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($release as $index=>$item)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td>
                                    <span class="badge badge-soft-success">
                                        {!!$item->request_status==5?'<i class="mdi mdi-scale"></i>รอปล่อยสินค้า':''!!}
                                        {!!$item->request_status==105?'<i class="mdi mdi-scale"></i> รอปล่อยสินค้า
                                        (Hold)':''!!}
                                    </span>
                                </td>
                                <td>{{$item->salename}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->type_shipping}}</td>
                                <td>{{date('d/m/Y',$item->delivery_date)}}</td>
                                <td>{{str_pad($item->id,7,"0",STR_PAD_LEFT)}}{{$item->type_delivery_note!=null?'/'.$item->type_delivery_note:''}}
                                </td>
                                <td>{{$item->box}}</td>
                            </tr>
                            @endforeach
                            @if(count($release)<=0) <tr>
                                <td colspan="7" style="text-align:center">ไม่มีข้อมูล</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                    <!--end /table-->
                </div>
            </div>
            <!--end card-body-->
        </div>

        <div class="card">
            <h5 class="card-header bg-{{count($pandding)==0?'success':'danger'}} text-white mt-0"
                onclick="$('.tableall').toggle('slow');">
                ของค้าง(จำนวน {{count($pandding)}} กล่อง)</h5>
            <div class="card-body">
                <div class="table-responsive tableall">
                    <table class="table table-bordered mb-0 table-centered">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>สถานะ</th>
                                <th>SALE</th>
                                <th>ชื่อลูกค้า</th>
                                <th>ประเทศปลายทาง</th>
                                <th>วันที่</th>
                                <th>เลขพัสดุ</th>
                                <th>หมายเหตุ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pandding as $index=>$item)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td>
                                    @if($item->boxstatus<10) <span
                                        class="badge badge-soft-{{$item->status>100?'purple':($item->status==99?'danger':'success')}}">
                                        {!!$item->status==0?'<i class="mdi mdi-account-clock-outline"></i> Sale':''!!}
                                        {!!$item->status==1?'<i class="mdi mdi-email-plus-outline"></i> Create':''!!}
                                        {!!$item->status==2?'<i class="mdi mdi-car"></i> Pickup':''!!}
                                        {!!$item->status==3?'<i class="mdi mdi-scale"></i> Calculate':''!!}
                                        {!!$item->status==4&&$item->payment_file_re!=null?'<i class="mdi mdi-cash"></i>
                                        Proof':''!!}
                                        {!!$item->status==4&&$item->payment_file_re==null?'<i class="mdi mdi-cash"
                                            style="color:red"></i>
                                        <span style="color:red">Waiting</span>':''!!}
                                        {!!$item->status==5?'<i class="mdi mdi-truck-check"></i> Release':''!!}
                                        {!!$item->status==6?'<i class="mdi mdi-progress-check"></i> Complete':''!!}
                                        {!!$item->status==99?'<i class="mdi mdi-cancel"></i> Cancel':''!!}
                                        {!!$item->status==102?'<i class="mdi mdi-car"></i> Pickup (Hold)':''!!}
                                        {!!$item->status==103?'<i class="mdi mdi-scale"></i> Calculate (Hold)':''!!}
                                        {!!$item->status==104?'<i class="mdi mdi-cash"></i> Payment (Hold)':''!!}
                                        {!!$item->status==105?'<i class="mdi mdi-truck-check"></i> Release
                                        (Hold)':''!!}
                                        {!!$item->status==106?'<i class="mdi mdi-progress-check"></i> Complete
                                        (Hold)':''!!}
                                        @if ($item->status==4&&$item->payment_file_re!=null)
                                        <img src="{{asset('assets/images/coins/coin.gif')}}" width="25px" />
                                        @endif
                                        </span>
                                        @else
                                        <span class="badge badge-soft-danger">
                                            <i class="fas fa-external-link-alt"></i> รอส่งคืนลูกค้า
                                        </span>
                                        @endif

                                </td>
                                <td>{{$item->salename}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->type_shipping}}</td>
                                <td>{{date('d/m/Y',$item->delivery_date)}}</td>
                                <td>{{str_pad($item->id,7,"0",STR_PAD_LEFT)}}{{$item->type_delivery_note!=null?'/'.$item->type_delivery_note:''}}
                                </td>
                                <td>{{$item->description}}</td>

                            </tr>
                            @endforeach
                            @if(count($pandding)<=0) <tr>
                                <td colspan="7" style="text-align:center">ไม่มีข้อมูล</td>
                                </tr>
                                @endif
                        </tbody>
                    </table>
                    <!--end /table-->
                </div>
            </div>
            <!--end card-body-->
        </div>

    </div>
</div>
<script>
    //setTimeout('window.location.reload();', 30000);
</script>
@endsection