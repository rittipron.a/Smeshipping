@extends('layouts.app')
@section('content')
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<style>
    .card-body {
        overflow-x: auto;
        width: auto;
    }

    .frow {
        padding-right: unset !important;
    }
</style>
@php
$ldate = strtotime(str_replace("/", "-",date('d/m/Y')));
@endphp
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Today Report</a>
                    </li>
                </ol>
            </div>
            <h4 class="page-title">Today Report</h4>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row justify-content-center">
            <!--end col-->
            <div class="col-md-3">
                <div class="card">
                    <h5 class="card-header bg-primary text-white mt-0">New booking</h5>
                    <div class="card-body">
                        <table class="table mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Phone</th>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($waitforsale as $index=>$item)
                                <tr>

                                    <th class="frow">{{$index+1}}</th>
                                    <td><a href="{{url('admin/request').'/'.$item->id}}">{{$item->phone}}</a></td>
                                    <td><a href="{{url('admin/request').'/'.$item->id}}">{{$item->name}}</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end card-body-->
                </div>
            </div>
            <!--end col-->
            <div class="col-md-3">
                <div class="card">
                    <h5 class="card-header bg-primary text-white mt-0">Wait for payment</h5>
                    <div class="card-body">
                        <table class="table mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th># Date</th>
                                    <th>Name</th>
                                    <th>Pay</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($waitforpaytoday as $index=>$item)
                                <tr>
                                    <td class="frow">{{$index+1}}. {{date('d/m/y',$item->delivery_date)}}</td>
                                    <td><a href="{{url('admin/request').'/'.$item->id}}">{{$item->name}} </a>
                                    </td>
                                    <td>
                                        @if ($item->status==4&&$item->payment_file_re!=null)
                                        <img src="{{asset('assets/images/coins/coin.gif')}}" width="25px" />
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end card-body-->
                </div>
            </div>
            <!--end col-->
            <div class="col-md-3">
                <div class="card">
                    <h5 class="card-header bg-primary text-white mt-0">Pending</h5>
                    <div class="card-body">
                        <table class="table mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th width="78"># Date</th>
                                    <th>Name</th>
                                    <th>Process</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($pendding as $index=>$item)
                                @if($item->delivery_date!=$ldate&&$item->status==5)
                                <tr {!!$item->delivery_date<$ldate?'style="color:red"':''!!}>
                                        <td class="frow">
                                            {{date('d/m/y',$item->delivery_date)}}
                                        </td>
                                        <td><a href="{{url('admin/request').'/'.$item->id}}">{{$item->name}}</a></td>
                                        <td><a href="{{url('admin/request').'/'.$item->id}}">{!!$item->status==0?'Sale':''!!}{!!$item->status==1?'Create':''!!}{!!$item->status==2?'Pickup':''!!}{!!$item->status==3?'Calculate':''!!}{!!$item->status==4?'Payment':''!!}{!!$item->status==5?'Release':''!!}{!!$item->status==6?'Complete':''!!}@if($item->status==4&&$item->payment_file_re!=null)<img
                                                    src="{{asset('assets/images/coins/coin.gif')}}" width="15px" />
                                                @endif
                                            </a>
                                        </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end card-body-->
                </div>
            </div>
            <!--end col-->
            <div class="col-md-3">
                <div class="card">
                    <h5 class="card-header bg-primary text-white mt-0">Check-Out</h5>
                    <div class="card-body">
                        <table class="table mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th># Date</th>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($success as $index=>$item)
                                <tr>
                                    <td class="frow">{{$index+1}}. {{date('d/m/Y',$item->delivery_date)}}</td>
                                    <td><a href="{{url('admin/request').'/'.$item->id}}">{{$item->name}}</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end card-body-->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    setTimeout('window.location.reload();', 30000);
</script>
@endsection