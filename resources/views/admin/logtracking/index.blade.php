@extends('layouts.app')

@section('content')
    <link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet"/>
    <link href="{{ asset('assets/css/datatables.min.css' )}}" rel="stylesheet"/>
    <link href="{{ asset('assets/css/magnific-popup.css' )}}" rel="stylesheet"/>
    <link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css' )}}" rel="stylesheet"/>
    <link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet"/>
    <style>
        .mfp-bg {
            z-index: 1051 !important;
        }

        .mfp-wrap {
            z-index: 1052 !important;
        }

        thead input {
            width: 100%;
        }

        #tracking-logs-table tbody tr td.tracing_awb > label,
        #tracking_modal > .tracing_awb {
            font-weight: bold;
            color: #f31414;
            text-decoration: underline;
            cursor: pointer;
        }

        #tracking-logs-table tbody tr td.tracing_awb > span.edit {
            padding-left: 2px;
            cursor: pointer;
        }

        table.dataTable tbody td.select-checkbox::before,
        table.dataTable tbody th.select-checkbox::before {
            cursor: pointer !important;
            margin: 0 !important;
            border: 1px solid #cdc7c2 !important;
        }

        table.dataTable tr.selected td.select-checkbox::after,
        table.dataTable tr.selected th.select-checkbox::after {
            cursor: pointer !important;
            margin-top: -5px !important;
            margin-left: 2px !important;
        }

        .topic_customer {
            font-size: 14px;
            color: #656269;
        }

        .detail_custome {
            border: 1px solid #e8ebf3;
            border-radius: 10px;
            font-size: 14px;
            padding: 8px 6px 0px 14px;
        }

        .detail_mail {
            margin-top: 10px;
            border: 1px solid #e8ebf3;
            border-radius: 10px;
            font-size: 14px;
            padding: 8px 6px 0px 14px;
        }

        .label {
            font-weight: bold;
            color: #656269;
            font-size: 16px;
        }

        table.dataTable tbody > tr.selected,
        table.dataTable tbody > tr > .selected {
            background-color: #656269;
        }

        table.dataTable.stripe tbody > tr.odd.selected,
        table.dataTable.stripe tbody > tr.odd > .selected,
        table.dataTable.display tbody > tr.odd.selected,
        table.dataTable.display tbody > tr.odd > .selected {
            background-color: #656269;
        }

        .number_query {
            border-radius: 10px !important;
            background-color: #f22727 !important;
            text-align: center;
        }

        .padding_left {
            font-size: 16px;
            color: #f22727;
        }

        .center_line {
            text-align: center;
        }

        li {
            display: inline-block;
        }

        li a {
            text-decoration: none;
            color: #f22727;
        }

        .manu_error .hover-border.active {
            font-weight: bold;
            border-bottom: 1px solid #f22727;
        }

        .nav-item, .selete_sale {
            padding-right: 15px;
            padding-left: 15px;
        }

        .margin_top_boottom {
            margin-bottom: 34px;
            margin-top: 14px;
        }

        .submit_active {
            background-color: #ffffff;
            color: #656269;
            border: 1px solid #f22727;
            border-radius: 10px;
        }

        .submit_active:hover {
            background-color: #f22727;
            color: #ffffff;
        }

        .padding_left_form {
            padding-left: 16px;
        }

        .font_selete {
            font-size: 15px;
            font-weight: bold;
        }

        .dataTables_length {
            position: absolute;
        }

        .mail_action {
            margin-top: 18px;
            width: 100%;
        }

        .hidden_ {
            display: none;
        }

        @media (min-width: 992px) {
            .modal-lg,
            .modal-xl {
                max-width: 48%;
            }

            .dataTables_length {
                position: absolute;
            }

            .select_sale_position {
                position: absolute;
            }
        }

        @media (max-width: 767px) {
            .form-control-sm {
                width: 87% !important;
            }

            .mobile_label {
                padding-top: 24px
            }

            .row_br {
                margin-bottom: 16px;
            }

            #tracking-logs-table_length {
                text-align: right;
            }

            .margin_top_boottom {
                margin-bottom: 10px;
                margin-top: 10px;
            }

            .nav-item {
                padding-right: 15px;
                padding-left: 15px;
                width: 49%;
                margin-bottom: 10px;
            }

            .margin_bottom {
                margin-bottom: 10px;
            }

            .selete_sale {
                padding-right: 15px;
                padding-left: 0px;
                width: 75%;
            }
        }

        .modal-body {
            max-height: calc(100vh - 210px);
            overflow-y: auto;
        }
    </style>
    <br/>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 margin_top_boottom">
                            <ul class="nav manu_error" role="tablist">
                                <li class="nav-item">
                                    <a class="hover-border active" data-toggle="tab" href="#teb1" role="tab"
                                       aria-selected="true">
                                        <span class="padding_left">Application</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        @foreach($groups as $group)
                            <div class="col-md-3">
                                <div class="card">
                                    <a href="{{ route('tracing_by', $group->id) }}">
                                        <h5 class="card-header bg-primary text-white mt-0 number_query">
                                            {{ $group->name }}
                                            <span class="text-muted">({{ number_format($group->filters->reduce(function ($count, $filter) {
    return $count + $filter->errors()->withTrashed()->count();
})) }})</span>
                                        </h5>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="padding_left_form">
                        <div class="margin_bottom select_sale_position">
                            <form id="frm-assign" action="{{ route('tracing_assign') }}" method="POST"
                                  class="form-inline">
                                @csrf
                                <div class="row row_br">
                                    <label for="officer" class="font_selete">Assign selected to</label>
                                    <div class="selete_sale">
                                        <select name="officer" id="officer" class="custom-select">
                                            @foreach($officers as $officer)
                                                <option value="{{ $officer->id }}">{{ $officer->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="submit_active" id="btn-assign">Confirm</button>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div style="width: 100%;">
                                <div class="table-responsive">
                                    <table id="tracking-logs-table" class="table cell-border compact stripe"
                                           cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th data-name="Priority" data-toggle="tooltip" data-placement="top"
                                                title="ลำดับความสำคัญ">
                                                Priority
                                            </th>
                                            <th data-name="Date" data-toggle="tooltip" data-placement="top"
                                                title="วันที่ตรวจพบ">Date
                                            </th>
                                            <th data-name="SO" data-toggle="tooltip" data-placement="top"
                                                title="เลขที่ SO">SO
                                            </th>
                                            <th data-name="AWB" data-toggle="tooltip" data-placement="top"
                                                title="เลขที่ AirWayBill">
                                                AWB
                                            </th>
                                            <th data-name="Status" data-toggle="tooltip" data-placement="top"
                                                title="สถานะล่าสุดจากผู้ให้บริการ">Recent Status
                                            </th>
                                            <th data-name="Deadline" data-toggle="tooltip" data-placement="top"
                                                title="กำหนดเวลา">Deadline
                                            </th>
                                            <th data-name="Note" data-toggle="tooltip" data-placement="top"
                                                title="บันทึกย่อๆ">Note
                                            </th>
                                            <th data-name="Assignee" data-toggle="tooltip" data-placement="top"
                                                title="มอบหมายงานให้">
                                                Assigned to
                                            </th>
                                            <th data-name="Time (Days)" data-toggle="tooltip" data-placement="top"
                                                title="วันที่ปล่อยของจนถึงวันนี้ (วัน)">Time (Days)
                                            </th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Priority</th>
                                            <th>Date</th>
                                            <th>SO</th>
                                            <th>AWB</th>
                                            <th>Recent Status</th>
                                            <th>Deadline</th>
                                            <th>Note</th>
                                            <th>Assigned to</th>
                                            <th>Time(s)</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--end table-->
                    </div>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!-- end col -->
    </div>
    <!-- end Awb -->
    <div class="modal fade" id="tracing_modal" tabindex="-1" role="dialog" aria-labelledby="tracking_modal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content" style="background-color: #ffffff;border-radius: 10px;">
                <div class="modal-header" style=" background-color: #ffffff;
                    border-bottom: 0px;
                    height: 0px;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;">
                    <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"
                            style="padding-top: 10px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding-bottom: 40px;">
                    <fieldset id="form-horizontal-p-0" role="tabpanel" aria-labelledby="form-horizontal-h-0"
                              class="body current" aria-hidden="false">
                        <!-- Head -->
                        <form id="frm_update_returned_awb" action="" method="POST">
                            @csrf
                            <input type="hidden" id="awb_return" name="awb_return" value="">
                            <div class="row">
                                <div class="col-1"></div>
                                <div class="col-7">
                                    <h5 class="" id="tracking_modal"><span class="label">Airway Bill #</span><span
                                            id="number_awb" style="color:red;"></span></h5>
                                </div>
                                <div class="col-3" style="text-align: right;">
                                    <button type="button" id="return" class="btn btn-primary">
                                        <span>RETURN</span>
                                    </button>
                                    <button type="submit" id="save_awb" class="btn btn-primary" style="display:none;">
                                        <span id="return">SAVE</span>
                                    </button>
                                </div>
                                <div class="col-1"></div>
                            </div>
                        </form>
                        <br/>
                        <!-- end Head -->

                        <!-- รายละเอียดลูกค้า -->
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-10">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="detail_custome">
                                            <label for="txtFirstNameBilling" class="topic_customer"> Customer : </label>
                                            <span id="customer"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-mb-6">
                                        <div class="detail_mail">
                                            <label for="txtFirstNameBilling" class="topic_customer"> E-mail : </label>
                                            <span id="email"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-mb-6">
                                        <div class="detail_mail">
                                            <label for="txtLastNameBilling" class="topic_customer"> Mobile No.
                                                : </label>
                                            <span id="mobile"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-mb-6">
                                        <div class="detail_mail">
                                            <label for="txtLastNameBilling" class="topic_customer"> SO No.
                                                : </label>
                                            <span id="so"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1"></div>
                        </div>
                        <br/>
                        <!-- end รายละเอียดลูกค้า -->

                        <!-- Note -->
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-10">
                                <div class="row">
                                    <div class="col-12 col-md-12">
                                        <label for="status_update" class="label">Commercial Invoice</label>
                                        <embed id="com_invoice" src="" style="width: 100%" height="400"
                                               type="application/pdf">
                                    </div>
                                </div>
                            </div>
                            <div class="col-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-10">
                                <div class="row">
                                    <div class="col-12 col-md-12">
                                        <label for="status_update" class="label">Attachments</label>
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row container-grid nf-col-3  projects-wrapper"
                                                     id="gallery-section"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-10">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <form id="frm-tracing-log" action="" method="POST">
                                            @csrf
                                            <label for="status_update" class="label">Status</label>
                                            <input type="hidden" id="awb" name="awb" value="">
                                            <input type="hidden" name="op" id="op"
                                                   value="{{ \Illuminate\Support\Facades\Auth::user()->id }}">
                                            <input id="status_update" name="status_update" type="text"
                                                   class="form-control"
                                                   required><br/>
                                            <label for="comment" class="label">Comment</label>
                                            <textarea id="comment" name="comment" rows="4" class="form-control"
                                                      style="resize: none;" required></textarea>
                                            <!--end row-->
                                            <label for="txtAddress1Billing" class="col-form-label"></label>
                                            <button type="button" id="save_tracing_log" class="btn btn-primary"
                                                    style="width: 100%;">Save
                                            </button>
                                        </form>
                                    </div>
                                    <div class="col-12 col-md-6 mobile_label">
                                        <label for="txtAddress1Billing" class="label">Activities</label>
                                        <div class="card" style="margin-bottom: 0px;">
                                            <div class="card-body"
                                                 style="height: 194px;overflow-y: scroll;border: 1px solid #e8ebf3;">
                                                <div class="" id="activities_panel"></div>
                                            </div>
                                        </div>
                                        <form id="frm-tracing-complete" action="" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="hidden" name="complete" id="complete" value="1">
                                            <label for="txtAddress1Billing" class="col-form-label"></label>
                                            <button type="submit" class="btn btn-success" style="width: 100%;">
                                                Complete
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1"></div>
                        </div>
                        <br/>
                        <!-- end Note -->
                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-10">
                                <div class="row">
                                    <div class="col-12 col-md-12">
                                        <label for="txtAddress1Billing" class="label">Email To</label>
                                    </div>
                                    <div class="col-6 col-md-3">
                                        <div style="text-align: center;" onclick="open_mail(1)">
                                            <i class="fab fa-dhl" style="font-size: 80px;cursor: pointer;"></i>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-3">
                                        <div style="text-align: center;" onclick="open_mail(2)">
                                            <i class="fab fa-fedex" style="font-size: 80px;cursor: pointer;"></i>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-3">
                                        <div
                                            style="text-align: center;font-size: 48px;cursor:pointer;font-weight: bold;"
                                            onclick="open_mail(3)">TNT
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-3">
                                        <div style="text-align: center;" onclick="open_mail(4)">
                                            <i class="fas fa-users" style="font-size: 65px;cursor: pointer;"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1"></div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <!-- end Awb -->

    <!-- Deadline & Note -->
    <div class="modal fade" id="modal_deadline" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" style="background-color: #ffffff;border-radius: 10px;">
                <div class="modal-header" style=" background-color: #ffffff;
                    border-bottom: 0px;
                    height: 0px;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;">
                    <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"
                            style="padding-top: 10px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding-bottom: 40px;">
                    <div class="col-12">
                        <form id="frm-tracing-detail" action="" method="POST">
                            @csrf
                            <label for="deadline" class="">Deadline</label>
                            <input type="text" class="form-control tracing-dtp-deadline" name="deadline" id="deadline" value=""><br/>
                            <input type="hidden" id="awb_detail" name="awb" value="">
                            <input type="hidden" name="op" id="op_detail"
                                   value="{{ \Illuminate\Support\Facades\Auth::user()->id }}">
                            <label for="note" class="">Note</label>
                            <textarea id="note" name="note" rows="4" class="form-control"
                                      style="resize: none;"></textarea>
                            <label for="txtAddress1Billing" class=""></label>
                            <button type="submit" id="save_deadline_note" class="btn btn-primary" style="width: 100%;">
                                Save
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end Deadline & Note -->

    <!-- Mail -->
    <div class="modal fade" id="modal_mail" tabindex="-1" role="dialog" aria-labelledby="tracking_modal"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content" style="background-color: #ffffff;border-radius: 10px;">
                <div class="modal-header" style=" background-color: #ffffff;
                    border-bottom: 0px;
                    height: 0px;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;">
                    <button type="button" class="close" id="close_mail" data-dismiss="modal" aria-label="Close"
                            style="padding-top: 10px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <!-- ซ่อนข้อมูลMailทั้งหมด -->
                    <div style='display:none;'>
                        @foreach($data as $index=>$item)
                            <div class="check_length">
                                <input type="hidden" id="code{{$item->id}}" value="{{$item->id}}">
                                <input type="hidden" id="sms_{{$item->id}}" value="{{$item->teamplate_sms}}">
                                <input type="hidden" id="email_topic{{$item->id}}" value="{{$item->email_topic}}">
                                <input type="hidden" id="cc{{$item->id}}" value="{{$item->cc}}">
                                <input type="hidden" id="bcc{{$item->id}}" value="{{$item->bcc}}">
                                <input type="hidden" id="group_name{{$item->id}}" value="{{$item->group_name}}">
                                <textarea id="email_{{$item->id}}">{{$item->teamplate_mail}}</textarea>
                            </div>
                        @endforeach
                    </div>
                    <!-- end -->
                </div>
                <div class="modal-body" style="padding-bottom: 40px;">
                    <form class="" id="form_sentmaill" action="{{url('/admin/sent_mail')}}">
                        <div class="row">
                            <div class="col-12">
                                <span id="group_mail"></span>
                                <input type="hidden" id="group_name" name="group_name" value="">
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label for="selected_mail">เลือกหัวข้ออีเมลที่บันทึกเอาไว้</label>
                                    <select class="form-control" id="selected_mail">
                                        @foreach($data as $index=>$item)
                                            @if($item->group_name == app('request')->input('group_name'))
                                                <?php
                                                $number = 0;
                                                ?>
                                                @if($number == 0)
                                                    <option id="{{$item->id}}"
                                                            teamplate_name="{{$item->teamplate_name}}"
                                                            group_name="{{$item->group_name}}"
                                                            email_topic="{{$item->email_topic}}"
                                                            teamplate_mail="{{$item->teamplate_mail}}"
                                                            teamplate_sms="{{$item->teamplate_sms}}"
                                                            cc="{{$item->cc}}" bcc="{{$item->bcc}}"
                                                            value="{{$item->id}}"
                                                            selected>{{$item->teamplate_name}}</option>
                                                    <?php
                                                    $number + 1
                                                    ?>
                                                @else
                                                    <option id="{{$item->id}}"
                                                            teamplate_name="{{$item->teamplate_name}}"
                                                            group_name="{{$item->group_name}}"
                                                            email_topic="{{$item->email_topic}}"
                                                            teamplate_mail="{{$item->teamplate_mail}}"
                                                            teamplate_sms="{{$item->teamplate_sms}}"
                                                            cc="{{$item->cc}}" bcc="{{$item->bcc}}"
                                                            value="{{$item->id}}">{{$item->teamplate_name}}</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-12">
                                <label for="validationCustomUsername">อีเมล <span style="color:red">*</span></label>
                                <input type="email" name="email" class="form-control" multiple required="">
                            </div>

                            <div class="col-md-6">
                                <label for="validationCustom02">เบอร์โทรศัพท์</label>
                                <input type="text" class="form-control" name="tel" id="tel" onkeyup="check_sms()">
                            </div>

                            <div class="col-12">
                                <label>ข้อความเพื่อส่งSMS (ปล่อยว่างไว้กรณีไม่ต้องการส่ง SMS)</label>
                                <input type="text" name="sms_detail" class="form-control" id="sms_detail"
                                       onkeyup="check_sms()" value="">
                            </div>

                            <div class="col-12">
                                <span>Toppic email : </span><input type="email" name="toppic" id="toppic_mail"
                                                                   class="form-control" value="">
                            </div>

                            <div class="col-12">
                                <span>CC : </span><input type="email" name="cc" id="cc" class="form-control" value="">
                            </div>

                            <div class="col-12">
                                    <textarea id="elm2" name="email_detail">
                                    <p></p>
                                    </textarea>
                                <div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" id="sent_mail" class="btn btn-primary mail_action">
                                            ส่งเมล
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end Mail -->

    <script src="{{ asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/js/datatables.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/moment/moment.js')}}"></script>
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js')}}"></script>
    <script>
        $(document).ready(function () {
            var tracking_logs_table = $('#tracking-logs-table');
            $(tracking_logs_table).find('thead tr').clone(true).appendTo('#tracking-logs-table thead');
            $(tracking_logs_table).find('thead tr:eq(1) th').each(function (i) {
                const title = $(this).data('name');
                if (title === "Date") {
                    $(this).html('<input type="text" class="form-control tracing-dtp" placeholder="Search ' + title + '" />');
                } else {
                    $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
                }

                $(".tracing-dtp").bootstrapMaterialDatePicker({
                    weekStart: 0,
                    time: false,
                    clearButton: true,
                    format: "YYYY-MM-DD",
                    maxDate: new Date()
                });

                $('input', this).on('keyup change', function () {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            });

            $(".tracing-dtp-deadline").bootstrapMaterialDatePicker({
                weekStart: 0,
                time: false,
                clearButton: true,
                format: "YYYY-MM-DD",
                minDate: new Date()
            });

            $(tracking_logs_table).find('thead tr:eq(1) th:lt(2)').text('')

            var group_id = '{{ ($group_id) ? $group_id : '?' }}'
            var url = '/admin/log_tracking_get_tracing'

            if (group_id !== '?') url = '/admin/log_tracking_get_tracing_by/' + group_id

            var table = $('#tracking-logs-table').DataTable({
                ajax: url,
                dom: '<"top"f>rt<"bottom"lp>',
                orderCellsTop: true,
                fixedHeader: true,
                columnDefs: [
                    {
                        targets: 0,
                        orderable: false,
                        className: 'dt-body-center select-checkbox',
                        data: function () {
                            return ''
                        }
                    },
                    {
                        targets: 1,
                        className: 'dt-body-center',
                        data: function (row, type, set) {
                            if (row.error.priority === 'P4') return '';

                            var color = ''

                            switch (row.error.priority) {
                                case 'P1':
                                    color = 'danger';
                                    break;
                                case 'P2':
                                    color = 'warning'
                                    break;
                                case 'P3':
                                    color = 'secondary'
                                    break;
                            }

                            return `<span class="btn btn-xs btn-${color} waves-effect waves-light">${row.error.priority}</span>`
                        }
                    },
                    {
                        targets: 2,
                        data: function (row, type, set) {
                            return moment(row.created_at).format('YYYY-MM-DD hh:mm:ss') + "<br/><br/><span class='text-warning'>Last Check</span><br/>" + moment(row.updated_at).format('DD-MM-YYYY hh:mm:ss')
                        }
                    },
                    {
                        targets: 3,
                        data: 'manifest_detail.so'
                    },
                    {
                        targets: 4,
                        className: 'tracing_awb',
                        data: function (row, type, set) {
                            var courier = ""
                            var awb = "";

                            switch (row.awb.length) {
                                case 10:
                                    courier = "(DHL)"
                                    break;
                                case 12:
                                    courier = "(FEDEX)"
                                    break;
                                default:
                                    courier = ""
                                    break;
                            }

                            if (!row.awb_return) {
                                awb = courier + ' <label data-awb="' + row.awb + '">' + row.awb + '</label>'
                            } else {
                                awb = row.awb + '<br/>' + courier + ' <label data-awb="' + row.awb + '">' + row.awb_return + ' (RTO)</label>'
                            }
                            return awb
                        }
                    },
                    {
                        targets: 5,
                        data: function (row, type, set) {
                            var action = row.action ? row.action : "-";
                            var action_description = row.action_description ? row.action_description : "-";
                            return `<span class='text-blue'>Status : ${row.message}</span><br/><br/><span class='text-warning'>Action : ${action}</span><hr/><span class='text-warning'>Description : ${action_description}</span>`
                        }
                    },
                    {
                        targets: 6,
                        className: 'tracing_awb',
                        orderable: false,
                        data: function (row, type, set) {
                            return ((row.deadline) ? row.deadline : "-") + `<br/><br/><span class=\"edit\" data-awb=\"${row.awb}\"><i class=\"fa fa-edit\"></i> Edit</span>`
                        }
                    },
                    {
                        targets: 7,
                        className: 'tracing_awb',
                        orderable: false,
                        data: function (row, type, set) {
                            return ((row.note) ? row.note : "-") + `<br/><br/><span class=\"edit\" data-awb=\"${row.awb}\"><i class=\"fa fa-edit\"></i> Edit</span>`
                        }
                    },
                    {
                        targets: 8,
                        data: function (row, type, set) {
                            if (row.assignee) {
                                return row.assignee.name
                            }
                            return '-'
                        }
                    },
                    {
                        targets: 9,
                        data: function (row, type, set) {
                            var given = moment(row.manifest_detail.updated_at);
                            var current = (row.deleted_at) ? moment(row.deleted_at) : moment().startOf('day');

                            return current.diff(given, 'days') + " Days<br/><br/>To : " + row.manifest_detail.country
                        }
                    }
                ],
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                order: [
                    [2, 'asc'],
                    [3, 'asc'],
                    [9, 'asc']
                ]
            });

            $(document).on('click', '#tracking-logs-table tbody tr td.tracing_awb > label, #tracking-logs-table tbody tr td.tracing_awb > span.edit', function () {
                $.ajax({
                    url: '/admin/log_tracking_get_tracing/' + $(this).data('awb'),
                    method: 'get'
                }).done(function (data) {
                    $('#customer').text(data.data.manifest_detail.manifest.customer.firstname)
                    $('#email').text(data.data.manifest_detail.manifest.customer.email)
                    $('#mobile').text(data.data.manifest_detail.manifest.customer.phone)
                    $('#so').text(data.data.manifest_detail.so)
                    $('#deadline').val(data.data.deadline)
                    $('#note').val(data.data.note)
                    $('#awb').val(data.data.awb)
                    $('#awb_detail').val(data.data.awb)
                    $('#awb_return').val(data.data.awb)
                    if (data.data.awb_return) {
                        $('#number_awb').text(data.data.awb_return)
                        $('#return').hide();
                    } else {
                        $('#number_awb').text(data.data.awb);
                        $('#return').show();
                        $('#save_awb').hide();
                    }
                    $('#frm-tracing-complete').attr('action', "/admin/log_tracking_get_tracing/" + data.data.awb);
                    $('#frm-tracing-detail').attr('action', "/admin/log_tracking_get_tracing/" + data.data.awb);
                    $('#frm_update_returned_awb').attr('action', "/admin/log_tracking_get_tracing/" + data.data.awb);
                    $('#activities_panel').html("");

                    var com_inv_parent = $('embed#com_invoice').parent();
                    var new_el = "<embed id=\"com_invoice\" src=\"/uploads/invoice/" + data.data.manifest_detail.invoice_file + "\" style=\"width: 100%\" height=\"400\" type=\"application/pdf\">"

                    $('embed#com_invoice').remove();
                    com_inv_parent.append(new_el);

                    $('#gallery-section').html('');

                    if (data.data.manifest_detail.payments.length) {
                        $.each(data.data.manifest_detail.payments, function (k, v) {
                            $('#gallery-section').append('<div class="col-lg-4 col-md-6 p-0 nf-item">\n' +
                                '<div class="item-box">\n' +
                                '<a class="cbox-gallary1 mfp-image" href="/uploads/payment/' + v.filename + '">\n' +
                                '<img class="item-container " src="/uploads/payment/' + v.filename + '"/>\n' +
                                '</a>\n' +
                                '</div>\n' +
                                '</div>')
                        })
                    }

                    if (data.data.manifest_detail.sos.length) {
                        $.each(data.data.manifest_detail.sos, function (k, v) {
                            $('#gallery-section').append('<div class="col-lg-4 col-md-6 p-0 nf-item">\n' +
                                '<div class="item-box">\n' +
                                '<a class="cbox-gallary1 mfp-image" href="/uploads/so/' + v.filename + '">\n' +
                                '<img class="item-container " src="/uploads/so/' + v.filename + '"/>\n' +
                                '</a>\n' +
                                '</div>\n' +
                                '</div>')
                        })
                    }

                    if (data.data.logs.length) {
                        $.each(data.data.logs, function (k, v) {
                            var event = "Status : " + v.status_update + "<br/>"
                            event += "Comment : " + v.comment + "<br/>"
                            event += "Date : " + v.created_at + "<br/>"
                            event += "By : " + v.operator.name + "<br/>"
                            event += "<br/>"
                            $('#activities_panel').prepend(event);
                        });
                    }
                });
                if (this.nodeName === 'LABEL') {
                    $("#tracing_modal").modal("show");
                } else {
                    $("#modal_deadline").modal("show");
                }
            });

            var awb = $('#awb');

            $('#save_tracing_log').click(function () {
                var d = new Date();
                var y = d.toLocaleDateString();
                var t = d.toLocaleTimeString();
                var status_update = $("#status_update").val();
                var comment = $("#comment").val();
                var op = $('#op').val();
                if (status_update != '' && comment != '') {
                    $.ajax({
                        type: "POST",
                        data: $(this).parent().serialize(),
                        url: '/admin/log_tracking_generate_event/',
                        success: function (data) {
                            console.log(data)
                            if (data) {
                                var event = "Status : " + data.status_update + "<br/>"
                                event += "Comment : " + data.comment + "<br/>"
                                event += "Date : " + data.created_at + "<br/>"
                                event += "By : " + data.operator.name + "<br/>"
                                event += "<br/>"
                                $('#activities_panel').prepend(event);
                                $('#status_update').val('');
                                $('#comment').val('');
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'เกิดข้อผิดพลาด...',
                                    text: 'โปรดลองใหม่อีกครั้งภายหลัง!',
                                })
                            }
                        },
                        error: function (e) {
                            Swal.fire({
                                icon: 'error',
                                title: 'เกิดข้อผิดพลาด...',
                                text: 'โปรดลองใหม่อีกครั้งภายหลัง!',
                            })
                        }
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'เกิดข้อผิดพลาด...',
                        text: 'จำเป็นต้องกรอกข้อมูลทั้ง 2 ช่อง!',
                    })
                }
            })

            $("#close").click(function () {
                $('#deadline').val('');
                $('#note').val('');
                $('#status_update').val('');
                $('#comment').val('');
                $("#return").show();
                $("#save_awb").hide();
                $('#activities_panel').empty();
                $('#number_awb').empty();
            });

            $("#return").click(function () {
                $("#return").hide();
                $("#save_awb").show();
                $('#number_awb').empty();
                html = '<input class="" name="awb">';
                $('#number_awb').append(html);
            })

            $("#notification").hide();

            // Handle form submission event
            $('#frm-assign').on('submit', function (e) {
                var form = this;
                var rows_selected = table.rows({
                    selected: true
                }).data();
                if (rows_selected.length) {
                    $(form).find('input[name=tracking_log_id]').remove();
                    $.each(rows_selected, function (k, v) {
                        $(form).append(
                            $('<input>')
                                .attr('type', 'hidden')
                                .attr('name', 'tracking_log_id[]')
                                .val(v.id)
                        );
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'เกิดข้อผิดพลาด...',
                        text: 'ดูเหมือนคุณยังไม่ได้เลือกรายการใดๆเลย!',
                    })
                    return false;
                }
            });

            $('.mfp-image').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-fade',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0, 1]
                },
                callbacks: {
                    open: function () {
                        // Disabling focus enforcement by magnific
                        $.magnificPopup.instance._onFocusIn = function (e) {
                        };
                    }
                }
            });

            $('#selected_mail').on('change', function () {
                var id = $(this).val();
                var obj = $("id" + id);
                var code_number = $('#code' + id).val();
                var sms = $('#sms_' + id).val();
                var email = $('#email_' + id).text();
                var topic = $('#email_topic' + id).val();
                var cc = $('#cc' + id).val();
                // var bcc = $('#bcc'+id).val();
                $('#id').val(obj);
                $('#teamplate_name').val(topic);
                $('#toppic_mail').val(topic);
                $("#elm2_ifr").contents().find("#tinymce").html(email);
                $('#sms_detail').val(sms);
                $('#cc').val(cc);
                // $('#bcc').val(bcc);
            });

            $("#sent_mail").submit(function () {
                var url = $('#sent_mail').attr('ation')
                $.ajax({
                    type: "POST",
                    data: $('#form_sentmaill').serialize(),
                    url: url,
                    success: function (result) {
                        console.log(result);
                        alert("ส่งเสร็จสิ้น");
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    }
                });
            })

            // Set tinymce
            tinymce.init({
                selector: 'textarea#elm2',
                height: 300
            });
        });
            // Model Function Email
            function open_mail(n) {
                    if (n == 1) {
                        $("#group_mail").text("DHL");
                        $("#group_name").val("DHL");
                    } else if (n == 2) {
                        $("#group_mail").text("Fedex");
                        $("#group_name").val("Fedex");
                    } else if (n == 3) {
                        $("#group_mail").text("TNT");
                        $("#group_name").val("TNT");
                    } else if (n == 4) {
                        $("#group_mail").text("Greut");
                        $("#group_name").val("Greut");
                    }
                    $('#footer').css('display', 'none');
                    $('#modal_mail').modal('show');
                }

                function check_sms() {
                if ($('#tel').val() != '' && $('#sms_detail').val() != '') {
                    $('#page_topic').text('ส่งอีเมล์ และ SMS');
                } else {
                    $('#page_topic').text('ส่งอีเมล์');
                }
            }
    </script>
@endsection
