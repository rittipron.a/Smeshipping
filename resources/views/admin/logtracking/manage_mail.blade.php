@extends('layouts.app')

@section('content')
    <style>
        .button_ {
            width: 100%;
        }
    </style>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0);">Admin</a></li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0);">Mail Message</a>
                        </li>

                    </ol>
                </div>
                <h4 class="page-title">Mail Message</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Edit Table With Button</h4>
                    <p class="text-muted mb-3">Add toolbar column with edit and delete buttons.</p>
                    <button type="button" class="btn btn-primary waves-effect waves-light float-left mb-3"
                            data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg" id="add">+
                        Add
                        New
                    </button>
                    <div class="table-responsive">
                        <table class="table mb-0" id="my-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Group Name</th>
                                <th>Teamplate Name</th>
                                <th class="tabledit-toolbar-column"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td id="{{$item->id}}" teamplate_name="{{$item->teamplate_name}}"
                                        group_name="{{$item->group_name}}"
                                        email_topic="{{$item->email_topic}}" teamplate_mail="{{$item->teamplate_mail}}"
                                        teamplate_sms="{{$item->teamplate_sms}}"
                                        cc="{{$item->cc}}" bcc="{{$item->bcc}}">{{$item->id}}</td>
                                    <td>{{$item->group_name}}</td>
                                    <td>{{$item->teamplate_name}}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" style="float: none;">
                                            <button type="button" class="btn btn-sm btn-info"
                                                    style="float: none; margin: 4px;" data-toggle="modal"
                                                    data-animation="bounce" data-target=".edit_employee_modal"
                                                    onclick="editmode({{$item->id}})">
                                                <span class="ti-pencil"></span></button>
                                            <button type="button" class="btn btn-sm btn-danger deleteitem"
                                                    style="float: none; margin: 4px;" delid="{{$item->id}}">
                                                <span class="ti-trash"></span></button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end table-->
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!-- end col -->
    </div>





    <!--  Modal content for the above example -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Mail Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form action="{{url('/admin/log_tracking_mail')}}" name="log_tracking_form" id="log_tracking_form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="group_name">Group Name</label>
                                    <select name="group_name" id="group_name" class="form-control">
                                        <option value="DHL">DHL</option>
                                        <option value="Fedex">Fedex</option>
                                        <option value="TNT">TNT</option>
                                        <option value="Greut">Greut</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--  เนื้อหา Mail -->
                        <div class="row">

                            <div class="col-12">
                                <span>Toppic email : </span><input type="text" name="toppic" id="toppic_mail"
                                                                   class="form-control" value="">
                            </div>

                            <div class="col-12">
                                <span>CC : </span><input type="email" name="cc" id="cc" class="form-control" value="">
                            </div>

                            <div class="col-12">
                            <textarea id="elm2" name="email_detail">
                            </textarea>
                                <div>
                                </div>
                                <!-- end -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-sm btn-primary button_">Save</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-sm btn-danger button_" data-dismiss="modal"
                                                aria-hidden="true">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                                <button type="reset" class="btn btn-sm btn-primary" id="reset" style="display:none">
                                    reset
                                </button>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" id="id" name="id" required="">
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#footer').css('display', 'none');
        });
        $('#log_tracking_form').submit(function (e) {
            e.preventDefault();
            var thisform = $(this);
            var url = thisform.attr('action');
            var method = 'POST';
            if ($('#id').val() != '') {
                method = 'PUT'
            }

            $('#loading').show();

            var id = $('#id').val();
            var toppic_mail = $('#toppic_mail').val();
            var cc = $('#cc').val();
            var group_name = $('#group_name').val();
            var email_detail = $("#elm2_ifr").contents().find("#tinymce").html();

            $.ajax({
                type: method,
                // data: $('#log_tracking_form').serialize(),
                data: {
                    toppic: toppic_mail,
                    cc: cc,
                    group_name: group_name,
                    email_detail: email_detail,
                    id: id
                },
                url: url,
                success: function (result) {
                    $('#loading').hide();
                    window.location.reload(true);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
        });

        function editmode(id) {
            $('#group_name option').removeAttr("selected");

            $('#add').click();
            var obj = $('#' + id);
            $('#id').val(id);
            $('#group_name option[value="' + obj.attr('group_name') + '"]').attr("selected", "selected");
            $('#message').val(obj.attr('message'));
            $('#teamplate_name').val(obj.attr('teamplate_name'));
            $('#toppic_mail').val(obj.attr('email_topic'));
            $("#elm2_ifr").contents().find("#tinymce").html(obj.attr('teamplate_mail'));
            $('#teamplate_sms').val(obj.attr('teamplate_sms'));
            $('#cc').val(obj.attr('cc'));
            $('#bcc').val(obj.attr('bcc'));
        }

        $('.deleteitem').click(function (e) {
            var x = confirm("Are you sure you want to delete?");
            if (x) {
                var id = $(this).attr('delid');
                var url = $('#log_tracking_form').attr('action');
                // url = url + '/' + id;
                $('#loading').show();
                $.ajax({
                    type: 'DELETE',
                    data: {
                        id: id
                    },
                    url: url,
                    success: function (result) {
                        $('#loading').hide();
                        window.location.reload(true);
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                        $('#loading').hide();
                    }
                });
            } else
                return false;

        });
        $('#add').click(function () {
            $('#reset').click();
            $('#id').val('');
        });
    </script>

@endsection
