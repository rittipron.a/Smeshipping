@extends('layouts.app')

@section('content')
    <style>
        .number_query {
            border-radius: 10px !important;
            background-color: #f22727 !important;
        }
    </style>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0);">Admin</a></li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0);">Error Message</a>
                        </li>

                    </ol>
                </div>
                <h4 class="page-title">Error Message</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Edit Table With Button</h4>
                    <p class="text-muted mb-3">Add toolbar column with edit and delete buttons.</p>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary waves-effect waves-light float-left mb-3 mr-3"
                                    data-toggle="modal" data-animation="bounce" data-target=".modalGroup"
                                    id="add-group">+ Add
                                New Filter Group
                            </button>
                            <button type="button" class="btn btn-primary waves-effect waves-light float-left mb-3"
                                    data-toggle="modal" data-animation="bounce" data-target=".modalFilter" id="add">+
                                Add
                                New Filter
                            </button>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        @foreach($groups as $group)
                            <div class="col-md-3">
                                <div class="card" id="gid{{ $group->id }}">
                                    <h5 class="card-header bg-primary text-white mt-0 number_query">
                                        <span class="group_name">{{ $group->name }}</span> <span
                                            class="pt-2 text-muted">({{ number_format($group->filters->count()) }})</span>
                                        @if ($group->isComplete)
                                            <i class="fa fa-check-square"></i>
                                        @endif
                                        <div class="float-right">
                                            <div class="btn-group btn-group-xs" style="float: none;">
                                                <button type="button" class="btn btn-xs btn-outline-danger"
                                                        style="float: none; margin: 4px;" data-toggle="modal"
                                                        data-animation="bounce" data-target=".edit_employee_modal"
                                                        onclick="editmode_group({{$group->id}})">
                                                    <span class="ti-pencil"></span></button>
                                                <button type="button"
                                                        class="btn btn-xs btn-outline-danger deleteitem_group"
                                                        style="float: none; margin: 4px;" delid="{{$group->id}}">
                                                    <span class="ti-trash"></span></button>
                                            </div>
                                        </div>
                                    </h5>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="table-responsive">
                        <table class="table mb-0" id="my-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Priority</th>
                                <th>Message</th>
                                <th>Group</th>
                                <th class="tabledit-toolbar-column"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($message as $item)
                                <tr>
                                    <td id="{{$item->id}}" message="{{$item->message}}"
                                        priority="{{$item->priority}}" group_id="{{$item->group_id}}">{{$item->id}}</td>
                                    <td>{{$item->priority}}</td>
                                    <td>{{$item->message}}</td>
                                    <td>{{$item->group->name}}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" style="float: none;">
                                            <button type="button" class="btn btn-sm btn-info"
                                                    style="float: none; margin: 4px;" data-toggle="modal"
                                                    data-animation="bounce" data-target=".edit_employee_modal"
                                                    onclick="editmode({{$item->id}})">
                                                <span class="ti-pencil"></span></button>
                                            <button type="button" class="btn btn-sm btn-danger deleteitem"
                                                    style="float: none; margin: 4px;" delid="{{$item->id}}">
                                                <span class="ti-trash"></span></button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end table-->
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!-- end col -->
    </div>





    <!--  Modal content for the above example -->
    <div class="modal fade modalGroup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Filter Group</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form action="{{url('/admin/log_tracking_group')}}" name="log_tracking_form_group"
                          id="log_tracking_form_group">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="LeadName">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" required="">
                                    <input type="hidden" class="form-control" id="group_id" name="group_id" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-check-inline my-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="isComplete"
                                                   name="isComplete">
                                            <label class="custom-control-label" for="isComplete">Is a complete
                                                trace</label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        <button type="reset" class="btn btn-sm btn-primary" id="reset" style="display:none">reset
                        </button>
                        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-hidden="true">
                            Cancel
                        </button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!--  Modal content for the above example -->
    <div class="modal fade modalFilter" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Filter Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form action="{{url('/admin/log_tracking')}}" name="log_tracking_form" id="log_tracking_form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="group">Group</label>
                                    <select name="group" id="group" class="form-control" required>
                                        @foreach($groups as $group)
                                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                                        @endforeach()
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="priority">Priority</label>
                                    <select name="priority" id="priority" class="form-control">
                                        <option value="P4">P4</option>
                                        <option value="P3">P3</option>
                                        <option value="P2">P2</option>
                                        <option value="P1">P1</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="LeadName">Message</label>
                                    <input type="text" class="form-control" id="message" name="message" required="">
                                    <input type="hidden" class="form-control" id="id" name="id" required="">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                        <button type="reset" class="btn btn-sm btn-primary" id="reset" style="display:none">reset
                        </button>
                        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-hidden="true">
                            Cancel
                        </button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
    <script>
        $('#log_tracking_form').submit(function (e) {
            e.preventDefault();
            var thisform = $(this);
            var url = thisform.attr('action');
            var method = 'POST';
            if ($('#id').val() != '') {
                url = url + '/' + $('#id').val();
                method = 'PUT'
            }

            $('#loading').show();
            $.ajax({
                type: method,
                data: $('#log_tracking_form').serialize(),
                url: url,
                success: function (result) {
                    $('#loading').hide();
                    window.location.reload(true);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
        });

        $('#log_tracking_form_group').submit(function (e) {
            e.preventDefault();
            var thisform = $(this);
            var url = thisform.attr('action');
            var method = 'POST';
            if ($('#group_id').val() != '') {
                // url = url + '/' + $('#group_id').val();
                method = 'PUT'
            }

            $('#loading').show();
            $.ajax({
                type: method,
                data: $('#log_tracking_form_group').serialize(),
                url: url,
                success: function (result) {
                    $('#loading').hide();
                    window.location.reload(true);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
        });

        function editmode(id) {
            $('#priority option').removeAttr("selected");
            $('#group option').removeAttr("selected");

            $('#add').click();
            var obj = $('#' + id);
            $('#id').val(id);
            $('#priority option[value="' + obj.attr('priority') + '"]').attr("selected", "selected");
            $('#group option[value="' + obj.attr('group_id') + '"]').attr("selected", "selected");
            $('#message').val(obj.attr('message'));
        }

        function editmode_group(id) {
            $('#add-group').click();
            var obj = $('#gid' + id);
            $('#group_id').val(id);
            $('#name').val($(obj).find('h5 > span.group_name').text());
            if ($(obj).find('h5').has('i').length) {
                $('#isComplete').prop("checked", true)
            } else {
                $('#isComplete').prop("checked", false)
            }
        }

        $('.deleteitem').click(function (e) {
            var x = confirm("Are you sure you want to delete?");
            if (x) {
                var id = $(this).attr('delid');
                var url = $('#log_tracking_form').attr('action');
                url = url + '/' + id;
                $('#loading').show();
                $.ajax({
                    type: 'DELETE',
                    url: url,
                    success: function (result) {
                        $('#loading').hide();
                        window.location.reload(true);
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                        $('#loading').hide();
                    }
                });
            } else
                return false;

        });

        $('.deleteitem_group').click(function (e) {
            var x = confirm("Are you sure you want to delete? all filter that's belong to this group will be deleted.");
            if (x) {
                var id = $(this).attr('delid');
                var url = $('#log_tracking_form_group').attr('action');
                // url = url + '/' + id;
                $('#loading').show();
                $.ajax({
                    type: 'DELETE',
                    data: {group_id: id},
                    url: url,
                    success: function (result) {
                        $('#loading').hide();
                        window.location.reload(true);
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                        $('#loading').hide();
                    }
                });
            } else
                return false;

        });

        $('#add').click(function () {
            $('#reset').click();
            $('#id').val('');
        });

        $('#add-group').click(function () {
            $('#reset').click();
            $('#group_id').val('');
        });
    </script>

@endsection
