@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Country</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Country</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <select class="select2 form-control mb-3 custom-select select2-hidden-accessible" id="sh_country"
                    name="sh_country" tabindex="-1" aria-hidden="true">
                    <option value="" code="">Select</option>
                    <option value="AFGHANISTAN" code="AF" iata="AF">
                        AFGHANISTAN</option>
                    <option value="ALBANIA" code="AL" iata="AL">
                        ALBANIA</option>
                    <option value="ALGERIA" code="DZ" iata="DZ">
                        ALGERIA</option>
                    <option value="AMERICAN SAMOA" code="AS" iata="AS">
                        AMERICAN SAMOA</option>
                    <option value="ANDORRA" code="AD" iata="AD">
                        ANDORRA</option>
                    <option value="ANGOLA" code="AO" iata="AO">
                        ANGOLA</option>
                    <option value="ANGUILLA" code="AI" iata="AI">
                        ANGUILLA</option>
                    <option value="ANTIGUA" code="AG" iata="AG">
                        ANTIGUA</option>
                    <option value="ARGENTINA" code="AR" iata="AR">
                        ARGENTINA</option>
                    <option value="ARMENIA" code="AM" iata="AM">
                        ARMENIA</option>
                    <option value="ARUBA" code="AW" iata="AW">
                        ARUBA</option>
                    <option value="AUSTRALIA" code="AU" iata="AU">
                        AUSTRALIA</option>
                    <option value="AUSTRIA" code="AT" iata="AT">
                        AUSTRIA</option>
                    <option value="AZERBAIJAN" code="AZ" iata="AZ">
                        AZERBAIJAN</option>
                    <option value="BAHAMAS" code="BS" iata="BS">
                        BAHAMAS</option>
                    <option value="BAHRAIN" code="BH" iata="BH">
                        BAHRAIN</option>
                    <option value="BANGLADESH" code="BD" iata="BD">
                        BANGLADESH</option>
                    <option value="BARBADOS" code="BB" iata="BB">
                        BARBADOS</option>
                    <option value="BELARUS" code="BY" iata="BY">
                        BELARUS</option>
                    <option value="BELGIUM" code="BE" iata="BE">
                        BELGIUM</option>
                    <option value="BELIZE" code="BZ" iata="BZ">
                        BELIZE</option>
                    <option value="BENIN" code="BJ" iata="BJ">
                        BENIN</option>
                    <option value="BERMUDA" code="BM" iata="BM">
                        BERMUDA</option>
                    <option value="BHUTAN" code="BT" iata="BT">
                        BHUTAN</option>
                    <option value="BOLIVIA" code="BO" iata="BO">
                        BOLIVIA</option>
                    <option value="BONAIRE" code="XB" iata="XB">
                        BONAIRE</option>
                    <option value="BOSNIA AND HERZEGOVINA" code="BA" iata="BA">
                        BOSNIA AND HERZEGOVINA</option>
                    <option value="BOTSWANA" code="BW" iata="BW">
                        BOTSWANA</option>
                    <option value="BRAZIL" code="BR" iata="BR">
                        BRAZIL</option>
                    <option value="BRUNEI" code="BN" iata="BN">
                        BRUNEI</option>
                    <option value="BULGARIA" code="BG" iata="BG">
                        BULGARIA</option>
                    <option value="BURKINA FASO" code="BF" iata="BF">
                        BURKINA FASO</option>
                    <option value="BURUNDI" code="BI" iata="BI">
                        BURUNDI</option>
                    <option value="CAMBODIA" code="KH" iata="KH">
                        CAMBODIA</option>
                    <option value="CAMEROON" code="CM" iata="CM">
                        CAMEROON</option>
                    <option value="CANADA" code="CA" iata="CA">
                        CANADA</option>
                    <option value="CANARY ISLANDS, THE" code="IC" iata="IC">
                        CANARY ISLANDS, THE</option>
                    <option value="CAPE VERDE" code="CV" iata="CV">
                        CAPE VERDE</option>
                    <option value="CAYMAN ISLANDS" code="KY" iata="KY">
                        CAYMAN ISLANDS</option>
                    <option value="CENTRAL AFRICAN REPUBLIC" code="CF" iata="CF">
                        CENTRAL AFRICAN REPUBLIC</option>
                    <option value="CHAD" code="TD" iata="TD">
                        CHAD</option>
                    <option value="CHILE" code="CL" iata="CL">
                        CHILE</option>
                    <option value="CHINA, PEOPLES REPUBLIC" code="CN" iata="CN">
                        CHINA, PEOPLES REPUBLIC</option>
                    <option value="COLOMBIA" code="CO" iata="CO">
                        COLOMBIA</option>
                    <option value="COMMONWEALTH NO. MARIANA ISLAN" code="MP" iata="MP">
                        COMMONWEALTH NO. MARIANA ISLAN</option>
                    <option value="COMOROS" code="KM" iata="KM">
                        COMOROS</option>
                    <option value="CONGO" code="CG" iata="CG">
                        CONGO</option>
                    <option value="CONGO, THE DEMOCRATIC REPUBLIC" code="CD" iata="CD">
                        CONGO, THE DEMOCRATIC REPUBLIC</option>
                    <option value="COOK ISLANDS" code="CK" iata="CK">
                        COOK ISLANDS</option>
                    <option value="COSTA RICA" code="CR" iata="CR">
                        COSTA RICA</option>
                    <option value="COTE D IVOIRE" code="CI" iata="CI">
                        COTE D IVOIRE</option>
                    <option value="CROATIA" code="HR" iata="HR">
                        CROATIA</option>
                    <option value="CUBA" code="CU" iata="CU">
                        CUBA</option>
                    <option value="CURACAO" code="XC" iata="XC">
                        CURACAO</option>
                    <option value="CYPRUS" code="CY" iata="CY">
                        CYPRUS</option>
                    <option value="CZECH REPUBLIC, THE" code="CZ" iata="CZ">
                        CZECH REPUBLIC, THE</option>
                    <option value="DENMARK" code="DK" iata="DK">
                        DENMARK</option>
                    <option value="DJIBOUTI" code="DJ" iata="DJ">
                        DJIBOUTI</option>
                    <option value="DOMINICA" code="DM" iata="DM">
                        DOMINICA</option>
                    <option value="DOMINICAN REPUBLIC" code="DO" iata="DO">
                        DOMINICAN REPUBLIC</option>
                    <option value="ECUADOR" code="EC" iata="EC">
                        ECUADOR</option>
                    <option value="EGYPT" code="EG" iata="EG">
                        EGYPT</option>
                    <option value="EL SALVADOR" code="SV" iata="SV">
                        EL SALVADOR</option>
                    <option value="ERITREA" code="ER" iata="ER">
                        ERITREA</option>
                    <option value="ESTONIA" code="EE" iata="EE">
                        ESTONIA</option>
                    <option value="ETHIOPIA" code="ET" iata="ET">
                        ETHIOPIA</option>
                    <option value="FALKLAND ISLANDS" code="FK" iata="FK">
                        FALKLAND ISLANDS</option>
                    <option value="FAROE ISLANDS" code="FO" iata="FO">
                        FAROE ISLANDS</option>
                    <option value="FIJI" code="FJ" iata="FJ">
                        FIJI</option>
                    <option value="FINLAND" code="FI" iata="FI">
                        FINLAND</option>
                    <option value="FRANCE" code="FR" iata="FR">
                        FRANCE</option>
                    <option value="FRENCH GUYANA" code="GF" iata="GF">
                        FRENCH GUYANA</option>
                    <option value="GABON" code="GA" iata="GA">
                        GABON</option>
                    <option value="GAMBIA" code="GM" iata="GM">
                        GAMBIA</option>
                    <option value="GEORGIA" code="GE" iata="GE">
                        GEORGIA</option>
                    <option value="GERMANY" code="DE" iata="DE">
                        GERMANY</option>
                    <option value="GHANA" code="GH" iata="GH">
                        GHANA</option>
                    <option value="GIBRALTAR" code="GI" iata="GI">
                        GIBRALTAR</option>
                    <option value="GREECE" code="GR" iata="GR">
                        GREECE</option>
                    <option value="GREENLAND" code="GL" iata="GL">
                        GREENLAND</option>
                    <option value="GRENADA" code="GD" iata="GD">
                        GRENADA</option>
                    <option value="GUADELOUPE" code="GP" iata="GP">
                        GUADELOUPE</option>
                    <option value="GUAM" code="GU" iata="GU">
                        GUAM</option>
                    <option value="GUATEMALA" code="GT" iata="GT">
                        GUATEMALA</option>
                    <option value="GUERNSEY" code="GG" iata="GG">
                        GUERNSEY</option>
                    <option value="GUINEA REPUBLIC" code="GN" iata="GN">
                        GUINEA REPUBLIC</option>
                    <option value="GUINEA-BISSAU" code="GW" iata="GW">
                        GUINEA-BISSAU</option>
                    <option value="GUINEA-EQUATORIAL" code="GQ" iata="GQ">
                        GUINEA-EQUATORIAL</option>
                    <option value="GUYANA (BRITISH)" code="GY" iata="GY">
                        GUYANA (BRITISH)</option>
                    <option value="HAITI" code="HT" iata="HT">
                        HAITI</option>
                    <option value="HONDURAS" code="HN" iata="HN">
                        HONDURAS</option>
                    <option value="HONG KONG" code="HK" iata="HK">
                        HONG KONG</option>
                    <option value="HUNGARY" code="HU" iata="HU">
                        HUNGARY</option>
                    <option value="ICELAND" code="IS" iata="IS">
                        ICELAND</option>
                    <option value="INDIA" code="IN" iata="IN">
                        INDIA</option>
                    <option value="INDONESIA" code="ID" iata="ID">
                        INDONESIA</option>
                    <option value="IRAN (ISLAMIC REPUBLIC OF)" code="IR" iata="IR">
                        IRAN (ISLAMIC REPUBLIC OF)</option>
                    <option value="IRAQ" code="IQ" iata="IQ">
                        IRAQ</option>
                    <option value="IRELAND, REPUBLIC OF" code="IE" iata="IE">
                        IRELAND, REPUBLIC OF</option>
                    <option value="ISRAEL" code="IL" iata="IL">
                        ISRAEL</option>
                    <option value="ITALY" code="IT" iata="IT">
                        ITALY</option>
                    <option value="JAMAICA" code="JM" iata="JM">
                        JAMAICA</option>
                    <option value="JAPAN" code="JP" iata="JP">
                        JAPAN</option>
                    <option value="JERSEY" code="JE" iata="JE">
                        JERSEY</option>
                    <option value="JORDAN" code="JO" iata="JO">
                        JORDAN</option>
                    <option value="KAZAKHSTAN" code="KZ" iata="KZ">
                        KAZAKHSTAN</option>
                    <option value="KENYA" code="KE" iata="KE">
                        KENYA</option>
                    <option value="KIRIBATI" code="KI" iata="KI">
                        KIRIBATI</option>
                    <option value="KOREA, REPUBLIC OF (SOUTH K.)" code="KR" iata="KR">
                        KOREA, REPUBLIC OF (SOUTH K.)</option>
                    <option value="KOREA, THE D.P.R OF (NORTH K.)" code="KP" iata="KP">
                        KOREA, THE D.P.R OF (NORTH K.)</option>
                    <option value="KOSOVO" code="KV" iata="KV">
                        KOSOVO</option>
                    <option value="KUWAIT" code="KW" iata="KW">
                        KUWAIT</option>
                    <option value="KYRGYZSTAN" code="KG" iata="KG">
                        KYRGYZSTAN</option>
                    <option value="LAO PEOPLES DEMOCRATIC REPUBLI" code="LA" iata="LA">
                        LAO PEOPLES DEMOCRATIC REPUBLI</option>
                    <option value="LATVIA" code="LV" iata="LV">
                        LATVIA</option>
                    <option value="LEBANON" code="LB" iata="LB">
                        LEBANON</option>
                    <option value="LESOTHO" code="LS" iata="LS">
                        LESOTHO</option>
                    <option value="LIBERIA" code="LR" iata="LR">
                        LIBERIA</option>
                    <option value="LIBYA" code="LY" iata="LY">
                        LIBYA</option>
                    <option value="LIECHTENSTEIN" code="LI" iata="LI">
                        LIECHTENSTEIN</option>
                    <option value="LITHUANIA" code="LT" iata="LT">
                        LITHUANIA</option>
                    <option value="LUXEMBOURG" code="LU" iata="LU">
                        LUXEMBOURG</option>
                    <option value="MACAU" code="MO" iata="MO">
                        MACAU</option>
                    <option value="MADAGASCAR" code="MG" iata="MG">
                        MADAGASCAR</option>
                    <option value="MALAWI" code="MW" iata="MW">
                        MALAWI</option>
                    <option value="MALAYSIA" code="MY" iata="MY">
                        MALAYSIA</option>
                    <option value="MALDIVES" code="MV" iata="MV">
                        MALDIVES</option>
                    <option value="MALI" code="ML" iata="ML">
                        MALI</option>
                    <option value="MALTA" code="MT" iata="MT">
                        MALTA</option>
                    <option value="MARSHALL ISLANDS" code="MH" iata="MH">
                        MARSHALL ISLANDS</option>
                    <option value="MARTINIQUE" code="MQ" iata="MQ">
                        MARTINIQUE</option>
                    <option value="MAURITANIA" code="MR" iata="MR">
                        MAURITANIA</option>
                    <option value="MAURITIUS" code="MU" iata="MU">
                        MAURITIUS</option>
                    <option value="MAYOTTE" code="YT" iata="YT">
                        MAYOTTE</option>
                    <option value="MEXICO" code="MX" iata="MX">
                        MEXICO</option>
                    <option value="MICRONESIA, FEDERATED STATES O" code="FM" iata="FM">
                        MICRONESIA, FEDERATED STATES O</option>
                    <option value="MOLDOVA, REPUBLIC OF" code="MD" iata="MD">
                        MOLDOVA, REPUBLIC OF</option>
                    <option value="MONACO" code="MC" iata="MC">
                        MONACO</option>
                    <option value="MONGOLIA" code="MN" iata="MN">
                        MONGOLIA</option>
                    <option value="MONTENEGRO, REPUBLIC OF" code="ME" iata="ME">
                        MONTENEGRO, REPUBLIC OF</option>
                    <option value="MONTSERRAT" code="MS" iata="MS">
                        MONTSERRAT</option>
                    <option value="MOROCCO" code="MA" iata="MA">
                        MOROCCO</option>
                    <option value="MOZAMBIQUE" code="MZ" iata="MZ">
                        MOZAMBIQUE</option>
                    <option value="MYANMAR" code="MM" iata="MM">
                        MYANMAR</option>
                    <option value="NAMIBIA" code="NA" iata="NA">
                        NAMIBIA</option>
                    <option value="NAURU, REPUBLIC OF" code="NR" iata="NR">
                        NAURU, REPUBLIC OF</option>
                    <option value="NEPAL" code="NP" iata="NP">
                        NEPAL</option>
                    <option value="NETHERLANDS, THE" code="NL" iata="NL">
                        NETHERLANDS, THE</option>
                    <option value="NEVIS" code="XN" iata="XN">
                        NEVIS</option>
                    <option value="NEW CALEDONIA" code="NC" iata="NC">
                        NEW CALEDONIA</option>
                    <option value="NEW ZEALAND" code="NZ" iata="NZ">
                        NEW ZEALAND</option>
                    <option value="NICARAGUA" code="NI" iata="NI">
                        NICARAGUA</option>
                    <option value="NIGER" code="NE" iata="NE">
                        NIGER</option>
                    <option value="NIGERIA" code="NG" iata="NG">
                        NIGERIA</option>
                    <option value="NIUE" code="NU" iata="NU">
                        NIUE</option>
                    <option value="NORTH MACEDONIA" code="MK" iata="MK">
                        NORTH MACEDONIA</option>
                    <option value="NORWAY" code="NO" iata="NO">
                        NORWAY</option>
                    <option value="OMAN" code="OM" iata="OM">
                        OMAN</option>
                    <option value="PAKISTAN" code="PK" iata="PK">
                        PAKISTAN</option>
                    <option value="PALAU" code="PW" iata="PW">
                        PALAU</option>
                    <option value="PANAMA" code="PA" iata="PA">
                        PANAMA</option>
                    <option value="PAPUA NEW GUINEA" code="PG" iata="PG">
                        PAPUA NEW GUINEA</option>
                    <option value="PARAGUAY" code="PY" iata="PY">
                        PARAGUAY</option>
                    <option value="PERU" code="PE" iata="PE">
                        PERU</option>
                    <option value="PHILIPPINES, THE" code="PH" iata="PH">
                        PHILIPPINES, THE</option>
                    <option value="POLAND" code="PL" iata="PL">
                        POLAND</option>
                    <option value="PORTUGAL" code="PT" iata="PT">
                        PORTUGAL</option>
                    <option value="PUERTO RICO" code="PR" iata="PR">
                        PUERTO RICO</option>
                    <option value="QATAR" code="QA" iata="QA">
                        QATAR</option>
                    <option value="REUNION, ISLAND OF" code="RE" iata="RE">
                        REUNION, ISLAND OF</option>
                    <option value="ROMANIA" code="RO" iata="RO">
                        ROMANIA</option>
                    <option value="RUSSIAN FEDERATION, THE" code="RU" iata="RU">
                        RUSSIAN FEDERATION, THE</option>
                    <option value="RWANDA" code="RW" iata="RW">
                        RWANDA</option>
                    <option value="SAINT HELENA" code="SH" iata="SH">
                        SAINT HELENA</option>
                    <option value="SAMOA" code="WS" iata="WS">
                        SAMOA</option>
                    <option value="SAN MARINO" code="SM" iata="SM">
                        SAN MARINO</option>
                    <option value="SAO TOME AND PRINCIPE" code="ST" iata="ST">
                        SAO TOME AND PRINCIPE</option>
                    <option value="SAUDI ARABIA" code="SA" iata="SA">
                        SAUDI ARABIA</option>
                    <option value="SENEGAL" code="SN" iata="SN">
                        SENEGAL</option>
                    <option value="SERBIA, REPUBLIC OF" code="RS" iata="RS">
                        SERBIA, REPUBLIC OF</option>
                    <option value="SEYCHELLES" code="SC" iata="SC">
                        SEYCHELLES</option>
                    <option value="SIERRA LEONE" code="SL" iata="SL">
                        SIERRA LEONE</option>
                    <option value="SINGAPORE" code="SG" iata="SG">
                        SINGAPORE</option>
                    <option value="SLOVAKIA" code="SK" iata="SK">
                        SLOVAKIA</option>
                    <option value="SLOVENIA" code="SI" iata="SI">
                        SLOVENIA</option>
                    <option value="SOLOMON ISLANDS" code="SB" iata="SB">
                        SOLOMON ISLANDS</option>
                    <option value="SOMALIA" code="SO" iata="SO">
                        SOMALIA</option>
                    <option value="SOMALILAND, REP OF (NORTH SOMA" code="XS" iata="XS">
                        SOMALILAND, REP OF (NORTH SOMA</option>
                    <option value="SOUTH AFRICA" code="ZA" iata="ZA">
                        SOUTH AFRICA</option>
                    <option value="SOUTH SUDAN" code="SS" iata="SS">
                        SOUTH SUDAN</option>
                    <option value="SPAIN" code="ES" iata="ES">
                        SPAIN</option>
                    <option value="SRI LANKA" code="LK" iata="LK">
                        SRI LANKA</option>
                    <option value="ST. BARTHELEMY" code="XY" iata="XY">
                        ST. BARTHELEMY</option>
                    <option value="ST. EUSTATIUS" code="XE" iata="XE">
                        ST. EUSTATIUS</option>
                    <option value="ST. KITTS" code="KN" iata="KN">
                        ST. KITTS</option>
                    <option value="ST. LUCIA" code="LC" iata="LC">
                        ST. LUCIA</option>
                    <option value="ST. MAARTEN" code="XM" iata="XM">
                        ST. MAARTEN</option>
                    <option value="ST. VINCENT" code="VC" iata="VC">
                        ST. VINCENT</option>
                    <option value="SUDAN" code="SD" iata="SD">
                        SUDAN</option>
                    <option value="SURINAME" code="SR" iata="SR">
                        SURINAME</option>
                    <option value="SWAZILAND" code="SZ" iata="SZ">
                        SWAZILAND</option>
                    <option value="SWEDEN" code="SE" iata="SE">
                        SWEDEN</option>
                    <option value="SWITZERLAND" code="CH" iata="CH">
                        SWITZERLAND</option>
                    <option value="SYRIA" code="SY" iata="SY">
                        SYRIA</option>
                    <option value="TAHITI" code="PF" iata="PF">
                        TAHITI</option>
                    <option value="TAIWAN" code="TW" iata="TW">
                        TAIWAN</option>
                    <option value="TANZANIA" code="TZ" iata="TZ">
                        TANZANIA</option>
                    <option value="THAILAND" code="TH" iata="TH">
                        THAILAND</option>
                    <option value="TIMOR LESTE" code="TL" iata="TL">
                        TIMOR LESTE</option>
                    <option value="TOGO" code="TG" iata="TG">
                        TOGO</option>
                    <option value="TONGA" code="TO" iata="TO">
                        TONGA</option>
                    <option value="TRINIDAD AND TOBAGO" code="TT" iata="TT">
                        TRINIDAD AND TOBAGO</option>
                    <option value="TUNISIA" code="TN" iata="TN">
                        TUNISIA</option>
                    <option value="TURKEY" code="TR" iata="TR">
                        TURKEY</option>
                    <option value="TURKMENISTAN" code="TM" iata="TM">
                        TURKMENISTAN</option>
                    <option value="TURKS AND CAICOS ISLANDS" code="TC" iata="TC">
                        TURKS AND CAICOS ISLANDS</option>
                    <option value="TUVALU" code="TV" iata="TV">
                        TUVALU</option>
                    <option value="UGANDA" code="UG" iata="UG">
                        UGANDA</option>
                    <option value="UKRAINE" code="UA" iata="UA">
                        UKRAINE</option>
                    <option value="UNITED ARAB EMIRATES" code="AE" iata="AE">
                        UNITED ARAB EMIRATES</option>
                    <option value="UNITED KINGDOM" code="GB" iata="GB">
                        UNITED KINGDOM</option>
                    <option value="UNITED STATES OF AMERICA" code="US" iata="US" selected="selected">
                        UNITED STATES OF AMERICA</option>
                    <option value="URUGUAY" code="UY" iata="UY">
                        URUGUAY</option>
                    <option value="UZBEKISTAN" code="UZ" iata="UZ">
                        UZBEKISTAN</option>
                    <option value="VANUATU" code="VU" iata="VU">
                        VANUATU</option>
                    <option value="VENEZUELA" code="VE" iata="VE">
                        VENEZUELA</option>
                    <option value="VIETNAM" code="VN" iata="VN">
                        VIETNAM</option>
                    <option value="VIRGIN ISLANDS (BRITISH)" code="VG" iata="VG">
                        VIRGIN ISLANDS (BRITISH)</option>
                    <option value="VIRGIN ISLANDS (US)" code="VI" iata="VI">
                        VIRGIN ISLANDS (US)</option>
                    <option value="YEMEN, REPUBLIC OF" code="YE" iata="YE">
                        YEMEN, REPUBLIC OF</option>
                    <option value="ZAMBIA" code="ZM" iata="ZM">
                        ZAMBIA</option>
                    <option value="ZIMBABWE" code="ZW" iata="ZW">
                        ZIMBABWE</option>

                </select>
                <input type="text" name="sh_postcode" id="sh_postcode" value="123456" />
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <div class="col-12">
    </div>
    <!-- end col -->
</div>


<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script>
    $(document).ready(function($){
        $('#sh_postcode').keyup(function(){ 
            $('#sh_postcode').unmask();
            this.value = this.value.toUpperCase();
        });
        $('#sh_postcode').change(function(){
            var sh_country = $('#sh_country option:selected');
            var sh_country_code = sh_country.attr('code');
            var sh_zipcode = $('#sh_postcode').val();
            $.ajax({
            type: "GET",
            url: '/checkformat/'+sh_country_code+'/'+sh_zipcode,
            processData: false,
            contentType: false,
            success: function (result) {
                console.log(result);
                if(result.status){
                    $('#sh_postcode').mask(result.value);
                }
            },
            error: function (e) {
                console.log("ERROR : ", e);
                $("#loading").hide();
            }
        });
        });
    });
</script>
@endsection