@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Country</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Country</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">Edit Table With Button</h4>
                <p class="text-muted mb-3">Add toolbar column with edit and delete buttons.</p>
                <div class="row">
                    <div class="col-md-2"> <button type="button"
                            class="btn btn-primary waves-effect waves-light float-left mb-3" data-toggle="modal"
                            data-animation="bounce" data-target=".bs-example-modal-lg" id="add">+ Add
                            New</button></div>
                    <div class="col-md-4">
                        <form action="{{url('admin/country')}}" method="GET">
                            <div class="input-group">
                                <input type="text" class="form-control" name="keyword" placeholder="Search for..."
                                    value="{{app('request')->input('keyword')}}" aria-label="ค้นหา">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="submit">ค้นหา</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>




                <div class="table-responsive">
                    <table class="table mb-0" id="my-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Iata</th>
                                <th>Code</th>
                                <th>Country</th>
                                <th>Note</th>
                                <th class="tabledit-toolbar-column"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($country as $item)
                            <tr>
                                <td id="{{$item->id}}" iata="{{$item->iata}}" code="{{$item->code}}"
                                    country="{{$item->country}}" note="{{$item->note}}"
                                    risk_price="{{$item->risk_price}}">{{$item->id}}</td>
                                <td>{{$item->iata}}</td>
                                <td>{{$item->code}}</td>
                                <td>{{$item->country}}</td>
                                <td><button type="button" class="btn btn-outline-success waves-effect waves-light"
                                        data-toggle="modal" data-animation="bounce" data-target=".view"
                                        onclick="set_view_note({{$item->id}})">view</button>
                                </td>
                                <td>
                                    <div class="btn-group btn-group-sm" style="float: none;">
                                        <button type="button" class="btn btn-sm btn-info"
                                            style="float: none; margin: 4px;" data-toggle="modal"
                                            data-animation="bounce" data-target=".edit_employee_modal"
                                            onclick="editmode({{$item->id}})">
                                            <span class="ti-pencil"></span></button>
                                        <button type="button" class="btn btn-sm btn-danger deleteitem"
                                            style="float: none; margin: 4px;" delid="{{$item->id}}">
                                            <span class="ti-trash"></span></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <div class="col-12">
        {{ $country->links() }}
    </div>
    <!-- end col -->
</div>



<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Country</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{url('/admin/country')}}" name="country_form" id="country_form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">Iata</label>
                                <input type="text" class="form-control" id="iata" name="iata" required="">
                                <input type="hidden" class="form-control" id="id" name="id" maxlength="3">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">Code</label>
                                <input type="text" class="form-control" id="code" name="code" required="" maxlength="2">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">Country</label>
                                <input type="text" class="form-control" id="country" name="country" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">Risk Price</label>
                                <input type="number" class="form-control" id="risk_price" name="risk_price" required="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="LeadName">Note</label>
                            <textarea id="elm1" name="note"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    <button type="reset" class="btn btn-sm btn-primary" id="reset" style="display:none">reset</button>
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-hidden="true">
                        Cancel</button>

                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--  Modal content for the above example -->
<div class="modal fade view" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Note</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div id="show_note"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>

<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script>
    $('#country_form').submit(function(e){
        e.preventDefault();
            var thisform = $(this);
            var url = thisform.attr('action');
            var method = 'POST';
            if($('#id').val()!=''){
                url = url+'/'+$('#id').val();
                method = 'PUT'
            }
            var content=$("#elm1_ifr").contents().find("#tinymce").html();
            $('#elm1').html(content);
            $('#loading').show();
            $.ajax({
                type: method,
                data: $('#country_form').serialize(), 
                url: url,
                success: function (result) {
                    $('#loading').hide();
                    window.location.reload(true);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
    });
    function set_view_note(id){
        var obj = $('#'+id);
        $('#show_note').html(obj.attr('note'));
    }
    function editmode(id){
        $('#add').click();
        var obj = $('#'+id);
        $('#id').val(id);
        $('#iata').val(obj.attr('iata'));
        $('#code').val(obj.attr('code'));
        $('#country').val(obj.attr('country'));
        $('#risk_price').val(obj.attr('risk_price'));
        $("#elm1_ifr").contents().find("#tinymce").html(obj.attr('note'));
        //$('#elm1').html(obj.attr('note'));
    }
    $('.deleteitem').click(function(e){
        var x = confirm("Are you sure you want to delete?");
        if (x){
        var id = $(this).attr('delid');
        var url = $('#country_form').attr('action');
        url = url+'/'+id;
        $('#loading').show();
            $.ajax({
                type: 'DELETE',
                url: url,
                success: function (result) {
                    $('#loading').hide();
                    window.location.reload(true);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
            }
        else
            return false;
        
    });
    $('#tinymce').change(function(){
        $('#elm1').html(this.html())
    })
    $('#add').click(function(){
        $('#reset').click();
        $('#id').val('');
    });
</script>
@endsection