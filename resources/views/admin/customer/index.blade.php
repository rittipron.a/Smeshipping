@extends('layouts.app')

@section('content')
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script type="text/javascript" src="{{ asset('assets/dependencies/JQL.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('assets/dependencies/typeahead.bundle.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/dist/jquery.Thailand.min.js') }}">
</script>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Customer</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Customer</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">Edit Table With Button</h4>
                <p class="text-muted mb-3">Add toolbar column with edit and delete buttons.</p>
                <div class="row">
                    <div class="col-md-1"><button type="button"
                            class="btn btn-primary waves-effect waves-light float-left mb-3" data-toggle="modal"
                            data-animation="bounce" data-target=".bs-example-modal-lg">+ Add New</button>
                    </div>
                    <div class="col-md-5">
                        <form action="{{url('/admin/customer/')}}" method="GET">

                            <div class="input-group" style="">

                                <input type="text" id="search_customer" name="keyword" class="form-control"
                                    placeholder="ค้นหาจาก E-mail, ชื่อ, เบอร์โทร"
                                    value="{{ app('request')->input('keyword') }}" autocomplete="off">
                                <span class="input-group-append">
                                    <button type="submit" class="btn  btn-primary"><i
                                            class="fas fa-search"></i>Search</button>
                                </span>

                            </div>
                        </form>
                    </div>
                </div>


                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Contact</th>
                                <th>Type</th>
                                <th>Active</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customer as $index=>$item)
                            <tr id="{{$item->id}}">
                                <td>{{app('request')->input('page')!=''?$index+1+( app('request')->input('page')*15-15):$index+1 }}
                                </td>
                                <td>{{$item->firstname}} {{$item->lastname}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->company}}</td>
                                <td>{{$item->phone}}</td>
                                <td>
                                    {{$item->status==2?'ลูกค้าเก่า':'ลูกค้าใหม่'}}
                                </td>
                                <td>
                                    <div class="custom-control custom-switch switch-success">
                                        <input type="checkbox" class="custom-control-input" id="active_{{$item->id}}"
                                            name="active_{{$item->id}}" {!!$item->del_cus==null?'checked':''!!}
                                        disabled>
                                        <label class="custom-control-label" for="active_{{$item->id}}"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group btn-group-sm" style="float: none;">
                                        <button type="button" class="btn btn-sm btn-info"
                                            style="float: none; margin: 4px;" data-toggle="modal"
                                            data-animation="bounce" data-target=".edit_customer_modal"
                                            onclick="editmode({{$item->id}})">
                                            <span class="ti-pencil"></span></button>
                                        <button type="button" {{Auth::user()->position!='Administrator'?'disabled':''}}
                                            class="btn btn-sm btn-danger deleteitem" delid="{{$item->id}}"
                                            style="float: none; margin: 4px;">
                                            <span class="ti-trash"></span></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <div class="col-12">
        {{ $customer->links() }}
    </div>
    <!-- end col -->
</div>


<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Customer</h5> -->
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form name="create_customer" id="create_customer" action="{{url('/admin/customer')}}" method="POST">
                    <div class="row">
                        <div class="col-sm-12">
                            <h5><u>ข้อมูลเข้าสู่ระบบ</u></h5>
                        </div>
                        <div class="col-sm-2">Username</div>
                        <div class="col-sm-4">
                            <input type="text" name="username" class="form-control" placeholder="Username">
                        </div>
                        <div class="col-sm-2">รหัสผ่าน:</div>
                        <div class="col-sm-4">
                            <input type="password" name="password" class="form-control" placeholder="กรุณากรอกรหัสผ่าน">
                        </div>

                        <div class="col-sm-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h5><u>รายละเอียดลูกค้า</u></h5>
                        </div>
                        <div class="col-sm-2">คำนำหน้า:</div>
                        <div class="col-sm-4" style="padding-right: unset;">
                            <input type="radio" name="title" value="mr" checked=""> นาย/Mr
                            <input type="radio" name="title" value="mrs"> นส./Mrs
                            <input type="radio" name="title" value="miss"> นาง/Miss
                            <input type="radio" name="title" value="other"> อื่นๆ
                        </div>
                        <div class="col-sm-2">สถานะ:</div>
                        <div class="col-sm-4"><input type="radio" name="status" value="1" checked="">
                            ลูกค้าใหม่
                            <input type="radio" name="status" value="2"> ลูกค้าเก่า
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">ชื่อ:</div>
                        <div class="col-sm-4">
                            <input type="text" name="firstname" class="form-control" placeholder="ชื่อ" required>
                        </div>
                        <!-- <div class="col-sm-2">นามสกุล:</div>
                        <div class="col-sm-4">
                            <input type="text" name="lastname" class="form-control" placeholder="นามสกุล">
                        </div> -->
                        <div class="col-sm-2">Note:</div>
                        <div class="col-sm-4">
                            <input type="text" name="note" class="form-control" placeholder="จดบันทึก">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">ชื่อบริษัท/ร้านค้า:</div>
                        <div class="col-sm-4">
                            <input type="text" name="company" class="form-control" placeholder="ชื่อบริษัท/ร้านค้า">
                        </div>
                        <div class="col-sm-2">ประเภทธุรกิจ:</div>
                        <div class="col-sm-4">
                            <select name="type" class="form-control">
                                <option value="0" selected="">ทั่วไป</option>
                                <option value="1">E-Commerce</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">ประเภทลูกค้า:</div>
                        <div class="col-sm-4">
                            <select name="type_customer" class="form-control">
                                <option value="1" selected="">บุคคลธรรมดา</option>
                                <option value="2">นิติบุคคล</option>
                            </select>
                        </div>
                        <div class="col-sm-2">เบอร์มือถือ:</div>
                        <div class="col-sm-4">
                            <input type="text" name="phone" class="form-control phones"
                                placeholder="กรุณากรอกเบอร์มือถือ" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Line ID:</div>
                        <div class="col-sm-4">
                            <input type="text" name="line" class="form-control" placeholder="กรุณาระบุ Line ID">
                        </div>
                        <div class="col-sm-2">อีเมล์:</div>
                        <div class="col-sm-4">
                            <input type="email" name="email" class="form-control" placeholder="กรุณากรอกอีเมล์">
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-2">รหัสบัตรประชาชน/<br />ทะเบียนการค้า:
                        </div>
                        <div class="col-sm-4">
                            <input type="text" name="code" class="form-control" onkeypress="return isNumberKey(event)"
                                placeholder="เลขประจำตัวประชาชน / ทะเบียนการค้า" />
                        </div>
                        <div class="col-sm-2">Credit Term:</div>
                        <div class="col-sm-4">
                            <select name="credit_term" class="form-control">
                                <option selected="" value="- choose -">- choose -</option>
                                <option value="cash">cash only</option>
                                <option value="7">7 days</option>
                                <option value="15">15 days</option>
                                <option value="30">30 days</option>
                                <option value="45">45 days</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Pick Fee:
                        </div>
                        <div class="col-sm-4">
                            <input type="text" name="pickup_fee" class="form-control" placeholder="ค่าธรรมเนียม ถ้ามี"
                                onkeypress="return isNumberKey(event)">
                        </div>
                        <!-- <div class="col-sm-2">Note:</div>
                        <div class="col-sm-4">
                            <input type="text" name="note" class="form-control" placeholder="จดบันทึก">
                        </div> -->
                        <div class="col-sm-2">ชุดตารางราคา:</div>
                        <div class="col-sm-4">
                            <select name="service_group" class="form-control">
                                @foreach ($servicegroup as $sgitem)
                                <option value="{{$sgitem->id}}">{{$sgitem->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"
                        aria-hidden="true">Cancel</button>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  Modal content for the above example -->
<div class="modal fade edit_customer_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Edit Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form name="edit_customer" id="edit_customer" action="{{url('/admin/customer')}}" method="POST">
                    <div class="row">
                        <div class="col-sm-12">
                            <h5><u>ข้อมูลเข้าสู่ระบบ</u></h5>
                            <input type="hidden" id="edit_id" />
                        </div>
                        <div class="col-sm-2">Username</div>
                        <div class="col-sm-4">
                            <input type="text" name="username" id="username" class="form-control" placeholder="Username"
                                autocomplete="off">
                        </div>
                        <div class="col-sm-2">รหัสผ่าน:</div>
                        <div class="col-sm-4">
                            <button type="button" id="sendemail" class="btn btn-warning waves-effect waves-light"
                                onclick="$('#resetpassword').slideToggle('slow');">เปลี่ยนรหัสผ่าน</button>
                        </div>
                        <div class="col-sm-12" style="display:none" id="resetpassword">
                            <h5><u>ตั้งรหัสผ่านใหม่</u></h5>
                            <div class="row">
                                <div class="col-md-6"><input type="text" name="password" id="password"
                                        class="form-control" value="******" placeholder="กรุณากรอกรหัสผ่าน"
                                        autocomplete="off">
                                </div>
                                <div class="col-md-6">
                                    <button type="button" id="saveandsend"
                                        class="btn btn-success waves-effect waves-light"
                                        onclick="randompassword()">ตั้งรหัสแบบสุ่ม</button>
                                    <button type="button" id="saveandsend"
                                        class="btn btn-primary waves-effect waves-light"
                                        onclick="savepassword()">บันทึกและแจ้งไปยังลูกค้า</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h5><u>รายละเอียดลูกค้า</u></h5>
                        </div>
                        <div class="col-sm-2">คำนำหน้า:</div>
                        <div class="col-sm-4" style="padding-right: unset;">
                            <input type="radio" name="title" id="mr" value="mr" checked=""> นาย/Mr
                            <input type="radio" name="title" id="mrs" value="mrs"> นส./Mrs
                            <input type="radio" name="title" id="miss" value="miss"> นาง/Miss
                            <input type="radio" name="title" id="other" value="other"> อื่นๆ
                        </div>
                        <div class="col-sm-2">สถานะ:</div>
                        <div class="col-sm-4"><input type="radio" name="status" id="status" value="1" checked="">
                            ลูกค้าใหม่
                            <input type="radio" name="status" value="2"> ลูกค้าเก่า
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">ชื่อ-นามสกุล:</div>
                        <div class="col-sm-4">
                            <input type="text" name="firstname" id="firstname" class="form-control" placeholder="ชื่อ"
                                required>
                        </div>
                        <div class="col-sm-2">Note:</div>
                        <div class="col-sm-4">
                            <input type="text" name="note" id="note" class="form-control" placeholder="จดบันทึก">
                        </div>
                        <!-- <div class="col-sm-2">นามสกุล:</div>
                        <div class="col-sm-4">
                            <input type="text" name="lastname" id="lastname" class="form-control" placeholder="นามสกุล">
                        </div> -->
                    </div>

                    <div class="row">
                        <div class="col-sm-2">ชื่อบริษัท/ร้านค้า:</div>
                        <div class="col-sm-4">
                            <input type="text" name="company" id="company" class="form-control"
                                placeholder="ชื่อบริษัท/ร้านค้า">
                        </div>
                        <div class="col-sm-2">ประเภทธุรกิจ:</div>
                        <div class="col-sm-4">
                            <select name="type" id="type" class="form-control">
                                <option value="0" selected="">ทั่วไป</option>
                                <option value="1">E-Commerce</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">ประเภทลูกค้า:</div>
                        <div class="col-sm-4">
                            <select name="type_customer" id="type_customer" class="form-control">
                                <option value="1" selected="">บุคคลธรรมดา</option>
                                <option value="2">นิติบุคคล</option>
                            </select>
                        </div>
                        <div class="col-sm-2">เบอร์มือถือ:</div>
                        <div class="col-sm-4">
                            <input type="text" name="phone" id="phone" class="form-control"
                            placeholder="กรุณากรอกเบอร์มือถือ" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Line ID:</div>
                        <div class="col-sm-4">
                            <input type="text" name="line" id="line" class="form-control"
                                placeholder="กรุณาระบุ Line ID">
                        </div>
                        <div class="col-sm-2">E-mail:</div>
                        <div class="col-sm-4">
                            <input type="email" name="email" id="email" class="form-control"
                                placeholder="กรุณากรอกอีเมล์" multiple>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">รหัสบัตรประชาชน/<br />ทะเบียนการค้า:
                        </div>
                        <div class="col-sm-4">
                            <input type="text" name="code" id="code" class="form-control"
                                onkeypress="return isNumberKey(event)"
                                placeholder="เลขประจำตัวประชาชน / ทะเบียนการค้า" />
                        </div>
                        <div class="col-sm-2">Credit Term:</div>
                        <div class="col-sm-4">
                            <select name="credit_term" id="credit_term" class="form-control">
                                <option selected="" value="cash">cash only</option>
                                <option value="7">7 days</option>
                                <option value="15">15 days</option>
                                <option value="30">30 days</option>
                                <option value="45">45 days</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Pick Fee:
                        </div>
                        <div class="col-sm-4">
                            <input type="text" name="pickup_fee" id="pickup_fee" class="form-control"
                                placeholder="ค่าธรรมเนียม ถ้ามี" onkeypress="return isNumberKey(event)">
                        </div>
                        <div class="col-sm-2">ชุดตารางราคา:</div>
                        <div class="col-sm-4">
                            <select name="service_group" id="service_group" class="form-control">
                                @foreach ($servicegroup as $sgitem)
                                <option value="{{$sgitem->id}}">{{$sgitem->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- <div class="col-sm-2">Note:</div>
                        <div class="col-sm-4">
                            <input type="text" name="note" id="note" class="form-control" placeholder="จดบันทึก">
                        </div> -->
                    </div>
                    <div class="row">
                        <!-- <div class="col-sm-2">ประเภทบริการ:</div>
                        <div class="col-sm-4">
                            <select name="service_group" id="service_group" class="form-control">
                                @foreach ($servicegroup as $sgitem)
                                <option value="{{$sgitem->id}}">{{$sgitem->name}}</option>
                                @endforeach
                            </select>
                        </div> -->
                        <div class="col-sm-2">Active:</div>
                        <div class="col-sm-4">
                            <div class="custom-control custom-switch switch-success">
                                <input type="checkbox" class="custom-control-input" id="active" name="active">
                                <label class="custom-control-label" for="active"></label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <hr />
                            <h5><u>จัดการที่อยู่</u><span data-repeater-create="" style="float:right;"
                                    class="btn btn-secondary" onclick="show_add_address();">
                                    <span class="fas fa-plus"></span>
                                </span></h5>
                        </div>
                        <div class="col-sm-12">
                            <div class="card" style="height: 200px;">
                                <div class="table-responsive">
                                    <table class="table mb-0" id="cus_address_show">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Address</th>
                                                <th>Phone</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <!--end /table-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-danger" id="close_modal" data-dismiss="modal"
                        aria-hidden="true">Cancel</button>
                </form>
                <div class="col-sm-12" id="add_address_area" style="display:none;">
                    <div class="card">
                        <h5 class="card-header bg-primary text-white mt-0" id="address_title">เพิ่มที่อยู่</h5>
                        <div class="card-body">
                            <form name="add_address_form" id="add_address_form" action="{{url('/admin/add_customer_address')}}" method="POST">
                                <div class="row">
                                    <div class="col-sm-2">ชื่อ - สกุล:</div>
                                    <div class="col-sm-4">
                                        <input type="text" name="name" id="addr_name" class="form-control"
                                            placeholder="ชื่อ-สกุล" required="" autocomplete="off">
                                        <input type="hidden" name="customer_id" id="addr_customer_id"
                                            class="form-control" value="" autocomplete="off">
                                        <input type="hidden" name="address_id" id="address_id" class="form-control"
                                            value="" autocomplete="off">
                                    </div>
                                    <div class="col-sm-2">หมายเลขโทรศัพท์:</div>
                                    <div class="col-sm-4">
                                        <input type="text" name="phone" id="addr_phone" class="form-control"
                                            placeholder="หมายเลขโทรศัพท์" required="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">E-Mail:</div>
                                    <div class="col-sm-4">
                                        <input type="email" name="email" id="addr_email" class="form-control"
                                            placeholder="E-Mail" multiple autocomplete="off">
                                    </div>

                                    <div class="col-sm-2">ที่อยู่:</div>
                                    <div class="col-sm-4">
                                        <input type="text" name="address" id="addr_address" class="form-control"
                                            placeholder="ที่อยู่" required="" autocomplete="off">
                                    </div>
                                </div>
                                <link rel="stylesheet" href="{{ asset('assets/dist/jquery.Thailand.min.css') }}">

                                <div id="demo1" class="demo" uk-grid>
                                    <div class="row">
                                        <div class="col-sm-2">ตำบล / แขวง:</div>
                                        <div class="col-sm-4 uk-width-1-2@m">
                                            <input name="tumbon" id="tumbon" class="form-control" type="text"
                                                autocomplete="new-password">
                                        </div>

                                        <div class="col-sm-2">อำเภอ / เขต:</div>
                                        <div class="col-sm-4 uk-width-1-2@m">
                                            <input name="amphur" id="amphur" class="form-control" type="text"
                                                autocomplete="new-password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">จังหวัด:</div>
                                        <div class="col-sm-4 uk-width-1-2@m">
                                            <input name="province" id="province" class="form-control" type="text"
                                                autocomplete="new-password">
                                        </div>

                                        <div class="col-sm-2">รหัสไปรษณีย์:</div>
                                        <div class="col-sm-4 uk-width-1-2@m">
                                            <input name="postcode" id="postcode" class="form-control" type="text"
                                                autocomplete="new-password">
                                        </div>
                                    </div>
                                </div>



                                <script type="text/javascript">
                                $.Thailand({
                                    $district: $('#demo1 [name="tumbon"]'),
                                    $amphoe: $('#demo1 [name="amphur"]'),
                                    $province: $('#demo1 [name="province"]'),
                                    $zipcode: $('#demo1 [name="postcode"]'),

                                    onDataFill: function(data) {
                                        console.info('Data Filled', data);
                                    },

                                    onLoad: function() {
                                        console.info('Autocomplete is ready!');
                                        // $('#loader, .demo').toggle();
                                    }
                                });
                                </script>
                                <div class="col-ms-6">
                                    <button type="submit" class="btn btn-sm btn-primary">บันทึก</button>
                                    <button type="button" class="btn btn-sm btn-danger"
                                        onclick="close_add_address()">ยกเลิก</button>
                                </div>
                        </div>
                        </form>
                    </div>
                    <!--end card-body-->
                </div>

            </div>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
var data = null;
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //------------ สร้าง customer ใหม่
    $("#create_customer").submit(function(e) {
        $('#loading').show();
        e.preventDefault();
        var thisform = $(this);
        var url = thisform.attr('action');
        $.ajax({
            type: "POST",
            data: $('#create_customer').serialize(),
            url: url,
            success: function(result) {
                $('#customer_select').hide();
                $('#customer_detail').show();
                $('#loading').hide();
                $('#close_modal').click();
                var result_data = JSON.parse(result);
                alert('Create customer successful.')
                window.location.reload();
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator.");
                $('#loading').hide();
            }
        });
    });

    $("#edit_customer").submit(function(e) {
        $('#loading').show();
        e.preventDefault();
        var thisform = $(this);
        var url = thisform.attr('action');
        var id = $('#edit_id').val();
        url = url+'/'+id;
        $.ajax({
            type: "PATCH",
            data: $('#edit_customer').serialize(),
            url: url,
            success: function(result) {
                $('#loading').hide();
                if (result == '0') {
                    alert('มีการใช้อีเมล์นี้แล้วในระบบ กรุณาเปลี่ยนอีเมล์ใหม่');
                } else if (result == '1') {
                    alert('มีการใช้ Username นี้แล้วในระบบ กรุณาเปลี่ยน Username ใหม่');
                } else {
                    location.reload();
                }
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator.");
                $('#loading').hide();
            }
        });
    });
    //------------ ดึงข้อมูลลูกค้า
});
//-------------- ห้ามใส่ตัวอักษร
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 &&
        (charCode < 48 || charCode > 57)) 
        return false;
    return true;
}

function editmode(id) {
    $('#edit_customer')[0].reset();
    close_add_address();
    $.ajax({
        type: "get",
        url: '{{url('admin/getCustomer_by_id')}}/' + id,
        success: function(result) {
            var dataset = JSON.parse(result);
            //console.log(data);
            var data = dataset[0];
            var address = dataset[1];

            $('#edit_id').val(data.id);
            if (data.title != null && data.title != '') {
                $('#' + data.title).prop("checked", true);
            }
            if (data.status != null && data.status != '') {
                $('#status_' + data.status).prop("checked", true);
            }
            $('#firstname').val(data.firstname);
            $('#lastname').val(data.lastname);
            $('#company').val(data.company);
            $("#type").find("option[value=" + data.type + "]").attr("selected", true);
            $("#type_customer").find("option[value=" + data.type_customer + "]").attr("selected", true);
            $("#service_group").find("option[value=" + data.service_group + "]").attr("selected", true);
            $('#phone').val(data.phone);
            $('#email').val(data.email);
            $('#username').val(data.username);
            $('#password').val('******');
            $('#line').val(data.line);
            $('#code').val(data.code);
            $("#credit_term").find("option[value=" + data.credit_term + "]").attr("selected", true);
            $('#pickup_fee').val(data.pickup_fee);
            $('#note').val(data.note);
            $('#' + data.title).prop('checked', true);
            if (data.del_cus == null) {
                $('#active').prop('checked', true);
            } else {
                $('#active').prop('checked', false);
            }

            console.log(address);
            $('#cus_address_show tbody').html('');
            $(address).each(function(i, e) {
                var row = '<tr>' +
                    '<th scope="row">' + (i + 1) + '</th>' +
                    '<td><span class="fullname">' + e.customer_name + '</span></td>' +
                    '<td><span class="address">' + e.address +
                    '</span>, <span class="tumbon_name" set="' + e.tumbon + '">' + e.tumbon +
                    '</span>, <span class="amphur_name" set="' + e.amphur + '">' + e.amphur +
                    '</span>, <span class="province_name" set="' + e.province + '">' + e.province +
                    '</span> <span class="postcode">' + e.postcode + '</span><br/>' +
                    '<input type="radio" name="pickup" value="' + e.id + '" ' + (e.pickup == 1 ?'checked' : '') + '> ตั้งเป็นที่อยู่รับของ ' +
                    '<input type="radio" name="invoice" value="' + e.id + '" ' + (e.invoice == 1 ?'checked' : '') + '> ตั้งเป็นที่อยู่ออกใบเสร็จ' +
                    '</div></td>' +
                    '<td><span class="phone">' + e.phone +
                    '</span><span class="email" style="display:none">' + e.email + '</span></td>' +
                    '<td>' +
                    '<a href="#" onclick="edit_address($(this),' + e.id +')" class="mr-2"><i class="fas fa-edit text-info font-16"></i></a>' +
                    '<a href="#" onclick="remove_address($(this),' + e.id +')"><i class="fas fa-trash-alt text-danger font-16"></i></a>' +
                    '</td>' +
                    '</tr>';
                $('#cus_address_show tbody').append(row);
            });

        },
        error: function(e) {
            console.log("ERROR : ", e);
            alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
            $('#loading').hide();
        }
    })
}

$('.deleteitem').click(function(e) {
    var x = confirm("Are you sure you want to delete?");
    if (x) {
        var id = $(this).attr('delid');
        var url = '{{url('/admin/customer')}}';
        url = url + '/' + id;
        $('#loading').show();
        $.ajax({
            type: 'DELETE',
            url: url,
            success: function(result) {
                $('#loading').hide();
                window.location.reload(true);
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                $('#loading').hide();
            }
        });
    } else
        return false;

});

function close_add_address() {
    $('#add_address_area').hide();
    $('#edit_customer').show();
    $('#addr_customer_id').val('');
    $('#amphur').html('');
    $('#tumbon').html('');
}

function show_add_address() {
    $('#address_title').html('เพิ่มที่อยู่');
    $('#address_id').val('');
    $('#add_address_area').show();
    $('#edit_customer').hide();
    $('#addr_customer_id').val($('#edit_id').val());
    var fullname = $('#firstname').val() + '  ' + $('#lastname').val();
    $('#addr_name').val(fullname);
    $('#addr_phone').val($('#phone').val());
    $('#amphur').html('');
    $('#tumbon').html('');
    $('#addr_address').val('');
    $('#postcode').val('');
}
$("#add_address_form").submit(function(e) {
    $('#loading').show();
    e.preventDefault();
    var thisform = $(this);
    var url = thisform.attr('action');

    $.ajax({
        type: "POST",
        data: $('#add_address_form').serialize(),
        url: url,
        success: function(result) {
            $('#loading').hide();

            var e = JSON.parse(result);
            e = e[0];
            console.log(e);
            if ($('#address_id').val() != '') {
                address_tmp.parent().parent().html(
                    '<th scope="row"><i class="mdi mdi-content-save"></i></th>' +
                    '<td><span class="fullname">' + e.customer_name + '</span></td>' +
                    '<td><span class="address">' + e.address +
                    '</span>, <span class="tumbon_name" set="' + e.tumbon + '">' + e.tumbon +
                    '</span>, <span class="amphur_name" set="' + e.amphur + '">' + e.amphur +
                    '</span>, <span class="province_name" set="' + e.province + '">' + e.province + 
                    '</span> <span class="postcode">' + e.postcode +
                    '</span><br/>' +
                    '<input type="radio" name="pickup" value="' + e.id + '" ' + (e.pickup == 1 ?'checked' : '') + '> ตั้งเป็นที่อยู่รับของ ' +
                    '<input type="radio" name="invoice" value="' + e.id + '" ' + (e.invoice == 1 ? 'checked' : '') + '> ตั้งเป็นที่อยู่ออกใบเสร็จ' +
                    '</div></td>' +
                    '<td><span class="phone">' + e.phone +
                    '</span><span class="email" style="display:none">' + e.email +
                    '</span></td>' +
                    '<td>' +
                    '<a href="#" onclick="edit_address($(this),' + e.id +')" class="mr-2"><i class="fas fa-edit text-info font-16"></i></a>' +
                    '<a href="#" onclick="remove_address($(this),' + e.id +')"><i class="fas fa-trash-alt text-danger font-16"></i></a>' +
                    '</td>');
            } else {
                var i = $('#cus_address_show tbody tr').length;
                var row = '<tr>' +
                    '<th scope="row">' + (i + 1) + '</th>' +
                    '<td><span class="fullname">' + e.customer_name + '</span></td>' +
                    '<td><span class="address">' + e.address +
                    '</span>, <span class="tumbon_name" set="' + e.tumbon + '">' + e.tumbon +
                    '</span>, <span class="amphur_name" set="' + e.amphur + '">' + e.amphur +
                    '</span>, <span class="province_name" set="' + e.province + '">' + e.province +
                    '</span> <span class="postcode">' + e.postcode + '</span><br/>' +
                    '<input type="radio" name="pickup" value="' + e.id + '" ' + (e.pickup == 1 ?'checked' : '') + '> ตั้งเป็นที่อยู่รับของ ' +
                    '<input type="radio" name="invoice" value="' + e.id + '" ' + (e.invoice == 1 ?'checked' : '') + '> ตั้งเป็นที่อยู่ออกใบเสร็จ' +
                    '</div></td>' +
                    '<td><span class="phone">' + e.phone +
                    '</span><span class="email" style="display:none">' + e.email + '</span></td>' +
                    '<td>' +
                    '<a href="#" onclick="edit_address($(this),' + e.id +')" class="mr-2"><i class="fas fa-edit text-info font-16"></i></a>' +
                    '<a href="#" onclick="remove_address($(this),' + e.id +')"><i class="fas fa-trash-alt text-danger font-16"></i></a>' +
                    '</td>' +
                    '</tr>';
                $('#cus_address_show tbody').append(row);
            }

            close_add_address();
        },
        error: function(e) {
            console.log("ERROR : ", e);
            alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
            $('#loading').hide();
        }
    });
});

function remove_address(obj, id) {
    if (confirm('Are you sure you want to delete this address into the database?')) {
        $.ajax({
            type: "POST",
            url: "{{url('admin/del_customer_address')}}/" + id,
            success: function(result) {
                $(obj).parent().parent().remove();
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                $('#loading').hide();
            }
        });
    } else {
        // Do nothing!
    }

}
var address_tmp = null;

function edit_address(obj, id) {
    $('#address_title').html('แก้ไขที่อยู่');
    address_tmp = $(obj);
    $('#address_id').val(id);
    $('#add_address_area').show();
    $('#edit_customer').hide();
    var data = $(obj).parent().parent();
    var prov = data.find('.province_name').attr('set');
    var email = data.find('.email').html();
    email = email == 'null' ? '' : email;
    $('#addr_customer_id').val($('#edit_id').val());
    $('#addr_name').val(data.find('.fullname').html());
    $('#addr_address').val(data.find('.address').html());
    $('#addr_phone').val(data.find('.phone').html());
    $('#addr_email').val(email);
    $('#province').val(prov);
    $('#postcode').val(data.find('.postcode').html());
    $('#amphur').val(data.find('.amphur_name').attr('set'));
    $('#tumbon').val(data.find('.tumbon_name').attr('set'));
}

function randompassword() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    $('#password').val(retVal);
}

function savepassword() {

    var phone = $('#phone').val();
    var email = $('#email').val();
    var id = $('#edit_id').val();
    if (phone != "" && email != "" && phone != null && email != null) {
        if (confirm('คุณต้องการส่งอีเมล์แจ้งรหัสใหม่ไปที่ ' + email + ' \n และส่ง SMS ไปที่ ' + phone +' ใช่หรือไม่?')) {
            // ต้องส่งไป controller ตัวใหม่ ที่บันทึกรหัสเสร็จแล้ว ส่งอีเมล์ และ SMS ด้วย
            $.ajax({
                type: 'POST',
                data: $('#edit_customer').serialize(),
                url: '/re_password/'+id,
                success: function(result) {
                    console.log('Succeed');
                    alert("ส่งข้อมูล Password ใหม่ ไปทาง Email และ SMS ให้ลูกค้าเรียบร้อยแล้วครับ");
                },
                error: function(e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
        } else {
            alert("บันทึกเสร็จสิ้น");
        }
    } else {
        if (phone == null && phone == "") {
            alert("กรุณาใส่เบอร์โทรศัพย์");
        } else {
            alert("กรุณาใส่Email");
        }
    }
}

var regex = new RegExp("^[0-9,]*$");

$('#phone').bind("keypress", function (event) {
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

$('.phones').bind("keypress", function (event) {
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});
</script>
@endsection