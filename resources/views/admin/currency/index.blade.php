@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Currency</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Currency</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">Edit Table With Button</h4>
                <p class="text-muted mb-3">Add toolbar column with edit and delete buttons.</p>
                <button type="button" class="btn btn-primary waves-effect waves-light float-left mb-3"
                    data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg" id="add">+ Add
                    New</button>
                <div class="table-responsive">
                    <table class="table mb-0" id="my-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Country</th>
                                <th style="display:none"></th>
                                <th class="tabledit-toolbar-column"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($currency as $item)
                            <tr>
                                <td id="{{$item->id}}" code="{{$item->code}}" country="{{$item->country}}">{{$item->id}}
                                </td>
                                <td>{{$item->code}}</td>
                                <td>{{$item->country}}</td>
                                <td style="display:none"></td>
                                <!--<td>
                                    <div class="custom-control custom-switch switch-success">
                                        <input type="checkbox" class="custom-control-input"
                                            id="customSwitchSuccess{{$item->id}}" name="active-{{$item->id}}"
                                            {!!$item->active==1?'checked':''!!}>
                                        <label class="custom-control-label"
                                            for="customSwitchSuccess{{$item->id}}"></label>
                                    </div>
                                </td>-->
                                <td>
                                    <div class="btn-group btn-group-sm" style="float: none;">
                                        <button type="button" class="btn btn-sm btn-info"
                                            style="float: none; margin: 4px;" data-toggle="modal"
                                            data-animation="bounce" data-target=".edit_employee_modal"
                                            onclick="editmode({{$item->id}})">
                                            <span class="ti-pencil"></span></button>
                                        <button type="button" class="btn btn-sm btn-danger deleteitem"
                                            style="float: none; margin: 4px;" delid="{{$item->id}}">
                                            <span class="ti-trash"></span></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!-- end col -->
</div>





<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Currency</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{url('/admin/currency')}}" name="currency_form" id="currency_form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">Code</label>
                                <input type="text" class="form-control" id="code" name="code" required="">
                                <input type="hidden" class="form-control" id="id" name="id" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">Country</label>
                                <input type="text" class="form-control" id="country" name="country" required="">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    <button type="reset" class="btn btn-sm btn-primary" id="reset" style="display:none">reset</button>
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-hidden="true">
                        Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script>
    $('#currency_form').submit(function(e){
        e.preventDefault();
            var thisform = $(this);
            var url = thisform.attr('action');
            var method = 'POST';
            if($('#id').val()!=''){
                url = url+'/'+$('#id').val();
                method = 'PUT'
            }

            $('#loading').show();
            $.ajax({
                type: method,
                data: $('#currency_form').serialize(), 
                url: url,
                success: function (result) {
                    $('#loading').hide();
                    window.location.reload(true);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
    });
    function editmode(id){
        $('#add').click();
        var obj = $('#'+id);
        $('#id').val(id);
        $('#code').val(obj.attr('code'));
        $('#country').val(obj.attr('country'));
    }
    $('.deleteitem').click(function(e){
        var x = confirm("Are you sure you want to delete?");
        if (x){
        var id = $(this).attr('delid');
        var url = $('#currency_form').attr('action');
        url = url+'/'+id;
        $('#loading').show();
            $.ajax({
                type: 'DELETE',
                url: url,
                success: function (result) {
                    $('#loading').hide();
                    window.location.reload(true);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
            }
        else
            return false;
        
    });
    $('#add').click(function(){
        $('#reset').click();
        $('#id').val('');
    });
</script>

@endsection