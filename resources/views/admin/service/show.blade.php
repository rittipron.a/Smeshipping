@extends('layouts.app')

@section('content')
    <style>
        .emailHelp {
            color: #f31414;
            font-size: 12px;
        }
    </style>
    <link href="{{ asset('/assets/pages/pickList.css')}}" rel="stylesheet" type="text/css"/>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Admin</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Service</a></li>
                        <li class="breadcrumb-item active">Edit Service</li>
                    </ol>
                </div>
                <h4 class="page-title">Edit Service</h4>
            </div>
            <!--end page-title-box-->
        </div>
        <!--end col-->
    </div>
    <div class="row">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4>Service: {{$service->name}} ({{$service->code}})</h4>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home" role="tab"
                               aria-selected="false">Service
                                Detail</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-selected="false"
                               onclick="zone_init(1);">Zone</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#settings" role="tab"
                               aria-selected="true">Price</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#esc" role="tab"
                               aria-selected="true">Surcharge</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane p-3 active" id="home" role="tabpanel">
                            <form method="POST" class="form-horizontal auth-form my-4" id="service_form"
                                  action="/admin/service/{{$service->id}}" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                <input type="hidden" name="tracking" id="tracking" value="{{$service->tracking}}"
                                       class="form-control">
                            <!--<div class="form-group row">
                                <label for="horizontalInput2" class="col-sm-2 col-form-label">Routing ID :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="routing_id" name="routing_id"
                                        value="{{$service->routing_id}}" required placeholder="กรุณากรอกข้อมูล">
                                    <div class="emailHelp">Routing ID :คือรหัสอ้างอิงของการออก AWB เพื่อให้ทาง EO
                                        ทราบว่าใช้หมายเลข Account number ใดในการออก AWB โดยเราใช้ 1 ตัวอักษร เช่น D =
                                        DHL
                                        F=FEDEX T=TNT และ หมายเลข Account number 3 หลักสุดท้าย ที่ใช้ออก AWB ของ Service
                                        ตัวนี้ ซึ่งจะออกมาในรูปแบบ "D894"</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="horizontalInput2" class="col-sm-2 col-form-label">Service Code :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="service_code" name="service_code"
                                        value="{{$service->code}}" required placeholder="กรุณากรอกข้อมูล" readonly>
                                    <div class="emailHelp">Service Code :คือการกำหนดรหัสของบริการ รายละเอียด วิธีการจัด
                                        Service code พร้อมทั้งรายชื่อลูกค้าที่ได้ราคาต่างๆ <a target="_blank"
                                            href="https://goo.gl/S8Evl3">https://goo.gl/S8Evl3</a></div>
                                </div>
                            </div>
                        -->
                                <div class="form-group row">
                                    <label for="horizontalInput2" class="col-sm-2 col-form-label">TYPE OF SERVICE
                                        :</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="type_of_service" name="type_of_service">
                                            @foreach ($servicetype as $servicetypeitem)
                                                <option value="{{$servicetypeitem->id}}" {!! $service->
                                            cat==$servicetypeitem->id?'selected':'' !!}>
                                                    [{{$servicetypeitem->code}}] {{$servicetypeitem->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="emailHelp">TYPE OF SERVICE :คือ ประเภทของบริการ</div>
                                    </div>
                                </div>
                            <!--<div class="form-group row">
                                <label for="horizontalInput2" class="col-sm-2 col-form-label">GROUP OF SERVICE :</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="group_of_service" name="group_of_service">
                                        @foreach ($group as $gitem)
                                <option value="{{$gitem->id}}" {!! $service->
                                            service_group==$gitem->id?'selected':'' !!}>
                                            {{$gitem->name}}</option>
                                        @endforeach
                                </select>
                                <div class="emailHelp">GROUP OF SERVICE :คือ กลุ่มของบริการ
                                    เพื่อใช้กับลูกค้าแต่ละกลุ่ม</div>
                            </div>
                        </div>-->
                                <div class="form-group row">
                                    <label for="horizontalInput2" class="col-sm-2 col-form-label">Product Name :</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="product_name" name="product_name"
                                               value="{{$service->name}}" required placeholder="กรุณากรอกข้อมูล">
                                        <div class="emailHelp">Product Name :คือการตั้งชื่อบริการ
                                            เป็นส่วนที่ลูกค้าจะเห็นได้
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="horizontalInput2" class="col-sm-2 col-form-label">Description :</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="description" name="description"
                                               value="{{$service->description}}" required placeholder="กรุณากรอกข้อมูล">
                                        <div class="emailHelp">Description :คำอธิบายบริการ ที่เห็นเฉพาะเจ้าหน้าที่ของเรา
                                        </div>
                                    </div>
                                </div>
                            <!--<div class="form-group row">
                                <label for="horizontalInput2" class="col-sm-2 col-form-label">Tracking :</label>
                                <div class="col-sm-10">
                                    <select name="tracking" id="tracking" class="form-control">
                                        <option value="" {!!$service->tracking==''?'selected':''!!}>-NONE-</option>
                                        <option value="DHL" {!!$service->tracking=='DHL'?'selected':''!!}>DHL</option>
                                        <option value="GMB" {!!$service->tracking=='GMB'?'selected':''!!}>GMB</option>
                                        <option value="FEDEX" {!!$service->tracking=='FEDEX'?'selected':''!!}>FEDEX
                                        </option>
                                        <option value="Aramex" {!!$service->tracking=='Aramex'?'selected':''!!}>Aramex
                                        </option>
                                        <option value="TNT" {!!$service->tracking=='TNT'?'selected':''!!}>TNT</option>
                                    </select>
                                    <div class="emailHelp">Tracking : Website For Tracking Shipment</div>
                                </div>
                            </div>-->
                                <div class="form-group row">
                                    <label for="horizontalInput2" class="col-sm-2 col-form-label">วิธีคิดราคา :</label>
                                    <div class="col-md-9">
                                        <div class="form-check-inline my-1">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="cal1" name="cal" value="weight"
                                                       class="custom-control-input" {!!$service->cal=='weight'?'checked':''!!}>
                                                <label class="custom-control-label" for="cal1">คิดราคาจากน้ำหนัก</label>
                                            </div>
                                        </div>
                                        <div class="form-check-inline my-1">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="cal2" name="cal" value="box"
                                                       class="custom-control-input" {!!$service->cal=='box'?'checked':''!!}>
                                                <label class="custom-control-label" for="cal2">คิดราคาจากน้ำหนัก
                                                    และขนาดกล่อง</label>
                                            </div>
                                        </div>
                                        <div class="emailHelp">- คิดราคาจากน้ำหนัก ใช้กับ Global Mail
                                        </div>
                                        <div class="emailHelp">- คิดราคาจากน้ำหนัก และขนาดกล่อง ใช้กับ Express</div>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label for="horizontalInput2" class="col-sm-2 col-form-label">วิธีการคิดน้ำหนัก
                                        :</label>
                                    <div class="col-md-9">
                                        <div class="form-check-inline my-1">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="cal_weight" name="cal_weight" value=""
                                                       class="custom-control-input"
                                                    {!!$service->cal_weight==''?'checked':''!!}>
                                                <label class="custom-control-label"
                                                       for="cal_weight">คิดตามน้ำหนักจริง</label>
                                            </div>
                                        </div>
                                        <div class="form-check-inline my-1">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="cal_weight2" name="cal_weight" value="up"
                                                       class="custom-control-input"
                                                    {!!$service->cal_weight=='up'?'checked':''!!}>
                                                <label class="custom-control-label" for="cal_weight2">ปัดขึ้นทีละ
                                                    0.5</label>
                                            </div>
                                        </div>
                                        <div class="emailHelp">- คิดตามน้ำหนักจริง ใช้กับ Global Mail</div>
                                        <div class="emailHelp">- ปัดขึ้นทีละ 0.5 ใช้กับ Express</div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="horizontalInput2" class="col-sm-2 col-form-label">ชนิด :</label>
                                    <div class="col-md-9">
                                        <div class="form-check-inline my-1">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="type" name="type" value="doc"
                                                       class="custom-control-input" {!!$service->type=='doc'?'checked':''!!}>
                                                <label class="custom-control-label" for="type">เอกสาร</label>
                                            </div>
                                        </div>
                                        <div class="form-check-inline my-1">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="type1" name="type" value="box"
                                                       class="custom-control-input" {!!$service->type=='box'?'checked':''!!}>
                                                <label class="custom-control-label" for="type1">พัสดุ</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <!--<div class="form-group row">
                                <label for="horizontalInput2" class="col-sm-2 col-form-label">Product drop point
                                    :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="prod_drop_point" name="prod_drop_point"
                                        value="{{$service->prod_drop_point}}" placeholder="กรุณากรอกข้อมูล">
                                    <div class="emailHelp">ข้อความแสดงหน้าราคา</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="horizontalInput2" class="col-sm-2 col-form-label">More information :</label>
                                <div class="col-sm-10">
                                    <textarea id="elm1"
                                        name="more_info_detail">{!!$service->more_info_detail!!}</textarea>
                                    <div class="emailHelp">รายละเอียด</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="horizontalInput2" class="col-sm-2 col-form-label">Active :</label>
                                <div class="col-sm-10">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="active" name="active"
                                            {!!$service->active=='1'?'checked':''!!}/>
                                        <label class="custom-control-label" for="active"></label>
                                    </div>
                                    <div class="emailHelp">เลือกเพื่อกำหนดว่าจะใช้งานบริการนี้หรือไม่
                                    </div>
                                </div>
                            </div>
                        -->
                                <div class="form-group row">
                                    <label for="logoInput2" class="col-sm-2 col-form-label">Logo :</label>
                                    <div class="col-sm-10">
                                        @if($service->logo!=null)
                                            <img src="{{ asset('/uploads/service/'.$service->logo) }}" width="80px"/>
                                            <br/>
                                        @endif
                                        <input type="file" name="logo" accept="image/*"/>
                                        <div class="logohelp">กรุณาเลือกรูปที่จะใช้เป็นโลโก้</div>
                                    </div>
                                </div>
                                <br/>
                                <button type="submit" class="btn btn-primary waves-effect waves-light"
                                        id="save_service">บันทึก
                                </button>
                            </form>
                        </div>
                        <!-- ******************************************************************************************************* -->
                        <div class="tab-pane p-3" id="profile" role="tabpanel">
                            <form method="POST" class="form-horizontal auth-form my-4" id="zone_form"
                                  action="/admin/service/setzone/{{$service->id}}">
                                @csrf
                                <div class="col-sm-1" style="padding:unset">เลือกโซน</div>
                                <div class="col-sm-3">
                                    <select name="zone" id="zone" class="form-control"
                                            onchange="zone_init($(this).val())"
                                            required>
                                        <option selected="">- choose -</option>
                                        <option value="1" selected="">zone 1</option>
                                        <option value="2">zone 2</option>
                                        <option value="3">zone 3</option>
                                        <option value="4">zone 4</option>
                                        <option value="5">zone 5</option>
                                        <option value="6">zone 6</option>
                                        <option value="7">zone 7</option>
                                        <option value="8">zone 8</option>
                                        <option value="9">zone 9</option>
                                        <option value="10">zone 10</option>
                                        <option value="11">zone 11</option>
                                        <option value="12">zone 12</option>
                                        <option value="13">zone 13</option>
                                        <option value="14">zone 14</option>
                                        <option value="15">zone 15</option>
                                        <option value="16">zone 16</option>
                                        <option value="17">zone 17</option>
                                        <option value="18">zone 18</option>
                                        <option value="19">zone 19</option>
                                        <option value="20">zone 20</option>
                                        <option value="21">zone 21</option>
                                        <option value="22">zone 22</option>
                                        <option value="23">zone 23</option>
                                        <option value="24">zone 24</option>
                                        <option value="25">zone 25</option>
                                    </select>
                                    <br/>
                                </div>
                                <div class="col-sm-1" style="padding:unset">ใช้เวลาจัดส่ง (วัน)</div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="day" name="day"
                                           placeholder="กรุณากรอกข้อมูล" required> <br/></div>
                                <div id="pickList"></div>
                                <br/>
                                <button type="submit" class="btn btn-primary waves-effect waves-light"
                                        id="save_zone">บันทึก
                                </button>
                            </form>
                        </div>
                        <!-- ******************************************************************************************************* -->
                        <div class="tab-pane p-3" id="settings" role="tabpanel">
                            <div class="card text-white card-dark">
                                <div class="card-body">
                                    <blockquote class="card-bodyquote mb-0">
                                        <h4><i class="far fa-file-excel"></i> .CSV Upload ราคาต้องไม่มี ( , )</h4>
                                        <div class="row">
                                            <div class="col-md-2"><input type="file" name="FileUpload" id="upload_price"
                                                                         accept=".csv" class="form-control"/></div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-primary waves-effect waves-light"
                                                        id="price_save"><i class="fas fa-check-circle"></i> ใช้งานทันที
                                                </button>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <input type="date" id="price_set_date" class="form-control"
                                                           placeholder="Email">
                                                    <span class="input-group-append">
                                                    <button type="button" class="btn  btn-primary"
                                                            id="price_set_date_click"><i class="fas fa-clock"></i>
                                                        กำหนดวันใช้งาน</button>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                <!--end card-body-->
                            </div>
                            <div id="show_new_price" @if(count($price_backup)==0) style="display:none" @endif>
                                <h4 class="card-title mt-0 title_edit_price">ราคาใหม่ (
                                    @if(count($price_backup)==0)
                                        ยังไม่ได้บันทึก
                                    @else
                                        <div class="spinner-border spinner-border-sm" role="status"></div>
                                        รอใช้งานจริงวันที่
                                        {{ date('d/m/Y', strtotime($price_backup[0]->set_date)) }}
                                    @endif
                                    )
                                </h4>
                                <table id="preTable" class="table table-striped  mb-0" style="background: #f2f5ff;">
                                    <thead>
                                    <tr>
                                        <th>Weight</th>
                                        <th>Zone1</th>
                                        <th>Zone2</th>
                                        <th>Zone3</th>
                                        <th>Zone4</th>
                                        <th>Zone5</th>
                                        <th>Zone6</th>
                                        <th>Zone7</th>
                                        <th>Zone8</th>
                                        <th>Zone9</th>
                                        <th>Zone10</th>
                                        <th>Zone11</th>
                                        <th>Zone12</th>
                                        <th>Zone13</th>
                                        <th>Zone14</th>
                                        <th>Zone15</th>
                                        <th>Zone16</th>
                                        <th>Zone17</th>
                                        <th>Zone18</th>
                                        <th>Zone19</th>
                                        <th>Zone20</th>
                                        <th>Zone21</th>
                                        <th>Zone22</th>
                                        <th>Zone23</th>
                                        <th>Zone24</th>
                                        <th>Zone25</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($price_backup as $item)
                                        <tr>
                                            <td>{{$item->weight}}</td>
                                            <td>{{$item->zone_1}}</td>
                                            <td>{{$item->zone_2}}</td>
                                            <td>{{$item->zone_3}}</td>
                                            <td>{{$item->zone_4}}</td>
                                            <td>{{$item->zone_5}}</td>
                                            <td>{{$item->zone_6}}</td>
                                            <td>{{$item->zone_7}}</td>
                                            <td>{{$item->zone_8}}</td>
                                            <td>{{$item->zone_9}}</td>
                                            <td>{{$item->zone_10}}</td>
                                            <td>{{$item->zone_11}}</td>
                                            <td>{{$item->zone_12}}</td>
                                            <td>{{$item->zone_13}}</td>
                                            <td>{{$item->zone_14}}</td>
                                            <td>{{$item->zone_15}}</td>
                                            <td>{{$item->zone_16}}</td>
                                            <td>{{$item->zone_17}}</td>
                                            <td>{{$item->zone_18}}</td>
                                            <td>{{$item->zone_19}}</td>
                                            <td>{{$item->zone_20}}</td>
                                            <td>{{$item->zone_21}}</td>
                                            <td>{{$item->zone_22}}</td>
                                            <td>{{$item->zone_23}}</td>
                                            <td>{{$item->zone_24}}</td>
                                            <td>{{$item->zone_25}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <br/>
                            </div>
                            <h4 class="card-title mt-0">ราคาที่ใช้งานอยู่ <span class="dripicons-document-edit"
                                                                                onclick="edit_price()"
                                                                                style="cursor: pointer;">Edit</span>
                            </h4>
                            <table id="mainTable" class="table table-striped  mb-0">
                                <thead>
                                <tr>
                                    <th>Weight</th>
                                    <th>Zone1</th>
                                    <th>Zone2</th>
                                    <th>Zone3</th>
                                    <th>Zone4</th>
                                    <th>Zone5</th>
                                    <th>Zone6</th>
                                    <th>Zone7</th>
                                    <th>Zone8</th>
                                    <th>Zone9</th>
                                    <th>Zone10</th>
                                    <th>Zone11</th>
                                    <th>Zone12</th>
                                    <th>Zone13</th>
                                    <th>Zone14</th>
                                    <th>Zone15</th>
                                    <th>Zone16</th>
                                    <th>Zone17</th>
                                    <th>Zone18</th>
                                    <th>Zone19</th>
                                    <th>Zone20</th>
                                    <th>Zone21</th>
                                    <th>Zone22</th>
                                    <th>Zone23</th>
                                    <th>Zone24</th>
                                    <th>Zone25</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($price as $item)
                                    <tr>
                                        <td>{{$item->weight}}</td>
                                        <td>{{$item->zone_1}}</td>
                                        <td>{{$item->zone_2}}</td>
                                        <td>{{$item->zone_3}}</td>
                                        <td>{{$item->zone_4}}</td>
                                        <td>{{$item->zone_5}}</td>
                                        <td>{{$item->zone_6}}</td>
                                        <td>{{$item->zone_7}}</td>
                                        <td>{{$item->zone_8}}</td>
                                        <td>{{$item->zone_9}}</td>
                                        <td>{{$item->zone_10}}</td>
                                        <td>{{$item->zone_11}}</td>
                                        <td>{{$item->zone_12}}</td>
                                        <td>{{$item->zone_13}}</td>
                                        <td>{{$item->zone_14}}</td>
                                        <td>{{$item->zone_15}}</td>
                                        <td>{{$item->zone_16}}</td>
                                        <td>{{$item->zone_17}}</td>
                                        <td>{{$item->zone_18}}</td>
                                        <td>{{$item->zone_19}}</td>
                                        <td>{{$item->zone_20}}</td>
                                        <td>{{$item->zone_21}}</td>
                                        <td>{{$item->zone_22}}</td>
                                        <td>{{$item->zone_23}}</td>
                                        <td>{{$item->zone_24}}</td>
                                        <td>{{$item->zone_25}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- ******************************************************************************************************* -->
                        <div class="tab-pane p-3" id="esc" role="tabpanel">
                            <div class="card text-white card-dark">
                                <div class="card-body">
                                    <blockquote class="card-bodyquote mb-0">
                                        <h4><i class="far fa-file-excel"></i> .CSV Upload ค่าบริการฉุกเฉินกรณีฉุกเฉินต้องไม่มี ( , )</h4>
                                        <div class="row">
                                            <div class="col-md-2"><input type="file" name="FileUpload" id="upload_esc"
                                                                         accept=".csv" class="form-control"/></div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-primary waves-effect waves-light"
                                                        id="esc_save"><i class="fas fa-check-circle"></i> ใช้งานทันที
                                                </button>
                                            </div>
{{--                                            <div class="col-md-3">--}}
{{--                                                <div class="input-group">--}}
{{--                                                    <input type="date" id="price_set_date" class="form-control"--}}
{{--                                                           placeholder="Email">--}}
{{--                                                    <span class="input-group-append">--}}
{{--                                                    <button type="button" class="btn  btn-primary"--}}
{{--                                                            id="price_set_date_click"><i class="fas fa-clock"></i>--}}
{{--                                                        กำหนดวันใช้งาน</button>--}}
{{--                                                </span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                        </div>
                                    </blockquote>
                                </div>
                                <!--end card-body-->
                            </div>
                            <div id="show_new_surcharges" style="display:none">
                                <h4 class="card-title mt-0 title_edit_esc">ค่าบริการฉุกเฉินกรณีฉุกเฉินใหม่ (ยังไม่ได้บันทึก)
                                </h4>
                                <table id="escTable" class="table table-striped  mb-0" style="background: #f2f5ff;">
                                    <thead>
                                    <tr>
                                        <th>Weight</th>
                                        <th>Zone1</th>
                                        <th>Zone2</th>
                                        <th>Zone3</th>
                                        <th>Zone4</th>
                                        <th>Zone5</th>
                                        <th>Zone6</th>
                                        <th>Zone7</th>
                                        <th>Zone8</th>
                                        <th>Zone9</th>
                                        <th>Zone10</th>
                                        <th>Zone11</th>
                                        <th>Zone12</th>
                                        <th>Zone13</th>
                                        <th>Zone14</th>
                                        <th>Zone15</th>
                                        <th>Zone16</th>
                                        <th>Zone17</th>
                                        <th>Zone18</th>
                                        <th>Zone19</th>
                                        <th>Zone20</th>
                                        <th>Zone21</th>
                                        <th>Zone22</th>
                                        <th>Zone23</th>
                                        <th>Zone24</th>
                                        <th>Zone25</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($surcharges as $item)
                                        <tr>
                                            <td>{{$item->weight}}</td>
                                            <td>{{$item->zone_1}}</td>
                                            <td>{{$item->zone_2}}</td>
                                            <td>{{$item->zone_3}}</td>
                                            <td>{{$item->zone_4}}</td>
                                            <td>{{$item->zone_5}}</td>
                                            <td>{{$item->zone_6}}</td>
                                            <td>{{$item->zone_7}}</td>
                                            <td>{{$item->zone_8}}</td>
                                            <td>{{$item->zone_9}}</td>
                                            <td>{{$item->zone_10}}</td>
                                            <td>{{$item->zone_11}}</td>
                                            <td>{{$item->zone_12}}</td>
                                            <td>{{$item->zone_13}}</td>
                                            <td>{{$item->zone_14}}</td>
                                            <td>{{$item->zone_15}}</td>
                                            <td>{{$item->zone_16}}</td>
                                            <td>{{$item->zone_17}}</td>
                                            <td>{{$item->zone_18}}</td>
                                            <td>{{$item->zone_19}}</td>
                                            <td>{{$item->zone_20}}</td>
                                            <td>{{$item->zone_21}}</td>
                                            <td>{{$item->zone_22}}</td>
                                            <td>{{$item->zone_23}}</td>
                                            <td>{{$item->zone_24}}</td>
                                            <td>{{$item->zone_25}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <br/>
                            </div>
                            <h4 class="card-title mt-0">ค่าบริการฉุกเฉินกรณีฉุกเฉิน <span class="dripicons-document-edit"
                                                                                onclick="edit_surcharges()"
                                                                                style="cursor: pointer;">Edit</span>
                            </h4>
                            <table id="mainESCTable" class="table table-striped  mb-0">
                                <thead>
                                <tr>
                                    <th>Weight</th>
                                    <th>Zone1</th>
                                    <th>Zone2</th>
                                    <th>Zone3</th>
                                    <th>Zone4</th>
                                    <th>Zone5</th>
                                    <th>Zone6</th>
                                    <th>Zone7</th>
                                    <th>Zone8</th>
                                    <th>Zone9</th>
                                    <th>Zone10</th>
                                    <th>Zone11</th>
                                    <th>Zone12</th>
                                    <th>Zone13</th>
                                    <th>Zone14</th>
                                    <th>Zone15</th>
                                    <th>Zone16</th>
                                    <th>Zone17</th>
                                    <th>Zone18</th>
                                    <th>Zone19</th>
                                    <th>Zone20</th>
                                    <th>Zone21</th>
                                    <th>Zone22</th>
                                    <th>Zone23</th>
                                    <th>Zone24</th>
                                    <th>Zone25</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($surcharges as $item)
                                    <tr>
                                        <td>{{$item->weight}}</td>
                                        <td>{{$item->zone_1}}</td>
                                        <td>{{$item->zone_2}}</td>
                                        <td>{{$item->zone_3}}</td>
                                        <td>{{$item->zone_4}}</td>
                                        <td>{{$item->zone_5}}</td>
                                        <td>{{$item->zone_6}}</td>
                                        <td>{{$item->zone_7}}</td>
                                        <td>{{$item->zone_8}}</td>
                                        <td>{{$item->zone_9}}</td>
                                        <td>{{$item->zone_10}}</td>
                                        <td>{{$item->zone_11}}</td>
                                        <td>{{$item->zone_12}}</td>
                                        <td>{{$item->zone_13}}</td>
                                        <td>{{$item->zone_14}}</td>
                                        <td>{{$item->zone_15}}</td>
                                        <td>{{$item->zone_16}}</td>
                                        <td>{{$item->zone_17}}</td>
                                        <td>{{$item->zone_18}}</td>
                                        <td>{{$item->zone_19}}</td>
                                        <td>{{$item->zone_20}}</td>
                                        <td>{{$item->zone_21}}</td>
                                        <td>{{$item->zone_22}}</td>
                                        <td>{{$item->zone_23}}</td>
                                        <td>{{$item->zone_24}}</td>
                                        <td>{{$item->zone_25}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end card-->
        </div>
        <!--end col-->
    </div>

    <script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>
    <script src="{{ asset('/assets/pages/pickList.js')}}"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $("#zone_form").submit(function (e) {
                $('#loading').show();
                e.preventDefault();
                var thisform = $(this);
                var url = thisform.attr('action');

                $.ajax({
                    type: "POST",
                    data: JSON.stringify({
                        input_zone: $('#zone').val(),
                        input_day: $('#day').val(),
                        country: pick.getValues()
                    }),
                    url: url,
                    success: function (data) {
                        //alert(data);
                        //console.log(pick.getValues()) ;
                        $('#loading').hide();
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                        $('#loading').hide();
                    }
                });
            });

        });

        function edit_price() {
            $('#show_new_price').show();
            $('#preTable').find('tbody').html($('#mainTable').find('tbody').html());
            var rows = $('#preTable tbody tr');
            rows.each(function (i) {
                var td = $(this).find('td');
                $(td).attr('contenteditable', 'true');
            });
            $('.title_edit_price').html('ราคาใหม่ (ยังไม่ได้บันทึก)')
        }

        function edit_surcharges() {
            $('#show_new_surcharges').show();
            $('#escTable').find('tbody').html($('#mainESCTable').find('tbody').html());
            var rows = $('#escTable tbody tr');
            rows.each(function (i) {
                var td = $(this).find('td');
                $(td).attr('contenteditable', 'true');
            });
            $('.title_edit_esc').html('ค่าบริการฉุกเฉินกรณีฉุกเฉิน (ยังไม่ได้บันทึก)')
        }

        var pick = $("#pickList");
        var pickresult = null;

        function zone_init(zone_id) {
            $('#loading').show();
            pick.html('');
            $('#day').val('');
            $.ajax({
                url: "{{ url('/admin/getCountryForZone/'.$service->id) }}",
                cache: false,
                success: function (result) {
                    pickresult = result;
                }
            });
            $.ajax({
                url: "{{ url('/admin/getBussinessDayByServiceZone/'.$service->id) }}" + "/" + zone_id,
                cache: false,
                success: function (result) {
                    var val = $.parseJSON(result);
                    $('#day').val(val[0].day);
                }
            });
            $.ajax({
                url: "{{ url('/admin/getZone/'.$service->id) }}/" + zone_id,
                cache: false,
                success: function (result) {
                    var val = $.parseJSON(result);
                    var setoption = '';
                    $.each(val, function (index, data) {
                        setoption += '<option data-id="' + data.id + '">' + data.text + '</option>';
                    });
                    pick.pickList({data: $.parseJSON(pickresult)}, setoption);
                }
            });


            $('#loading').hide();
        }

        $('#price_save').click(function () {
            $('#loading').show();
            var ary = [];
            var rows = $('#preTable tbody tr');
            if (rows.length < 1) {
                alert("กรุณาอัพโหลดไฟล์ราคาก่อนกดบันทึก");
                $('#loading').hide();
            } else {
                rows.each(function (i) {
                    var td = $(this).find('td');
                    ary.push({
                        weight: td[0].innerText,
                        zone_1: td[1].innerText,
                        zone_2: td[2].innerText,
                        zone_3: td[3].innerText,
                        zone_4: td[4].innerText,
                        zone_5: td[5].innerText,
                        zone_6: td[6].innerText,
                        zone_7: td[7].innerText,
                        zone_8: td[8].innerText,
                        zone_9: td[9].innerText,
                        zone_10: td[10].innerText,
                        zone_11: td[11].innerText,
                        zone_12: td[12].innerText,
                        zone_13: td[13].innerText,
                        zone_14: td[14].innerText,
                        zone_15: td[15].innerText,
                        zone_16: td[16].innerText,
                        zone_17: td[17].innerText,
                        zone_18: td[18].innerText,
                        zone_19: td[19].innerText,
                        zone_20: td[20].innerText,
                        zone_21: td[21].innerText,
                        zone_22: td[22].innerText,
                        zone_23: td[23].innerText,
                        zone_24: td[24].innerText,
                        zone_25: td[25].innerText,
                    });
                });
                var jsonData = JSON.stringify({data: ary});
                $.ajax({
                    type: "POST",
                    url: "/admin/service/setprice/{{$service->id}}",
                    data: jsonData, // serializes the form's elements.
                    contentType: "json",
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        location.reload();
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                        $('#loading').hide();
                    }
                });
            }
            console.log(ary);
        });

        $('#esc_save').click(function () {
            $('#loading').show();
            var ary = [];
            var rows = $('#escTable tbody tr');
            if (rows.length < 1) {
                alert("กรุณาอัพโหลดไฟล์ราคาก่อนกดบันทึก");
                $('#loading').hide();
            } else {
                rows.each(function (i) {
                    var td = $(this).find('td');
                    ary.push({
                        weight: td[0].innerText,
                        zone_1: td[1].innerText,
                        zone_2: td[2].innerText,
                        zone_3: td[3].innerText,
                        zone_4: td[4].innerText,
                        zone_5: td[5].innerText,
                        zone_6: td[6].innerText,
                        zone_7: td[7].innerText,
                        zone_8: td[8].innerText,
                        zone_9: td[9].innerText,
                        zone_10: td[10].innerText,
                        zone_11: td[11].innerText,
                        zone_12: td[12].innerText,
                        zone_13: td[13].innerText,
                        zone_14: td[14].innerText,
                        zone_15: td[15].innerText,
                        zone_16: td[16].innerText,
                        zone_17: td[17].innerText,
                        zone_18: td[18].innerText,
                        zone_19: td[19].innerText,
                        zone_20: td[20].innerText,
                        zone_21: td[21].innerText,
                        zone_22: td[22].innerText,
                        zone_23: td[23].innerText,
                        zone_24: td[24].innerText,
                        zone_25: td[25].innerText,
                    });
                });
                var jsonData = JSON.stringify({data: ary});
                $.ajax({
                    type: "POST",
                    url: "/admin/service/setesc/{{$service->id}}",
                    data: jsonData, // serializes the form's elements.
                    contentType: "json",
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        location.reload();
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                        $('#loading').hide();
                    }
                });
            }
            console.log(ary);
        });

        $('#price_set_date_click').click(function () {
            $('#loading').show();
            var ary = [];
            var rows = $('#preTable tbody tr');
            if (rows.length < 1) {
                alert("กรุณาอัพโหลดไฟล์ราคาก่อนกดกำหนดวันที่");
                $('#loading').hide();
            } else if ($('#price_set_date').val() == '') {
                alert("กรุณาเลือกวันที่ก่อนกดกำหนดวันที่");
                $('#loading').hide();
            } else {
                rows.each(function (i) {
                    var td = $(this).find('td');
                    ary.push({
                        weight: td[0].innerText,
                        zone_1: td[1].innerText,
                        zone_2: td[2].innerText,
                        zone_3: td[3].innerText,
                        zone_4: td[4].innerText,
                        zone_5: td[5].innerText,
                        zone_6: td[6].innerText,
                        zone_7: td[7].innerText,
                        zone_8: td[8].innerText,
                        zone_9: td[9].innerText,
                        zone_10: td[10].innerText,
                        zone_11: td[11].innerText,
                        zone_12: td[12].innerText,
                        zone_13: td[13].innerText,
                        zone_14: td[14].innerText,
                        zone_15: td[15].innerText,
                        zone_16: td[16].innerText,
                        zone_17: td[17].innerText,
                        zone_18: td[18].innerText,
                        zone_19: td[19].innerText,
                        zone_20: td[20].innerText,
                        zone_21: td[21].innerText,
                        zone_22: td[22].innerText,
                        zone_23: td[23].innerText,
                        zone_24: td[24].innerText,
                        zone_25: td[25].innerText
                    });
                });
                var jsonData = JSON.stringify({data: ary, date: $('#price_set_date').val()});
                $.ajax({
                    type: "POST",
                    url: "/admin/service/price_set_date/{{$service->id}}",
                    data: jsonData, // serializes the form's elements.
                    contentType: "json",
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        location.reload();
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                        $('#loading').hide();
                    }
                });
            }
            console.log(ary);
        });

        $("#getSelected").click(function () {
            console.log(pick.getValues());
        });

        $("#upload_price").change(function (e) {
            var ext = $("input#upload_price").val().split(".").pop().toLowerCase();
            $('#preTable').find('tbody').html('');
            if ($.inArray(ext, ["csv"]) == -1) {
                alert('Upload CSV');
                return false;
            }

            if (e.target.files != undefined) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var csvval = e.target.result.split("\n");
                    $('#show_new_price').show();

                    $.each(csvval, function (index, data) {
                        var csvvalue = csvval[index].split(",");
                        if (csvvalue.length > 0 && csvval[index] != '') {
                            var output = '<tr>';
                            $.each(csvvalue, function (index, data) {
                                if (index < 26) {
                                    output += '<td contenteditable="true">' + data + '</td>'
                                }
                                ;
                            });

                            var i;
                            var cslength = 27 - csvvalue.length;
                            for (i = 0; i < cslength; i++) {
                                output += '<td contenteditable="true"></td>'
                            }
                            output += '</tr>';

                            $('#preTable').find('tbody').append(output)
                        }
                    });
                };

                reader.readAsText(e.target.files.item(0));
            }

            return false;

        });

        $("#upload_esc").change(function (e) {
            var ext = $("input#upload_esc").val().split(".").pop().toLowerCase();
            $('#preTable').find('tbody').html('');
            if ($.inArray(ext, ["csv"]) == -1) {
                alert('Upload CSV');
                return false;
            }

            if (e.target.files != undefined) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var csvval = e.target.result.split("\n");
                    $('#show_new_surcharges').show();

                    $.each(csvval, function (index, data) {
                        var csvvalue = csvval[index].split(",");
                        if (csvvalue.length > 0 && csvval[index] != '') {
                            var output = '<tr>';
                            $.each(csvvalue, function (index, data) {
                                if (index < 26) {
                                    output += '<td contenteditable="true">' + data + '</td>'
                                }
                                ;
                            });

                            var i;
                            var cslength = 27 - csvvalue.length;
                            for (i = 0; i < cslength; i++) {
                                output += '<td contenteditable="true"></td>'
                            }
                            output += '</tr>';

                            $('#escTable').find('tbody').append(output)
                        }
                    });
                };

                reader.readAsText(e.target.files.item(0));
            }

            return false;

        });

        zone_init(1);
    </script>

@endsection
