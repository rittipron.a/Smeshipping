@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item active">Service</li>
                </ol>
            </div>
            <h4 class="page-title">Service</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">Edit Table With Button</h4>
                <p class="text-muted mb-3">Add toolbar column with edit and delete buttons.</p>
                <button type="button" class="btn btn-primary waves-effect waves-light float-left mb-3"
                    data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg">+ Add New</button>
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                            <tr>
                                <th>Image</th>
                                <th>Tracking</th>
                                <th>ID</th>
                                <th>Description</th>
                                <th>Cal</th>
                                <th>Date</th>
                                <th></th>
                            </tr>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($service as $item)
                            <tr id="s_{{$item->id}}">
                                <td><img src="https://www.smeshipping.com/images/iconexport_09.jpg" alt=""
                                        class="rounded-circle thumb-sm mr-1"></td>
                                <td>{{$item->tracking}}</td>
                                <td>{{$item->id}}</td>
                                <td>
                                    <i class="mdi mdi-earth"
                                        style="color:green; font-size:18px; {!! $item->active=='1'?'':'display:none;'!!}"
                                        data-toggle="tooltip-custom" data-placement="top" title=""
                                        data-original-title="ใช้คำนวณผล"></i>
                                    [{{$item->cat!=null?$item->type_code:'None type of service.'}}]
                                    {{$item->cat!=null?$item->type_name:''}}
                                    <br />
                                    {{$item->description}}
                                </td>
                                <td>{{$item->cal_weight}}</td>
                                <td>{{$item->updated_at}}</td>
                                <td>
                                    <a href="{{url('/admin/service').'/'.$item->id}}" class="mr-2"><i
                                            class="fas fa-edit text-info font-16"></i></a>
                                    <a href="{{url('/admin/copyService').'/'.$item->id}}" class="mr-2"><i
                                            class="far fa-copy font-16"></i></i></a>
                                    <i class=" fas fa-trash-alt text-danger font-16"
                                        onclick="delete_service({{$item->id}})" style="cursor: pointer;"></i>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!-- end col -->
    <div class=" col-12">
        {{ $service->links() }}
    </div>
</div>



<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Service</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" class="form-horizontal auth-form my-4" id="service_form" action="/admin/service">
                    @csrf
                    <!--<div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">Routing ID :</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="routing_id" name="routing_id" value="" required
                                placeholder="กรุณากรอกข้อมูล">
                            <div class="emailHelp">Routing ID :คือรหัสอ้างอิงของการออก AWB เพื่อให้ทาง EO
                                ทราบว่าใช้หมายเลข Account number ใดในการออก AWB โดยเราใช้ 1 ตัวอักษร เช่น D =
                                DHL
                                F=FEDEX T=TNT และ หมายเลข Account number 3 หลักสุดท้าย ที่ใช้ออก AWB ของ Service
                                ตัวนี้ ซึ่งจะออกมาในรูปแบบ "D894"</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">Service Code :</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="service_code" name="service_code" value=""
                                required placeholder="กรุณากรอกข้อมูล">
                            <div class="emailHelp">Service Code :คือการกำหนดรหัสของบริการ รายละเอียด วิธีการจัด
                                Service code พร้อมทั้งรายชื่อลูกค้าที่ได้ราคาต่างๆ <a target="_blank"
                                    href="https://goo.gl/S8Evl3">https://goo.gl/S8Evl3</a></div>
                        </div>
                    </div>
                    -->
                    <div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">TYPE OF SERVICE:</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="type_of_service" name="type_of_service">
                                @foreach ($servicetype as $servicetypeitem)
                                <option value="{{$servicetypeitem->id}}">
                                    [{{$servicetypeitem->code}}] {{$servicetypeitem->name}}</option>
                                @endforeach
                            </select>
                            <div class="emailHelp">TYPE OF SERVICE :คือ ประเภทของบริการ</div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">Product Name :</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="product_name" name="product_name" value=""
                                required placeholder="กรุณากรอกข้อมูล">
                            <div class="emailHelp">Product Name :คือการตั้งชื่อบริการ เป็นส่วนที่ลูกค้าจะเห็นได้
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">Description :</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="description" name="description" value=""
                                required placeholder="กรุณากรอกข้อมูล">
                            <div class="emailHelp">Description :คำอธิบายบริการ ที่เห็นเฉพาะเจ้าหน้าที่ของเรา
                            </div>
                        </div>
                    </div>
                    <!--<div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">Tracking :</label>
                        <div class="col-sm-10">
                            <select name="tracking" id="tracking" class="form-control">
                                <option value="" selected>-NONE-</option>
                                <option value="DHL">DHL</option>
                                <option value="GMB">GMB</option>
                                <option value="Aramex">Aramex</option>
                                <option value="FEDEX">FEDEX
                                </option>
                                <option value="TNT">TNT</option>
                            </select>
                            <div class="emailHelp">Tracking : Website For Tracking Shipment</div>
                        </div>
                    </div>-->
                    <div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">วิธีคิดราคา :</label>
                        <div class="col-md-9">
                            <div class="form-check-inline my-1">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="cal1" name="cal" value="weight"
                                        class="custom-control-input">
                                    <label class="custom-control-label" for="cal1">คิดราคาจากน้ำหนัก</label>
                                </div>
                            </div>
                            <div class="form-check-inline my-1">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="cal2" name="cal" value="box" checked
                                        class="custom-control-input">
                                    <label class="custom-control-label" for="cal2">คิดราคาจากน้ำหนัก
                                        และขนาดกล่อง</label>
                                </div>
                            </div>
                            <div class="emailHelp">- คิดราคาจากน้ำหนัก ใช้กับ Global Mail
                            </div>
                            <div class="emailHelp">- คิดราคาจากน้ำหนัก และขนาดกล่อง ใช้กับ Express</div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">วิธีการคิดน้ำหนัก
                            :</label>
                        <div class="col-md-9">
                            <div class="form-check-inline my-1">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="cal_weight" name="cal_weight" value=""
                                        class="custom-control-input">
                                    <label class="custom-control-label" for="cal_weight">คิดตามน้ำหนักจริง</label>
                                </div>
                            </div>
                            <div class="form-check-inline my-1">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="cal_weight2" name="cal_weight" checked value="up"
                                        class="custom-control-input">
                                    <label class="custom-control-label" for="cal_weight2">ปัดขึ้นทีละ
                                        0.5</label>
                                </div>
                            </div>
                            <div class="emailHelp">- คิดตามน้ำหนักจริง ใช้กับ Global Mail</div>
                            <div class="emailHelp">- ปัดขึ้นทีละ 0.5 ใช้กับ Express</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">ชนิด :</label>
                        <div class="col-md-9">
                            <div class="form-check-inline my-1">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="type" name="type" value="doc" class="custom-control-input"
                                        checked>
                                    <label class="custom-control-label" for="type">เอกสาร</label>
                                </div>
                            </div>
                            <div class="form-check-inline my-1">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="type1" name="type" value="box" checked
                                        class="custom-control-input">
                                    <label class="custom-control-label" for="type1">พัสดุ</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">Product drop point
                            :</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="prod_drop_point" name="prod_drop_point" value=""
                                placeholder="กรุณากรอกข้อมูล">
                            <div class="emailHelp">ข้อความแสดงหน้าราคา</div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="horizontalInput2" class="col-sm-2 col-form-label">Active :</label>
                        <div class="col-sm-10">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="active" name="active" checked />
                                <label class="custom-control-label" for="active"></label>
                            </div>
                            <div class="emailHelp">เลือกเพื่อกำหนดว่าจะใช้งานบริการนี้หรือไม่
                            </div>
                        </div>
                    </div>-->
                    <button type="submit" class="btn btn-primary waves-effect waves-light"
                        id="save_service">บันทึก</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>
<script>
    $("#service_form").submit(function (e) {
            $('#loading').show();
            e.preventDefault();
            var thisform = $(this);
            var url = thisform.attr('action');
            $.ajax({
                type: "POST",
                data: $('#service_form').serialize(), 
                url: url,
                success: function (data) {
                    window.location.replace("{{url('admin/service/')}}"+'/'+data);
                    $('#loading').hide();
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
        });
        function delete_service(id) {
            var x = confirm("Are you sure you want to delete?");
            if (x){
                $('#loading').show();
                var url = "{{url('admin/service/')}}"+"/"+id;
                $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "DELETE",
                    url: url,
                    success: function (data) {
                        //alert(data);
                        $('#s_'+id).remove();
                        $('#loading').hide();
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                        $('#loading').hide();
                    }
                });
            }
            else{
                return false;
            }   
        };
</script>
@endsection