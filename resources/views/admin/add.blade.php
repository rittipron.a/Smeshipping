@extends('layouts.app')

@section('content')


<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/css/uikit.css">
<link rel="stylesheet" href="{{ asset('/assets/dist/jquery.Thailand.min.css')}}">


<div id="loader">
    <div uk-spinner></div> รอสักครู่ กำลังโหลดฐานข้อมูล...
</div>
<div id="demo1" class="demo" style="display:none;" autocomplete="off" uk-grid>

    <div class="uk-width-1-2@m">
        <label class="h2">ตำบล / แขวง</label>
        <input name="district" class="uk-input uk-width-1-1" type="text" autocomplete="new-password">
    </div>

    <div class="uk-width-1-2@m">
        <label class="h2">อำเภอ / เขต</label>
        <input name="amphoe" class="uk-input uk-width-1-1" type="text" autocomplete="new-password">
    </div>

    <div class="uk-width-1-2@m">
        <label class="h2">จังหวัด</label>
        <input name="province" class="uk-input uk-width-1-1" type="text" autocomplete="new-password">
    </div>

    <div class="uk-width-1-2@m">
        <label class="h2">รหัสไปรษณีย์</label>
        <input name="zipcode" class="uk-input uk-width-1-1" type="text" autocomplete="new-password">
    </div>

</div>



<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset('/assets/dependencies/JQL.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('/assets/dependencies/typeahead.bundle.js')}}"></script>
<link rel="stylesheet" href="{{ asset('/assets/dist/jquery.Thailand.min.css')}}">
<script type="text/javascript" src="{{ asset('/assets/dist/jquery.Thailand.min.js')}}"></script>

<script type="text/javascript">
    $.Thailand({
            database: '{{ asset('/assets/database/db.json')}}', 
            $district: $('#demo1 [name="district"]'),
            $amphoe: $('#demo1 [name="amphoe"]'),
            $province: $('#demo1 [name="province"]'),
            $zipcode: $('#demo1 [name="zipcode"]'),

            onDataFill: function(data){
                console.info('Data Filled', data);
            },

            onLoad: function(){
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });

        // watch on change

        $('#demo1 [name="district"]').change(function(){
            console.log('ตำบล', this.value);
        });
        $('#demo1 [name="amphoe"]').change(function(){
            console.log('อำเภอ', this.value);
        });
        $('#demo1 [name="province"]').change(function(){
            console.log('จังหวัด', this.value);
        });
        $('#demo1 [name="zipcode"]').change(function(){
            console.log('รหัสไปรษณีย์', this.value);
        });
        
</script>
@endsection