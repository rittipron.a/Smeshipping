@extends('layouts.app')

@section('content')
<style>
    .center_text {
        text-align: center;
    }

    .space_top {
        margin-top: 7px;
    }

    .disa {
        background: #f3f3f391;
        color: red;
    }

    #notification {
        display: none;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th,
    td {
        padding: 8px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }

    tr:hover {
        background-color: #f5f5f5;
        cursor: pointer;
    }

    .topic {
        text-align: right;
        font-size: 18px;
        font-weight: 800;
    }

    .detail {
        font-size: 18px;
    }
</style>

<div class="row" onload="startTime()">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Customer key description</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">ส่งลิงค์ให้ลูกค้า</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-primary waves-effect waves-light float-left mb-3"
                            data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg"
                            id="add">รับลิงค์สำรหับส่งให้ลูกค้า</button>
                        <button type="button" style="display:none" data-toggle="modal" data-animation="bounce"
                            data-target=".view_detail" id="view">ดูรายละเอียด</button>
                    </div>
                    <div class="col-md-6" style="text-align:right">
                        <form action="#" method="GET">
                            <div class="input-group">
                                <input type="text" class="form-control" name="keyword" placeholder="Search for..."
                                    value="{{app('request')->input('keyword')}}" aria-label="ค้นหา">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="submit">ค้นหา</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table mb-0" id="my-table">
                        <thead>
                            <tr>
                                <th>เลข Tracking</th>
                                <th>ชื่อผู้ส่ง</th>
                                <th>เบอร์โทร</th>
                                <th>ชื่อผู้รับ</th>
                                <th>หมายเหตุ</th>
                                <th>วันที่</th>
                                <th>ผู้สร้างลิงค์</th>
                                <th class="center_text">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            <tr ondblclick="view('{{json_encode($item)}}')" class="{!!$item->
                                del!=null?'disa':''!!}">
                                <td>{{$item->tracking}}</td>
                                <td>{{$item->sender_name}}</td>
                                <td>{{$item->sender_tel}}</td>
                                <td>{{$item->receiver_name}}</td>
                                <td>{{$item->note}}</td>
                                <td>{{ date('d/M/Y H:m:s' , strtotime($item->created_at))}}</td>
                                <td>{{$item->editname}}</td>
                                <td class=" center_text">
                                    <div class="btn-group btn-group-sm" style="float: none;">
                                        @if($item->del==null) <button type="button" class="btn btn-sm btn-danger"
                                            style="float: none; margin: 4px;" onclick="del({{$item->id}})">
                                            จบการทำงาน
                                        </button>
                                        @endif
                                        @if($item->sender_name!=null)
                                        <button type="button" class="btn btn-sm btn-info"
                                            style="float: none; margin: 4px;" onclick="view('{{json_encode($item)}}')">
                                            ดูรายละเอียด
                                        </button>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <!--end table-->

            </div>

            <!--end card-body-->
        </div>
        {{ $data->appends(request()->input())->links() }}
        <!--end card-->
    </div>
    <div class="col-12">
    </div>
    <!-- end col -->
</div>



<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" role="dialog" aria-labelledby="bs-example-modal-lg" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="bs-example-modal-lg">สร้างลิงค์</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="LeadName">URL</label>
                            <input type="text" class="form-control" id="url" name="url" readonly>
                        </div>
                    </div>
                </div>

                <button type="button" class="btn btn-sm btn-success" onclick="copylink()">
                    คัดลอกลิงค์
                </button>
                <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-hidden="true">
                    ปิด
                </button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--  Modal content for the above example -->
<div class="modal fade view_detail" role="dialog" aria-labelledby="bs-example-modal-lg" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title mt-0" id="view_detail">รายละเอียด</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="background-color: white;">
                <form action="#" method="GET" name="getinbox" id="getinbox" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 topic">
                            Tracking Number :
                        </div>
                        <div class="col-md-6 detail" id="tracking">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            ชื่อผู้ส่ง :
                        </div>
                        <div class="col-md-6 detail" id="sender_name">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            ที่อยู่ผู้ส่ง :
                        </div>
                        <div class="col-md-6 detail" id="sender_address">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            เบอร์โทรผู้ส่ง :
                        </div>
                        <div class="col-md-6 detail" id="sender_tel">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            E-mail ผู้ส่ง :
                        </div>
                        <div class="col-md-6 detail" id="sender_email">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            ชื่อผู้รับ :
                        </div>
                        <div class="col-md-6 detail" id="receiver_name">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            ที่อยู่ผู้รับ :
                        </div>
                        <div class="col-md-6 detail" id="receiver_address">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            เบอร์โทรผู้รับ :
                        </div>
                        <div class="col-md-6 detail" id="receiver_tel">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            E-mail ผู้รับ :
                        </div>
                        <div class="col-md-6 detail" id="receiver_email">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            ชื่อประเภทสินค้า :
                        </div>
                        <div class="col-md-6 detail" id="commodity_description">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            จำนวนในกล่อง :
                        </div>
                        <div class="col-md-6 detail" id="quantity_unit">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            สกุลเงินของสินค้า :
                        </div>
                        <div class="col-md-6 detail" id="currency">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            จำนวนกล่องทั้งหมด :
                        </div>
                        <div class="col-md-6 detail" id="total_packages">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            มูลค่าสินค้า :
                        </div>
                        <div class="col-md-6 detail" id="value">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            สินค้า/จำนวน/มูลค่า(บาท) :
                        </div>
                        <div class="col-md-6 detail" id="shipdetail">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            หมายเหตุ :
                        </div>
                        <div class="col-md-6 detail" id="note">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            ส่งมาโดยใช้ :
                        </div>
                        <div class="col-md-6 detail" id="dalivery">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-6 topic">
                            หมายเลขจัดส่ง :
                        </div>
                        <div class="col-md-6 detail" id="dalivery_tracking">
                            กำลังรอข้อมูล...
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"
                                aria-hidden="true">ปิด</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>

<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script>
    $('#add').click(function(){
            $('#loading').show();
            $.ajax({
                type: 'GET',
                url: "{{url('/admin/inboxforlink_getlink')}}",
                success: function (result) {
                    result = JSON.parse(result)
                    console.log(result);
                    $('#url').val('https://backend.smeshipping.com/customer/'+result.uuid);
                    var html = '<tr><td>'+result.tracking+'</td> <td></td><td></td> <td></td><td></td><td>01/Apr/2020 15:04:52</td><td>admin</td><td class="center_text"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-sm btn-success" onclick="setcopylink(\'https://backend.smeshipping.com/customer/'+result.uuid+'\')">ดูลิงค์</button></div></td><tr>';
                    $('#my-table tbody').prepend(html)
                    $('#loading').hide();
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
        });

    function view(obj){
        obj = JSON.parse(obj)
        console.log(obj);
        $('#tracking').text(obj.tracking);
        $('#sender_name').text(obj.sender_name);
        $('#sender_address').text(obj.sender_address);
        $('#sender_tel').text(obj.sender_tel);
        $('#sender_email').text(obj.sender_email);
        $('#receiver_name').text(obj.receiver_name);
        $('#receiver_address').text(obj.receiver_address);
        $('#receiver_tel').text(obj.receiver_tel);
        $('#receiver_email').text(obj.receiver_email);
        $('#commodity_description').text(obj.commodity_description);
        $('#quantity_unit').text(obj.quantity_unit);
        $('#currency').text(obj.currency);
        $('#total_packages').text(obj.total_packages);
        $('#note').text(obj.note);
        $('#value').text(obj.value);
        $('#shipdetail').text(obj.shipdetail);
        $('#dalivery').text(obj.dalivery);
        $('#dalivery_tracking').text(obj.dalivery_tracking);
        $('#view').click();
    }

    function setcopylink(text){
        alert(text);
    }
    function copylink(){
        var copyText = document.getElementById("url");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");
    }

    function del(id){
        $.ajax({
                type: 'GET',
                url: "{{url('/admin/inboxforlink_del')}}/"+id,
                success: function (result) {
                    if(result=='success'){
                      location.reload();  
                    }
                    $('#loading').hide();
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
    }
</script>
@endsection