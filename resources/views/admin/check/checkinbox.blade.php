@extends('layouts.app')

@section('content')
<style>
    .center_text {
        text-align: center;
    }

    .space_top {
        margin-top: 7px;
    }

    .disa {
        background: #f3f3f391;
    }

    #notification {
        display: none;
    }
</style>
<div class="row" onload="startTime()">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Country</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title"> Check In</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary waves-effect waves-light float-left mb-3"
                            data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg" id="add">+
                            Add
                            New</button>
                    </div>
                    <div class="col-md-4">
                        <form action="{{url('admin/checkinbox')}}" method="GET">
                            <div class="input-group">
                                <input type="text" class="form-control" name="keyword" placeholder="Search for..."
                                    value="{{app('request')->input('keyword')}}" aria-label="ค้นหา">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="submit">ค้นหา</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table mb-0" id="my-table">
                        <thead>
                            <tr>
                                <th>ส่งของโดย</th>
                                <th>ชื่อผู้ส่ง</th>
                                <th>วันที่รับ</th>
                                <th>เลข Tracking</th>
                                <th class="center_text">รูปถ่าย</th>
                                <th class="center_text">รายละเอียด</th>
                                <th class="center_text">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            <tr class="{{$item->status==2?'disa':''}}">
                                <td>{{$item->delivery_by}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{date('d/m/Y',strtotime($item->created_at))}}</td>
                                <td>{{$item->tracking_no}}</td>
                                <td class="center_text ">
                                    @if($item->image!=null)
                                    @foreach(explode(',', $item->image) as $info)
                                    <a href="{{ asset('/uploads/inbox/'.$info) }}" target="_new"> <img
                                            src="{{ asset('/uploads/inbox/'.$info) }}" width="50px"></a>
                                    @endforeach

                                    @endif
                                </td>
                                <td class=" center_text">{{$item->note}} </td>
                                <td class="center_text">
                                    <div class="btn-group btn-group-sm" style="float: none;">

                                        <button type="button" class="btn btn-sm btn-success" data-toggle="modal"
                                            data-animation="bounce" data-target=".bs-getandbooking"
                                            style="float: none; margin: 4px;" delid=""
                                            onclick="getandbooking({{$item->id}})" {!!$item->status==2?'disabled':''!!}>
                                            {{$item->status==1?'รับงาน (Booking)':$item->editname.' กดรับแล้ว'}}
                                        </button>
                                        <a href="{{url('/admin/delinbox/'.$item->id)}}"><button type="button"
                                                class="btn btn-sm btn-danger" style="float: none; margin: 4px;" delid=""
                                                onclick="return confirm('คุณต้องการลบพัสดุใช่หรือไม่?');">
                                                ลบ</button></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <!--end table-->

            </div>

            <!--end card-body-->
        </div>
        {{ $data->appends(request()->input())->links() }}
        <!--end card-->
    </div>
    <div class="col-12">
    </div>
    <!-- end col -->
</div>



<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" role="dialog" aria-labelledby="bs-example-modal-lg" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="bs-example-modal-lg">Check In</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{url('/admin/checkinbox')}}" method="POST" name="checkinbox" id="checkinbox"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">Tracking *</label>
                                <input type="text" class="form-control" id="tracking_no" name="tracking_no">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">ชื่อผู้ส่ง</label>
                                <input type="text" class="form-control" id="name" name="name" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">ส่งโดย</label>
                                <select name="delivery_by" id="delivery_by" class="form-control">
                                    <option selected="" value="ไม่ระบุ">ไม่ระบุ</option>
                                    <option value="TNT_Express">TNT Express</option>
                                    <option value="BEST_Express">BEST Express</option>
                                    <option value="Laramove">Laramove</option>
                                    <option value="KERRY">KERRY</option>
                                    <option value="Ninja">Ninja</option>
                                    <option value="ไปรษณีย์ไทย">ไปรษณีย์ไทย</option>
                                    <option value="FLASH_Express">FLASH Express</option>
                                    <option value="Nim_Express">NIM Express</option>
                                    <option value="SCG_Express">SCG Express</option>
                                    <option value="J&T_Express">J&T Express</option>
                                    <option value="DHL_Standard">DHL Standard</option>
                                    <option value="Lazada_Mall">Lazada Mall</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="file">แนบรูปถ่าย</label>
                                <input type="file" class="form-control" id="file" accept="image/*" name="files[]"
                                    multiple />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="LeadName">หมายเหตุ</label>
                                <input type="text" class="form-control" id="note" name="note">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    <button type="reset" class="btn btn-sm btn-primary" id="reset" style="display:none">reset</button>
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-hidden="true">
                        Cancel
                    </button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--  Modal content for the above example -->
<div class="modal fade bs-getandbooking" role="dialog" aria-labelledby="bs-example-modal-lg" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="bs-getandbooking">รับงาน กรุณากรอกข้อมูลลูกค้า</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="#" method="GET" name="getinbox" id="getinbox" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>กรุณากรอก ชื่อลูกค้า เบอร์โทร ประเทศปลายทาง</label>
                                <input type="text" class="form-control" id="remark" name="remark" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-sm btn-primary">ยันยันรับงาน</button>
                            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"
                                aria-hidden="true">Cancel</button>
                        </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>

<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script>
    function getandbooking(id){
        $("#getinbox").attr('action', "{{url('/admin/getinbox/')}}"+'/'+id);
    }
</script>
@endsection