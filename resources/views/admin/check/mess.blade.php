@extends('layouts.app')
@section('content')
<style>
    .res {
        color: black;
        font-weight: 600;
    }
    .delete_padding{
        padding:0 0 0 0;
    }

    @media screen and (min-width: 768px) {
        .phoneshow {
            display: none;
        }
    }

    #notification {
        display: none;
    }

    input[type=radio] {
        border: 0px;
        width: 20px;
        height: 20px;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Messenger Check In</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">{{$mode=='check'?'งานที่รอไปรับ':'งานที่รับมาแล้ว กำลังรอ Office Chick-In'}} <button
                    type="button" class="btn btn-outline-info btn-round" onClick="window.location.reload();"><i
                        class="mdi mdi-refresh"></i>
                </button></h4>
            @if($mode=='search')
            <form action="{{url('/admin/messenger_search')}}" method="GET">
                <div class="row">
                    <div class="col-md-3">
                        <select class="custom-select" name="mess">
                            <option value="">เลือกพนักงาน</option>
                            @foreach ($mess as $messitem)
                            <option value="{{$messitem->id}}"
                                {{app('request')->input('mess')==$messitem->id?'selected':''}}>
                                {{$messitem->name}}</option>
                            @endforeach
                        </select>
                        <input type=" text" name="so" class="form-control" placeholder="ค้นหาจากเลข SO"
                            value="{{app('request')->input('so')}}" autocomplete="off"><button type="submit"
                            class="btn  btn-primary"><i class="fas fa-search"></i>ค้นหา</button>


                    </div>
                </div>
            </form>
            @endif
        </div>
    </div>
</div>
<div class="row">

    @foreach ($data as $item)
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="task-box">
                    <div class="task-priority-icon"><i
                            class="fas fa-circle text-{{$mode=='check'?'success':'warning'}}"></i></div>
                    <p class="text-muted float-right">
                        <span class="text-muted">{{$item->delivery_time}}</span>
                        <span class="mx-1">·</span>
                        <span><i class="far fa-fw fa-clock"></i> {{date("d/m/Y",$item->delivery_date)}}</span>
                    </p>
                    <span class="badge badge-danger px-3">{{$item->manifestid}}</span>
                    <h4 class="mt-0" style="text-overflow: ellipsis;
                    white-space: nowrap;
                    overflow: hidden;">{{$item->name}}</h4>
                    <p class="text-muted mb-1"> <i class="mdi mdi-home-account"></i><span style="color:black"> ที่อยู่ :
                            {{$item->address}}</span>
                    </p>
                    @if($item->m_note!=null)
                    <p class="text-muted mb-1"> <i class="mdi mdi-tooltip-text"></i><span style="color:black">
                            คำสั่งพิเศษ :
                            {{$item->m_note}}</span>
                    </p>
                    @endif
                    @if($item->pay!=null)
                    <p class="text-muted mb-1"> <i class="mdi mdi-tooltip-text"></i><span style="color:black">
                            การซำระเงิน: </strong> {{$item->type_pay}} {{$item->pay}} บาท.</span>
                    </p>
                    @endif
                    @if($item->type_shipping!=null)
                    <p class="text-muted mb-1"> <i class="mdi mdi-ballot-outline"></i><span style="color:black">
                            ประเทศปลายทาง :
                            {{$item->type_shipping}}</span>
                    </p>
                    @endif
                    @if($item->type_delivery!=null)
                    <p class="text-muted mb-1"> <i class="mdi mdi-car"></i><span style="color:black">
                            รับโดยใช้ :
                            {{$item->type_delivery}}</span>
                    </p>
                    @endif
                    <i class="far fa-arrow-alt-circle-right" style="
                    color: #1ecab863;
                    font-size: 50px;
                    position: absolute;
                    bottom: 15px;
                    right: 15px;
                " data-toggle="modal" data-animation="bounce"
                        data-target=".bs-example-modal-lg{{$item->manifestid}}"></i>

                </div>
                <!--end task-box-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>

    <!--  Modal content for the above example -->
    <div class="modal fade bs-example-modal-lg{{$item->manifestid}}" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel{{$item->manifestid}}" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">รายละเอียดงาน</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12" id="show_popup">
                        @if($mode=='check')
                        <button type="button" class="btn btn-success waves-effect waves-light"
                            onclick="update_check($(this))">รับงาน</button>
                        <button type="button" class="btn btn-warning waves-effect waves-light"
                            onclick="update_tranfer($(this))">โอน</button>
                        <a href="tel:{{$item->phone}}">
                            <button type="button" class="btn btn-dark waves-effect waves-light">โทร</button>
                        </a>
                        <button type="button" class="btn btn-danger waves-effect waves-light"
                            onclick="update_cancel($(this))">ยกเลิก</button>
                        <div id="cancel_area" style="display:none">
                            <hr />
                            <form action="{{url("/admin/update_cancel_mess/".$item->manifestid)}}" method="get">
                                สาเหตุที่ยกเลิก
                                <input list="select" name="remark" class="form-control" required>
                                <datalist class="form-control" id="select" style="display:none">
                                    <option value="ลูกค้าไม่อยู่" />
                                    <option value="สินค้าไม่พร้อมรับ" />
                                    <option value="เข้ารับไม่ทัน" />
                                </datalist>
                                <button type="submit" class="btn btn-dark btn-lg btn-block">ยืนยันการยกเลิก</button>
                            </form>
                        </div>
                        <div id="change_mess_area" style="display:none">
                            <hr />
                            <form action="{{url("/admin/update_change_mess/".$item->manifestid)}}" method="get">
                                <div class="form-group mb-0">
                                    <label for="exampleInputPassword1">โอนงานให้</label>
                                    <div class="input-group">
                                        <select class="custom-select driver" name="driver" required>
                                            @foreach ($mess as $messitem)
                                            <option value="{{$messitem->id}}" tel="{{$messitem->tel}}">
                                                {{$messitem->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-light" type="button"
                                                onclick="tel($(this))">โทรออก</button>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-dark btn-lg btn-block">ยืนยันการโอนงาน</button>

                                </div>
                            </form>
                        </div>
                        <div id="chin_in_area" style="display:none;">
                            <hr />
                            <input type="text" id="so_{{$item->manifestid}}" class="form-control form-control-success"
                                placeholder="หมายเลข SO" />
                            <hr />
                            <input type="radio" name="idcard_{{$item->manifestid}}" value="มีบัตรประชาชน" required>
                            มีบัตรประชาชน
                            <input type="radio" name="idcard_{{$item->manifestid}}" value="ไม่มีบัตรประชาชน"
                                required />ไม่มีบัตรประชาชน
                            <hr />
                            <div class="table-responsive shopping-cart">
                                <h4 style="color:#2f4686;text-decoration: underline;"> แนบเอกสารการรับงาน </h4>
                                <table class="table mb-0">
                                    <tbody>
                                        @foreach ($item->so_image as $so_item)
                                        <tr>
                                            <td style="padding:5px">
                                                {{$so_item->so_note!=null?$so_item->so_note:'ไม่ได้ระบุข้อมูล'}}
                                            </td>
                                            <td style="padding:5px;width: 100px;">
                                                <a href="{{asset('/uploads/so').'/'.$so_item->so_image}}"
                                                    target="_blank">
                                                    <img src="{{asset('/uploads/so').'/'.$so_item->so_image}}"
                                                        width="50" class="img-thumbnail" /></a>
                                                <button class="btn btn-sm btn-soft-danger"
                                                    onclick="del_so($(this),{{$so_item->id}})"><i
                                                        class="far fa-trash-alt"></i></button>
                                            </td>
                                        </tr>
                                        @endforeach <tr>
                                            <td>
                                                <input type="text" class="form-control so" placeholder="คำอธิบายภาพ" />
                                            </td>
                                            <td>
                                                <label for="file{{$item->manifestid}}"
                                                    class="btn btn-outline-danger waves-effect waves-light"><i
                                                        class="mdi mdi-camera"></i></label>
                                                <input type="file" id="file{{$item->manifestid}}"
                                                    class="form-control photo" multiple style="display:none"
                                                    manifest="{{$item->manifestid}}" onchange="upload_so($(this))" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <button type="button" class="btn btn-dark btn-lg btn-block"
                                onclick="massenger_cf({{$item->manifestid}})">ยืนยันการรับงาน</button>
                        </div>
                        @else
                        <a href="tel:{{$item->phone}}">
                            <button type="button" class="btn btn-dark waves-effect waves-light">โทร</button>
                        </a>
                        <!-- Button Update -->
                        <button type="button" class="btn btn-success waves-effect waves-light"
                            onclick="update_history($(this))">อัพเดท</button>
                        <div id="chin_history" style="display:none;">
                            <hr />
                            <input type="radio" id="type_0{{$item->manifestid}}" name="pack_type" value="0" >
                            <label for="type_0">พัสดุในกล่อง</label>
                            <input type="radio" id="type_1{{$item->manifestid}}" name="pack_type" value="1">
                            <label for="type_1">พัสดุดึงออก</label>
                            <h4 style="color:#2f4686;"> รายละเอียดพัสดุ </h4>
                            <input type="text" id="so_pack{{$item->manifestid}}"
                                class="form-control form-control-success" placeholder="หมายเลข SO" /></br>
                            <textarea type="text" id="note_pack{{$item->manifestid}}"
                                class="form-control form-control-success"
                                style="resize :none;height: 100px;"></textarea></br>
                            <div class="row">
                                <div class="col-3 col-md-2">
                                    <label for="filepack{{$item->manifestid}}"
                                        class="btn btn-outline-danger waves-effect waves-light">
                                        <i class="mdi mdi-camera"></i></label>
                                    <input type="file" id="filepack{{$item->manifestid}}" class="form-control photo"
                                        multiple style="display:none" manifest="{{$item->manifestid}}"
                                        onchange="count_file_pack('{{$item->manifestid}}')" />

                                </div><span id="count-file-pack-{{$item->manifestid}}"
                                    style="color:red;font-size:20px"></span>
                            </div>
                            <button type="button" class="btn btn-dark btn-lg btn-block"
                                onclick="upload_pack('{{$item->manifestid}}')">ยืนยันการอัพเดทงาน</button>
                            <div id="pack_history{{$item->manifestid}}">
                                <!-- แสดงผลการยืนยัน -->
                                @foreach ($item->pack as $pack_item)
                                <div>
                                    <hr />
                                    <h4 style="color:#2f4686;"> รายละเอียดพัสดุ
                                    <button type="button" class="btn btn-sm btn-danger deleteitem"
                                            style="float: right; margin: 4px;"
                                            onclick="del_pack($(this),{{$pack_item->id}})">
                                            <span class="ti-trash"></span></button>
                                        <span style="color:red;">SO : {{$pack_item->ref}}</span>
                                        <div style="padding-top: 6px;">
                                        @if($pack_item->type == 0)
                                        <span style="color:green;">พัสดุในกล่อง</span>
                                        @else
                                        <span style="color:black;">พัสดุดึงออก</span>
                                        @endif
                                        </div>
                                        
                                    </h4>

                                    <div style="font-size: 14px;">Note : <span>{{$pack_item->note}}</span></div></br>
                                    <!-- วนลูปภาพ -->
                                    <div class="row">
                                        @if($pack_item->image!=null)
                                        @foreach(explode(',', $pack_item->image) as $info)
                                        <div class="col-3 col-md-1" style="padding-bottom: 10px;">
                                            <a href="{{ asset('/uploads/pack/'.$info) }}" target="_new">
                                                <img class="img-thumbnail" src="{{ asset('/uploads/pack/'.$info) }}"
                                                    width="50px" style="height:50px;"></a>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                                <!-- end -->
                                @endforeach

                                <div id="show_pack{{$item->manifestid}}"></div>

                                <!-- end -->
                            </div>
                        </div>
                        <!-- end -->
                        @endif
                    </div>

                    <hr />
                    <strong style="color:#900;">เลขที่งาน: </strong> {{$item->manifestid}}<br />
                    <strong style="color:#900;">รหัสลูกค้า: </strong> {{$item->m_id}}<br />
                    <strong style="color:#900;">ชื่อผู้รับ: </strong> {{$item->name}} <br />
                    <strong style="color:#900;">ที่อยู่เข้ารับ: </strong> {{$item->address}}<br />
                    <strong style="color:#900;">วันที่รับ: </strong> {{date("d/m/Y",$item->delivery_date)}}<br />
                    <strong style="color:#900;">เวลารับ: </strong> {{$item->delivery_time}}<br />
                    <strong style="color:#900;">เบอร์ติดต่อทั้งหมด: </strong>
                    @foreach ($item->contact as $item_phone)
                    <a href="tel:{{$item_phone->phone}}">{{$item_phone->phone}}</a>,
                    @endforeach
                    <br />
                    <strong style="color:#900;">Email:</strong> {{$item->email}}<br />
                    <strong style="color:#900;">ประเภทงาน: </strong>
                    {{$item->type_work==1?'งานส่งออก':''}}{{$item->type_work==2?'งานบริการลูกค้า':''}}{{$item->type_work==3?'งานภายใน':''}}<br />
                    <strong style="color:#900;">Service: </strong> {{$item->type_of_service_name}}<br />
                    <strong style="color:#900;">ประเทศปลายทาง: </strong> {{$item->type_shipping}}<br />
                    <strong style="color:#900;">คำสั่งพิเศษ: </strong> {{$item->m_note}}<br />
                    <strong style="color:#900;">การซำระเงิน: </strong> {{$item->type_pay}} {{$item->pay}} บาท.<br />
                    <strong style="color:#900;">Sales: </strong> {{$item->sale_name}}<i class="fa fa-phone"></i><a
                        href="tel:{{$item->sale_tel}}">โทรหาเชลล์</a><br />
                    <strong style="color:#900;">วันที่ลงในระบบ: </strong> {{$item->created_at}}
                    <br />
                    <hr />

                    <a
                        href="http://line.me/R/msg/text/มีงานใหม่รหัส {{$item->manifestid}} ชื่อ {{$item->name}}  ที่อยู่ {{$item->address}} เบอร์โทร {{$item->phone}}  {{url('/admin/request/'.$item->id)}}">
                        <button type="button" class="btn btn-light btn-lg btn-block" style="background: white;"><i
                                class="fab fa-line" style="
                                color: #12a712;
                                font-size: 22px;
                            "> แอปพลิเคชัน</i> </button>
                    </a>

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endforeach

    <!-- end col -->

    <div class="row" style="overflow-x: auto; width: 1603px;">
        <div class="col-md-12">@if($mode=='search') {{ $data->appends(request()->input())->links() }} @endif</div>
    </div>
</div>
<script src="{{ asset('assets/js/jquery.min.js' )}}">
</script>
<script>
    function preview(manifestid,shipcode){
        window.open('{{url("/admin/request/")}}'+'/'+manifestid+'?find='+shipcode, '_blank');
    }
    $('#findCode').submit(function(e){
        e.preventDefault();
        $("#loading").show();
        var url = $('#findCode').attr("action");
        url = url+'/'+$('#code').val();
        $.ajax({
            type: "GET",
            url: url,
            success: function (result) {
                $("#loading").hide();
                $('#show_preview').show();
                $('#show_start').hide();
                var data = result.shipment[0];
                var box = result.box;
                console.log(data);
                $('#show_recipient').html(data.recipient);
                $('#show_address').html(data.address+' '+data.address2+' '+data.address3 + ' ' + data.country +' ' +data.zipcode);
                $('#show_service').html(data.service_name);
                $('#show_currency').html(data.currency);
                $('#show_desc').html(data.description);
                $('#show_value').html(data.declared_value);
                $('#show_tracking').html(smecode(data.tracking_code));
                $('#show_box').html('');
                var _box_ = 0;
                $(box).each(function(i,e){
                    $('#show_box').prepend('<div class="col-lg-4">'+
                                                '<div class="pro-order-box mb-2 mb-lg-0">'+
                                                    '<i class="dripicons-box"></i>'+e.series+
                                                    '<h4 class="header-title">Weight : '+e.weight+', Width : '+e.width+', Length : '+e.length+', Height : '+e.height+'</h4>'+
                                                    '<p class="text-muted mb-0">'+
                                                    (e.status==1?' <span class="badge badge-soft-success">รับเข้าระบบแล้ว</span>':'<span class="badge badge-soft-danger">รอรับเข้าระบบ</span>')+
                                                    '</p>'+
                                                '</div>'+
                                            '</div>');
                    if(e.status==0){
                        _box_ = _box_+1;
                    }
                    $('#count_box').html(_box_);
                    if(_box_==0){
                        $('#successful').show();
                        $('#warning').hide();
                    }
                    else{
                        $('#successful').hide();
                        $('#warning').show();
                    }
                   
                });
                 $('.shid_'+data.tracking_code).first().remove();
                 var table_tr = $('#table_all tbody tr').length;
                 $('#count_all_').html(table_tr);
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
        $('#code').val('');
    });
    function smecode(code) {
        var result = code;
        if (code.length == 6) {
            result = code.substr(0, 2) + ' ' + code.substr(2, 2) + ' ' + code.substr(4, 2);
        }
    return result;
    }
    function update_cancel(obj){
        $(obj).parent().find('#cancel_area').slideToggle('slow');
    }
    function update_tranfer(obj){
        $(obj).parent().find('#change_mess_area').slideToggle('slow');
    }
    function update_check(obj){
        $(obj).parent().find('#chin_in_area').slideToggle('slow');
    }
    function update_history(obj){
        $(obj).parent().find('#chin_history').slideToggle('slow');
    }
    function tel(obj){
        var number = $(obj).parent().parent().find('.driver option:selected').attr('tel');
        console.log(number);
        //window.open('tel:'+number);
        window.location.href="tel://"+number;
    }
    function check_in(obj){
        var tracking_code = $(obj).parent().parent().find('.tracking').val();
        var check = $(obj).parent().parent().parent();
        var checkbox = $(check).find('.'+tracking_code).not(":checked");
        if(checkbox.length == 0){
            alert('ไม่มีพัสดุหมายเลข : '+tracking_code+' ที่รอการ Check-in กรุณาตรวจสอบแล้วลองอีกครั้ง');
        }
        else{
            var boxid = $(checkbox).eq(0).val();

            $.ajax({
            type: "GET",
            url: "{{url('/admin/mess_check_in_by_box_id')}}/"+boxid,
            success: function (result) {
                $(checkbox).eq(0).prop('checked',true);
                $(checkbox).eq(0).prop('disabled',true);
                //$(checkbox).prop('checked',true);
                //console.log($(checkbox).not(":checked"));
                console.log(result);
                $(obj).parent().parent().find('.tracking').val('');
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
        }
        
    }
    function check_in_change(obj,boxid){    
        $("#loading").show();    
            $.ajax({
            type: "GET",
            url: "{{url('/admin/mess_check_in_by_box_id')}}/"+boxid,
            success: function (result) {
                $(obj).prop('checked',true);
                $(obj).prop('disabled',true);
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $(obj).prop('checked',false);
                $(obj).prop('disabled',false);
            }
        });
        $("#loading").hide();
    }
    function upload_so(obj){
        $("#loading").show();
        var manifestid = $(obj).attr('manifest');
        var so = $(obj).parent().parent().find('.so').val();

        var form_data = new FormData();
        // Read selected files
        var totalfiles = document.getElementById('file'+manifestid).files.length;
        for (var index = 0; index < totalfiles; index++) {
            form_data.append("files[]", document.getElementById('file'+manifestid).files[index]);
        }
        //form_data.append("file", $('#file'+manifestid)[0].files[0]);
        form_data.append("so", so);
        console.log(form_data);
        console.log(so);
        console.log(manifestid);
       $.ajax({
            type: "POST",
            data: form_data,
            url: '/upload_so/' + manifestid,
            processData: false,
            contentType: false,
            success: function (result) {
                console.log(result);
                var data = JSON.parse(result);
                console.log(data);
                var str = '';
                $(data).each(function(i,e){
                    str += '<tr>'+
                    '<td style="padding:5px">'+(e.so_note!=null?e.so_note:'ไม่ได้ระบุข้อมูล')+'</td>'+
                    '<td style="padding:5px;width:100px;">'+
                    '<a href="{{asset("/uploads/so")."/"}}'+e.so_image+'" target="_blank"><img src="{{asset("/uploads/so")."/"}}'+e.so_image+'" width="50" class="img-thumbnail"></a>'+
                    '<button class="btn btn-sm btn-soft-danger" onclick="del_so($(this),'+e.id+')"><i class="far fa-trash-alt"></i></button></td></tr>';
                });
                $(obj).parent().parent().find('.tracking').val('');
                    var tbody = $(obj).parent().parent().parent().parent().find('tbody tr');
                    tbody.eq(tbody.length-1).before(str);
                $("#loading").hide();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
        
    }

    // เก็บรูปของ history
    function upload_pack(id){
        $('#count-file-pack-'+id).html("");
        $("#loading").show();
        var manifestid = id;
        var so = $('#so_pack'+ manifestid).val();
        var note = $('#note_pack'+ manifestid).val();
        var type_ = $("#type_0"+manifestid).is(':checked') == true?$("#type_0"+manifestid).val():$("#type_1"+manifestid).val();
        var form_data = new FormData();
        // Read selected files
        var totalfiles = document.getElementById('filepack'+manifestid).files.length;
        for (var index = 0; index < totalfiles; index++) {
            form_data.append("files[]", document.getElementById('filepack'+manifestid).files[index]);
        }
        //form_data.append("file", $('#file'+manifestid)[0].files[0]);
        form_data.append("so", so);
        form_data.append("note", note);
        form_data.append("type", type_);
        console.log(form_data);
        console.log(so);
        console.log(note);
        console.log(manifestid);
        console.log(type_);
       $.ajax({
            type: "POST",
            data: form_data,
            url: '/upload_pack/' + manifestid,
            processData: false,
            contentType: false,
            success: function (result) {
                console.log(result);
                var data = JSON.parse(result);
                console.log(data);
                var str = '';
                var type_name = type_ == 0 ? '<span style="color:green;">พัสดุในกล่อง</span>':'<span style="color:black;">พัสดุดึงออก</span>';
                $(data).each(function(i,e){ 
                    var imagearray = e.image.split(",");
                    str += '<div>'+
                    '<hr />'+
                    '<h4 style="color:#2f4686;"> รายละเอียดพัสดุ <span style="color:red;">SO : '+ so +'</span>'+
                    '<button type="button" class="btn btn-sm btn-danger deleteitem"'+
                    'style="float: right; margin: 4px;"  onclick="del_pack($(this),'+e.id+')">'+
                    '<span class="ti-trash"></span></button>'+
                    '<div style="padding-top: 6px;">' +type_name+ '</div>'+
                    '</h4>'+
                    '<div style="font-size: 14px;">Note : <span>'+e.note+'</span></div></br>'+
                    '<div class="row">';
                    $(imagearray).each(function(ii,ee){
                        str+=
                        '<div class="col-3 col-md-1"  style="padding-bottom: 10px;">'+
                        '<a href="{{asset("/uploads/pack")."/"}}'+ee+'" target="_blank">'+
                        '<img src="{{asset("/uploads/pack")."/"}}'+ee+'"width="50px" style="height:50px;" class="img-thumbnail"></a></div>';
                    });
                    str+='</div></div>';
                    $("#show_pack"+manifestid).append(str);
                });
                $("#loading").hide();
                $('#so_pack'+ manifestid).val("");
                $('#note_pack'+ manifestid).val("");

            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
        
    }
    // end

    function del_so(obj,id){
        $("#loading").show();
        console.log();
        //$(this).parent().parent().parent().html();
        if (confirm('คุณแน่ใจที่จะลบข้อมูลนี้')) {
            $("#loading").show();
            $.ajax({
            type: "GET",
            url: '/del_so/' + id,
            success: function (result) {
                obj.parent().parent().remove();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
            }
            });
        } else {
            // Do nothing!
        }
        $("#loading").hide();
    }

    // del_pack
    function del_pack(obj,id){
        debugger;
        $("#loading").show();
        console.log();
        //$(this).parent().parent().parent().html();
        if (confirm('คุณแน่ใจที่จะลบข้อมูลนี้')) {
            $("#loading").show();
            $.ajax({
            type: "GET",
            url: '/del_pack/' + id,
            success: function (result) {
                obj.parent().parent().remove();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
            }
            });
        } else {
            // Do nothing!
        }
        $("#loading").hide();
    }
    // end
    function massenger_cf(id){
        var idcard = $('input[name=idcard_'+id+']:checked').val();
        var so = $('#so_'+id).val();
        if (confirm('กรุณาเช็คให้แน่ใจว่าคุณได้เขียนหมายเลข SO ข้างกล่องพัสดุ, ยืนยันการรับงาน?')) {
            $("#loading").show();
            $.ajax({
            type: "PATCH",
            data: { 'so': so, 'idcard': idcard },
            url: '{{url('/admin/request/')}}/' + id,
            success: function (result) {
                location.reload();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
            }
            });
            $("#loading").hide();
        } else {
            $("#loading").hide();
        }
    }

    function count_file_pack(id){
        var totalfiles = document.getElementById('filepack'+id).files.length;
        $('#count-file-pack-'+id).html("เลือกรูป "+totalfiles+" รูป")
    }
<?php 
if(isset($_GET["code"])){
    echo "$('#findCode').submit();";
}
?>
</script>
@endsection