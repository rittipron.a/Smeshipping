@extends('layouts.app')

@section('content')
<style>
    .res {
        color: black;
        font-weight: 600;
    }

    @media screen and (min-width: 768px) {
        .mobileshow {
            display: none;
        }
    }

    #notification {
        display: none;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Check In</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Check In</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-9">
                        <div class="card bg-newsletters">
                            <div class="card-body">
                                <div class="row">
                                    <a href="{{url('assets/readQRcode')}}" class="mobileshow" style="font-size:50px;position: absolute;
                                                top: 0;right: 5px;color:white;z-index:9999">
                                        <i class="mdi mdi-qrcode-scan"></i>
                                    </a>
                                    <div class="col-md-6">
                                        <div class="newsletters-text">
                                            <h4>Check In &amp; รับเข้า SME Shipping</h4>


                                            <p class="text-muted mb-0">กรุณากรอกหมายเลขข้างกล่อง 6 หลักแล้วกด Confirm
                                                หรือ ใช้มือถืออ่าน QR Code
                                            </p>
                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-md-6 align-self-center">
                                        <div class="newsletters-input">
                                            <form class="position-relative" name="findCode" id="findCode"
                                                action="{{url('/admin/getDetailByShipmentCode/')}}">
                                                <input type="number" id="code" placeholder="Enter Code" required=""
                                                    value="{{ app('request')->input('code') }}">
                                                <button type="submit" id="confirm"
                                                    class="btn btn-success">Confirm</button>
                                            </form>
                                        </div>
                                    </div>

                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                        <div class="card" id="show_start">
                            <div class="card-body">
                                <div class="row">

                                </div>
                            </div>
                        </div>
                        <div class="card" id="show_preview" style="display:none">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-3 align-self-center">
                                        <a href="#"
                                            class="btn btn-block  btn-secondary btn-square btn-skew btn-outline-dashed mt-3 py-3 font-18">รายละเอียดของ
                                            : </strong> <span id="show_tracking"></a> <br />
                                        <img src="{{ asset('/assets/images/17198.jpg') }}" alt="" height="250"
                                            class="d-block mx-auto">
                                        <div class="alert icon-custom-alert alert-outline-success alert-success-shadow"
                                            role="alert" id="successful">
                                            <i class="mdi mdi-check-all alert-icon"></i>
                                            <div class="alert-text">
                                                รับเข้าระบบครบเรียบร้อย.
                                            </div>
                                        </div>
                                        <div class="alert icon-custom-alert alert-outline-warning alert-warning-shadow"
                                            role="alert" id="warning">
                                            <i class="mdi mdi-alert-outline alert-icon"></i>
                                            <div class="alert-text">
                                                Shipment นี้รอรับเข้าระบบอีก <span id="count_box">4</span> กล่อง
                                            </div>
                                        </div>

                                        <!--<span class="bg-soft-danger rounded-pill px-3 py-1 font-weight-bold">ยกเลิกการรับเข้า</span> -->
                                    </div>
                                    <div class="col-lg-9">

                                        <h5 class="mt-3" id="show_recipient"> </h5>
                                        <p class="text-muted mb-4" id="show_address"> </p>
                                        <ul class="list-unstyled mb-4">
                                            <li class="mb-2 font-13 text-muted"><i
                                                    class="mdi mdi-checkbox-marked-circle-outline text-success mr-2"></i>service_name
                                                : <span id="show_service" class="res"></span></li>
                                            <li class="mb-2 font-13 text-muted"><i
                                                    class="mdi mdi-checkbox-marked-circle-outline text-success mr-2"></i>Currency
                                                : <span id="show_currency" class="res"></span></li>
                                            <li class="mb-2 font-13 text-muted"><i
                                                    class="mdi mdi-checkbox-marked-circle-outline text-success mr-2"></i>Description
                                                : <span id="show_desc" class="res"></span></li>
                                            <li class="mb-2 font-13 text-muted"><i
                                                    class="mdi mdi-checkbox-marked-circle-outline text-success mr-2"></i>Total
                                                Declared Value : <span id="show_value" class="res"></span>
                                            </li>
                                        </ul>
                                        <hr />
                                        <div class="row" id="show_box">

                                        </div>
                                        <!--end row-->
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->

                    </div>

                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="review-box text-center align-item-center">
                                    <h1 id="count_all_">{{count($data)}}</h1>
                                    <h4 class="header-title">ที่รอการรับเข้า</h4>
                                    <table class="table table-dark mb-0" id="table_all">
                                        <thead class="bg-dark">
                                            <tr>
                                                <th>Manifast ID</th>
                                                <th>Shipping Code</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($data as $item)
                                            <tr class="shid_{{$item->tracking_code}}"
                                                onclick="preview({{$item->manifestid}},{{$item->tracking_code}})">
                                                <td>{{$item->manifestid}}</td>
                                                <td>{{strlen($item->tracking_code)==7?substr($item->tracking_code,0,2).' '.substr($item->tracking_code,2,2).' '.substr($item->tracking_code,4,3):$item->tracking_code}}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <!--end card-body-->
                        </div>
                        <!--end card-->
                    </div>
                </div>
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>

    <!-- end col -->
</div>
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script>
    function preview(manifestid,shipcode){
        window.open('{{url("/admin/request/")}}'+'/'+manifestid+'?find='+shipcode, '_blank');
    }
    $('#findCode').submit(function(e){
        e.preventDefault();
        $("#loading").show();
        var url = $('#findCode').attr("action");
        url = url+'/'+$('#code').val();
        $.ajax({
            type: "GET",
            url: url,
            success: function (result) {
                $("#loading").hide();
                $('#show_preview').show();
                $('#show_start').hide();
                var data = result.shipment[0];
                var box = result.box;
                console.log(data);
                $('#show_recipient').html(data.recipient);
                $('#show_address').html(data.address+' '+data.address2+' '+data.address3 + ' ' + data.country +' ' +data.zipcode);
                $('#show_service').html(data.service_name);
                $('#show_currency').html(data.currency);
                $('#show_desc').html(data.description);
                $('#show_value').html(data.declared_value);
                $('#show_tracking').html(smecode(data.tracking_code));
                $('#show_box').html('');
                var _box_ = 0;
                $(box).each(function(i,e){
                    $('#show_box').prepend('<div class="col-lg-4">'+
                                                '<div class="pro-order-box mb-2 mb-lg-0">'+
                                                    '<i class="dripicons-box"></i>'+e.series+
                                                    '<h4 class="header-title">Weight : '+e.weight+', Width : '+e.width+', Length : '+e.length+', Height : '+e.height+'</h4>'+
                                                    '<p class="text-muted mb-0">'+
                                                    (e.status>=2?' <span class="badge badge-soft-success">รับเข้าระบบแล้ว</span>':'<span class="badge badge-soft-danger">รอรับเข้าระบบ</span>')+
                                                    '</p>'+
                                                '</div>'+
                                            '</div>');
                    if(e.status==0){
                        _box_ = _box_+1;
                    }
                    $('#count_box').html(_box_);
                    if(_box_==0){
                        $('#successful').show();
                        $('#warning').hide();
                    }
                    else{
                        $('#successful').hide();
                        $('#warning').show();
                    }
                   
                });
                 $('.shid_'+data.tracking_code).first().remove();
                 var table_tr = $('#table_all tbody tr').length;
                 $('#count_all_').html(table_tr);
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
        $('#code').val('');
    });
    function smecode(code) {
    var result = code;
    if (code.length == 6) {
        result = code.substr(0, 2) + ' ' + code.substr(2, 2) + ' ' + code.substr(4, 2);
    }
    return result;
}
<?php 
if(isset($_GET["code"])){
    echo "$('#findCode').submit();";
}
?>
</script>
@endsection