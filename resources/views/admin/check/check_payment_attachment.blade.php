@extends('layouts.app')

@section('content')
<style>
    .center_text {
        text-align: center;
    }

    .space_top {
        margin-top: 7px;
    }

    .disa {
        background: #f3f3f391;
    }

    #notification {
        display: none;
    }
</style>
<div class="row" onload="startTime()">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Attachment</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title"> Attachment</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <form action="{{url('admin/check_payment_attachment')}}" method="GET">
                            <div class="input-group">
                                <input type="text" class="form-control" name="keyword"
                                    placeholder="ค้นหาจากเบอร์โทร หรือ หมายเลข SO"
                                    value="{{app('request')->input('keyword')}}" aria-label="ค้นหา">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="submit">ค้นหา</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table mb-0" id="my-table">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ชื่อ</th>
                                <th>วันที่</th>
                                <th>เบอร์โทร</th>
                                <th>หมายเลข SO</th>
                                <th class="center_text">รูปถ่าย</th>
                                <th>Action</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index=>$item)
                            <tr class="{{$item->update_by!=null?'disa':''}}" onclick="">
                                <td>{{ ($data->currentpage()-1) * $data->perpage() + $index + 1 }}</td>
                                <td>{{$item->customer_name}}</td>
                                <td>{{date('d/m/Y',strtotime($item->created_at))}}</td>
                                <td>{{$item->tel}}</td>
                                <td>{{$item->so}}</td>
                                <td class="center_text">
                                    @if($item->filename!=null)
                                    @foreach(explode(',', $item->filename) as $info)
                                    <a href="{{ asset('/uploads/so/'.$info) }}" target="_new"> <img
                                            src="{{ asset('/uploads/so/'.$info) }}" width="50px"></a>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    <div class="custom-control custom-switch switch-success">
                                        <input type="checkbox" class="custom-control-input"
                                            id="customSwitchSuccess{{$item->id}}" name="active-{{$item->id}}"
                                            {!!$item->update_by!=null?'checked':''!!} onchange="switch_save('{{$item->id}}')">
                                        <label class="custom-control-label"
                                            for="customSwitchSuccess{{$item->id}}">{{$item->update_by!=null?'แนบแล้ว':'ยังไม่แนบ'}}</label>
                                    </div>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-success waves-effect waves-light"
                                        data-toggle="modal" data-animation="bounce" data-target=".view"
                                        onclick="view('{{$item->customer_name}}','{{$item->tel}}','{{$item->so}}','{{$item->id}}')">view</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end table-->

            </div>

            <!--end card-body-->
        </div>
        {{ $data->appends(request()->input())->links() }}
        <!--end card-->
    </div>
    <div class="col-12">
    </div>
    <!-- end col -->
</div>

<!--  Modal content for the above example -->
<div class="modal fade view" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">View</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">                    
                    <div class="row" id="map_area">

                    </div>
                    <button type="button" class="btn btn-sm btn-danger" id="close_modal_discount" data-dismiss="modal"
                        aria-hidden="true">ปิด</button>
               
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script>
    function view(name,tel,so,id) {
        $('#map_area').html('<p style="text-align: center; width: 100%;">Loading....</p>');
        $('#loading').show();
        var form_data = new FormData();
        form_data.append("name", name);
        form_data.append("tel", tel);
        form_data.append("so", so);
        $('#att_id').val(id);
        $.ajax({
            type: "POST",
            data: form_data,
            url: '/admin/check_payment_attachment_search',
            processData: false,
            contentType: false,
            success: function (result) {
                result = JSON.parse(result);
                if($(result.data).length==0){
                    $('#map_area').html('<p style="text-align: center; width: 100%;">ไม่พบข้อมูลที่ตรงกัน</p>');
                }else{
                    var str = `<div class="table-responsive">
                                        <table class="table mb-0">
                                            <thead class="thead-light">
                                            <tr>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>SO</th>
                                                <th>Tel</th>
                                                <th>Link</th>
                                            </tr>
                                            </thead>
                                            <tbody>`;
                        $(result.data).each(function(i,e){
                            str+=`          <tr>
                                                <td>`+e.name+`</td>
                                                <td>`+e.created_at+`</td>
                                                <td>`+e.so+`</td>
                                                <td>`+e.phone+`</td>
                                                <td><a href="/admin/request/`+e.manifestid+`" target="_blank"><button type="button" class="btn btn-secondary btn-xs">Open new teb</button></a></td>
                                            </tr>`;
                        });                    
                        str+=`                    </tbody>
                                        </table>
                                    </div>`;
                    $('#map_area').html(str);
                }
                $("#loading").hide();
            },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
    }

    function switch_save(e){
        $('#loading').show();
        var form_data = new FormData();
        form_data.append("att_id", e);
        $.ajax({
            type: "POST",
            data: form_data,
            url: '{{url('/admin/check_payment_attachment_save')}}',
            processData: false,
            contentType: false,
            success: function (result) {
                location.reload();
             },
            error: function (e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                location.reload();
                $("#loading").hide();
            }
        });
    }
</script>
@endsection