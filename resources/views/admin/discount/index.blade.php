@extends('layouts.app')

@section('content')
<link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/select2/select2.min.css" rel="stylesheet' )}}" type="text/css" />
<link href="{{ asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css' )}}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
    rel="stylesheet" />
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Discount</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Discount</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <button type="button" class="btn btn-primary waves-effect waves-light float-left mb-3"
                    data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg" id="add">+
                    สร้างรหัสส่วนลดใหม่</button>
                <div class="table-responsive">
                    <table class="table mb-0" id="my-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Amount</th>
                                <th>Ignore Weight</th>
                                <th>Expired Date </th>
                                <th>Description</th>
                                <th>Active</th>
                                <th class="tabledit-toolbar-column"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($discount as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->discount_code }}</td>
                                <td>{{$item->price!=null?$item->price.'฿':$item->percent.'%' }}</td>
                                <td>{{$item->ignore_weight}} KG.</td>
                                <td>{{$item->expired_date}}</td>
                                <td>{{$item->description}}</td>
                                <td>{{$item->active==1?'เปิดใช้งานอยู่':'ปิดใช้งานไปแล้ว'}}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" style="float: none;">
                                        <button type="button" class="btn btn-sm btn-info"
                                            style="float: none; margin: 4px;" data-toggle="modal"
                                            data-animation="bounce" data-target=".bs-example-modal-lg"
                                            onclick="editmode('{{json_encode($item)}}')">
                                            <span class="ti-pencil"></span></button>
                                        <button type="button" class="btn btn-sm btn-danger deleteitem"
                                            style="float: none; margin: 4px;" delid="{{$item->id}}">
                                            <span class="ti-trash"></span></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!-- end col -->
</div>





<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Discount</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{url('/admin/discount')}}" name="Discount_form" id="Discount_form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="LeadEmail">รายละเอียดโค้ดส่วนลดนี้ </label>
                                <input type="text" class="form-control" id="description" name="description" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">รหัสส่วนลด /Discount Code </label>
                                <input type="text" class="form-control" id="discount_code" name="discount_code"
                                    required="">
                                <input type="hidden" class="form-control" id="id" name="id">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">จำนวน </label>
                                <div style="position: absolute;top: 38px;right: 20px;">
                                    <input type="radio" name="type" value="price" checked> บาท (THB)
                                    <input type="radio" name="type" value="percent"> เปอร์เซ็น (%)
                                </div>
                                <input type="number" class="form-control" id="number" name="number" required="">

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">น้ำหนักรวมไม่เกิน (KG.)</label>
                                <input type="text" class="form-control" id="ignore_weight" name="ignore_weight">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">จำกัดจำนวนครั้ง (ปล่อยว่างไว้ กรุณีไม่จำกัดจำนวนการใช้)</label>
                                <input type="number" class="form-control" id="used_limit" name="used_limit">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">กลุ่มบริการที่ใช้โค้ดนี้ได้</label>
                                <div class="row">
                                    @foreach ($serviceGroup as $sgitem)
                                    <div class="col-md-6"><input type="checkbox" checked name="focus_service_group_id[]"
                                            value="{{$sgitem->id}}" /> {{$sgitem->name}}</div>
                                    @endforeach


                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">การกำหนดประเทศได้ด้วยรหัสประเทศ 2 หลัก </label>
                                <textarea class="form-control" id="focus_country_code" name="focus_country_code"
                                    placeholder="เช่น US, CN, JP"></textarea>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">วันที่เริ่มใช้โค้ดส่วนลด</label>
                                <input type="text" class="form-control" name="effective_date" required="" id="mdate"
                                    value="{{date('d/m/Y')}}" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">วันที่สุดท้ายที่ใช้โค้ดส่วนลดได้</label>
                                <input type="text" class="form-control" name="expired_date" required="" id="date-end"
                                    value="{{date('d/m/Y')}}" />
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">บันทึก</button>
                    <button type="reset" class="btn btn-sm btn-primary" id="reset" style="display:none">reset</button>
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-hidden="true">
                        ปิด</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}"></script>
<script src="{{ asset('assets/pages/jquery.forms-advanced.js' )}}"></script>
<script>
    function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 &&
        (charCode < 48 || charCode > 57))
        return false;
    return true;
}

    $('#Discount_form').submit(function(e){
        e.preventDefault();
            var thisform = $(this);
            var url = thisform.attr('action');
            var method = 'POST';
            if($('#id').val()!=''){
                url = url+'/'+$('#id').val();
                method = 'PUT'
            }

            $('#loading').show();
            $.ajax({
                type: method,
                data: $('#Discount_form').serialize(), 
                url: url,
                success: function (result) {
                    $('#loading').hide();
                    window.location.reload(true);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
    });
    function editmode(obj){
        obj = JSON.parse(obj);
        $('#id').val(obj.id);
        $('#discount_code').val(obj.discount_code);
        $('#description').val(obj.description);
        if(obj.price>1){
            $(":radio[value=price]").prop("checked","true");
            $('#number').val(obj.price);
        }
        else{
            $(":radio[value=percent]").prop("checked","true");
            $('#number').val(obj.percent);
        }

        $('#ignore_weight').val(obj.ignore_weight);
        $('#used_limit').val(obj.used_limit);

        if(obj.focus_service_group_id!=null){
            var datalist = obj.focus_service_group_id.split(",");
            $("input:checkbox").prop("checked", false);
            $(datalist).each(function(i,e){
                $(":checkbox[value="+e+"]").prop("checked","true");
            });
        }
        
        
        $('#focus_country_code').val(obj.focus_country_code);
        $('#mdate').val(obj.effective_date);
        $('#date-end').val(obj.expired_date);
    }
    $('.deleteitem').click(function(e){
        var x = confirm("Are you sure you want to delete?");
        if (x){
        var id = $(this).attr('delid');
        var url = $('#Discount_form').attr('action');
        url = url+'/'+id;
        $('#loading').show();
            $.ajax({
                type: 'DELETE',
                url: url,
                success: function (result) {
                    $('#loading').hide();
                    window.location.reload(true);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
            }
        else
            return false;
        
    });
    $('#add').click(function(){
        $('#reset').click();
        $('#id').val('');
    });
</script>

@endsection