@extends('layouts.app')

@section('content')
<style>
    .center {
        text-align: center;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Employee</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Employee</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">Edit Table With Button</h4>
                <p class="text-muted mb-3">Add toolbar column with edit and delete buttons.</p>
                <div class="row">
                    <div class="col-1"><button type="button"
                            class="btn btn-primary waves-effect waves-light float-left mb-3" data-toggle="modal"
                            data-animation="bounce" data-target=".bs-example-modal-lg">+ Add New</button>
                    </div>
                    <form action="{{url('/admin/employee/')}}" method="GET">
                        <div class="col-3">
                            <div class="input-group" style="width:500px;">

                                <input type="text" id="search_customer" name="keyword" class="form-control"
                                    placeholder="ค้นหาจาก E-mail, ชื่อ, ตำแหน่ง"
                                    value="{{ app('request')->input('keyword') }}" autocomplete="off">
                                <span class="input-group-append">
                                    <button type="submit" class="btn  btn-primary"><i
                                            class="fas fa-search"></i>Search</button>
                                </span>

                            </div>
                        </div>
                    </form>
                </div>


                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Position</th>
                                <th class="center">จัดการราคา&บริการ</th>
                                <th class="center">จัดการลูกค้า</th>
                                <th class="center">จัดการพนักงาน</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($employee as $index=>$item)
                            <tr id="{{$index+1}}">
                                <td>{{$index+1}}</td>
                                <td id="name_{{$item->id}}">{{$item->name}}</td>
                                <td id="email_{{$item->id}}">{{$item->email}}</td>
                                <td id="tel_{{$item->id}}">{{$item->tel}}</td>
                                <td id="position_{{$item->id}}">{{$item->position}}</td>
                                <td class="center">
                                    <input type="checkbox" id="role_service_{{$item->id}}" onclick="return false;"
                                        {{$item->role_service==1?'checked':''}}>
                                </td>
                                <td class="center">
                                    <input type="checkbox" id="role_customer_{{$item->id}}" onclick="return false;"
                                        {{$item->role_customer==1?'checked':''}}>
                                </td>
                                <td class="center">
                                    <input type="checkbox" id="role_employee_{{$item->id}}" onclick="return false;"
                                        {{$item->role_employee==1?'checked':''}}>
                                </td>
                                <td>
                                    <div class="btn-group btn-group-sm" style="float: none;">
                                        <button type="button" class="btn btn-sm btn-info"
                                            style="float: none; margin: 4px;" data-toggle="modal"
                                            data-animation="bounce" data-target=".edit_employee_modal"
                                            onclick="editmode({{$item->id}})">
                                            <span class="ti-pencil"></span></button>
                                        <button type="button" class="btn btn-sm btn-danger"
                                            style="float: none; margin: 4px;">
                                            <span class="ti-trash"></span></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <div class="col-12">
        {{ $employee->links() }}
    </div>
    <!-- end col -->
</div>



<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{url('/admin/employee/')}}" method="POST" name="create_employee" id="create_employee">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">Name</label>
                                <input type="text" class="form-control" id="LeadName" name="name" required=""
                                    placeholder="ชื่อที่จะแสดงในระบบ">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">Email</label>
                                <input type="text" class="form-control" id="LeadEmail" name="email" required=""
                                    placeholder="อีเมล์สำหรับใช้เข้าสู่ระบบ">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">Phone</label>
                                <input type="text" class="form-control" id="LeadTel" name="tel" required=""
                                    placeholder="เบอร์มือถือ">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Position">Position</label>
                                <select name="position" id="position" class="form-control">
                                    <option selected="" value="Administrator">Administrator</option>
                                    <option value="Booking Agent">Booking Agent</option>
                                    <option value="Sale">Sale</option>
                                    <option value="Marketing">Marketing</option>
                                    <option value="Messenger">Messenger</option>
                                    <option value="Supervisor Messenger">Supervisor Messenger</option>
                                    <option value="Accountant">Accountant</option>
                                    <option value="Operation">Operation</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="status-select" class="mr-2">Password</label>
                                <input type="password" name="password" value="" class="form-control"
                                    placeholder="รหัสสำหรับใช้เข้าสู่ระบบ" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="checkbox" id="add_role_service" name="role_service">
                                <label for="status-select" class="mr-2">จัดการราคาและบริการ</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="checkbox" id="add_role_customer" name="role_customer">
                                <label for="status-select" class="mr-2">จัดการลูกค้า</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="checkbox" id="add_role_employee" name="role_employee">
                                <label for="status-select" class="mr-2">จัดการพนักงาน</label>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="status-select" class="mr-2">Active</label>
                                <div class="custom-control custom-switch switch-success">
                                    <input type="checkbox" class="custom-control-input" id="active" name="active"
                                        checked>
                                    <label class="custom-control-label" for="active"></label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-hidden="true">
                        Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  Modal content for the above example -->
<div class="modal fade edit_employee_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{url('/admin/employee/')}}" method="POST" name="edit_employee" id="edit_employee">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">Name</label>
                                <input type="text" class="form-control" id="edit_name" name="name" required="">
                                <input type="hidden" class="form-control" id="edit_id" name="id" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">Email</label>
                                <input type="text" class="form-control" id="edit_email" name="email" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadTel">Phone</label>
                                <input type="text" class="form-control" id="edit_tel" name="tel" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Position">Position</label>
                                <select name="position" id="edit_position" class="form-control">
                                    <option value="Administrator">Administrator</option>
                                    <option value="Booking Agent">Booking Agent</option>
                                    <option value="Sale">Sale</option>
                                    <option value="Marketing">Marketing</option>
                                    <option value="Messenger">Messenger</option>
                                    <option value="Supervisor Messenger">Supervisor Messenger</option>
                                    <option value="Accountant">Accountant</option>
                                    <option value="Operation">Operation</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="status-select" class="mr-2">Password</label>
                                <input type="password" name="password" id="edit_password" value="*****"
                                    class="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="checkbox" id="edit_role_service" name="role_service">
                                <label for="status-select" class="mr-2">จัดการราคาและบริการ</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="checkbox" id="edit_role_customer" name="role_customer">
                                <label for="status-select" class="mr-2">จัดการลูกค้า</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="checkbox" id="edit_role_employee" name="role_employee">
                                <label for="status-select" class="mr-2">จัดการพนักงาน</label>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="status-select" class="mr-2">Active</label>
                                <div class="custom-control custom-switch switch-success">
                                    <input type="checkbox" class="custom-control-input" id="edit_active" name="active">
                                    <label class="custom-control-label" for="edit_active"></label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal" aria-hidden="true">
                        Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script>
    var data = null;
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            //------------ สร้าง customer ใหม่
            $("#create_employee").submit(function (e) {
                $('#loading').show();
                e.preventDefault();
                var thisform = $(this);
                var url = thisform.attr('action');
                
                $.ajax({
                    type: "POST",
                    data: $('#create_employee').serialize(), 
                    url: url,
                    success: function (result) {
                        $('#loading').hide();
                        if(result=='0'){
                            alert('มีการใช้อีเมล์นี้แล้วในระบบ กรุณาเปลี่ยนอีเมล์ใหม่')
                        }
                        else{
                            location.reload();
                        }
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                        $('#loading').hide();
                    }
                });
            });
            $("#edit_employee").submit(function (e) {
                $('#loading').show();
                e.preventDefault();
                var thisform = $(this);
                var url = thisform.attr('action');
                var id = $('#edit_id').val();
                url = url+'/'+id;
                $.ajax({
                    type: "PATCH",
                    data: $('#edit_employee').serialize(), 
                    url: url,
                    success: function (result) {
                        $('#loading').hide();
                        if(result=='0'){
                            alert('มีการใช้อีเมล์นี้แล้วในระบบ กรุณาเปลี่ยนอีเมล์ใหม่')
                        }
                        else{
                            location.reload();
                        }
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                        alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                        $('#loading').hide();
                    }
                });
            });

            $("#edit_position,#position").change(function(){
                if(this.value=='Administrator'||this.value=='Accountant'){
                    $("#edit_role_service").prop('checked',true);
                    $("#add_role_service").prop('checked',true);
                    $("#edit_role_customer").prop('checked',true);
                    $("#add_role_customer").prop('checked',true);
                    $("#edit_role_employee").prop('checked',true);
                    $("#add_role_employee").prop('checked',true);
                }
                else if(this.value=='Booking Agent'||this.value=='Sale'||this.value=='Operation'){
                    $("#edit_role_service").prop('checked',false);
                    $("#add_role_service").prop('checked',false);
                    $("#edit_role_customer").prop('checked',true);
                    $("#add_role_customer").prop('checked',true);
                    $("#edit_role_employee").prop('checked',false);
                    $("#add_role_employee").prop('checked',false);
                }else{
                    $("#edit_role_service").prop('checked',false);
                    $("#add_role_service").prop('checked',false);
                    $("#edit_role_customer").prop('checked',false);
                    $("#add_role_customer").prop('checked',false);
                    $("#edit_role_employee").prop('checked',false);
                    $("#add_role_employee").prop('checked',false);
                }                           
            });
        });
            function isNumberKey(evt)
                {
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode != 46 && charCode > 31 
                    && (charCode < 48 || charCode > 57))
                    return false;
                    return true;
                }  
            function editmode(id){
                
                var name = $('#name_'+id).html();
                var email = $('#email_'+id).html();
                var tel = $('#tel_'+id).html();
                var position = $('#position_'+id).html();
                $('#edit_id').val(id);
                $('#edit_name').val(name);
                $('#edit_email').val(email);
                $('#edit_tel').val(tel);
                $('#edit_active').prop('checked', true); 
                $("#edit_position").find("option[value='" + position+ "']").attr("selected", true);

                $("#role_service_"+id).is(":checked")?$("#edit_role_service").prop('checked',true):$("#edit_role_service").prop('checked',false);               
                $("#role_customer_"+id).is(":checked")?$("#edit_role_customer").prop('checked',true):$("#edit_role_customer").prop('checked',false);               
                $("#role_employee_"+id).is(":checked")?$("#edit_role_employee").prop('checked',true):$("#edit_role_employee").prop('checked',false);               
                console.log(name+'|'+email+'|'+position+'|'+active);
            }
</script>
@endsection