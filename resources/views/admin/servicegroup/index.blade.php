@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Service Group</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Service Group</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <button type="button" class="btn btn-primary waves-effect waves-light float-left mb-3"
                    data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg">+ Add Group</button>
                <div class="table-responsive">
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!-- end col -->
</div>

<div class="row">
    <div class="col-lg-3">
        <div class="card">
            <div class="card-body">
                <h5 class="mt-0 mb-3">Service Group List</h5>
                <div class="activity">
                    @foreach ($group as $item)
                    <a href="{{ asset('/admin/servicegroup/'.$item->id) }}"><button type="button"
                            class="btn btn-block btn-outline-secondary">{{$item->name}}</button></a>
                    @endforeach

                </div>
                <!--end activity-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!--end col-->
    <div class="col-lg-9">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                            <tr>
                                <th>Image</th>
                                <th>Tracking</th>
                                <th>ID</th>
                                <th>Description</th>
                                <th>Cal</th>
                                <th>Date</th>
                                <!--<th>Active</th>-->
                                <th></th>
                            </tr>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($service as $item)
                            <tr id="s_{{$item->id}}">
                                <td><img src="https://www.smeshipping.com/images/iconexport_09.jpg" alt=""
                                        class="rounded-circle thumb-sm mr-1"></td>
                                <td>{{$item->tracking}}</td>
                                <td>{{$item->id}}</td>
                                <td>
                                    <i class="mdi mdi-earth"
                                        style="color:green; font-size:18px; {!! $item->active=='1'?'':'display:none;'!!}"
                                        data-toggle="tooltip-custom" data-placement="top" title=""
                                        data-original-title="ใช้คำนวณผล"></i>
                                    [{{$item->cat!=null?$item->type_code:'None type of service.'}}]
                                    {{$item->cat!=null?$item->type_name:''}}
                                    <br />
                                    {{$item->description}}
                                </td>
                                <td>{{$item->cal_weight}}</td>
                                <td>{{$item->updated_at}}</td>
                                <!--<td>
                                            <div class="custom-control custom-switch switch-success">
                                                <input type="checkbox" class="custom-control-input"
                                                    id="customSwitchSuccess{{$item->id}}" name="active-{{$item->id}}"
                                                    {!!$item->del==null?'checked':''!!} disabled>
                                                <label class="custom-control-label"
                                                    for="customSwitchSuccess{{$item->id}}"></label>
                                            </div>
                                        </td>-->
                                <td>
                                    <a href="{{url('admin/service/'.$item->id)}}" class="mr-2"><i
                                            class="fas fa-edit text-info font-16"></i></a>
                                    <i class=" fas fa-trash-alt text-danger font-16"
                                        onclick="delete_service({{$item->id}})" style="cursor: pointer;"></i>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <!--end table-->
                {{ $service->links() }}
            </div>

            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!--end col-->

</div>

<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Service Group</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{url('/admin/servicegroup')}}" method="POST">
                    @csrf
                    @method('POST')
                    <h4 class="mt-0 header-title">Service Group</h4>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-1 col-form-label text-left">Name</label>
                        <div class="col-sm-11">
                            <input class="form-control" type="text" value="" required id="name" name="name">
                        </div>
                    </div>
                    <br />
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-danger" id="close_modal" data-dismiss="modal"
                        aria-hidden="true">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script src="{{ asset('assets/js/other_service.js' )}}"></script>
<script>
    $('.deleteitem').click(function(e){
                var x = confirm("Are you sure you want to delete?");
                if (x){
                var id = $(this).attr('delid');
                var url = '{{url('/admin/servicetype')}}';
                url = url+'/'+id;
                var b = $(this).parent().parent().parent()
                $('#loading').show();
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        success: function (result) {
                            $('#loading').hide();
                            //window.location.reload(true);
                            
                            console.log(b);
                            b.remove();
                        },
                        error: function (e) {
                            console.log("ERROR : ", e);
                            alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                            $('#loading').hide();
                        }
                    });
                    }
                else
                    return false;
                
            });
</script>
@endsection