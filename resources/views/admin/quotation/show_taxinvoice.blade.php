@extends('layouts.app')
@section('content')
<link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css' )}}" rel="stylesheet" />
<style>
.botton {
    padding-bottom: 10px;
}

.submit {
    margin-top: 3%;
    width: 100%;
}
.display_none{
    display:none;
}
.button_edit {
    width: 49%;
}
.email{
    font-size: 13px;
    border: 1px solid #e8ebf3;
    height: calc(2.3rem + 2px);
    color: #2f5275;
    width: 100%;
    font-weight: 400;
    line-height: 1.5;
    padding: .375rem .75rem;
}
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Show Tax Invoice</a>
                    </li>
                </ol>
            </div>
            <h4 class="page-title">ใบกำกับภาษี</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">View Table With Button</h4>
                <div class="row">
                    <div class="col-md-4 botton">
                        <div>สามารถก๊อปลิ้งนี้ไปให้ลูกค้าเพื่อกรอก ใบกำกับภาษีได้ครับ</div>
                        <div class="input-group">
                            <input type="text" class="form-control" id="copyTarget"
                                value="https://backend.smeshipping.com/tax_invoice" readonly>
                            <span class="input-group-append"><button class="btn btn-primary"
                                    id="copyButton">Copy</button></span><br>
                        </div>
                    </div>
                </div>
                <div class="row botton">
                    <div class="col-md-4">
                        <form action="{{url('admin/show_taxinvoice')}}" method="GET">
                            <div class="input-group">
                                <input type="text" class="form-control" name="keyword"
                                    placeholder="ค้นหา เบอร์/ชื่อ/อีเมล์/สถานะ"
                                    value="{{app('request')->input('keyword')}}" aria-label="ค้นหา">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="submit">ค้นหา</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table mb-0" id="my-table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Status</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th class="tabledit-toolbar-column"></th>
                                <th class="tabledit-toolbar-column"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customer as $index=>$item)
                            <tr>
                                <td id="{{$item->id}}" status="{{$item->status}}" status_name="{{$item->status_name}}"
                                    name="{{$item->name}}" status_subject="{{$item->status_subject}}"
                                    tax_number="{{$item->tax_number}}" phone="{{$item->phone}}"
                                    status_address="{{$item->status_address}}" address="{{$item->address}}"
                                    email="{{$item->email}}" file_name="{{$item->file_name}}" wht="{{$item->wht}}">
                                    {{$index+1}}</td>
                                <td>{{$item->status}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->phone}}</td>
                                <td>{{$item->email}}</td>
                                <td><button type="button" class="btn btn-outline-success waves-effect waves-light"
                                        data-toggle="modal" data-animation="bounce" data-target=".view"
                                        onclick="set_view_tax_number({{$item->id}})">view</button>
                                </td>
                                <td>
                                    <div class="btn-group btn-group-sm" style="float: none;">
                                        <button type="button" class="btn btn-sm btn-info"
                                            style="float: none; margin: 4px;" data-toggle="modal"
                                            data-animation="bounce" data-target=".edit_employee_modal"
                                            onclick="edit({{$item->id}})">
                                            <span class="ti-pencil"></span></button>
                                        <button type="button" class="btn btn-sm btn-danger deleteitem"
                                            style="float: none; margin: 4px;" delid="{{$item->id}}">
                                            <span class="ti-trash"></span></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!-- end col -->
    <div class="col-12">
        {{ $customer->links() }}
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade bs-example-modal-lg edit_employee_modal" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Edit Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{url('/admin/edit_taxinvoice')}}" name="show_taxinvoice" id="show_taxinvoice" method="POST">
                @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">เลือกความต้องการ</label>
                                <input type="hidden" class="form-control check_modal" id="id" name="id">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" onclick="check_status()" id="acc" value="รับ"
                                      checked>
                                    <label class="form-check-label" for="acc">
                                        รับ
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" onclick="check_status()" id="n_acc" value="ไม่รับ">
                                    <label class="form-check-label" for="n_acc">
                                        ไม่รับ
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 hidden_">
                            <div class="form-group">
                                <label for="LeadName">ออกในนาม</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status_name" id="personal" value="บุคคล"
                                    onclick="check_s_name()"  checked>
                                    <label class="form-check-label" for="personal">
                                        บุคคล
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status_name" id="company" value="บริษัท/ห้างหุ้นส่วนจำกัด" onclick="check_s_name()">
                                    <label class="form-check-label" for="company">
                                        บริษัท / ห้างหุ้นส่วนจำกัด
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <span id="v_status_name"></span>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">ชื่อ</label>
                                <input type="text" class="form-control v_name" name="name" >
                            </div>
                        </div>
                        <div class="col-md-6 hidden_">
                            <div class="form-group">
                                <label for="LeadName">เบอร์</label>
                                <input type="text" class="form-control v_phone" name="phone" >
                            </div>
                        </div>
                    </div>
                    <div class="row hidden_">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">อีเมลล์</label>
                                <input type="text" class="email v_email" name="email" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">ที่อยู่</label>
                                <input type="text" class="form-control v_address" name="address" >
                            </div>
                        </div>
                    </div>
                    <div class="row hidden_">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">สำนักงานใหญ่/สาขา</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status_subject" id="main"
                                        value="สำนักงานใหญ่" checked>
                                    <label class="form-check-label" for="main">
                                        สำนักงานใหญ่
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status_subject" id="branch"
                                        value="สาขา">
                                    <label class="form-check-label" for="branch">
                                        สาขา
                                    </label>
                                    <input type="text" class="form-control v_subject" name="name_branch" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">เลขผู้เสียภาษี</label>
                                <input type="text" class="form-control v_taxnumber" name="tax_number" >
                            </div>
                        </div>
                    </div>
                    <div class="row hidden_">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">หัก/ไม่หัก</label>
                                <div class="form-check tax_wht">
                                    <input class="form-check-input" type="radio" name="wht" id="wht" value="หัก"
                                        checked>
                                    <label class="form-check-label" for="wht">
                                        หัก
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="wht" id="no_wht" value="ไม่หัก">
                                    <label class="form-check-label" for="no_wht">
                                        ไม่หัก
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary button_edit">Save</button>
                    <button type="button" class="btn btn-sm btn-danger button_edit" data-dismiss="modal"
                        aria-hidden="true">
                        Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end -->


<!--  Modal View -->
<div class="modal fade view" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">
                    <span class="v_status"></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <span id="v_status_name"></span>
                    </div>
                    <div class="col-md-6">
                        <div>
                            ชื่อ : <span class="v_name"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div>
                            เบอร์ : <span class="v_phone"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div>
                            อีเมลล์ : <span class="v_email"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div>
                            ที่อยู่ : <span class="v_address"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div>
                            สำนักงานใหญ่/สาขา : <span class="v_subject"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div>
                            เลขผู้เสียภาษี : <span class="v_taxnumber"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div>
                            หัก/ไม่หัก : <span class="v_wht"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-sm btn-danger submit" data-dismiss="modal"
                            aria-hidden="true">
                            Cancel</button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js')}}"></script>
@if (session('alert'))
<script>
swal.fire("เสร็จสิ้น", "ยินยันการแก้ไข", "success");
</script>
@endif
<script>
// $('#show_taxinvoice').submit(function(e) {
//     e.preventDefault();
//     debugger;
//     var thisform = $(this);
//     var url = thisform.attr('action');
//     url = url + '/' + $('#id').val();
//     method = 'POST'
//     $('#loading').show();
//     $.ajax({
//         type: method,
//         data: $('#show_taxinvoice').serialize(),
//         url: url,
//         success: function(result) {
//             debugger;
//             $('#loading').hide();
//             window.location.reload(true);
//         },
//         error: function(e) {
//             console.log("ERROR : ", e);
//             alert(
//                 "An error occurred on the server when processing the URL. Please contact the system administrator."
//             );
//             $('#loading').hide();
//         }
//     });
// });

function set_view_tax_number(id) {
    var obj = $('#' + id);
    $('.v_n_tax').html(obj.attr('id'));
    $('.v_status').html(obj.attr('status'));
    $('.v_status_name').html(obj.attr('status_name'));
    $('.v_name').html(obj.attr('name'));
    $('.v_subject').html(obj.attr('status_subject'));
    $('.v_taxnumber').html(obj.attr('tax_number'));
    $('.v_phone').html(obj.attr('phone'));
    $('.v_s_address').html(obj.attr('status_address'));
    $('.v_address').html(obj.attr('address'));
    $('.v_email').html(obj.attr('email'));
    $('.v_wht').html(obj.attr('wht'));
}

function edit(id, e) {
    var obj = $('#' + id);
    $('#id').val(obj.attr('id'));

    var status = obj.attr('status');
    if(status == "รับ"){
    $('#acc').prop('checked', true);
    $('.hidden_').removeClass('display_none');
    }else{
    $('#n_acc').prop('checked', true);
    $('.hidden_').addClass('display_none');
    }

    var check_wht = obj.attr('wht');
    if (check_wht == 'หัก') {
        $('#wht').prop('checked', true);
    } else {
        $('#no_wht').prop('checked', true);
    }

    var status_name = obj.attr('status_name');
    if(status_name == "บุคคล"){
        $('#personal').prop('checked', true);
        $('.tax_wht').addClass('display_none');
        $('#no_wht').prop('checked', true);
    }else{
        $('#company').prop('checked', true);
    }

    var subject = obj.attr('status_subject');
    if (subject == "สำนักงานใหญ่") {
        $('#main').prop('checked', true);
    } else {
        $('#branch').prop('checked', true);
        subject = $('.v_subject').val(obj.attr('status_subject').substring(4));
    }
    
    $('.v_name').val(obj.attr('name'));
    $('.v_taxnumber').val(obj.attr('tax_number'));
    $('.v_phone').val(obj.attr('phone'));
    $('.v_s_address').val(obj.attr('status_address'));
    $('.v_address').val(obj.attr('address'));
    $('.v_email').val(obj.attr('email'));
}

function check_status(){
    var tax = $('#acc').is(":checked");
    var no_tax = $('#n_acc').is(":checked");
    if (tax) {
        $('.hidden_').removeClass('display_none');
    } else if (no_tax) {
        $('.hidden_').addClass('display_none');
    }
}

function check_s_name(){
    var c_personal = $('#personal').is(":checked");
    var c_company = $('#company').is(":checked");
    if (c_company) {
        $('.tax_wht').removeClass('display_none');
        $('#wht').prop('checked', true);
    }
    if (c_personal) {
        $('.tax_wht').addClass('display_none');
        $('#no_wht').prop('checked', true);
    }
}

$('.deleteitem').click(function(e) {
    swal.fire({
        type: 'question',
        title: 'Are you sure?',
        text: "Are you sure you want to delete?",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            var id = $(this).attr('delid');
            var url = '/admin/delete_taxinvoice/' + id;
            $('#loading').show();
            $.ajax({
                type: 'DELETE',
                url: url,
                success: async function(result) {
                    $('#loading').hide();
                    await swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                    window.location.reload(true);
                },
                error: function(e) {
                    console.log("ERROR : ", e);
                    $('#loading').hide();
                    swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'An error occurred on the server when processing the URL. Please contact the system administrator.',
                    })
                }
            });
        }
    })
});

$('#add').click(function() {
    $('#reset').click();
    $('#id').val('');
});

// Copy To Clipboard
document.getElementById("copyButton").addEventListener("click", function() {
    copyToClipboard(document.getElementById("copyTarget"));
});

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch (e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}
// end
</script>
@endsection