@extends('layouts.app')

@section('content')
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">List Quotation</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">List Quotation</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">Edit Table With Button</h4>
                <p class="text-muted mb-3">Add toolbar column with edit and delete buttons.</p>

                <div class="table-responsive">
                    <table class="table mb-0" id="my-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>เลขที่</th>
                                <th>ลูกค้า</th>
                                <th>เบอร์โทร</th>
                                <th>อีเมล์</th>
                                <th>BA</th>
                                <th>ส่งเมล์ </th>
                                <th class="tabledit-toolbar-column"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->quote_no}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->tel}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->sale}}</td>
                                <td>{!!$item->send_email==null?'<span style="color:red">No</span>':'<span
                                        style="color:green">Yes</span>'!!}</td>

                                <td>
                                    <div class="btn-group btn-group-sm" style="float: none;">
                                        <a href="{{url('/admin/quotation/view/'.$item->id)}}">
                                            <button type="button" class="btn btn-sm btn-info"
                                                style="float: none; margin: 4px;">
                                                <span class="mdi mdi-file-find"></span></button>
                                        </a>

                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!-- end col -->
    {{ $data->links() }}
</div>

@endsection