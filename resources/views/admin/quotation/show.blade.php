<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="COPYRIGHT" content="SME SHIPPING">
    <meta name="CONTACT_ADDR" content="cs@smeshipping.com">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="Cache-Control" content="no-cache">

    <link rel="shortcut icon" type="image/ico" href="{{ asset('/assets/images/favicon.png') }}">
    <link href="{{ asset('/assets/css/smeqt.css') }}" rel="stylesheet" type="text/css">
    <style type="text/css">
        @page {
            size: A4;
            margin: 0;
        }

        @media print {

            html,
            body {
                width: 210mm;
                height: 297mm;
            }

            /* ... the rest of the rules ... */
        }
    </style>

    <title>SME QUOTATION</title>
</head>

<body style="">
    <div id="overlay"></div>


    <section id="qt-content-form" class="qt-wrapper qt-data-info">

        <div class="panel-container" id="qtform" style="border:none">

            <div class="header-th-content">
                <img src="{{ asset('/assets/images/logo.png') }}" class="img-responsive">
            </div>
            <div class="header-th-content ctrl-inlineblock header-content">
                <p>บริษัทเอสเอ็มอี ชิปปิ้ง จำกัด</p>
                <p>46-48 ชอยรัชดาภิเษก 16 ถนนรัชดาภิเษก แขวงวัดท่าพระ</p>
                <p>เขตบางกออกใหญ่ กรุงเทพฯ 10600</p>
                <p>โทรศัพท์ : 02-105-7777 โทรสาร : 02-105-7778</p>
            </div>
            <div class="header-eng-content ctrl-inlineblock header-content">
                <p>SME Shipping Co., Ltd.</p>
                <p>Rachadapisek 16 Alley, Lane 2,</p>
                <p>Wat Tha Pra, Bangkok Yai, Bangkok 10600</p>
                <p>Text : 02-105-7777 Fax : 02-105-7778</p>
            </div>


            <h6>ใบเสนอราคา</h6>

            <div class="customer-info-box rounded-10">
                <p class="cust-info-label ctrl-inlineblock">เลขประจำตัวผู้เสียภาษีอากร</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->customer_id}}</p>

                <p class="cust-info-label ctrl-inlineblock">บริษัท (COMPANY NAME)</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->company}}</p>

                <p class="cust-info-label ctrl-inlineblock">ลูกค้า (CUSTOMER NAME)</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->name}}</p>

                <p class="cust-info-label ctrl-inlineblock">ที่อยู่ (Address)</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->address}}</p>

                <p class="cust-info-label ctrl-inlineblock">โทรศัพท์ (Telephone)</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->tel}}</p>

                <p class="cust-info-label ctrl-inlineblock">อีเมล์ (Email)</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->email}}</p>

            </div>
            <div class="quotation-info-box rounded-10">
                <p class="cust-info-label ctrl-inlineblock">วันที่ (DATE)</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{date('d/m/Y', strtotime($data->created_at))}}</p>

                <p class="cust-info-label ctrl-inlineblock">เลขที่ (QUOTE NO.)</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->quote_no}}</p>

                <p class="cust-info-label ctrl-inlineblock">เงื่อนไขการชำระ (PAYMENT TERMS)</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->payment}}</p>

                <p class="cust-info-label ctrl-inlineblock">กำหนดวันยืนยันราคา (VALID TIME DAY)</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->valid_day}}</p>

                <p class="cust-info-label ctrl-inlineblock">อ้างอิง (REFER)</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->reference}}</p>

            </div>

            <p class="cond-th-desc" style="font-size:12px !important;padding-left:10px;margin-top:10px;">บริษัทฯ
                มีความยินดีที่จะเสนอราคาเพื่อขายบริการแด่ท่านตามราคาและเงื่อนไขที่ระบุไว้ในใบเสนอราคานี้</p>
            <p class="cond-en-desc" style="font-size:9px !important;padding-left:10px">We are please to submit you
                the
                following quotation and offer to sell the services described here in at price. Item and term stated.</p>

            <table class="table table-bordered" id="tbform">
                <thead>
                    <tr>
                        <th width="5px">
                            <p>ลำดับ</p>
                            <p>No.</p>
                        </th>
                        <th>
                            <p>รายละเอียด</p>
                            <p>Description</p>
                        </th>
                        <th>
                            <p>ปลายทาง</p>
                            <p>Destination</p>
                        </th>
                        <th>
                            <p>น้ำหนัก</p>
                            <p>Weight</p>
                        </th>
                        <th>
                            <p>จำนวนเงิน/บาท</p>
                            <p>AMOUNT/THB</p>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $price_total=0;$risk_total=0;?>
                    @foreach ($shipment as $index=>$item)
                    <tr class="irow">
                        <td>{{$index+1}}</td>
                        <td><strong style="font-size: 12px !important;">
                                <input type="hidden" name="s_id[{{$index}}]" value="{{$item->s_id}}">
                                <input type="hidden" name="s_name[{{$index}}]"
                                    value="{{$item->s_name}}">{{$item->s_name}}</strong><br>
                            <?php $volumnWeight = 0; ?>
                            @foreach ($item->box as $indexI=>$itemI)
                            <span style="font-size:12px;">({{$indexI+1}}). </span><span
                                style=" font-size:12px;">{{$itemI->weight}}
                                เทียบกับ {{$itemI->width}} x {{$itemI->height}} x {{$itemI->length}} = </span>
                            <span
                                style="font-size:12px;">{{$itemI->weight>$itemI->width*$itemI->weight*$itemI->length/5000?$itemI->weight:$itemI->width*$itemI->weight*$itemI->length/5000}}</span><br>
                            <input type="hidden" name="width[{{$index}}][{{$indexI}}]" value="{{$itemI->width}}">
                            <input type="hidden" name="weight[{{$index}}][{{$indexI}}]" value="{{$itemI->weight}}">
                            <input type="hidden" name="length[{{$index}}][{{$indexI}}]" value="{{$itemI->length}}">
                            <input type="hidden" name="height[{{$index}}][{{$indexI}}]" value="{{$itemI->height}}">
                            <?php $volumnWeight += ($itemI->weight>$itemI->width*$itemI->weight*$itemI->length/5000?$itemI->weight:$itemI->width*$itemI->weight*$itemI->length/5000); ?>
                            @endforeach
                        </td>
                        <?php 
                            $price_total += $item->s_price;
                            $risk_total +=$item->risk;
                        ?>
                        <td>{{$item->country_name}}</td>
                        <td><input type="hidden" name="volumnWeight[{{$index}}]"
                                value="{{$volumnWeight}}">{{number_format($volumnWeight)}}
                            KG</td>
                        <td><input type="hidden" name="s_price[{{$index}}]"
                                value="{{$item->s_price}}">{{number_format($item->s_price)}}
                            <br></td>
                    </tr>
                    <tr style="border: 1px solid #ddd;">
                        <td style=" border: 1px solid #ddd;">&nbsp;</td>
                        <td style=" border: 1px solid #ddd;">+ Risk and Restricted</td>
                        <td style=" border: 1px solid #ddd;">&nbsp;</td>
                        <td style=" border: 1px solid #ddd;">&nbsp;</td>
                        <td style=" border: 1px solid #ddd;"><input type="hidden" name="risk[{{$index}}]"
                                value="{{$item->risk}}">{{number_format($item->risk)}}</td>
                    </tr>
                    @endforeach

                </tbody>


                <tfoot>
                    <tr>
                        <td colspan="2" rowspan="4" style="text-align:left">
                            <p style="font-weight:700">Condition</p>
                            <p>- Service term Door, Delivered Duties Unpaid.</p>
                            <p>- Money Bock Guarantee (MBG) does not apply to this shipment.</p>
                            <p>- All other conditions of Contact on SHIPPING ORDER and SME SHIPPING Standard Conditions
                                of Carriage
                                shall apply to the shipment.</p>
                            <p>- Remote area and surcharges and other surcharges my be charged in additional to this
                                rate.</p>
                            <p>- SME SHIPPING reserves the right at any time to amend, modify, or discontinue the rate
                                and the terms
                                and conditions herein without prior notice.</p>
                            <p>- Duties and taxes and/or GST (goods and services tax) may be assessed on the contents.
                                Any such charge
                                will be billed to the recipient, unless instructed otherwise by the shipper.
                                Notwithstanding the
                                aforesaid, will aways be primarily responsible for all charges relating to this
                                shipment.</p>
                        </td>
                        <td colspan="2" style="font-weight:bold">จำนวนวเงิน<br>Total</td>
                        <td style="font-weight:bold">{{number_format($price_total+$risk_total)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight:bold">ส่วนลด<br>Discount</td>
                        <td style="font-weight:bold">{{number_format($data->discount)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight:bold">หัก ณ ที่จ่าย<br>WHT {{$data->withholding}}%</td>
                        <td style="font-weight:bold">
                            {{number_format(($price_total+$risk_total)*($data->withholding/100),2)}}</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight:bold">จำนวนเงินรวมทั้งหมด<br>GRAND TOTAL</td>
                        <td style="font-weight:bold">
                            {{number_format(($price_total+$risk_total)-$data->discount-(($price_total+$risk_total)*($data->withholding/100)),2)}}
                        </td>
                    </tr>
                </tfoot>

            </table>


            <div class="twocolumn qt-approve" style="width:45%;">
                <p>อนุมัติสั่งซื้อบริการตามที่ได้เสนอราคา</p>
                <p>Approve to order items descrived here in at price and terms states.</p>

                <p style="margin-top:10px">ผู้สั่งซื้อ</p>
                <p>Approved by __________________________________</p>

                <p style="margin-top:10px">ตำแหน่ง</p>
                <p>Position ______________________________________</p>
            </div>


            <div class="twocolumn qt-sale" style="width:45%;float:right">
                <p>พนักงานขาย <u style="font-size:12px; font-weight:100;">{{$data->salename}}</u></p>

                <p style="margin-top:15px;font-size:12px !important;margin-top:25px;">ขอแสดงความนับถือ</p>
                <p style="font-size:12px !important;margin-bottom: 25px;">Sincerely Yours.</p>

                <p style="margin-top:10px;">หัวหน้าฝ่ายขาย</p>
                <p>Sales Manager ____________________________</p>
            </div>
            <div class="clearfix"></div>


        </div>

    </section>
    <script type="text/javascript">
        window.print();
    </script>

</body>

</html>