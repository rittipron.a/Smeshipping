@extends('layouts.app')
@section('content')
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<link href="{{ asset('/assets/css/smeqt_edit_mode.css') }}" rel="stylesheet" type="text/css">
<style type="text/css">
    .border_{
        border-top: 3px solid #ddd
    }
    .spaced_service{
        font-size: 13px !important;
        font-weight: bold;
        padding-left: 4px;
    }
    .other_service{
        font-size: 13px !important;
        font-weight: bold;
        padding-left: 4px;
    }
    .spaced_detail{
        font-size: 12px;
        padding-left: 13px;
    }
    .spaced_topdetail{
        font-size: 12px;
        padding-left: 4px;
    }
    .hendingfee{
        font-size: 12px;
        text-align: revert;
        padding-left: 13px !important;
    }
    .end_service{
        border-bottom: 1px !important;
    }
    @page {
        size: A4;
        margin: 0;
    }

    @media print {

        html,
        body {
            width: 210mm;
            height: 297mm;
        }

        /* ... the rest of the rules ... */
    }
</style>

<div id="overlay" style="padding-top:50px"></div>

<section id="qt-content-form" class="qt-wrapper qt-data-info">

    <div class="panel-container" id="qtform" style="border:none">

            <div class="header-th-content">
                <img src="{{ asset('/assets/images/logo.png') }}" class="img-responsive">
            </div>
            <div class="header-th-content ctrl-inlineblock header-content">
                <p style="margin-top:10px;">บริษัทเอสเอ็มอี ชิปปิ้ง จำกัด</p>
                <p>46-48 ชอยรัชดาภิเษก 16 ถนนรัชดาภิเษก แขวงวัดท่าพระ</p>
                <p>เขตบางกออกใหญ่ กรุงเทพฯ 10600</p>
                <p>โทรศัพท์ : 02-105-7777 โทรสาร : 02-105-7778</p>
            </div>


            <h6>{{$data->quotation_toppic}}</h6>

            <div class="customer-info-box rounded-10">
                <p class="cust-info-label ctrl-inlineblock">เลขประจำตัวผู้เสียภาษีอากร</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->customer_id}}</p>

                <p class="cust-info-label ctrl-inlineblock">บริษัท</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->company}}</p>

                <p class="cust-info-label ctrl-inlineblock">ลูกค้า</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->name}}</p>

                <p class="cust-info-label ctrl-inlineblock">ที่อยู่</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->address}}</p>

                <p class="cust-info-label ctrl-inlineblock">โทรศัพท์</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->tel}}</p>

                <p class="cust-info-label ctrl-inlineblock">อีเมล์</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->email}}</p>

            </div>
            <div class="quotation-info-box rounded-10">
                <p class="cust-info-label ctrl-inlineblock">วันที่</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{date('d/m/Y', strtotime($data->created_at))}}</p>

                <p class="cust-info-label ctrl-inlineblock">เลขที่</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->quote_no}}</p>

                <p class="cust-info-label ctrl-inlineblock">เงื่อนไขการชำระ</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->payment}}</p>

                <p class="cust-info-label ctrl-inlineblock">กำหนดวันยืนยันราคา</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->valid_day}}</p>

                <p class="cust-info-label ctrl-inlineblock">อ้างอิง</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->reference}}</p>
            </div>

            <p class="cond-th-desc" style="font-size:12px !important;padding-left:10px;margin-top:10px;">
                บริษัทฯมีความยินดีที่จะเสนอราคาขนส่งแด่ท่านตามราคาและเงื่อนไขที่ระบุไว้ในใบเสนอราคานี้</p>
            <table class="table table-bordered" id="tbform">
                <thead>
                    <tr>
                        <th width="5px">
                            <p>ลำดับ</p>
                        </th>
                        <th colspan="2" width="29%">
                            <p>รายละเอียด</p>
                        </th>
                        <th width="5px">
                            <p>หน่วย</p>
                        </th>
                        <th width="100px">
                            <p>จำนวนเงิน (บาท)</p>
                        </th>
                        <th width="100px">
                            <p>ภาษีมูลค่าเพิ่ม 7%</p>
                        </th>
                        <th width="60px">
                            <p>หัก ณ ที่จ่าย 1%</p>
                        </th>
                        <th width="60px">
                            <p>หัก ณ ที่จ่าย 1.5%</p>
                        </th>
                        <th width="100px">
                            <p>จำนวนเงินรวม (บาท)</p>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $price_total=0;$risk_total=0;?>
                    @foreach ($shipment as $index=>$item)
                    <tr class="irow">
                        <td class="border_">{{$index+1}}</td>
                        <td class="border_" colspan="2" style="text-align: revert;">
                                <strong class="spaced_service">
                                    <input type="hidden" name="s_id[{{$index}}]" value="{{$item->s_id}}">
                                    <input type="hidden" name="s_name[{{$index}}]" value="{{$item->s_name}}">{{$item->s_name}}
                                </strong><br>
                            <?php $volumnWeight = 0; ?>
                            @foreach ($item->box as $indexI=>$itemI)
                            <span class="spaced_topdetail">({{$indexI+1}}.)ประเทศ {{$item->country_name}}</span><br>
                            <span class="spaced_detail">น้ำหนักจริง {{$itemI->true_width}} กก.</span><br>
                            <span class="spaced_detail">น้ำหนักปริมาตร {{number_format($itemI->volume_width)}} กก.</span><br>
                            <input type="hidden" name="width[{{$index}}][{{$indexI}}]" value="{{$itemI->width}}">
                            <input type="hidden" name="weight[{{$index}}][{{$indexI}}]" value="{{$itemI->weight}}">
                            <input type="hidden" name="length[{{$index}}][{{$indexI}}]" value="{{$itemI->length}}">
                            <input type="hidden" name="height[{{$index}}][{{$indexI}}]" value="{{$itemI->height}}">
                            <?php $volumnWeight += ($itemI->weight>$itemI->width*$itemI->weight*$itemI->length/5000?$itemI->weight:$itemI->width*$itemI->weight*$itemI->length/5000); ?>
                            @endforeach
                        </td>
                        <?php
                            $price_total += $item->s_price;
                            $risk_total +=$item->risk;
                        ?>
                        <td class="border_">{{$item->unit}} กก.</td>
                        <td class="border_">
                            <input type="hidden" name="volumnWeight[{{$index}}]"
                            value="{{$volumnWeight}}">
                            {{number_format($item->s_price,2)}}
                        </td>
                        <td class="border_">
                            <input type="hidden" name="s_price[{{$index}}]" value="{{$item->s_price}}">
                            {{number_format($item->vat_serven,2)}}
                        </td>
                        <td class="border_">{{number_format($item->wht_one,2)}}</td>
                        <td class="border_">{{number_format($item->wht_tree,2)}}</td>
                        <td class="border_">{{number_format((($item->s_price+$item->vat_serven)-($item->wht_one+$item->wht_tree)),2)}}</td>
                    </tr>
                    @if($item->vat_serven == 0)
                        <tr>
                            <td></td>
                            <td colspan="2" class="hendingfee">ค่าเอกสารจัดส่ง</td>
                            <td>1</td>
                            <td>93.46</td>
                            <td>6.54</td>
                            <td>0.00</td>
                            <td>{{number_format($item->wht_handingfee,2)}}</td>
                            <td>{{number_format(100.00-$item->wht_handingfee,2)}}</td>
                        </tr>
                    @endif
                    @if(!empty($item->other))
                            <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2" style="text-align: revert;"><span class="other_service">บริการเสริม</span></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><input type="hidden" name="risk[{{$index}}]" value="{{$item->risk}}"></td>
                            </tr>
                            @foreach ($item->other as $indexII=>$itemII)
                            @if(count($item->other) > $indexII+1)
                            <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2" class="hendingfee" style="text-align: revert;">{{$itemII->other_name}}</td>
                                    <td>{{$itemII->unit_other_service}}</td>
                                    <td>{{number_format($itemII->service_value,2)}}</td>
                                    <td>{{number_format($itemII->vat_other_service,2)}}</td>
                                    <td>{{number_format($itemII->other_wht_one,2)}}</td>
                                    <td>{{number_format($itemII->other_wht_tree,2)}}</td>
                                    <td>{{number_format($itemII->total_other_service,2)}}</td>
                            </tr>
                            @else
                            <tr>
                                    <td class="end_service">&nbsp;</td>
                                    <td class="end_service hendingfee" colspan="2" style="text-align: revert;">{{$itemII->other_name}}</td>
                                    <td class="end_service">{{$itemII->unit_other_service}}</td>
                                    <td class="end_service">{{number_format($itemII->service_value,2)}}</td>
                                    <td class="end_service">{{number_format($itemII->vat_other_service,2)}}</td>
                                    <td class="end_service">{{number_format($itemII->other_wht_one,2)}}</td>
                                    <td class="end_service">{{number_format($itemII->other_wht_tree,2)}}</td>
                                    <td class="end_service">{{number_format($itemII->total_other_service,2)}}</td>
                            </tr>
                            @endif
                        @endforeach
                        @endif
                    @endforeach
                </tbody>


                <tfoot>
                    <tr>
                        <td colspan="6" rowspan="4" style="text-align:left">
                            <p style="font-weight:700">ข้อกำหนด เงื่อนไขการส่งพัสดุ </p>
                            <p>- การจัดส่งเป็นการบริการแบบส่งถึงที่อยู่ผู้รับปลายทาง
                                (Door To
                                Door) ไม่รวมการชำระค่าภาษี
                                ที่เกิดขึ้นจากศุลกากร</p>
                            <p>- ไม่มีนโยบายรับประกันความพึงพอใจ ยินดีคืนเงิน
                                สำหรับการส่งพัสดุประเภทนี้ </p>
                            <p>- เงื่อนไข และสัญญาอื่นๆ ทั้งหมดบนแบบฟอร์มการส่งสินค้า
                                และข้อกำหนดพื้นฐานของผู้ให้บริการมีผล
                                บังคับใช้กับการส่งสินค้าชิ้นนี้ </p>
                            <p>- พื้นที่นอกเหนือจากการให้บริการ
                                อาจมีการค่าบริการบริการเพิ่มเติม
                                ขึ้นอยู่กับระยะทาง </p>
                            <p>- บริษัท SME SHIPPING จำกัด ขอสงวนสิทธิ์ในการปรับปรุง
                                แก้ไข
                                หรือเปลี่ยนแปลงราคารวมถึง
                                ข้อกำหนด และเงื่อนไขที่แนบมาในที่นี้ได้ตลอดเวลาโดยไม่แจ้งให้ทราบล่วงหน้า
                            </p>
                            <p>- ค่าภาษีศุลกากร และภาษีมูลค่าเพิ่ม หรือ
                                ค่าภาษีสินค้าและบริการขึ้นอยู่กับดุลยพินิจของหน่วยงานที่เกี่ยวข้อง
                                ค่าภาษีใดๆก็ตามที่เกี่ยวข้องจะเป็นหน้าที่ของผู้รับพัสดุในการชำระค่าใช้จ่ายดังกล่าว
                                นอกจากได้รับคำแนะนำ/ เงื่อนไข/ ข้อแนะนำอื่นๆ จากผู้ส่ง</p>
                        </td>
                        <td colspan="2" rowspan="5" style="font-weight:bold;padding-top:9%;">จำนวนวเงิน</td>
                        <td rowspan="5" style="font-weight:bold;padding-top: 9%;">
                            {{number_format($data->total_price,2)}}
                        </td>
                    </tr>
                </tfoot>

        </table>


        <div class="twocolumn qt-approve" style="width:45%;">
                <p>อนุมัติสั่งซื้อบริการตามที่ได้เสนอราคา</p>
                <p></p>

                <p style="margin-top:10px"></p>
                <p>ผู้สั่งซื้อ __________________________________</p>

                <p style="margin-top:10px"></p>
                <p>ตำแหน่ง______________________________________</p>
            </div>
            <div class="twocolumn qt-sale" style="width:45%;float:right">
                <p>พนักงานขาย <u style="font-size:12px; font-weight:100;">{{$data->salename}}</u></p>
                <p style="margin-top:10px;font-size:12px !important;">ขอแสดงความนับถือ</p>
                <p style="margin-top:10px;">หัวหน้าฝ่ายขาย ____________________________</p>
            </div>
            <div class="clearfix"></div>
    </div>

</section>
<br /><br />
<?php
    $th = "th";
    $en = "en";
?>
<div style="text-align:center">
    <a href="{{url('/admin/quotation/sendemail/'.$data->id)}}">
        <button type="button" id="sendemail" class="btn btn-warning waves-effect waves-light bottom_width"
            onclick="$('#loading').show();">Send
            Email {{$data->send_email!=null?'(ส่งแล้ว)':''}}</button>
    </a>
    <a href="{{url('/admin/quotation/'.$data->id.'/'.$th)}}" target="_new">
        <button type="button" id="sendemail"
            class="btn btn-success waves-effect waves-light bottom_width">Print Thai</button>
    </a>
    <a href="{{url('/admin/quotation/'.$data->id.'/'.$en)}}" target="_new">
        <button type="button" id="sendemail"
            class="btn btn-success waves-effect waves-light bottom_width">Print Eng</button>
    </a>
    <a href="{{url('/admin/quotation/edit/'.$data->id)}}">
        <button type="button" id="edit" class="btn btn-secondary waves-effect waves-light bottom_width">Edit</button>
    </a>
</div>
<br /><br />

@if($errors->any())
<script>
    alert("{{$errors->first()}}");
</script>
@endif
@endsection
