<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="COPYRIGHT" content="SME SHIPPING">
    <meta name="CONTACT_ADDR" content="cs@smeshipping.com">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="Cache-Control" content="no-cache">

    <link rel="shortcut icon" type="image/ico" href="{{ asset('/assets/images/favicon.png') }}">
    <link href="{{ asset('/assets/css/smeqt.css') }}" rel="stylesheet" type="text/css">
    <style type="text/css">
    .spaced_service{
        font-size: 13px !important;
        font-weight: bold;
        padding-left: 4px;
    }
    .other_service{
        font-size: 13px !important;
        font-weight: bold;
        padding-left: 4px;
    }
    .spaced_detail{
        font-size: 12px;
        padding-left: 13px;
    }
    .spaced_topdetail{
        font-size: 12px;
        padding-left: 4px;
    }
    .hendingfee{
        font-size: 12px;
        text-align: revert;
        padding-left: 13px !important;
    }
    @page {
        size: A4;
        margin: 0;
    }

    @media print {

        html,
        body {
            width: 210mm;
            height: 297mm;
        }

        /* ... the rest of the rules ... */
    }
    </style>

    <title>SME QUOTATION</title>
</head>

<body style="">
    <div id="overlay"></div>


    <section id="qt-content-form" class="qt-wrapper qt-data-info">

        <div class="panel-container" id="qtform" style="border:none">

            <div class="header-th-content">
                <img src="{{ asset('/assets/images/logo.png') }}" class="img-responsive">
            </div>
            <div class="header-th-content ctrl-inlineblock header-content">
                <p style="margin-top:10px;">SME Shipping Co., Ltd.</p>
                <p>Rachadapisek 16 Alley, Lane 2,</p>
                <p>Wat Tha Pra, Bangkok Yai, Bangkok 10600</p>
                <p>Text : 02-105-7777 Fax : 02-105-7778</p>
            </div>


            <h6>{{$header}}</h6>

            <div class="customer-info-box rounded-10">
                <p class="cust-info-label ctrl-inlineblock">TAXPAYER IDENTIFICATION NUMEBER</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->customer_id}}</p>

                <p class="cust-info-label ctrl-inlineblock">COMPANY NAME</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->company}}</p>

                <p class="cust-info-label ctrl-inlineblock">CUSTOMER NAME</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->name}}</p>

                <p class="cust-info-label ctrl-inlineblock">Address</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->address}}</p>

                <p class="cust-info-label ctrl-inlineblock">Telephone</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->tel}}</p>

                <p class="cust-info-label ctrl-inlineblock">Email</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->email}}</p>

            </div>
            <div class="quotation-info-box rounded-10">
                <p class="cust-info-label ctrl-inlineblock">DATE</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{date('d/m/Y', strtotime($data->created_at))}}</p>

                <p class="cust-info-label ctrl-inlineblock">QUOTE NO.</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->quote_no}}</p>

                <p class="cust-info-label ctrl-inlineblock">PAYMENT TERMS</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->payment}}</p>

                <p class="cust-info-label ctrl-inlineblock">VALID TIME DAY</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->valid_day}}</p>

                <p class="cust-info-label ctrl-inlineblock">REFER</p>
                <p class="cust-info-colon ctrl-inlineblock">:</p>
                <p class="cust-info-cnt ctrl-inlineblock">{{$data->reference}}</p>
            </div>

            <p class="cond-th-desc" style="font-size:12px !important;padding-left:10px;margin-top:10px;">
            We are please to submit you the following quotation and offer to sell shipping rate described here in at price. Item and term stated.</p>
            <table class="table table-bordered" id="tbform">
                <thead>
                    <tr>
                        <th width="5px">
                            <p>No.</p>
                        </th>
                        <th colspan="2">
                            <p>Description</p>
                        </th>
                        <th width="5px">
                            <p>Unit</p>
                        </th>
                        <th width="100px">
                            <p>AMOUNT(THB)</p>
                        </th>
                        <th width="100px">
                            <p>Vat 7%</p>
                        </th>
                        <th width="60px">
                            <p>WHT 1%</p>
                        </th>
                        <th width="60px">
                            <p>WHT 1.5%</p>
                        </th>
                        <th width="100px">
                            <p>Total Amount(THB)</p>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $price_total=0;$risk_total=0;?>
                    @foreach ($shipment as $index=>$item)
                    <tr class="irow">
                        <td>{{$index+1}}</td>
                        <td colspan="2" style="text-align: revert;">
                                <strong class="spaced_service">
                                    <input type="hidden" name="s_id[{{$index}}]" value="{{$item->s_id}}">
                                    <input type="hidden" name="s_name[{{$index}}]" value="{{$item->s_name}}">{{$item->s_name}}
                                </strong><br>
                            <?php $volumnWeight = 0; ?>
                            @foreach ($item->box as $indexI=>$itemI)
                            <span class="spaced_topdetail">({{$indexI+1}}.)Country {{$item->country_name}}</span><br>
                            <span class="spaced_detail">Real Weight {{$item->true_width}} KG.</span><br>
                            <span class="spaced_detail">Volume Weight {{number_format($item->volume_width)}} KG.</span><br>
                            <input type="hidden" name="width[{{$index}}][{{$indexI}}]" value="{{$itemI->width}}">
                            <input type="hidden" name="weight[{{$index}}][{{$indexI}}]" value="{{$itemI->weight}}">
                            <input type="hidden" name="length[{{$index}}][{{$indexI}}]" value="{{$itemI->length}}">
                            <input type="hidden" name="height[{{$index}}][{{$indexI}}]" value="{{$itemI->height}}">
                            <?php $volumnWeight += ($itemI->weight>$itemI->width*$itemI->weight*$itemI->length/5000?$itemI->weight:$itemI->width*$itemI->weight*$itemI->length/5000); ?>
                            @endforeach
                        </td>
                        <?php
                            $price_total += $item->s_price;
                            $risk_total +=$item->risk;
                        ?>
                        <td>{{$item->unit}} KG.</td>
                        <td>
                            <input type="hidden" name="volumnWeight[{{$index}}]"
                            value="{{$volumnWeight}}">
                            {{number_format($item->s_price)}}
                        </td>
                        <td>
                            <input type="hidden" name="s_price[{{$index}}]" value="{{$item->s_price}}">
                            {{number_format($item->vat_serven,2)}}
                        </td>
                        <td>{{number_format($item->wht_one,2)}}</td>
                        <td>{{number_format($item->wht_tree,2)}}</td>
                        <td>{{number_format(($item->s_price+$item->vat_serven)-($item->wht_one+$item->wht_tree))}}</td>
                    </tr>
                    @if($item->vat_serven == 0)
                    <tr>
                        <td></td>
                        <td colspan="2" class="hendingfee">ค่าเอกสารจัดส่ง</td>
                        <td>1</td>
                        <td>93.46</td>
                        <td>6.54</td>
                        <td>0.00</td>
                        <td>{{number_format($item->wht_handingfee,2)}}</td>
                        <td>{{number_format(100.00-$item->wht_handingfee,2)}}</td>
                    </tr>
                    @endif
                    @if(!empty($item->other))
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2" style="text-align: revert;"><span class="other_service">Other Service Charge</span></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><input type="hidden" name="risk[{{$index}}]" value="{{$item->risk}}"></td>
                            </tr>
                            @foreach ($item->other as $indexII=>$itemII)
                            @if(count($item->other) > $indexII+1)
                            <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2" class="hendingfee" style="text-align: revert;">{{$itemII->other_name}}</td>
                                    <td>{{$itemII->unit_other_service}}</td>
                                    <td>{{number_format($itemII->service_value,2)}}</td>
                                    <td>{{number_format($itemII->vat_other_service,2)}}</td>
                                    <td>{{number_format($itemII->other_wht_one,2)}}</td>
                                    <td>{{number_format($itemII->other_wht_tree,2)}}</td>
                                    <td>{{number_format($itemII->total_other_service,2)}}</td>
                            </tr>
                            @else
                            <tr>
                                    <td class="end_service">&nbsp;</td>
                                    <td class="end_service hendingfee" colspan="2" style="text-align: revert;">{{$itemII->other_name}}</td>
                                    <td class="end_service">{{$itemII->unit_other_service}}</td>
                                    <td class="end_service">{{number_format($itemII->service_value,2)}}</td>
                                    <td class="end_service">{{number_format($itemII->vat_other_service,2)}}</td>
                                    <td class="end_service">{{number_format($itemII->other_wht_one,2)}}</td>
                                    <td class="end_service">{{number_format($itemII->other_wht_tree,2)}}</td>
                                    <td class="end_service">{{number_format($itemII->total_other_service,2)}}</td>
                            </tr>
                            @endif
                        @endforeach
                    @endif
                    @endforeach
                </tbody>


                <tfoot>
                    <tr>
                        <td colspan="6" rowspan="4" style="text-align:left">
                            <p style="font-weight:700">Condition</p>
                            <p>- Service term Door to Door, Delivered Duties Unpaid*</p>
                            <p>- Money Bock Guarantee (MBG) does not apply to this shipment.</p>
                            <p>- All other conditions of Contact on SHIPPING ORDER and SME SHIPPING Standard Conditions of Carriage shall apply to the shipment.</p>
                            <p>- Remote area and surcharges and other surcharges my be charged in additional to this rate. </p>
                            <p>- SME SHIPPING reserves the right at any time to amend, modify, or discontinue the rate and the terms and conditions herein without prior notice.</p>
                            <p>- Duties and taxes and/or GST (goods and services tax) may be assessed on the contents. Any such charge will be billed to the recipient, unless instructed otherwise by the shipper. Notwithstanding the aforesaid, will aways be primarily responsible for all charges relating to this shipment.</p>
                        </td>
                        <td colspan="2" rowspan="5" style="font-weight:bold;padding-top: 15%;">Total</td>
                        <td rowspan="5" style="font-weight:bold;padding-top: 15%;">
                            {{number_format($data->total_price,2)}}
                        </td>
                    </tr>
                </tfoot>

            </table>


            <div class="twocolumn qt-approve" style="width:45%;">
                <p>Approve to order items descrived here in at price and terms states.</p>
                <p></p>

                <p style="margin-top:10px"></p>
                <p>Approved by __________________________________</p>

                <p style="margin-top:10px"></p>
                <p>Position ______________________________________</p>
            </div>


            <div class="twocolumn qt-sale" style="width:45%;float:right">
                <p>Employee <u style="font-size:12px; font-weight:100;">{{$data->salename}}</u></p>

                <p style="margin-top:10px;font-size:12px !important;">Sincerely Yours.</p>

                <p style="margin-top:10px;">Sales Manager ____________________________</p>
            </div>
            <div class="clearfix"></div>


        </div>

    </section>
    <script type="text/javascript">
    window.print();
    </script>

</body>

</html>
