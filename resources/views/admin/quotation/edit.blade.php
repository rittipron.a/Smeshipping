@extends('layouts.app')
@section('content')
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/select2/select2.min.css" rel="stylesheet' )}}" type="text/css" />
<link href="{{ asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css' )}}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
    rel="stylesheet" />
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Quotation</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Quotation</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <link href="{{ asset('/assets/css/smeqt_edit_mode.css') }}" rel="stylesheet" type="text/css">
                <style type="text/css">
                .border_underline{
                    border-bottom-color: #ddd;
                }
                .spaced_service {
                    font-size: 13px !important;
                    font-weight: bold;
                    padding-left: 4px;
                }
                .other_service{
                    font-size: 13px !important;
                    font-weight: bold;
                    padding-left: 4px;
                }
                .spaced_detail {
                    font-size: 12px;
                    padding-left: 37px;
                }

                .spaced_topdetail {
                    font-size: 12px;
                    padding-left: 15px;
                }
                .hendingfee {
                    font-size: 12px;
                    text-align: revert;
                    padding-left: 13px !important;
                }
                .topic_text{
                    color: #A91D20 !important;
                    font-family: RSU_BOLD,RSU_light,RSU_Regular, SourceSansPro-Regular,"Lucida Sans Unicode", "Lucida Grande", sans-serif;
                    font-size: 18px;
                }
                .th_select{
                    color:red;
                    font-weight:bold;
                }
                .en_select{
                    color:red;
                    font-weight:bold;
                }
                .select_tr{
                    cursor: pointer;
                }
                .select_tr:hover{
                    color: red;
                }
                .select_tr:active{
                    color: red;
                    font-weight: bold;
                }
                .center_input {
                    text-align: center;
                }
                .end_service{
                    border-bottom: 1px !important;
                }
                @page {
                    size: A4;
                    margin: 0;
                }

                @media print {

                    html,
                    body {
                        width: 210mm;
                        height: 297mm;
                    }
                    /* ... the rest of the rules ... */
                }
                </style>

                <title>SME QUOTATION</title>
                <div id="overlay"></div>
                <form action="{{url('/admin/quotation_update/'.$data->id)}}" method="POST">
                    @csrf
                    <section id="qt-content-form" class="qt-wrapper qt-data-info">
                        <div style="text-align: right; margin-right: 1%;">
                            <span>
                                เปลี่ยนภาษาในการส่ง
                            </span>
                                <span class="select_tr translate th_select" id="th"> TH</span> /
                                <span class="select_tr translate" id="en">EN</span>
                                <input type="hidden" id="translate" name="translate" value="th">
                        </div>
                        <h1>
                            <span key="quotation" class="switch_text topic_text">ใบเสนอราคา</span>
                            <span class="switch_toppic" style="cursor: pointer;">
                                <input type="hidden" id="switch_toppic" value="0">
                                <i class="fas fa-recycle" style=""></i>
                                <span>เปลี่ยนเป็นหัวข้อ</span>
                                <span class="switch_">ใบแจ้งหนี้</span>
                            </span>
                        </h1>
                        <div class="panel-container" id="qtform" style="border:none">
                            <div class="header-th-content">
                                <img src="{{ asset('/assets/images/logo.png') }}" class="img-responsive">
                            </div>
                            <div class="header-th-content ctrl-inlineblock header-content">
                                <p class="lang" key="company_address_1">บริษัทเอสเอ็มอี ชิปปิ้ง จำกัด</p>
                                <p class="lang" key="company_address_2">46-48 ชอยรัชดาภิเษก 16 ถนนรัชดาภิเษก
                                    แขวงวัดท่าพระ</p>
                                <p class="lang" key="company_address_3">เขตบางกออกใหญ่ กรุงเทพฯ 10600</p>
                                <p class="lang" key="company_address_4">โทรศัพท์ : 02-105-7777 โทรสาร : 02-105-7778</p>
                            </div>

                            <h6 class="quotation_toppic">ใบเสนอราคา</h6>
                            <input type="hidden" class="quotation_toppic" name="quotation_toppic" value="ใบเสนอราคา">
                            <div class="customer-info-box rounded-10">
                                <p class="cust-info-label ctrl-inlineblock lang" key="taxpayer_number">
                                    เลขประจำตัวผู้เสียภาษีอากร</p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock"><input type="text" name="customer_id"
                                        value="{{$data->customer_id}}" style="width:100%"></p>

                                <p class="cust-info-label ctrl-inlineblock lang" key="company_name">บริษัท</p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock"><input type="text" name="company"
                                        value="{{$data->company}}" style="width:100%"></p>

                                <p class="cust-info-label ctrl-inlineblock lang" key="customer_name">ลูกค้า</p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock"><input type="text" name="name"
                                        value="{{$data->name}}" style="width:100%" required></p>

                                <p class="cust-info-label ctrl-inlineblock lang" key="address">ที่อยู่</p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock"><input type="text" name="address"
                                        value="{{$data->address}}" style="width:100%"> </p>

                                <p class="cust-info-label ctrl-inlineblock lang" key="telephone">โทรศัพท์</p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock"><input type="text" name="tel"
                                        value="{{$data->tel}}" style="width:100%"></p>

                                <p class="cust-info-label ctrl-inlineblock lang" key="email">อีเมล์</p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock"><input type="email" name="email"
                                        value="{{$data->email}}" style="width:100%" required>
                                </p>

                            </div>
                            <div class="quotation-info-box rounded-10">
                                <p class="cust-info-label ctrl-inlineblock lang" key="date">วันที่</p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock">{{ date('d M Y') }}</p>

                                <p class="cust-info-label ctrl-inlineblock lang" key="quote_no">เลขที่</p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock"><input style="width: 100%;" type="text"
                                        value="{{$data->quote_no}}" name="quote_no" disabled placeholder="Auto" />
                                </p>

                                <p class="cust-info-label ctrl-inlineblock lang" key="payment_terms">เงื่อนไขการชำระ</p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock" style="padding-top: 5px;">
                                    <select name="payment">
                                        <option selected="" value="Cash">Cash</option>
                                        <option value="{{$data->payment}}">Credit</option>
                                    </select></p>

                                <p class="cust-info-label ctrl-inlineblock lang" key="valid_time_day">กำหนดวันยืนยันราคา
                                </p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock"><input style="width: 100%;" type="text"
                                        name="valid_day" id="mdate" value="{{$data->valid_day}}"></p>

                                <p class="cust-info-label ctrl-inlineblock lang" key="refer">อ้างอิง</p>
                                <p class="cust-info-colon ctrl-inlineblock">:</p>
                                <p class="cust-info-cnt ctrl-inlineblock"><input style="width: 100%;" type="text"
                                        value="{{$data->reference}}" name="reference"></p>
                            </div>
                            <p class="cond-th-desc lang"
                                style="font-size:0.9em !important;padding-left:10px;margin-top:10px;"
                                key="detail_condition">
                                บริษัทฯมีความยินดีที่จะเสนอราคาขนส่งแด่ท่านตามราคาและเงื่อนไขที่ระบุไว้ในใบเสนอราคานี้
                            </p>
                            <div style="text-align: center;padding-top: 10px;padding-bottom: 10px;">
                                <button class="btn btn-primary lang" style="width: 98%;" type="button"
                                    data-toggle="modal" data-animation="bounce" data-target=".setdiscount"
                                    key="add_service">+ เพิ่มบริการ</button>
                            </div>
                            <table class="table table-bordered" id="tbform">
                                <thead>
                                    <tr>
                                        <th width="5px">
                                            <p class="lang" key="no">ลำดับ</p>
                                        </th>
                                        <th colspan="2">
                                            <p class="lang" key="description">รายละเอียด</p>
                                        </th>
                                        <th width="84px">
                                            <p class="lang" key="unit">หน่วย</p>
                                        </th>
                                        <th width="104px">
                                            <p class="lang" key="amount">จำนวนเงิน (บาท)</p>
                                        </th>
                                        <th width="104px">
                                            <p class="lang" key="serven_vat">ภาษีมูลค่าเพิ่ม 7%</p>
                                        </th>
                                        <th width="104px">
                                            <p class="lang" key="wht_1">หัก ณ ที่จ่าย 1%</p>
                                        </th>
                                        <th width="104px">
                                            <p class="lang" key="wht_3">หัก ณ ที่จ่าย 1.5%</p>
                                        </th>
                                        <th>
                                            <p class="lang" key="total_amount">จำนวนเงินรวม (บาท)</p>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $price_total=0;$risk_total=0;?>
                                    @foreach ($shipment as $index=>$item)
                                    <tr class="irow delete_{{$index}}">
                                        <td>{{$index+1}}<br>
                                        </td>
                                        <td colspan="2" style="text-align: revert;"><strong class="spaced_service" style="font-size: 1.1em !important;">
                                                <input type="hidden" name="s_id[{{$index}}]" value="{{$item->s_id}}">
                                                <input type="hidden" name="s_name[{{$index}}]"
                                                    value="{{$item->s_name}}">{{$item->s_name}}</strong><br>
                                            <?php $volumnWeight = 0; ?>
                                            @foreach ($item->box as $indexI=>$itemI)
                                            <span style="padding-left:5%; font-size:16px;">
                                                <span class="lang" key="country">({{$indexI+1}}) ประเทศ </span>{{$item->country_name}}
                                            </span><br>
                                            <span style="font-size:16px; padding-left:5%;">
                                                <span class="lang" key="real_weight">น้ำหนักจริง</span> 
                                                {{$item->weight}} <span class="lang" key="kg">กก.</span>
                                            </span><br>
                                            <span style="font-size:16px; padding-left:5%;">
                                            <span class="lang" key="volume_weight">น้ำหนักปริมาตร</span>
                                                {{number_format($item->volume_width)}} <span class="lang" key="kg">กก.</span>
                                            </span><br>
                                            <input type="hidden" name="width[{{$index}}][{{$indexI}}]"
                                                value="{{$itemI->width}}">
                                            <input type="hidden" name="weight[{{$index}}][{{$indexI}}]"
                                                value="{{$itemI->weight}}">
                                            <input type="hidden" name="length[{{$index}}][{{$indexI}}]"
                                                value="{{$itemI->length}}">
                                            <input type="hidden" name="height[{{$index}}][{{$indexI}}]"
                                                value="{{$itemI->height}}">
                                                <input type="hidden" name="volume_width[{{$index}}][{{$indexI}}]" 
                                                value="{{number_format($item->volume_width)}}">
                                            <?php $volumnWeight += ($itemI->weight>$itemI->width*$itemI->weight*$itemI->length/5000?$itemI->weight:$itemI->width*$itemI->weight*$itemI->length/5000); ?>
                                            @endforeach
                                        </td>
                                        <?php
                                         $price_total += $item->s_price;
                                        $risk_total +=$item->risk;
                                        ?>
                                        <td>{{$item->unit}} <span class="lang" key="kg">กก.</span></td>
                                        <td>
                                            <input type="text" class="center_input" onkeypress="return isNumberKey(event)"
                                            onkeyup="cal({{$index+1}},{{$index}})" name="s_price[{{$index}}]" value="{{$item->s_price}}">
                                        </td>
                                        <td>
                                            {{number_format($item->vat_serven,2)}}
                                        </td>
                                        <td>
                                        {{number_format($item->wht_one,2)}}
                                        </td>
                                        <td>
                                        {{number_format($item->wht_tree,2)}}
                                        </td>
                                        <td>
                                            <?php
                                            if($item->vat_serven == 0){
                                                $total_s_h = (($item->s_price+$item->vat_serven)-($item->wht_one+$item->wht_tree));
                                            }else{
                                                $total_s_h = ((($item->s_price-100)+$item->vat_serven)-($item->wht_one+$item->wht_tree));
                                            }
                                            ?>
                                            <span id="show_s_price{{$index+1}}">{{number_format($total_s_h,2)}}</span><br>
                                            <div class="btn btn-danger" style="margin-top: 10%;" onclick="td_delete($(this),{{$index}})">
                                            <i class="fa fa-trash"></i></div>
                                            <input type="hidden" name="total_s_price[{{$index}}]" id="total_s_price{{$index+1}}" value="{{$total_s_h,2}}">
                                            <input type="hidden" name="wht_tree[{{$index}}]" id="wht_tree{{$index+1}}" value="{{$item->wht_tree}}">
                                            <input type="hidden" name="wht_one[{{$index}}]" id="wht_one{{$index+1}}" value="{{$item->wht_one}}">
                                            <input type="hidden" name="vat_serven[{{$index}}]" id="vat_serven{{$index+1}}" value="{{$item->vat_serven}}">
                                            <input type="hidden" name="unit[{{$index}}]" value="{{$item->unit}}">
                                            <input type="hidden" name="risk[{{$index}}]" value="{{$item->risk}}">
                                            <input type="hidden" name="country[{{$index}}]" value="'{{$item->country_name}}">
                                        </td>
                                    </tr>
                                    @if($item->vat_serven == 0)
                                    <tr class="delete_{{$index}}">
                                        <td></td>
                                        <td colspan="2" class="hendingfee">ค่าเอกสารจัดส่ง</td>
                                        <td>1</td>
                                        <td>93.46</td>
                                        <td>6.54</td>
                                        <td>0.00</td>
                                        <td>
                                            <input type="text" class="center_input" name="wht_handingfee[{{$index}}]" onkeyup="cal_handing({{$index}})" id="handing_fee{{$index}}"
                                                   onkeypress="return isNumberKey(event)" value="{{$item->wht_handingfee}}"></td>
                                        <td>
                                            <span class="total_handing{{$index}}">{{number_format(100.00-$item->wht_handingfee,2)}}</span>
                                            <input type="hidden" name="total_handingfee[{{$index}}]" id="total_handing{{$index}}" value="{{100-$item->wht_handingfee}}">
                                        </td>
                                    </tr>
                                    @else
                                    <input type="hidden"  name="wht_handingfee[{{$index}}]" value="{{$item->wht_handingfee}}">
                                    <input type="hidden" id="total_handing{{$index}}" name="total_handingfee[{{$index}}]" value="{$item->total_handing}}">
                                    @endif
                                        @foreach ($item->other as $indexII=>$itemII)
                                        <tr class="delete_{{$index}} n{{$index}}">
                                                <td>&nbsp;</td>
                                                <td colspan="2" style="text-align:left"><span>{{$itemII->other_name}}</span></td>
                                                <td><span class="unit_other_service{{$index}}">{{$itemII->unit_other_service}}</span></td>
                                                <td><span class="sh_other_service" id="sh_other_service{{$index}}">{{number_format($itemII->service_value,2)}}</span></td>
                                                <td><span class="vat_other_service{{$index}}{{$index}}">{{number_format($itemII->vat_other_service,2)}}</td>
                                                <td>
                                                @if($itemII->other_wht_one != 0)
                                                <input type="text" class="center_input" id="other_vat_1_{{$index}}{{$indexII}}" name="other_wht_one[{{$index}}][{{$indexII}}]"
                                                value="{{$itemII->other_wht_one}}" onkeypress="return isNumberKey(event)" onkeyup="cal_other_service({{$index}},{{$indexII}})">
                                                @else
                                                <input type="text" class="center_input" style="background-color:#ddd;" id="other_vat_1_{{$index}}{{$indexII}}" name="other_wht_one[{{$index}}][{{$indexII}}]"
                                                value="{{$itemII->other_wht_one}}" onkeypress="return isNumberKey(event)" onkeyup="cal_other_service({{$index}},{{$indexII}})" readonly>
                                                @endif
                                                </td>
                                                <td>
                                                @if($itemII->other_wht_tree != 0)
                                                <input type="text" class="center_input" id="other_vat_3_{{$index}}{{$indexII}}" name="other_wht_tree[{{$index}}][{{$indexII}}]"
                                                value="{{$itemII->other_wht_tree}}" onkeypress="return isNumberKey(event)" onkeyup="cal_other_service({{$index}},{{$indexII}})">
                                                @else
                                                <input type="text" class="center_input" style="background-color:#ddd;" id="other_vat_3_{{$index}}{{$indexII}}" name="other_wht_tree[{{$index}}][{{$indexII}}]"
                                                value="{{$itemII->other_wht_tree}}" onkeypress="return isNumberKey(event)" onkeyup="cal_other_service({{$index}},{{$indexII}})" readonly>
                                                @endif
                                                </td>
                                                <td><span id="show_other_service{{$index}}{{$indexII}}">{{number_format($itemII->total_other_service,2)}}</span>
                                                <input type="hidden" id="other_name{{$index}}{{$indexII}}" name="other_name[{{$index}}][{{$indexII}}]" value="{{$itemII->other_name}}">
                                                <input type="hidden" id="vat_other_service{{$index}}{{$indexII}}" name="vat_other_service[{{$index}}][{{$indexII}}]" value="{{$itemII->vat_other_service}}">
                                                <input type="hidden" id="unit_other_service{{$index}}{{$indexII}}" name="unit_other_service[{{$index}}][{{$indexII}}]" value="{{$itemII->unit_other_service}}">
                                                <input type="hidden" id="service_value{{$index}}{{$indexII}}" name="service_value[{{$index}}][{{$indexII}}]" value="{{$itemII->service_value}}">
                                                <input type="hidden" id="total_other_service{{$index}}{{$indexII}}" name="total_other_service[{{$index}}][{{$indexII}}]" value="{{$itemII->total_other_service}}">
                                                </td>
                                        </tr>
                                        @endforeach
                                        <tr class="delete_{{$index}}" id="add_other{{$index}}">
                                                <td class="end_service">&nbsp;</td>
                                                <td colspan="2" class="end_service" style="text-align: revert;padding-left:1%;font-size:16px;">
                                                    <span class="select_tr lang" key="other_service" onclick="modal_other({{$index}},0)">+ บริการเสริม</span>
                                                </td>
                                                <td class="end_service">&nbsp;</td>
                                                <td class="end_service">&nbsp;</td>
                                                <td class="end_service">&nbsp;</td>
                                                <td class="end_service">&nbsp;</td>
                                                <td class="end_service">&nbsp;</td>
                                                <td class="end_service"><input type="hidden" name="risk[{{$index}}]" value="{{$item->risk}}"></td>
                                        </tr>
                                        <?php
                                            $num =0;
                                            $num = $num + ($index+1);
                                        ?>
                                    @endforeach
                                    <input type="hidden" value="{{$num}}" id="number_">
                                </tbody>

                                <tfoot id="tfoot">
                                    <tr>
                                        <td colspan="7" rowspan="4" style="text-align:left">
                                            <p class="lang" style="font-weight:700" key="condition">ข้อกำหนด
                                                เงื่อนไขการส่งพัสดุ </p>
                                            <p class="lang" key="condition_1">-
                                                การจัดส่งเป็นการบริการแบบส่งถึงที่อยู่ผู้รับปลายทาง (Door To
                                                Door) ไม่รวมการชำระค่าภาษี
                                                ที่เกิดขึ้นจากศุลกากร</p>
                                            <p class="lang" key="condition_2">- ไม่มีนโยบายรับประกันความพึงพอใจ
                                                ยินดีคืนเงิน สำหรับการส่งพัสดุประเภทนี้ </p>
                                            <p class="lang" key="condition_3">- เงื่อนไข และสัญญาอื่นๆ
                                                ทั้งหมดบนแบบฟอร์มการส่งสินค้า
                                                และข้อกำหนดพื้นฐานของผู้ให้บริการมีผล
                                                บังคับใช้กับการส่งสินค้าชิ้นนี้ </p>
                                            <p class="lang" key="condition_4">- พื้นที่นอกเหนือจากการให้บริการ
                                                อาจมีการค่าบริการบริการเพิ่มเติม
                                                ขึ้นอยู่กับระยะทาง </p>
                                            <p class="lang" key="condition_5">- บริษัท SME SHIPPING จำกัด
                                                ขอสงวนสิทธิ์ในการปรับปรุง แก้ไข
                                                หรือเปลี่ยนแปลงราคารวมถึง
                                                ข้อกำหนด และเงื่อนไขที่แนบมาในที่นี้ได้ตลอดเวลาโดยไม่แจ้งให้ทราบล่วงหน้า
                                            </p>
                                            <p class="lang" key="condition_6">- ค่าภาษีศุลกากร และภาษีมูลค่าเพิ่ม หรือ
                                                ค่าภาษีสินค้าและบริการขึ้นอยู่กับดุลยพินิจของหน่วยงานที่เกี่ยวข้อง
                                                ค่าภาษีใดๆก็ตามที่เกี่ยวข้องจะเป็นหน้าที่ของผู้รับพัสดุในการชำระค่าใช้จ่ายดังกล่าว
                                                นอกจากได้รับคำแนะนำ/ เงื่อนไข/ ข้อแนะนำอื่นๆ จากผู้ส่ง</p>
                                        </td>
                                        <td colspan="1" rowspan="5" class="padding_tb"
                                            style="font-weight:bold;padding-top: 9%;">
                                            <span class="lang" key="total">จำนวนวเงิน</span></td>
                                        <td class="padding_tb" rowspan="5" style="font-weight:bold;padding-top: 9%;"
                                            id="total_price">{{number_format($data->total_price,2)}}
                                        </td>
                                        <input type="hidden" class="total_price" name="total_price" value="{{number_format($data->total_price,2)}}">
                                    </tr>
                                </tfoot>

                            </table>
                            <div class="twocolumn qt-approve" style="width:45%;">
                                <p class="lang" key="approve">อนุมัติสั่งซื้อบริการตามที่ได้เสนอราคา</p>
                                <p style="margin-top: 20px;" class="lang" key="approved_by">ผู้สั่งซื้อ
                                    __________________________________</p>
                                <p style="margin-top: 20px;" class="lang" key="position">ตำแหน่ง
                                    ______________________________________</p>
                            </div>
                            <div class="twocolumn qt-sale" style="width:45%;float:right">
                                <p><span class="lang" key="employee">พนักงานขาย</span> <u
                                        style="font-size:16px; font-weight:100;">{{Auth::user()->name}}</u></p>
                                <p style="font-size:1.4em !important;margin-bottom: 25px;margin-top: 20px;" class="lang"
                                    key="sincerely">ขอแสดงความนับถือ</p>
                                <p style="margin-top: 20px;" class="lang" key="sales">หัวหน้าฝ่ายขาย
                                    ____________________________</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </section>
                    <div style="text-align: center;padding-top: 20px;"><button style="width: 77%;" type="submit"
                            class="btn btn-success waves-effect waves-light bottom_width">Save</button></div>
                </form>
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>

    <!-- end col -->
</div>

<!--  Modal content for the above example -->
<div class="modal fade setdiscount" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add service</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <iframe id='select_frame' name='select_frame' src='{{url('/admin/mainpickupcost_ifram')}}' width="100%"
                    height="700px"></iframe>
                <button type="button" class="btn btn-sm btn-primary" id="ifram_add">เลือก</button>
                <button type="button" class="btn btn-sm btn-danger" id="close_modal_discount" data-dismiss="modal"
                    aria-hidden="true">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="supplement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" id="close_supplement" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-pane p-3" id="Other" role="tabpanel">
                        <div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true"
                             data-toggle="toast">
                            <div class="toast-header">
                                <i class="dripicons-checklist"></i>
                                <strong class="mr-auto"> :: Other Service Charge<input type="hidden" class="num_other"
                                value=""></strong>
                            </div>
                            <div class="toast-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered mb-0 table-centered" id="other_service_edit">
                                        <thead class="thead-light">
                                        <tr>
                                            <th width="5"></th>
                                            <th>Service</th>
                                            <th width="120">Charge (Baht)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck0"/></td>
                                            <td>Emergency surcharge<input type="hidden" class="other_service_name"
                                                                          value="Emergency surcharge"/></td>
                                            <td style="padding: unset;"><input name="esc" id="esc"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck1"/></td>
                                            <td>International Freight<input type="hidden" class="other_service_name"
                                                                            value="International Freight"/></td>
                                            <td style="padding: unset;"><input name="sh_international_freight"
                                                                               id="sh_international_freight"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck2"/></td>
                                            <td>Permium<input type="hidden" class="other_service_name"
                                                              value="Permium"/></td>
                                            <td style="padding: unset;"><input name="sh_permium"
                                                                               id="sh_permium"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck3"/></td>
                                            <td>Remote area Pickup<input type="hidden" class="other_service_name"
                                                                         value="Remote area Pickup"/></td>
                                            <td style="padding: unset;"><input name="sh_remote_area_pickup"
                                                                               id="sh_remote_area_pickup"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck4"/></td>
                                            <td>Remote area Delivery<input type="hidden" class="other_service_name"
                                                                           value="Remote area Delivery"/></td>
                                            <td style="padding: unset;"><input name="sh_remote_area_delivery"
                                                                               id="sh_remote_area_delivery"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck5"/></td>
                                            <td>Shipment Redirect<input type="hidden" class="other_service_name"
                                                                        value="Shipment Redirect"/></td>
                                            <td style="padding: unset;"><input name="sh_shipment_redirect"
                                                                               id="sh_shipment_redirect"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck6"/></td>
                                            <td>Over Weight Piece<input type="hidden" class="other_service_name"
                                                                        value="Over Weight Piece"/></td>
                                            <td style="padding: unset;"><input name="sh_over_weight_piece"
                                                                               id="sh_over_weight_piece"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck7"/></td>
                                            <td>Over Sized Piece<input type="hidden" class="other_service_name"
                                                                       value="Over Sized Piece"/></td>
                                            <td style="padding: unset;"><input name="sh_over_sized_piece"
                                                                               id="sh_over_sized_piece"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck8"/></td>
                                            <td>Elevated Risk<input type="hidden" class="other_service_name"
                                                                    value="Elevated Risk"/></td>
                                            <td style="padding: unset;"><input name="sh_elevated_risk"
                                                                               id="sh_elevated_risk"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck9"/></td>
                                            <td>Restricted Destination<input type="hidden" class="other_service_name"
                                                                             value="Restricted Destination"/>
                                            </td>
                                            <td style="padding: unset;"><input name="sh_restricted_destination"
                                                                               id="sh_restricted_destination"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck10"/></td>
                                            <td>Change of billing<input type="hidden" class="other_service_name"
                                                                        value="Change of billing"/></td>
                                            <td style="padding: unset;"><input name="sh_change_of_billing"
                                                                               id="sh_change_of_billing"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck11"/></td>
                                            <td>Address correction<input type="hidden" class="other_service_name"
                                                                         value="Address correction"/></td>
                                            <td style="padding: unset;"><input name="sh_address_correction"
                                                                               id="sh_address_correction<"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck12"/></td>
                                            <td>Shipment Insurance<input type="hidden" class="other_service_name"
                                                                         value="Shipment Insurance"/></td>
                                            <td style="padding: unset;"><input name="sh_shipment_insurance"
                                                                               id="sh_shipment_insurance"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck13"/></td>
                                            <td>All custom services<input type="hidden" class="other_service_name"
                                                                          value="All custom services"/></td>
                                            <td style="padding: unset;"><input name="sh_all_custom_services"
                                                                               id="sh_all_custom_services"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close_modal_all" class="btn btn-secondary" data-dismiss="modal">Close
                    </button>
                    <button type="button" onclick="add_ck()" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
<script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}"></script>
<script src="{{ asset('assets/pages/jquery.forms-advanced.js' )}}"></script>
<script src="{{ asset('assets/js/create_quotation.js?1')}}"></script>
@endsection
