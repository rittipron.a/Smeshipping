@extends('layouts.app')
@section('content')
<style>
    label {
        padding-top: 15px;
    }

    .button_white{
        color: red;
        border-color: red;
        background: white;
    }

    .button_red{
        background: red;
        border-color: red;
        color: white;
    }
    input:checked + label {
        background: red;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Introduce</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Introduce</h4>
        </div>
    </div>
</div>
<div class="row justify-content-md-center">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h3 id="page_topic">ส่งอีเมล์</h3>

                <form action="{{url('/admin/introduce')}}" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="validationCustom01">ชื่อลูกค้า <span style="color:red">*</span></label>
                            <input type="text" class="form-control" name="name" required="">
                        </div>
                        <!--end col-->
                        <div class="col-md-6">
                            <label for="validationCustom02">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" name="tel" id="tel" onkeyup="check_sms()">
                        </div>
                    </div>
                    <div class="form-row">
                        <!--end col-->
                        <div class="col-md-12">
                            <label for="validationCustomUsername">อีเมล <span style="color:red">*</span></label>
                            <div class="input-group">
                                <input type="email" name="email" class="form-control" multiple required="">
                            </div>
                        </div>
                        <!--end col-->

                            <!-- เก็บข้อมูล teamplate_sms -->
                            <div class="col-md-12">
                                <label>ข้อความเพื่อส่งSMS (ปล่อยว่างไว้กรณีไม่ต้องการส่ง SMS)</label>
                                @if(!$data->isEmpty())
                                @foreach($data as $index=>$item)
                                    @if($index == 0)
                                        <input type="text" name="sms_detail" class="form-control" id="sms_detail"  onkeyup="check_sms()" value="{{$item->teamplate_sms}}">
                                    @endif
                                @endforeach
                                @else
                                    <input type="text" name="sms_detail" class="form-control" id="sms_detail"  onkeyup="check_sms()" value="">
                                @endif
                            </div>
                            <!-- สิ้นสุดการเก็บข้อมูล teamplate_sms -->

                            <div class="col-md-12">
                                <label>เนื้อหาที่ส่งอีเมล <span style="color:red">*</span></label>
                                <br />

                                <!-- เก็บข้อมูล teamplate_name == set_email_detail -->
                                <div class="btn-group btn-group-toggle" data-toggle="buttons" id="add_teample_mail">
                                @if(!$data->isEmpty())
                                    @foreach($data as $index=>$item)
                                    <label class="btn btn-info" style="margin-right: 5px; border-radius: 3px;" for="teamplate_name{{$index+1}}" onclick="set_email_detail({{$index+1}})">
                                        <input type="radio" class="check_button" id="teamplate_name{{$index+1}}"  value="{{$item->teamplate_name}}" autocomplete="off">{{$item->teamplate_name}}
                                    </label>
                                    @endforeach
                                @else
                                <label class="btn btn-info">
                                    <input type="button" class="check_button" id="teamplate_name1" onclick="set_email_detail(1)" value="แบบฟอร์มแนะนำบริการ">แบบฟอร์มแนะนำบริการ
                                </label>
                                @endif
                                <button class="btn btn-info" type="button" id="add_teample_number" onclick="add_teample()"> + เพิ่มฟอร์ม</button>
                                </div>
                                <!-- สิ้นสุดการเก็บข้อมูล teamplate_name -->
                                
                                <!-- เก็บข้อมูล email_topic -->
                                <div style="margin-top: 10px;">
                                @if(!$data->isEmpty())
                                @foreach($data as $index=>$item)
                                    @if($index == 0)
                                        <span>Toppic email : </span>
                                        <input type="text" name="email_topic" id="email_topic" class="form-control"  value="{{$item->email_topic}}">
                                    @endif
                                @endforeach
                                @else
                                    <span>Toppic email : </span>
                                    <input type="text" name="email_topic" id="email_topic" class="form-control"  value="แนะนำบริการส่งทั่วโลกจาก SME SHIPPING ผ่านเครือข่าย DHL FEDEX TNT">
                                @endif
                                </div>
                                <!-- สิ้นสุดการเก็บข้อมูล email_topic --> 

                                  <!-- เก็บข้อมูล CC -->
                                  <div>
                                  @if(!$data->isEmpty())
                                    @foreach($data as $index=>$item)
                                        @if($index == 0)
                                            <span>CC : </span>
                                            <input type="email" name="cc" id="cc" class="form-control"  value="{{$item->cc}}">
                                        @endif
                                    @endforeach
                                  @else
                                    <span>CC : </span>
                                    <input type="email" name="cc" id="cc" class="form-control"  value="">
                                  @endif
                                </div>
                                <!-- สิ้นสุดการเก็บข้อมูล CC --> 

                                  <!-- เก็บข้อมูล BCC -->
                                  <!-- <div>
                                @if(!$data->isEmpty())
                                @foreach($data as $index=>$item)
                                    @if($index == 0)
                                    <span>BCC : </span>
                                    <input type="email" name="bcc" id="bcc" class="form-control"  value="{{$item->bcc}}">
                                    @endif
                                @endforeach
                                @else
                                    <span>BCC : </span>
                                    <input type="email" name="bcc" id="bcc" class="form-control"  value="">
                                @endif
                                </div> -->
                                <!-- สิ้นสุดการเก็บข้อมูล BCC --> 

                                <!-- เก็บข้อมูล teamplate_mail -->
                                <input type="hidden" id="code" value="1">
                                @if(!$data->isEmpty())
                                @foreach($data as $index=>$item)
                                    @if($index == 0)
                                    <textarea id="elm2" name="email_detail">
                                            {{$item->teamplate_mail}}
                                    </textarea>
                                    @endif
                                @endforeach
                                @else
                                <textarea id="elm2" name="email_detail">
                                    <p><strong>SME Shipping ให้บริการขนส่งพัสดุด่วนระหว่างประเทศ ผ่านเครือข่ายการขนส่งชั้นนำ DHL FEDEX TNT</strong></p>
                                    <p><strong>&nbsp;</strong></p>
                                    <p>อัตราค่าบริการส่งเอกสารและพัสดุภัณฑ์ระหว่างประเทศแบบ Door to Door คุณลูกค้าสามารถกด download ได้ตามลิงค์ด้านล่างนี้</p>
                                    <p><a href="https://smeshipping.com/ftp/smeshipping_expintro.pdf"><strong>อัตราค่าบริการส่งด้วย </strong><strong>DHL EXPRESS</strong></a></p>
                                    <p><a href="https://smeshipping.com/ftp/smeshipping_expintro_fedex.pdf"><strong>อัตราค่าบริการส่งด้วย </strong><strong>FEDEX EXPRESS</strong></a></p>
                                    <p>&nbsp;</p>
                                    <p><strong>ข้อกำหนดและเงื่อนไขในการให้บริการ</strong><strong>&nbsp;</strong></p>
                                    <p><strong>วิธีการคำนวณค่าบริการ</strong></p>
                                    <p>อัตราค่าบริการคำนวณจากสิ่งที่มากกว่าระหว่างน้ำหนัก เปรียบเทียบกับปริมาตรกล่อง</p>
                                    <p>วิธีการคำนวณปริมาตรกล่องคือ</p>
                                    <p>ความกว้าง x ความยาว x ความสูง (หน่วยเป็นเซนติเมตร) หารด้วย 5000 = &nbsp;ผลลัพธ์ หน่วย กิโลกรัม</p>
                                    <p>&nbsp;</p>
                                    <p><strong>ภาษีนำเข้า</strong></p>
                                    <p>ราคาค่าบริการการส่งระหว่างประเทศ ไม่รวมค่าภาษีนำเข้าที่ประเทศปลายทาง ค่าใช้จ่ายใดๆที่เกิดจากการผ่านพิธีการนำเข้าที่ประเทศปลายทาง ผู้รับสิ่งของที่ประเทศปลายทาง เป็นผู้รับผิดชอบค่าใช้จ่ายที่เกิดขึ้น&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <p><strong>ข้อมูลสิ่งจัดส่ง</strong></p>
                                    <p>ผู้ส่งจำเป็นต้องชี้แจงรายละเอียดของสิ่งของที่จัดส่งให้ครบถ้วนชัดเจน เพื่อนำข้อมูลมาใช้ในการผ่านพิธีการศุลกากรที่ประเทศปลายทาง&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <p><strong>การชำระค่าบริการ</strong> <br />ลูกค้าสามารถชำระค่าบริการได้ 3 ช่องทางดังนี้</p>
                                    <ol>
                                    <li>การโอนเงินผ่านธนาคาร <br />ธนาคารกรุงเทพ&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; เลขที่ 7492340884<br />ธนาคารกสิกร&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;เลขที่ 1414584381<br />ธนาคารกรุงศรีอยุธยา&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; เลขที่ 1421318612<br />พร้อมเพย์ (Prompt Pay) &nbsp; เลขที่ 0105552114948</li>
                                    <li>ผ่านบัตรเครดิต (VISA/MASTER )</li>
                                    <li>Online payment&nbsp;(VISA/MASTER )</li>
                                    </ol>
                                    <p>&nbsp;</p>
                                    <p><strong>การหัก ณ ที่จ่าย</strong></p>
                                    <p>ลูกค้านิติบุคคลสามารถหัก ณ ที่จ่าย 1% เป็นค่าขนส่ง (กรุณาแจ้งเจ้าหน้าที่ ไม่สามารถหักย้อนหลังได้)</p>
                                    <p>&nbsp;</p>
                                    <p><strong>การนำส่งใบเสร็จรับเงิน</strong></p>
                                    <p>ใบเสร็จและใบกำกับภาษีจะจัดส่งให้ภายใน 7 วันทำการ (กรณีลูกค้าบุคคลต้องการใบเสร็จโปรดแจ้งเจ้าหน้าที่)</p>
                                    <p>กรณีหัก ณ ที่จ่าย จะจัดส่งใบเสร็จและใบกำกับให้ หลังจากได้รับใบหักตัวจริงเท่านั้น</p>
                                    <p>&nbsp;</p>
                                    <p><strong>ค่าบริการส่วนเพิ่มเติม </strong></p>
                                    <p>การใช้บริการส่งเอกสารและพัสดุระหว่างประเทศ อาจจะมีค่าบริการเพิ่มเติมดังนี้</p>
                                    <p>&nbsp;</p>
                                    <ol>
                                    <li>บริการให้บริการเข้ารับเอกสารและพัสดุภัณฑ์</li>
                                    </ol>
                                    <p style="padding-left: 30px;">&nbsp; 1.1&nbsp;เข้ารับฟรี ภายในกรุงเทพ และปริมณฑล</p>
                                    <p style="padding-left: 30px;">&nbsp; 1.2&nbsp;บริการเข้ารับในเขตที่นอกเหนือจากกรุงเทพ และปริมณฑล มีค่าบริการรับสินค้ากิโลกรัมละ 25 บาท ค่าบริการขั้นต่ำ 500 บาท&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <ol start="2">
                                    <li>ราคาค่าขนส่งระหว่างประเทศแบบ Door to Door อาจมีค่าธรรมเนียมการส่งเพิ่มในเขตพื้นที่นอกเขตการให้บริการ(Remote Area) ซึ่งสามารถตรวจสอบได้จาก ชื่อเมือง และรหัสไปรษณีย์ของประเทศปลายทาง โดยคิดค่าธรรมเนียมการจัดส่งในพื้นที่นอกเขตการให้บริการกิโลกรัม 25 บาท หรือขั้นต่ำที่ 900 บาทต่อที่อยู่การจัดส่ง&nbsp;</li>
                                    </ol>
                                    <p>&nbsp;</p>
                                    <ol start="3">
                                    <li>การประกันภัยสินค้า</li>
                                    </ol>
                                    <p style="padding-left: 30px;">&nbsp; 3.1 สำหรับสินค้ามูลค่าไม่เกิน 20,000 บาท สามารถซื้อประกันภัยเพิ่มเพียง 500บาท&nbsp;</p>
                                    <p style="padding-left: 30px;">&nbsp; 3.2 สำหรับสินค้ามูลค่าเกิน 20,000 บาท สามารถซื้อประกันภัยเพิ่มเพียง 2.5% ของมูลค่าสินค้า</p>
                                    <p>&nbsp;</p>
                                    <ol start="4">
                                    <li>หากมีการเปลี่ยนแปลงที่อยู่การจัดส่ง หลังจากที่สิ่งจัดส่งถูกส่งออกไปแล้ว จะมีค่าธรรมเนียมในการเปลี่ยนแปลง 1000 บาท ต่อที่อยู่การจัดส่ง</li>
                                    </ol>
                                    <p>&nbsp;</p>
                                    <ol start="5">
                                    <li>ค่าบริการแพคกิ้ง</li>
                                    </ol>
                                    <p style="padding-left: 30px;">&nbsp; 5.1 บรรจุลงกล่องกระดาษพร้อมวัสดุกันกระแทก กิโลกรัมละ 10 บาท หรือขั้นต่ำ 100 บาท ต่อกล่อง&nbsp;</p>
                                    <p style="padding-left: 30px;">&nbsp; 5.2 บรรจุลงลังไม้ อัตราค่าบริการ&nbsp;คิวบิคเมตรละ 4,800 บาท&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <ol start="6">
                                    <li>ค่าธรรมเนียมการจัดส่งพัสดุขนาดใหญ่ ที่มีน้ำหนักมากกว่า/เท่ากับ 70กิโลกรัม หรือขนาดของพัสดุด้านใดด้านหนึ่งมีขนาดมากกว่า/เท่ากับ 120 ซม. คิดค่าธรรมเนียม 1700 บาทต่อที่อยู่การจัดส่ง</li>
                                    </ol>
                                    <p>&nbsp;</p>
                                    <ol start="7">
                                    <li>การส่งกลับ หรือเรียกคืนพัสดุที่จัดส่งกลับจากต่างประเทศ จะมีค่าใช้จ่ายในการขนส่งขากลับ และค่าพิธีการและภาษีศุลกากรที่ประเทศปลายทางและประเทศไทย</li>
                                    </ol>
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <p>ต้องการใช้บริการสามารถติดต่อได้ที่</p>
                                    <p>โทร 02-105-7777</p>
                                    <p>Line: @shipping<br />Facebook: <a href="http://www.fb.com/smeshippingthailand">www.fb.com/smeshippingthailand</a><br />ลงทะเบียนใช้บริการด้วยตัวเองได้ที่: <a href="http://www.smeshipping.com/regis">www.smeshipping.com/regis</a></p>
                                    <p>&nbsp;</p>
                                    <p>บริษัท เอสเอ็มอี ชิปปิ้ง จำกัด (SME SHIPPING)<br />ที่อยู่: <a href="https://maps.google.com/?q=46-48+%E0%B8%8B%E0%B8%AD%E0%B8%A2%E0%B8%A3%E0%B8%B1%E0%B8%8A%E0%B8%94%E0%B8%B2%E0%B8%A0%E0%B8%B4%E0%B9%80%E0%B8%A9%E0%B8%81+16+%E0%B8%96.%E0%B8%A3%E0%B8%B1%E0%B8%8A%E0%B8%94%E0%B8%B2%E0%B8%A0%E0%B8%B4%E0%B9%80%E0%B8%A9%E0%B8%81%C2%A0%E0%B9%81%E0%B8%82%E0%B8%A7%E0%B8%87%E0%B8%A7%E0%B8%B1%E0%B8%94%E0%B8%97%E0%B9%88%E0%B8%B2%E0%B8%9E%E0%B8%A3%E0%B8%B0+%E0%B9%80%E0%B8%82%E0%B8%95%E0%B8%9A%E0%B8%B2%E0%B8%87%E0%B8%81%E0%B8%AD%E0%B8%81%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88+%E0%B8%81%E0%B8%97%E0%B8%A1+10600&amp;entry=gmail&amp;source=g">46-48 ซอยรัชดาภิเษก 16 ถ.รัชดาภิเษก&nbsp;แขวงวัดท่าพระ เขตบางกอกใหญ่ กทม 10600</a>.<br />แผนที่ตั้ง:&nbsp;<a href="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;oe=UTF8&amp;msa=0&amp;msid=214455629359140717341.0004b533cd6c74a2446c1&amp;t=m&amp;vpsrc=0&amp;ll=13.725044,100.477567&amp;spn=0.005003,0.006866&amp;z=17&amp;source=embed">Google Maps</a></p>
                                    <p><strong>&nbsp;</strong></p>
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>
                                </textarea>
                                @endif
                                <!-- สิ้นสุดการเก็บเนื้อหาเมลล์ -->
                                <br />
                                <!--end form-group-->
                                <button class="btn btn-danger button_white" id="send_mail" type="submit">ส่งข้อมูล</button>&nbsp;&nbsp;
                                <input type="button" class="btn btn-danger button_white" id="set_topic_name" data-toggle="modal" data-target="#save" value="บันทึก">
                                <input type="button" class="btn btn-danger button_white" id="delete_templete" onclick="del_templete()" value="ลบ">
                                <input type="button" class="btn btn-danger button_white" id="cancel_templete" value="ยกเลิก">
                            </div>
                    </div>
                </form>
                <!--end form-->
                <!-- Templete -->
                <div style='display:none;'>
                    @foreach($data as $index=>$item)
                    <div class="check_length">
                            <input type="hidden" id="code{{$index+1}}" value="{{$item->id}}">
                            <input type="hidden" id="sms_{{$index+1}}"  value="{{$item->teamplate_sms}}">
                            <input type="hidden" id="email_topic{{$index+1}}" value="{{$item->email_topic}}">
                            <input type="hidden" id="cc{{$index+1}}" value="{{$item->cc}}">
                            <input type="hidden" id="bcc{{$index+1}}" value="{{$item->bcc}}">
                            <input type="hidden" id="group_name{{$index+1}}" value="{{$item->group_name}}">
                            <textarea id="email_{{$index+1}}">{{$item->teamplate_mail}}</textarea>
                    </div>
                    @endforeach
                </div>
                <!-- END Templete -->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!--end col-->
</div>

<div class="modal fade" id="save" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">สร้างหัวข้อของเนื้อหาอีเมลล์นี้ </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <label for="teamplate_name_new" class="col-form-label">หัวข้ออีเมลล์:</label>
            <input type="text" class="form-control" id="teamplate_name_new" value="แบบฟอร์มแนะนำบริการ">
            <div class="form-group">
                <label for="group_email">Group Email</label>
                <select class="form-control" id="group_email">
                <option value="Greut">Greut</option>
                <option value="DHL">DHL</option>
                <option value="Fedex">Fedex</option>
                <option value="TNT">TNT</option>
                </select>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary button_white" data-dismiss="modal">ยกเลิก</button>
        <button type="button" id="save_templete" class="btn btn-primary button_red">บันทึก</button>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script>
    $(document).ready(function(){
            $('#set_topic_name').show();
            $('#cancel_templete').hide();
            $('#teamplate_name1').prop('checked', true);
            var group_name = $('#group_name1').val();
            if(group_name != ""){
                $('#group_email option[value='+group_name+']').attr('selected','selected');
            }else{
                $('#group_email option[value=Greut]').attr('selected','selected');
            }
    });

    function check_sms(){
        if($('#tel').val()!=''&&$('#sms_detail').val()!=''){
           $('#page_topic').text('ส่งอีเมล์ และ SMS');
        }
        else{
            $('#page_topic').text('ส่งอีเมล์');
        }
    }

    var number = $('.check_length').parent().parent().find('.check_length').length;
    var check_number = $('.check_length').parent().parent().find('.check_length').length;
    chack = true;
    function set_email_detail(n){
        for(i = 1; i = n; i++){
        if(n == i){
            var code_number = $('#code'+i).val();
            var sms = $('#sms_'+i).val();
            var email = $('#email_'+i).text();
            var topic = $('#email_topic'+i).val();
            var cc = $('#cc'+i).val();
            var bcc = $('#bcc'+i).val();
            debugger;
            var group_name = $('#group_name'+i).val();
            $("#group_email").find('option').attr("selected",false) ;
            if(group_name != ""){
                $('#group_email option[value='+group_name+']').attr('selected',true);
            }else{
                $('#group_email option[value=Greut]').attr('selected',true);
            }
            var teamplate_name = $('#teamplate_name'+i).val();
            if(email == "" || email == null){
                $("#code").val("0");
                $('#sms_detail').val("");
                $('#email_topic').val('SME SHIPPING');
                $('#cc').val('');
                $('#bcc').val('');
                var str = 'รูปแบบที่ '+ n +' อยู่ระหว่างรอแบบฟอร์ม... ';
                $('#elm2').text(str);
                $("#elm2_ifr").contents().find("#tinymce").html(str);
                $('#teamplate_name_new').val(teamplate_name);
            }else{
                $("#code").val(code_number);
                $("#sms_detail").val(sms);
                $("#email_topic").val(topic);
                $("#cc").val(cc);
                $("#bcc").val(bcc);
                $("#elm2_ifr").contents().find("#tinymce").html(email);
                $('#teamplate_name_new').val(teamplate_name);
            }
            
                if(n <= check_number){
                    $('#set_topic_name').show();
                    $('#cancel_templete').hide();
                    $('#send_mail').show();
                    $('#delete_templete').show();
                }else{
                    $('#set_topic_name').show();
                    $('#cancel_templete').show();
                    $('#send_mail').hide();
                    $('#delete_templete').hide();
                }
            }
            if(n != 1){
                chack = false;
                { break; }
            }else{
                chack = true;
                { break; }
            }       
      }
    }
    function add_teample(){
        var html = "";
        number++;
        var n = number;
        html = 
        '<label class="btn btn-info" for="add_'+number+'" onclick="set_email_detail('+number+')" style="margin-right: 5px; border-radius: 3px;">'+
        '<input class="check_button" type="radio" id="add_'+number+'"  style="margin-right: 3px;" >แบบฟอร์มแนะนำบริการ '+ number + 
        '</label>';
            $("#add_teample_mail").append(html); 
            $('#set_topic_name').show();
            $('#cancel_templete').show();
            $('#add_teample_number').hide();
            $('#send_mail').hide();
            $('#delete_templete').hide();
            $("#add_"+number ).prop( "checked", true );
        set_email_detail(n);
        }

    $('#cancel_templete').click(function(){
        var cancel = $("#add_" + number);
        cancel.parent().remove();
        $('#set_topic_name').show();
        $('#cancel_templete').hide();
        $('#add_teample_number').show();
        $('#send_mail').show();
        $('#delete_templete').show();
        number--;
        set_email_detail(1);
    });

    function del_templete(){
        var id = $("#code").val()
        $("#loading").show();
        if (confirm('คุณแน่ใจที่จะลบข้อมูลนี้')) {
            $("#loading").show();
            $.ajax({
            type: "GET",
            url: '/delete_templete_mail_sms/'+ id,
                success: function (result) {
                    console.log(result);
                    window.location.reload();
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                }
            });
        } else {
            // Do nothing!
        }
        $("#loading").hide();
    }

    $("#save_templete").click(function(){
        var id = $('#code').val();
        var group_name = $('#group_email').val();
        if(chack){
        var templete_mail = $('#elm2').val();
        }else{
        var templete_mail = $("#elm2_ifr").contents().find("#tinymce").html();
        }
        var sms_detail = $('#sms_detail').val();
        var email_topic = $('#email_topic').val();
        var cc = $('#cc').val();
        var bcc = $('#bcc').val();
        var teamplate_name = $('#teamplate_name_new').val();
        if(teamplate_name == ""){
            alert('กรุณากรอกชื่อหัวข้อ');
        }else{
        var form_data = new FormData;
        form_data.append("id", id);
        form_data.append("teamplate_name", teamplate_name);
        form_data.append("teamplate_sms", sms_detail);
        form_data.append("teamplate_mail", templete_mail);
        form_data.append("email_topic", email_topic);
        form_data.append("cc", cc);
        form_data.append("bcc", bcc);
        form_data.append("group_name", group_name);

        $.ajax({
            type: "POST",
            data: form_data,
            url: '/save_teamplete_mail_sms',
            processData: false,
            contentType: false,
            success: function(result) {
                console.log(result);
                console.log('Save temp success.');
                window.location.reload();
            },
            error: function(e) {
                console.log("ERROR : ", e);
            }
        });
        }
    });

    @if($errors->any())
     alert('{{$errors->first()}}');
    @endif
</script>
@endsection