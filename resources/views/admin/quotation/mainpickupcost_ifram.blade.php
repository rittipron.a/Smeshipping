@extends('layouts.app')

@section('content')
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<style>
    #notification,
    .topbar,
    #footer {
        display: none;
    }

    body,
    p,
    .label,
    .btn-next {
        color: rgba(101, 98, 105, 1);
        font-family: 'Prompt', sans-serif;
        font-size: 16px;
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    .btn-next {
        background: white !important;
        border: unset !important;
        color: #ef2828 !important;
        font-size: 18px;
        filter: drop-shadow(rgba(0, 0, 0, 0.15) 0px 3px 6px);
    }

    .note {
        border-radius: 13px;
        margin: 33px 0 0 0;
        -webkit-box-shadow: 5px 7px 15px 6px rgba(168, 150, 168, 0.71);
        -moz-box-shadow: 5px 7px 15px 6px rgba(168, 150, 168, 0.71);
        box-shadow: 5px 7px 15px 6px rgba(168, 150, 168, 0.71);
    }

    .text_note {
        padding: 26px 0px 0px 37px;
        border-bottom: 1px solid;
        padding-bottom: 16px;
    }

    .text_note_detail {
        padding: 22px 0px 6px 0px;
    }

    .note_center {
        text-align: center;
    }

    .text-topic {
        font-size: 13px;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Quotation</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Quotation</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3 style="text-align:center">
                    <b>เช็คราคาส่งพัสดุ</b>
                </h3>
                <form name="check_form" id="check_form" action="{{url('/admin/quotation')}}" method="POST">
                    @csrf
                    <div class="row">
                        <input type="hidden" class="form-control" name="service_name" id="service_name">
                        <input type="hidden" class="form-control" name="service_price" id="service_price">
                        <div class="col-md-12">
                            <i class="mdi mdi-airplane"></i> ส่งไป<br />
                            <select class="select2 form-control mb-3 custom-select" id="sh_country" name="sh_country"
                                required>
                                @foreach ($country_all as $c_item)
                                <option value="{{$c_item->country_name}}" code="{{$c_item->country_cd}}"
                                    iata="{{$c_item->country_cd}}" risk="{{$c_item->risk_price}}">
                                    {{$c_item->country_name}}</option>
                                @endforeach
                            </select>
                            <div class="risk" style="display:none">
                                <i class="mdi mdi-bitcoin"></i> Risk and Restricted<br />
                                <input type="text" class="form-control" name="risk" id="risk"
                                    onkeypress="return isNumberKey(event)">
                                <br />
                            </div>
                            <div id="box_area">
                                <div class="row box">
                                    <div class="col-12 col-md-12" style="padding-bottom:10px;"><i
                                            class="mdi mdi-dropbox"></i>
                                        ขนาดพัสดุกล่องที่ <span class="box_number">1</span><br />
                                    </div>

                                    <div class="col-12 col-md-3">
                                        <span class="text-topic">น้ำหนัก (KG)</span>
                                        <input type="text" name="weight[]" class="form-control weight"
                                            style="text-align:center" placeholder="" onkeyup="cal_box()"
                                            onkeypress="return isNumberKey(event)">
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <span class="text-topic">กว้าง (CM)</span>
                                        <input type="text" name="width[]" class="form-control width"
                                            style="text-align:center" placeholder="" onkeyup="cal_box()"
                                            onkeypress="return isNumberKey(event)">
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <span class="text-topic">ยาว (CM)</span>
                                        <input type="text" name="length[]" class="form-control length"
                                            style="text-align:center" placeholder="" onkeyup="cal_box()"
                                            onkeypress="return isNumberKey(event)">
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <span class="text-topic">สูง (CM)</span>
                                        <input type="text" name="height[]" class="form-control height"
                                            style="text-align:center" placeholder="" onkeyup="cal_box()"
                                            onkeypress="return isNumberKey(event)">
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <span class="text-topic">น้ำหนักปริมาตร</span>
                                        <input type="text" name="total_box[]" class="form-control total_box"
                                            id="total_box" style="text-align:center" placeholder="" onkeyup="cal_box()"
                                            onkeypress="return isNumberKey(event)" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12" style="text-align:center">
                                <button type="button" class="btn btn-danger btn-next waves-effect waves-light"
                                    style="margin-top:10px;" onclick="addbox()"><i
                                        class="mdi mdi-plus-circle-outline"></i>
                                    เพิ่มกล่อง</button>
                            </div><br />
                            <i class="mdi mdi-scale"></i> น้ำหนักที่ใช้คิดราคา : <span id="total_volumnWeight">0</span>
                            KG<br />
                            <i class="mdi mdi-scale"></i> น้ำหนักจริงรวม : <span id="box_volumnWeight">0</span> KG<br />
                            <div>
                                <input type="text" class="form-control" style="display:none" name="volumnWeight"
                                    id="volumnWeight" placeholder="" readonly>
                            </div>
                            <div id="note"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="row service_select" id="service_select" style="display:none">
                            </div>
                        </div>
                        <button type="submit" id="submit_form" style="display:none"></button>
                    </div>
                </form>
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>

    <!-- end col -->
</div>

<script>
    function addbox() {
    var box_number = $('.box').length + 1;
    var html =
        '<div class="row box"><div class="col-12 col-md-12" style="padding-bottom:10px;"><i class="mdi mdi-dropbox"></i> ขนาดพัสดุกล่องที่ <span class="box_number">' +
        box_number +
        '</span><span class="box_del badge badge-danger" onclick="box_del($(this))">X</span><br></div>'+
        '<div class="col-12 col-md-3"><span class="text-topic">น้ำหนัก (KG)</span><input type="text" name="weight[]" class="form-control weight" style="text-align:center" placeholder="" onchange="cal_box()" onkeypress="return isNumberKey(event)"></div>'+
        '<div class="col-12 col-md-2"><span class="text-topic">กว้าง (CM)</span><input type="text" name="width[]" class="form-control width" style="text-align:center" placeholder="" onchange="cal_box()" onkeypress="return isNumberKey(event)"></div>'+
        '<div class="col-12 col-md-2"><span class="text-topic">ยาว (CM)</span><input type="text" name="length[]" class="form-control length" style="text-align:center" placeholder="" onchange="cal_box()" onkeypress="return isNumberKey(event)"></div>'+
        '<div class="col-12 col-md-2"><span class="text-topic">สูง (CM)</span><input type="text" name="height[]" class="form-control height" style="text-align:center" placeholder="" onchange="cal_box()" onkeypress="return isNumberKey(event)"></div>'+
        '<div class="col-12 col-md-3"><span class="text-topic">น้ำหนักปริมาตร</span><input type="text" name="total_box[]" class="form-control total_box"style="text-align:center" placeholder="" onchange="cal_box()" onkeypress="return isNumberKey(event)" readonly> </div></div>';
    $('#box_area').append(html);
}

function box_del(obj) {
    $(obj).parent().parent().remove();
    $('.box').each(function(i, e) {
        $(e).find(".box_number").html(i + 1);
    });
    cal_box();
}

function cal_box() {
    console.log('cal_box');
    var volumnWeight = 0;
    var box_weight = 0;
    $('.box').each(function(i, e) {
        var weight = parseFloat(convert2number($(e).find(".weight").val()));
        var width = parseFloat(convert2number($(e).find(".width").val()));
        var length = parseFloat(convert2number($(e).find(".length").val()));
        var height = parseFloat(convert2number($(e).find(".height").val()));
        var total_box = 0;
        var sum = width * length * height / 5000;
        var total_sum = calup(sum);
        if(sum > weight){
            total_box = calup(sum);
        }else if(sum < weight){
            total_box  = calup(weight);
        }
        $(e).find(".total_box").val(total_sum);
        volumnWeight=volumnWeight+total_box;
        box_weight += weight;
    });
    
    $('#total_volumnWeight').text(volumnWeight);
    $('#box_volumnWeight').text(box_weight);
    $('#volumnWeight').val(volumnWeight);
    $('#volumnWeight').change();
    get_service()
}

function convert2number(value) {
    var result = value;
    if (value === undefined || isNaN(value) || value === "") {
        result = 0;
    }
    return result;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function get_note_country() {
    var id = $('#sh_country option:selected').attr('code');
    var risk = 0;//$('#sh_country option:selected').attr('risk');
    $.ajax({
        type: "GET",
        url: '/admin/getcountrycontent/' + id,
        success: function(result) {
            var data = $.parseJSON(result);
            $('#note').html(''); 
            if(data[0].note!=''&&data[0].note!=null){
               var str = '<div class="note">'+
            '<div class="text_note">NOTE</div>'+
            '<div class="note_center">'+
            '<div class="text_note_detail">'+data[0].note+
            '</div>'+
            '</div>'+
            '</div>';
            $('#note').html(str); 
            $('#risk').val(data[0].risk_price);
            }
            
        },
        error: function(e) {
            console.log("ERROR : ", e);
        }
    });
    
    get_service();
}

function get_service() {
    $("#loading").show();
    $('#service_select').html('');
    var sh_country = $('#sh_country option:selected');
    var totalweight = $('#volumnWeight').val();
    var url = '/getprice_sale/' + sh_country.attr('code') + '/' + totalweight;
    if ($('#volumnWeight').val() != '' && $('#volumnWeight').val() != 0) {
        $.ajax({
            type: "GET",
            url: url,
            success: function(result) {
                var data = $.parseJSON(result);
                console.log(data);
                var html = '';
                var esc = 0;
                $(data).each(function(i, e) {
                    e.logo = e.logo != null ? e.logo : 'sme.jpg';
                    if(e.price_esc == null){
                        esc = 0;
                    }else{
                        esc = e.price_esc.result;
                    }
                    if (e.price.result != undefined && e.price.result != '') {
                        html +=
                            '<div class="col-md-12" title="' + e.name + '"><input type="radio" name="service" class="radio_service_item" id="s_' +
                            e.id + '" value="' + e.id + '" s_id="' + e.id + '" s_price="' + e.price.result + '" s_logo="' + e.logo + '" p_esc="'+ esc +'" onclick="set_service()">' +
                            '<label class="row service_card" for="s_' + e.id + '">' +
                            '<div style="float: left;padding: 10px;">' +
                            '<img src="/uploads/service/' + e.logo +
                            '" alt="user" height="70px"></div>' +
                            '<div  style="font-size:18px;padding-top: 10px;width:70%"><span class="cut-text">' +
                            e.name + ' </span>( ' + e.day.day + ' วันทำการ )<span style="float:right">' +
                            numberWithCommas(e.price.result) + ' ฿</span></div>' +
                            '</label></div>';
                    }
                });
                $('#service_select').html(html);
                $("#loading").hide();
            },
            error: function(e) {
                console.log("ERROR : ", e);
                $("#loading").hide();
            }
        });
        $('#service_select').show();
        $('#service_wait_data').hide();
    } else {
        $("#loading").hide();
        $('#service_select').hide();
        $('#service_wait_data').show();
    }
}
function set_service() {
    var service = $("input[name='service']:checked");
    $('#service_price').val(service.attr('s_price'));
    $('#service_name').val($(service).parent().find(".cut-text").text());
}
function isNumberKey(evt) {
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function calup(value) {
    var sum_digit = value.toFixed(10);
    var sum_notdigit = Math.floor(value);
    var digit = sum_digit - sum_notdigit;
    var result = value;
    if (digit > 0) {
        if (digit < 0.5) {
            result = sum_notdigit + 0.5;
        }
        else if (digit > 0.5) {
            result = sum_notdigit + 1;
        }
        else {
            result = sum_digit;
        }
    }
    return result;
}
</script>


@endsection