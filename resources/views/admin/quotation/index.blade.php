@extends('layouts.app')
@section('content')
    <script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
    <link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet"/>
    <link href="{{ asset('assets/plugins/select2/select2.min.css" rel="stylesheet' )}}" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css' )}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
          rel="stylesheet"/>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0);">Admin</a></li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0);">Quotation</a>
                        </li>

                    </ol>
                </div>
                <h4 class="page-title">Quotation</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <link href="{{ asset('/assets/css/smeqt_edit_mode.css') }}" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        @page {
                            size: A4;
                            margin: 0;
                        }

                        @media print {

                            html,
                            body {
                                width: 210mm;
                                height: 297mm;
                            }

                            /* ... the rest of the rules ... */
                        }

                        @media (min-width: 576px) {
                            .modal-dialog {
                                max-width: 50%;
                                margin: 1.75rem auto;
                            }
                        }

                        .topic_text {
                            color: #A91D20 !important;
                            font-family: RSU_BOLD, RSU_light, RSU_Regular, SourceSansPro-Regular, "Lucida Sans Unicode", "Lucida Grande", sans-serif;
                            font-size: 18px;
                        }
                        .end_service{
                            border-bottom: 1px !important;
                        }
                        .th_select {
                            color: red;
                            font-weight: "bold";
                        }

                        .en_select {
                            color: red;
                            font-weight: "bold";
                        }

                        .select_tr {
                            cursor: pointer;
                        }

                        .select_tr:hover {
                            color: red;
                        }

                        .select_tr:active {
                            color: red;
                            font-weight: bold;
                        }
                        .center_input {
                            text-align: center;
                        }
                    </style>
                    <title>SME QUOTATION</title>
                    <div id="overlay"></div>
                    <form action="{{url('/admin/quotation_save')}}" method="POST">
                        @csrf
                        <section id="qt-content-form" class="qt-wrapper qt-data-info">
                            <div style="text-align: right; margin-right: 1%;">
                            <span style="">
                                เปลี่ยนภาษาในการส่ง
                            </span>
                                <span class="select_tr translate th_select" id="th"> TH</span> /
                                <span class="select_tr translate" id="en">EN</span>
                                    <input type="hidden" id="translate" name="translate" value="th">

                            </div>
                            <h1>
                                <span key="quotation" class="switch_text topic_text">ใบเสนอราคา</span>
                                <span class="switch_toppic" style="cursor: pointer;">
                                <input type="hidden" id="switch_toppic" value="0">
                                <i class="fas fa-recycle" style=""></i>
                                <span>เปลี่ยนเป็นหัวข้อ</span>
                                <span class="switch_">ใบแจ้งหนี้</span>
                            </span>
                            </h1>
                            <div class="panel-container" id="qtform" style="border:none">
                                <div class="header-th-content">
                                    <img src="{{ asset('/assets/images/logo.png') }}" class="img-responsive">
                                </div>
                                <div class="header-th-content ctrl-inlineblock header-content">
                                    <p class="lang" key="company_address_1">บริษัทเอสเอ็มอี ชิปปิ้ง จำกัด</p>
                                    <p class="lang" key="company_address_2">46-48 ชอยรัชดาภิเษก 16 ถนนรัชดาภิเษก
                                        แขวงวัดท่าพระ</p>
                                    <p class="lang" key="company_address_3">เขตบางกออกใหญ่ กรุงเทพฯ 10600</p>
                                    <p class="lang" key="company_address_4">โทรศัพท์ : 02-105-7777 โทรสาร :
                                        02-105-7778</p>
                                </div>

                                <h6 class="quotation_toppic">ใบเสนอราคา</h6>
                                <input type="hidden" class="quotation_toppic" name="quotation_toppic"
                                       value="ใบเสนอราคา">
                                <div class="customer-info-box rounded-10">
                                    <p class="cust-info-label ctrl-inlineblock lang" key="taxpayer_number">
                                        เลขประจำตัวผู้เสียภาษีอากร</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock"><input type="text" name="customer_id"
                                                                                     style="width:100%"></p>

                                    <p class="cust-info-label ctrl-inlineblock lang" key="company_name">บริษัท</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock"><input type="text" name="company"
                                                                                     style="width:100%"></p>

                                    <p class="cust-info-label ctrl-inlineblock lang" key="customer_name">ลูกค้า</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock"><input type="text" name="name"
                                                                                     style="width:100%" required></p>

                                    <p class="cust-info-label ctrl-inlineblock lang" key="address">ที่อยู่</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock"><input type="text" name="address"
                                                                                     style="width:100%"></p>

                                    <p class="cust-info-label ctrl-inlineblock lang" key="telephone">โทรศัพท์</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock"><input type="text" name="tel"
                                                                                     style="width:100%"></p>

                                    <p class="cust-info-label ctrl-inlineblock lang" key="email">อีเมล์</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock"><input type="email" name="email"
                                                                                     style="width:100%" required>
                                    </p>

                                </div>
                                <div class="quotation-info-box rounded-10">
                                    <p class="cust-info-label ctrl-inlineblock lang" key="date">วันที่</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock">{{ date('d M Y') }}</p>

                                    <p class="cust-info-label ctrl-inlineblock lang" key="quote_no">เลขที่</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock"><input style="width: 100%;" type="text"
                                                                                     name="quote_no" disabled
                                                                                     placeholder="Auto"/>
                                    </p>

                                    <p class="cust-info-label ctrl-inlineblock lang" key="payment_terms">
                                        เงื่อนไขการชำระ</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock" style="padding-top: 5px;">
                                        <select name="payment">
                                            <option selected="" value="Cash">Cash</option>
                                            <option value="Credit">Credit</option>
                                        </select></p>

                                    <p class="cust-info-label ctrl-inlineblock lang" key="valid_time_day">
                                        กำหนดวันยืนยันราคา</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock"><input style="width: 100%;" type="text"
                                                                                     name="valid_day" id="mdate"
                                                                                     value="{{ date('d/m/Y') }}"></p>

                                    <p class="cust-info-label ctrl-inlineblock lang" key="refer">อ้างอิง</p>
                                    <p class="cust-info-colon ctrl-inlineblock">:</p>
                                    <p class="cust-info-cnt ctrl-inlineblock"><input style="width: 100%;" type="text"
                                                                                     name="reference"></p>

                                </div>

                                <p class="cond-th-desc lang"
                                   style="font-size:0.9em !important;padding-left:10px;margin-top:10px;"
                                   key="detail_condition">
                                    บริษัทฯมีความยินดีที่จะเสนอราคาขนส่งแด่ท่านตามราคาและเงื่อนไขที่ระบุไว้ในใบเสนอราคานี้
                                </p>
                                <div style="text-align: center;padding-top: 10px;padding-bottom: 10px;">
                                    <button class="btn btn-primary lang" style="width: 98%;" type="button"
                                            data-toggle="modal"
                                            data-animation="bounce" data-target=".setdiscount" key="add_service">+
                                        เพิ่มบริการ
                                    </button>
                                </div>
                                <table class="table table-bordered" id="tbform">
                                    <thead>
                                    <tr>
                                        <th width="5px">
                                            <p class="lang" key="no">ลำดับ</p>
                                        </th>
                                        <th colspan="2">
                                            <p class="lang" key="description">รายละเอียด</p>
                                        </th>
                                        <th width="84px">
                                            <p class="lang" key="unit">หน่วย</p>
                                        </th>
                                        <th width="104px">
                                            <p class="lang" key="amount">จำนวนเงิน (บาท)</p>
                                        </th>
                                        <th width="104px">
                                            <p class="lang" key="serven_vat">ภาษีมูลค่าเพิ่ม 7%</p>
                                        </th>
                                        <th width="104px">
                                            <p class="lang" key="wht_1">หัก ณ ที่จ่าย 1%</p>
                                        </th>
                                        <th width="104px">
                                            <p class="lang" key="wht_3">หัก ณ ที่จ่าย 1.5%</p>
                                        </th>
                                        <th>
                                            <p class="lang" key="total_amount">จำนวนเงินรวม (บาท)</p>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot id="tfoot">
                                    <tr>
                                        <td colspan="7" rowspan="4" style="text-align:left">
                                            <p class="lang" style="font-weight:700" key="condition">ข้อกำหนด
                                                เงื่อนไขการส่งพัสดุ </p>
                                            <p class="lang" key="condition_1">-
                                                การจัดส่งเป็นการบริการแบบส่งถึงที่อยู่ผู้รับปลายทาง (Door To
                                                Door) ไม่รวมการชำระค่าภาษี
                                                ที่เกิดขึ้นจากศุลกากร</p>
                                            <p class="lang" key="condition_2">- ไม่มีนโยบายรับประกันความพึงพอใจ
                                                ยินดีคืนเงิน สำหรับการส่งพัสดุประเภทนี้ </p>
                                            <p class="lang" key="condition_3">- เงื่อนไข และสัญญาอื่นๆ
                                                ทั้งหมดบนแบบฟอร์มการส่งสินค้า
                                                และข้อกำหนดพื้นฐานของผู้ให้บริการมีผล
                                                บังคับใช้กับการส่งสินค้าชิ้นนี้ </p>
                                            <p class="lang" key="condition_4">- พื้นที่นอกเหนือจากการให้บริการ
                                                อาจมีการค่าบริการบริการเพิ่มเติม
                                                ขึ้นอยู่กับระยะทาง </p>
                                            <p class="lang" key="condition_5">- บริษัท SME SHIPPING จำกัด
                                                ขอสงวนสิทธิ์ในการปรับปรุง แก้ไข
                                                หรือเปลี่ยนแปลงราคารวมถึง
                                                ข้อกำหนด และเงื่อนไขที่แนบมาในที่นี้ได้ตลอดเวลาโดยไม่แจ้งให้ทราบล่วงหน้า
                                            </p>
                                            <p class="lang" key="condition_6">- ค่าภาษีศุลกากร และภาษีมูลค่าเพิ่ม หรือ
                                                ค่าภาษีสินค้าและบริการขึ้นอยู่กับดุลยพินิจของหน่วยงานที่เกี่ยวข้อง
                                                ค่าภาษีใดๆก็ตามที่เกี่ยวข้องจะเป็นหน้าที่ของผู้รับพัสดุในการชำระค่าใช้จ่ายดังกล่าว
                                                นอกจากได้รับคำแนะนำ/ เงื่อนไข/ ข้อแนะนำอื่นๆ จากผู้ส่ง</p>
                                        </td>
                                        <td colspan="1" rowspan="5" class="padding_tb"
                                            style="font-weight:bold;padding-top: 9%;">
                                            <span class="lang" key="total">จำนวนวเงิน</span></td>
                                        <td class="padding_tb" rowspan="5" style="font-weight:bold;padding-top: 9%;"
                                            id="total_price">0
                                        </td>
                                        <input type="hidden" class="total_price" name="total_price" value="0">
                                    </tr>
                                    <!-- <tr>
                                        <td class="padding_tb" colspan="1" style="font-weight:bold"><span class="lang" key="wht">หัก ณ ที่จ่าย</span>
                                        <span> 1%</span>
                                            <input type="hidden" name="withholding" style="width:20px"
                                                onkeypress="return isNumberKey(event)" value="1" onchange="cal()"
                                                onkeypress="cal()">
                                            </td>
                                        <td class="padding_tb" style="font-weight:bold" id="wht_value">0</td>
                                    </tr>
                                    <tr>
                                        <td class="padding_tb" colspan="1" style="font-weight:bold"><span class="lang" key="wht">หัก ณ ที่จ่าย</span>
                                        <span> 3%</span>
                                            <input type="hidden" name="withholding_vat" style="width:20px"
                                                onkeypress="return isNumberKey(event)" value="3" onchange="cal()"
                                                onkeypress="cal()">
                                        </td>
                                        <td class="padding_tb" style="font-weight:bold" id="wht_val">0</td>
                                    </tr> -->
                                    <!-- <tr>
                                        <td class="padding_tb" colspan="1"  style="font-weight:bold">
                                            <span class="lang" key="total">จำนวนเงินรวมทั้งหมด</span>
                                        </td>
                                        <td class="padding_tb" style="font-weight:bold" id="grand_total">0</td>
                                    </tr> -->
                                    </tfoot>

                                </table>


                                <div class="twocolumn qt-approve" style="width:45%;">
                                    <p class="lang" key="approve">อนุมัติสั่งซื้อบริการตามที่ได้เสนอราคา</p>
                                    <p style="margin-top: 20px;" class="lang" key="approved_by">ผู้สั่งซื้อ
                                        __________________________________</p>
                                    <p style="margin-top: 20px;" class="lang" key="position">ตำแหน่ง
                                        ______________________________________</p>
                                </div>


                                <div class="twocolumn qt-sale" style="width:45%;float:right">
                                    <p><span class="lang" key="employee">พนักงานขาย</span> <u
                                            style="font-size:16px; font-weight:100;">{{Auth::user()->name}}</u></p>
                                    <p style="font-size:1.4em !important;margin-bottom: 25px;margin-top: 20px;"
                                       class="lang" key="sincerely">ขอแสดงความนับถือ</p>
                                    <p style="margin-top: 20px;" class="lang" key="sales">หัวหน้าฝ่ายขาย
                                        ____________________________</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </section>
                        <div style="text-align: center;padding-top: 20px;">
                            <button style="width: 78%;" type="submit"
                                    class="btn btn-success waves-effect waves-light bottom_width">บันทึก
                            </button>
                        </div>
                    </form>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <input type="hidden" value="0" id="number_">
        <!-- end col -->
    </div>

    <!--  Modal content for the above example -->
    <div class="modal fade setdiscount" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Add service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <iframe name='select_frame' src='{{url('/admin/mainpickupcost_ifram')}}' width="100%"
                            height="700px"></iframe>
                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-sm btn-primary" id="ifram_add"
                                    style="width:100%">เลือก
                            </button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-sm btn-danger" id="close_modal_discount"
                                    data-dismiss="modal" aria-hidden="true" style="width:100%">Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Modal -->
    <div class="modal fade" id="supplement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" id="close_supplement" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-pane p-3" id="Other" role="tabpanel">
                        <div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true"
                             data-toggle="toast">
                            <div class="toast-header">
                                <i class="dripicons-checklist"></i>
                                <strong class="mr-auto"> :: Other Service Charge<input type="hidden" class="num_other"
                                value=""></strong>
                            </div>
                            <div class="toast-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered mb-0 table-centered" id="other_service_edit">
                                        <thead class="thead-light">
                                        <tr>
                                            <th width="5"></th>
                                            <th>Service</th>
                                            <th width="120">Charge (Baht)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck0"/></td>
                                            <td>Emergency surcharge<input type="hidden" class="other_service_name"
                                                                          value="Emergency surcharge"/></td>
                                            <td style="padding: unset;"><input name="esc" id="esc"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck1"/></td>
                                            <td>International Freight<input type="hidden" class="other_service_name"
                                                                            value="International Freight"/></td>
                                            <td style="padding: unset;"><input name="sh_international_freight"
                                                                               id="sh_international_freight"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck2"/></td>
                                            <td>Permium<input type="hidden" class="other_service_name"
                                                              value="Permium"/></td>
                                            <td style="padding: unset;"><input name="sh_permium"
                                                                               id="sh_permium"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck3"/></td>
                                            <td>Remote area Pickup<input type="hidden" class="other_service_name"
                                                                         value="Remote area Pickup"/></td>
                                            <td style="padding: unset;"><input name="sh_remote_area_pickup"
                                                                               id="sh_remote_area_pickup"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck4"/></td>
                                            <td>Remote area Delivery<input type="hidden" class="other_service_name"
                                                                           value="Remote area Delivery"/></td>
                                            <td style="padding: unset;"><input name="sh_remote_area_delivery"
                                                                               id="sh_remote_area_delivery"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck5"/></td>
                                            <td>Shipment Redirect<input type="hidden" class="other_service_name"
                                                                        value="Shipment Redirect"/></td>
                                            <td style="padding: unset;"><input name="sh_shipment_redirect"
                                                                               id="sh_shipment_redirect"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck6"/></td>
                                            <td>Over Weight Piece<input type="hidden" class="other_service_name"
                                                                        value="Over Weight Piece"/></td>
                                            <td style="padding: unset;"><input name="sh_over_weight_piece"
                                                                               id="sh_over_weight_piece"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck7"/></td>
                                            <td>Over Sized Piece<input type="hidden" class="other_service_name"
                                                                       value="Over Sized Piece"/></td>
                                            <td style="padding: unset;"><input name="sh_over_sized_piece"
                                                                               id="sh_over_sized_piece"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck8"/></td>
                                            <td>Elevated Risk<input type="hidden" class="other_service_name"
                                                                    value="Elevated Risk"/></td>
                                            <td style="padding: unset;"><input name="sh_elevated_risk"
                                                                               id="sh_elevated_risk"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck9"/></td>
                                            <td>Restricted Destination<input type="hidden" class="other_service_name"
                                                                             value="Restricted Destination"/>
                                            </td>
                                            <td style="padding: unset;"><input name="sh_restricted_destination"
                                                                               id="sh_restricted_destination"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck10"/></td>
                                            <td>Change of billing<input type="hidden" class="other_service_name"
                                                                        value="Change of billing"/></td>
                                            <td style="padding: unset;"><input name="sh_change_of_billing"
                                                                               id="sh_change_of_billing"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck11"/></td>
                                            <td>Address correction<input type="hidden" class="other_service_name"
                                                                         value="Address correction"/></td>
                                            <td style="padding: unset;"><input name="sh_address_correction"
                                                                               id="sh_address_correction<"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck12"/></td>
                                            <td>Shipment Insurance<input type="hidden" class="other_service_name"
                                                                         value="Shipment Insurance"/></td>
                                            <td style="padding: unset;"><input name="sh_shipment_insurance"
                                                                               id="sh_shipment_insurance"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="other_service_ck ck13"/></td>
                                            <td>All custom services<input type="hidden" class="other_service_name"
                                                                          value="All custom services"/></td>
                                            <td style="padding: unset;"><input name="sh_all_custom_services"
                                                                               id="sh_all_custom_services"
                                                                               class="form-control other_service_value"
                                                                               onkeypress="return isNumberKey(event)"
                                                                               value="0" disabled/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close_modal_all" class="btn btn-secondary" data-dismiss="modal">Close
                    </button>
                    <button type="button" onclick="add_ck()" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
    <script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
    <script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' )}}"></script>
    <script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}"></script>
    <script src="{{ asset('assets/pages/jquery.forms-advanced.js')}}"></script>
    <script src="{{ asset('assets/js/create_quotation.js?1')}}"></script>
@endsection
