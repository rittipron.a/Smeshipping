@extends('layouts.app')

@section('content')
<link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/select2/select2.min.css" rel="stylesheet' )}}" type="text/css" />
<link href="{{ asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css' )}}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
    rel="stylesheet" />
<style>
    .dtp-buttons {
        background-color: white;
    }

    .mdi {
        font-size: 16px;
    }

    .col-sm-4 {
        padding-top: 5px;
    }

    .table td,
    .table th {
        padding: .25rem;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">{{$title}}</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">{{$title}}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">ค้นหา</h4>
                <form name="search" id="search" accept="#" method="GET">
                    @csrf
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-sm-2">คำค้นหา :</div>
                                <div class="col-sm-4">
                                    <div style="position: absolute;top: 15px;right: 20px;">
                                        <input type="radio" name="keyis" value="tel" checked>เบอร์
                                        <input type="radio" name="keyis" value="name"
                                            {{ app('request')->input('keyis')=='name'?'checked':'' }}>ชื่อลูกค้า
                                        <input type="radio" name="keyis" value="awb"
                                            {{ app('request')->input('keyis')=='awb'?'checked':'' }}>AWB
                                    </div>
                                    <input type="text" name="keyword" class="form-control"
                                        placeholder="เบอร์, ชื่อลูกค้า, AWB"
                                        value="{{ app('request')->input('keyword') }}" id="keyword">
                                </div>
                                <div class="col-sm-2">ค้นหา SO :</div>
                                <div class="col-sm-4">
                                    <input type="text" name="so" class="form-control" placeholder="เลข SO"
                                        value="{{ app('request')->input('so') }}" id="keyword">
                                </div>
                                <div class="col-sm-2">สถานะ :</div>
                                <div class="col-sm-4">
                                    <select name="status" id="status" class="form-control">
                                        <option {{ app('request')->input('status')==''?'selected':'' }} value="">
                                            เลือกสถานะ</option>
                                        <option {{ app('request')->input('status')==1?'selected':'' }} value="1">Create
                                        </option>
                                        <option {{ app('request')->input('status')==2?'selected':'' }} value="2">Pickup
                                        </option>
                                        <option {{ app('request')->input('status')==3?'selected':'' }} value="3">
                                            Calculate</option>
                                        <option {{ app('request')->input('status')==4?'selected':'' }} value="4">Payment
                                        </option>
                                        <option {{ app('request')->input('status')==5?'selected':'' }} value="5">Release
                                        </option>
                                        <option {{ app('request')->input('status')==6?'selected':'' }} value="6">
                                            Complete</option>
                                        <option {{ app('request')->input('status')==99?'selected':'' }} value="99">
                                            Cancel</option>
                                    </select>
                                </div>

                                <div class="col-sm-2">Mess :</div>
                                <div class="col-sm-4">
                                    <select name="driver" id="driver" class="form-control">
                                        <option value="" selected="">- เลือก Messenger. -</option>
                                        @foreach ($mess as $mess_item)
                                        <option {{ app('request')->input('driver')==$mess_item->id?'selected':'' }}
                                            value="{{$mess_item->id}}">{{$mess_item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2">วันที่เข้ารับ :</div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" placeholder="โปรดระบุวันที่"
                                        name="delivery_date" id="sdate"
                                        value="{{ app('request')->input('delivery_date') }}">
                                </div>
                                <div class="col-sm-2">Sale :</div>
                                <div class="col-sm-4">
                                    <select name="sale" id="sale" class="form-control">
                                        <option value="" selected="">- เลือก Sale -</option>
                                        @foreach ($sale as $sale_item)
                                        <option {{ app('request')->input('sale')==$sale_item->id?'selected':'' }}
                                            value="{{$sale_item->id}}">{{$sale_item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-2">เงินรับมาแล้ว :</div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" placeholder="" name="payment_value"
                                        id="payment_value" value="{{ app('request')->input('payment_value') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light"
                                        onclick="$('#loading').show()">Search</button>
                                    <button type="reset"
                                        onclick="window.open('{{url('admin/search_history')}}','_self')"
                                        class="btn btn-info waves-effect waves-light">Clear</button>
                                    <button type="reset"
                                        onclick="window.open('{{str_replace('search_history', 'search_print', url()->full())}}','_blank')"
                                        class="btn btn-success waves-effect waves-light"><i class="fas fa-print"></i>
                                        Print</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered mb-0 table-centered">
                        <thead class="thead-light">
                            <tr>
                                <th width="3">No</th>
                                <th width="150">Service</th>
                                <th width="100">SO</th>
                                <th width="100">AWB</th>
                                <th>Status</th>
                                <th>E-mail</th>
                                <th width="150">Name</th>
                                <th width="200">Address</th>
                                <th>Date</th>
                                <th>ชำระ</th>
                                <th width="100">Contact</th>
                                <th>Note</th>
                                <th>Mess.</th>
                                <th>Sale</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index=>$item)
                            <tr>

                                <td>{{ ($data->currentpage()-1) * $data->perpage() + $index + 1 }}</td>
                                <td><a
                                        href="{{url('/admin/request/'.$item->id)}}">{{$item->service_name!=null?$item->service_name:'ดูรายละเอียด'}}</a>
                                </td>
                                <td><a href="{{url('/admin/request/'.$item->id)}}">
                                        {{count($item->solist)>0?'':$item->so}}
                                        @foreach ($item->solist as $index=>$itemso)
                                        {{$index>0?',':''}}{{$itemso->so}}
                                        @endforeach</a>
                                    </a>
                                </td>
                                <td><a href="{{url('/admin/request/'.$item->id)}}">
                                        @foreach ($item->solist as $index=>$itemso)
                                        {{$index>0?',':''}}{{$itemso->awb}}
                                        @endforeach
                                    </a>
                                </td>
                                <td>
                                    <span
                                        class="badge badge-soft-{{$item->status>100?'purple':($item->status==99?'danger':'success')}}">
                                        {!!$item->status==0?'<i class="mdi mdi-account-clock-outline"></i> Sale':''!!}
                                        {!!$item->status==1?'<i class="mdi mdi-email-plus-outline"></i> Create':''!!}
                                        {!!$item->status==2?'<i class="mdi mdi-car"></i> Pickup':''!!}
                                        {!!$item->status==3?'<i class="mdi mdi-scale"></i> Calculate':''!!}
                                        {!!$item->status==4&&$item->payment_file_re!=null?'<i class="mdi mdi-cash"></i>
                                        Proof':''!!}
                                        {!!$item->status==4&&$item->payment_file_re==null?'<i class="mdi mdi-cash"
                                            style="color:red"></i>
                                        <span style="color:red">Waiting</span>':''!!}
                                        {!!$item->status==5?'<i class="mdi mdi-truck-check"></i> Release':''!!}
                                        {!!$item->status==6?'<i class="mdi mdi-progress-check"></i> Complete':''!!}
                                        {!!$item->status==99?'<i class="mdi mdi-cancel"></i> Cancel':''!!}
                                        {!!$item->status==102?'<i class="mdi mdi-car"></i> Pickup (Hold)':''!!}
                                        {!!$item->status==103?'<i class="mdi mdi-scale"></i> Calculate (Hold)':''!!}
                                        {!!$item->status==104?'<i class="mdi mdi-cash"></i> Payment (Hold)':''!!}
                                        {!!$item->status==105?'<i class="mdi mdi-truck-check"></i> Release
                                        (Hold)':''!!}
                                        {!!$item->status==106?'<i class="mdi mdi-progress-check"></i> Complete
                                        (Hold)':''!!}
                                        @if ($item->payment_file_re!=null)
                                        <img src="{{asset('assets/images/coins/coin.gif')}}" width="25px" />
                                        @endif
                                    </span>
                                </td>
                                <td> <a href="{{url('/admin/request/'.$item->id)}}">
                                        @foreach ($item->maillist as $index=>$itemmail)
                                        {{$index>0?',':''}}{{$itemmail->email}}
                                        @endforeach</a></td>
                                <td><a href="{{url('/admin/request/'.$item->id)}}">
                                        {{$item->name}} {{$item->company}}
                                    </a>
                                    @if ($item->m_id==9999999)
                                    <img src="{{asset('/assets/images/'.'create_users_group_people-512.gif')}}"
                                        width="25px" />
                                    @endif
                                </td>
                                <td><a href="{{url('/admin/request/'.$item->id)}}">{{$item->address}}</a></td>
                                <td><a href="{{url('/admin/request/'.$item->id)}}">{{date('d/m/Y',$item->delivery_date)}}
                                        {{$item->delivery_time}}</a>
                                </td>
                                <td><a href="{{url('/admin/request/'.$item->id)}}">
                                        {{$item->type_pay}} {{$item->pay>0?$item->pay." .-":""}}</a></td>
                                <td><a href="{{url('/admin/request/'.$item->id)}}">
                                        @foreach ($item->phonelist as $index=>$itemphone)
                                        {{$index>0?',':''}}{{$itemphone->phone}}
                                        @endforeach
                                    </a></td>
                                <td><a href="{{url('/admin/request/'.$item->id)}}">{{$item->m_note}}</a></td>
                                <td><a
                                        href="{{url('/admin/request/'.$item->id)}}">{{$item->type_delivery=='walkin'?$item->type_delivery:$item->driver}}</a>
                                </td>
                                <td><a href="{{url('/admin/request/'.$item->id)}}">{{$item->salename}}</a></td>
                            </tr>

                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
            <!--end card-body-->

        </div>
        <!--end card-->
        {{ $data->appends(request()->input())->links() }}
    </div>

    <!-- end col -->
</div>
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}"></script>
<script src="{{ asset('assets/pages/jquery.forms-advanced.js' )}}"></script>
@endsection