@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('assets/dist/jquery.Thailand.min.css') }}">
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script type="text/javascript" src="{{ asset('assets/dependencies/JQL.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('assets/dependencies/typeahead.bundle.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/dist/jquery.Thailand.min.js') }}">
</script>
<style>
    .dtp-select-year-after {
        display: none;
    }

    .row,
    .bottom10 {
        padding-bottom: 10px;
    }

    .col-sm-2 {
        color: black !important;
    }

    .b-title {
        color: black;
        font-weight: 600;
    }

    span {
        color: black;
    }

    .table td {
        vertical-align: top;
    }

    .badge.badge-soft-success {
        font-size: 12px;
        color: black !important;
    }

    .nav-link {
        padding: unset;
    }

    .line-npd {
        padding: unset;
        margin: unset;
        margin-bottom: 5px;
        margin-top: 5px;
    }

    .sum-bottom {
        bottom: 1px;
        color: #078307;
        font-weight: 500;
    }

    .dtp-buttons {
        background-color: white;
    }

    .nav-tabs .nav-link {
        padding: 10px;
    }

    .nav-tabs .nav-item.show .nav-link,
    .nav-tabs .nav-link.active {
        padding: 10px;
    }

    .title-topic {
        padding-top: 10px;
        padding-right: unset;
        font-size: 12px;
    }

    .tt-input {
        background: #ffffff !important;
    }
</style>
<link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/select2/select2.min.css" rel="stylesheet' )}}" type="text/css" />
<link href="{{ asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css' )}}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
    rel="stylesheet" />

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Create Request</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Create Request</h4>
        </div>
    </div>
</div>
<form name="booking" id="booking" action="{{url('/admin/request')}}" method="POST">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">สถานะ</h4>
                    <ol class="c-progress-steps">
                        <li class="c-progress-steps__step  current"><span>Create</span></li>
                        <li class="c-progress-steps__step  "><span>Pickup</span></li>
                        <li class="c-progress-steps__step  "><span>Calculate</span></li>
                        <li class="c-progress-steps__step  "><span>Payment</span></li>
                        <li class="c-progress-steps__step  "><span>Release</span></li>
                        <li class="c-progress-steps__step"><span>Complete</span></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">ข้อมูลลูกค้า</h4>
                    <div class="row">
                        <div class="col-sm-2">Customer :</div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" id="search_customer" name="" class="form-control"
                                    placeholder="E-mail, เบอร์โทร, ชื่อ" autocomplete="new-password">
                                <input type="hidden" id="m_id" name="m_id" />
                                <span class="input-group-append">
                                    <button type="button" class="btn  btn-primary" data-toggle="modal"
                                        data-animation="bounce" data-target=".bs-example-modal-lg"><i
                                            class="dripicons-plus"></i>New</button>
                                </span>
                            </div>

                        </div>

                    </div>
                    <div class="table-responsive" style="background: #fdfdff;">
                        <table class="table mb-0" id="customer_select">
                            <tbody>

                            </tbody>
                        </table>
                        <!--end row-->
                    </div>
                    <div class="row" id="customer_detail">
                        <div class="col-sm-2">Name:</div>
                        <div class="col-sm-4">
                            <p id="customer_name_show"> -</p>
                            <input type="hidden" name="name" id="customer_name_show_" />
                        </div>
                        <div class="col-sm-2">Company:</div>
                        <div class="col-sm-4">
                            <p id="customer_company_show"> - </p>
                            <input type="text" name="company" id="customer_company_show_" class="form-control"
                                autocomplete="new-password" style="margin-bottom: 5px;" />
                        </div>
                        <div class="col-sm-2">Phone:</div>
                        <div class="col-sm-4">
                            <p id="customer_phone_show"> -</p>
                            <input type="hidden" name="phone" id="customer_phone_show_" />
                        </div>
                        <div class="col-sm-2">line:</div>
                        <div class="col-sm-4">
                            <p id="customer_line_show"> -</p>
                            <input type="text" name="line" id="customer_line_show_" class="form-control"
                                autocomplete="new-password" style="margin-bottom: 5px;" />
                        </div>
                        <div class="col-sm-2">E-Mail :</div>
                        <div class="col-sm-4">
                            <p id="customer_email_show"> - </p>
                            <input type="email" name="email" id="customer_email_show_" class="form-control"
                                autocomplete="new-password" style="margin-bottom: 5px;" multiple />
                        </div>
                        <div class="col-sm-2">บันทึก insighly:</div>
                        <div class="col-sm-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="insighly" name="insighly"
                                    data-parsley-multiple="groups" data-parsley-mincheck="2">
                                <label class="custom-control-label" for="insighly"><img
                                        src="{{ asset('/assets/images/in.png') }}" width="24" border="0">
                                    insighly</label>
                            </div>
                        </div>
                        <div class="col-sm-2">Address:</div>
                        <div class="col-sm-10">
                            <input type="text" name="address" id="address" class="form-control" required>
                        </div>

                        <div class="col-sm-2" style="padding-top: 15px;">Sale :</div>
                        <div class="col-sm-4" style="padding-top: 15px;">
                            <select name="sale" id="sale" class="form-control">
                                <option value="" selected="">- เลือก Sale -</option>
                                @foreach ($sale as $sale_item)
                                <option value="{{$sale_item->id}}">
                                    {{$sale_item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <!--end card-->
        </div>

        <!--end col-->
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body" style="padding-bottom: 2px;">
                    <h4 class="mt-0 header-title">ข้อมูลการเข้ารับสินค้า</h4>
                    <div class="row">
                        <div class="col-sm-2">ประเภทงาน :</div>
                        <div class="col-sm-4">
                            <select name="type_work" id="type_work" class="form-control">
                                <option selected="" value="1">งานส่งออก</option>
                                <option value="2">งานบริการลูกค้า</option>
                                <option value="3">งานภายใน</option>

                            </select>
                        </div>
                        <div class="col-sm-2">ประเภทบริการ :</div>
                        <div class="col-sm-4">
                            <select name="type_of_service" id="type_of_service" class="form-control">
                                @foreach ($service_type as $index=>$item)
                                <option {{$index==0?'select':''}} value="{{$item->id}}">[{{$item->code}}]
                                    {{$item->name}}
                                </option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Pickup Service :</div>
                        <div class="col-sm-4">
                            <select name="type_delivery" id="type_delivery" class="form-control">
                                <option selected="" value="ไม่ระบุ">ไม่ระบุ</option>
                                <option value="walkin">Walk in (ลูกค้ามาส่งเอง)</option>
                                <option value="มอเตอร์ไชต์">มอเตอร์ไชต์</option>
                                <option value="รถกระบะ">รถกระบะ</option>
                                <option value="TNT">TNT</option>
                                <option value="ไปรษณียไทย">ไปรษณียไทย</option>
                                <option value="kerry">kerry</option>
                                <option value="Lalamove">Lalamove</option>
                                <option value="อื่นๆ">อื่นๆ</option>
                            </select>
                        </div>
                        <div class="col-sm-2">Tracking(ถ้ามี) :</div>
                        <div class="col-sm-4">
                            <input type="text" name="type_delivery_note" class="form-control"
                                placeholder="ระบุเลขพัสดุ Kerry,TNT,ไปรษณีย์" id="tracking">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">วันที่รับสินค้า :</div>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="โปรดระบุวันที่" name="delivery_date"
                                id="mdate" value="{{date('d/m/Y')}}" required>
                        </div>
                        <div class="col-sm-2">
                            <input type="radio" name="time" value="พร้อมรับ" checked />
                            พร้อมรับ <br />
                            <input type="radio" name="time" value="ระบุเวลา" /> ระบุเวลา
                        </div>
                        <div class="col-sm-4">
                            <select id="delivery_time" name="delivery_time" class="form-control" disabled required>
                                <option value="">เลือกเวลาที่รับ</option>
                                <option value="08:00">08:00</option>
                                <option value="08:30">08:30</option>
                                <option value="09:00">09:00</option>
                                <option value="09:30">09:30</option>
                                <option value="10:00">10:00</option>
                                <option value="10:30">10:30</option>
                                <option value="11:00">11:00</option>
                                <option value="11:30">11:30</option>
                                <option value="12:00">12:00</option>
                                <option value="12:30">12:30</option>
                                <option value="13:00">13:00</option>
                                <option value="13:30">13:30</option>
                                <option value="14:00">14:00</option>
                                <option value="14:30">14:30</option>
                                <option value="15:00">15:00</option>
                                <option value="15:30">15:30</option>
                                <option value="16:00">16:00</option>
                                <option value="16:30">16:30</option>
                                <option value="17:00">17:00</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2" style="
                        padding-right: unset;
                    ">จำนวน Shipment :</div>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" name="shipment" min="1" max="99" required />
                        </div>
                        <div class="col-sm-2">Messenger :</div>
                        <div class="col-sm-4">
                            <select name="driver" id="driver" class="form-control">
                                <option value="" selected="">- เลือก Messenger. -</option>
                                @foreach ($mess as $mess_item)
                                <option value="{{$mess_item->id}}">{{$mess_item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">คำสั่งพิเศษ :</div>
                        <div class="col-sm-4">
                            <textarea name="m_note" class="form-control"></textarea>
                        </div>
                        <div class="col-sm-2">ประเทศปลายทาง :</div>
                        <div class="col-sm-4">
                            <textarea name="type_shipping" class="form-control"> </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <!--end card-->
        </div>
        <!--end col-->
    </div>

    <div class="row">


        <!--end col-->
        <div class="col-lg-5">
            <div class="card">
                <div class="card-body">
                    <h5 class="mt-0 mb-3">Action</h5>
                    <button type="submit" id="save" class="btn btn-primary" disabled>Submit</button>
                    <button type="button" class="btn btn-danger">Cancel</button>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="mt-0 mb-3">Payment</h5>
                    <div class="row">
                        <div class="col-sm-4">รับชำระโดย :</div>
                        <div class="col-sm-8">
                            <select name="payment_type" id="payment_type" class="form-control">
                                <option value="รับเงินสด">รับเงินสด</option>
                                <option selected="" value="โอนเงิน">โอนเงิน</option>
                                <option value="ใช้เครื่องรูดบัตร">ใช้เครื่องรูดบัตร</option>
                                <option value="Credit">Credit</option>
                                <option value="2C2P">2C2P</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">แจ้งให้เก็บตอนรับของ :</div>
                        <div class="col-sm-8">
                            <input type="text" name="payment_value_formess" id="payment_value_formess" value="0"
                                class="form-control" placeholder="จำนวนเงิน" onkeypress="return isNumberKey(event)">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">จำนวนเงินรับมาแล้ว :</div>
                        <div class="col-sm-8">
                            <input type="text" name="payment_value" id="payment_value" value="0" class="form-control"
                                placeholder="จำนวนเงิน" onkeypress="return isNumberKey(event)">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="mt-0 mb-3">History</h5>
                    <div class="activity">
                        <i class="mdi mdi-check text-success"></i>
                        <div class="time-item">
                            <div class="item-info">
                                <div class="text-muted text-right font-10">{{ date('d-m-Y H:i:s') }}</div>
                                <h5 class="mt-0">{{Auth::user()->name}}</h5>
                                <p class="text-muted font-13">Create Request
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end activity-->
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
    </div>
</form>





<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Customer</h5>
                <button type="button" class="close" id="close_modal" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane p-3 active" id="Quick" role="tabpanel">
                            <form name="short_customer" id="short_customer" action="{{url('/admin/add_address')}}"
                                method="POST">
                                <div class="row">
                                    <div class="col-sm-2">ชื่อ - สกุล:</div>
                                    <div class="col-sm-4">
                                        <input type="text" name="name" id="addr_name" class="form-control"
                                            placeholder="" required="" autocomplete="new-password">
                                        <input type="hidden" name="customer_id" id="addr_customer_id"
                                            class="form-control" value="" autocomplete="new-password">
                                        <input type="hidden" name="address_id" id="address_id" class="form-control"
                                            value="" autocomplete="new-password">
                                    </div>
                                    <div class="col-sm-2">หมายเลขโทรศัพท์:</div>
                                    <div class="col-sm-4">
                                        <input type="text" name="phone" id="addr_phone" class="form-control"
                                            placeholder="" required="" autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">อีเมล์:</div>
                                    <div class="col-sm-4">
                                        <input type="email" name="email" id="addr_email" class="form-control"
                                            placeholder="" autocomplete="new-password" multiple>
                                    </div>
                                    <div class="col-sm-2">ที่อยู่:</div>
                                    <div class="col-sm-4">
                                        <input type="text" name="address" id="addr_address" class="form-control"
                                            placeholder="" required="" autocomplete="new-password">
                                    </div>
                                </div>


                                <div id="demo1" class="demo" uk-grid>
                                    <div class="row">
                                        <div class="col-sm-2">ตำบล / แขวง:</div>
                                        <div class="col-sm-4 uk-width-1-2@m">
                                            <input name="tumbon" id="tumbon" class="form-control" type="text"
                                                autocomplete="new-password">
                                        </div>

                                        <div class="col-sm-2">อำเภอ / เขต:</div>
                                        <div class="col-sm-4 uk-width-1-2@m">
                                            <input name="amphur" id="amphur" class="form-control" type="text"
                                                autocomplete="new-password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">จังหวัด:</div>
                                        <div class="col-sm-4 uk-width-1-2@m">
                                            <input name="province" id="province" class="form-control" type="text"
                                                autocomplete="new-password">
                                        </div>

                                        <div class="col-sm-2">รหัสไปรษณีย์:</div>
                                        <div class="col-sm-4 uk-width-1-2@m">
                                            <input name="postcode" id="postcode" class="form-control" type="text"
                                                autocomplete="new-password">
                                        </div>
                                    </div>
                                </div>

                                <script>
                                    $.Thailand({
                                        $district: $('#demo1 [name="tumbon"]'),
                                        $amphoe: $('#demo1 [name="amphur"]'),
                                        $province: $('#demo1 [name="province"]'),
                                        $zipcode: $('#demo1 [name="postcode"]'),

                                        onDataFill: function (data) {
                                            console.info('Data Filled', data);
                                        },

                                        onLoad: function () {
                                            console.info('Autocomplete is ready!');
                                            //$('#loader, .demo').toggle();
                                        }
                                    });

                                </script>

                        </div>
                        <div class="col-ms-6">
                            <button type="submit" class="btn btn-sm btn-primary">บันทึก</button>
                            <button type="button" class="btn btn-sm btn-danger"
                                onclick="close_add_address()">ยกเลิก</button>
                        </div>
                        </form>
                    </div>

                </div>
            </div>


        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Plugins js -->
<!-- jQuery  -->

<script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}"></script>
<script src="{{ asset('assets/pages/jquery.forms-advanced.js' )}}"></script>

<script>
    var data = null;
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#customer_company_show_').hide();
        $('#customer_line_show_').hide();
        $('#customer_email_show_').hide();
        //------------ สร้าง customer ใหม่
        $("#create_customer").submit(function (e) {
            $('#loading').show();
            e.preventDefault();
            var thisform = $(this);
            var url = thisform.attr('action');
            
            $.ajax({
                type: "POST",
                data: $('#create_customer').serialize(), 
                url: url,
                success: function (result) {
                    $('#customer_select').hide();
                    $('#customer_detail').show();
                    $('#loading').hide();
                    $('#close_modal').click();
                    var result_data = JSON.parse(result);
                    $(result_data).each(function(i,res) {
                        if(i==0){
                            var address = (res.address!=null?res.address:'')+
                            (res.tumbon!=null?', '+res.tumbon:'')+
                            (res.amphur!=null?', '+res.amphur:'')+
                            (res.province!=null?', '+res.province:'')+
                            (res.postcode!=null?' '+res.postcode:'');
                            $('#m_id').val(res.id!=null?res.id:'');
                            $('#address').val(address);
                            $('#customer_name_show').text((res.firstname!=null?res.firstname:'') +' '+(res.lastname!=null?res.lastname:''));
                            $('#customer_company_show').text(res.company!=null?res.company:'-');
                            $('#customer_line_show').text(res.line!=null?res.line:'-');
                            $('#customer_phone_show').text(res.phone!=null?res.phone:'-');
                            $('#customer_email_show').text(res.email!=null?res.email:'');
                        }
                    });
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
        });
        //------------ ดึงข้อมูลลูกค้า
        $("#search_customer").keypress(function (e) {
            $('#loading').show();
            $('save').prop( "disabled", true );
            var keywork = $("#search_customer").val();
            $.ajax({
                type: "GET",
                url: '/admin/getCustomer/'+keywork,
                success: function (result) {
                    $('#m_id').val('');
                    $('#customer_select tbody').html("");
                    $('#loading').hide();
                    $('#customer_detail').hide();
                    data = JSON.parse(result);
                    $(data).each(function(index,i) {
                        $('#customer_select').show();
                        
                        $('#customer_select tbody').append('<tr onclick="setcustomer('+index+')" style="cursor: pointer;"><td>ชื่อ: '+i.firstname +' '+ i.lastname+ ' </td><td> Tel: '+i.phone+' </td><td>ที่อยู่: '+i.address+'</td></tr>');
                    });
                    $('#customer_select tbody').append('<tr style="cursor: pointer;"><td colspan="3" style="text-align:center"><a href="#" onclick="close_search_customer()" class="btn btn-soft-pink ">Close</a></td></tr>');
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    $('#loading').hide();
                }
            });
        });
        $("#addr_email").change(function (e) {
            var cus_id = $('#addr_customer_id').val();
            var email = $("#addr_email").val();
           if(cus_id==''){
                $.ajax({
                type: "GET",
                url: '/admin/checkEmailDup/'+email,
                success: function (result) {
                    if(result!='true'){
                        var str = "";
                        $(result.data).each(function(i,e){
                            str +=(i+1)+". ชื่อ:"+e.customer_name+"\n - Email:"+e.email+"\n - เบอร์โทร:"+e.phone+"\n";
                        });
                        alert("ตรวจพบ Email นี้ถูกใช้แล้วในระบบ โดยมีข้อมูลดังนี้ \n"+str+"\n ***โปรดตรวจสอบให้แน่ในว่าลูกค้าคนที่กำลังจะสร้างไม่ซ้ำกับที่มีอยู่แล้วในระบบ!!!");
                    }
                    console.log(result.data);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    $('#loading').hide();
                }
            });
           }
           
        });
        //--------- submit
        $("#booking").submit(function (e) {
            $('#loading').show();
            e.preventDefault();
            var thisform = $(this);
            var url = thisform.attr('action');
            
            $.ajax({
                type: "POST",
                data: $('#booking').serialize(), 
                url: url,
                success: function (result) {
                    $('#loading').hide();
                    window.location.replace("/admin/request/"+result);
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
        });
        $('input[type=radio][name=time]').change(function() {
            if (this.value == 'พร้อมรับ') {
                $('#delivery_time').prop('disabled', true);
            }
            else  {
                $('#delivery_time').prop('disabled', false);
            }
        });

        $("#short_customer").submit(function (e) {
            e.preventDefault();
            var thisform = $(this);
            var url = thisform.attr('action');
            $('#addr_customer_id').val($('#m_id').val());
            $.ajax({
                type: "POST",
                data: $('#short_customer').serialize(), 
                url: url,
                success: function (result) {
                    result = JSON.parse(result);
                    result = result[0];
                    console.log(result);
                    $('#m_id').val(result.customer_id);
                    $('#address').val(result.address+" "+result.tumbon+" "+result.amphur+' '+result.province +' '+ result.postcode);
                    $('#customer_name_show').text(result.customer_name);
                    $('#customer_company_show').text('-').hide();
                    $('#customer_line_show').text('-').hide();
                    $('#customer_phone_show').text(result.phone);
                    $('#customer_email_show').text('-').hide();

                    $('#customer_name_show_').val(result.customer_name);
                    $('#customer_company_show_').val("").show();
                    $('#customer_line_show_').val("").show();
                    $('#customer_email_show_').val(result.email!=null?result.email:'').show();
                    $('#customer_phone_show_').val(result.phone);
                    $('#search_customer').val(result.customer_id);
                    $('#save').prop( "disabled", false );
                    $('#close_modal').click();
                },
                error: function (e) {
                    console.log("ERROR : ", e);
                    alert("พบเบอร์โทร ซ้ำกับของเดิมในระบบ โปรดเช็คว่ามีการสมัครสมาชิกอยู่ก่อนแล้วหรือไม่ หรือติดต่อผู้ดูแลระบบ");
                    $('#loading').hide();
                }
            });
        });
    });
    function setcustomer(index){
        $('#customer_select').hide();
        $('#save').prop( "disabled", false );
        $('#customer_detail').show();
        $(data).each(function(i,res) {
            if(index==i){
                var address = (res.address!=null?res.address:'')+
                (res.tumbon!=null?', '+res.tumbon:'')+
                (res.amphur!=null?', '+res.amphur:'')+
                (res.province!=null?', '+res.province:'')+
                (res.postcode!=null?' '+res.postcode:'');
                $('#m_id').val(res.id!=null?res.id:'');
                $('#address').val(address);
                $('#customer_name_show').text((res.firstname!=null?res.firstname:'') +' '+(res.lastname!=null?res.lastname:''));
                $('#customer_company_show').text(res.company!=null?res.company:'-').hide();
                $('#customer_line_show').text(res.line!=null?res.line:'-').hide();
                $('#customer_phone_show').text(res.phone!=null?res.phone:'-');
                $('#customer_email_show').text(res.email!=null?res.email:'-').hide();

                $('#customer_name_show_').val((res.firstname!=null?res.firstname:'') +' '+(res.lastname!=null?res.lastname:''));
                $('#customer_company_show_').val(res.company!=null?res.company:'').show();
                $('#customer_line_show_').val(res.line!=null?res.line:'').show();
                $('#customer_phone_show_').val(res.phone!=null?res.phone:'');
                $('#customer_email_show_').val(res.email!=null?res.email:'').show();
                $('#search_customer').val(res.id);

                $('#addr_name').val((res.firstname!=null?res.firstname:'') +' '+(res.lastname!=null?res.lastname:''));
                $('#customer_id').val(res.id);
                $('#addr_phone').val(res.phone!=null?res.phone:'');
                $('#addr_address').val(res.address!=null?res.address:'');
                $('#province').val(res.province!=null?res.province:'');
                $('#amphur').val(res.amphur!=null?res.amphur:'');
                $('#tumbon').val(res.tumbon!=null?res.tumbon:'');
                $('#postcode').val(res.postcode!=null?' '+res.postcode:'');
                $('#addr_email').val(res.email!=null?' '+res.email:'');
                


            }
        });
        
    } //-------------- ห้ามใส่ตัวอักษร
        function isNumberKey(evt)
            {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31 
                && (charCode < 48 || charCode > 57))
                return false;
                return true;
            }  
        function close_search_customer(){
            $('#customer_detail').show();
            $('#customer_select').hide();
            $('#search_customer').val('');
        }
</script>
@endsection