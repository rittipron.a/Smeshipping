@extends('layouts.app')

@section('content')
<?php $sum_weight = 0;$sum_package = $data->total_shipment;$total_price = 0; ?>
<style>
    .f_left {
        float: left;
    }

    .row {
        padding-bottom: 10px;
    }

    .col-sm-2 {
        color: black !important;
    }

    .b-title {
        color: black;
        font-weight: 600;
    }

    span {
        color: black;
    }

    .table td {
        vertical-align: top;
    }

    .badge.badge-soft-success {
        font-size: 12px;
        color: black !important;
    }

    .nav-link {
        padding: unset;
    }

    .line-npd {
        padding: unset;
        margin: unset;
        margin-bottom: 5px;
        margin-top: 5px;
    }

    .sum-bottom {
        bottom: 1px;
        color: #078307;
        font-weight: 500;
    }

    .nav-tabs .nav-link {
        padding: 10px;
    }

    .nav-tabs .nav-item.show .nav-link,
    .nav-tabs .nav-link.active {
        padding: 10px;
    }

    .title-topic {
        padding-top: 10px;
        padding-right: unset;
        font-size: 12px;
    }

    .other_service,
    .other_service_value {
        text-align: right;
    }

    .sh-input {
        margin-top: 6px
    }

    .sh_tracking_code {
        font-size: 18px;
        font-weight: 600;
        color: #860303;
    }

    .remove_inv {
        font-size: 18px;
        color: red;
        cursor: pointer;
    }
</style>
<link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/select2/select2.min.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/css/style.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
    rel="stylesheet" />
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">View Request</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Booking ID:{{$data->id}} {{$data->so!=null?'/ SO: '.$data->so:''}}</h4>
        </div>
    </div>
</div>
<form name="booking" id="booking" action="{{url('/admin/request/'.$data->id)}}" method="POST">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4>Status</h4>
                    <ol class="c-progress-steps">
                        <li
                            class="c-progress-steps__step  {{($data->request_status>=1&&$data->request_status<=100)||$data->request_status==101?'current':''}}">
                            <span>Create{{($data->request_status>100)&&($data->request_status==101)?' (Hold)':''}}</span>
                        </li>
                        <li
                            class="c-progress-steps__step  {{($data->request_status>=2&&$data->request_status<=100)||$data->request_status==102?'current':''}}">
                            <span>Pickup{{($data->request_status>100)&&($data->request_status==102)?' (Hold)':''}}</span>
                        </li>
                        <li
                            class="c-progress-steps__step  {{($data->request_status>=3&&$data->request_status<=100)||$data->request_status==103?'current':''}}">
                            <span>Calculate{{($data->request_status>100)&&($data->request_status==103)?' (Hold)':''}}</span>
                        </li>
                        <li
                            class="c-progress-steps__step  {{($data->request_status>=4&&$data->request_status<=100)||$data->request_status==104?'current':''}}">
                            <span>Payment{{($data->request_status>100)&&($data->request_status==104)?' (Hold)':''}}</span>
                        </li>
                        <li
                            class="c-progress-steps__step  {{($data->request_status>=5&&$data->request_status<=100)||$data->request_status==105?'current':''}}">
                            <span>Release{{($data->request_status>100)&&($data->request_status==105)?' (Hold)':''}}</span>
                        </li>
                        <li
                            class="c-progress-steps__step {{($data->request_status>=6&&$data->request_status<=100)||$data->request_status==106?'current':''}}">
                            <span>Complete{{($data->request_status>100)&&($data->request_status==106)?' (Hold)':''}}</span>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body" style=" padding-bottom: 15px;">
                    <h4 class="mt-0 header-title">ข้อมูลลูกค้า
                        @if($data->m_id==9999999)
                        <a href="{{url('/admin/customer')}}" target="_blank">
                            <img src="{{asset('/assets/images/'.'create_users_group_people-512.gif')}}" width="30px" />
                        </a>
                        @endif
                    </h4>
                    <button type="button" class="btn btn-info btn-square btn-outline-dashed waves-effect waves-light"
                        style="position: absolute; right: 0;top: 0;" data-toggle="modal" data-animation="bounce"
                        data-target=".edit_customer" {{$data->request_status>100?'disabled':''}}>Edit</button>
                    <div class="row">
                        <div class="col-sm-2 customer_data" cus_id="{{$data->m_id}}"
                            cus_status="{{$data->request_status}}">Name:</div>
                        <div class="col-sm-4">
                            {{$customer->firstname}} {{$customer->lastname}}
                        </div>
                        <div class="col-sm-2">Company:</div>
                        <div class="col-sm-4">
                            {{$customer->company!=''?$customer->company:'-'}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Mobile:</div>
                        <div class="col-sm-4">
                            <a href="tel:{{$customer->phone}}">{{$customer->phone!=''?$customer->phone:'-'}}</a>
                        </div>
                        <div class="col-sm-2">Line ID:</div>
                        <div class="col-sm-4">
                            {{$customer->line!=''?$customer->line:'-'}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">E-Mail :</div>
                        <div class="col-sm-4">
                            {{$customer->email!=''?$customer->email:'-'}}
                        </div>
                        <div class="col-sm-2">บันทึก insighly:</div>
                        <div class="col-sm-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="insighly" name="insighly"
                                    {{$data->insighly=='yes'?"checked":""}} data-parsley-multiple="groups"
                                    data-parsley-mincheck="2">
                                <label class="custom-control-label" for="insighly"><img
                                        src="{{ asset('/assets/images/in.png') }}" width="24" border="0">
                                    insighly</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Address:</div>
                        <div class="col-sm-10">
                            {{$data->address!=''?$data->address:''}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Sale :</div>
                        <div class="col-sm-4">
                            {{$data->salename!=null?$data->salename:'-'}}
                        </div>
                        @if($customer->note!=null)
                        <div class="col-sm-2">คำสั่งพิเศษ :</div>
                        <div class="col-sm-4" style="color:red">
                            {{$customer->note}}
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            <!--end card-->
        </div>


        <!--end col-->
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">ข้อมูลการเข้ารับสินค้า</h4>
                    <button type="button" class="btn btn-info btn-square btn-outline-dashed waves-effect waves-light"
                        style="position: absolute; right: 0;top: 0;" data-toggle="modal" data-animation="bounce"
                        data-target=".edit_pickup" {{$data->request_status>100?'disabled':''}}>Edit</button>
                    <div class="row">
                        <div class="col-sm-2">ประเภทงาน :</div>
                        <div class="col-sm-4">
                            {{$data->type_work==1?'งานส่งออก':''}}
                            {{$data->type_work==2?'งานบริการลูกค้า':''}}
                            {{$data->type_work==3?'งานภายใน':''}}
                        </div>
                        <div class="col-sm-2">ประเภทบริการ :</div>
                        <div class="col-sm-4">
                            [{{$service_type[0]->code}}] {{$service_type[0]->name}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Pickup Service :</div>
                        <div class="col-sm-4">
                            {{$data->type_delivery}}
                        </div>
                        <div class="col-sm-2">Tracking(ถ้ามี) :</div>
                        <div class="col-sm-4">
                            {{$data->type_delivery_note!=''?$data->type_delivery_note:'-'}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">วันที่รับสินค้า :</div>
                        <div class="col-sm-4">
                            {{date('d/m/Y',$data->delivery_date)  }}
                        </div>
                        <div class="col-sm-2">
                            เวลาเข้ารับ :
                        </div>
                        <div class="col-sm-4">
                            {{$data->delivery_time}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2" style="
                        padding-right: unset;
                    ">จำนวน Shipment :</div>
                        <div class="col-sm-4">
                            {{$data->shipment}}
                        </div>
                        <div class="col-sm-2">Messenger :</div>
                        <div class="col-sm-4">
                            @if($data->request_status==1) <select name="driver" id="driver"
                                class="form-control form-control-danger" required>
                                <option value="" {{$data->driver==''?'selected':''}}>- เลือก Messenger. -</option>
                                @foreach ($mess as $mess_item)

                                <option value="{{$mess_item->id}}" {{$data->driver==$mess_item->id?'selected':''}}>
                                    {{$mess_item->name}}
                                </option>
                                @endforeach
                            </select>
                            @else
                            @foreach ($mess as $mess_item)
                            {{$data->driver==$mess_item->id?$mess_item->name:''}}
                            @endforeach

                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">คำสั่งพิเศษ :</div>
                        <div class="col-sm-4">
                            {{$data->m_note!=''?$data->m_note:'-'}}
                        </div>
                        <div class="col-sm-2">ประเทศปลายทาง :</div>
                        <div class="col-sm-4">
                            {{$data->type_shipping!=''?$data->type_shipping:'-'}}
                        </div>
                    </div>

                </div>
            </div>
            <!--end card-->
        </div>
        <!--end col-->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 mb-3 header-title">All Shipment</h4>
                    <div class="row">
                        <div class="col-sm-6">
                            <button type="button" id="add_sh" class="btn btn-light waves-effect waves-light"
                                style="width:100%" data-toggle="modal" data-animation="bounce"
                                data-target=".addshipment">+
                                Add Shipment</button>

                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <select name="sh_type_of_service_excel" id="sh_type_of_service_excel"
                                        class="form-control">
                                        @foreach ($service_all as $index=>$item)
                                        <option {{1==$service_type[0]->id?'selected':''}} value="{{$item->id}}"
                                            routing="{{$item->routing_id}}" cal="{{$item->cal_weight}}">
                                            {{$item->description}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="col-sm-6"><input type="file" id="my_file_input" class="form-control"
                                        id="upload_price" accept=".xls">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" id="table_size">
                            <div id="table_view" style="overflow-x:auto;">
                                <table class="table table-bordered mb-0 table-centered" id="shipment_detail"
                                    style="width:1500px;display:none">
                                    <thead class="thead-light">
                                        <tr>
                                            <th width="3px">No</th>
                                            <th width="250px">Information</th>
                                            <th width="180px">Shipment</th>
                                            <th width="200px">Recipient</th>
                                            <th width="180px">Other Service</th>
                                            <th width="180px">Volume(KG)</th>
                                            <th width="170px">CHARGE</th>
                                            <th width="2px"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {!!count($shipment)==0?'<tr id="message-notdata">
                                            <td colspan="7" align="center" style="color:red">ยังไม่มีข้อมูล Shipment
                                            <td>
                                        </tr>':''!!}
                                        @foreach ($shipment as $key=>$item)

                                        <tr id="{{$item->tracking_code}}">
                                            <td>{{$key+1}} </td>
                                            <td>
                                                <span class="b-title sh_id" set="{{$item->id}}">SERVICE:</span>
                                                <span class="sh_type_of_service"
                                                    set="{{$item->services}}">{{$item->service_name}}</span><br />
                                                <span class="b-title">Insurance:</span> <span class="sh_insurance"
                                                    set="{{$item->insurance}}">{{$item->insurance}}</span><br />
                                                <span class="b-title">Account Number:</span> <span
                                                    class="sh_account_number"
                                                    set="{{$item->account_number}}">{{$item->account_number}}</span><br />
                                                <span class="b-title">SO:</span> <span class="sh_so"
                                                    set="{{$item->so}}">{{$item->so}}</span><br />
                                                <span class="b-title">C/O:</span> <span class="sh_co"
                                                    set="{{$item->co!=null?$item->co:$customer->firstname}}">{{$item->co!=null?$item->co:$customer->firstname}}</span><br />
                                                <span class="b-title">AWB:</span> <span class="sh_awb"
                                                    set="{{$item->awb}}">{{$item->awb}}</span><span
                                                    class="sh_awb_download">
                                                    @if($item->awb!=''&&$item->awb_file!=''?$item->awb_file:'')
                                                    <a href="{{url('/uploads/labels/'.$item->awb_file)}}"
                                                        target="_blank"><i class="mdi mdi-file-pdf"></i></a>
                                                    @endif</span><br />
                                                <span class="b-title">Routing:</span>
                                                <span class="sh_routing_id"
                                                    set="{{$item->routing_id}}">{{$item->routing_id}}</span><br />
                                                <span class="b-title">Cal:</span>
                                                <span class="sh_cal"
                                                    set="{{$item->cal_weight=='up'?'up':''}}">{{$item->cal_weight=='up'?'Up':'Normal'}}</span><br />

                                                <span class="b-title">Tracking DHL:</span>
                                                <span class="sh_tracking_DHL"
                                                    set="{{$item->tracking_dhl}}">{{$item->tracking_dhl}}</span>
                                                <br />
                                                <span class="b-title">Tracking LOCAL:</span>
                                                <span class="sh_tracking_local"
                                                    set="{{$item->tracking_local}}">{{$item->tracking_local}}</span>
                                                <br />
                                                <span class="b-title">Tracking SME:</span>
                                                <span class="sh_tracking_code"
                                                    set="{{$item->tracking_code}}">{{strlen($item->tracking_code)==7?substr($item->tracking_code,0,2).' '.substr($item->tracking_code,2,2).' '.substr($item->tracking_code,4,3):$item->tracking_code}}</span>

                                                <i class="mdi mdi-printer" style="font-size:20px;color:black"
                                                    onclick="printsmelabel(this)" action="{{url('/smelabel')}}"></i>

                                                <br />
                                                @if(empty($item->awb))
                                                <div id="gen_awb">
                                                    <!--<button type="button" class="btn btn-outline-light"
                                                        onclick="gen_dhlexpress(this)">Gen AWB
                                                        Express</button>
                                                    <button type="button" class="btn btn-outline-light">Gen AWB Air
                                                        Mail</button> -->
                                                    <div class="btn-group mb-4">
                                                        <button type="button"
                                                            class="btn btn-outline-warning dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">Generate AWB <i
                                                                class="mdi mdi-chevron-down"></i></button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="javascript:void(0)"
                                                                onclick="gen_dhlexpress(this)">DHL
                                                                Express</a>
                                                            <a class="dropdown-item" href="javascript:void(0)">DHL Air
                                                                Mail</a>
                                                            <a class="dropdown-item" href="javascript:void(0)">TNT
                                                                Express</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            </td>
                                            <td>

                                                <span class="b-title">Shipping Type:</span>
                                                <span class="sh_shipping_type"
                                                    set="{{$item->shipping_type}}">{{$item->shipping_type}}</span><br />
                                                <span class="b-title">Currency:</span>
                                                <span class="sh_currency"
                                                    set="{{$item->currency}}">{{$item->currency}}</span><br />
                                                <span class="b-title">Declared Value:</span>
                                                <span class="sh_declared_value"
                                                    set="{{$item->declared_value}}">{{$item->declared_value}}</span><br />
                                                <span class="b-title">Description:</span>
                                                <span class="sh_description"
                                                    set="{{$item->description}}">{{$item->description}}</span><br />
                                                <span class="b-title">Content Quantity:</span>
                                                <span class="sh_content_quantity"
                                                    set="{{$item->content_quantity}}">{{$item->content_quantity}}</span><br />
                                                <span class="b-title">Note:</span>
                                                <span class="sh_note" set="{{$item->note}}">{{$item->note}}</span><br />
                                                <span class="b-title">Packge Type:</span>
                                                <span class="sh_packge_type"
                                                    set="{{$item->packge_type}}">{{$item->packge_type}}</span><br />
                                                <div class="inv_{{$item->id}}" ship set="{{$item->invoice_file}}">
                                                    @if($item->invoice_file!=null)
                                                    <a href="{{url('/uploads/invoice/'.$item->invoice_file)}}"
                                                        target="_blank"> <i class="mdi mdi-file"></i>View Invoice</a>
                                                    <span aria-hidden="true" class="remove_inv"
                                                        onclick="reset_inv({{$item->id}})">×</span>
                                                    @else
                                                    <label for="file-input{{$item->id}}"
                                                        class="btn btn-outline-danger waves-effect waves-light">
                                                        <i class="mdi mdi-file" style="font-size:18px"></i>Commercial
                                                        Invoice
                                                    </label>
                                                    <input id="file-input{{$item->id}}" type="file"
                                                        accept="application/pdf" style="display:none"
                                                        onchange="upload_inv({{$item->id}})" />
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <span class="b-title">Name:</span>
                                                <span class="sh_recipient"
                                                    set="{{$item->recipient}}">{{$item->recipient}}</span><br />
                                                <span class="b-title">Company:</span>
                                                <span class="sh_company"
                                                    set="{{$item->company}}">{{$item->company}}</span><br />
                                                <span class="b-title">E-Mail:</span>
                                                <span class="sh_email"
                                                    set="{{$item->email}}">{{$item->email}}</span><br />
                                                <span class="b-title">Address1:</span>
                                                <span class="sh_address1"
                                                    set="{{$item->address}}">{{$item->address}}</span><br />
                                                <span class="b-title">Address2:</span>
                                                <span class="sh_address2"
                                                    set="{{$item->address2}}">{{$item->address2}}</span><br />
                                                <span class="b-title">Address3:</span>
                                                <span class="sh_address3"
                                                    set="{{$item->address3}}">{{$item->address3}}</span><br />
                                                <span class="b-title">Phone Number:</span>
                                                <span class="sh_phone"
                                                    set="{{$item->phone}}">{{$item->phone}}</span><br />
                                                <span class="b-title">Country:</span>
                                                <span class="sh_country"
                                                    set="{{$item->country_name!=''?$item->country_name:$item->country}}">
                                                    {{$item->country_name!=''?$item->country_name:$item->country}}
                                                </span><br />
                                                <span class="b-title">Country Code:</span>
                                                <span class="sh_country_code"
                                                    set="{{$item->country_code}}">{{$item->country_code}}</span><br />
                                                <span class="b-title">City:</span>
                                                <span class="sh_city" set="{{$item->city}}">{{$item->city}}</span><br />
                                                <span class="b-title">Suburb:</span>
                                                <span class="sh_suburb"
                                                    set="{{$item->suburb}}">{{$item->suburb}}</span><br />
                                                <span class="b-title">State:</span>
                                                <span class="sh_state" set="{{$item->state}}">{{$item->state}}
                                                </span><br />
                                                <span class="b-title">Postal Code:</span>
                                                <span class="sh_postcode" set="{{$item->zipcode}}">{{$item->zipcode}}
                                                </span><br />
                                            </td>
                                            <td>
                                                @foreach ($item->other_service_name as $key=>$other_item)
                                                <span class="b-title">{{$other_item}}</span>
                                                <span class="sh_other_service" set="{{$other_item}}"
                                                    price="{{$item->other_service_price[$key]}}">{{number_format($item->other_service_price[$key],2)}}</span>
                                                <hr class="line-npd" />
                                                @endforeach
                                                <div class="sum-bottom">Total Amount:
                                                    {{number_format($item->other_service_total_price,2)}} ฿
                                                </div>
                                            </td>
                                            <td>
                                                <?php $box_key=1;$package=0; ?>
                                                @foreach ($box as $keyitem=>$boxitem)
                                                @if ($boxitem->shipment_id === $item->id)

                                                <span class="b-title box" weight="{{$boxitem->weight}}"
                                                    width="{{$boxitem->width}}" length="{{$boxitem->length}}"
                                                    height="{{$boxitem->height}}" series="{{$box_key}}"
                                                    status="{{$boxitem->status}}">{{$box_key++}}:</span>

                                                <span> {{$boxitem->weight}} KG //
                                                    {{$boxitem->width}}x{{$boxitem->length}}x{{$boxitem->height}} =
                                                    {{calup($boxitem->weight>$boxitem->sum_weight?$boxitem->weight:$boxitem->sum_weight)}}
                                                    KG
                                                </span>

                                                <hr class="line-npd" />
                                                <?php $package = $package+1; ?>
                                                @endif
                                                @endforeach
                                                <?php $package > 0? $sum_package=$sum_package+$package-1:$sum_package=$sum_package; ?>
                                                @if (isset($item->total_sum_weight))
                                                <div class="sum-bottom">
                                                    Total Weight : <span class="total_weight"
                                                        set="{{$item->total_sum_weight}}">{{$item->total_sum_weight}}</span>
                                                    KG.
                                                </div>
                                                <?php $sum_weight =$sum_weight+ $item->total_sum_weight;?>
                                                @endif
                                            </td>
                                            <td>
                                                <span class="b-title">Regular:</span><span class="total_regular_item"
                                                    set="{{$item->price}}">
                                                    {{number_format($item->price,2)}}</span><br />
                                                <span class="b-title">Other
                                                    Service:</span><span class="total_other_service_item"
                                                    set="{{$item->other_service_total_price}}">
                                                    {{number_format($item->other_service_total_price,2)}}</span><br />
                                                <div class="sum-bottom total_charge_item"
                                                    set="{{$item->price+$item->other_service_total_price}}">Total
                                                    Charge:
                                                    {{number_format($item->price+$item->other_service_total_price,2)}} ฿
                                                    <?php $total_price = $total_price+$item->price+$item->other_service_total_price;?>
                                                </div>
                                                <div class="box-status">
                                                    @if ($item->status>=10)
                                                    <img src="{{asset('assets/images/return_box.webp' )}}"
                                                        width="100%" />
                                                    @endif

                                                </div>
                                            </td>
                                            <td>
                                                <div class="dropdown d-inline-block float-right">
                                                    <a class="nav-link dropdown-toggle arrow-none" id="dLabel8"
                                                        data-toggle="dropdown" href="#" role="button"
                                                        aria-haspopup="false" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v font-20 text-muted"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                        aria-labelledby="dLabel8">
                                                        <a class="dropdown-item" onclick="copy_sh($(this))">Copy</a>
                                                        <a class="dropdown-item" onclick="edit_sh($(this))">Edit</a>
                                                        <a class="dropdown-item" onclick="delete_sh($(this))">Delete</a>
                                                        <a class="dropdown-item" onclick="return_sh($(this))">Return</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!--end table-responsive-->
                        </div>
                        <!--end col-->

                    </div>
                    <!--end row-->
                    <hr class="line-npd" />
                    <div class="row">
                        <div class="col-sm-2 b-title">TOTAL PACKAGE:</div>
                        <div class="col-sm-2"><span id="total_package">{{$sum_package}}</span> PACKAGE</div>
                        <div class="col-sm-2 b-title">PICKUP FEE:</div>
                        <div class="col-sm-2"><span id="total_pickup_fee">test</span>
                            THB</div>
                        <div class="col-sm-2 b-title">GRAND TOTAL:</div>
                        <div class="col-sm-2" style="color:green !important;font-weight:500"><span id="grand_total"
                                style="color:green !important;font-weight:500">
                                {{number_format($total_price-$data->discount,2)}}</span> THB</div>
                    </div>
                    <hr class="line-npd" />
                    <div class="row">
                        <div class="col-sm-2 b-title">TOTAL SHIPMENT:</div>
                        <div class="col-sm-2"><span id="total_shipment">{{$data->total_shipment}}</span> SHIPMENT</div>
                        <div class="col-sm-2 b-title" style="color:red" data-toggle="modal" data-animation="bounce"
                            data-target=".setdiscount">DISCOUNT:</div>
                        <div class="col-sm-2" data-toggle="modal" data-animation="bounce" data-target=".setdiscount"
                            style="cursor:pointer">
                            <span id="total_discount" style="color:red">{{number_format($data->discount,2)}}</span>
                            <span style="color:red">THB</span>

                        </div>
                    </div>
                    <hr class="line-npd" />
                    <div class="row">
                        <div class="col-sm-2 b-title">TOTAL WEIGHT:</div>
                        <div class="col-sm-2"><span id="total_weight">{{number_format($sum_weight,2)}}</span> KG</div>
                        <div class="col-sm-2 b-title">TOTAL:</div>
                        <div class="col-sm-2"><span id="total_amount">{{number_format($total_price,2)}}</span> THB</div>
                    </div>
                    <hr class="line-npd" />
                </div>
                <div id="data_binding" action="{{url('/admin/update_shipment/'.$data->id)}}">

                </div>
                <button type="button" id="save_shipment" style="display:none"
                    class="btn btn-dark btn-lg btn-block">Save</button>

                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!--end col-->

        <!--end col-->

        <div class="col-lg-3">

            <div class="card">
                <div class="card-body">
                    <h5 class="mt-0 mb-3">Action</h5>
                    <div class="row">
                        <div class="col-sm-3">Note:</div>
                        <div class="col-sm-9"><input type="text" name="comment" id="comment" value=""
                                class="form-control" placeholder="ระบุข้อความ">
                        </div>
                        <div class="col-sm-12" style="padding-top:15px;">
                            <button type="submit" onclick="$(this).hide()" class="btn btn-primary"
                                @if(($data->request_status>=99)||
                                ($data->request_status==2)||
                                ($data->request_status==0&&Auth::user()->position!='Sale')||
                                ($data->request_status==4&&Auth::user()->position!='Operation')||
                                ($data->request_status==5&&Auth::user()->position!='Operation')
                                && Auth::user()->position!='Administrator')
                                disabled
                                @endif
                                >Submit</button>
                            <button type="button" id="save" action="{{url("/admin/update_save/".$data->id)}}"
                                class="btn btn-success waves-effect waves-light">Save</button>
                            @if($data->request_status!=0)
                            <button type="button" id="hold" action="{{url("/admin/update_hold/".$data->id)}}"
                                class="btn btn-info waves-effect waves-light"
                                {{$data->request_status==99?'disabled':''}}>{{$data->request_status>100?'Unhold':'Hold'}}</button>
                            @endif
                            @if($data->request_status!=0)
                            <button type="button" id="complete" action="{{url("/admin/update_complete/".$data->id)}}"
                                class="btn btn-success waves-effect waves-light">Complete</button>
                            @endif
                            <button type="button" id="cancel" action="{{url("/admin/update_cancel/".$data->id)}}"
                                class="btn btn-danger">{{$data->request_status==99?'Restore':'Cancel'}}</button></div>
                    </div>
                </div>
            </div>
        </div>

        @if(count($so)>=1)
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body" style="height: 260px;overflow-y: scroll;">
                    <h5 class="mt-0 mb-3">Attachment</h5>
                    @foreach ($so as $so_item)
                    <div class="row text-left bg-light p-3 mb-3">
                        <div class="col-sm-8">{{$so_item->so_note!=null?$so_item->so_note:'ไม่ได้ระบุข้อมูล'}}</div>
                        <div class="col-sm-4">
                            <a href="{{asset('/uploads/so').'/'.$so_item->so_image}}" target="_blank"><img
                                    src="{{asset('/uploads/so').'/'.$so_item->so_image}}" width="50"
                                    class="img-thumbnail" /></a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body" style="">
                    <h5 class="mt-0 mb-3">Payment</h5>
                    <div class="row">
                        <div class="col-sm-4">รับชำระโดย :</div>
                        <div class="col-sm-8">
                            <select name="payment_type" id="payment_type" class="form-control">
                                <option {{($data->type_pay=="รับเงินสด"?"selected":"")}} value="รับเงินสด">รับเงินสด
                                </option>
                                <option {{($data->type_pay=="โอนเงิน"?"selected":"")}} value="โอนเงิน" value="โอนเงิน">
                                    โอนเงิน</option>
                                <option {{($data->type_pay=="ใช้เครื่องรูดบัตร"?"selected":"")}}
                                    value="ใช้เครื่องรูดบัตร">ใช้เครื่องรูดบัตร</option>
                                <option {{($data->type_pay=="Credit"?"selected":"")}} value="Credit">Credit
                                </option>
                                <option {{($data->type_pay=="2C2P"?"selected":"")}} value="2C2P">2C2P
                                </option>
                            </select>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">แจ้งให้เก็บตอนรับของ :</div>
                        <div class="col-sm-8">
                            <input type="text" name="payment_value_formess" id="payment_value_formess"
                                value="{{$data->pay}}" class="form-control" placeholder="จำนวนเงิน"
                                onkeypress="return isNumberKey(event)">
                            <input type="hidden" name="payment_status" id="payment_status"
                                value="{{number_format($total_price-$data->discount,2)>=number_format($data->price_pay,2)?'1':'0'}}" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">จำนวนเงินรับมาแล้ว :</div>
                        <div class="col-sm-8">
                            <input type="text" name="payment_value" id="payment_value" value="{{$data->price_pay}}"
                                class="form-control" placeholder="จำนวนเงิน" onkeypress="return isNumberKey(event)">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">ภาพหลักฐาน :</div>
                        <div class="col-sm-8">
                            <input type="file" name="payment_file" id="payment_file" accept="image/*" value="" multiple
                                class="form-control" style="" onchange="upload_payment({{$data->id}})">

                            <!--@if(!empty($data->payment_file_re))
                                <a href="{{url('/uploads/payment/').'/'.$data->payment_file_re}}" target="_blank"><i
                                        class="mdi mdi-image"></i> คลิกเพื่อดู</a><span aria-hidden="true"
                                    class="remove_inv" onclick="reset_payment({{$data->id}})">×</span>
                                @endif -->


                        </div>

                        <div id="viewpayment" style="float:left">
                            @foreach ($attach_payment as $item)
                            <div class="f_left">
                                <a href="{{asset('/uploads/payment').'/'.$item->filename}}" target="_blank"><img
                                        src="{{asset('/uploads/payment').'/'.$item->filename}}" width="50px"
                                        height="50px" class="img-thumbnail" /></a>
                                <span aria-hidden="true" class="remove_inv"
                                    onclick="reset_payment({{$item->id}},$(this))" style="padding-right: 10px;">×</span>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-body" style="height: 260px;overflow-y: scroll;">
                    <h5 class="mt-0 mb-3">History</h5>
                    <div class="activity">
                        @foreach ($history as $item_history)
                        <i class="mdi mdi-check text-success"></i>
                        <div class="time-item">
                            <div class="item-info">
                                <div class="text-muted text-right font-10">
                                    By: {{$item_history->name}}
                                    {{  date('d/m/Y H:i:s' , strtotime($item_history->created_at)) }}
                                </div>
                                <h5 class="mt-0">{{$item_history->topic}}</h5>
                                <p class="text-muted font-13">{{$item_history->detail}}

                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!--end activity-->
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
    </div>
</form>

<!--  Modal content for the above example -->
<div class="modal fade addshipment" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Shipment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form name="sh_form" id="sh_form" autocomplete="off">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#Consignee" role="tab">Consignee
                                Detail</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#Shipment" role="tab">Shipment Detail</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#Other" role="tab">Other Service Charge</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content" style="background-color:white">
                        <div class="tab-pane active p-3" id="Consignee" role="tabpanel">
                            <div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true"
                                data-toggle="toast">
                                <div class="toast-header">
                                    <i class="dripicons-user"> </i>
                                    <strong class="mr-auto"> :: C/O</strong>
                                </div>
                                <div class="toast-body">
                                    <div class="row">
                                        <div class="col-sm-2 title-topic">Name:</div>
                                        <div class="col-sm-10">
                                            <input type="text" id="sh_co" name="sh_co" class="form-control sh-input"
                                                placeholder="โปรดระบุ C/O" required="" maxlength="35">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true"
                                data-toggle="toast">
                                <div class="toast-header">
                                    <i class="dripicons-user"> </i>
                                    <strong class="mr-auto"> :: Consignee</strong>
                                </div>
                                <div class="toast-body">
                                    <div class="row">
                                        <div class="col-sm-2 title-topic">Receiver:</div>
                                        <div class="col-sm-10">
                                            <input type="text" id="sh_recipient" name="recipient"
                                                class="form-control sh-input" placeholder="" required="" maxlength="35">
                                            <input type="hidden" name="sh_id" id="sh_id" value="" />
                                        </div>
                                        <div class="col-sm-2 title-topic">Company:</div>
                                        <div class="col-sm-4">
                                            <input type="text" id="sh_company" name="sh_company"
                                                class="form-control sh-input" placeholder="" maxlength="35">
                                        </div>
                                        <div class="col-sm-2 title-topic">E-Mail:</div>
                                        <div class="col-sm-4">
                                            <input type="text" id="sh_email" name="sh_email"
                                                class="form-control sh-input" placeholder="">
                                        </div>
                                        <div class="col-sm-2 title-topic">Address1:</div>
                                        <div class="col-sm-10">
                                            <input type="text" id="sh_address1" name="sh_address1"
                                                class="form-control sh-input" placeholder="" required="" maxlength="45">
                                        </div>
                                        <div class="col-sm-2 title-topic">Address2:</div>
                                        <div class="col-sm-10">
                                            <input type="text" id="sh_address2" name="sh_address2"
                                                class="form-control sh-input" placeholder="" maxlength="45">
                                        </div>
                                        <div class="col-sm-2 title-topic">Address3:</div>
                                        <div class="col-sm-10">
                                            <input type="text" id="sh_address3" name="sh_address3"
                                                class="form-control sh-input" placeholder="" maxlength="45">
                                        </div>
                                        <div class="col-sm-2 title-topic">PhoneNumber:</div>
                                        <div class="col-sm-4">
                                            <input type="text" id="sh_phone" name="sh_phone"
                                                class="form-control sh-input" placeholder="" required="" maxlength="25">
                                        </div>
                                        <div class="col-sm-2 title-topic">Country:</div>
                                        <div class="col-sm-4" style="padding-top:6px">
                                            <select class="select2 form-control mb-3 custom-select" id="sh_country"
                                                name="sh_country">
                                                <option value="" code="">Select</option>
                                                @foreach ($country_all as $c_item)
                                                <option value="{{$c_item->country_name}}" code="{{$c_item->country_cd}}"
                                                    iata="{{$c_item->country_cd}}">
                                                    {{$c_item->country_name}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="col-sm-2 title-topic">City:</div>
                                        <div class="col-sm-4">
                                            <input list="select_city" id="sh_city" name="sh_city"
                                                class="form-control sh-input" placeholder="โปรดระบุเมือง" required=""
                                                autocomplete="new-password">
                                            <datalist class="form-control" id="select_city" style="display:none">
                                            </datalist>
                                        </div>
                                        <div class="col-sm-2 title-topic">Post Code:</div>
                                        <div class="col-sm-4">
                                            <input type="text" id="sh_postcode" name="sh_postcode"
                                                class="form-control sh-input" placeholder="">
                                        </div>
                                        <div class="col-sm-2 title-topic">State Code:</div>
                                        <div class="col-sm-4">
                                            <input type="text" name="sh_state" id="sh_state"
                                                class="form-control sh-input">
                                        </div>
                                        <div class="col-sm-2 title-topic subrub_area">Subrub:</div>
                                        <div class="col-sm-4 subrub_area">
                                            <input list="select_subrub" id="sh_suburb" name="sh_suburb"
                                                class="form-control sh-input" placeholder="โปรดระบุชานเมือง"
                                                autocomplete="new-password">
                                            <datalist class="form-control" id="select_subrub" style="display:none">
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane p-3" id="Shipment" role="tabpanel">
                            <div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true"
                                data-toggle="toast">
                                <div class="toast-header">
                                    <i class="dripicons-document-remove"></i>
                                    <strong class="mr-auto"> :: Shipment</strong>
                                </div>
                                <div class="toast-body">
                                    <div class="row">
                                        <div class="col-sm-2 title-topic">Service:</div>
                                        <div class="col-sm-10">
                                            <select name="sh_type_of_service" id="sh_type_of_service"
                                                class="form-control">
                                                @foreach ($service_all as $index=>$item)
                                                <option {{$item->cat==$service_type[0]->id?'selected':''}}
                                                    value="{{$item->id}}" routing="{{$item->routing_id}}"
                                                    cal="{{$item->cal_weight}}">
                                                    {{$item->description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-2 title-topic">Insurance:</div>
                                        <div class="col-sm-4">
                                            <input type="number" id="sh_insurance" name="sh_insurance"
                                                class="form-control sh-input" onkeypress="return isNumberKey(event)">
                                        </div>
                                        <div class="col-sm-2 title-topic">Account Number:</div>
                                        <div class="col-sm-4">
                                            <select name="sh_account_number" id="sh_account_number"
                                                class="form-control sh-input">
                                                <option selected value="">Auto</option>
                                                <option value="560659744">Altemate DHL Account</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2 title-topic">SO:</div>
                                        <div class="col-sm-4">
                                            <input type="text" id="sh_so" name="sh_so" class="form-control sh-input"
                                                placeholder="">
                                        </div>
                                        <div class="col-sm-2 title-topic">AWB:</div>
                                        <div class="col-sm-4">
                                            <input type="text" id="sh_awb" name="sh_awb" class="form-control sh-input"
                                                placeholder="">
                                        </div>
                                        <div class="col-sm-2 title-topic">Tracking DHL:</div>
                                        <div class="col-sm-4">
                                            <input type="text" id="sh_tracking_DHL" name="sh_tracking_DHL"
                                                class="form-control sh-input" placeholder="">
                                        </div>
                                        <div class="col-sm-2 title-topic">Tracking Local:</div>
                                        <div class="col-sm-4">
                                            <input type="text" id="sh_tracking_local" name="sh_trackinglocal"
                                                class="form-control sh-input" placeholder="">
                                            <input type="hidden" id="sh_tracking_code" name="sh_tracking_code"
                                                class="form-control sh-input" placeholder="">
                                            <input type="hidden" id="sh_cal" name="sh_cal" class="form-control sh-input"
                                                placeholder="">
                                        </div>
                                        <div class="col-sm-2 title-topic">Description:</div>
                                        <div class="col-sm-10">
                                            <input type="text" id="sh_description" name="sh_description"
                                                class="form-control sh-input" placeholder="" required="">
                                        </div>
                                        <div class="col-sm-2 title-topic">Declared Value:</div>
                                        <div class="col-sm-4">
                                            <input type="text" id="sh_declared_value" name="sh_declaredvalue"
                                                class="form-control sh-input" placeholder=""
                                                onkeypress="return isNumberKey(event)">
                                        </div>
                                        <div class="col-sm-2 title-topic">Content Quantity:</div>
                                        <div class="col-sm-4">
                                            <input type="text" id="sh_content_quantity" name="sh_content_quantity"
                                                class="form-control sh-input" placeholder=""
                                                onkeypress="return isNumberKey(event)">
                                        </div>
                                        <div class="col-sm-2 title-topic">Currency</div>
                                        <div class="col-sm-4">
                                            <select name="sh_currency" id="sh_currency" class="form-control sh-input"
                                                required>
                                                @foreach ($currency as $index=>$item)
                                                <option {{$item->code=='USD'?'selected':''}} value="{{$item->code}}">
                                                    {{$item->code}} - {{$item->country}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-2 title-topic">Shipment Type:</div>
                                        <div class="col-sm-4">
                                            <select name="sh_shipment_type" id="sh_shipment_type"
                                                class="form-control sh-input">
                                                <option selected value="PPS">PPS ( Registered )</option>
                                                <option value="PKD">PKD ( Non-Registered )</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2 title-topic">Packge Type:</div>
                                        <div class="col-sm-4">
                                            <select name="sh_packge_type" id="sh_packge_type"
                                                class="form-control sh-input">
                                                <option value="DC">Documents</option>
                                                <option selected="" value="PA">Parcel</option>
                                                <option value="CP">Customer Packges</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2 title-topic">Note:</div>
                                        <div class="col-sm-4">
                                            <textarea name="sh_note" id="sh_note"
                                                class="form-control sh-input"></textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true"
                                data-toggle="toast">
                                <div class="toast-header">
                                    <i class="dripicons-inbox"></i>
                                    <strong class="mr-auto"> :: Box</strong>
                                    <small><button type="button" class="btn btn-secondary btn-xs" id="add_box">+ Add
                                            Box</button></small>
                                </div>
                                <div class="toast-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered mb-0 table-centered" id="box">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th width="15%">Packaging</th>
                                                    <th width="15%">Weight (KG)</th>
                                                    <th width="15%">Width (CM)</th>
                                                    <th width="15%">Length (CM)</th>
                                                    <th width="15%">Height (CM)</th>
                                                    <th width="10%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td> 1 </td>
                                                    <td><input name="weight" class="weight form-control"
                                                            onkeypress="return isNumberKey(event)" required /></td>
                                                    <td><input name="width" class="width form-control"
                                                            onkeypress="return isNumberKey(event)" /></td>
                                                    <td><input name="length" class="length form-control"
                                                            onkeypress="return isNumberKey(event)" /></td>
                                                    <td><input name="height" class="height form-control"
                                                            onkeypress="return isNumberKey(event)" /></td>
                                                    <td>
                                                    <td style="padding:unset"><button type="button"
                                                            class="btn btn-sm btn-danger" onclick="box_remove(this)">
                                                            <span class="ti-trash"></span></button> <button
                                                            type="button" class="btn btn-sm btn-success"
                                                            onclick="box_copy(this)"> <span
                                                                class="ti-plus"></span></button></td>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane p-3" id="Other" role="tabpanel">
                            <div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true"
                                data-toggle="toast">
                                <div class="toast-header">
                                    <i class="dripicons-checklist"></i>
                                    <strong class="mr-auto"> :: Other Service Charge</strong>
                                </div>
                                <div class="toast-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered mb-0 table-centered" id="other_service_edit">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th width="5"></th>
                                                    <th>Service</th>
                                                    <th width="120">Charge (Baht)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="checkbox" class="other_service_ck ck1" /></td>
                                                    <td>Pickup Fee<input type="hidden" class="other_service_name"
                                                            value="Pickup Fee" /></td>
                                                    <td style="padding: unset;"><input name="sh_pickup_fee"
                                                            id="sh_pickup_fee" class="form-control other_service_value"
                                                            onkeypress="return isNumberKey(event)" value="0" disabled />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" class="other_service_ck ck2" /></td>
                                                    <td>Time Guaranty<input type="hidden" class="other_service_name"
                                                            value="Time Guaranty" /></td>
                                                    <td style="padding: unset;"><input name="sh_time_guaranty"
                                                            id="sh_time_guaranty"
                                                            class="form-control other_service_value"
                                                            onkeypress="return isNumberKey(event)" value="0" disabled />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" class="other_service_ck ck3" /></td>
                                                    <td>Remote Area<input type="hidden" class="other_service_name"
                                                            value="Remote Area" /></td>
                                                    <td style="padding: unset;"><input name="sh_remote_area"
                                                            id="sh_remote_area" class="form-control other_service_value"
                                                            onkeypress="return isNumberKey(event)" value="0" disabled />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" class="other_service_ck ck4" /></td>
                                                    <td>Insurance<input type="hidden" class="other_service_name"
                                                            value="Insurance" /></td>
                                                    <td style="padding: unset;"><input name="sh_insurance"
                                                            id="sh_insurance" class="form-control other_service_value"
                                                            onkeypress="return isNumberKey(event)" value="0" disabled />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" class="other_service_ck ck5" /></td>
                                                    <td>Export Doc<input type="hidden" class="other_service_name"
                                                            value="Export Doc" /></td>
                                                    <td style="padding: unset;"><input name="sh_export_doc"
                                                            id="sh_export_doc" class="form-control other_service_value"
                                                            onkeypress="return isNumberKey(event)" value="0" disabled />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" class="other_service_ck ck6" /></td>
                                                    <td>CO From<input type="hidden" class="other_service_name"
                                                            value="CO From" /></td>
                                                    <td style="padding: unset;"><input name="sh_co_from" id="sh_co_from"
                                                            class="form-control other_service_value"
                                                            onkeypress="return isNumberKey(event)" value="0" disabled />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" class="other_service_ck ck7" /></td>
                                                    <td>Over Size<input type="hidden" class="other_service_name"
                                                            value="Over Size" /></td>
                                                    <td style="padding: unset;"><input name="sh_over_size"
                                                            id="sh_over_size" class="form-control other_service_value"
                                                            onkeypress="return isNumberKey(event)" value="0" disabled />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" class="other_service_ck ck8" /></td>
                                                    <td>Over Weight<input type="hidden" class="other_service_name"
                                                            value="Over Weight" /></td>
                                                    <td style="padding: unset;"><input name="sh_over_weight"
                                                            id="sh_over_weight" class="form-control other_service_value"
                                                            onkeypress="return isNumberKey(event)" value="0" disabled />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" class="other_service_ck ck9" /></td>
                                                    <td>Packing Paperbox<input type="hidden" class="other_service_name"
                                                            value="Packing Paperbox" />
                                                    </td>
                                                    <td style="padding: unset;"><input name="sh_packing_paperbox"
                                                            id="sh_packing_paperbox"
                                                            class="form-control other_service_value"
                                                            onkeypress="return isNumberKey(event)" value="0" disabled />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" class="other_service_ck ck10" /></td>
                                                    <td>Wood Packing<input type="hidden" class="other_service_name"
                                                            value="Wood Packing" /></td>
                                                    <td style="padding: unset;"><input name="sh_wood_packing"
                                                            id="sh_wood_packing"
                                                            class="form-control other_service_value"
                                                            onkeypress="return isNumberKey(event)" value="0" disabled />
                                                    </td>
                                                </tr>
                                                <tr id="other_service_footer">
                                                    <td></td>
                                                    <td>
                                                        <div id="add_other_service" style="cursor: pointer;">+ OTHER
                                                            SERVICE CHARGE</div>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row" style="padding-top:10px">
                        <button type="submit" id="sh_save" class="btn btn-sm btn-primary">Confirm</button>
                        <button type="reset" id="sh_reset" class="btn btn-sm btn-danger">Clear</button>
                        <button type="button" class="btn btn-sm btn-danger" id="close_modal_all" data-dismiss="modal"
                            aria-hidden="true">Cancel</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  Modal content for the above example -->
<div class="modal fade edit_customer" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Edit Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form name="form_edit_customer" id="form_edit_customer"
                    action="{{url('/admin/edit_customer/'.$data->id)}}">
                    <div class="row">
                        <div class="col-sm-2">Customer :</div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" id="search_customer" name="" class="form-control"
                                    placeholder="E-mail, เบอร์โทร, ชื่อ" value="" autocomplete="off">
                                <input type="hidden" id="m_id" name="m_id" value="{{$data->m_id}}" />
                            </div>

                        </div>

                    </div>
                    <div class="table-responsive" style="background: #fdfdff;">
                        <table class="table mb-0" id="customer_select">
                            <tbody>

                            </tbody>
                        </table>
                        <!--end row-->
                    </div>
                    <div class="row" id="customer_detail">
                        <div class="col-sm-2">Name:</div>
                        <div class="col-sm-4">
                            <p id="customer_name_show"> {{$customer->firstname}} {{$customer->lastname}}</p>
                        </div>
                        <div class="col-sm-2">Company:</div>
                        <div class="col-sm-4">
                            <p id="customer_company_show"> {{$customer->company!=''?$customer->company:'-'}}
                            </p>
                        </div>
                        <div class="col-sm-2">Mobile:</div>
                        <div class="col-sm-4">
                            <p id="customer_phone_show"> {{$customer->phone!=''?$customer->phone:'-'}}</p>
                        </div>
                        <div class="col-sm-2">Line:</div>
                        <div class="col-sm-4">
                            <p id="customer_Line_show"> {{$customer->line!=''?$customer->line:'-'}}</p>
                        </div>
                        <div class="col-sm-2">E-Mail :</div>
                        <div class="col-sm-4">
                            <p id="customer_email_show"> {{$customer->email!=''?$customer->email:'-'}} </p>
                        </div>
                        <div class="col-sm-2">บันทึก insighly:</div>
                        <div class="col-sm-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="edit_insighly"
                                    name="edit_insighly" data-parsley-multiple="groups"
                                    {{$data->insighly=='yes'?"checked":""}} data-parsley-mincheck="2">
                                <label class="custom-control-label" for="edit_insighly"><img
                                        src="{{ asset('/assets/images/in.png') }}" width="24" border="0">
                                    insighly</label>
                            </div>
                        </div>
                        <div class="col-sm-2">Address:</div>
                        <div class="col-sm-10">
                            <input type="text" name="address" id="address"
                                value="{{$data->address!=''?$data->address:''}}" class="form-control" required>
                        </div>

                        <div class="col-sm-2" style="padding-top: 15px;">Sale :</div>
                        <div class="col-sm-4" style="padding-top: 15px;">
                            <select name="sale" id="sale" class="form-control">
                                <option value="" selected="">- เลือก Sale -</option>
                                @foreach ($sale as $sale_item)
                                <option {{ $data->sales==$sale_item->id?'selected':'' }} value="{{$sale_item->id}}">
                                    {{$sale_item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div style="display:none">
                            <div class="col-sm-2" style="padding-top:15px;">Note:</div>
                            <div class="col-sm-4" style="padding-top:5px;"><input type="text" name="comment"
                                    id="comment_cus" value="" class="form-control" placeholder="ระบุข้อความ">
                            </div>
                        </div>
                    </div>
                    <button type="submit" id="edit_customer_save" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-danger" id="close_modal" data-dismiss="modal"
                        aria-hidden="true">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade edit_pickup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myedit_pickup">Edit Pickup</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form name="form_edit_delivery" id="form_edit_delivery"
                    action="{{url('/admin/edit_delivery/'.$data->id)}}">
                    <div class="row">
                        <div class="col-sm-2">ประเภทงาน :</div>
                        <div class="col-sm-4">
                            <select name="type_work" id="type_work" class="form-control">
                                <option value="1" {{$data->type_work==1?'selected':''}}>งานส่งออก</option>
                                <option value="2" {{$data->type_work==2?'selected':''}}>งานบริการลูกค้า</option>
                                <option value="3" {{$data->type_work==3?'selected':''}}>งานภายใน</option>
                            </select>
                        </div>
                        <div class="col-sm-2">ประเภทบริการ :</div>
                        <div class="col-sm-4">
                            <select name="type_of_service" id="type_of_service" class="form-control">
                                @foreach ($service_type_all as $index=>$item)
                                <option {{$item->code==$service_type[0]->code?'selected':''}} value="{{$item->id}}">
                                    [{{$item->code}}]
                                    {{$item->name}}
                                </option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Pickup Service :</div>
                        <div class="col-sm-4">
                            <select name="type_delivery" id="type_delivery" class="form-control">
                                <option value="ไม่ระบุ" {{$data->type_delivery=='ไม่ระบุ'?'selected':''}}>ไม่ระบุ
                                </option>
                                <option value="walkin" {{$data->type_delivery=='walkin'?'selected':''}}>Walk in
                                    (ลูกค้ามาส่งเอง)</option>
                                <option value="มอเตอร์ไชต์" {{$data->type_delivery=='มอเตอร์ไชต์'?'selected':''}}>
                                    มอเตอร์ไชต์</option>
                                <option value="รถกระบะ" {{$data->type_delivery=='รถกระบะ'?'selected':''}}>รถกระบะ
                                </option>
                                <option value="TNT" {{$data->type_delivery=='TNT'?'selected':''}}>TNT</option>
                                <option value="ไปรษณียไทย" {{$data->type_delivery=='ไปรษณียไทย'?'selected':''}}>
                                    ไปรษณียไทย
                                </option>
                                <option value="kerry" {{$data->type_delivery=='kerry'?'selected':''}}>kerry</option>
                                <option value="Lalamove" {{$data->type_delivery=='Lalamove'?'selected':''}}>Lalamove
                                </option>
                                <option value="อื่นๆ" {{$data->type_delivery=='อื่นๆ'?'selected':''}}>อื่นๆ</option>
                            </select>
                        </div>
                        <div class="col-sm-2">Tracking(ถ้ามี) :</div>
                        <div class="col-sm-4">
                            <input type="text" name="type_delivery_note" class="form-control"
                                placeholder="ระบุเลขพัสดุ Kerry,TNT,ไปรษณีย์"
                                value="{{$data->type_delivery_note!=''?$data->type_delivery_note:''}}" id="tracking">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">วันที่รับสินค้า :</div>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="โปรดระบุวันที่" name="delivery_date"
                                id="mdate" value="{{date('d-m-Y',$data->delivery_date)  }}" required>
                        </div>
                        <div class="col-sm-2">
                            <input type="radio" name="time" value="พร้อมรับ" checked />
                            พร้อมรับ <br />
                            <input type="radio" name="time" value="ระบุเวลา" /> ระบุเวลา
                        </div>
                        <div class="col-sm-4">
                            <select id="delivery_time" name="delivery_time" class="form-control" disabled required>
                                <option value="">เลือกเวลาที่รับ</option>
                                <option value="08:00">08:00</option>
                                <option value="08:30">08:30</option>
                                <option value="09:00">09:00</option>
                                <option value="09:30">09:30</option>
                                <option value="10:00">10:00</option>
                                <option value="10:30">10:30</option>
                                <option value="11:00">11:00</option>
                                <option value="11:30">11:30</option>
                                <option value="12:00">12:00</option>
                                <option value="12:30">12:30</option>
                                <option value="13:00">13:00</option>
                                <option value="13:30">13:30</option>
                                <option value="14:00">14:00</option>
                                <option value="14:30">14:30</option>
                                <option value="15:00">15:00</option>
                                <option value="15:30">15:30</option>
                                <option value="16:00">16:00</option>
                                <option value="16:30">16:30</option>
                                <option value="17:00">17:00</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2" style="
                        padding-right: unset;
                    ">จำนวน Shipment :</div>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" name="shipment" min="1" max="99"
                                value="{{$data->shipment}}" required />
                        </div>
                        <div class="col-sm-2">Messenger :</div>
                        <div class="col-sm-4">
                            <select name="driver" id="driver" class="form-control">
                                <option value="" {{$data->driver==''?'selected':''}}>- เลือก Messenger. -</option>
                                @foreach ($mess as $mess_item)
                                <option value="{{$mess_item->id}}" {{$data->driver==$mess_item->id?'selected':''}}>
                                    {{$mess_item->name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">คำสั่งพิเศษ :</div>
                        <div class="col-sm-4">
                            <textarea name="m_note"
                                class="form-control">{{$data->m_note!=''?$data->m_note:''}}</textarea>
                        </div>
                        <div class="col-sm-2">ประเทศปลายทาง :</div>
                        <div class="col-sm-4">
                            <textarea name="type_shipping" class="form-control" minlength="3"
                                required> {{$data->type_shipping!=''?$data->type_shipping:''}}</textarea>
                        </div>
                    </div>
                    <div class="row" style="display:none">
                        <div class="col-sm-2">Note:</div>
                        <div class="col-sm-4"><input type="text" name="comment" id="comment_del" value=""
                                class="form-control" placeholder="ระบุข้อความ">
                        </div>
                    </div>
                    <button type="submit" id="edit_customer_save" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-danger" id="close_modal1" data-dismiss="modal"
                        aria-hidden="true">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--  Modal content for the above example -->
<div class="modal fade setdiscount" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Discount</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form name="form_discount" id="form_discount" action="{{url('admin/discount/'.$data->id)}}">
                    <div class="row">
                        <div class="col-sm-2">ส่วนลด :</div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" id="input_discount" name="" class="form-control"
                                    placeholder="กรุณาใส่ส่วนลด" value="{{$data->discount}}"
                                    onkeypress="return isNumberKey(event)" required autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <button type="submit" id="discount_save" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-danger" id="close_modal_discount" data-dismiss="modal"
                        aria-hidden="true">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php 
function calup($value) {
    $sum_digit = round($value,10);
    $sum_notdigit = floor($value);
    $digit = $sum_digit - $sum_notdigit;
    $result = $sum_digit;
    if ($digit > 0) {
        if ($digit < 0.5) {
            $result = $sum_notdigit + 0.5;
        }
        else if($digit > 0.5){
            $result = $sum_notdigit + 1;
        }
        else {
            $result = $sum_digit;
        }
    }
    return $result;
}
?>
<!-- Plugins js -->
<script src="{{ asset('assets/js/xls.js' )}}"></script>
<!-- jQuery  -->
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}"></script>
<script src="{{ asset('assets/pages/jquery.forms-advanced.js' )}}"></script>
<script src="{{ asset('assets/js/request_show.js?2' )}}"></script>
<script src="{{ asset('assets/js/shipment.js?6' )}}"></script>
<script src="{{ asset('assets/js/other_service.js?2' )}}"></script>
<script src="{{ asset('assets/js/gen_awb.js?1' )}}"></script>
<script src="{{ asset('assets/js/jquery.mask.min.js?2' )}}"></script>
<script>
    //----------- พยายามทำ Responsive Table แต่ยังไม่เวร์ค--------------------------
    var table_size = $('#table_size').width();
    console.log(table_size);
    $('#table_view').width(table_size);
    $('#shipment_detail').show();
    $( window ).resize(function() {
        var table_size = $('#table_size').width();
        $('#table_view').width(table_size);
        $('#shipment_detail').show();
    });
    //---------------------------------------------------------------------------

    <?php if(isset($_GET['find'])){
        echo "$('#".$_GET['find']."').css({'background-color': '#f3f6f7', 'box-shadow': '2px 2px 5px grey'});
    $('html, body').animate({
        scrollTop: $('#".$_GET['find']."').offset().top
    }, 1000);";
    }
    ?>
</script>
@endsection