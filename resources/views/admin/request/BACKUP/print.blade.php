<!DOCTYPE html>
<html>
<header>
    <title>งานของ : {{$title}}</title>
    <style>
        table {
            border-collapse: collapse;
        }

        table,
        td,
        th {
            border: 1px solid black;
        }

        body {
            font-size: 14px;
        }

        .print:last-child {
            page-break-after: auto;
        }

        table {
            page-break-inside: auto
        }

        tr {
            page-break-inside: avoid;
            page-break-after: auto
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-footer-group
        }
    </style>
</header>

<body onload="window.print()">

    <p>วันที่ : {{$date}}</p>

    <table style="width:100%" border='1'>
        <tr>
            <th style="text-align:center">ลำดับ</th>
            <th>เวลา</th>
            <th>โทรศัพท์</th>
            <th>ชื่อ-สกุล</th>
            <th>ที่อยู่เข้ารับ</th>
            <th>การชำระเงิน</th>
            <th>บริการ</th>
            <th>คำสั่งพิเศษ</th>
            <th>ชื่อที่อยู่ผู้รับ</th>
            <th>Sale</th>
        </tr>
        @foreach ($data as $index=>$item)
        <tr>
            <td style="text-align:center">{{$index+1}}</td>
            <td style="text-align:center">{{$item->delivery_time}}</td>
            <td>@foreach ($item->phonelist as $index=>$itemphone)
                {{$index>0?',':''}} {{$itemphone->phone}}
                @endforeach</td>
            <td>{{$item->name}}</td>
            <td>{{$item->address}}</td>
            <td style="text-align:center">{{$item->type_pay}} {{$item->pay>0?$item->pay." .-":""}}</td>
            <td>{{$item->service_name!=null?$item->service_name:'ดูรายละเอียด'}}</td>
            <td>{{$item->m_note}}</td>
            <td>{{$item->type_shipping}}</td>
            <td>{{$item->salename}}</td>
        </tr>
        @endforeach
    </table>
</body>

</html>