@extends('layouts.app')

@section('content')

<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('/assets/pages/jquery.form-editor.init.js')}}"></script>
<style>
    .emailHelp {
        color: #f31414;
        font-size: 12px;
    }
</style>
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Email</a></li>
                </ol>
            </div>
            <h4 class="page-title">Edit Service</h4>
        </div>
        <!--end page-title-box-->
    </div>
    <!--end col-->
</div>
<div class="row">

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h4>ส่งเมล์แจ้งชำระค่าบริการ</h4>
                <!-- Tab panes -->
                <div class="tab-content">

                    <form method="POST" class="form-horizontal auth-form my-4" id="service_form"
                        action="{{url('/admin/emailpayment')}}">
                        @csrf
                        <?php

                            //สร้าง shortURL
                            $curl_shorturl = curl_init();
                            curl_setopt_array($curl_shorturl, array(
                                CURLOPT_URL => "https://is.gd/create.php?format=simple&url=https://backend.smeshipping.com/app/historydetail/".$data->id."/".$uid,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "GET",
                                CURLOPT_HTTPHEADER => array(
                                "Cookie: __cfduid=d5d0336382e8f80c883bc40ef0c597ace1585972753"
                                ),
                            ));
                            $res_shorturl = curl_exec($curl_shorturl);
                            curl_close($curl_shorturl);
                         ?>
                        <input type="hidden" name="bookid" value="{{$data->id}}" />
                        <input type="hidden" name="token" value="{{$uid}}" />
                        <input type="hidden" name="m_id" value="{{$data->m_id}}" />
                        <label>ที่อยู่อีเมล์ผู้รับ</label>
                        <input name="email" class="form-control" type="email" value="{{$data->email}}" multiple><br />
                        <label>เบอร์โทรศัพท์มือถือ</label>
                        <input name="tel" class="form-control" type="text" value="{{$data->phone}}"><br />
                        <label>เนื้อหาใน SMS</label>
                        <input name="sms" class="form-control" type="text"
                            value="ท่านมียอดค้างชำระ กรุณาชำระค่าบริการ {{number_format($price<0?0:$price)}} บาท ขออภัยหากชำระแล้ว"><br />
                        <label>เนื้อหาใน E-mail</label>
                        <textarea id="elm2" name="detail">
                            <p style="text-align:center"><img src="http://backend.smeshipping.com/public/assets/email/LOGOSMESHIP.png" width="208" height="51" /></p>
                            <p>เรียน ลูกค้าที่เคารพ</p>
                            <p>ขอเรียนแจ้งค่าบริการ เลขที่ {{str_pad($data->id, 6, '0', STR_PAD_LEFT)}} วันที่ {{date('d/m/Y',$data->delivery_date)}} ยอดค่าบริการเป็นจำนวนเงิน {{number_format($price<0?0:$price)}} บาท</p>
                            <p>&nbsp;</p>
                            <p>รายละเอียดการจัดส่ง<br /><a href="{{$res_shorturl}}">{{url('/app/historydetail/'.$data->id.'/'.$uid)}}</a></p>
                            <p><br /><p><br /><strong>โปรดชำระค่าบริการด้วยการโอนเงินผ่านบัญชีธนาคาร</strong></p>
                            <p><img src="http://backend.smeshipping.com/public/assets/email/1.png" width="43" height="46" /> ธนาคารกสิกร เลขที่บัญชี: 025-2-69009-3</p>
                            <p><img src="http://backend.smeshipping.com/public/assets/email/2.png" width="40" height="40" /> ธนาคารกรุงเทพ เลขที่บัญชี: 141-468551-9</p>
                            <p><img src="http://backend.smeshipping.com/public/assets/email/3.png" width="42" height="32" /> ธนาคารไทยพาณิชย์ เลขที่บัญชี: 070-220179-0</p>
                            <p><br /><p><br /><strong>หรือแสกน QR Code เพื่อชำระค่าบริการ</strong></p>
{{--                            <p><img src="data:image/png;base64, {{ $generatedQR['data']->qrImage }}" width="200" height="200" /></p>--}}
                            <p>&nbsp;</p>
                            <?php
                            $uid = uniqid();
                            //สร้าง shortURL
                            $curl_shorturl = curl_init();
                            curl_setopt_array($curl_shorturl, array(
                                CURLOPT_URL => "https://is.gd/create.php?format=simple&url=https://backend.smeshipping.com/payment_attachment?booking=".str_pad($data->id, 6, '0', STR_PAD_LEFT),
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "GET",
                                CURLOPT_HTTPHEADER => array(
                                "Cookie: __cfduid=d5d0336382e8f80c883bc40ef0c597ace1585972753"
                                ),
                            ));
                            $res_shorturl = curl_exec($curl_shorturl);
                            curl_close($curl_shorturl);
                         ?>
                            <p><strong>กรุณาส่งหลักฐานการชำระค่าบริการเพื่อยืนยันการส่งออกพัสดุ&nbsp;</strong><br /><a href="{{$res_shorturl}}">www.smeshipping.com/paid?booking={{$data->id}}</a></p>
                            <p><br />หากลูกค้ามีข้อสงสัยเกี่ยวกับค่าบริการ สามารถติดต่อได้ที่ <br /> E-Mail: service@smeshipping.com<br />โทร: 02-105-7703-5<br />Line: @shipping</p>
                            <p><br />เอสเอ็มอี ชิปปิ้ง ขนส่งกับเราแบบเบาใจ</p>
                            <p>ขอขอบพระคุณสำหรับการใช้บริการในครั้งนี้</p>
                        </textarea>
                        <br />
                        <button type="submit" class="btn btn-primary waves-effect waves-light" id="send"
                            onclick="$('#loading').show()">ส่ง</button>
                        <a href="{{url('/admin/request/'.$data->id)}}"><button type="button"
                                class="btn btn-danger waves-effect waves-light" id="back">กลับ</button></a>
                    </form>
                    <!-- ******************************************************************************************************* -->
                </div>
            </div>
        </div>
        <!--end card-->
    </div>
    <!--end col-->
</div>

@endsection
