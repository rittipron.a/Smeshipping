@extends('layouts.app')
@section('content')
    <script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
    <link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet"/>
    <link href="{{ asset('assets/plugins/select2/select2.min.css" rel="stylesheet' )}}" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css' )}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
          rel="stylesheet"/>
          <link href="{{ asset('/assets/css/smeqt_edit_mode.css') }}" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        @page {
                            size: A4;
                            margin: 0;
                        }

                        @media print {

                            html,
                            body {
                                width: 210mm;
                                height: 297mm;
                            }

                            /* ... the rest of the rules ... */
                        }

                        @media (min-width: 576px) {
                            .modal-dialog {
                                max-width: 50%;
                                margin: 1.75rem auto;
                            }
                        }
                    </style>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0);">Admin</a></li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0);">Commercial Invoice</a>
                        </li>
                    </ol>
                </div>
                <h4 class="page-title">Commercial Invoice</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!-- end col -->
    </div>

    <script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
    <script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
    <script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' )}}"></script>
    <script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}"></script>
    <script src="{{ asset('assets/pages/jquery.forms-advanced.js')}}"></script>
    <script src="{{ asset('assets/js/create_quotation.js?1')}}"></script>
@endsection
