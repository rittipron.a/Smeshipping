<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="COPYRIGHT" content="SME SHIPPING">
    <meta name="CONTACT_ADDR" content="cs@smeshipping.com">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="Cache-Control" content="no-cache">

    <link rel="shortcut icon" type="image/ico" href="{{ asset('/assets/images/favicon.png') }}">
    <link href="{{ asset('/assets/css/smeqt.css') }}" rel="stylesheet" type="text/css">
    <style type="text/css">
    .spaced_service{
        font-size: 13px !important;
        font-weight: bold;
        padding-left: 4px;
    }
    .other_service{
        font-size: 13px !important;
        font-weight: bold;
        padding-left: 4px;
    }
    .spaced_detail{
        font-size: 12px;
        padding-left: 13px;
    }
    .spaced_topdetail{
        font-size: 12px;
        padding-left: 4px;
    }
    .hendingfee{
        font-size: 12px;
        text-align: revert;
        padding-left: 13px !important;
    }
    .qt-approve, .qt-sale {
        margin-top: 0px;
    }
    .text_recert{
        text-align: revert;
        border-bottom: 1px solid #ddd !important;
    }
    @page {
        size: A4;
        margin: 0;
    }

    @media print {

        html,
        body {
            width: 210mm;
            height: 297mm;
        }

        /* ... the rest of the rules ... */
    }
    </style>

    <title>SME QUOTATION</title>
</head>

<body style="">
    <section id="qt-content-form" class="qt-wrapper qt-data-info">
        <div class="panel-container" id="qtform" style="border:none">
            <div class="header-th-content ctrl-inlineblock header-content">
                <p>Commercial Invoice</p>
                <p style="margin-top:10px;">Date Of Exporatation : <span>10/10/2020</span></p>
                <p>Internation Air Way Bill Number : <span>000000001</span></p>
                <p>Invoice Number : <span>00 00 01</span></p>
            </div>
            <table class="table table-bordered">
                <thead>
                </thead>
                <tbody>
                    <!-- head  -->
                    <tr>
                        <td colspan="2"class="text_recert">
                            <p>SHIPPER/EXPORTER</p>
                            <p>TNT EXPRESS</p>
                            <p>ZIP CODE : <span>10510</span></p>
                            <p>COUNTRY : <span>THAILAND</span></p>
                        </td>
                        <td colspan="2" class="text_recert">
                            <p>CONSIGNEE</p>
                            <p>247-2 Ratchada Rd., Bangkok, Bangkok Yai, Ratchada</p>
                            <p>ZIP CODE : <span></span></p>
                            <p>COUNTRY : <span></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text_recert">
                            <p>PURPOSE OF SHIPMENT</p>
                            <p>SOLD</p>
                            <p>NOT SOLD</p>
                            <p>GIFT</p>
                        </td>
                        <td colspan="2" class="text_recert">
                            <p>IMPORTER (IF OTHER THAN CONSIGNEE)</p>
                        </td>
                    </tr>
                    <!-- body -->
                    <tr>
                        <td rowspan="2" class="text_recert">
                            <p>FULL DESCRIPTION OF GOODS</p>
                        </td>
                        <td>QTY</td>
                        <td>UNIT VALUE</td>
                        <td>TOTAL VALUE</td>
                    </tr>
                    <tr>
                        <td>(EACH ITEM)</td>
                        <td>(EACH ITEM)</td>
                        <td>(THB)</td>
                    </tr>
                    <!-- body detail -->
                    <tr class="text_recert">
                        <td>A</td>
                        <td>1</td>
                        <td>1</td>
                        <td>100</td>
                    </tr>
                    <!-- foot -->
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" rowspan="4" style="text-align:left">
                            <p style="font-weight:700">Condition</p>
                            <p>- Service term Door to Door, Delivered Duties Unpaid*</p>
                            <p>- Money Bock Guarantee (MBG) does not apply to this shipment.</p>
                            <p>- All other conditions of Contact on SHIPPING ORDER and SME SHIPPING Standard Conditions of Carriage shall apply to the shipment.</p>
                            <p>- Remote area and surcharges and other surcharges my be charged in additional to this rate. </p>
                            <p>- SME SHIPPING reserves the right at any time to amend, modify, or discontinue the rate and the terms and conditions herein without prior notice.</p>
                            <p>- Duties and taxes and/or GST (goods and services tax) may be assessed on the contents. Any such charge will be billed to the recipient, unless instructed otherwise by the shipper. Notwithstanding the aforesaid, will aways be primarily responsible for all charges relating to this shipment.</p>
                        </td>
                        <td colspan="2" rowspan="5" style="font-weight:bold;padding-top: 15%;">Total</td>
                        <td rowspan="5" style="font-weight:bold;padding-top: 15%;">
                            
                        </td>
                    </tr>
                </tfoot>

            </table>


            <p class="cond-th-desc" style="font-size:12px !important;padding-left:10px;margin-top:10px;">
            I DECLARE ALL THE INFORMATION CONTAINED IN THIS INVOICE IS TRUE AND CORRECT TO THE BEST OF MY KNOWLEDGE. <br>(SIGNATURE REQUIREMENTS MAY VARY PER COUNTRY)
            </p>

            <div class="twocolumn qt-approve" style="width:45%;">
                <p style="margin-top:10px"></p>
                <p>Approved by __________________________________</p>

                <p style="margin-top:10px"></p>
                <p>Position ______________________________________</p>
            </div>


            <div class="twocolumn qt-sale" style="width:45%;float:right">
                <p style="margin-top:10px;font-size:12px !important;">Sincerely Yours.</p>

                <p style="margin-top:10px;">Sales Manager ____________________________</p>
            </div>
        </div>

    </section>
    <script type="text/javascript">
    window.print();
    </script>

</body>

</html>
