@extends('layouts.app')
@section('content')
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script type="text/javascript" src="{{ asset('assets/dependencies/JQL.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/dependencies/typeahead.bundle.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/dist/jquery.Thailand.min.js') }}"></script>
<link href="{{ asset('/assets/css/commercial.css') }}" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Commercial Invoid</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Commercial Invoid</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">Edit Table With Button</h4>
                <p class="text-muted mb-3">Add toolbar column with edit and delete buttons.</p>
                <div class="row">
                    <div class="col-md-1"><button type="button"
                            class="btn btn-primary waves-effect waves-light float-left mb-3" data-toggle="modal"
                            data-animation="bounce" data-target=".bs-example-modal-lg">+ Add New</button>
                    </div>
                    <div class="col-md-5">
                        <form>

                            <div class="input-group" style="">
                                <input type="text" id="search_customer" name="keyword" class="form-control"
                                    placeholder="ค้นหาจาก E-mail, ชื่อ, เบอร์โทร" value="" autocomplete="off">
                                <span class="input-group-append">
                                    <button type="submit" class="btn  btn-primary"><i
                                            class="fas fa-search"></i>Search</button>
                                </span>

                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Contact</th>
                                <th>Type</th>
                                <th>Active</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <div class="col-12">

    </div>
    <!-- end col -->
</div>


<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <span>สินค้า :</span> <input type="text" class="form-control" name="c_product">
                        </div>
                        <div class="col-md-6">
                            <span>คำแนะนำ :</span> <input type="text" class="form-control" name="c_recommend">
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-danger" onclick="add_country()">+ADD</button>
                        </div>
                        <div class="col-md-3 add_country">
                            <select class="form-control" id="c_country" name="c_country" required>
                                @foreach ($country_all as $c_item)
                                <option value="{{$c_item->country_name}}" code="{{$c_item->country_cd}}"
                                    iata="{{$c_item->country_cd}}" risk="{{$c_item->risk_price}}">
                                    {{$c_item->country_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary submit_">ยืนยัน</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  Modal content for the above example -->
<div class="modal fade edit_commercial_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Edit Commercial</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="{{ asset('assets/js/commercial.js?1')}}"></script>

<!-- <script>
var data = null;
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //------------ สร้าง customer ใหม่
    $("#create_customer").submit(function(e) {
        $('#loading').show();
        e.preventDefault();
        var thisform = $(this);
        var url = thisform.attr('action');
        $.ajax({
            type: "POST",
            data: $('#create_customer').serialize(),
            url: url,
            success: function(result) {
                $('#customer_select').hide();
                $('#customer_detail').show();
                $('#loading').hide();
                $('#close_modal').click();
                var result_data = JSON.parse(result);
                alert('Create customer successful.')
                window.location.reload();
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator.");
                $('#loading').hide();
            }
        });
    });

    $("#edit_customer").submit(function(e) {
        $('#loading').show();
        e.preventDefault();
        var thisform = $(this);
        var url = thisform.attr('action');
        var id = $('#edit_id').val();
        url = url+'/'+id;
        $.ajax({
            type: "PATCH",
            data: $('#edit_customer').serialize(),
            url: url,
            success: function(result) {
                $('#loading').hide();
                if (result == '0') {
                    alert('มีการใช้อีเมล์นี้แล้วในระบบ กรุณาเปลี่ยนอีเมล์ใหม่');
                } else if (result == '1') {
                    alert('มีการใช้ Username นี้แล้วในระบบ กรุณาเปลี่ยน Username ใหม่');
                } else {
                    location.reload();
                }
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator.");
                $('#loading').hide();
            }
        });
    });
    //------------ ดึงข้อมูลลูกค้า
});
//-------------- ห้ามใส่ตัวอักษร
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 &&
        (charCode < 48 || charCode > 57)) 
        return false;
    return true;
}

function editmode(id) {
    $('#edit_customer')[0].reset();
    close_add_address();
    $.ajax({
        type: "get",
        url: '{{url('admin/getCustomer_by_id')}}/' + id,
        success: function(result) {
            var dataset = JSON.parse(result);
            //console.log(data);
            var data = dataset[0];
            var address = dataset[1];

            $('#edit_id').val(data.id);
            if (data.title != null && data.title != '') {
                $('#' + data.title).prop("checked", true);
            }
            if (data.status != null && data.status != '') {
                $('#status_' + data.status).prop("checked", true);
            }
            $('#firstname').val(data.firstname);
            $('#lastname').val(data.lastname);
            $('#company').val(data.company);
            $("#type").find("option[value=" + data.type + "]").attr("selected", true);
            $("#type_customer").find("option[value=" + data.type_customer + "]").attr("selected", true);
            $("#service_group").find("option[value=" + data.service_group + "]").attr("selected", true);
            $('#phone').val(data.phone);
            $('#email').val(data.email);
            $('#username').val(data.username);
            $('#password').val('******');
            $('#line').val(data.line);
            $('#code').val(data.code);
            $("#credit_term").find("option[value=" + data.credit_term + "]").attr("selected", true);
            $('#pickup_fee').val(data.pickup_fee);
            $('#note').val(data.note);
            $('#' + data.title).prop('checked', true);
            if (data.del_cus == null) {
                $('#active').prop('checked', true);
            } else {
                $('#active').prop('checked', false);
            }

            console.log(address);
            $('#cus_address_show tbody').html('');
            $(address).each(function(i, e) {
                var row = '<tr>' +
                    '<th scope="row">' + (i + 1) + '</th>' +
                    '<td><span class="fullname">' + e.customer_name + '</span></td>' +
                    '<td><span class="address">' + e.address +
                    '</span>, <span class="tumbon_name" set="' + e.tumbon + '">' + e.tumbon +
                    '</span>, <span class="amphur_name" set="' + e.amphur + '">' + e.amphur +
                    '</span>, <span class="province_name" set="' + e.province + '">' + e.province +
                    '</span> <span class="postcode">' + e.postcode + '</span><br/>' +
                    '<input type="radio" name="pickup" value="' + e.id + '" ' + (e.pickup == 1 ?'checked' : '') + '> ตั้งเป็นที่อยู่รับของ ' +
                    '<input type="radio" name="invoice" value="' + e.id + '" ' + (e.invoice == 1 ?'checked' : '') + '> ตั้งเป็นที่อยู่ออกใบเสร็จ' +
                    '</div></td>' +
                    '<td><span class="phone">' + e.phone +
                    '</span><span class="email" style="display:none">' + e.email + '</span></td>' +
                    '<td>' +
                    '<a href="#" onclick="edit_address($(this),' + e.id +')" class="mr-2"><i class="fas fa-edit text-info font-16"></i></a>' +
                    '<a href="#" onclick="remove_address($(this),' + e.id +')"><i class="fas fa-trash-alt text-danger font-16"></i></a>' +
                    '</td>' +
                    '</tr>';
                $('#cus_address_show tbody').append(row);
            });

        },
        error: function(e) {
            console.log("ERROR : ", e);
            alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
            $('#loading').hide();
        }
    })
}

$('.deleteitem').click(function(e) {
    var x = confirm("Are you sure you want to delete?");
    if (x) {
        var id = $(this).attr('delid');
        var url = '{{url('/admin/customer')}}';
        url = url + '/' + id;
        $('#loading').show();
        $.ajax({
            type: 'DELETE',
            url: url,
            success: function(result) {
                $('#loading').hide();
                window.location.reload(true);
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                $('#loading').hide();
            }
        });
    } else
        return false;

});

function close_add_address() {
    $('#add_address_area').hide();
    $('#edit_customer').show();
    $('#addr_customer_id').val('');
    $('#amphur').html('');
    $('#tumbon').html('');
}

function show_add_address() {
    $('#address_title').html('เพิ่มที่อยู่');
    $('#address_id').val('');
    $('#add_address_area').show();
    $('#edit_customer').hide();
    $('#addr_customer_id').val($('#edit_id').val());
    var fullname = $('#firstname').val() + '  ' + $('#lastname').val();
    $('#addr_name').val(fullname);
    $('#addr_phone').val($('#phone').val());
    $('#amphur').html('');
    $('#tumbon').html('');
    $('#addr_address').val('');
    $('#postcode').val('');
}
$("#add_address_form").submit(function(e) {
    $('#loading').show();
    e.preventDefault();
    var thisform = $(this);
    var url = thisform.attr('action');

    $.ajax({
        type: "POST",
        data: $('#add_address_form').serialize(),
        url: url,
        success: function(result) {
            $('#loading').hide();

            var e = JSON.parse(result);
            e = e[0];
            console.log(e);
            if ($('#address_id').val() != '') {
                address_tmp.parent().parent().html(
                    '<th scope="row"><i class="mdi mdi-content-save"></i></th>' +
                    '<td><span class="fullname">' + e.customer_name + '</span></td>' +
                    '<td><span class="address">' + e.address +
                    '</span>, <span class="tumbon_name" set="' + e.tumbon + '">' + e.tumbon +
                    '</span>, <span class="amphur_name" set="' + e.amphur + '">' + e.amphur +
                    '</span>, <span class="province_name" set="' + e.province + '">' + e.province + 
                    '</span> <span class="postcode">' + e.postcode +
                    '</span><br/>' +
                    '<input type="radio" name="pickup" value="' + e.id + '" ' + (e.pickup == 1 ?'checked' : '') + '> ตั้งเป็นที่อยู่รับของ ' +
                    '<input type="radio" name="invoice" value="' + e.id + '" ' + (e.invoice == 1 ? 'checked' : '') + '> ตั้งเป็นที่อยู่ออกใบเสร็จ' +
                    '</div></td>' +
                    '<td><span class="phone">' + e.phone +
                    '</span><span class="email" style="display:none">' + e.email +
                    '</span></td>' +
                    '<td>' +
                    '<a href="#" onclick="edit_address($(this),' + e.id +')" class="mr-2"><i class="fas fa-edit text-info font-16"></i></a>' +
                    '<a href="#" onclick="remove_address($(this),' + e.id +')"><i class="fas fa-trash-alt text-danger font-16"></i></a>' +
                    '</td>');
            } else {
                var i = $('#cus_address_show tbody tr').length;
                var row = '<tr>' +
                    '<th scope="row">' + (i + 1) + '</th>' +
                    '<td><span class="fullname">' + e.customer_name + '</span></td>' +
                    '<td><span class="address">' + e.address +
                    '</span>, <span class="tumbon_name" set="' + e.tumbon + '">' + e.tumbon +
                    '</span>, <span class="amphur_name" set="' + e.amphur + '">' + e.amphur +
                    '</span>, <span class="province_name" set="' + e.province + '">' + e.province +
                    '</span> <span class="postcode">' + e.postcode + '</span><br/>' +
                    '<input type="radio" name="pickup" value="' + e.id + '" ' + (e.pickup == 1 ?'checked' : '') + '> ตั้งเป็นที่อยู่รับของ ' +
                    '<input type="radio" name="invoice" value="' + e.id + '" ' + (e.invoice == 1 ?'checked' : '') + '> ตั้งเป็นที่อยู่ออกใบเสร็จ' +
                    '</div></td>' +
                    '<td><span class="phone">' + e.phone +
                    '</span><span class="email" style="display:none">' + e.email + '</span></td>' +
                    '<td>' +
                    '<a href="#" onclick="edit_address($(this),' + e.id +')" class="mr-2"><i class="fas fa-edit text-info font-16"></i></a>' +
                    '<a href="#" onclick="remove_address($(this),' + e.id +')"><i class="fas fa-trash-alt text-danger font-16"></i></a>' +
                    '</td>' +
                    '</tr>';
                $('#cus_address_show tbody').append(row);
            }

            close_add_address();
        },
        error: function(e) {
            console.log("ERROR : ", e);
            alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
            $('#loading').hide();
        }
    });
});

function remove_address(obj, id) {
    if (confirm('Are you sure you want to delete this address into the database?')) {
        $.ajax({
            type: "POST",
            url: "{{url('admin/del_customer_address')}}/" + id,
            success: function(result) {
                $(obj).parent().parent().remove();
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                $('#loading').hide();
            }
        });
    } else {
        // Do nothing!
    }

}
var address_tmp = null;

function edit_address(obj, id) {
    $('#address_title').html('แก้ไขที่อยู่');
    address_tmp = $(obj);
    $('#address_id').val(id);
    $('#add_address_area').show();
    $('#edit_customer').hide();
    var data = $(obj).parent().parent();
    var prov = data.find('.province_name').attr('set');
    var email = data.find('.email').html();
    email = email == 'null' ? '' : email;
    $('#addr_customer_id').val($('#edit_id').val());
    $('#addr_name').val(data.find('.fullname').html());
    $('#addr_address').val(data.find('.address').html());
    $('#addr_phone').val(data.find('.phone').html());
    $('#addr_email').val(email);
    $('#province').val(prov);
    $('#postcode').val(data.find('.postcode').html());
    $('#amphur').val(data.find('.amphur_name').attr('set'));
    $('#tumbon').val(data.find('.tumbon_name').attr('set'));
}

function randompassword() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    $('#password').val(retVal);
}

function savepassword() {

    var phone = $('#phone').val();
    var email = $('#email').val();
    var id = $('#edit_id').val();
    if (phone != "" && email != "" && phone != null && email != null) {
        if (confirm('คุณต้องการส่งอีเมล์แจ้งรหัสใหม่ไปที่ ' + email + ' \n และส่ง SMS ไปที่ ' + phone +' ใช่หรือไม่?')) {
            // ต้องส่งไป controller ตัวใหม่ ที่บันทึกรหัสเสร็จแล้ว ส่งอีเมล์ และ SMS ด้วย
            $.ajax({
                type: 'POST',
                data: $('#edit_customer').serialize(),
                url: '/re_password/'+id,
                success: function(result) {
                    console.log('Succeed');
                    alert("ส่งข้อมูล Password ใหม่ ไปทาง Email และ SMS ให้ลูกค้าเรียบร้อยแล้วครับ");
                },
                error: function(e) {
                    console.log("ERROR : ", e);
                    alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                    $('#loading').hide();
                }
            });
        } else {
            alert("บันทึกเสร็จสิ้น");
        }
    } else {
        if (phone == null && phone == "") {
            alert("กรุณาใส่เบอร์โทรศัพย์");
        } else {
            alert("กรุณาใส่Email");
        }
    }
}

var regex = new RegExp("^[0-9,]*$");

$('#phone').bind("keypress", function (event) {
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

$('.phones').bind("keypress", function (event) {
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});
</script> -->
@endsection