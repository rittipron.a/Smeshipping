@extends('layouts.app')
@section('content')
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/select2/select2.min.css" rel="stylesheet' )}}" type="text/css" />
<link href="{{ asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css' )}}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
    rel="stylesheet" />
<link href="{{ asset('/assets/css/commercial.css') }}" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);"></a>
                    </li>
                </ol>
            </div>
            <h4 class="page-title">Commercial Invoice</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="body_commercial">
                    <div class="margin_commercial">
                        <form>
                            @csrf
                            <h1>Commercial Invoice</h1>
                            <p class="top_commercial">
                                <span>Date Of Exporatation : </span>
                                <span>
                                    <input type="text" name="valid_day" id="mdate" value="{{ date('d/m/Y') }}">
                                </span>
                            </p>
                            <p class="top_commercial">
                                <span>Internation Air Way Bill Number : </span>
                                <input type="text" name="" value="">
                            </p>
                            <p class="top_commercial">
                                <span>Invoice Number :</span>
                                <input type="text" name="" value="">
                            </p>
                            <div class="row border_detail">
                                <div class="col-md-6 detail border_rigth_detail">
                                    <p>SHIPPER/EXPORTER</p>
                                    <textarea class="addres_detail"></textarea>
                                    <p class="top_commercial">
                                        <span>ZIP CODE :</span>
                                        <input type="text" name="" value="">
                                    </p>
                                    <p class="top_commercial">
                                        <span>COUNTRY :</span>
                                        <select class="form-control" id="" name="" required>
                                            @foreach ($country_all as $c_item)
                                            <option value="{{$c_item->country_name}}" code="{{$c_item->country_cd}}"
                                                iata="{{$c_item->country_cd}}" risk="{{$c_item->risk_price}}">
                                                {{$c_item->country_name}}</option>
                                            @endforeach
                                        </select>
                                    </p>
                                </div>
                                <div class="col-md-6 detail">
                                    <p>CONSIGNEE</p>
                                    <textarea class="addres_detail"></textarea>
                                    <p class="top_commercial">
                                        <span>ZIP CODE :</span>
                                        <input type="text" name="" value="">
                                    </p>
                                    <p class="top_commercial">
                                        <span>COUNTRY :</span>
                                        <input type="text" name="" value="">
                                    </p>
                                </div>
                                <div class="col-md-6 detail border_rigth_detail border_top_detail">
                                    <p>PURPOSE OF SHIPMENT</p>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <label class="form-check-label" for="sold">SOLD</label>
                                                <input class="form-check-input" type="radio" name="sold" id="SOLD"
                                                    value="SOLD" checked>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label" for="not_sold">
                                                    NOT SOLD
                                                </label>
                                                <input class="form-check-input" type="radio" name="not_sold"
                                                    id="not_sold" value="NOT SOLD">
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label" for="gift">
                                                    GIFT
                                                </label>
                                                <input class="form-check-input" type="radio" name="gift" id="gift"
                                                    value="GIFT">
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label" for="other">
                                                    OTHER
                                                </label>
                                                <input class="form-check-input" type="radio" name="other" id="other"
                                                    value="OTHER">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <label class="form-check-label" for="personal">
                                                    PERSONAL EFFECTS
                                                </label>
                                                <input class="form-check-input" type="radio" name="personal"
                                                    id="personal" value="PERSONAL EFFECTS">
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label" for="return">
                                                    RETURN AND REPAIR
                                                </label>
                                                <input class="form-check-input" type="radio" name="return" id="return"
                                                    value="RETURN AND REPAIR">
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label" for="sample">
                                                    SAMPLE
                                                </label>
                                                <input class="form-check-input" type="radio" name="sample" id="sample"
                                                    value="SAMPLE">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 detail border_top_detail">
                                    <p>IMPORTER (IF OTHER THAN CONSIGNEE)</p>
                                    <textarea class="addres_detail"></textarea>
                                </div>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td rowspan="2" colspan="2" class="border_top_detail border_rigth_detail">
                                                FULL DESCRIPTION OF GOODS</td>
                                            <td class="border_top_detail border_rigth_detail">QTY</td>
                                            <td class="border_top_detail border_rigth_detail">UNIT VALUE</td>
                                            <td class="border_top_detail">TOTAL VALUE</td>
                                        </tr>
                                        <tr class="">
                                            <td class="border_top_detail border_rigth_detail">(EACH ITEM)</td>
                                            <td class="border_top_detail border_rigth_detail">(EACH ITEM)</td>
                                            <td class="border_top_detail">(THB)</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="border_top_detail border_rigth_detail">+
                                                เพิ่มรายละเอียด</td>
                                            <td class="border_top_detail border_rigth_detail">&nbsp;</td>
                                            <td class="border_top_detail border_rigth_detail">&nbsp;</td>
                                            <td class="border_top_detail">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="border_top_detail border_rigth_detail">+ เพิ่มรายละเอียด</td>
                                            <td class="border_top_detail border_rigth_detail">&nbsp;</td>
                                            <td class="border_top_detail border_rigth_detail">&nbsp;</td>
                                            <td class="border_top_detail">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <p>
                                I DECLARE ALL THE INFORMATION CONTAINED IN THIS INVOICE IS TRUE AND CORRECT TO THE
                                BEST OF MY KNOWLEDGE.
                                <br>(SIGNATURE REQUIREMENTS MAY VARY PER COUNTRY)
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>
                                        <span>DATE</span>
                                        <span></span>
                                    </p>
                                    <p>
                                        <span>NAME</span>
                                        <input type="text">
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p>
                                        <span>SIGNATURE</span>
                                        <input type="text">
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!-- end col -->
</div>

<script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}"></script>
<script src="{{ asset('assets/pages/jquery.forms-advanced.js')}}"></script>
<script src="{{ asset('assets/js/create_quotation.js?1')}}"></script>
<script src="{{ asset('assets/js/commercial.js?1')}}"></script>
@endsection