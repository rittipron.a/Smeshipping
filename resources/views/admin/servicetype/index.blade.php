@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Service Type</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Service Type</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mt-0 header-title">Edit Table With Button</h4>
                <p class="text-muted mb-3">Add toolbar column with edit and delete buttons.</p>
                <button type="button" class="btn btn-primary waves-effect waves-light float-left mb-3"
                    data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg">+ Add New</button>
                <div class="table-responsive">
                    <table class="table mb-0" id="my-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Account Api</th>
                                <th>Tracking</th>
                                <th class="tabledit-toolbar-column"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($serviceType as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->code}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->account_api}}</td>
                                <td>{{$item->tracking}}</td>
                                <!--<td>
                                    <div class="custom-control custom-switch switch-success">
                                        <input type="checkbox" class="custom-control-input"
                                            id="customSwitchSuccess{{$item->id}}" name="active-{{$item->id}}"
                                            {!!$item->del==null?'checked':''!!}>
                                        <label class="custom-control-label"
                                            for="customSwitchSuccess{{$item->id}}"></label>
                                    </div>
                                </td>
                            -->
                                <td>
                                    <div class="btn-group btn-group-sm" style="float: none;">
                                        <a href="{{url('/admin/servicetype')}}/{{$item->id}}">
                                            <button type="button" class="btn btn-sm btn-info"
                                                style="float: none; margin: 4px;">
                                                <span class="ti-pencil"></span></button>
                                        </a>
                                        <button type="button" class="btn btn-sm btn-danger deleteitem"
                                            style="float: none; margin: 4px;" delid="{{$item->id}}">
                                            <span class="ti-trash"></span></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end table-->
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!-- end col -->
</div>



<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Service Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="{{url('/admin/servicetype')}}" method="POST">
                    @csrf
                    @method('POST')
                    <h4 class="mt-0 header-title">Service Type Detail</h4>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-1 col-form-label text-left">Code</label>
                        <div class="col-sm-11">
                            <input class="form-control" type="number" value="" required id="code" name="code">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-1 col-form-label text-left">Name</label>
                        <div class="col-sm-11">
                            <input class="form-control" type="text" value="" required id="name" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-1 col-form-label text-left">Account API</label>
                        <div class="col-sm-11">
                            <input class="form-control" type="text" value="" id="account_api" name="account_api">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-1 col-form-label text-left">Tracking</label>
                        <div class="col-sm-11">
                            <select name="tracking" id="tracking" class="form-control">
                                <option value="DHL">DHL</option>
                                <option value="GMB">GMB</option>
                                <option value="Aramex">Aramex</option>
                                <option value="FEDEX">FEDEX</option>
                                <option value="TNT">TNT</option>
                            </select>
                        </div>
                    </div>
                    <h4 class="mt-0 header-title">Other Service Charge </h4>
                    <div class="table-responsive">
                        <table class="table table-bordered mb-0 table-centered" id="other_service_edit">
                            <thead class="thead-light">
                                <tr>
                                    <th width="5"></th>
                                    <th>Service</th>
                                    <th width="120">Charge (Baht)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck1"></td>
                                    <td>Pickup Fee<input type="hidden" class="other_service_name" value="Pickup Fee">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_pickup_fee" id="sh_pickup_fee"
                                            class="form-control other_service_value" type="number" value="0" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck2"></td>
                                    <td>Time Guaranty<input type="hidden" class="other_service_name"
                                            value="Time Guaranty">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_time_guaranty" id="sh_time_guaranty"
                                            class="form-control other_service_value" type="number" value="0" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck3"></td>
                                    <td>Remote Area<input type="hidden" class="other_service_name" value="Remote Area">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_remote_area" id="sh_remote_area"
                                            class="form-control other_service_value" type="number" value="0" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck4"></td>
                                    <td>Insurance<input type="hidden" class="other_service_name" value="Insurance"></td>
                                    <td style="padding: unset;"><input name="sh_insurance" id="sh_insurance"
                                            class="form-control other_service_value" type="number" value="0" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck5"></td>
                                    <td>Export Doc<input type="hidden" class="other_service_name" value="Export Doc">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_export_doc" id="sh_export_doc"
                                            class="form-control other_service_value" type="number" value="0" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck6"></td>
                                    <td>CO From<input type="hidden" class="other_service_name" value="CO From"></td>
                                    <td style="padding: unset;"><input name="sh_co_from" id="sh_co_from"
                                            class="form-control other_service_value" type="number" value="0" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck7"></td>
                                    <td>Over Size<input type="hidden" class="other_service_name" value="Over Size"></td>
                                    <td style="padding: unset;"><input name="sh_over_size" id="sh_over_size"
                                            class="form-control other_service_value" type="number" value="0" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck8"></td>
                                    <td>Over Weight<input type="hidden" class="other_service_name" value="Over Weight">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_over_weight" id="sh_over_weight"
                                            class="form-control other_service_value" type="number" value="0" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck9"></td>
                                    <td>Packing Paperbox<input type="hidden" class="other_service_name"
                                            value="Packing Paperbox"></td>
                                    <td style="padding: unset;"><input name="sh_packing_paperbox"
                                            id="sh_packing_paperbox" class="form-control other_service_value"
                                            type="number" value="0" disabled>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck10"></td>
                                    <td>Wood Packing<input type="hidden" class="other_service_name"
                                            value="Wood Packing">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_wood_packing" id="sh_wood_packing"
                                            class="form-control other_service_value" type="number" value="0" disabled>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <br />
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-danger" id="close_modal" data-dismiss="modal"
                        aria-hidden="true">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script src="{{ asset('assets/js/other_service.js' )}}"></script>
<script>
    $('.deleteitem').click(function(e){
                var x = confirm("Are you sure you want to delete?");
                if (x){
                var id = $(this).attr('delid');
                var url = '{{url('/admin/servicetype')}}';
                url = url+'/'+id;
                var b = $(this).parent().parent().parent()
                $('#loading').show();
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        success: function (result) {
                            $('#loading').hide();
                            //window.location.reload(true);
                            
                            console.log(b);
                            b.remove();
                        },
                        error: function (e) {
                            console.log("ERROR : ", e);
                            alert("An error occurred on the server when processing the URL. Please contact the system administrator.");
                            $('#loading').hide();
                        }
                    });
                    }
                else
                    return false;
                
            });
</script>
@endsection