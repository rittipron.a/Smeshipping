@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="float-right">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Admin</a></li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">Service Type</a>
                    </li>

                </ol>
            </div>
            <h4 class="page-title">Service Type</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{url('/admin/servicetype')}}/{{$serviceType->id}}" method="POST">
                    @csrf
                    @method('PUT')
                    <h4 class="mt-0 header-title">Service Type Detail</h4>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-1 col-form-label text-left">Code</label>
                        <div class="col-sm-11">
                            <input class="form-control" type="number" value="{{$serviceType->code}}" required id="code"
                                name="code">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-1 col-form-label text-left">Name</label>
                        <div class="col-sm-11">
                            <input class="form-control" type="text" value="{{$serviceType->name}}" required id="name"
                                name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-1 col-form-label text-left">Account API</label>
                        <div class="col-sm-11">
                            <input class="form-control" type="text" value="{{$serviceType->account_api}}"
                                id="account_api" name="account_api">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-1 col-form-label text-left">Tracking</label>
                        <div class="col-sm-11">
                            <select name="tracking" id="tracking" class="form-control">
                                <option value="{{$serviceType->tracking}}" selected>{{$serviceType->tracking}}</option>
                                <option value="DHL">DHL</option>
                                <option value="GMB">GMB</option>
                                <option value="Aramex">Aramex</option>
                                <option value="FEDEX">FEDEX</option>
                                <option value="TNT">TNT</option>
                            </select>
                        </div>
                    </div>
                    <h4 class="mt-0 header-title">Other Service Charge </h4>
                    <div class="table-responsive">
                        <table class="table table-bordered mb-0 table-centered" id="other_service_edit">
                            <thead class="thead-light">
                                <tr>
                                    <th width="5"></th>
                                    <th>Service</th>
                                    <th width="120">Charge (Baht)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck1"
                                            {{$serviceType->pickup_fee==null?'':'checked'}}></td>
                                    <td>Pickup Fee<input type="hidden" class="other_service_name" value="Pickup Fee">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_pickup_fee" id="sh_pickup_fee"
                                            class="form-control other_service_value" type="number"
                                            value="{{$serviceType->pickup_fee==null?0:$serviceType->pickup_fee}}"
                                            {{$serviceType->pickup_fee==null?'disabled':''}}>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck2"
                                            {{$serviceType->time_guaranty==null?'':'checked'}}></td>
                                    <td>Time Guaranty<input type="hidden" class="other_service_name"
                                            value="Time Guaranty">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_time_guaranty" id="sh_time_guaranty"
                                            class="form-control other_service_value" type="number"
                                            value="{{$serviceType->time_guaranty==null?0:$serviceType->time_guaranty}}"
                                            {{$serviceType->time_guaranty==null?'disabled':''}}>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck3"
                                            {{$serviceType->remote_area==null?'':'checked'}}></td>
                                    <td>Remote Area<input type="hidden" class="other_service_name" value="Remote Area">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_remote_area" id="sh_remote_area"
                                            class="form-control other_service_value" type="number"
                                            value="{{$serviceType->remote_area==null?0:$serviceType->remote_area}}"
                                            {{$serviceType->remote_area==null?'disabled':''}}>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck4"
                                            {{$serviceType->insurance==null?'':'checked'}}></td>
                                    <td>Insurance<input type="hidden" class="other_service_name" value="Insurance"></td>
                                    <td style="padding: unset;"><input name="sh_insurance" id="sh_insurance"
                                            class="form-control other_service_value" type="number"
                                            value="{{$serviceType->insurance==null?0:$serviceType->insurance}}"
                                            {{$serviceType->insurance==null?'disabled':''}}>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck5"
                                            {{$serviceType->export_doc==null?'':'checked'}}></td>
                                    <td>Export Doc<input type="hidden" class="other_service_name" value="Export Doc">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_export_doc" id="sh_export_doc"
                                            class="form-control other_service_value" type="number"
                                            value="{{$serviceType->export_doc==null?0:$serviceType->export_doc}}"
                                            {{$serviceType->export_doc==null?'disabled':''}}>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck6"
                                            {{$serviceType->co_from==null?'':'checked'}}></td>
                                    <td>CO From<input type="hidden" class="other_service_name" value="CO From"></td>
                                    <td style="padding: unset;"><input name="sh_co_from" id="sh_co_from"
                                            class="form-control other_service_value" type="number"
                                            value="{{$serviceType->co_from==null?0:$serviceType->co_from}}"
                                            {{$serviceType->co_from==null?'disabled':''}}>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck7"
                                            {{$serviceType->over_size==null?'':'checked'}}></td>
                                    <td>Over Size<input type="hidden" class="other_service_name" value="Over Size"></td>
                                    <td style="padding: unset;"><input name="sh_over_size" id="sh_over_size"
                                            class="form-control other_service_value" type="number"
                                            value="{{$serviceType->over_size==null?0:$serviceType->over_size}}"
                                            {{$serviceType->over_size==null?'disabled':''}}>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck8"
                                            {{$serviceType->over_weight==null?'':'checked'}}></td>
                                    <td>Over Weight<input type="hidden" class="other_service_name" value="Over Weight">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_over_weight" id="sh_over_weight"
                                            class="form-control other_service_value" type="number"
                                            value="{{$serviceType->over_weight==null?0:$serviceType->over_weight}}"
                                            {{$serviceType->over_weight==null?'disabled':''}}>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck9"
                                            {{$serviceType->packing_paperbox==null?'':'checked'}}></td>
                                    <td>Packing Paperbox<input type="hidden" class="other_service_name"
                                            value="Packing Paperbox"></td>
                                    <td style="padding: unset;"><input name="sh_packing_paperbox"
                                            id="sh_packing_paperbox" class="form-control other_service_value"
                                            type="number"
                                            value="{{$serviceType->packing_paperbox==null?0:$serviceType->packing_paperbox}}"
                                            {{$serviceType->packing_paperbox==null?'disabled':''}}>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" class="other_service_ck ck10"
                                            {{$serviceType->wood_packing==null?'':'checked'}}></td>
                                    <td>Wood Packing<input type="hidden" class="other_service_name"
                                            value="Wood Packing">
                                    </td>
                                    <td style="padding: unset;"><input name="sh_wood_packing" id="sh_wood_packing"
                                            class="form-control other_service_value" type="number"
                                            value="{{$serviceType->wood_packing==null?0:$serviceType->wood_packing}}"
                                            {{$serviceType->wood_packing==null?'disabled':''}}>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <br />
                    <button type="submit" class="btn btn-primary waves-effect waves-light"
                        id="save_service">บันทึก</button>
                </form>
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
    <!-- end col -->
</div>



<!--  Modal content for the above example -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Lead</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadName">Name</label>
                                <input type="text" class="form-control" id="LeadName" required="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="LeadEmail">Email</label>
                                <input type="email" class="form-control" id="LeadEmail" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="PhoneNo">Phone No</label>
                                <input type="text" class="form-control" id="PhoneNo" required="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="status-select" class="mr-2">Country</label>
                                <select class="custom-select" id="status-select">
                                    <option selected="">Select</option>
                                    <option value="1">India</option>
                                    <option value="2">USA</option>
                                    <option value="3">Japan</option>
                                    <option value="4">China</option>
                                    <option value="5">Germany</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-danger">Delete</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script src="{{ asset('assets/js/other_service.js' )}}"></script>
@endsection