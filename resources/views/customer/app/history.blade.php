@extends('layouts.customer_app')

@section('content')
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<?php $sum_weight = 0;$sum_package = $data->total_shipment;$total_price = 0; ?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3 style="text-align:center">รายละเอียด</h3>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <h5 class="card-header bg-danger text-white mt-0">ข้อมูลการใช้บริการ</h5>
                            <div class="card-body">
                                <p style="font-size:16px;color:black">
                                    รหัสบริการ: {{str_pad($data->id, 6, "0", STR_PAD_LEFT)}}<br />
                                    จำนวน: {{count($shipment)}} Shipment<br />
                                    วันที่ใช้บริการ: {{  date('d/m/Y' , strtotime($data->created_at)) }}<br />
                                    อัตราค่าบริการ: {{number_format($data->price_total,2)}} บาท<br />
                                </p>
                            </div>
                            <!--end card-body-->
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <h5 class="card-header bg-danger text-white mt-0">ข้อมูลผู้ส่ง</h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-2 customer_data" cus_id="{{$data->m_id}}"
                                        cus_status="{{$data->request_status}}">ชื่อ-สกุล:</div>
                                    <div class="col-sm-4">
                                        {{$customer->firstname}} {{$customer->lastname}}
                                    </div>
                                    <div class="col-sm-2">บริษัท:</div>
                                    <div class="col-sm-4">
                                        {{$customer->company!=''?$customer->company:'-'}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">เบอร์โทร:</div>
                                    <div class="col-sm-4">
                                        <a
                                            href="tel:{{$customer->phone}}">{{$customer->phone!=''?$customer->phone:'-'}}</a>
                                    </div>
                                    <div class="col-sm-2">Line ID:</div>
                                    <div class="col-sm-4">
                                        {{$customer->line!=''?$customer->line:'-'}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">E-Mail :</div>
                                    <div class="col-sm-4">
                                        {{$customer->email!=''?$customer->email:'-'}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">Address:</div>
                                    <div class="col-sm-10">
                                        {{$data->address!=''?$data->address:''}}
                                    </div>
                                </div>
                            </div>
                            <!--end card-body-->
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <h5 class="card-header bg-danger text-white mt-0">ข้อมูล Shipment</h5>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ผู้รับ</th>
                                            <th>ประเทศ</th>
                                            <th>จำนวน</th>
                                            <th>ติดตามพัสดุ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($shipment as $key=>$item)
                                        <tr>
                                            <th scope="row">{{$key+1}}</th>
                                            <td>{{$item->recipient}}</td>
                                            <td>{{$item->country_name!=''?$item->country_name:$item->country}}</td>
                                            <td>
                                                {{count($box->where('shipment_id', $item->id))}} กล่อง
                                            </td>
                                            <td>
                                                @if($item->awb!=null)
                                                <a
                                                    href="https://www.dhl.com/en/express/tracking.shtml?AWB={{$item->awb}}&brand=DHL">แสดงผลการนำส่ง</a>
                                                @else
                                                ยังไม่ได้รับหมายเลข AWB
                                                @endif

                                                <a href="{{url('/app/copy/'.$item->id)}}"><button type="button"
                                                        id="copy"
                                                        class="btn btn-success waves-effect waves-light">Copy</button></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <!--end /table-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end card-body-->
        </div>
    </div>
    <div class="col-md-12" style="text-align:center">

    </div>
</div>


@endsection