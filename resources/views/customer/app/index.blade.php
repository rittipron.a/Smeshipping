@extends('layouts.customer_app')

@section('content')
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<style>
    h3 {
        color: #656269 !important;
        font-size: 16px !important;
    }

    .text-info,
    .mt-2 {
        color: #656269 !important;
    }

    .textcenter {
        padding: unset;
    }

    .client-card {
        padding-top: 10px;
        padding-bottom: 10px;
        margin-bottom: 10px !important;
    }

    .tracking-no {
        color: red;
        font-size: 20px;
        font-weight: 800;
    }

    .btn-next {
        background: white !important;
        border: unset !important;
        color: #3677b6 !important;
        font-weight: bold;
        font-size: 18px;
        filter: drop-shadow(rgba(0, 0, 0, 0.15) 0px 3px 6px);
    }

    .client-card {
        margin-bottom: 10px !important;
    }

    .client-card .row .col-1:last-child,
    .client-card .row .col-12:last-child {
        display: none;
    }

    .sumary_data .card {
        padding: 15px;
    }
    @media screen and (max-width: 767px){
    .teblet_show{
        margin-top: 10px;
        }
    }
</style>
<div class="teblet_show">
    <div class="row" style="padding:10px;">
        <div class="col-6 col-md-3" style="padding:unset">
            <a href="{{url('app/booking')}}"><img src="{{ asset('/assets/images/m_menu1.jpg') }}" width="100%"></a>
        </div>
        <div class="col-6 col-md-3" style="padding:unset">
            <a href="{{url('/app/history')}}"><img src="{{ asset('/assets/images/m_menu2.jpg') }}" width="100%"></a>
        </div>
        <div class="col-6 col-md-3" style="padding:unset">
            <a href="{{url('/app/waitforpayment')}}"><img src="{{ asset('/assets/images/m_menu3.jpg') }}" width="100%">
        </div>
        <div class="col-6 col-md-3" style="padding:unset">
            <a href="{{url('/app/check_price')}}"><img src="{{ asset('/assets/images/m_menu4.jpg') }}" width="100%"></a>
        </div>
    </div>
</div><br/>
<div class="row teblet_hide" style="padding-bottom:unset;">
    <div class="col-md-3">
        <a href="{{url('app/booking')}}">
            <div class="card crm-data-card waves-effect" style="width:100%;margin-bottom:unset">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 align-self-center">
                            <div class="data-icon">
                                <img src="{{ asset('/assets/images/1001.jpg') }}" height="80px">
                            </div>
                        </div><!-- end col-->
                        <div class="col-sm-12 textcenter">
                            <h3>สร้างพัสดุ</h3>
                        </div><!-- end col-->
                    </div><!-- end row-->
                </div>
                <!--end card-body-->
            </div>
        </a>
    </div>
    <div class="col-md-3">
        <a href="{{url('/tracking')}}">
            <div class="card crm-data-card waves-effect" style="width:100%;margin-bottom:unset">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 align-self-center">
                            <div class="data-icon">
                                <img src="{{ asset('/assets/images/1002.jpg') }}" height="80px">
                            </div>
                        </div><!-- end col-->
                        <div class="col-sm-12 textcenter">
                            <h3>ติดตามสถานะ</h3>
                        </div><!-- end col-->
                    </div><!-- end row-->
                </div>
                <!--end card-body-->
            </div>
        </a>
    </div>
    <div class="col-md-3">
        <a href="{{url('/app/waitforpayment')}}">
            <div class="card crm-data-card waves-effect" style="width:100%;margin-bottom:unset">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 align-self-center">
                            <div class="data-icon">
                                <img src="{{ asset('/assets/images/1003.jpg') }}" height="80px">
                            </div>
                        </div><!-- end col-->
                        <div class="col-sm-12 textcenter">
                            <h3>แจ้งการชำระเงิน</h3>
                        </div><!-- end col-->
                    </div><!-- end row-->
                </div>
                <!--end card-body-->
            </div>
        </a>
    </div>
    <div class="col-md-3">
        <a href="{{url('/app/check_price')}}">
            <div class="card crm-data-card waves-effect" style="width:100%;margin-bottom:unset">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 align-self-center">
                            <div class="data-icon">
                                <img src="{{ asset('/assets/images/1004.jpg') }}" height="80px">
                            </div>
                        </div><!-- end col-->
                        <div class="col-sm-12 textcenter">
                            <h3>เช็คราคา</h3>
                        </div><!-- end col-->
                    </div><!-- end row-->
                </div>
                <!--end card-body-->
            </div>
        </a>
    </div>
</div>
@if($data!=null&&$checkpickup>0)
<p style="text-align:center;font-size:20px;font-weight: bold;">รายการส่งของฉัน</p>
<div id="shipment_temp" class="row">{!!$data->data!!}</div>
<div class="row justify-content-md-center">
    <div class="col-md-3">
        <a href="{{url('/app/read_temp')}}"><button type="button"
                class="btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                style="width: 100%;padding:5px;">
                <span style="color:red">ทำการจัดส่ง</span>
            </button>
        </a>
    </div>
</div>
@else
<p style="text-align:center;font-size:20px;font-weight: bold;">ยังไม่มีรายการส่ง</p>
@endif

</div>
</div>
</div>

@endsection