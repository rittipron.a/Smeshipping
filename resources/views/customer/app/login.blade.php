@extends('layouts.customer_app_hidden_nav')

@section('title','Customer login -')

@section('content')
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<style>
.row {
    padding-bottom: 10px;
}

.toggle-password {
    padding: 23%;
}

.error_login {
    text-align: center;
    font-size: 16px;
    font-weight: bold;
    color: red;
}

.error_required {
    font-size: 16px;
    font-weight: bold;
    color: red;
}

.topic_input {
    text-align: right;
    font-size: 16px;
    font-weight: bold;
    padding-top: 8px;
}

*:-webkit-autofill,
*:-webkit-autofill:hover,
*:-webkit-autofill:focus,
*:-webkit-autofill:active {
    /* use animation hack, if you have hard styled input */
    transition-property: background-color, color;
    /* if input has one color, and didn't have bg-image use shadow */
    -webkit-box-shadow: 0 0 0 1000px #fff inset;
    /* text color */
}

.form-control.is-valid:focus,
.was-validated .form-control:valid:focus {
    border-color: #007bff;
    box-shadow: 0 0 0 0
}

.form-control.is-valid,
.was-validated .form-control:valid {
    border: 1px solid #e8ebf3;
    padding-right: calc(1.5em + .75rem);
    background-image: url();
    background-repeat: no-repeat;
    background-position: center right calc(.375em + .1875rem);
    background-size: calc(.75em + .375rem) calc(.75em + .375rem);
}

.form-control.is-invalid,
.was-validated .form-control:invalid {
    border-color: #dc3545;
    padding-right: calc(1.5em + .75rem);
    background-image: url();
    background-repeat: no-repeat;
    background-position: center right calc(.375em + .1875rem);
    background-size: calc() calc(.75em + .375rem);
}

input::-ms-reveal {
    display: none;
}

.login_ {
    width: 10px !important;
    padding: 0px;
    border: 1px solid #e8ebf3;
    border-left: 0px saddlebrown;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
}

.login_spaced {
    border-top-right-radius: 0px !important;
    border-bottom-right-radius: 0px !important;
    border-right: 0px;
}

.spaced_col {
    padding-right: 0px;
}
</style>

<div class="row" style="padding-bottom:0px;">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <br />
        <br />
        <div class="card">
            <div class="card-body" style="padding: 20px 0px 30px 0px;">
                <H3 style="padding-left:20px;padding-right: 20px;">ระบบจัดการส่งสำหรับลูกค้า</H3>

                <hr style="border: 1px solid rgb(242, 39, 39);
                background-color: white;
                position: inherit;
                top: 100px;
                z-index: 2;" />
                <form name="create_customer" id="create_customer" action="{{url('/app/login')}}" method="POST"
                    novalidate>
                    @csrf
                    <div class="row">
                        @if($errors->any())
                        <div class="col-12 error_login">
                            {{$errors->first()}}
                        </div>
                        @endif
                        <div class="col-4 topic_input">
                            อีเมล์</div>
                        <div class="col-6 spaced_col">
                            <input type="text" name="email" class="form-control login_spaced"
                                placeholder="กรุณากรอกอีเมล์" required="">
                            <div class="invalid-feedback error_required">
                                กรุณากรอกอีเมล์
                            </div>
                        </div>
                        <div class="col-1 login_ email_">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 topic_input">
                            รหัสผ่าน
                        </div>
                        <div class="col-6 spaced_col">
                            <div>
                                <input type="password" name="password" id="password" class="form-control login_spaced"
                                    placeholder="กรุณากรอกรหัสผ่าน" required="" />
                                <div class="invalid-feedback error_required">
                                    กรุณากรอกรหัสผ่าน
                                </div>
                            </div>
                        </div>
                        <div class="col-1 login_ password_">
                            <span toggle="#password"
                                class="fa fa-fw fa-eye field-icon toggle-password toggle_password"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4"></div>
                        <div class="col-7">
                            <button type="submit" class="btn btn-danger waves-effect waves-light"
                                style="width: 100%;">เข้าสู่ระบบ</button>
                        </div>
                    </div>
                    <a href="{{url('/app/forgetpassword')}}">
                        <div class="col-11 col-sm-11" style="text-align:right; font-size: 14px;color: #656269;">
                            ลืมรหัสผ่าน
                        </div>
                    </a>
                    <a href="{{url('/app/register')}}">
                        <div class="col-11 col-sm-11"
                            style="text-align:right; font-size: 14px; color: rgba(54,119,182,1);">
                            สมัครใช้บริการ
                        </div>
                    </a>
            </div>
            <div class="col-sm-3"></div>
        </div>
        </form>
    </div>
    <!--end card-body-->
</div>
<!-- <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
        <a href="{{url('/app/register')}}">
            <div class="card" style="background-color:rgb(228, 228, 228);height:40px">
                <div
                    style="text-align:center;top:10px;position:inherit;color:rgb(241, 39, 39);font-size:16px!important">
                    หากยังไม่มีบัญชี
                    สมัครฟรีคลิกที่นี่
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-3"></div>
</div> -->


<div class="col-md-2"></div>

</div>
<script>
$(function() {
    $("#create_customer").submit(function() {
        var form = $(this)[0];
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        form.classList.add('was-validated');
    });
});

$(".toggle_password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

$('input[name=email]').focus(function() {
    $('.email_').css('border-color', '#80bdff');
}).blur(
    function() {
        $('.email_').css('border-color', '#e8ebf3');
    });

$('input[type=password]').focus(function() {
    $('.password_').css('border-color', '#80bdff');
}).blur(
    function() {
        $('.password_').css('border-color', '#e8ebf3');
    });
</script>
@endsection