@extends('layouts.customer_app_hidden_nav')

@section('content')
<div style="width:100%; text-align:center">
    <img
        src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(300)->generate(url('app/pass/'.$token->token))) !!} ">
</div>

@endsection