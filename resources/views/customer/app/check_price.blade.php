@extends(session()->has('cus.id')?'layouts.customer_app':'layouts.customer_app_hidden_nav')

@section('content')
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<style>
body,
p,
.label,
.btn-next {
    color: rgba(101, 98, 105, 1);
    font-family: 'Prompt', sans-serif;
    font-size: 16px;
}

/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

/* Firefox */
input[type=number] {
    -moz-appearance: textfield;
}

.btn-next {
    background: white !important;
    border: unset !important;
    color: #ef2828 !important;
    font-size: 18px;
    filter: drop-shadow(rgba(0, 0, 0, 0.15) 0px 3px 6px);
}

.font_service {
    font-size: 18px;
    padding-top: 16px;
    width: 70%;
}

@media screen and (max-width: 768px) {
    .font_service {
        font-size: 16px;
        padding-top: 10px;
        width: 70%;
    }
}


</style>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3 style="text-align:center">
                    <b>เช็คราคาส่งพัสดุ</b>
                </h3>
                <form name="check_form" id="check_form" action="{{url('/app/booking/')}}" method="GET">
                    <div class="row">
                        <div class="col-md-6">
                            <i class="mdi mdi-airplane"></i> ส่งไป<br />
                            <select class="select2 form-control mb-3 custom-select" id="sh_country" name="sh_country"
                                required onchange="get_service()">
                                @foreach ($country_all as $c_item)
                                <option value="{{$c_item->country_name}}" code="{{$c_item->country_cd}}"
                                    iata="{{$c_item->country_cd}}">
                                    {{$c_item->country_name}}</option>
                                @endforeach
                            </select>
                            <i class="mdi mdi-scale"></i> น้ำหนัก<br />
                            <div>
                                <input type="text" class="form-control" name="sh_weight" id="sh_weight" placeholder=""
                                    required onkeyup="cal_box()" onkeypress="return isNumberKey(event)">
                                <div>
                                    <span style="position: absolute;top: 110px;right: 20px">KG</span>
                                </div>
                            </div>
                            <br />
                            <div id="box_area">
                                <div class="row box">
                                    <div class="col-12" style="padding-bottom:10px;"><i class="mdi mdi-dropbox"></i>
                                        ขนาดพัสดุกล่องที่ <span class="box_number">1</span><br />
                                    </div>
                                    <div class="col-4">
                                        <input type="number" name="width[]" class="form-control width"
                                            style="text-align:center" placeholder="กว้าง (CM)" onchange="cal_box()">
                                    </div>
                                    <div class="col-4">
                                        <input type="number" name="length[]" class="form-control length"
                                            style="text-align:center" placeholder="ยาว (CM)" onchange="cal_box()">
                                    </div>
                                    <div class="col-4">
                                        <input type="number" name="height[]" class="form-control height"
                                            style="text-align:center" placeholder="สูง (CM)" onchange="cal_box()">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12" style="text-align:center">
                                <input type="hidden" id="volumnWeight" name="volumnWeight" />
                                <button type="button" class="btn btn-danger btn-next waves-effect waves-light"
                                    style="margin-top:10px;" onclick="addbox()"><i
                                        class="mdi mdi-plus-circle-outline"></i>
                                    เพิ่มกล่อง</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row service_select" id="service_select" style="display:none">
                            </div>
                        </div>
                        <button type="submit" id="submit_form" style="display:none"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<script>
function addbox() {
    var box_number = $('.box').length + 1;
    var html =
        '<div class="row box"><div class="col-12" style="padding-bottom:10px;"><i class="mdi mdi-dropbox"></i> ขนาดพัสดุกล่องที่ <span class="box_number">' +
        box_number +
        '</span><span class="box_del badge badge-danger" onclick="box_del($(this))">X</span><br></div> <div class="col-4"><input type="number" name="width[]" class="form-control width" style="text-align:center" placeholder="กว้าง (CM)" onchange="cal_box()"></div> <div class="col-4"><input type="number" name="length[]" class="form-control length" style="text-align:center" placeholder="ยาว (CM)" onchange="cal_box()"></div><div class="col-4"><input type="number" name="height[]" class="form-control height" style="text-align:center" placeholder="สูง (CM)" onchange="cal_box()"></div></div>';
    $('#box_area').append(html);
}

function box_del(obj) {
    $(obj).parent().parent().remove();
    $('.box').each(function(i, e) {
        $(e).find(".box_number").html(i + 1);
    });
    cal_box();
}

function cal_box() {
    console.log('cal_box');
    var volumnWeight = 0;
    var weight = parseFloat(convert2number($("#sh_weight").val()));

    $('.box').each(function(i, e) {
        var width = parseFloat(convert2number($(e).find(".width").val()));
        var length = parseFloat(convert2number($(e).find(".length").val()));
        var height = parseFloat(convert2number($(e).find(".height").val()));
        var sum = width * length * height / 5000;
        volumnWeight = volumnWeight + sum;
    });
    if (volumnWeight < weight) {
        volumnWeight = weight;
    }
    $('#volumnWeight').val(volumnWeight);
    $('#volumnWeight').change();
    get_service()

}

function convert2number(value) {
    var result = value;
    if (value === undefined || isNaN(value) || value === "") {
        result = 0;
    }
    return result;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function get_service() {
    $('#service_select').html('');
    var sh_country = $('#sh_country option:selected');
    var totalweight = $('#volumnWeight').val();
    var url = '/getprice/' + sh_country.attr('code') + '/' + totalweight;
    if ($('#volumnWeight').val() != '' && $('#volumnWeight').val() != 0) {
        $.ajax({
            type: "GET",
            url: url,
            success: function(result) {
                var data = $.parseJSON(result);
                console.log(data);
                var html = '';
                $(data).each(function(i, e) {
                    e.logo = e.logo != null ? e.logo : 'sme.jpg';
                    if (e.price.result != undefined) {
                        html += '<div class="col-md-12" onclick="submit_form()">' +
                            '<input type="radio" name="service" class="radio_service_item" id="s_' +
                            e.id + '" value="' + e.id + '" s_id="' + e.id + '" s_price="' + e.price
                            .result + '" s_logo="' + e.logo + '">' +
                            '<label class="row service_card" for="s_' + e.id + '">' +
                            '<div class="col-4 col-md-3" style="float: left;padding: 10px;text-align:center">' +
                            '<img src="/uploads/service/' + e.logo +
                            '" alt="user" width="70px" height="70px"></div>' +
                            '<div class="col-8 col-md-9 font_service">' +
                            '<span class="cut-text">' + e.name + ' </span>( ' + e.day.day +
                            ' วันทำการ )' +
                            '<span style="float:right">' + numberWithCommas(e.price.result) +
                            ' ฿</span></div>' + '</label></div>';
                    }
                });
                $('#service_select').html(html);
            },
            error: function(e) {
                console.log("ERROR : ", e);
            }
        });
        $('#service_select').show();
        $('#service_wait_data').hide();
    } else {
        $('#service_select').hide();
        $('#service_wait_data').show();
    }
}

function submit_form() {
    $('#submit_form').trigger("click");
    window.status = '/app/booking';
    return true;
}

function isNumberKey(evt) {
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$(document).ready(function() {
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});
</script>
@endsection