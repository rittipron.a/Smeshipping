@extends('layouts.customer_app')

@section('content')
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<style>
    .row {
        padding-bottom: 10px;
        padding-top: 10px;
        width: 100%;
    }

    input.largerCheckbox {
        width: 20px;
        height: 20px;
        margin-top: 10px;
    }

    #sumary {
        position: fixed;
        left: 0;
        bottom: 0;
        padding: unset;
    }

    #sumary .col-md-3 {
        width: 100%;
        background-color: white;
        text-align: center;
        box-shadow: 0px 0px 3px rgba(31, 30, 47, 0.05);
        padding: 10px;
    }

    #sumary .col-md-3 button {
        float: right;
    }

    #amount_total {
        color: red;
        font-size: 25px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <hr style="border: 1px solid rgb(242, 39, 39);
        background-color: white;
        position: inherit;
        top: 100px;
        z-index: 2;">
        <div class="card">
            <div class="card-body">

                <h3>รายการรอชำระเงิน</h3>

                <div class="row" style="padding-top:40px;">
                    <div class="col-12">
                        <div class="row" style="padding:unset">
                            <div class="col-1" style="text-align:center">
                                <input type="checkbox" class="largerCheckbox" id="check_all" />
                            </div>
                            <div class="col-11 mobile_hide">
                                <div class="row">
                                    <div class="col-md-2" style="text-align:center">สถานะ</div>
                                    <div class="col-md-2" style="text-align:center">Booking No.</div>
                                    <div class="col-md-2" style="text-align:center">ค่าบริการ</div>
                                    <div class="col-md-3" style="text-align:center">วันที่</div>
                                    <div class="col-md-2" style="text-align:center">กำหนดชำระ</div>
                                </div>
                            </div>
                            <div class="col-11 mobile_show">
                                เลือกทั้งหมด
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-12">
                        @foreach ($data as $item)
                        <div class="card client-card" style="margin-bottom: 10px;">
                            <div class="row">
                                <div class="col-1" style="text-align:center">
                                    @if($item->payment_file_re==null)
                                    <input type="checkbox" class="largerCheckbox check-booking"
                                        price="{{$item->price_total-$item->discount<0?0: $item->price_total-$item->discount}}" id="{{$item->id}}" />
                                    @else
                                    <input type="checkbox" class="largerCheckbox" disabled />
                                    @endif
                                </div>
                                <div class="col-11 mobile_hide">
                                    <div class="row" style="padding:unset">
                                        <div class="col-md-2" style="text-align:left">
                                            {!!$item->payment_file_re==null?'<label for="'.$item->id.'"
                                                class="btn btn-outline-danger waves-effect waves-light"
                                                style="width:100%">รอชำระเงิน</label>':'<button type="button"
                                                class="btn btn-outline-light waves-effect waves-light"
                                                style="width:100%;color:grey" disabled>กำลังตรวจสอบ</button>'!!}
                                        </div>
                                        <div class="col-md-2" style="text-align:center">
                                            {{str_pad($item->id,6,"0",STR_PAD_LEFT)}}</div>
                                        <div class="col-md-2" style="text-align:center">
                                            {{number_format($item->price_total-$item->discount>0?$item->price_total-$item->discount:0)}} บาท
                                        </div>
                                        <div class="col-md-3" style="text-align:center">
                                            {{date('d M Y',$item->delivery_date)}}</div>
                                        <div class="col-md-2" style="text-align:center">
                                            {{$item->credit_term=='cash'?'-':date("d M Y", strtotime("+".$item->credit_term." day",$item->delivery_date))}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-11 mobile_show">
                                    <div class="row">
                                        <div class="col-6"><b>Booking no.</b></div>
                                        <div class="col-6">{{str_pad($item->id,6,"0",STR_PAD_LEFT)}}</div>
                                        <div class="col-6"><b>ค่าบริการ</b></div>
                                        <div class="col-6">
                                        {{number_format($item->price_total-$item->discount>0?$item->price_total-$item->discount:0)}} บาท
                                        </div>
                                        <div class="col-6"><b>วันที่</b></div>
                                        <div class="col-6">{{date('d M y',$item->delivery_date)}}
                                        </div>
                                        <div class="col-6"><b>กำหนดชำระ</b></div>
                                        <div class="col-6">
                                            {{$item->credit_term=='cash'?'-':date("d M Y", strtotime("+".$item->credit_term." day",$item->delivery_date))}}
                                        </div>
                                        <div class="col-12">
                                            {!!$item->payment_file_re==null?'<label for="'.$item->id.'"
                                                class="btn btn-outline-danger waves-effect waves-light"
                                                style="width:100%">รอชำระเงิน</label>':'<button type="button"
                                                class="btn btn-outline-light waves-effect waves-light"
                                                style="width:100%;color:grey" disabled>กำลังตรวจสอบ</button>'!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-md-center" id="sumary">
    <div class="col-md-3"> ยอดรวม <span id="amount_total">0</span> บาท 
        <button type="button" class="btn btn-danger waves-effect waves-light" id="confirm">ชำระเงิน</button>
    </div>
</div>
<script>
    $('#sumary').hide();
    $('#check_all').click(function(){
        if ($(this).is(':checked')) {
            var allcheck = $('.check-booking');
            $(allcheck).each(function(i,e){
                $(this).prop('checked',true);
            });
        }
        else{
            var allcheck = $('.check-booking');
            $(allcheck).each(function(i,e){
                $(this).prop('checked',false);
            });
        }
        sumary();
    });

    $('#confirm').click(function(){
        var allcheck = $('.check-booking:checked');
        var list_id = '';
        $(allcheck).each(function(i,e){
            list_id += (i>0?',':'')+$(this).attr('id');
        });
        
        window.location.href = "{{url('/app/payment?list=')}}"+list_id;
    });

    $('.check-booking').change(function(){
        sumary();
    });
    function sumary(){
        var allcheck = $('.check-booking:checked');
        var amount_total = 0;
        
        $(allcheck).each(function(i,e){
            amount_total = amount_total + parseFloat($(this).attr('price'));
        });
        if($(allcheck).length==0){
            $('#sumary').hide();
        }else{
            $('#sumary').show();
        }

        $('#amount_total').html(numberWithCommas(amount_total));  
    }
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

</script>
@endsection