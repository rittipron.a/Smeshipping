@extends('layouts.customer_app')

@section('content')
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/xls.js' )}}"></script>
<style>
    .col-md-4,
    .col-md-8,
    .col-md-12 {
        padding-top: 5px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <hr style="border: 1px solid rgb(242, 39, 39);
        background-color: white;
        position: inherit;
        top: 100px;
        z-index: 2;">
        <div class="card">
            <div class="card-body">
                <h3>สร้างพัสดุโดย Excel</h3>
                <Br />
                <form id="form" action="{{url('app/upload_booking')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <b>1. ดาวโหลดฟอร์มสำหรับกรอกรายละเอียด Shipment</b>
                            </div>
                            <div class="col-md-12" style="text-align:center">
                                <a href="{{asset('/download/xls_form.xls')}}">
                                    <button type="button" class="btn btn-outline-danger waves-effect waves-light"
                                        style="width:100%">
                                        <i class="mdi mdi-download"></i> Download </button>
                                </a>
                                <br />
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-12">
                                <b>2. กรุณาเลือกบริการที่ต้องการ</b>
                                <div class="row">
                                    <div class="col-md-12">เลือกบริการ*</div>
                                    <div class="col-md-12"><select name="sh_type_of_service" id="sh_type_of_service"
                                            class="form-control">
                                            @foreach ($service_all as $index=>$item)
                                            <option value="{{$item->id}}" routing="{{$item->routing_id}}"
                                                cal="{{$item->cal_weight}}">
                                                {{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12">อัพโหลดไฟล์ Excel*</div>
                                    <div class="col-md-12">
                                        <input type="file" id="my_file_input" name="file" class="form-control"
                                            accept=".xls,.csv" required>

                                    </div>
                                    <div class="col-md-12">คำสั่งพิเศษ</div>
                                    <div class="col-md-12">
                                        <textarea name="m_note" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <b>3. กรุณากรอกข้อมูลที่อยู่ผู้ส่ง</b>
                                    <div class="row">
                                        <input type="hidden" class="form-control pointers" placeholder="โปรดระบุวันที่"
                                            name="delivery_date" id="mdate" required="" data-dtp="dtp_Qyted"
                                            value="{{date('d/m/Y')}}" />
                                        <div class="col-sm-12">ชื่อ*</div>
                                        <div class="col-sm-12">
                                            <div class="input-group">
                                                <input type="text" id="addr_name" name="name" class="form-control"
                                                    maxlength="35" placeholder="" autocomplete="new-password"
                                                    required="">
                                                <span class="input-group-append">
                                                    <button class="btn btn-danger" type="button" data-toggle="modal"
                                                        data-animation="bounce" data-target=".sender_list"
                                                        id="sender_list"><i class="mdi mdi-format-list-bulleted"></i>
                                                        เลือกผู้ส่ง</button>
                                                </span>
                                            </div>
                                            <input type="hidden" name="customer_id" id="addr_customer_id"
                                                class="form-control" value="60337">
                                            <input type="hidden" name="address_id" id="address_id" class="form-control"
                                                value="3513">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">เบอร์มือถือ*</div>
                                        <div class="col-sm-12">
                                            <input type="text" name="phone" id="addr_phone" class="form-control"
                                                placeholder="" required="" autocomplete="new-password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">อีเมล*</div>
                                        <div class="col-sm-12">
                                            <input type="email" id="email" name="addr_email"
                                                class="form-control sh-input" placeholder="" required=""
                                                autocomplete="new-password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">ที่อยู่*</div>
                                        <div class="col-sm-12">
                                            <textarea type="text" name="address" id="addr_address" class="form-control"
                                                placeholder="" required="" autocomplete="new-password"
                                                style=" resize:none;height: 85px;"></textarea>
                                        </div>
                                    </div>
                                    <div style="display:none">
                                        <div class="row">
                                            <div class="col-sm-12">ตำบล / แขวง*</div>
                                            <div class="col-sm-12">
                                                <input name="tumbon" id="tumbon" class="form-control" type="text"
                                                    autocomplete="new-password" placeholder="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">อำเภอ / เขต*</div>
                                            <div class="col-sm-12">
                                                <input name="amphur" id="amphur" class="form-control" type="text"
                                                    autocomplete="new-password" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">จังหวัด*</div>
                                        <div class="col-sm-12">
                                            <input name="province" id="province" class="form-control" type="text"
                                                autocomplete="new-password" placeholder="" required="">
                                        </div>
                                    </div>
                                    <div id="demo1" class="demo" uk-grid="">
                                        <div class="row">
                                            <div class="col-sm-12">รหัสไปรษณีย์*</div>
                                            <div class="col-sm-12">
                                                <input name="postcode" id="postcode" class="form-control" type="text"
                                                    autocomplete="new-password" placeholder="" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="margin-top:15px;">
                                            <button type="submit" id="submit_step3"
                                                class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                                                style="width: 100%;display:none">
                                                <span style="color:red">ถัดไป <i
                                                        class="mdi mdi-arrow-right"></i></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="text-align:center">
                            <br />
                            <button type="submit" class="btn btn-danger btn-lg" style="width:100%">
                                สร้างพัสดุ <i class="mdi mdi-send"></i></button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!--  Modal content for the above example -->
<div class="modal fade sender_list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">เลือกผู้ส่ง</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="background-color: white;">
                <div class="form-group">
                    <label for="example-input2-group1"></label>
                    <div class="input-group">
                        <input type="text" id="find_sender" name="find_sender" class="form-control"
                            placeholder="ค้นหาจากชื่อ หรือ เบอร์โทร">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="mdi mdi-account-search-outline"></i></span>
                        </div>
                    </div>
                </div>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                            <th>ชื่อ</th>
                            <th>ปลายทาง</th>
                            <th>เบอร์ติดต่อ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($address as $index=>$item)
                        <tr style="cursor: pointer;"
                            onclick="set_sender('{{$item->id}}','{{$item->customer_name}}','{{$item->address}}','{{$item->tumbon}}','{{$item->amphur}}','{{$item->province}}','{{$item->postcode}}','{{$item->phone}}','{{$item->email}}')">
                            <td>{{$item->customer_name}}</td>
                            <td>
                                {{$item->address." ".$item->tumbon." ".$item->amphur." ".$item->province." ".$item->postcode}}
                            </td>
                            <td>{{$item->phone}}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->
@if($errors->any())
<!--  Modal content for the above example -->
<div class="modal fade high_light" id="onloadPopup" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLongTitle">สร้างพัสดุเสร็จสิ้น</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="text-align: center;"><img src="{{ asset('/assets/images/success.png') }}" width="50%" />
                </div>
                <div id="modal_tracking"></div>
                <div style="text-align: center; color: red;">พนักงานของเราจะรีบติดต่อกลับไปหาท่านโดยเร็วที่สุดค่ะ</div>
                <div style="text-align: center;">
                    <br />
                    <br />
                    <a href="{{url('/app')}}"><button type="button" class="btn btn-secondary"
                            style="width:100%">กลับหน้าหลัก</button></a>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endif
<script>
    $(function() {
        oFileIn = document.getElementById('my_file_input');
        if (oFileIn.addEventListener) {   
            oFileIn.addEventListener('change', filePicked, false);
        }
    });
    
    function filePicked() {
        debugger
        var oEvent = document.getElementById('my_file_input');
        // Get The File From The Input
        var oFile = oEvent.files[0];
        var sFilename = oFile.name;
        // Create A File Reader HTML5
        var reader = new FileReader();
        
    
        // Ready The Event For When A File Gets Selected
        reader.onload = function(e) {
            try{
                var data = e.target.result;
                var cfb = XLS.CFB.read(data, {
                    type: 'binary'
                });
                var wb = XLS.parse_xlscfb(cfb);
                // Loop Over Each Sheet
                // Obtain The Current Row As CSV
                var sheetName = "Sheet1";
                var sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);
                var data = XLS.utils.sheet_to_json(wb.Sheets[sheetName], {
                    header: 1
                });
                if($(data)[0].length >=20) {
                    $('#my_file_input').addClass('is-valid');
                    $('#my_file_input').removeClass('is-invalid');
                }
                else{
                    $('#my_file_input').removeClass('is-valid');
                    $('#my_file_input').addClass('is-invalid');
                    $('#my_file_input').wrap('<form>').closest('form').get(0).reset();
                    alert('ไฟล์ที่คุณเลือก มีรูปแบบที่ไม่ตรงกับที่ระบบรองรับ โปรดกดดาวโหลดฟอร์มใหม่อีกครั้งจากปุ่ม Download และดำเนินการใหม่อีกครั้ง');
                }
            
            }
            catch(e){
                $('#my_file_input').wrap('<form>').closest('form').get(0).reset();
                    $('#my_file_input').removeClass('is-valid');
                    $('#my_file_input').addClass('is-invalid');
                    alert('ไฟล์ที่คุณเลือก มีรูปแบบที่ไม่ตรงกับที่ระบบรองรับ โปรดกดดาวโหลดฟอร์มใหม่อีกครั้งจากปุ่ม Download และดำเนินการใหม่อีกครั้ง');
            }
        };
    
    
        // Tell JS To Start Reading The File.. You could delay this if desired
        reader.readAsBinaryString(oFile);
    }
    
</script>
<script src="{{ asset('assets/js/app_eship.js?2' )}}"></script>

@if($address->where('pickup',1)->first())
<?php $address_item = $address->where('pickup',1)->first();?>
<script>
    $('#addr_name').val('{{$address_item->customer_name}}');
    $('#addr_phone').val('{{$address_item->phone}}');
    $('#addr_address').val('{{$address_item->address}}');
    $('#province').val('{{$address_item->province}}');
    $('#email').val('{{$address_item->email}}');
    $('#amphur').val('{{$address_item->amphur}}');
    $('#tumbon').val('{{$address_item->tumbon}}');
    $('#postcode').val('{{$address_item->postcode}}');
    $('#address_id').val('{{$address_item->id}}');
</script>

@endif
@endsection