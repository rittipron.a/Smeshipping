@extends('layouts.customer_app_hidden_nav')

@section('title','Customer login -')

@section('content')
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/select2/select2.min.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/css/style.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
    rel="stylesheet" />

<link rel="stylesheet" href="{{ asset('assets/dist/jquery.Thailand.min.css') }}">
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script type="text/javascript" src="{{ asset('assets/dependencies/JQL.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('assets/dependencies/typeahead.bundle.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/dist/jquery.Thailand.min.js') }}">
</script>
<style>
    <style>.row {
        padding-bottom: 10px;
        padding-top: 10px;
        width: 100%;
        color: black;
    }

    input.largerCheckbox {
        width: 20px;
        height: 20px;
        margin-top: 10px;
    }

    #sumary {
        position: fixed;
        left: 0;
        bottom: 0;
        padding: unset;
    }

    #sumary .col-md-3 {
        width: 100%;
        background-color: white;
        text-align: center;
        box-shadow: 0px 0px 3px rgba(31, 30, 47, 0.05);
        padding: 10px;
    }

    #sumary .col-md-3 button {
        float: right;
    }

    #amount_total {
        color: red;
        font-size: 25px;
    }

    #dLabel {
        width: 240px;
        height: 40px;
        border-radius: 4px;
        background-color: #fff;
        border: solid 1px #cccccc;
        text-align: left;
        padding: 7.5px 15px;
        color: #ccc;
        letter-spacing: 0.7px;
        margin-top: 25px;
    }

    .font_tracking {
        width: 100%;
        height: 100%;
        font-size: 23px;
        text-align: right;
    }

    .tracking {
        text-align: right;
        font-size: 18px;
    }

    .highlight {
        color: red;
        padding: 0 0 0 21px;
    }

    .form-control.is-valid, .was-validated .form-control:valid{
    border: 1px solid #e8ebf3;
    padding-right: calc(1.5em + .75rem);
    background-image: url();
    background-repeat: no-repeat;
    background-position: center right calc(.375em + .1875rem);
    background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }

    .form-control.is-invalid, .was-validated .form-control:invalid{
        border-color: #dc3545;
        padding-right: calc(1.5em + .75rem);
        background-image: url();
        background-repeat: no-repeat;
        background-position: center right calc(.375em + .1875rem);
        background-size: calc() calc(.75em + .375rem);
    }

    .form-control.is-valid:focus,
    .was-validated .form-control:valid:focus {
        border-color: #e8ebf3;
        box-shadow: 0 0 0 0rem ;
    }

    @media only screen and (max-width: 768px) {
        .font_tracking {
            width: 100%;
            height: 100%;
            font-size: 18px;
            text-align: right;
            padding: 4px 0 0 0;
        }

        .space_div {
            margin: 20px 0 20px 0;
        }

        .highlight {
            font-size: 15px;
        }

        .tracking {
            margin: 20px 0 0 0;
            text-align: center;
            font-size: 15px;
        }
    }

    textarea {
        resize: none;
        height: 85px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        @if($data->sender_name == null)
        <div id="step_1">
            <form name="form_step_1" id="form_step_1" action="#" novalidate>
                <div class="card">
                    <div class="card-body">
                        <div style="text-align: center;"><img src="{{ asset('/assets/images/logo.png') }}"
                                width='50%' />
                        </div>
                        <div class="tracking">Tracking :
                            <span>{{$data->tracking}}<span>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <br />
                    <div class="row" style="padding: 0 0 0 0;">
                        <div class="col-8 col-md-12">
                            <span style="font-size: 17px;padding: 0 0 0 23px;"> กรอกข้อมูลการจัดส่ง</span>
                        </div>
                        <div class="col-4 col-md-12">
                            <span class="highlight">*จำเป็น</span>
                        </div>
                    </div><br />
                </div>
                <div class="card">
                    <div class="card-body">
                        <div clss="row">
                            <div class="col-12 col-md-12">
                                <div>Sender / ข้อมูลผู้ส่ง<span style="color:red;">*</span></div>
                            </div>

                            <div class="col-12 col-md-12">
                                <div>
                                    <input type="text" id="sender_name" name="sender_name" class="form-control"
                                        placeholder="" autocomplete="new-password" required="">
                                        <div class="invalid-feedback">
                                            *กรุณากรอกSender/ข้อมูลผู้ส่ง
                                        </div>
                                </div>
                            </div><br />

                            <div class="col-12 col-md-12">
                                <div>Sender Address / ที่อยู่ผู้ส่ง<span style="color:red;">*</span></div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div>
                                    <input type="text" id="sender_address" name="sender_address" class="form-control"
                                        placeholder="" autocomplete="new-password" required="">
                                        <div class="invalid-feedback">
                                            *กรุณากรอกSender Address/ที่อยู่ผู้ส่ง
                                        </div>
                                </div>
                            </div><br />

                            <div class="col-12 col-md-12">
                                <div>Sender Telephone No. / เบอร์โทร<span style="color:red;">*</span></div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div>
                                    <input type="text" id="sender_tel" name="sender_tel" class="form-control"
                                        placeholder="" autocomplete="new-password" required=""
                                        onkeypress="return isNumberKey(event)">
                                        <div class="invalid-feedback">
                                            *กรุณากรอกSender Telephone No./เบอร์โทร
                                        </div>
                                </div><br />
                            </div>

                            <div class="col-12 col-md-12">
                                <div>Sender E-mail</div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div>
                                    <input type="email" id="sender_email" name="sender_email" class="form-control"
                                        placeholder="" autocomplete="new-password">
                                </div>
                            </div><br />
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger" type="submit" style="width: 100%;">
                    ถัดไป</button><br>
            </form>
        </div>
        <!-- Step 2 -->
        <div id="step_2">
            <form id="form_step_2" name="form_step_2" action="#" novalidate>
                <div class="card"><br />
                    <div style="font-size: 17px;padding: 0 0 0 23px;">กรุณากรอกรายละเอียดของผู้ส่ง</div> <br />
                </div>
                <div class="card">
                    <div class="card-body">
                        <div clss="row">
                            <div class="col-12 col-md-12">
                                <div>Receiver Name / ชื่อผู้รับ<span style="color:red;">*</span></div>
                            </div>
                            <div class="col-12 col-md-12">
                                <input type="text" id="receiver_name" name="receiver_name" class="form-control"
                                    placeholder="" autocomplete="new-password" required="">
                                    <div class="invalid-feedback">
                                            *กรุณากรอกReceiver Name / ชื่อผู้รับ
                                        </div>
                            </div><br />


                            <div class="col-12 col-md-12">
                                <div>Receiver Address / ที่อยู่ผู้รับ<span style="color:red;">*</span></div>
                            </div>
                            <div class="col-12 col-md-12">
                                <input type="text" id="receiver_address" name="receiver_address" class="form-control"
                                    placeholder="" autocomplete="new-password" required="">
                                    <div class="invalid-feedback">
                                        *กรุณากรอกReceiver Address / ที่อยู่ผู้รับ
                                    </div>
                            </div><br />


                            <div class="col-12 col-md-12">
                                <div>Receiver Telephone No. / เบอร์โทรผู้รับ<span style="color:red;">*</span>
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                                <input type="text" id="receiver_tel" name="receiver_tel" class="form-control"
                                    placeholder="" autocomplete="new-password" required=""
                                    onkeypress="return isNumberKey(event)">
                                    <div class="invalid-feedback">
                                        *กรุณากรอกReceiver Telephone No. / เบอร์โทรผู้รับ
                                    </div>
                            </div><br />

                            <div class="col-12 col-md-12">
                                <div>Receiver E-mail</div>
                            </div>
                            <div class="col-12 col-md-12">
                                <input type="email" id="receiver_email" name="receiver_email" class="form-control"
                                    placeholder="" autocomplete="new-password">
                            </div>
                        </div>
                    </div>
                </div><button class="btn btn-danger" type="submit" style="width: 100%;"> ถัดไป</button><br>
            </form>
        </div>
        <!-- Step 3  -->
        <div id="step_3">
            <form id="form_step_3" name="form_step_3" action="#" novalidate>
                <div class="card">
                    <br />
                    <div style="font-size: 17px;padding: 0 0 0 23px;">กรุณากรอกรายละเอียดของสินค้าที่จัดส่ง</div>
                    <br />
                </div>
                <div class="card">
                    <div class="card-body">
                        <div clss="row">
                            <div class="col-12 col-md-12">
                                <div> Goods (สินค้า) / Quantity (จำนวน) / Value (มูลค่า) BAHT (บาท)</div>
                                <div>รายการสินค้าทั้งหมดที่จะจัดส่ง หนึ่งรายการต่อบรรทัด เช่น เสื้อผู้ชาย สีขวา / 10
                                    ตัว/ 5000 บาท<span style="color:red;">*</span></div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div>
                                    <textarea type="text" id="shipdetail" name="shipdetail" class="form-control"
                                        placeholder="" autocomplete="new-password" required=""></textarea>
                                        <div class="invalid-feedback">
                                        *กรุณากรอกรายการสินค้าทั้งหมดที่จะจัดส่ง
                                    </div>
                                </div><br />
                            </div>
                            <div class="col-12 col-md-12">
                                <div>Commodity Description / ชื่อประเภทสินค้า<span style="color:red;">*</span></div>
                            </div>
                            <div class="col-12 col-md-12">
                                <input type="text" id="commodity_description" name="commodity_description"
                                    class="form-control" placeholder="" autocomplete="new-password" required="">
                                    <div class="invalid-feedback">
                                        *กรุณากรอกCommodity Description / ชื่อประเภทสินค้า
                                    </div>
                            </div><br />

                            <div class="col-12 col-md-12">
                                <div>Quantity Unit / จำนวนในกล่อง<span style="color:red;">*</span></div>
                            </div>
                            <div class="col-12 col-md-12">
                                <input type="text" id="quantity_unit" name="quantity_unit" class="form-control"
                                    placeholder="" autocomplete="new-password" required=""
                                    onkeypress="return isNumberKey(event)">
                                    <div class="invalid-feedback">
                                        *กรุณากรอกQuantity Unit / จำนวนในกล่อง
                                    </div>
                            </div><br />

                            <div class="col-12 col-md-12">
                                <div>Value / มูลค่าสินค้า<span style="color:red;">*</span></div>
                            </div>

                            <div class="col-12 col-md-12">
                                <input type="text" id="value" name="value" class="form-control" placeholder=""
                                    autocomplete="new-password" required="" onkeypress="return isNumberKey(event)">
                                    <div class="invalid-feedback">
                                        *กรุณากรอกValue / มูลค่าสินค้า
                                    </div>
                            </div><br />
                            <div class="col-12 col-md-12">
                                <div>Currency / สกุลเงินของสินค้า<span style="color:red;">*</span></div>
                            </div>
                            <div class="col-12 col-md-12">
                                <select class="form-control form-control-xs selectpicker" name="currency" data-size="7"
                                    data-live-search="true" data-title="Location" id="currency" data-width="100%"
                                    width="50%" required>
                                    <option value="thb" selected>THB</option>
                                    <option value="usa">USA</option>
                                </select>
                                <div class="invalid-feedback">
                                        *กรุณาเลือกCurrency / สกุลเงินของสินค้า
                                </div>
                            </div><br />
                            <div class="col-12 col-md-12">
                                <div>Total Packages / จำนวนกล่องทั้งหมด <span style="color:red;">*</span></div>
                            </div>
                            <div class="col-12 col-md-12">
                                <input type="numder" id="total_packages" name="total_packages" class="form-control"
                                    placeholder="" autocomplete="new-password" required=""
                                    onkeypress="return isNumberKey(event)">
                                    <div class="invalid-feedback">
                                        *กรุณากรอกTotal Packages / จำนวนกล่องทั้งหมด
                                    </div>
                            </div><br />

                            <div class="col-12 col-md-12">
                                <div>Note / หมายเหตุ</div>
                            </div>
                            <div class="col-12 col-md-12">
                                <input type="text" id="note" name="note" class="form-control" placeholder=""
                                    autocomplete="new-password">
                            </div>
                        </div>
                    </div>
                </div>
                <div style="display:none">
                    <input type="text" id="uuid" name="uuid" value="{{$data->uuid}}">
                    <input type="text" id="tracking" name="tracking" value="{{$data->tracking}}">
                </div>
                <button type="submit" class="btn btn-danger" style="width: 100%;"> Submit</button>
            </form>
        </div>
        @endif
        <!-- END -->
        @if($data->sender_name != null && $data->dalivery_tracking == null)
        <div class="row">
            <div class="col-12 col-md-12">
                <form id="dalivery_sme" name="dalivery_sme" novalidate>
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <h3>เลือกการขนส่ง</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5 col-md-2">
                                            <div class="font_tracking">Tracking :</div>
                                        </div>
                                        <div class="col-7 col-md-4">
                                            <div style="width: 100%;font-size: 23px;">
                                                <input type="text" id="dalivery_tracking" name="dalivery_tracking"
                                                    class="form-control" placeholder="" autocomplete="new-password"
                                                    required="">
                                                    <div class="invalid-feedback">
                                                        *กรุณากรอกTracking
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-3 space_div">
                                            <select class="form-control form-control-xs selectpicker" name="dalivery"
                                                data-size="7" data-live-search="true" data-title="Location"
                                                id="dalivery" data-width="100%">
                                                <option value="lineman" selected>Lineman</option>
                                                <option value="kerry">Kerry</option>
                                                <option value="ไปรษณีย์">ไปรษณีย์</option>
                                                <option value="lalamove">Lalamove</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <div style="text-align: center;">
                                                <button type="Submit" id="complete" class="btn btn-danger"
                                                    style="width: 50%;">Submit</button>
                                            </div>
                                        </div>
                                        <div style="display:none">
                                            <input type="text" id="uuid" name="uuid" value="{{$data->uuid}}">
                                            <input type="text" id="tracking" name="tracking"
                                                value="{{$data->tracking}}">
                                            <input type="text" id="sender_name" name="sender_name"
                                                value="{{$data->sender_name}}">
                                            <input type="text" id="sender_address" name="sender_address"
                                                value="{{$data->sender_address}}">
                                            <input type="text" id="sender_tel" name="sender_tel"
                                                value="{{$data->sender_tel}}">
                                            <input type="text" id="sender_email" name="sender_email"
                                                value="{{$data->sender_email}}">
                                            <input type="text" id="receiver_name" name="receiver_name"
                                                value="{{$data->receiver_name}}">
                                            <input type="text" id="receiver_address" name="receiver_address"
                                                value="{{$data->receiver_address}}">
                                            <input type="text" id="receiver_tel" name="receiver_tel"
                                                value="{{$data->receiver_tel}}">
                                            <input type="text" id="receiver_email" name="receiver_email"
                                                value="{{$data->receiver_email}}">
                                            <input type="text" id="commodity_description" name="commodity_description"
                                                value="{{$data->receiver_acommodity_descriptionddress}}">
                                            <input type="text" id="quantity_unit" name="quantity_unit"
                                                value="{{$data->quantity_unit}}">
                                            <input type="text" id="currency" name="currency"
                                                value="{{$data->currency}}">
                                            <input type="text" id="total_packages" name="total_packages"
                                                value="{{$data->total_packages}}">
                                            <input type="text" id="note" name="note" value="{{$data->note}}">
                                            <input type="text" id="value" name="value" value="{{$data->value}}">
                                            <input type="text" id="shipdetail" name="shipdetail"
                                                value="{{$data->shipdetail}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endif

        <!-- Thank -->
        @if($data->sender_name != null && $data->dalivery != null)
        <div class="row">
            <div class="col-md-12">
                <hr style="border: 1px solid rgb(242, 39, 39);
        background-color: white;
        position: inherit;
        top: 80px;
        z-index: 2;">
                <div class="card">
                    <div class="card-body">
                        <h3 style="text-align:left">Thank You</h3>
                    </div>
                    <div class="col-4" style="margin: auto; padding: 0 0 26px 0;">
                        <img src="{{ asset('/assets/images/success.png') }}" width="100%" />
                    </div>
                </div>
            </div>
        </div>
        @endif

        <div class="row" style="display:none;" id="thank_you">
            <div class="col-md-12">
                <hr style="border: 1px solid rgb(242, 39, 39);
        background-color: white;
        position: inherit;
        top: 80px;
        z-index: 2;">
                <div class="card">
                    <div class="card-body">
                        <h3 style="text-align:left">Thank You</h3>
                    </div>
                    <div class="col-4" style="margin: auto; padding: 0 0 26px 0;">
                        <img src="{{ asset('/assets/images/success.png') }}" width="100%" />
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' )}}">
</script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}">
</script>
<script src="{{ asset('assets/pages/jquery.forms-advanced.js?1' )}}"></script>
<script src="{{ asset('assets/js/jquery.mask.min.js?1.1' )}}"></script>

<script>
    function nextstep(step) {
    $('#step_1').hide();
    $('#step_2').hide();
    $('#step_3').hide();
    $('#step_' + step).show();
}

$("#dalivery_sme").on("submit",function(e) {
    var form = $(this)[0];
    debugger;
    if (form.checkValidity() === false) {
        e.preventDefault();
        e.stopPropagation();
        form.classList.add('was-validated');
    }else{
        e.preventDefault();
        $.ajax({
        type: "POST",
        data: $("#dalivery_sme").serialize(),
        url: '/app/customer',
        success: function(result) {
            window.location.reload();
        },
        error: function(e) {
            console.log("ERROR : ", e);
            }
        });
    }
});


$("#form_step_1").on("submit", function(e) {
    var form = $(this)[0];
    if (form.checkValidity() === false) {
        e.preventDefault();
        e.stopPropagation();
        form.classList.add('was-validated');
    }else{
    e.preventDefault();
    nextstep(2);
    }
});

$("#form_step_2").on("submit", function(e) {
    var form = $(this)[0];
    if (form.checkValidity() === false) {
        e.preventDefault();
        e.stopPropagation();
        form.classList.add('was-validated');
    }else{
    e.preventDefault();
    nextstep(3);
    }
});

$("#form_step_3").on("submit", function(e) {
    $("#loading").show();
    var form = $(this)[0];
    if (form.checkValidity() === false) {
        e.preventDefault();
        e.stopPropagation();
        form.classList.add('was-validated');
    }else{
    e.preventDefault();
    $.ajax({
        type: "POST",
        data: $("#form_step_1,#form_step_2,#form_step_3").serialize(),
        url: '/app/customer',
        success: function(result) {
            $("#loading").hide();
            alert("ข้อมูลที่ส่งเสร็จสิ้น");
            nextstep(10);
            $('#thank_you').show();
        },
        error: function(e) {
            console.log("ERROR : ", e);
            alert(
                "An error occurred on the server when processing the URL. Please contact the system administrator."
            );
            $("#loading").hide();
            }
        });
    }
});

function isNumberKey(evt) {
    var charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
@endsection