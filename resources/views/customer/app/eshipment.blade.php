@extends('layouts.customer_app')
@section('content')
<link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/select2/select2.min.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/css/style.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/css/style_app_eship.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
    rel="stylesheet" />

<link rel="stylesheet" href="{{ asset('assets/dist/jquery.Thailand.min.css') }}">
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script type="text/javascript" src="{{ asset('assets/dependencies/JQL.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('assets/dependencies/typeahead.bundle.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/dist/jquery.Thailand.min.js') }}">
</script>
<style>
body,
p,
.label {
    color: rgba(101, 98, 105, 1);
    font-family: 'Prompt', sans-serif;
    font-size: 16px;
}

.client-card {
    margin-bottom: 10px !important;
}

.btn-next {
    background: white !important;
    border: unset !important;
    color: #3677b6 !important;
    font-weight: bold;
    font-size: 18px;
    filter: drop-shadow(rgba(0, 0, 0, 0.15) 0px 3px 6px);
}

.btn {
    color: white;
    font-family: 'Prompt', sans-serif;
    font-size: 16px !important;
}

.w-col-3 span {
    color: #3677b6 !important;
    font-weight: bold;
}

.custom-control-label::after {
    position: absolute;
    top: .25rem;
    left: -1.5rem;
    display: block;
    width: 2rem;
    height: 2rem;
    content: "";
    background: no-repeat 50%/50% 50%;
}

.custom-control-label::before {
    position: absolute;
    top: .25rem;
    left: -1.5rem;
    display: block;
    width: 2rem;
    height: 2rem;
    pointer-events: none;
    content: "";
    background-color: #fff;
    border: #adb5bd solid 1px;
}

.delete-item {
    color: #f22727;
    font-size: 25px;
    right: 10px;
    top: 5px;
    position: absolute;
    cursor: pointer;
}

.edit-item {
    width: 100%;
    margin-top: 10px;
    border: 2px dashed #3677b6 !important;
    border-radius: 10px 10px 10px 10px !important;
    color: #3677b6 !important;
    filter: unset;
}

.sumary_data .card {
    padding: 15px;
}

#step2,
#step3,
#step4,
#step5,
#step6 {
    display: block;
}

.modal-content .modal-header,
.modal-content .modal-footer {
    border-color: #ff0000;
    background-color: #ffffff;
}

.eship_space {
    margin-top: -40px;
    filter: drop-shadow(rgba(0, 0, 0, 0.15) 0px 3px 6px);
}

.textarea {
    resize: none;
}

.pointer {
    cursor: pointer;
    padding-top: 5px;
}

.pointers {
    cursor: pointer;
}

/* Style STEP 6 */
.font_weight {
    font-weight: bold;
    font-size: 18px;
}

.space_line {
    margin: 0px 0px 27px 0;
    border-top: 1px solid;
}

.dis_space {
    padding: 0 0 0 0;
}

.space_detail {
    padding-left: 15px;
}

.space_topic {
    padding-left: 11px;
    padding-bottom: 10px;
}

.space_topic_report {
    padding-left: 4px;
    padding-top: 9px;
}

.space_right {
    text-align: right;
    padding: 0 16px 0 0;
}

.space_total {
    padding-left: 13px;
}

.toppic {
    padding-top: 10px;
}

.space_datetime {
    padding-left: 23px;
}

.mb_font {
    padding-right: 12px;
    padding-left: 12px;
}

.delete_paddingleft {
    padding-left: 0px;
}

.font_service {
    font-size: 18px;
    padding-top: 16px;
    width: 70%;
}

.box_del_add {
    font-size: 13px;
    top: 0px;
    position: relative;
    cursor: pointer;
    z-index: 2;
    float: right;
}
.select_sevice{
    font-size: 14px;
    width: 22%;
    padding: 1%;
    text-align: center;
    border: 1px solid;
    border-radius: 7px;
    background-color: #f22727;
    color: #ffff;
}
.gettracking{
    font-size: 56px;
    color: red;
    font-weight: bold;
    text-align: center;
    line-height: 1;
}
.number_tracking{
    padding-left: 24%;
}
.button_style{
    width: 66%;
}

.form-control.is-valid, .was-validated .form-control:valid{
    border: 1px solid #e8ebf3;
    padding-right: calc(1.5em + .75rem);
    background-image: url();
    background-repeat: no-repeat;
    background-position: center right calc(.375em + .1875rem);
    background-size: calc(.75em + .375rem) calc(.75em + .375rem);
    }

.form-control.is-invalid, .was-validated .form-control:invalid{
    border-color: #dc3545;
    padding-right: calc(1.5em + .75rem);
    background-image: url();
    background-repeat: no-repeat;
    background-position: center right calc(.375em + .1875rem);
    background-size: calc() calc(.75em + .375rem);
    }

.form-control.is-valid:focus,
.was-validated .form-control:valid:focus {
	border-color: #e8ebf3;
	box-shadow: 0 0 0 0rem ;
}

/* mobile */
@media screen and (max-width: 992px) {
    .font_small {
        font-size: 13px;
    }
    .mb_font {
        font-size: 15px;
    }
    .delete_paddingright {
        padding-right: 0px;
    }

    .delete_paddingleft {
        padding-left: 0px;
    }

    .mb_font_small {
        font-size: 10px;
    }

    .mb_space {
        padding-top: 22px;
    }

    .space_datetime {
        padding-left: 12px;
    }
    .space_quantity{
        padding-top: 10px;
    }
    .select_sevice{
    font-size: 14px;
    width: 35%;
    padding: 1%;
    text-align: center;
    border: 1px solid;
    border-radius: 7px;
    background-color: #f22727;
    color: #ffff;
    }
    .number_tracking{
        padding-left: 8%;
    }
    .button_style{
        width: 100%;
    }
}

@media screen and (max-width: 768px) {
    .font_service {
        font-size: 16px;
        padding-top: 10px;
        width: 70%;
    }

    .mb_font_small {
        font-size: 13px;
    }
}

.next_step{
    width: 40%;
    padding-bottom: 5%;
    padding-top: 5%;
    border-radius: 10px;
    background-color: #f22727;
    color: #ffffff;
    border: 1px solid;
    font-weight: bold;
}
.cancel_service{
    width: 30%;
    padding-bottom: 5%;
    padding-top: 5%;
    border-radius: 10px;
    background-color: #ffffff;
    color: #f22727;
    border: 1px solid;
    font-weight: bold;
}

}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-12 col-md-10">
                <input type="hidden" id="edit_number" value="" />
                <input type="hidden" id="service_number" value="{{app('request')->input('service')}}" />
                <!-- STEP 1 -->
                <div class="row" id="step_1">
                    <div class="card">
                        <form name="form_step_1" id="form_step_1" action="#" novalidate>
                            <div class="col-md-12">
                                <h3 style="text-align:center">
                                    <b>เช็คราคาส่งพัสดุ</b>
                                </h3>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <i class="mdi mdi-airplane"></i> ส่งไปประเทศ<br />
                                    </div>
                                    <div class="col-md-12">
                                        <select class="select2 form-control mb-3 custom-select" id="sh_country"
                                            name="sh_country" required>
                                            @foreach ($country_all as $c_item)
                                            <option value="{{$c_item->country_name}}" code="{{$c_item->country_cd}}"
                                                iata="{{$c_item->country_cd}}">
                                                {{$c_item->country_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <i class="mdi mdi-scale"></i>  น้ำหนัก (KG)<br />
                                    </div>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="weight" id="sh_weight"
                                            placeholder="" autocomplete="off" onkeypress="return isNumberKey(event)"
                                            onkeyup="cal_box()" required>
                                            <div class="invalid-feedback">
                                                กรุณากรอกน้ำหนัก(KG)
                                             </div>
                                        <div>
                                            <span style="position: absolute;top: 95px;right: 20px">KG</span>
                                        </div>
                                    </div>
                                    <br />

                                    <div class="col-md-12" id="box_area">
                                        @if(app('request')->input('sh_country')!=null)
                                        @foreach(app('request')->input('width') as $index=>$width)
                                        <div class="row box">
                                            <div class="col-12" style="padding-bottom:10px;">
                                                <i class="mdi mdi-dropbox"></i>
                                                ขนาดกล่อง <span class="box_number">{{$index+1}}</span><br />
                                            </div>
                                            <div class="col-4">
                                                <input type="number" name="width[]" class="form-control width"
                                                    style="text-align:center" placeholder="กว้าง (CM)"
                                                    onkeypress="cal_box()" value="{{app('request')->input('width')[$index]}}">
                                            </div>
                                            <div class="col-4">
                                                <input type="number" name="length[]" class="form-control length"
                                                    style="text-align:center" placeholder="ยาว (CM)"
                                                    onkeypress="cal_box()" value="{{app('request')->input('length')[$index]}}">
                                            </div>
                                            <div class="col-4">
                                                <input type="number" name="height[]" class="form-control height"
                                                    style="text-align:center" placeholder="สูง (CM)"
                                                    onkeypress="cal_box()" value="{{app('request')->input('height')[$index]}}">
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="row box">
                                            <div class="col-12" style="padding-bottom:10px;">
                                                <i class="mdi mdi-dropbox"></i>
                                                ขนาดกล่อง <span class="box_number">1</span><br />
                                            </div>
                                            <div class="col-4">
                                                <input type="number" name="width[]" class="form-control width"
                                                    style="text-align:center" placeholder="กว้าง (CM)"
                                                    onkeypress="cal_box()">
                                            </div>
                                            <div class="col-4">
                                                <input type="number" name="length[]" class="form-control length"
                                                    style="text-align:center" placeholder="ยาว (CM)"
                                                    onkeypress="cal_box()">
                                            </div>
                                            <div class="col-4">
                                                <input type="number" name="height[]" class="form-control height"
                                                    style="text-align:center" placeholder="สูง (CM)"
                                                    onkeypress="cal_box()">
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    <div style="padding-bottom:10px;padding-left:unset; display:none;">
                                        <input type="number" id="box_total" class="form-control"
                                            style="text-align:center" placeholder="กล่อง" required="" value="1"
                                            minlength="1">
                                    </div>
                                    <div class="col-12" style="text-align:center">
                                        <input type="hidden" id="volumnWeight" name="volumnWeight" />
                                        <button type="button" class="btn btn-danger waves-effect waves-light"
                                            style="margin-top:10px;" onclick="addbox()"><i
                                                class="mdi mdi-plus-circle-outline"></i>
                                            เพิ่มกล่อง</button>
                                    </div>
                                </div>
                                <!-- service -->
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div id="sercive_price" class="mb-0 desc-topic" style="display:none;">ราคาและบริการ</div>
                                        <div class="mb-0" id="service_wait_data"></div>
                                        <div class="row service_select" id="service_select" style="display:none">
                                        </div>
                                    </div>
                                </div>
                                <!-- end service -->
                            </div>
                                <div class="col-md-12" style="margin-top:15px; display:none;">
                                    <button type="submit" id="submit_step1"
                                        class="btn btn-danger  btn-square  waves-effect waves-light"
                                        style="width: 100%;">
                                        <span>ถัดไป <i class="mdi mdi-arrow-right"></i></span></button>
                                </div>
                        </form>
                    </div>
                </div>
                <!-- END STEP 1 -->

                <!-- STEP 2 -->
                <div class="row" id="step_2">
                    <div class="card">
                        <form name="form_step_2" id="form_step_2" action="#" novalidate>
                            <div class="col-12">
                                <h3 style="text-align:center"><u>ข้อมูลผู้ส่ง</u></h3>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="col-12 col-md-12 toppic">ชื่อ*</div>
                                    <div class="col-12 col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="addr_name" name="name" class="form-control"
                                                maxlength="35" placeholder="" autocomplete="new-password" required="">
                                            <span class="input-group-append">
                                                <button class="btn btn-danger" type="button" data-toggle="modal"
                                                    data-animation="bounce" data-target=".sender_list"
                                                    id="sender_list"><i class="mdi mdi-format-list-bulleted"></i>
                                                    เลือกผู้ส่ง</button>
                                            </span>
                                            <div class="invalid-feedback">
                                                    กรุณากรอกชื่อ
                                                </div>
                                        </div>
                                        <input type="hidden" name="customer_id" id="addr_customer_id"
                                            class="form-control" value="{{session('cus.id')}}">
                                        <input type="hidden" name="address_id" id="address_id" class="form-control"
                                            value="">
                                    </div>


                                    <div class="col-12 col-md-12 toppic">เบอร์มือถือ*</div>
                                    <div class="col-12 col-md-12">
                                        <input type="text" name="phone" id="addr_phone" class="form-control"
                                            placeholder="" required="" autocomplete="new-password">
                                            <div class="invalid-feedback">
                                                    กรุณากรอกเบอร์มือถือ
                                            </div>
                                    </div>


                                    <div class="col-12 col-md-12 toppic">อีเมล*</div>
                                    <div class="col-12 col-md-12">
                                        <input type="email" id="email" name="email" class="form-control sh-input"
                                            placeholder="" required="" autocomplete="new-password">
                                            <div class="invalid-feedback">
                                                    กรุณากรอกอีเมล
                                            </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="col-12 col-md-12 toppic">ที่อยู่*</div>
                                    <div class="col-12 col-md-12">
                                        <textarea type="text" name="address" id="addr_address" class="form-control"
                                            placeholder="" required="" autocomplete="new-password"
                                            style=" resize:none;height: 85px;"></textarea>
                                            <div class="invalid-feedback">
                                                    กรุณากรอกที่อยู่
                                            </div>
                                    </div>

                                    <div class="col-12 col-md-12 toppic">จังหวัด*</div>
                                    <div class="col-12 col-md-12">
                                        <input name="province" id="province" class="form-control" type="text"
                                            autocomplete="new-password" placeholder="" required="">
                                            <div class="invalid-feedback">
                                                    กรุณากรอกจังหวัด
                                            </div>
                                    </div>


                                    <div class="col-12 col-md-12 toppic">รหัสไปรษณีย์*</div>
                                    <div id="demo1" class="demo" uk-grid>
                                        <div class="col-12 col-md-12">
                                            <input name="postcode" id="postcode" class="form-control" type="text"
                                                autocomplete="new-password" placeholder="" required="">
                                                <div class="invalid-feedback">
                                                    กรุณากรอกรหัสไปรษณีย์
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <script>
                                 $.Thailand({
                                  $district: $('#demo1 [name="tumbon"]'),
                                  $amphoe: $('#demo1 [name="amphur"]'),
                                  $zipcode: $('#demo1 [name="postcode"]'),

                                  onDataFill: function(data) {
                                    console.info('Data Filled', data);
                                  },

                                onLoad: function() {
                                         console.info('Autocomplete is ready!');
                                         //$('#loader, .demo').toggle();
                                    }
                                });
                                </script> -->

                            <div style="display:none">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="radio" name="time" value="พร้อมรับ" checked="">
                                        พร้อมรับ <br>
                                        <input type="radio" name="time" value="ระบุเวลา"> ระบุเวลา
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">คำสั่งถึงพนักงาน :</div>
                                    <div class="col-sm-8">
                                        <textarea name="m_note" class="form-control" maxlength="255"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div style="margin-top:15px;">
                                <button type="submit" id="submit_step2"
                                    class="btn btn-danger  btn-square  waves-effect waves-light"
                                    style="width: 100%;display:none">
                                    <span >ถัดไป <i class="mdi mdi-arrow-right"></i></span></button>
                            </div>
                        </form>
                    </div>

                </div>
                <!-- END STEP 2 -->

                <!-- STEP 3 -->
                <div class="row" id="step_3">
                    <form name="form_step_3" id="form_step_3" action="#" novalidate>
                        <div class="card">
                            <div class="col-12 col-md-12">
                                <h3 style="text-align:center"><u>ข้อมูลผู้รับ</u></h3>
                            </div>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="col-12 col-md-12 toppic">ชื่อผู้รับ*</div>

                                    <div class="col-12 col-md-12 input-group">
                                        <input type="text" id="sh_recipient" name="recipient[0]" class="form-control"
                                            maxlength="35" autocomplete="new-password" required>
                                        <span class="input-group-append">
                                            <button class="btn btn-danger" type="button" data-toggle="modal"
                                                data-animation="bounce" data-target=".recipient_list"
                                                id="recipient_list"><i class="mdi mdi-format-list-bulleted"></i>
                                                เลือกผู้รับ</button>
                                        </span>
                                        <div class="invalid-feedback">
                                            กรุณากรอกชื่อผู้รับ
                                        </div>
                                    </div>
                                    <input type="hidden" name="sh_id" id="sh_id" value="3">
                                    <div class="col-12 col-md-12 toppic">เบอร์มือถือ*</div>
                                    <div class="col-12 col-md-12">
                                        <input type="text" id="sh_phone" name="sh_phone" class="form-control sh-input"
                                            placeholder="" required="" maxlength="25" autocomplete="new-password">
                                            <div class="invalid-feedback">
                                                กรุณากรอกเบอร์มือถือ
                                            </div>
                                    </div>
                                    <div class="col-12 col-md-12 toppic">อีเมล</div>
                                    <div class="col-12 col-md-12">
                                        <input type="email" id="sh_email" name="sh_email" class="form-control sh-input"
                                            placeholder="" autocomplete="new-password">
                                    </div>

                                    <div class="col-12 col-md-12 toppic">ที่อยู่*</div>
                                    <div class="col-12 col-md-12">
                                        <textarea id="sh_address1" name="sh_address1" class="form-control sh-input"
                                            required="" autocomplete="new-password"
                                            style="resize:none; height: 85px;"></textarea>
                                            <div class="invalid-feedback">
                                                กรุณากรอกที่อยู่
                                            </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="col-12 col-md-12 toppic">รหัสไปรษณีย์</div>
                                    <div class="col-12 col-md-12">
                                        <input type="text" id="sh_postcode" name="sh_postcode"
                                            class="form-control sh-input" placeholder="" autocomplete="new-password">
                                        <span id="process_postcode" style="display:none">กำลังตรวจสอบ<div
                                                class="spinner-border spinner-border-sm" role="status">
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-12 col-md-12 toppic">เมือง</div>
                                    <div class="col-12 col-md-12">
                                        <input type="text" list="select_city" id="sh_city" name="sh_city"
                                            class="form-control sh-input" placeholder="" autocomplete="new-password">
                                        <datalist class="form-control" id="select_city" style="display:none">
                                        </datalist>
                                    </div>
                                    <div class="col-12 col-md-12 toppic">ประเทศปลายทาง*</div>
                                    <div class="col-12 col-md-12" style="padding-top:6px">
                                        <input type="text" id="s_country" name="s_country" class="form-control sh-input"
                                            readonly>
                                            <div class="invalid-feedback">
                                                กรุณากรอกประเทศปลายทาง
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" id="submit_step3"
                                class="btn btn-danger  btn-square  waves-effect waves-light"
                                style="width: 100%;display:none">
                                <span >ถัดไป <i class="mdi mdi-arrow-right"></i></span></button>
                        </div>
                    </form>
                </div>
                <!-- END STEP 3 -->

                <!-- STEP 4 -->
                <div class="row" id="step_4" style="padding: 10px;">
                    <div class="card">
                        <form name="form_step_4" id="form_step_4" action="{{url('/app/eshipment')}}" novalidate>
                            <div class="col-md-12 toppic">
                                <h3 style="text-align:center"><u>รายการสินค้า</u></h3>
                            </div>
                            <br />
                            <div id="desc_area">
                                <div class="desc">
                                    <div class="row">
                                        <div class="col-12 col-md-6" style="padding-left: 27px;">
                                            <div class="col-12 col-md-12">คำอธิบายสินค้า*</div>
                                            <div class="col-12 col-md-12">
                                                <div style="color: red;font-size: 13px;padding-top: 12px;">
                                                    โปรดอธิบายโดยละเอียด สินค้าคืออะไร ใช้เพื่ออะไร ทำจากอะไร เช่น Men black polo shirt Cotton 100%. หรือ Document for Visa.
                                                </div>
                                                <textarea class="d_description form-control sh-input textarea" rows="3"
                                                    style="height: 82px;" placeholder="" required
                                                    onchange="cal_desc()"></textarea>
                                                    <div class="invalid-feedback">
                                                        กรุณากรอกคำอธิบายสินค้า
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="col-12 col-md-6 space_quantity" style="padding-left: 27px;">
                                                <div class="col-12 col-md-12">จำนวน (ชิ้น)*</div>
                                                <div class="col-12 col-md-12">
                                                    <input type="text"
                                                    class="d_number form-control sh-input" placeholder="" required
                                                    onchange="cal_desc()" onkeypress="return isNumberKey(event)">
                                                    <div class="invalid-feedback">
                                                        กรุณากรอกจำนวน(ชิ้น)*
                                                    </div>
                                                </div>
                                                <br/>
                                                <div class="col-12 col-md-12">มูลค่ารวม (บาท)*</div>
                                                <div class="col-12 col-md-12">
                                                    <input type="text"
                                                    class="d_amount form-control sh-input" placeholder="" required
                                                    onchange="cal_desc()" onkeypress="return isNumberKey(event)">
                                                    <div class="invalid-feedback">
                                                        กรุณากรอกมูลค่ารวม(บาท)*
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="text-align:center">
                                <button type="button" class="btn btn-danger waves-effect waves-light"
                                    style="margin-top:10px;margin-bottom: 20px;" onclick="adddesc()">
                                    <i class="mdi mdi-plus-circle-outline"></i> เพิ่มสินค้า</button>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" id="submit_step4"
                                    class="btn btn-danger  btn-square  waves-effect waves-light"
                                    style="width: 100%;display:none">
                                    <span >ถัดไป <i class="mdi mdi-arrow-right"></i></span></button>
                            </div>
                            <div class="row" style="padding:unset;display:none;">
                                <input type="text" class="" id="sh_description" placeholder="ภาษาอังกฤษเท่านั้น"
                                    required>
                                <input type="text" id="sh_quantity" onkeypress="return isNumberKey(event)"
                                    placeholder="0 ">
                                <input type="text" id="sh_declared" onkeypress="return isNumberKey(event)"
                                    placeholder="0 ">
                            </div>
                            <div id="bindingdata"></div>

                            <!-- Copy -->
                        @if(isset($shipment))
                            @foreach ($shipment as $index=>$item)
                            <input type="hidden" class="sh_type_of_service" name="sh_type_of_service[{{$index}}]"
                                value="{{$item->services}}">
                                <input type="hidden" class="sh_service" name="sh_service[{{$index}}]"
                                value="{{$item->service_name}}">
                                
                            <input type="hidden" class="sh_packge_type" name="sh_packge_type[{{$index}}]" value="PA">
                            <input type="hidden" class="sh_recipient" name="sh_recipient[{{$index}}]" value="{{$item->recipient}}">
                            <input type="hidden" class="sh_company" name="sh_company[{{$index}}]" value="{{$item->company}}">
                            <input type="hidden" class="sh_email" name="sh_email[{{$index}}]" value="{{$item->email}}">
                            <input type="hidden" class="sh_address1" name="sh_address1[{{$index}}]" value="{{$item->address}}">
                            <input type="hidden" class="sh_address2" name="sh_address2[{{$index}}]" value="{{$item->address2}}">
                            <input type="hidden" class="sh_address3" name="sh_address3[{{$index}}]" value="{{$item->address3}}">
                            <input type="hidden" class="sh_phone" name="sh_phone[{{$index}}]" value="{{$item->phone}}">
                            <input type="hidden" class="sh_country" name="sh_country[{{$index}}]" value="{{$item->country}}">
                            <input type="hidden" class="sh_currency" name="sh_currency[{{$index}}]" value="THB">
                            <input type="hidden" class="sh_country_code" name="sh_country_code[{{$index}}]" value="{{$item->country_code}}">
                            <input type="hidden" class="sh_city" name="sh_city[{{$index}}]" value="{{$item->city}}">
                            <input type="hidden" class="sh_suburb" name="sh_suburb[{{$index}}]" value="{{$item->suburb}}">
                            <input type="hidden" class="sh_state" name="sh_state[{{$index}}]" value="{{$item->state}}">
                            <input type="hidden" class="sh_postcode" name="sh_postcode[{{$index}}]" value="{{$item->zipcode}}">
                            <input type="hidden" class="sh_Note" name="sh_Note[{{$index}}]" value="{{$item->note}}">
                            <input type="hidden" class="total_regular_item" name="total_regular_item[{{$index}}]"  value="{{$item->price}}">
                            <input type="hidden" class="total_weight_item" name="total_weight_item[{{$index}}]" value="{{$item->weight}}">
                            <input type="hidden" class="sh_ExportDoc" name="sh_ExportDoc[{{$index}}]" value="no">
                            @foreach ($item->box as $i=>$box_item)
                            <input type="hidden" name="series[{{$index}}][{{$i}}]" value="{{$box_item->series}}">
                            <input type="hidden" name="weight[{{$index}}][{{$i}}]" value="{{$box_item->weight}}">
                            <input type="hidden" name="width[{{$index}}][{{$i}}]" value="{{$box_item->width}}">
                            <input type="hidden" name="length[{{$index}}][{{$i}}]" value="{{$box_item->length}}">
                            <input type="hidden" name="height[{{$index}}][{{$i}}]" value="{{$box_item->height}}">
                            @endforeach
                            @foreach ($item->desc as $i=>$desc_item)
                            <input type="hidden" name="d_description[{{$index}}][{{$i}}]"
                                value="{{$desc_item->description}}">
                            <input type="hidden" name="d_amount[{{$index}}][{{$i}}]" value="{{$desc_item->value}}">
                            <input type="hidden" name="d_number[{{$index}}][{{$i}}]" value="{{$desc_item->quantity}}">
                            @endforeach

                            <input type="hidden" class="sh_Pack" name="sh_Pack[{{$index}}]" value="{{$item->pack}}">
                            <input type="hidden" class="sh_Insurance" name="sh_Insurance[{{$index}}]" value="{{$item->insurance}}">

                            <input type="hidden" class="sh_declared_value" name="sh_declared_value[{{$index}}]"
                                value="{{$item->declared_value}}">
                            <input type="hidden" class="sh_description" name="sh_description[{{$index}}]" value="{{$item->description}}">
                            <input type="hidden" class="sh_content_quantity" name="sh_content_quantity[{{$index}}]"
                                value="{{$item->content_quantity}}">
                            @endforeach
                            @endif
                            <!-- end -->
                            
                        </form>
                    </div>
                </div>
                <!-- END STEP 4 -->

                <!-- Temp -->
                <div style="display:none;">
                    <div id="sumary_area">
                        @if(isset($temp))
                        {!!$temp->data!!}
                        @else
                        <div id="temp_sumary" class="row"></div>
                        @endif
                        @if(isset($shipment))
                            @foreach ($address as $index=>$item)
                            <input type="hidden" class="addr_name" name="addr_name[{{$index}}]" value="{{$item->customer_name}}">
                            <input type="hidden" class="addr_phone" name="addr_phone[{{$index}}]" value="{{$item->phone}}">
                            <input type="hidden" class="addr_address" name="addr_address[{{$index}}]" value="{{$item->address}}">
                            <input type="hidden" class="email" name="email[{{$index}}]" value="{{$item->email}}">
                            <input type="hidden" class="province" name="province[{{$index}}]" value="{{$item->province}}">
                            <input type="hidden" class="postcode" name="postcode[{{$index}}]" value="{{$item->postcode}}">
                            @endforeach
                        @endif
                    </div>
                </div>
                <!-- end -->

                <!-- STEP 5 -->
                <div id="step_5">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <form name="form_step_5" id="form_step_5">
                                <div class="card">

                                    <div class="col-12 col-md-12 toppic" style="text-align: center;">
                                        <h2>ตรวจสอบข้อมูลการจัดส่ง</h2>
                                    </div>

                                    <hr style="border-top: 1px dashed;" />
                                    <!-- Send Summarize -->
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb_font">
                                            <div class="font_weight space_topic">ข้อมูลผู้ส่ง (สถานที่รับงาน)</div>
                                        </div>
                                            <div class="col-4 col-md-4 mb_font">
                                                <div class="mb_font">ชื่อ</div>
                                            </div>
                                            <div class="col-8 col-md-8 dis_space delete_paddingleft">
                                                <div id="sr_name"></div>
                                            </div>
                                            <div class="col-4 col-md-4 mb_font space">
                                                <div class="mb_font">ที่อยู่</div>
                                            </div>
                                            <div class="col-8 col-md-8 delete_paddingleft space">
                                                <div id="sr_addr_adderss"></div>
                                            </div>
                                            <div class="col-4 col-md-4 mb_font space">
                                                <div class="mb_font">เบอร์โทร</div>
                                            </div>
                                            <div class="col-8 col-md-8 delete_paddingleft space">
                                                <div id="sr_phone"></div>
                                            </div>  
                                    </div>
                                    <!-- End Send Summarize -->

                                    <hr class="space_line" />
                                    <!-- Receive Summarize -->
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb_font">
                                            <div class="font_weight space_topic">ข้อมูลผู้รับ</div>
                                        </div>
                                            <div class="col-4 col-md-4">
                                                <div class="mb_font">ชื่อ</div>
                                            </div>
                                            <div class="col-8 col-md-8 delete_paddingleft">
                                                <div id="sr_recipient"></div>
                                            </div>
                                            <div class="col-4 col-md-4 mb_font space">
                                                <div class="mb_font">ที่อยู่</div>
                                            </div>
                                            <div class="col-8 col-md-8 delete_paddingleft space">
                                                <div id="sr_sh_address"></div>
                                            </div>
                                            <div class="col-4 col-md-4 mb_font space">
                                                <div class="mb_font">เบอร์โทร</div>
                                            </div>
                                            <div class="col-8 col-md-8 delete_paddingleft space">
                                                <div id="sr_sent_phone"></div>
                                            </div>
                                            <div class="col-4 col-md-4 mb_font space">
                                                <div class="mb_font">ประเทศ</div>
                                            </div>
                                            <div class="col-8 col-md-8 delete_paddingleft space">
                                                <div id="sr_country"></div>
                                            </div>
                                            <div class="col-4 col-md-4 mb_font space">
                                                <div class="mb_font">รหัสไปรษณีย์</div>
                                            </div>
                                            <div class="col-8 col-md-8 delete_paddingleft space">
                                                <div id="sr_postcode"></div>
                                            </div>
                                            <div class="col-4 col-md-4 mb_font space">
                                                <div class="mb_font">อีเมล</div>
                                            </div>
                                            <div class="col-8 col-md-8 delete_paddingleft space">
                                                <div id="sr_email"></div>
                                            </div>
                                    </div>
                                    <!-- End Receive Summarize -->

                                    <hr class="space_line" />
                                    <!-- Product Detail -->
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb_font">
                                            <div class="font_weight" style="padding-left: 11px;">รายละเอียดสินค้า</div>
                                        </div>
                                        <div class="col-12 col-md-12 space">
                                            <div id="sr_product_detail" class="space_topic"></div>
                                        </div>
                                            
                                            <!-- <div class="col-5 col-md-4 mb_font space">
                                                <div class="mb_font">จำนวน<span class="mb_font_small">(ชิ้น)*</span></div>
                                            </div> -->
                                            <div class="col-12 col-md-12 space">
                                                <div class="space_topic"><span id="sr_product_quality"></span> ชิ้น</div>
                                            </div>
                                            <!-- <div class="col-5 col-md-4 mb_font space">
                                                <div class="mb_font">มูลค่า</div>
                                            </div> -->
                                            <div class="col-12 col-md-12 space">
                                                <div class="space_topic">มูลค่าสินค้า <span id="sr_product_baht"></span> บาท</div>
                                            </div>


                                        <!-- <div class="col-6 col-md-4 mb_font">
                                            <div class="col-12 col-md-12">
                                                <div>จำนวน<span class="mb_font_small">(ชิ้น)*</span></div><br>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <div>มูลค่า</div>
                                            </div>
                                        </div>
                                        <div class="col-6 col-md-8 mb_font">
                                            <div class="col-12 col-md-12">
                                                <div><span class="mb_space" id="sr_product_quality"></span>
                                                    ชิ้น
                                                </div><br>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <div><span class="mb_space" id="sr_product_baht"></span>
                                                    บาท
                                                </div>
                                            </div>
                                        </div> -->

                                    </div><br />
                                    <!-- End Product Detail -->
                                </div>

                                <!-- Service -->
                                <div class="card">

                                    <div class="col-12">
                                        <!-- <div class="space_topic"><span id="t_country"></span>(
                                            <span id="sr_weight"></span>)KG
                                        </div> -->
                                        <h2 class="space_topic_report">รายละเอียดบริการ</h2>
                                    </div>

                                    <hr class="space_line" />

                                    <div class="row">
                                        <!-- <div class="col-8">
                                            <div class="col-12 mb_font"><span id="sr_service">xxxxxxxxxx</span></div>
                                        </div> -->
                                        <div class="col-12">
                                            <div class="space_topic"><span id="sr_service"></span></div>
                                            <div class="col-12">ค่าจัดส่ง <span id="price_service"></span> บาท</div><br />
                                        </div>
                                    </div>
                                </div>
                                <!-- End Service -->

                                <!-- Send Select -->
                                <div class="card">

                                    <div class="col-12">
                                        <h2 class="space_topic_report">เลือกวิธีจัดส่ง</h2>
                                    </div>

                                    <hr style="border-top: 1px dashed; margin: 3px 0 20px 0;" />

                                    <div class="row">
                                        <div class="col-2 col-md-2 pointer delete_paddingright"
                                            style="text-align: right;">
                                            <input id="select_type1" style="display : none;" type="radio"
                                                name="type_delivery" value="เรียกรถเข้ารับพัสดุ" checked>
                                            <label for="select_type1" id="img_type1"><img class="pointer"
                                                    src="{{ asset('/assets/images/red1.png') }}"></label>
                                        </div>
                                        <div class="col-10 col-md-10">
                                            <span>เรียก SME SHIPPING รับพัสดุ</span><br>
                                            <span class="font_small">ฟรีสำหรับบริการ Express Plus</span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="space_datetime">วันและเวลาที่เข้ารับ :</div>
                                        </div>
                                        <div class="row space_datetime">
                                            <div class="col-6 col-md-6">

                                                <input type="text" class="form-control pointers"
                                                    placeholder="โปรดระบุวันที่" name="delivery_date" id="mdate"
                                                    required="" data-dtp="dtp_Qyted" value="{{date('d/m/Y')}}" />

                                            </div>
                                            <div class="col-5 col-md-6">
                                                <select class="form-control" name="delivery_time" id="mtime">
                                                    <option value="10.00">10.00</option>
                                                    <option value="11.00">11.00</option>
                                                    <option value="12.00">12.00</option>
                                                    <option value="13.00">13.00</option>
                                                    <option value="14.00">14.00</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-2 col-md-2 delete_paddingright" style="text-align: right;">
                                            <input id="select_type2" style="display:none;" type="radio"
                                                name="type_delivery" value="นำส่งที่ศูนย์บริการ">
                                            <label for="select_type2" id="img_type2"><img class="pointer"
                                                    src="{{ asset('/assets/images/red2.png') }}"></label>
                                        </div>
                                        <div class="col-10 col-md-10">
                                            <span class="">นำส่งที่ศูนย์บริการ</span><br>
                                            <span class="pointer font_small" data-toggle="modal" data-animation="bounce"
                                                data-target=".view_map">
                                                <b>แผนที่จุดบริการ</b> <i class="mdi mdi-map-marker"></i></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-2 col-md-2 delete_paddingright" style="text-align: right;">
                                            <input id="select_type3" style="display:none;" type="radio"
                                                name="type_delivery" value="ส่งพัสดุมาที่ SME SHIPPING">
                                            <label for="select_type3" id="img_type3"><img class="pointer"
                                                    src="{{ asset('/assets/images/red2.png') }}"></label>
                                        </div>
                                        <div class="col-10 col-md-10" style="padding-top: 9px;">
                                            <span class="">ส่งพัสดุมาที่ SME SHIPPING</span><br>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-2 col-md-2 delete_paddingright" style="text-align: right;">
                                            <input id="select_type4" style="display:none;" type="radio"
                                                name="type_delivery" value="Drop off ไปรษณีย์ไทย">
                                            <label for="select_type4" id="img_type4"><img class="pointer"
                                                    src="{{ asset('/assets/images/red2.png') }}"></label>
                                        </div>
                                        <div class="col-10 col-md-10" style="padding-top: 9px;">
                                            <span class="">Drop off ไปรษณีย์ไทย</span><br>
                                        </div>
                                    </div>


                                </div>
                                <!-- End Send Select -->

                                <!-- Discount Code -->
                                <div class="card" style="display:none;">
                                    <div class="col-12">
                                        <h2 class="space_topic_report">Code ส่วนลด</h2>
                                    </div>
                                    <div class="row">
                                        <div class="col-8 col-md-8">
                                            <div class="col-12">
                                                <input id="discount_code" class="form-control" type="text"
                                                    name="discount_code">
                                                <input id="check_code" style="display:none;" class="form-control"
                                                    type="text">
                                            </div>
                                        </div>
                                        <div class="col-4 col-md-4 delete_paddingleft">
                                            <div class="col-12 delete_paddingleft">
                                                <button type="button" id="check_discount" class="btn btn-danger"
                                                    style="width: 100%;">
                                                    <span class="mb_font_small"
                                                        style="color:width">ตรวจสอบ</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end -->

                                <!-- Total price -->
                                <div class="card" style="display:none;">
                                    <div class="col-12">
                                        <h2 class="space_topic_report">สรุปยอดค่าบริการ</h2>
                                    </div>
                                    <hr style="border-top: 1px dashed;margin-top: 4px;margin-bottom: 1rem;" />

                                    <div class="row">
                                        <div class="col-6 col-md-6">
                                            <div class="col-12">
                                                <div>ค่าขนส่ง</div><br>
                                            </div>
                                            <div class="col-12">
                                                <div>ส่วนลด</div><br>
                                            </div>
                                            <div class="col-12">
                                                <div>ค่ารับพัสดุ</div><br>
                                            </div>
                                            <div class="col-12">
                                                <div>promotion</div><br>
                                            </div>
                                        </div>
                                        <div class="col-6 col-md-6">
                                            <div class="col-12 space_right">
                                                <div><span id="total_service"></span> บาท </div><br>
                                            </div>
                                            <div class="col-12 space_right">
                                                <div><span id="discount_price">0</span> บาท </div><br>
                                                <input name="discount_price" style="display:none;"
                                                    class="discount_price">
                                            </div>
                                            <div class="col-12 space_right">
                                                <div><span id="price_receive"></span> บาท </div><br>
                                            </div>
                                            <div class="col-12 space_right">
                                                <div><span id="sh_promotion"></span> บาท </div><br>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="space_line" />
                                    <div class="row">
                                        <div class="col-6 col-md-6">
                                            <div class="col-12">
                                                <div>ค่าบริการรวม</div><br>
                                            </div>
                                        </div>
                                        <div class="col-6 col-md-6 space_right">
                                            <div class="col-12 space_right">
                                                <div><span id="totle_pirce"></span> บาท </div><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!-- End Total Price -->
                        </div>
                        <div class="col-md-12">
                            <button type="submit" id="submit_step5"
                                class="btn btn-danger  btn-square  waves-effect waves-light"
                                style="width: 100%;display:none">
                                <span >ถัดไป <i class="mdi mdi-arrow-right"></i></span></button>
                        </div>
                        </form>
                    </div>
                </div><!-- END STEP 5 -->
            </div>
        </div>
    </div>
</div>
<div class="col-3 col-md-1"></div>
</div>
<!-- btn-outline-dashed -->

<!-- Botton Next -->
<div class="row">
    <div class="col-1 col-md-4"></div>
    <div class="col-10 col-md-4">
        <!-- <button type="button" onclick="$('#submit_step1').trigger('click')" id="step1"
            class="btn btn-danger btn-square  waves-effect waves-light eship_space"
            style="width: 100%;">
            <span >ถัดไป</span></button> -->
        <button type="button" onclick="$('#submit_step2').trigger('click')" id="step2"
            class="btn btn-danger  btn-square  waves-effect waves-light eship_space"
            style="width: 100%;display:none"><span >ถัดไป</span></button>
        <button type="button" onclick="$('#submit_step3').trigger('click')" id="step3"
            class="btn btn-danger  btn-square  waves-effect waves-light eship_space"
            style="width: 100%;display:none"><span >ถัดไป</span></button>
        <button type="button" onclick="$('#submit_step4').trigger('click')" id="step4"
            class="btn btn-danger  btn-square  waves-effect waves-light eship_space"
            style="width: 100%;display:none"><span >ถัดไป</span></button>
            <div style="display: flex;justify-content: center;">
        <button type="button" onclick="$('#submit_step5').trigger('click')" id="step5"
            class="btn btn-danger  btn-square  waves-effect waves-light eship_space"
            style="width: 66%;display:none">
            <span>ยืนยันข้อมูลพัสดุ</span></button>
            </div>
        <div id="printer" style="display: flex;justify-content: center;"></div>
        <div id="tracking"></div>
    </div>
    <div class="col-1 col-md-3"></div>
</div>
<!-- END NEXT -->
</div>


<!--  Modal content for the above example -->
<div class="modal fade recipient_list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">เลือกผู้รับ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="background-color: white;">
                <div class="form-group">
                    <label for="example-input2-group1"></label>
                    <div class="input-group">
                        <input type="text" id="find_recipient" name="find_recipient" class="form-control"
                            placeholder="ค้นหาจากชื่อ หรือ เบอร์โทร">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="mdi mdi-account-search-outline"></i></span>
                        </div>
                    </div>
                </div>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                            <th>ชื่อ</th>
                            <th>ปลายทาง</th>
                            <th>เบอร์ติดต่อ</th>
                        </tr>
                    </thead>
                    <tbody id="recipient_area">
                    </tbody>
                </table>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<!--  Modal content for the above example -->
<div class="modal fade sender_list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">เลือกผู้ส่ง</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="background-color: white;">
                <div class="form-group">
                    <label for="example-input2-group1"></label>
                    <div class="input-group">
                        <input type="text" id="find_sender" name="find_sender" class="form-control"
                            placeholder="ค้นหาจากชื่อ หรือ เบอร์โทร">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="mdi mdi-account-search-outline"></i></span>
                        </div>
                    </div>
                </div>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                            <th>ชื่อ</th>
                            <th>ปลายทาง</th>
                            <th>เบอร์ติดต่อ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($address as $index=>$item)
                        <tr style="cursor: pointer;"
                            onclick="set_sender('{{$item->id}}','{{$item->customer_name}}','{{$item->address}}','{{$item->tumbon}}','{{$item->amphur}}','{{$item->province}}','{{$item->postcode}}','{{$item->phone}}','{{$item->email}}')">
                            <td>{{$item->customer_name}}</td>
                            <td>
                                {{$item->address." ".$item->tumbon." ".$item->amphur." ".$item->province." ".$item->postcode}}
                            </td>
                            <td>{{$item->phone}}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<!--  Modal content for the above example -->
<div class="modal fade high_light" id="finished" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">สร้างรายการส่งสำเร็จ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modal_tracking"></div>
                <!-- <div style="text-align: center; color: red;font-size: 92%;">สามารถพิมพ์เอกสารได้ที่ด้านล่างของ Page ค่ะ</div> -->
            </div>
            <div class="modal-footer" style="justify-content: space-around;">
                <button type="button" class="btn" data-dismiss="modal" style="width: 72px; color: #f22727;background-color: #ffffff;font-weight: bold;border: 1px solid #f22727;"> ปิด</button>
                <div id="print_label"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--  Modal content for the above example -->
<div class="modal fade high_light" id="select_one_service" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="modal-title" style="text-align: center;" id="exampleModalLongTitle">คุณต้องการเลือกใช้บริการ <span id="name_service"></span> ใช่หรือไม่</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div style="text-align: center; padding-top: 17px;">
                <button type="button" class="btn cancel_service next_step" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn next_step" 
                onclick="$('#submit_step1').trigger('click')" data-dismiss="modal">ยืนยัน</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- ดึง body ที่ใช้ร่วมกันมาแสดง -->
@yield('content', View::make('customer/app/privacyterms'))
<!--  -->
<script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' )}}">
</script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}">
</script>
<script src="{{ asset('assets/pages/jquery.forms-advanced.js?1' )}}"></script>
<script src="{{ asset('assets/js/jquery.mask.min.js?1.1' )}}"></script>
<script src="{{ asset('assets/js/app_eship.js?14' )}}"></script>

@if($address->where('pickup',1)->first())
<?php $address_item = $address->where('pickup',1)->first();?>
<script>
$('#addr_name').val('{{$address_item->customer_name}}');
$('#addr_phone').val('{{$address_item->phone}}');
$('#addr_address').val('{{$address_item->address}}');
$('#email').val('{{$address_item->email}}');
$('#province').val('{{$address_item->province}}');
$('#amphur').val('{{$address_item->amphur}}');
$('#tumbon').val('{{$address_item->tumbon}}');
$('#postcode').val('{{$address_item->postcode}}');
$('#address_id').val('{{$address_item->id}}');
</script>

@endif
@if(isset($shipment))
<script>
$('#addshipment_btn').show();
nextstep(5);
</script>
<script src="{{ asset('assets/js/copy.js?1' )}}"></script>
@endif
@if(isset($temp)&&$address->where('pickup',1)->count()>0)
<script>
$('#addshipment_btn').show();
nextstep(5);
</script>
<script src="{{ asset('assets/js/binding_temp.js?2' )}}"></script>
@endif

@if(app('request')->input('sh_country')!=null)
<script>
$('#sh_country').val("{{app('request')->input('sh_country')}}").change();
</script>
@endif
@if(app('request')->input('volumnWeight')!=null)
<script>
$('#sh_weight').val("{{app('request')->input('volumnWeight')}}").change();
</script>
@endif

@endsection