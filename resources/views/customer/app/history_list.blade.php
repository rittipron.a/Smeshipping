@extends('layouts.customer_app')

@section('content')
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<style>
    .client-card {
        padding-top: 10px;
        padding-bottom: 10px;
        margin-top: 10px !important;
        margin-bottom: unset;
    }

    .tracking-no {
        color: red;
        font-size: 20px;
        font-weight: 800;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <hr style="border: 1px solid rgb(242, 39, 39);
        background-color: white;
        position: inherit;
        top: 100px;
        z-index: 2;">
        <div class="card">
            <div class="card-body">

                <h3>ประวัติการส่ง</h3>
                <div class="row" id="sumary_area" style="padding-top:40px;">
                    <div class="col-2 mobile_hide" style="text-align:center">Tracking No.</div>
                    <div class="col-1 mobile_hide" style="text-align:center">วันที่</div>
                    <div class="col-2 mobile_hide" style="text-align:center">ผู้รับ</div>
                    <div class="col-2 mobile_hide" style="text-align:center">ปลายทาง</div>
                    <div class="col-1 mobile_hide" style="text-align:center">น้ำหนัก</div>
                    <div class="col-2 mobile_hide" style="text-align:center">บริการ</div>
                    <div class="col-2 mobile_hide" style="text-align:center">ค่าบริการ</div>
                    <div class="col-md-12 sumary_data">
                        @foreach ($data as $index=>$item)
                        <div class="card client-card mobile_hide">
                            <div class="row" style="padding-bottom:unset">
                                <div class="col-2" style="text-align:center"><b
                                        class="cut-text">{{strlen($item->tracking_code)==6?substr($item->tracking_code,0,2).' '.substr($item->tracking_code,2,2).' '.substr($item->tracking_code,4,2):$item->tracking_code}}</b>
                                </div>
                                <div class="col-1" style="text-align:center;padding: unset;"><span
                                        class="cut-text">{{ date('d/m/y' , strtotime($item->created_at))}}</span>
                                </div>
                                <div class="col-2" style="text-align:center"><span
                                        class="cut-text">{{$item->recipient}}</span>
                                </div>
                                <div class="col-2" style="text-align:center"><span class="cut-text"
                                        title="{{$item->country}}">{{$item->country}}</span>
                                </div>
                                <div class="col-1" style="text-align:center">{{$item->weight}} KG</div>
                                <div class="col-2" style="text-align:center"><span class="cut-text"
                                        title="{{$item->service_name}}">{{$item->service_name}}</span></div>
                                <div class="col-2" style="text-align:center">{{number_format($item->price)}} บาท
                                    <a href="{{url('/smelabel/'.$item->inv_id)}}" target="_blank"
                                        title="คลิกเพื่อปริ้นท์ SME Label ปิดหน้ากล่องพัสดุ"><br /><span
                                            style="color:red"><i class="mdi mdi-printer"></i>Label</span></a>
                                    @if($item->awb!=''&&$item->awb_file!=''?$item->awb_file:'')
                                    | <a href="{{url('/uploads/labels/'.$item->awb_file)}}" target="_blank"
                                        title="คลิกเพื่อปริ้นท์ AWB Label ปิดหน้ากล่องพัสดุ"><span style="color:red"><i
                                                class="mdi mdi-file-pdf"></i>AWB</span></a>
                                    @endif

                                    {{-- <a href="{{url('/app/copy/'.$item->id)}}"><span
                                        style="color:red">ส่งอีกครั้ง</span></a> --}}
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="row mobile_show">
                            @foreach ($data as $index=>$item)
                            <div class="card client-card" style="padding: 15px;">
                                <div class="row" style="padding-bottom:unset">
                                    <div class="col-5">Tracking No.</div>
                                    <div class="col-7"><span
                                            style="color:red;font-size:18px;font-weight: bold;">{{strlen($item->tracking_code)==6?substr($item->tracking_code,0,2).' '.substr($item->tracking_code,2,2).' '.substr($item->tracking_code,4,2):$item->tracking_code}}</span>
                                    </div>
                                    <div class="col-5">วันที่</div>
                                    <div class="col-7">{{ date('d/m/y' , strtotime($item->created_at))}}</div>
                                    <div class="col-5">ชื่อผู้รับ</div>
                                    <div class="col-7"><b>{{$item->recipient}}</b></div>
                                    <div class="col-5">ปลายทาง</div>
                                    <div class="col-7"><span class="cut-text">{{$item->country}}</span></div>
                                    <div class="col-5">น้ำหนัก</div>
                                    <div class="col-7">{{$item->weight}} KG.</div>
                                    <div class="col-5">บริการ</div>
                                    <div class="col-7"><span class="cut-text">{{$item->service_name}}</span></div>
                                    <div class="col-5">ค่าบริการ</div>
                                    <div class="col-7"><span class="cut-text">{{number_format($item->price >0?$item->price:0)}} บาท</span>
                                    </div>
                                    <div class="col-12">
                                        <!-- <a href="{{url('/app/copy/'.$item->id)}}">
                                            <h4 style="color:red;text-align:center;margin-bottom:unset">ส่งอีกครั้ง</h4>
                                        </a> -->
                                        <a href="{{url('/smelabel/'.$item->inv_id)}}" target="_blank"><span
                                                style="color:red"><i
                                                    class="mdi mdi-format-page-break"></i>Label</span></a> |
                                        <a href="{{url('/app/copy/'.$item->id)}}" target="_blank"><span
                                                style="color:red"><i class="mdi mdi-printer"></i>AWB</span></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                </div>

            </div>
            <!--end /tableresponsive-->
        </div>
        <!--end card-body-->
    </div>
</div>
<div class=" col-12">
    {{ $data->links() }}
</div>
</div>
@endsection