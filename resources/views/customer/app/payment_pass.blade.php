@extends(session()->has('cus.id')?'layouts.customer_app':'layouts.customer_app_hidden_nav')

@section('content')
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">

<style>
    .payment_bank {
        width: 27cm;
        border-radius: 10px;
        margin: 0 0 0 0;
        font-size: smaller;
        padding: 7px;
        -webkit-box-shadow: 0px 3px 8px -1px rgba(153, 151, 153, 1);
        -moz-box-shadow: 0px 3px 8px -1px rgba(153, 151, 153, 1);
        box-shadow: 0px 3px 8px -1px rgba(153, 151, 153, 1);
        "          

    }

    .fontweight {
        font-weight: bold;
    }

    .detail_pay {
        margin-top: 10px;
    }

    .highlight {
        color: red;
        font-weight: bold;
    }

    .space {
        margin: 0 28.766px;
    }

    .underline {
        text-align: center;
        text-decoration: underline;
    }

    .btn {
        border: 1px solid #656269;
        border-radius: 7px;
        padding: 7px 20px 7px 20px;
        margin-top: 0px;
        color: #3677B6;
    }

    .space_btn {
        margin-top: 35px;
        margin-bottom: 30px;
        text-align: center;
    }

    .pointer {
        cursor: pointer;
    }

    /* mobile */
    .mb_payment_bank {
        background-color: #ffffff;
        border-radius: 10px;
        margin: 0 0 0 0;
        font-size: smaller;
        -webkit-box-shadow: 0px 3px 8px -1px rgba(153, 151, 153, 1);
        -moz-box-shadow: 0px 3px 8px -1px rgba(153, 151, 153, 1);
        box-shadow: 0px 3px 8px -1px rgba(153, 151, 153, 1);
        "   

    }

    .mb_detail_pay {
        margin-top: 10px;
    }

    .mb_row {
        padding-top: 10px
    }

    .mb_font {
        font-size: 12px;
    }

    .mb_highlight {
        color: red;
        font-weight: bold;
        padding-top: 10px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <hr style="border: 1px solid rgb(242, 39, 39);
        background-color: white;
        position: inherit;
        top: 80px;
        z-index: 2;">
        <div class="card">
            <div class="card-body">
                <h3 style="text-align:left">ชำระเงิน</h3>
                <div class="row" style="padding-top: 10px;">
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-6 detail_pay"><b>จำนวนรายการ</b></div>
                            <div class="col-6 detail_pay">{{$booking}} Booking</div>
                            <div class="col-6 detail_pay"><b>จำนวนพัสดุ</b></div>
                            <div class="col-6 detail_pay">{{$shipment}} Shipment</div>
                            <div class="col-6 detail_pay"><b>อัตราค่าบริการ</b></div>
                            <div class="col-6 detail_pay">{{number_format($amount_total>0?$amount_total:0)}} บาท</div>
                            <div class="col-6 detail_pay"><b>ชำระค่าบริการแล้ว</b></div>
                            <div class="col-6 detail_pay">{{number_format($amount_pay)}} บาท</div>
                            <div class="col-6 detail_pay"><b>ยอดค้างชำระจำนวน </b></div>
                            <div class="col-6 detail_pay highlight mobile_hide">{{number_format($amount>0?$amount:0)}}
                                บาท</div>
                            <div class="col-6 detail_pay mb_highlight mobile_show">
                                {{number_format($amount>0?$amount:0)}} บาท</div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-4 ">

                        @if($payment_file_re==null)
                        <div id="is_upload">
                            <label for="payment_file"> <img class="pointer"
                                    src="{{ asset('/assets/images/upload.png') }}" width="100%" /></label>
                            <input id="payment_file" type="file" accept="image/*" class="form-control" multiple
                                onchange="upload_payment('{{app('request')->input('list')}}')" style="display:none" />

                        </div>
                        @endif
                        <div id="is_success"
                            style="text-align:center;display:{{$payment_file_re!=null?'block':'none'}}">
                            <img src="https://icon-library.net/images/success-icon/success-icon-5.jpg"
                                width="100px"><br />
                            แนบหลักฐานการชำระเงินเรียบร้อยแล้ว<br />อยู่ระหว่างรอตรวจสอบข้อมูลค่ะ<br />
                        </div>

                    </div>
                </div>
                <div class="mobile_hide">
                    <h4 class="underline">ช่องทางการชำระเงิน</h4>
                    <div class="row space">
                        <div class="row payment_bank detail_pay">
                            <div class="col-2 fontweight">
                                <span>พร้อมเพย์</span>
                            </div>
                            <div class="col-3 ">
                                <span>เลขอ้างอิง 0105552114948</span>
                            </div>
                            <div class="col-4">
                                <span>ชื่อบัญชี บริษัท เอสเอ็มอี ชิปปิ้ง</span>
                            </div>
                            <div class="col-3 fontweight pointer">
                                <span data-toggle="modal" data-animation="bounce" data-target=".edit_customer_modal">
                                    สแกนคิวอาร์โค้ด
                                </span>
                            </div>
                        </div>

                        <div class="row payment_bank detail_pay">
                            <div class="col-2 fontweight">
                                <span>ธนาคารกสิกรไทย</span>
                            </div>
                            <div class="col-3">
                                <span>เลขบัญชี 749-2-34088-4</span>
                            </div>
                            <div class="col-4">
                                <span>ชื่อบัญชี บริษัท เอสเอ็มอี ชิปปิ้ง จำกัด</span>
                            </div>
                            <div class="col-3">
                                <span>สาขาถนนพาณิชยการธนบุรี</span>
                            </div>
                        </div>

                        <div class="row payment_bank detail_pay">
                            <div class="col-2 fontweight">
                                <span>ธนาคารกรุงเทพ</span>
                            </div>
                            <div class="col-3">
                                <span>เลขบัญชี 141-458438-1</span>
                            </div>
                            <div class="col-4">
                                <span>ชื่อบัญชี บริษัท เอสเอ็มอี ชิปปิ้ง จำกัด</span>
                            </div>
                            <div class="col-3">
                                <span>สาขาท่าพระ</span>
                            </div>
                        </div>
                        <div class="row payment_bank detail_pay">
                            <div class="col-2 fontweight">
                                <span>ธนาคารกรุงศรีอยุธยา</span>
                            </div>
                            <div class="col-3">
                                <span>เลขบัญชี 141-1-31861-2</span>
                            </div>
                            <div class="col-4">
                                <span>ชื่อบัญชี บริษัท เอสเอ็มอี ชิปปิ้ง จำกัด</span>
                            </div>
                            <div class="col-3 ">
                                <span>สาขาท่าพระ</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end card-body-->
            </div>
        </div>

        <!-- MOBLIE -->
        <div class="mobile_show">
            <h4 class="underline">ช่องทางการชำระเงิน</h4>
            <div class="row">
                <div class="row  mb_row mb_payment_bank mb_detail_pay">
                    <div class="col fontweight">
                        <span>พร้อมเพย์</span>
                    </div>
                    <div class="col fontweight">
                        <span data-toggle="modal" data-animation="bounce" data-target=".edit_customer_modal">
                            สแกนคิวอาร์โค้ด
                        </span>
                    </div>
                    <div class="w-100"></div>
                    <div class="col mb_detail_pay">
                        <span>เลขอ้างอิง <br> 0105552114948</span>
                    </div>
                    <div class="col mb_detail_pay mb_font">
                        <span>ชื่อบัญชี <br> บริษัท เอสเอ็มอี ชิปปิ้ง</span>
                    </div>
                </div>

                <div class="row  mb_row mb_payment_bank mb_detail_pay">
                    <div class="col fontweight">
                        <span>ธนาคารกสิกรไทย</span>
                    </div>
                    <div class="col">
                        <span>สาขา <br>ถนนพาณิชยการธนบุรี</span>
                    </div>
                    <div class="w-100"></div>
                    <div class="col mb_detail_pay">
                        <span>เลขบัญชี <br> 749-2-34088-4</span>
                    </div>
                    <div class="col mb_detail_pay mb_font">
                        <span>ชื่อบัญชี <br> บริษัท เอสเอ็มอี ชิปปิ้ง จำกัด</span>
                    </div>

                </div>

                <div class="row mb_row mb_payment_bank mb_detail_pay">
                    <div class="col fontweight">
                        <span>ธนาคารกรุงเทพ</span>
                    </div>
                    <div class="col">
                        <span>สาขาท่าพระ</span>
                    </div>
                    <div class="w-100"></div>
                    <div class="col mb_detail_pay">
                        <span>เลขบัญชี <br> 141-458438-1</span>
                    </div>
                    <div class="col mb_detail_pay mb_font">
                        <span>ชื่อบัญชี <br> บริษัท เอสเอ็มอี ชิปปิ้ง จำกัด</span>
                    </div>
                </div>
                <div class="row mb_row mb_payment_bank mb_detail_pay">
                    <div class="col fontweight">
                        <span>ธนาคารกรุงศรีอยุธยา</span>
                    </div>
                    <div class="col">
                        <span>สาขาท่าพระ</span>
                    </div>
                    <div class="w-100"></div>
                    <div class="col mb_detail_pay">
                        <span>เลขบัญชี <br> 141-1-31861-2</span>
                    </div>
                    <div class="col mb_detail_pay mb_font">
                        <span>ชื่อบัญชี <br> บริษัท เอสเอ็มอี ชิปปิ้ง จำกัด</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- end MOBLIE 

        <div class="space_btn">
            <a href="{{url('/app')}}" class="btn">
                กลับหน้าหลัก</a>
        </div>-->
    </div>
    <!--  Modal content for the above example -->
    <div class="modal fade edit_customer_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="qrcode_pay">สแกนคิวอาร์โค้ด</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <img src="{{ asset('/assets/images/qrcode.png') }}" width="100%" />
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
    <script>
        function upload_payment(id) {
        $("#loading").show();
        var form_data = new FormData();
        var totalfiles = document.getElementById('payment_file').files.length;
        for (var index = 0; index < totalfiles; index++) {
            form_data.append("files[]", document.getElementById('payment_file').files[index]);
        }
        // /console.log(form_data);
        $.ajax({
            type: "POST",
            data: form_data,
            url: '/app/upload_payment?list=' + id,
            processData: false,
            contentType: false,
            success: function(result) {
                $("#loading").hide();
                $('#is_upload').hide();
                $('#is_success').show();
                location.replace('{{url('/app/paycompleted')}}');

                console.log(result);
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $("#loading").hide();
            }
        });
        return true;
    }
    </script>
    @endsection