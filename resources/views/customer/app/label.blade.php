@extends('layouts.customer_app')

@section('content')
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<style>
    .text-muted {
        color: #656269 !important;
    }

    .client-card {
        padding-top: 10px;
        padding-bottom: 10px;
        margin-bottom: 10px !important;
    }

    .tracking-no {
        color: red;
        font-size: 20px;
        font-weight: 800;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <hr style="border: 1px solid rgb(242, 39, 39);
            background-color: white;
            position: inherit;
            top: 80px;
            z-index: 2;">
        <div class="card">
            <div class="card-body">
                <div class="mb-0">
                    <h3 style="margin:unset" class="mobile_hide">กรุณาเขียนหมายเลข Tracking No.
                        ลงบนหน้ากล่องพัสดุทุกกล่อง</h3>
                    <b class="mobile_show">กรุณาเขียนหมายเลข Tracking No.
                        ลงบนกล่อง</b>
                    <br />
                    <div class="mobile_hide">
                        <div class="row">
                            <div class="col-2" style="text-align: center">Tracking No.</div>
                            <div class="col-2" style="text-align: center">ชื่อ</div>
                            <div class="col-2" style="text-align: center">ปลายทาง</div>
                            <div class="col-1" style="text-align: center">น้ำหนัก</div>
                            <div class="col-2" style="text-align: center">จำนวนกล่อง</div>
                            <div class="col-3" style="text-align: center">บริการ</div>
                        </div>

                        @foreach ($shipment as $index=>$item)
                        <div class="card client-card">
                            <div class="row" style="padding-bottom:unset">
                                <div class="col-2" style="text-align: center">
                                    <span
                                        class="tracking-no">{{strlen($item->tracking_code)==6?substr($item->tracking_code,0,2).' '.substr($item->tracking_code,2,2).' '.substr($item->tracking_code,4,2):$item->tracking_code}}</span>
                                </div>
                                <div class="col-2" style="text-align: center"><b
                                        class="cut-text">{{$item->recipient}}</b>
                                </div>
                                <div class="col-2" style="text-align: center"><span
                                        class="cut-text">{{$item->country}}</span></div>
                                <div class="col-1" style="text-align: center">{{$item->weight}} KG</div>
                                <div class="col-2" style="text-align: center">{{$item->box}} กล่อง</div>
                                <div class="col-3" style="text-align: center"><span
                                        class="cut-text">{{$item->service_name}}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row mobile_show">
                        @foreach ($shipment as $index=>$item)
                        <div class="col-md-4" style="padding: unset;margin-top: 10px;">
                            <div class="card client-card" style="padding: 15px;">
                                <div class="row" style="padding-bottom:unset">
                                    <div class="col-5">Tracking No.</div>
                                    <div class="col-7"><span
                                            style="color:red;font-size:18px;font-weight: bold;">{{strlen($item->tracking_code)==6?substr($item->tracking_code,0,2).' '.substr($item->tracking_code,2,2).' '.substr($item->tracking_code,4,2):$item->tracking_code}}</span>
                                    </div>
                                    <div class="col-5">ชื่อผู้รับ</div>
                                    <div class="col-7"><b>{{$item->recipient}}</b></div>
                                    <div class="col-5">ปลายทาง</div>
                                    <div class="col-7"><span class="cut-text">{{$item->country}}</span></div>
                                    <div class="col-5">น้ำหนัก</div>
                                    <div class="col-7">{{$item->weight}} KG.</div>
                                    <div class="col-5">จำนวนกล่อง</div>
                                    <div class="col-7">{{$item->box}} กล่อง</div>
                                    <div class="col-5">บริการ</div>
                                    <div class="col-7"><span class="cut-text">{{$item->service_name}}</span></div>

                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <p class="lead" style="text-align:center;">
                        <br /><br />
                        <span style="color:darkgray;font-size:12px;">กรุณาเขียน หรือ พิมพ์ใบนำส่ง
                            และติดบนหน้ากล่องพัสดุทุกกล่อง</span><br />
                        <a href="{{url('smelabel/'.$item->inv_id)}}" target="_blank">
                            <img src="{{asset('assets/images/print.png')}}" width="190" />
                        </a>
                    </p>
                </div>
            </div>
            <!--end card-body-->
        </div>
    </div>
    <div class="col-md-12" style="text-align:center">
        <a href="{{url('app/')}}">
            <img src="{{asset('assets/images/backtohome.png')}}" width="150" />
        </a>
    </div>
</div>

@endsection