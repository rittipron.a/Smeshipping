@extends('layouts.customer_app')

@section('content')
<link href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css' )}}" rel="stylesheet" />
<link href="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/select2/select2.min.css' )}}" rel="stylesheet">
<link href="{{ asset('assets/css/style.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css' )}}"
    rel="stylesheet" />

<link rel="stylesheet" href="{{ asset('assets/dist/jquery.Thailand.min.css') }}">
<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script type="text/javascript" src="{{ asset('assets/dependencies/JQL.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('assets/dependencies/typeahead.bundle.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/dist/jquery.Thailand.min.js') }}">
</script>
<style>
    body,
    p,
    .label {
        color: rgba(101, 98, 105, 1);
        font-family: 'Prompt', sans-serif;
        font-size: 16px;
    }

    .client-card {
        margin-bottom: 10px !important;
    }

    .btn-next {
        background: white !important;
        border: unset !important;
        color: #3677b6 !important;
        font-weight: bold;
        font-size: 18px;
        filter: drop-shadow(rgba(0, 0, 0, 0.15) 0px 3px 6px);
    }

    .btn {
        color: white;
        font-family: 'Prompt', sans-serif;
        font-size: 16px !important;
    }

    .w-col-3 span {
        color: #3677b6 !important;
        font-weight: bold;
    }

    .custom-control-label::after {
        position: absolute;
        top: .25rem;
        left: -1.5rem;
        display: block;
        width: 2rem;
        height: 2rem;
        content: "";
        background: no-repeat 50%/50% 50%;
    }

    .custom-control-label::before {
        position: absolute;
        top: .25rem;
        left: -1.5rem;
        display: block;
        width: 2rem;
        height: 2rem;
        pointer-events: none;
        content: "";
        background-color: #fff;
        border: #adb5bd solid 1px;
    }

    .delete-item {
        color: #f22727;
        font-size: 25px;
        right: 10px;
        top: 5px;
        position: absolute;
        cursor: pointer;
    }

    .edit-item {
        width: 100%;
        margin-top: 10px;
        border: 2px dashed #3677b6 !important;
        border-radius: 10px 10px 10px 10px !important;
        color: #3677b6 !important;
        filter: unset;
    }

    .sumary_data .card {
        padding: 15px;
    }

    #step2,
    #step3,
    #step4,
    #step5 {
        display: none;
    }

    .modal-content .modal-header,
    .modal-content .modal-footer {
        border-color: #ff0000;
        background-color: #ffffff;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <hr style="border: 1px solid rgb(242, 39, 39);
        background-color: white;
        position: inherit;
        top: 100px;
        z-index: 2;">
        <div class="card">
            <div class="card-body">
                <h3>สร้างพัสดุ</h3>
                <input type="hidden" id="edit_number" value="" />
                <input type="hidden" id="service_number" value="{{app('request')->input('service')}}" />
                <div class="row">
                    <div class="progress-steps">
                        <div class="step active" id="step_1_progress">
                            <div class="step-circle">
                            </div>
                            <span class="label">ที่อยู่ผู้ส่ง</span>
                        </div>
                        <div class="step" id="step_2_progress">
                            <div class="step-circle">
                            </div>
                            <span class="label">ที่อยู่ผู้รับ</span>
                        </div>
                        <div class="step" id="step_3_progress">
                            <div class="step-circle">
                            </div>
                            <span class="label">ข้อมูลพัสดุ</span>
                        </div>
                        <div class="step" id="step_4_progress">
                            <div class="step-circle">
                            </div>
                            <span class="label">ส่งพัสดุ</span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row" id="step_1">
                <form name="form_step_1" id="form_step_1" action="#">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <h3 style="text-align:center"><u>กรอกข้อมูลผู้ส่ง</u></h3>
                                <div class="row">
                                    <div class="col-sm-12">ชื่อ*</div>
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input type="text" id="addr_name" name="name" class="form-control"
                                                maxlength="35" placeholder="ชื่อ-สกุล" autocomplete="new-password"
                                                required="">
                                            <span class="input-group-append">
                                                <button class="btn btn-danger" type="button" data-toggle="modal"
                                                    data-animation="bounce" data-target=".sender_list"
                                                    id="sender_list"><i class="mdi mdi-format-list-bulleted"></i>
                                                    เลือกผู้ส่ง</button>
                                            </span>
                                        </div>
                                        <input type="hidden" name="customer_id" id="addr_customer_id"
                                            class="form-control" value="{{session('cus.id')}}">
                                        <input type="hidden" name="address_id" id="address_id" class="form-control"
                                            value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">เบอร์มือถือ*</div>
                                    <div class="col-sm-12">
                                        <input type="text" name="phone" id="addr_phone" class="form-control"
                                            placeholder="หมายเลขโทรศัพท์" required="" autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">ที่อยู่ผู้ส่ง*</div>
                                    <div class="col-sm-12">
                                        <input type="text" name="address" id="addr_address" class="form-control"
                                            placeholder="ที่อยู่" required="" autocomplete="new-password">
                                    </div>
                                </div>
                                <div id="demo1" class="demo" uk-grid>
                                    <div class="row">
                                        <div class="col-sm-12">ตำบล / แขวง*</div>
                                        <div class="col-sm-12">
                                            <input name="tumbon" id="tumbon" class="form-control" type="text"
                                                autocomplete="new-password" placeholder="ตำบล / แขวง" required="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">อำเภอ / เขต*</div>
                                        <div class="col-sm-12">
                                            <input name="amphur" id="amphur" class="form-control" type="text"
                                                autocomplete="new-password" placeholder="อำเภอ / เขต" required="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">จังหวัด*</div>
                                        <div class="col-sm-12">
                                            <input name="province" id="province" class="form-control" type="text"
                                                autocomplete="new-password" placeholder="จังหวัด" required="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">รหัสไปรษณีย์*</div>
                                        <div class="col-sm-12">
                                            <input name="postcode" id="postcode" class="form-control" type="text"
                                                autocomplete="new-password" placeholder="รหัสไปรษณีย์" required="">
                                        </div>
                                    </div>
                                </div>

                                <script>
                                    $.Thailand({
                                            $district: $('#demo1 [name="tumbon"]'),
                                            $amphoe: $('#demo1 [name="amphur"]'),
                                            $province: $('#demo1 [name="province"]'),
                                            $zipcode: $('#demo1 [name="postcode"]'),
    
                                            onDataFill: function (data) {
                                                console.info('Data Filled', data);
                                            },
    
                                            onLoad: function () {
                                                console.info('Autocomplete is ready!');
                                                //$('#loader, .demo').toggle();
                                            }
                                        });
    
                                </script>
                                <div class="col-md-12" style="margin-top:15px;">
                                    <button type="submit" id="submit_step1"
                                        class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                                        style="width: 100%;display:none">
                                        <span style="color:red">ถัดไป <i
                                                class="mdi mdi-arrow-right"></i></span></button>
                                </div>
                            </div>

                        </div>

                        <div style="display:none">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="radio" name="time" value="พร้อมรับ" checked="">
                                    พร้อมรับ <br>
                                    <input type="radio" name="time" value="ระบุเวลา"> ระบุเวลา
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">คำสั่งถึงพนักงาน :</div>
                                <div class="col-sm-8">
                                    <textarea name="m_note" class="form-control" maxlength="255"></textarea>
                                </div>

                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <br />
            <div class="row" id="step_2">
                <form name="form_step_2" id="form_step_2" action="#">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <h3 style="text-align:center"><u>กรอกข้อมูลผู้รับ</u></h3>
                                <div class="row">
                                    <div class="col-sm-12">ชื่อผู้รับ</div>
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input type="text" id="sh_recipient" name="recipient" class="form-control"
                                                maxlength="35" autocomplete="new-password" required>
                                            <span class="input-group-append">
                                                <button class="btn btn-danger" type="button" data-toggle="modal"
                                                    data-animation="bounce" data-target=".recipient_list"
                                                    id="recipient_list"><i class="mdi mdi-format-list-bulleted"></i>
                                                    เลือกผู้รับ</button>
                                            </span>
                                        </div>
                                        <input type="hidden" name="sh_id" id="sh_id" value="3">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">บริษัท</div>
                                    <div class="col-sm-12">
                                        <input type="text" id="sh_company" name="sh_company"
                                            class="form-control sh-input" placeholder="" maxlength="35"
                                            autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">E-Mail</div>
                                    <div class="col-sm-12">
                                        <input type="email" id="sh_email" name="sh_email" class="form-control sh-input"
                                            placeholder="" autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">ที่อยู่*</div>
                                    <div class="col-sm-12">
                                        <input type="text" id="sh_address1" name="sh_address1"
                                            class="form-control sh-input" placeholder="" required="" maxlength="45"
                                            autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">ที่อยู่2</div>
                                    <div class="col-sm-12">
                                        <input type="text" id="sh_address2" name="sh_address2"
                                            class="form-control sh-input" placeholder="" maxlength="45"
                                            autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">ที่อยู่3</div>
                                    <div class="col-sm-12">
                                        <input type="text" id="sh_address3" name="sh_address3"
                                            class="form-control sh-input" placeholder="" maxlength="45"
                                            autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">เบอร์มือถือผู้รับ*</div>
                                    <div class="col-sm-12">
                                        <input type="text" id="sh_phone" name="sh_phone" class="form-control sh-input"
                                            placeholder="" required="" maxlength="25" autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">ประเทศปลายทาง*</div>
                                    <div class="col-sm-12" style="padding-top:6px">
                                        <select class="select2 form-control mb-3 custom-select" id="sh_country"
                                            name="sh_country" required onchange="get_service()">
                                            @foreach ($country_all as $c_item)
                                            <option value="{{$c_item->country_name}}" code="{{$c_item->country_cd}}"
                                                iata="{{$c_item->country_cd}} {{app('request')->input('sh_country')==$c_item->country_name?"selected":""}}">
                                                {{$c_item->country_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">รหัสไปรษณีย์</div>
                                    <div class="col-sm-12">
                                        <input type="text" id="sh_postcode" name="sh_postcode"
                                            class="form-control sh-input" placeholder="" autocomplete="new-password">
                                        <span id="process_postcode" style="display:none">กำลังตรวจสอบ<div
                                                class="spinner-border spinner-border-sm" role="status"></div></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">เมือง*</div>
                                    <div class="col-sm-12">
                                        <input list="select_city" id="sh_city" name="sh_city"
                                            class="form-control sh-input" placeholder="" required=""
                                            autocomplete="new-password">
                                        <datalist class="form-control" id="select_city" style="display:none">
                                        </datalist>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">State Code</div>
                                    <div class="col-sm-12">
                                        <input type="text" name="sh_state" id="sh_state" class="form-control sh-input">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 subrub_area">Subrub</div>
                                    <div class="col-sm-12 subrub_area">
                                        <input list="select_subrub" id="sh_suburb" name="sh_suburb"
                                            class="form-control sh-input" placeholder="โปรดระบุชานเมือง"
                                            autocomplete="new-password">
                                        <datalist class="form-control" id="select_subrub" style="display:none">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" id="submit_step2"
                                            class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                                            style="width: 100%;display:none">
                                            <span style="color:red">ถัดไป <i
                                                    class="mdi mdi-arrow-right"></i></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row" id="step_3" style="padding: 10px;">
                <form name="form_step_3" id="form_step_3" action="#">
                    <div class="col-md-12">
                        <h3 style="text-align:center"><u>กรอกข้อมูลผู้รับ</u></h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12" style="padding-top:15px;">
                                        <br />
                                        <div id="desc_area">
                                            <div class="desc">
                                                <div class="desc-topic"> รายการสินค้า
                                                    <span class="desc_number">1</span>
                                                </div>
                                                <div class="col-md-12">ชื่อสินค้า*</div>
                                                <div class="col-md-12"><input type="text"
                                                        class="d_description form-control sh-input"
                                                        placeholder="เช่น เสื้อผ้า, เครื่องเขียน" required
                                                        onchange="cal_desc()"></div>
                                                <div class="col-md-12">จำนวน(ชิ้น)*</div>
                                                <div class="col-md-12"><input type="text"
                                                        class="d_number form-control sh-input" placeholder="ต้องกรอก"
                                                        required onchange="cal_desc()"
                                                        onkeypress="return isNumberKey(event)">
                                                </div>
                                                <div class="col-md-12">มูลค่ารวม(บาท)*</div>
                                                <div class="col-md-12"><input type="text"
                                                        class="d_amount form-control sh-input" placeholder="ต้องกรอก"
                                                        required onchange="cal_desc()"
                                                        onkeypress="return isNumberKey(event)">
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button"
                                            class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                                            style="width: 100%;
                                            margin-top: 10px;
                                            border: 2px dashed #6e6b72 !important;
                                            border-radius: 10px 10px 10px 10px !important;
                                            color: #6e6b72!important;filter:unset;" onclick="adddesc()"><i
                                                class="mdi mdi-plus-circle-outline"></i>
                                            เพิ่มรายละเอียดพัสดุ</button>
                                    </div>

                                    <div class="col-md-12" style="padding-top:15px;">
                                        สกุลเงิน
                                    </div>
                                    <div class="col-md-12" style="padding-top:6px">
                                        <select class="select2 form-control mb-3 custom-select" id="sh_currency"
                                            name="sh_currency" required>
                                            @foreach ($currency as $cu_item)
                                            <option value="{{$cu_item->code}}" {{$cu_item->code=='THB'?'selected':''}}>
                                                {{$cu_item->code}} ({{$cu_item->country}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12" style="padding-top:15px;">
                                        <div class="desc-topic"> น้ำหนักและขนาดของพัสดุ</div>
                                        <div class="col-12" style="padding-bottom:10px;padding-left:unset">
                                            น้ำหนักรวม <br>
                                        </div>
                                        <div class="col-4" style="padding-bottom:10px;padding-left:unset">
                                            <input type="text" id="sh_weight" class="form-control sh_weight"
                                                style="text-align:center" placeholder="KG"
                                                onkeypress="return isNumberKey(event)" onchange="cal_box()" required="">
                                        </div>
                                        <div class="col-12" style="padding-bottom:10px;padding-left:unset">
                                            จำนวนกล่องพัสดุทั้งหมด <br>
                                        </div>
                                        <div class="col-4" style="padding-bottom:10px;padding-left:unset">
                                            <input type="number" id="box_total" class="form-control"
                                                style="text-align:center" placeholder="กล่อง" required="" value="1"
                                                minlength="1">
                                        </div>
                                        <div id="box_area">
                                            <div class="row box">
                                                <div class="col-12" style="padding-bottom:10px;">
                                                    ขนาดพัสดุกล่องที่ <span class="box_number">1</span><br>
                                                </div>

                                                <div class="col-4">
                                                    <input type="number" class="form-control width"
                                                        style="text-align:center" placeholder="กว้าง (CM)"
                                                        onchange="cal_box()">
                                                </div>
                                                <div class="col-4">
                                                    <input type="number" class="form-control length"
                                                        style="text-align:center" placeholder="ยาว (CM)"
                                                        onchange="cal_box()">
                                                </div>
                                                <div class="col-4">
                                                    <input type="number" class="form-control height"
                                                        style="text-align:center" placeholder="สูง (CM)"
                                                        onchange="cal_box()">
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="volumnWeight" onchange="get_service()">
                                        <button type="button"
                                            class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                                            style="width: 100%;
                                        margin-top: 10px;
                                        border: 2px dashed #6e6b72 !important;
                                        border-radius: 10px 10px 10px 10px !important;
                                        color: #6e6b72!important;filter:unset;" onclick="addbox()"><i
                                                class="mdi mdi-plus-circle-outline"></i>
                                            เพิ่มกล่อง</button>
                                    </div>


                                </div>
                                <div class="mb-0 desc-topic">บริการเสริม</div>
                                <div class="row" style="text-align:left">
                                    <div class="col-12" style="padding-bottom: 15px;">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Insurance">
                                            <label class="custom-control-label other_service"
                                                for="Insurance">ต้องการประกันภัยสำหรับการจัดส่ง</label>
                                        </div>
                                    </div>
                                    <div class="col-12" style="padding-bottom: 15px;">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Pack">
                                            <label class="custom-control-label other_service"
                                                for="Pack">ต้องการบริการแพ็คพัสดุ</label>
                                        </div>
                                    </div>
                                    <div class="col-12" style="padding-bottom: 15px;">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="ExportDoc">
                                            <label class="custom-control-label other_service"
                                                for="ExportDoc">ต้องการเอกสารใบขนขาออก</label>
                                        </div>
                                    </div>
                                    <div class="col-12" style="padding-bottom: 15px;">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck03">
                                            <label class="custom-control-label other_service"
                                                for="customCheck03">ระบุความต้องการอื่นๆ</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="Note_area"
                                        style="padding:25px;padding-top:unset;display:none">
                                        <textarea class="form-control" id="Note"
                                            placeholder="ระบุความต้องการของคุณ"></textarea>
                                    </div>
                                </div>
                                <div class="row" style="padding:unset;display:none;">
                                    <input type="text" class="" id="sh_description" placeholder="ภาษาอังกฤษเท่านั้น"
                                        required>
                                    <input type="text" id="sh_quantity" onkeypress="return isNumberKey(event)"
                                        placeholder="0 ">
                                    <input type="text" id="sh_declared" onkeypress="return isNumberKey(event)"
                                        placeholder="0 ">

                                </div>

                            </div>
                            <div class="col-md-6" style="padding-top: 35px;">
                                <div class="mb-0 desc-topic">เลือกบริการ</div>
                                <div class="mb-0" id="service_wait_data">
                                </div>
                                <div class="row service_select" id="service_select" style="display:none">
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" id="submit_step3"
                            class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                            style="width: 100%;display:none">
                            <span style="color:red">ถัดไป <i class="mdi mdi-arrow-right"></i></span></button>
                    </div>
                </form>
            </div>
            <div class="row" id="step_4" style="padding: 10px;">
                <form name="form_step_4" id="form_step_4" action="{{url('/app/booking')}}">
                    <div class="col-md-12">
                        <div class="row" id="sumary_area">
                            @if(isset($temp)&&$address->where('pickup',1)->count()>0)
                            {!!$temp->data!!}
                            @else
                            <div class="col-2 mobile_hide" style="text-align:center">ส่งถึง</div>
                            <div class="col-3 mobile_hide" style="text-align:center">ปลายทาง</div>
                            <div class="col-1 mobile_hide" style="text-align:center">น้ำหนัก</div>
                            <div class="col-3 mobile_hide" style="text-align:center">บริการ</div>
                            <div class="col-2 mobile_hide">ค่าบริการ</div>
                            <div class="col-1 mobile_hide"></div>
                            @if(isset($shipment))
                            @foreach ($shipment as $index=>$item)
                            <div class="col-md-12 sumary_data">
                                <input type="hidden" name="sh_type_of_service[{{$index}}]" value="{{$item->services}}">

                                <input type="hidden" name="sh_packge_type[{{$index}}]" value="PA">
                                <input type="hidden" name="sh_recipient[{{$index}}]" value="{{$item->recipient}}">
                                <input type="hidden" name="sh_company[{{$index}}]" value="{{$item->company}}">
                                <input type="hidden" name="sh_email[{{$index}}]" value="{{$item->email}}">
                                <input type="hidden" name="sh_address1[{{$index}}]" value="{{$item->address}}">
                                <input type="hidden" name="sh_address2[{{$index}}]" value="{{$item->address2}}">
                                <input type="hidden" name="sh_address3[{{$index}}]" value="{{$item->address3}}">
                                <input type="hidden" name="sh_phone[{{$index}}]" value="{{$item->phone}}">
                                <input type="hidden" name="sh_country[{{$index}}]" value="{{$item->country}}">
                                <input type="hidden" name="sh_currency[{{$index}}]" value="THB">
                                <input type="hidden" name="sh_country_code[{{$index}}]" value="{{$item->country_code}}">
                                <input type="hidden" name="sh_city[{{$index}}]" value="{{$item->city}}">
                                <input type="hidden" name="sh_suburb[{{$index}}]" value="{{$item->suburb}}">
                                <input type="hidden" name="sh_state[{{$index}}]" value="{{$item->state}}">
                                <input type="hidden" name="sh_postcode[{{$index}}]" value="{{$item->zipcode}}">

                                <input type="hidden" name="sh_Note[{{$index}}]" value="{{$item->note}}">
                                <input type="hidden" name="total_regular_item[{{$index}}]" value="{{$item->price}}">
                                <input type="hidden" name="total_weight_item[{{$index}}]" value="{{$item->weight}}">

                                @foreach ($item->box as $i=>$box_item)
                                <input type="hidden" name="series[{{$index}}][{{$i}}]" value="{{$box_item->series}}">
                                <input type="hidden" name="weight[{{$index}}][{{$i}}]" value="{{$box_item->weight}}">
                                <input type="hidden" name="width[{{$index}}][{{$i}}]" value="{{$box_item->width}}">
                                <input type="hidden" name="length[{{$index}}][{{$i}}]" value="{{$box_item->length}}">
                                <input type="hidden" name="height[{{$index}}][{{$i}}]" value="{{$box_item->height}}">
                                @endforeach
                                @foreach ($item->desc as $i=>$desc_item)
                                <input type="hidden" name="d_description[{{$index}}][{{$i}}]"
                                    value="{{$desc_item->description}}">
                                <input type="hidden" name="d_amount[{{$index}}][{{$i}}]" value="{{$desc_item->value}}">
                                <input type="hidden" name="d_number[{{$index}}][{{$i}}]"
                                    value="{{$desc_item->quantity}}">
                                @endforeach

                                <input type="hidden" name="sh_Pack[{{$index}}]" value="{{$item->pack}}">
                                <input type="hidden" name="sh_Insurance[{{$index}}]" value="{{$item->insurance}}">

                                <input type="hidden" name="sh_declared_value[{{$index}}]"
                                    value="{{$item->declared_value}}">
                                <input type="hidden" name="sh_description[{{$index}}]" value="{{$item->description}},">
                                <input type="hidden" name="sh_content_quantity[{{$index}}]"
                                    value="{{$item->content_quantity}}">


                                <div class="card client-card mobile_hide">
                                    <div class="row" style="padding-bottom:unset">
                                        <div class="col-2" style="text-align:center"><b
                                                class="cut-text">{{$item->recipient}}</b></div>
                                        <div class="col-3" style="text-align:center"><span
                                                class="cut-text">{{$item->country}}</span></div>
                                        <div class="col-1" style="text-align:center">{{$item->weight}} KG</div>
                                        <div class="col-3" style="text-align:center"><span
                                                class="cut-text">{{$item->service_name}}</span></div>
                                        <div class="col-2">{{$item->price}} บาท</div>
                                        <div class="col-1" onclick="edit_shipment({{$index}})"><span
                                                style="color:red">แก้ไข</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card client-card mobile_show">
                                    <div class="row">
                                        <div class="col-4">ชื่อผู้รับ</div>
                                        <div class="col-8"><b class="cut-text">{{$item->recipient}}</b></div>
                                        <div class="col-4">ปลายทาง</div>
                                        <div class="col-8"><span class="cut-text">{{$item->country}}</span></div>
                                        <div class="col-4">น้ำหนัก</div>
                                        <div class="col-8">{{$item->weight}} KG</div>
                                        <div class="col-4">บริการ</div>
                                        <div class="col-8"><span class="cut-text">{{$item->service_name}}</span></div>
                                        <div class="col-4">ค่าบริการ</div>
                                        <div class="col-8">{{$item->price}} บาท</div>
                                        <div class="col-12" onclick="edit_shipment({{$index}})">
                                            <h4 style="color:red;text-align:center;margin-bottom:unset">แก้ไข</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif

                            @endif
                        </div>
                        <div class="col-md-12">*ราคาจัดส่งอาจมีการเปลี่ยนแปลงตามน้ำหนัก, ขนาดพัสดุ และ
                            เมืองที่จัดส่งที่ประเทศปลายทาง
                            หากมีการเปลี่ยนแปลงเจ้าหน้าที่จะติดต่อกลับเพื่อแจ้งให้ทราบ<br />
                            ค่าบริการจัดส่งไม่รวมภาษีนำเข้าประเทศปลายทาง<br />
                            <span style="color:#3677b6;cursor:pointer" data-toggle="modal" data-animation="bounce"
                                data-target=".bs-example-modal-lg">อ่านรายละเอียการใช้บริการเพิ่มเติม</span>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" id="submit_step4"
                                class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                                style="width: 100%;display:none">
                                <span style="color:red">ถัดไป <i class="mdi mdi-arrow-right"></i></span></button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="step_5" style="padding: 10px;">
                <form name="form_step_5" id="form_step_5" action="{{url('/app/booking')}}">
                    <div class="row justify-content-md-center">
                        <div class="col-md-10">
                            <h5 style="color:red;text-align:center;font-weight:800;">เลือกวิธีจัดส่ง</h5>
                            <div class="row box-shadow">
                                <div class="col-md-5">
                                    <input type="radio" name="type_delivery" value="ไม่ระบุ" checked> เรียก SME SHIPPING
                                    เข้ารับพัสดุ
                                </div>
                                <div class="col-6 col-sm-2" style="text-align:right"> เลือกวันเข้ารับ </div>
                                <div class="col-6 col-sm-2">
                                    <input type="text" class="form-control" placeholder="โปรดระบุวันที่"
                                        name="delivery_date" id="mdate" required="" data-dtp="dtp_Qyted"
                                        value="{{date('d/m/Y')}}" />
                                </div>
                                <div class="col-6 col-sm-1" style="text-align:right"> เวลา </div>
                                <div class="col-6 col-sm-2">
                                    <select class="form-control" name="delivery_time">
                                        <option value="10.00">10.00</option>
                                        <option value="11.00">11.00</option>
                                        <option value="12.00">12.00</option>
                                        <option value="13.00">13.00</option>
                                        <option value="14.00">14.00</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row box-shadow" style="padding-bottom: 20px">
                                <div class="col-md-6">
                                    <input type="radio" name="type_delivery" value="walkin" /> นำส่งที่จุดบริการ SME
                                    SHIPPING
                                </div>
                                <div class="col-md-6" style="text-align:right" data-toggle="modal"
                                    data-animation="bounce" data-target=".view_map"><b>แผนที่จุดบริการ <i
                                            class="mdi mdi-map-marker"></i></b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" id="submit_step5"
                        class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                        style="width: 100%;display:none">
                        <span style="color:red">ถัดไป <i class="mdi mdi-arrow-right"></i></span></button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <button type="button" onclick="$('#submit_step1').trigger('click')" id="step1"
                    class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                    style="width: 100%;">
                    <span style="color:red">ถัดไป</span></button>
                <button type="button" onclick="$('#submit_step2').trigger('click')" id="step2"
                    class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                    style="width: 100%;display:none">
                    <span style="color:red">ถัดไป</span></button>
                <button type="button" onclick="$('#submit_step3').trigger('click')" id="step3"
                    class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                    style="width: 100%;display:none">
                    <span style="color:red">ถัดไป</span></button>
                <div id="step4">
                    <div class="row">
                        <div class="col-6">
                            <button type="button" onclick="addshipping()"
                                class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                                style="width: 100%">
                                <span style="color:red">+ เพิ่มพัสดุ</span></button>
                        </div>
                        <div class="col-6">
                            <button type="button" onclick="$('#submit_step4').trigger('click')"
                                class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                                style="width: 100%">
                                <span style="color:red">ถัดไป</span></button>
                        </div>
                    </div>
                </div>
                <button type="button" onclick="$('#submit_step5').trigger('click')" id="step5"
                    class="btn btn-danger btn-next btn-square btn-outline-dashed waves-effect waves-light"
                    style="width: 100%;display:none">
                    ยืนยัน</button>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</div>
</div>


<!--  Modal content for the above example -->
<div class="modal fade recipient_list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">เลือกผู้รับ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="background-color: white;">
                <div class="form-group">
                    <label for="example-input2-group1"></label>
                    <div class="input-group">
                        <input type="text" id="find_recipient" name="find_recipient" class="form-control"
                            placeholder="ค้นหาจากชื่อ หรือ เบอร์โทร">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="mdi mdi-account-search-outline"></i></span>
                        </div>
                    </div>
                </div>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                            <th>ชื่อ</th>
                            <th>ปลายทาง</th>
                            <th>เบอร์ติดต่อ</th>
                        </tr>
                    </thead>
                    <tbody id="recipient_area">

                    </tbody>
                </table>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<!--  Modal content for the above example -->
<div class="modal fade sender_list" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">เลือกผู้ส่ง</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="background-color: white;">
                <div class="form-group">
                    <label for="example-input2-group1"></label>
                    <div class="input-group">
                        <input type="text" id="find_sender" name="find_sender" class="form-control"
                            placeholder="ค้นหาจากชื่อ หรือ เบอร์โทร">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="mdi mdi-account-search-outline"></i></span>
                        </div>
                    </div>
                </div>
                <table class="table table-hover mb-0">
                    <thead>
                        <tr>
                            <th>ชื่อ</th>
                            <th>ปลายทาง</th>
                            <th>เบอร์ติดต่อ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($address as $index=>$item)
                        <tr style="cursor: pointer;"
                            onclick="set_sender('{{$item->id}}','{{$item->customer_name}}','{{$item->address}}','{{$item->tumbon}}','{{$item->amphur}}','{{$item->province}}','{{$item->postcode}}','{{$item->phone}}','{{$item->email}}')">
                            <td>{{$item->customer_name}}</td>
                            <td>
                                {{$item->address." ".$item->tumbon." ".$item->amphur." ".$item->province." ".$item->postcode}}
                            </td>
                            <td>{{$item->phone}}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<!-- ดึง body ที่ใช้ร่วมกันมาแสดง -->
@yield('content', View::make('customer/app/privacyterms'))
<!--  -->
<script src="{{ asset('assets/plugins/moment/moment.js' )}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/select2/select2.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-material-datetimepicker.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js' )}}"></script>
<script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js' )}}"></script>
<script src="{{ asset('assets/pages/jquery.forms-advanced.js?1' )}}"></script>
<script src="{{ asset('assets/js/jquery.mask.min.js?1.1' )}}"></script>
<script src="{{ asset('assets/js/app_booking.js?1.1' )}}"></script>

@if($address->where('pickup',1)->first())
<?php $address_item = $address->where('pickup',1)->first();?>
<script>
    $('#addr_name').val('{{$address_item->customer_name}}');
    $('#addr_phone').val('{{$address_item->phone}}');
    $('#addr_address').val('{{$address_item->address}}');
    $('#province').val('{{$address_item->province}}'); 
    $('#amphur').val('{{$address_item->amphur}}'); 
    $('#tumbon').val('{{$address_item->tumbon}}'); 
    $('#postcode').val('{{$address_item->postcode}}');
    $('#address_id').val('{{$address_item->id}}');
</script>

@endif
@if(isset($shipment))
<script>
    $('#addshipment_btn').show();
    nextstep(4);
</script>
@endif
@if(isset($temp)&&$address->where('pickup',1)->count()>0)
<script>
    $('#addshipment_btn').show();
    nextstep(4);
</script>
@endif

@if(app('request')->input('sh_country')!=null)
<script>
    $('#sh_country').val("{{app('request')->input('sh_country')}}").change();
</script>
@endif
@if(app('request')->input('volumnWeight')!=null)
<script>
    $('#sh_weight').val("{{app('request')->input('volumnWeight')}}").change();
</script>
@endif

@endsection