@extends('layouts.customer_app')
<?php $sum_weight = 0;$sum_package = $data->total_shipment;$total_price = 0; ?>
@section('content')
<link href="{{ asset('assets/css/style.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/css/style_app_booking.css?1' )}}" rel="stylesheet">

<style>
    body {
        color: black;
    }

    .hilight_text {
        font-weight: bold;
    }

    .line_hilight {
        border-bottom: 1px solid red;
    }

    @media print {

        .no-print,
        .no-print * {
            display: none !important;
        }
        body {
            zoom: 80%;
            margin-right: 11%;
            }
    }

    @media screen and (max-width: 900px) {
        .mobileshow {
            display: block;
        }

        .slide_data {
            display: flex;
            scroll-snap-type: x mandatory;
            overflow-x: scroll;
        }

        .mb_print {
            width: 80px;
            height: 51px;
            background: #f22727;
            color: #ffffff;
            border-radius: 10px;
            padding: 0px 0 0 0;
            text-align: center;
            position: absolute;
            right: 0px;
        }

        .mb_excel {
            width: 80px;
            height: 51px;
            background: #20973c;
            color: #ffffff;
            border-radius: 10px;
            padding: 0px 0 0 0;
            margin: auto;
            text-align: center;
        }

        .mb_history {
            width: 80px;
            height: 51px;
            background: #f2b427;
            color: #ffffff;
            border-radius: 10px;
            padding: 0px 0 0 0;
            text-align: center;
            position: absolute;
            left: 0px;
        }

        .top {
            margin-top: 14px;
        }

        .mb_text_right {
            text-align: right;
        }

        .mobileshow {
            display: none;
        }
    }

    @media screen and (min-width: 900px) {

        /* Style Desktop */
        .text-color {
            color: gray;
        }

        .text_right {
            text-align: right;
        }

        .center_all {
            text-align: center;
        }

        .text_center {
            text-align: center;
            font-size: 11px;
        }

        .line-red {
            border-bottom: 1px solid red;
        }

        .line-solid {
            border-right: 1px solid;
        }

        .print {
            width: 120px;
            height: 34px;
            background: #f22727;
            color: #ffffff;
            border-radius: 10px;
            padding: 0 0 0 0;
            text-align: center;
            position: absolute;
            right: 0px;
        }

        .excel {
            width: 120px;
            height: 34px;
            background: #20973c;
            color: #ffffff;
            border-radius: 10px;
            margin: auto;
            text-align: center;
            padding: 0 0 0 0;
        }

        .history {
            width: 120px;
            height: 34px;
            background: #f2b427;
            color: #ffffff;
            border-radius: 10px;
            padding: 0 0 0 0;
            text-align: center;
            position: absolute;
            left: 0px;
        }

        .top {
            margin-top: 14px;
        }

        .space_button {
            margin-bottom: 10px;
        }

        .mobileshow {
            display: none;
        }

        /* END */

        /*Style Dowload XLS */
        .green {
            background-color: green;
        }

        .center_xls {
            text-align: center;
        }

        .red_style {
            background-color: red;
        }
    }

    /* END */

    /* Print Detail customer */
    .mb_text_right {
        text-align: right;
    }

    .border_table {
        border: 1px solid;
    }

    .margin_space {
        margin: 0 0px 0 -15px;
    }

    /* END */
</style>

<!-- Top content -->

<!-- Desktop -->
<div class="row no-print">
    <div class="col-md-12">
        <hr style="border: 1px solid rgb(242, 39, 39);
        background-color: white;
        position: inherit;
        top: 80px;
        z-index: 2;">
        <div class="card">
            <div class="card-body ">
                <h3 style="text-align:left">Customer Detail</h3>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-1 mobile_show"></div>
                        <div class="col-4 text_right">
                            <span> Company : </span>
                        </div>
                        <div class="col-6 hilight_text">
                            <span> {{$data->company}} </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-1 mobile_show"></div>
                        <div class="col-4 text_right">
                            <span> Contact Name : </span>
                        </div>
                        <div class="col-6 hilight_text">
                            <span> {{$data->name}} </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-1 mobile_show"></div>
                        <div class="col-4 text_right">
                            <span> Address : </span>
                        </div>
                        <div class="col-6 hilight_text">
                            <span> {{$data->address}} </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-1 mobile_show"></div>
                        <div class="col-4 text_right">
                            <span> TelePhone : </span>
                        </div>
                        <div class="col-6 hilight_text">
                            <span> {{$data->phone}} </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-1 mobile_show"></div>
                        <div class="col-4 text_right">
                            <span> Date : </span>
                        </div>
                        <div class="col-6 hilight_text">
                            <span> {{$data->created_at}} </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-1 mobile_show"></div>
                        <div class="col-4 text_right">
                            <span> Customer Note : </span>
                        </div>
                        <div class="col-6 hilight_text">
                            <span> {{$data->customer_note}} </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-1 mobile_show"></div>
                        <div class="col-4 text_right">
                            <span> Manifest ID : </span>
                        </div>
                        <div class="col-6 hilight_text">
                            <span> {{$data->id}} </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row space_button no-print">
                <div class="col-3 mobile_hide"></div>
                <div class="col-2 mobile_show"></div>
                <div class="col-2">
                    <button class="btn print mb_print" onclick="print_detail()">
                        <i class="fas fa-print"></i> <span> Print </span>
                    </button>
                </div>
                <div class="col-2 center_all">
                    <button class="btn excel mb_excel" onclick="tableToExcel('testTable', 'Customer Detail')">
                        <i class="far fa-file-excel"></i><span> Export Excel </span>
                    </button>
                </div>
                <div class="col-2 mobile_show"></div>
                <div class="col-2">
                    <a href="{{url('/app/history')}}">
                        <button class=" btn history mb_history">
                            <i class="fas fa-history"></i><span> History </span>
                        </button>
                    </a>
                </div>
                <div class="col-3"></div>
            </div>
        </div>
    </div>
</div>

<!-- DATA Customer -->
<div class="card slide_data no-print" style="overflow-x: scroll;">
    <table class="table table-bordered text_center" style="color: black;">
        <thead>
            <!-- Table #0 -->
            <tr>
                <td class="line-solid" scope="col">NO.</td>
                <td class="line-solid" scope="col">SO</td>
                <td class="line-solid" scope="col">AWB</td>
                <td class="line-solid" scope="col">TRACKING</td>
                <td class="line-solid" scope="col">SERVICE</td>
                <td class="line-solid" scope="col">RECEIVER</td>
                <td class="line-solid" scope="col">COUNTRY</td>
                <td class="line-solid" scope="col">CITY</td>
                <td class="line-solid" scope="col">ZIPCODE</td>
                <td class="line-solid" scope="col">Volume(KG)</td>
                <td class="line-solid" scope="col">WEIGHT(KG)</td>
                <td class="line-solid" scope="col">PIECES</td>
                <td class="line-solid" scope="col">Other Service</td>
                <td class="line-solid" scope="col">NET CHARGE</td>
                <td scope="col">NOTE</td>
            </tr>
        </thead>
        <tbody>

            <!-- Table #1 -->
            @foreach ($shipment as $index=>$item)
            <tr>
                <td class="line-solid" scope="row">{{$index+1}}</td>
                <td><a href="/tracking/{{$item->tracking_dhl == null?$item->awb:$item->tracking_dhl}}"
                        style="color:blue;">{{$item->so}}<br> <i class="fas fa-search"></i></a></td>
                <td><a href="/tracking/{{$item->tracking_dhl == null?$item->awb:$item->tracking_dhl}}"
                        style="color:blue;">{{$item->awb}}<br><i class="fas fa-search"></i></a></td>
                <td class="line-solid">{{$item->tracking_dhl}}</td>
                <td class="line-solid">{{$item->service_name}}</td>
                <td class="line-solid">{{$item->recipient}}</td>
                <td class="line-solid">{{$item->country}}</td>
                <td class="line-solid">{{$item->city}}</td>
                <td class="line-solid">{{$item->zipcode}}</td>
                <td class="">
                    <?php $box_key=1;$package=0; ?>
                    @foreach ($box as $keyitem=>$boxitem)
                    @if ($boxitem->shipment_id === $item->id)
                    <span class="b-title box" weight="{{$boxitem->weight}}"
                    width="{{$boxitem->width}}" length="{{$boxitem->length}}"
                    height="{{$boxitem->height}}" series="{{$box_key}}"
                    status="{{$boxitem->status}}">{{$box_key++}}:</span>
                    @if($item->shipper_phone == null)
                    <span> {{$boxitem->weight}} KG //
                     {{$boxitem->width}}x{{$boxitem->length}}x{{$boxitem->height}} = {{$boxitem->weight>$boxitem->sum_weight?$boxitem->weight:$boxitem->sum_weight}}KG
                     </span>
                     @else
                     <span> {{$boxitem->weight}} KG //{{$boxitem->width}}x{{$boxitem->length}}x{{$boxitem->height}} = {{calup($boxitem->weight>$boxitem->sum_weight?$boxitem->weight:$boxitem->sum_weight)}}KG
                     </span>
                     @endif
                     <br/>
                     <?php $package = $package+1; ?>
                     @endif
                     @endforeach
                     <hr class="line-npd" />
                     <?php $package > 0? $sum_package=$sum_package+$package-1:$sum_package=$sum_package; ?>
                     @if (isset($item->total_sum_weight))
                     <div class="sum-bottom">
                        Total Weight : <span class="total_weight"
                        set="{{$item->total_sum_weight}}">{{$item->total_sum_weight}}</span>
                         KG.
                     </div>
                    <?php $sum_weight =$sum_weight+ $item->total_sum_weight;?>
                    @endif
                </td>
                <td class="line-solid">{{$item->weight}}</td>
                <td class="line-solid">1</td>
                <td class="line-solid">
                @foreach ($item->other_service_name as $key=>$other_item)
                <span class="b-title">{{$other_item}}</span>
                <span>{{number_format($item->other_service_price[$key],2)}}</span>
                <hr class="line-npd" />
                @endforeach
                <div class="sum-bottom">Total Amount:
                    {{number_format($item->other_service_total_price,2)}} ฿
                </div>
                </td>
                <td class="line-solid">{{$item->price}}</td>
                <td>{{$item->note}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<!-- end -->

<!-- Total price -->
<?php
                     $total_sup= 0;
                     $total_weight= 0;
                        ?>
@foreach ($shipment as $item)
<?php
                     $total_sup = $item->price +$item->other_service_total_price + $total_sup;
                     $total_weight = $total_weight+$item->weight;
                        ?>
@endforeach

<div class="card no-print">
    <div class="row top">
        <div class="col-md-6">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-5">
                    <span> TOTAL PACKAGE </span>
                </div>
                <div class="col-2 mb_text_right ">
                    <span> {{count($box)}} </span>
                </div>
                <div class="col-3 text-color">
                    <span> PIECES </span>
                </div>
            </div>
            <div class="row">
                <div class="col-1"></div>
                <div class="col-5">
                    <span> TOTAL SHIPMENT </span>
                </div>
                <div class="col-2 mb_text_right ">
                    <span> {{count($shipment)}} </span>
                </div>
                <div class="col-3 text-color">
                    <span> SHIPMENT </span>
                </div>
            </div>
            <div class="row">
                <div class="col-1"></div>
                <div class="col-5">
                    <span> TOTAL WEIGHT </span>
                </div>
                <div class="col-2 mb_text_right ">
                    <span> {{$total_weight}} </span>
                </div>
                <div class="col-3 text-color">
                    <span> KG </span>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-3 mobile_hide"></div>
                <div class="col-1 mobile_show"></div>
                <div class="col-4">
                    <span> SUB TOTAL </span>
                </div>
                <div class="col-3 mb_text_right ">
                    <span> {{$total_sup}} </span>
                </div>
                <div class="col-2">
                    <span> THB </span>
                </div>
            </div>
            <div class="row">
                <div class="col-3 mobile_hide"></div>
                <div class="col-1 mobile_show"></div>
                <div class="col-4">
                    <span> PICKUP FEE </span>
                </div>
                <div class="col-3 mb_text_right ">
                    <span> 0.00 </span>
                </div>
                <div class="col-2">
                    <span> THB </span>
                </div>
            </div>
            <div class="row">
                <div class="col-3 mobile_hide"></div>
                <div class="col-1 mobile_show"></div>
                <div class="col-4">
                    <span> DISCOUNT </span>
                </div>
                <div class="col-3 mb_text_right ">
                    <span> {{$data->discount}} </span>
                </div>
                <div class="col-2">
                    <span> THB </span>
                </div>
            </div>
            <div class="row">
                <div class="col-3 mobile_hide"></div>
                <div class="col-1 mobile_show"></div>
                <div class="col-4 ">
                    <span> TOTAL </span>
                </div>
                <div class="col-3 mb_text_right ">
                    <span> {{$total_sup - $data->discount}} </span>
                </div>
                <div class="col-2">
                    <span> THB </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END Desktop -->

<!-- Print Detail Customer -->
<div class=" mobileshow margin_space">
    <div class="row">
        <div class="col-1"></div>
        <div class="col-7">
            <div>
                <span> Company : {{$data->company}} </span>
            </div>
            <div>
                <span> Contact Name : {{$data->name}} </span>
            </div>
            <div>
                <span> Address : {{$data->address}} </span>
            </div>
        </div>
        <div class="col-3
        ">
            <div>
                <span> TelePhone : {{$data->phone}} </span>
            </div>
            <div>
                <span> Date : {{$data->created_at}} </span>
            </div>
            <div>
                <span> Customer Note : {{$data->customer_note}} </span>
            </div>
            <div>
                <span> Manifest ID : {{$data->id}} </span>
            </div>
        </div>
        <div class="col-1"></div>
    </div>
    <br>
    <div class="table-responsive-sm margin_space ">
        <table class="table table-bordered text_center">
            <thead>
                <tr>
                    <th>NO.</th>
                    <th>SO</th>
                    <th>AWB</th>
                    <th>TRACKING</th>
                    <th>SERVICE</th>
                    <th>RECEIVER</th>
                    <th>COUNTRY</th>
                    <th>CITY</th>
                    <th>ZIPCODE</th>
                    <th>Volume(KG)</th>
                    <th>WEIGHT(KG)</th>
                    <th>PIECES</th>
                    <td>Other Service</td>
                    <th>NET CHARGE</th>
                    <th>NOTE</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($shipment as $index=>$item)
                <tr>
                    <td>{{$index+1}} </td>
                    <td>{{$item->so}} </td>
                    <td>{{$item->awb}}</td>
                    <td>{{$item->tracking_dhl}}</td>
                    <td>{{$item->service_name}}</td>
                    <td>{{$item->recipient}} </td>
                    <td>{{$item->country}} </td>
                    <td>{{$item->city}} </td>
                    <td>{{$item->zipcode}} </td>
                    <td>
                        <?php $box_key=1;$package=0; ?>
                        @foreach ($box as $keyitem=>$boxitem)
                        @if ($boxitem->shipment_id === $item->id)
                            <span class="b-title box" weight="{{$boxitem->weight}}"
                            width="{{$boxitem->width}}" length="{{$boxitem->length}}"
                            height="{{$boxitem->height}}" series="{{$box_key}}"
                            status="{{$boxitem->status}}">{{$box_key++}}:</span>
                            @if($item->shipper_phone == null)
                            <span> {{$boxitem->weight}} KG //
                            {{$boxitem->width}}x{{$boxitem->length}}x{{$boxitem->height}} = {{$boxitem->weight>$boxitem->sum_weight?$boxitem->weight:$boxitem->sum_weight}}KG
                            </span>
                            @else
                            <span> {{$boxitem->weight}} KG //{{$boxitem->width}}x{{$boxitem->length}}x{{$boxitem->height}} = {{calup($boxitem->weight>$boxitem->sum_weight?$boxitem->weight:$boxitem->sum_weight)}}KG
                            </span>
                            @endif
                            <br/>
                            <?php $package = $package+1; ?>
                        @endif
                        @endforeach
                        <hr class="line-npd" />
                        <?php $package > 0? $sum_package=$sum_package+$package-1:$sum_package=$sum_package; ?>
                        @if (isset($item->total_sum_weight))
                        <div class="sum-bottom">
                            Total Weight : <span class="total_weight"
                            set="{{$item->total_sum_weight}}">{{$item->total_sum_weight}}</span>
                            KG.
                        </div>
                        <?php $sum_weight =$sum_weight+ $item->total_sum_weight;?>
                        @endif
                    </td>
                    <td>{{$item->weight}} </td>
                    <td> 1 </td>
                    <td class="line-solid">
                    @foreach ($item->other_service_name as $key=>$other_item)
                    <span class="b-title">{{$other_item}}</span>
                    <span>{{number_format($item->other_service_price[$key],2)}}</span>
                    <hr class="line-npd" />
                    @endforeach
                    <div class="sum-bottom">Total Amount:
                        {{number_format($item->other_service_total_price,2)}} ฿
                    </div>
                    </td>
                    <td>{{$item->price}} </td>
                    <td>{{$item->note}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>
    </div>
    <div>
        <div class="row">
            <div class="col-6">
            </div>
            <div class="col-3">
                <div>
                    <span> TOTAL PACKAGE :</span>
                    <span> {{count($box)}} </span>
                    <span> PIECES </span>
                </div>
                <div>
                    <span> TOTAL SHIPMENT :</span>
                    <span> {{count($shipment)}} </span>
                    <span> SHIPMENT </span>
                </div>
                <div>
                    <span> TOTAL WEIGHT :</span>
                    <span> {{$total_weight}} </span>
                    <span> KG </span>
                </div>
            </div>
            <div class="col-3">
                <div>
                    <span> SUB TOTAL :</span>
                    <span> {{$total_sup}} </span>
                    <span> KG </span>
                </div>
                <div>
                    <span> PICKUP FEE :</span>
                    <span> 0.00 </span>
                    <span> THB </span>
                </div>
                <div>
                    <span> DISCOUNT :</span>
                    <span> {{$data->discount}} </span>
                    <span> THB </span>
                </div>
                <div>
                    <span> TOTAL :</span>
                    <span> {{$total_sup - $data->discount}} </span>
                    <span> THB </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END -->

<!-- Dowload XLS -->
<div class="mobileshow no-print">
    <table id="testTable" rules="groups" frame="hsides">
        <caption>
            Shipping Manifest
        </caption>
        <colgroup align="center"></colgroup>
        <colgroup align="left"></colgroup>
        <colgroup span="2" align="center"></colgroup>
        <colgroup span="3" align="center"></colgroup>
        <tbody class="center_xls">
            <tr>
                <td class="center_xls">Company :</td>
                <td class="center_xls">{{$data->company}}</td>
            </tr>
            <tr>
                <td>Contact Name :</td>
                <td>{{$data->name}}</td>
            </tr>
            <tr>
                <td> Address :</td>
                <td>{{$data->address}}</td>
            </tr>
            <tr>
                <td>TelePhone :</td>
                <td>'{{$data->phone}}</td>
            </tr>
            <tr>
                <td> Date :</td>
                <td>{{$data->created_at}}</td>
            </tr>
            <tr>
                <td>Customer Note :</td>
                <td>{{$data->customer_note}}</td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <thead valign="top">
                <tr>
                    <th class="red_style" scope="col">NO.</th>
                    <th class="red_style" scope="col">SO</th>
                    <th class="red_style" scope="col">AWB</th>
                    <th class="red_style" scope="col">TRACKING</th>
                    <th class="red_style" scope="col">SERVICE</th>
                    <th class="red_style" scope="col">RECEIVER</th>
                    <th class="red_style" scope="col">COUNTRY</th>
                    <th class="red_style" scope="col">CITY</th>
                    <th class="red_style" scope="col">ZIPCODE</th>
                    <th class="red_style" scope="col">Volume(KG)</th>
                    <th class="red_style" scope="col">WEIGHT(KG)</th>
                    <th class="red_style" scope="col">PIECES</th>
                    <th class="red_style" scope="col">Other Service</th>
                    <th class="red_style" scope="col">NET CHARGE</th>
                    <th class="red_style" scope="col">NOTE</th>
                </tr>
            </thead>
            @foreach ($shipment as $index=>$item)
            <tr>
                <td>{{$index+1}}</td>
                <td>{{$item->so}}</td>
                <td>{{$item->awb}}</td>
                <td>{{$item->tracking_dhl}}</td>
                <td>{{$item->service_name}}</td>
                <td>{{$item->recipient}}</td>
                <td>{{$item->country}}</td>
                <td>{{$item->city}}</td>
                <td>{{$item->zipcode}}</td>
                <td>
                    <?php $box_key=1;$package=0; ?>
                    @foreach ($box as $keyitem=>$boxitem)
                    @if ($boxitem->shipment_id === $item->id)
                        <span class="b-title box" weight="{{$boxitem->weight}}"
                        width="{{$boxitem->width}}" length="{{$boxitem->length}}"
                        height="{{$boxitem->height}}" series="{{$box_key}}"
                        status="{{$boxitem->status}}">{{$box_key++}}:</span>
                        @if($item->shipper_phone == null)
                        <span> {{$boxitem->weight}} KG //
                        {{$boxitem->width}}x{{$boxitem->length}}x{{$boxitem->height}} = {{$boxitem->weight>$boxitem->sum_weight?$boxitem->weight:$boxitem->sum_weight}}KG
                        </span>
                        @else
                        <span> {{$boxitem->weight}} KG //{{$boxitem->width}}x{{$boxitem->length}}x{{$boxitem->height}} = {{calup($boxitem->weight>$boxitem->sum_weight?$boxitem->weight:$boxitem->sum_weight)}}KG
                        </span>
                        @endif
                        <br/>
                        <?php $package = $package+1; ?>
                        @endif
                        @endforeach
                        <?php $package > 0? $sum_package=$sum_package+$package-1:$sum_package=$sum_package; ?>
                        @if (isset($item->total_sum_weight))
                        <div class="sum-bottom">
                        Total Weight : <span class="total_weight"
                        set="{{$item->total_sum_weight}}">{{$item->total_sum_weight}}</span>
                        KG.
                        </div>
                    <?php $sum_weight =$sum_weight+ $item->total_sum_weight;?>
                    @endif
                </td>
                <td>{{$item->weight}}</td>
                <td>1</td>
                <td>
                    @foreach ($item->other_service_name as $key=>$other_item)
                    <span class="b-title">{{$other_item}}</span>
                    <span>{{number_format($item->other_service_price[$key],2)}}</span>
                    <hr class="line-npd" />
                    @endforeach
                    <div class="sum-bottom">Total Amount:
                        {{number_format($item->other_service_total_price,2)}} ฿
                    </div>
                </td>
                <td>{{$item->price}}</td>
                <td>{{$item->note}}</td>
            </tr>
            @endforeach
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>SUB TOTAL</td>
                <td>{{$total_sup}}</td>
                <td>THB</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>TOTAL PACKAGE</td>
                <td>{{count($box)}}</td>
                <td>PIECES</td>
                <td>PICKUP FEE</td>
                <td>0.00</td>
                <td>THB</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>TOTAL SHIPMENT</td>
                <td>{{count($shipment)}}</td>
                <td>SHIPMENT</td>
                <td>DISCOUNT</td>
                <td>{{$data->discount}}</td>
                <td>THB</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>TOTAL WEIGHT</td>
                <td>{{$total_weight}}</td>
                <td>KG</td>
                <td>TOTAL</td>
                <td>{{$total_sup - $data->discount>0?$total_sup - $data->discount:0}}</td>
                <td>THB</td>
            </tr>
        </tbody>
    </table>
</div>
<!-- END -->

<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var tableToExcel = (function() {
    // Define your style class template.
    var style = "<style>.red_style { background-color: red;}  .center_xls{text-align: center;}</style>";
    var uri = 'data:application/vnd.ms-excel;base64,',
        template =
        '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->' +
        style + '</head><body><table>{table}</table></body></html>',
        base64 = function(s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        },
        format = function(s, c) {
            return s.replace(/{(\w+)}/g, function(m, p) {
                return c[p];
            })
        }
    return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {
            worksheet: name || 'Worksheet',
            table: table.innerHTML
        }
        downloadWithName(uri + base64(format(template, ctx)), name + ".xls");
    }
})()

function downloadWithName(uri, name) {
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.click();
}

function print_detail() {
    window.print('#detail_customer');
}

$("#loading").hide();
</script>
@endsection
