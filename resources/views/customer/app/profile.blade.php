@extends('layouts.customer_app')
@section('content')
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/dependencies/JQL.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('assets/dependencies/typeahead.bundle.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/dist/jquery.Thailand.min.js') }}"></script>
<style>
h3 {
    color: #656269 !important;
    font-size: 16px !important;
}

.text-info,
.mt-2 {
    color: #656269 !important;
}

.textcenter {
    padding: unset;
}

.topic {
    font-weight: bold;
    font-size: 18px !important;
    color: #656269 !important;
}

.btn {
    font-family: 'Prompt', sans-serif;
}

.project-title {
    font-family: 'Prompt', sans-serif;
    font-weight: bold;
    font-size: 16px;
    color: #3677b6 !important;
}

.text-secondary {
    color: #656269 !important;
    font-size: 14px;
    font-weight: bold;
}

.text-muted {
    color: #656269 !important;
}

.nav-item .nav-link {
    color: #656269 !important;
    font-weight: bold;
    background: #d7d7d8;
}

.btn-next {
    background: white !important;
    border: unset !important;
    color: #ef2828 !important;
    font-size: 18px;
    filter: drop-shadow(rgba(0, 0, 0, 0.15) 0px 3px 6px);
}

.modal-content .modal-header,
.modal-content .modal-footer {
    border-color: #F44336;
    background-color: #ffffff;
}

.modal-body {
    background-color: white;
}

.color-red {
    color: red;

}

.fa-check {
    font-size: 71px;
    color: #27ae60;
    border: 1px solid #27ae60;
    padding: 13px;
    border-radius: 50%;
    margin: 0px 0 0 39%;
}

.newpassword {
    text-align: center;
    padding-top: 15px;
}


.toggle-password {
    /* position: fixed;
    right: 52%;
    top: 44%; */
}

.toggle-repassword {
    /* position: fixed;
    right: 52%;
    top: 59%; */
}

.form-control.is-valid:focus,
.was-validated .form-control:valid:focus {
    border-color: #007bff;
    box-shadow: 0 0 0 0;
}

.custom-control-input.is-valid:focus~.custom-control-label::before,
.was-validated .custom-control-input:valid:focus~.custom-control-label::before {
    box-shadow: 0 0 0 .2rem rgba(0, 123, 255, .25);
}

.custom-file-input.is-valid~.custom-file-label,
.was-validated .custom-file-input:valid~.custom-file-label {
    border-color: #007bff;
}

.custom-control-input.is-valid~.custom-control-label::before,
.was-validated .custom-control-input:valid~.custom-control-label::before {
    border-color: #007bff;
}

.form-control.is-valid,
.was-validated .form-control:valid {
    border: 1px solid #e8ebf3;
    padding-right: calc(1.5em + .75rem);
    background-image: url();
    background-repeat: no-repeat;
    background-position: center right calc(.375em + .1875rem);
    background-size: calc(.75em + .375rem) calc(.75em + .375rem);
}

.form-control.is-invalid,
.was-validated .form-control:invalid {
    border-color: #dc3545;
    padding-right: calc(1.5em + .75rem);
    background-image: url();
    background-repeat: no-repeat;
    background-position: center right calc(.375em + .1875rem);
    background-size: calc() calc(.75em + .375rem);
}

.custom-control-input.is-valid~.custom-control-label::before,
.was-validated .custom-control-input:valid~.custom-control-label::before {
    border-color: #e8ebf3;
}

.custom-control-input.is-valid:checked~.custom-control-label::before,
.was-validated .custom-control-input:valid:checked~.custom-control-label::before {
    color: #fff;
    border-color: #007bff;
    background-color: #007bff;
}

.custom-checkbox .custom-control-input:checked~.custom-control-label::after {
    border-color: #ffffff;
}

.login_ {
    padding: 6px;
    text-align: center;
    border: 1px solid #e8ebf3;
    border-left: 0px saddlebrown;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
}

.login_spaced {
    border-top-right-radius: 0px !important;
    border-bottom-right-radius: 0px !important;
    border-right: 0px;
}

.spaced_col {
    padding-right: 0px;
}

@media screen and (max-width:768px) {
    .toggle-password {
        /* position: fixed;
    right: 13%;
    top: 48%; */
    }

    .toggle-repassword {
        /* position: fixed;
        right: 13%;
        top: 66%; */
    }
}
</style>
<br />
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="row" style="padding-bottom:0px !important;">
                <div class="col-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#teb1" role="tab"
                                aria-selected="true">ข้อมูลเข้าสู่ระบบ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#teb2" role="tab"
                                aria-selected="false">ที่อยู่ผู้รับ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#teb3" role="tab"
                                aria-selected="false">ที่อยู่ผู้ส่ง</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#teb4" role="tab"
                                aria-selected="false">ใบกำกับภาษี</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content detail-list" id="pills-tabContent">

                <div class="tab-pane fade " id="teb2">
                    <form name="edit_customer" id="edit_customer" action="{{route('customer.profile')}}" method="POST">
                        @csrf
                        <div class="row">
                            @foreach ($address as $item)
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="text-left project-card">
                                            <span class="badge badge-soft-success font-11"></span>
                                            <h3 class="project-title">{{$item->customer_name}}</h3>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <p class="text-muted" id="add_{{$item->id}}">
                                                        <span class="company"
                                                            style="display:none">{{$item->company}}</span>
                                                        <span class="card_id"
                                                            style="display:none">{{$item->card_id}}</span>
                                                        <span class="text-secondary font-14"><b>ที่อยู่
                                                                : </b></span>
                                                        <span class="address">{{$item->address}}</span>,
                                                        <span class="tumbon_name"
                                                            set="{{$item->tumbon}}">{{$item->tumbon}}</span>,
                                                        <span class="amphur_name" set="{{$item->amphur}}">
                                                            {{$item->amphur}}</span>, <span class="province_name"
                                                            set="{{$item->province}}">{{$item->province}}</span>
                                                        <span class="postcode">{{$item->postcode}}</span>
                                                        <br />
                                                        <span class="text-secondary font-14"><b>เบอรติดต่อ
                                                                : </b></span> <span
                                                            class="phone">{{$item->phone}}</span>
                                                        <span class="email" style="display:none">{{$item->email}}</span>
                                                        <span class="fullname"
                                                            style="display:none">{{$item->customer_name}}</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <div>
                                                        <button type="button" class="btn btn-sm btn-danger" style="position: absolute;
                                                                    right: 0;
                                                                    top: 10px;
                                                                    z-index: 2;" data-toggle="modal"
                                                            data-animation="bounce" data-target=".edit_customer_modal"
                                                            onclick="edit_address({{$item->id}})">แก้ไขที่อยู่นี้</button>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="pickup{{$item->id}}"
                                                            {{$item->pickup==1?'checked':''}} name="pickup"
                                                            onchange="$('#edit_customer').submit();"
                                                            value="{{$item->id}}" class="custom-control-input">
                                                        <label class="custom-control-label"
                                                            for="pickup{{$item->id}}">ตั้งเป็นที่อยู่ส่งพัสดุ</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="invoice{{$item->id}}"
                                                            onchange="$('#edit_customer').submit();"
                                                            {{$item->invoice==1?'checked':''}} name="invoice"
                                                            value="{{$item->id}}" class="custom-control-input">
                                                        <label class="custom-control-label"
                                                            for="invoice{{$item->id}}">ตั้งเป็นที่อยู่ออกใบเสร็จ</label>
                                                    </div>
                                                </div>
                                                <!--end img-group-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--end card-body-->
                                </div>
                                <!--end card-->
                            </div>
                            @endforeach
                        </div>
                        <!--end col-->
                        <!--end row-->
                        <div class="row justify-content-md-center">
                            <div class="col-md-2">
                                <button type="button" data-toggle="modal" data-animation="bounce"
                                    data-target=".edit_customer_modal" onclick="show_add_address()"
                                    class="btn btn-danger btn-next waves-effect waves-light">เพิ่มที่อยู่</button>
                            </div>
                        </div>
                    </form>
                </div>

                <!--end general detail-->

                <!-- Form Password -->
                <div class="tab-pane fade show active" id="teb1">
                    <form name="edit_password" id="edit_password" action="{{url('/app/profile_password')}}"
                        method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="card" style="border-top-left-radius: 0px !important;
                                        border-top-right-radius: 0px !important;
                                        background-color: white;
                                        filter: drop-shadow(0px 0px 0px white) !important;
                                        -webkit-box-shadow: unset !important;
                                        background-color: white;
                                        box-shadow: unset !important;">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h5 style="font-weight: bold; font-size: 25px;">
                                                            ข้อมูลเข้าสู่ระบบ</h5>
                                                        <input type="hidden" id="edit_id_pass" value="60226">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-2">Username</div>
                                                    <div class="col-11 col-sm-3 spaced_col">
                                                        <input type="text" name="username" id="username"
                                                            class="form-control login_spaced"
                                                            placeholder="กรุณากรอกอีเมล์" required="" disabled
                                                            value="{{$customer->username}}">
                                                    </div>
                                                    <div class="col-1 col-sm-1 login_"
                                                        style="background-color: #e9ecef;"></div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12 col-sm-2">รหัสผ่าน*</div>
                                                    <div class="col-10 col-sm-3 spaced_col">
                                                        <input type="password" name="password" id="password"
                                                            class="form-control login_spaced" value="******"
                                                            placeholder="กรุณากรอกรหัสผ่าน" required="">
                                                    </div>
                                                    <div class="col-2 col-sm-1 login_ password_">
                                                        <span toggle="#password"
                                                            class="fa fa-fw fa-eye field-icon toggle-password toggle_password"></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-2">ยืนยันรหัสผ่าน*</div>
                                                    <div class="col-10 col-sm-3 spaced_col">
                                                        <input type="password" name="repassword" id="repassword"
                                                            class="form-control login_spaced" value="******"
                                                            placeholder="กรุณากรอกรหัสผ่าน" required="">
                                                    </div>
                                                    <div class="col-2 col-sm-1 login_ repassword_">
                                                        <span toggle="#repassword"
                                                            class="fa fa-fw fa-eye field-icon toggle-repassword toggle_password"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="padding:15px">
                                                    <button type="submit" class="btn btn-sm btn-danger">ยืนยัน</button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="email" id="email" value="{{$customer->email}}">
                                        </div>
                                    </div>
                                </div>
                                <!--end card-body-->
                            </div>
                            <!--end card-->
                        </div>
                        <!--end col-->
                    </form>
                </div>
                <!-- end form Password -->

                <!-- ข้อมูลผู้ส่ง -->
                <div class="tab-pane fade " id="teb3">
                    <form name="edit_customer" id="edit_customer" action="{{route('customer.profile')}}" method="POST">
                        @csrf
                        <div class="row">
                            @foreach ($address as $item)
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="text-left project-card">
                                            <span class="badge badge-soft-success font-11"></span>
                                            <h3 class="project-title">{{$item->customer_name}}</h3>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <p class="text-muted" id="add_{{$item->id}}">
                                                        <span class="company"
                                                            style="display:none">{{$item->company}}</span>
                                                        <span class="card_id"
                                                            style="display:none">{{$item->card_id}}</span>
                                                        <span class="text-secondary font-14"><b>ที่อยู่
                                                                : </b></span>
                                                        <span class="address">{{$item->address}}</span>,
                                                        <span class="tumbon_name"
                                                            set="{{$item->tumbon}}">{{$item->tumbon}}</span>,
                                                        <span class="amphur_name" set="{{$item->amphur}}">
                                                            {{$item->amphur}}</span>, <span class="province_name"
                                                            set="{{$item->province}}">{{$item->province}}</span>
                                                        <span class="postcode">{{$item->postcode}}</span>
                                                        <br />
                                                        <span class="text-secondary font-14"><b>เบอรติดต่อ
                                                                : </b></span> <span
                                                            class="phone">{{$item->phone}}</span>
                                                        <span class="email" style="display:none">{{$item->email}}</span>
                                                        <span class="fullname"
                                                            style="display:none">{{$item->customer_name}}</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <div>
                                                        <button type="button" class="btn btn-sm btn-danger" style="position: absolute;
                                                                    right: 0;
                                                                    top: 10px;
                                                                    z-index: 2;" data-toggle="modal"
                                                            data-animation="bounce" data-target=".edit_customer_modal"
                                                            onclick="edit_address({{$item->id}})">แก้ไขที่อยู่นี้</button>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="pickup{{$item->id}}"
                                                            {{$item->pickup==1?'checked':''}} name="pickup"
                                                            onchange="$('#edit_customer').submit();"
                                                            value="{{$item->id}}" class="custom-control-input">
                                                        <label class="custom-control-label"
                                                            for="pickup{{$item->id}}">ตั้งเป็นที่อยู่ส่งพัสดุ</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="invoice{{$item->id}}"
                                                            onchange="$('#edit_customer').submit();"
                                                            {{$item->invoice==1?'checked':''}} name="invoice"
                                                            value="{{$item->id}}" class="custom-control-input">
                                                        <label class="custom-control-label"
                                                            for="invoice{{$item->id}}">ตั้งเป็นที่อยู่ออกใบเสร็จ</label>
                                                    </div>
                                                </div>
                                                <!--end img-group-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--end card-body-->
                                </div>
                                <!--end card-->
                            </div>
                            @endforeach
                        </div>
                        <!--end col-->
                        <!--end row-->
                        <div class="row justify-content-md-center">
                            <div class="col-md-2">
                                <button type="button" data-toggle="modal" data-animation="bounce"
                                    data-target=".edit_customer_modal" onclick="show_add_address()"
                                    class="btn btn-danger btn-next waves-effect waves-light">เพิ่มที่อยู่</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- End ข้อมูลผู้ส่ง -->

                <!-- ข้อมูลผู้ส่ง -->
                <div class="tab-pane fade " id="teb4">
                    <form name="" id="" action="" method="POST">
                        @csrf
                        <div class="row">
                            @foreach ($address as $item)
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="text-left project-card">
                                            <span class="badge badge-soft-success font-11"></span>
                                            <h3 class="project-title">{{$item->customer_name}}</h3>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <p class="text-muted" id="add_{{$item->id}}">
                                                        <span class="company"
                                                            style="display:none">{{$item->company}}</span>
                                                        <span class="card_id"
                                                            style="display:none">{{$item->card_id}}</span>
                                                        <span class="text-secondary font-14"><b>ที่อยู่
                                                                : </b></span>
                                                        <span class="address">{{$item->address}}</span>,
                                                        <span class="tumbon_name"
                                                            set="{{$item->tumbon}}">{{$item->tumbon}}</span>,
                                                        <span class="amphur_name" set="{{$item->amphur}}">
                                                            {{$item->amphur}}</span>, <span class="province_name"
                                                            set="{{$item->province}}">{{$item->province}}</span>
                                                        <span class="postcode">{{$item->postcode}}</span>
                                                        <br />
                                                        <span class="text-secondary font-14"><b>เบอรติดต่อ
                                                                : </b></span> <span
                                                            class="phone">{{$item->phone}}</span>
                                                        <span class="email" style="display:none">{{$item->email}}</span>
                                                        <span class="fullname"
                                                            style="display:none">{{$item->customer_name}}</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <div>
                                                        <button type="button" class="btn btn-sm btn-danger" style="position: absolute;
                                                                    right: 0;
                                                                    top: 10px;
                                                                    z-index: 2;" data-toggle="modal"
                                                            data-animation="bounce" data-target=".edit_customer_modal"
                                                            onclick="edit_address({{$item->id}})">แก้ไขที่อยู่นี้</button>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="pickup{{$item->id}}"
                                                            {{$item->pickup==1?'checked':''}} name="pickup"
                                                            onchange="$('#edit_customer').submit();"
                                                            value="{{$item->id}}" class="custom-control-input">
                                                        <label class="custom-control-label"
                                                            for="pickup{{$item->id}}">ตั้งเป็นที่อยู่ส่งพัสดุ</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="invoice{{$item->id}}"
                                                            onchange="$('#edit_customer').submit();"
                                                            {{$item->invoice==1?'checked':''}} name="invoice"
                                                            value="{{$item->id}}" class="custom-control-input">
                                                        <label class="custom-control-label"
                                                            for="invoice{{$item->id}}">ตั้งเป็นที่อยู่ออกใบเสร็จ</label>
                                                    </div>
                                                </div>
                                                <!--end img-group-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--end card-body-->
                                </div>
                                <!--end card-->
                            </div>
                            @endforeach
                        </div>
                        <!--end col-->
                        <!--end row-->
                        <div class="row justify-content-md-center">
                            <div class="col-md-2">
                                <button type="button" data-toggle="modal" data-animation="bounce"
                                    data-target=".edit_customer_modal" onclick="show_add_address()"
                                    class="btn btn-danger btn-next waves-effect waves-light">เพิ่มที่อยู่</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- End ข้อมูลผู้ส่ง -->
            </div>
            <!--end row-->
        </div>
        <!--end education detail-->
    </div>
    <!--end tab-content-->

    <!--end row-->
</div>
@if($errors->any())
<script>
alert('{{$errors->first()}}');
</script>
@endif
<!--  Modal content for the above example -->
<div class="modal fade edit_customer_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title mt-0 topic" id="address_title">Edit Customer</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12" id="add_address_area" style="display:none;">
                    <form name="add_address_form" id="add_address_form" action="{{url('/app/add_customer_address')}}"
                        method="POST" novalidate>
                        <div class="row">
                            <div class="col-sm-4 topic">ชื่อ - สกุล<span class="color-red">*</span></div>
                            <div class="col-sm-8">
                                <input type="text" name="name" id="addr_name" class="form-control" placeholder=""
                                    required="" autocomplete="off">
                                <div class="invalid-feedback">กรุณากรอกชื่อ นามสกุล</div>
                                <input type="hidden" name="customer_id" id="addr_customer_id" class="form-control"
                                    value="{{Session::get('cus.id')}}" autocomplete="off">
                                <input type="hidden" name="address_id" id="address_id" class="form-control" value=""
                                    autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 topic">หมายเลขโทรศัพท์<span class="color-red">*</span></div>
                            <div class="col-sm-8">
                                <input type="text" name="phone" id="addr_phone" class="form-control" placeholder=""
                                    required="" autocomplete="off">
                                <div class="invalid-feedback">กรุณากรอก หมายเลขโทรศัพท์</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 topic">ชื่อบริษัท</div>
                            <div class="col-sm-8">
                                <input type="text" name="company" id="company" class="form-control" placeholder=""
                                    autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 topic">เลขประจำตัวผู้เสียภาษี</div>
                            <div class="col-sm-8">
                                <input type="text" name="card_id" id="card_id" class="form-control" placeholder=""
                                    autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 topic">อีเมล์<span class="color-red">*</span></div>
                            <div class="col-sm-8">
                                <input type="email" name="email" id="addr_email" class="form-control" placeholder=""
                                    multiple autocomplete="off" required>
                                <div class="invalid-feedback">กรุณากรอก อีเมล์</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 topic">ที่อยู่<span class="color-red">*</span></div>
                            <div class="col-sm-8">
                                <input type="text" name="address" id="addr_address" class="form-control" placeholder=""
                                    required="" autocomplete="off">
                                <div class="invalid-feedback">กรุณากรอกที่อยู่</div>
                            </div>
                        </div>
                        <link rel="stylesheet" href="{{ asset('assets/dist/jquery.Thailand.min.css') }}">

                        <div id="demo1" class="demo" uk-grid>
                            <div class="row">
                                <div class="col-sm-4 topic">ตำบล / แขวง<span class="color-red">*</span></div>
                                <div class="col-sm-8 uk-width-1-2@m">
                                    <input name="tumbon" id="tumbon" class="form-control" type="text" placeholder=""
                                        required="" autocomplete="new-password">
                                    <div class="invalid-feedback">กรุณากรอกตำบล / แขวง</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 topic">อำเภอ / เขต<span class="color-red">*</span></div>
                                <div class="col-sm-8 uk-width-1-2@m">
                                    <input name="amphur" id="amphur" class="form-control" type="text"
                                        autocomplete="new-password" required="">
                                    <div class="invalid-feedback">กรุณากรอกอำเภอ / เขต</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 topic">จังหวัด<span class="color-red">*</span></div>
                                <div class="col-sm-8 uk-width-1-2@m">
                                    <input name="province" id="province" class="form-control" type="text"
                                        autocomplete="new-password" required="">
                                    <div class="invalid-feedback">กรุณากรอก จังหวัด</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 topic">รหัสไปรษณีย์<span class="color-red">*</span></div>
                                <div class="col-sm-8 uk-width-1-2@m">
                                    <input name="postcode" id="postcode" class="form-control" type="text"
                                        autocomplete="new-password" required="">
                                    <div class="invalid-feedback">กรุณากรอก รหัสไปรษณีย์</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="pickup" id="pickup"
                                            data-parsley-multiple="groups" data-parsley-mincheck="2">
                                        <label class="custom-control-label topic" for="pickup"> ตั้งเป็นที่อยู่ส่งพัสดุ
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="invoice" id="invoice"
                                            data-parsley-multiple="groups" data-parsley-mincheck="2">
                                        <label class="custom-control-label topic" for="invoice">
                                            ตั้งเป็นที่อยู่ออกใบเสร็จ
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $.Thailand({
                            $district: $('#demo1 [name="tumbon"]'),
                            $amphoe: $('#demo1 [name="amphur"]'),
                            $province: $('#demo1 [name="province"]'),
                            $zipcode: $('#demo1 [name="postcode"]'),

                            onDataFill: function(data) {
                                console.info('Data Filled', data);
                            },

                            onLoad: function() {
                                console.info('Autocomplete is ready!');
                                // $('#loader, .demo').toggle();
                            }
                        });
                        </script>
                        <div class="col-ms-6">
                            <button type="submit" class="btn btn-sm btn-danger" style="padding-left: 20px;
                            padding-right: 20px;">บันทึก</button>
                        </div>
                </div>
                </form>

            </div>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade edit_pass_modal" id="exampleModalCenter" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">บันทึก</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="click" class="fas fa-check"></label>
                <div class="newpassword">บันทึกเสร็จสิ้น</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->

<script>
var data = null;
$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //------------ สร้าง customer ใหม่
    $("#create_customer").submit(function(e) {
        $('#loading').show();
        e.preventDefault();
        var thisform = $(this);
        var url = thisform.attr('action');

        $.ajax({
            type: "POST",
            data: $('#create_customer').serialize(),
            url: url,
            success: function(result) {
                $('#customer_select').hide();
                $('#customer_detail').show();
                $('#loading').hide();
                $('#close_modal').click();
                var result_data = JSON.parse(result);
                alert('Create customer successful.')
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $('#loading').hide();
            }
        });
    });
    $("#edit_customer").submit(function(e) {
        $('#loading').show();
        e.preventDefault();
        var thisform = $(this);
        var url = thisform.attr('action');
        var id = $('#edit_id').val();
        url = url + '/' + id;
        $.ajax({
            type: "POST",
            data: $('#edit_customer').serialize(),
            url: url,
            success: function(result) {
                $('#loading').hide();
                if (result == '0') {
                    alert('มีการใช้อีเมล์นี้แล้วในระบบ กรุณาเปลี่ยนอีเมล์ใหม่');
                }
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $('#loading').hide();
            }
        });
    });
    $("#edit_password").submit(function(e) {
        $('#loading').show();
        e.preventDefault();
        debugger;
        var password = $('#password').val();
        var repassword = $('#repassword').val();
        var thisform = $(this);
        var url = thisform.attr('action');
        var id = $('#edit_id').val();
        url = url + '/' + id;
        if (password == repassword) {
            $.ajax({
                type: "POST",
                data: $('#edit_password').serialize(),
                url: url,
                success: function(result) {
                    $('#loading').hide();
                    if (result == '0') {
                        alert('มีการใช้อีเมล์นี้แล้วในระบบ กรุณาเปลี่ยนอีเมล์ใหม่');
                    } else {
                        $('.edit_pass_modal').modal('show');
                    }
                },
                error: function(e) {
                    console.log("ERROR : ", e);
                    alert(
                        "An error occurred on the server when processing the URL. Please contact the system administrator."
                    );
                    $('#loading').hide();
                }
            });
        } else {
            alert('รหัสกับยืนยันรหัส ไม่ตรงกันกรุณาลองใหม่อีกครั้ง');
            $('#loading').hide();
        }
    });
    //------------ ดึงข้อมูลลูกค้า
});
//-------------- ห้ามใส่ตัวอักษร
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 &&
        (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function show_add_address() {
    $('#add_address_form').trigger("reset");
    $('#add_address_area').show();
    $('#address_title').html('เพิ่มที่อยู่ผู้ส่ง');
    $('#address_id').val('');
    $('#addr_customer_id').val($('#edit_id').val());
    // var fullname = $('#firstname').val() + '  ' + $('#lastname').val();
    var fullname = $('#firstname').val()
    $('#addr_name').val(fullname);
    $('#addr_phone').val($('#phone').val());
    $('#amphur').html('');
    $('#tumbon').html('');
    $('#addr_address').val('');
    $('#postcode').val('');
}

$("#add_address_form").submit(function(e) {
    $('#loading').show();
    e.preventDefault();
    var thisform = $(this);
    var url = thisform.attr('action');
    $.ajax({
        type: "POST",
        data: $('#add_address_form').serialize(),
        url: url,
        success: function(result) {
            location.reload();
        },
        error: function(e) {
            console.log("ERROR : ", e);
            alert(
                "An error occurred on the server when processing the URL. Please contact the system administrator."
            );
            $('#loading').hide();
        }
    });
});

function remove_address(obj, id) {
    if (confirm('Are you sure you want to delete this address into the database?')) {
        $.ajax({
            type: "POST",
            url: "{{url('admin/del_customer_address')}}/" + id,
            success: function(result) {
                $(obj).parent().parent().remove();
            },
            error: function(e) {
                console.log("ERROR : ", e);
                alert(
                    "An error occurred on the server when processing the URL. Please contact the system administrator."
                );
                $('#loading').hide();
            }
        });
    } else {
        // Do nothing!
    }

}
var address_tmp = null;

function edit_address(id) {
    $('#pickup').prop('checked', false);
    $('#invoice').prop('checked', false);
    $('#address_title').html('แก้ไขที่อยู่ผู้ส่ง');
    $('#address_id').val(id);
    $('#add_address_area').show();
    var data = $('#add_' + id);
    var prov = data.find('.province_name').attr('set');
    var email = data.find('.email').html();
    var card_id = data.find('.card_id').html();
    var company = data.find('.company').html();
    email = email == 'null' ? '' : email;
    //$('#addr_customer_id').val($('#edit_id').val());
    $('#company').val(company);
    $('#card_id').val(card_id);
    $('#addr_name').val(data.find('.fullname').html());
    $('#addr_address').val(data.find('.address').html());
    $('#addr_phone').val(data.find('.phone').html());
    $('#addr_email').val(email);
    $('#province').val(prov);
    $('#postcode').val(data.find('.postcode').html());
    $('#amphur').val(data.find('.amphur_name').attr('set'));
    $('#tumbon').val(data.find('.tumbon_name').attr('set'));

    if ($('#pickup' + id).is(":checked")) {
        $('#pickup').prop('checked', true);
    }
    if ($('#invoice' + id).is(":checked")) {
        $('#invoice').prop('checked', true);
    }
    $("#add_address_form").resetForm();
}
$(function() {
    $("#add_address_form").submit(function() {
        var form = $(this)[0];
        if (form.checkValidity() === false) {
            // event.preventDefault();
            event.stopPropagation();
        }
        form.classList.add('was-validated');
    });
});

$(".toggle_password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

$('input[name=password]').focus(function() {
    $('.password_').css('border-color', '#80bdff');
}).blur(
    function() {
        $('.password_').css('border-color', '#e8ebf3');
    });

$('input[name=repassword]').focus(function() {
    $('.repassword_').css('border-color', '#80bdff');
}).blur(
    function() {
        $('.repassword_').css('border-color', '#e8ebf3');
    });
</script>
@endsection