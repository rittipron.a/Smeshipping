
@extends(session()->has('cus.id')?'layouts.customer_app':'layouts.customer_app_hidden_nav')

@section('content')

<style>
.textsize{
    font-size: 100%;
    text-align: center;
    margin: 15px 0 30px 0;
}
@media (max-width: 992px){
.textsize{
    font-size: 88%;
    text-align: center;
    margin: 15px 0 30px 0;
    }
}
</style>

<div class="row">
    <div class="col-md-12">
        <hr style="border: 1px solid rgb(242, 39, 39);
        background-color: white;
        position: inherit;
        top: 80px;
        z-index: 2;">
        <div class="card">
            <div class="card-body">
                <h3 style="text-align:left">ส่งหลักฐานการชำระสำเร็จ</h3>
            </div>
            <div class="col-6 col-md-4" style="margin: auto;">
                <img src="{{ asset('/assets/images/success01.png') }}" width="100%" />
            </div>
            <div class="textsize">
            ลูกค้าสามารถใช้เลข SHIPPING ORDER<br/>
            เป็นเลข TRACKING NUMBER ได้ที่ <br/>
            <a href="https://backend.smeshipping.com/public/tracking">www.smeshipping.com/tracking</a><br/><br/>
            ขอบพระคุณที่ใช้บริการ
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/jquery.min.js' )}}"></script>
<script>
    $("#loading").hide();
</script>
@endsection