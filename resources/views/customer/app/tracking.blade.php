@extends(session()->has('cus.id')?'layouts.customer_app':'layouts.customer_app_hidden_nav')

@section('content')
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('/assets/plugins/moment/moment.js') }}"></script>
<style>
    .card-body {
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 0.5rem;
    }

    .track-bg {

        padding-top: 10px;
        font-size: 15px;
        font-weight: unset;
    }

    .track-bg .mdi {
        color: #f12727;
        font-size: 25px;
    }

    .track-bg:nth-child(odd) {
        background: #f1f0f1;
    }

    .track-bg:nth-child(even) {
        background: #ffffff;
    }

    .track-bg:first-of-type {
        color: #f12727;
    }

    #no-found {
        display: none;
        color: #f12727;
        font-size: 26px;
    }
</style>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>Tracking Status</h2>
                        <form class="position-relative" name="tracking" id="tracking"
                            action="{{url('/app/tracking_json')}}">
                            <input type="text" id="code" placeholder="Enter your number." required="" value="{{$code}}"
                                class="form-control" style="
                    width: 100%;
                    font-size: 20px;
                    height: 60px;
                    color: #3677b6 !important;
                    padding: 15px;
                ">
                            <button type="submit" id="confirm" class="btn btn-danger" style="
                    position: absolute;
                    top: 0px;
                    right: -5px;
                    height: 60px;
                    font-size: 30px;
                    color:white;
                    width: 90px;
                ">Track</button>
                        </form>
                        <p id="no-found">Sorry, No result found. Please try again.</p>
                        <div class="container">
                            <div class="row" id="detail_area" style="display:none">
                                <div class="col-lg-12">
                                    <br />
                                    <div class="row" style="pandding-top:20px;margin">
                                        <div class="col-md-6">
                                            Ship Date: <span id="shipdate">01-01-63</span>
                                        </div>
                                        <div class="col-md-6">
                                            Destination : <span id="destination">United State Of Amarica</span>
                                        </div>
                                        <div class="col-md-6">
                                            Ship To: <span id="shipto"></span>
                                        </div>
                                        <div class="col-md-6">
                                            AWB: <span id="awb">0000000000</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



                <div id="detail">
                </div>
                <!--end card-body-->
            </div>
        </div>
    </div>

    <script>
        $('#tracking').submit(function (e) {
            $("#loading").show();
            e.preventDefault();
            var code = $('#code').val();
            var url = $('#tracking').attr("action");
            $.ajax({
                type: "GET",
                url: url + '/' + code,
                processData: false,
                contentType: false,
                success: function (result) {
                    $('#no-found').hide();
                    
                    $('#detail').html('');
                    result = JSON.parse(result);
                    $('#detail_area').show();
                    var date = moment(result[0].created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY');
                    var history = result[0].history;
                    $(history).each(function(i,e){
                        $('#detail').prepend('<div class="row track-bg">'+
                                '<div class="col-2" style="text-align:center;">'+
                                '<i class="mdi mdi-check-circle"></i>'+
                                '</div>'+
                                '<div class="col-10 ">Location: BANGKOK-THAILAND. Date: '+
                                                        moment(e.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MMM-YYYY')+', '+moment(e.created_at, 'YYYY-MM-DD HH:mm:ss').format('HH:mm')+'<br />'
                                    +retopic(e.topic)+'</div>'+
                            '</div>');
                    });
                    
                    $(result).each(function(i,e){
                        if(i!=0){
                            $('#detail').prepend('<div class="row track-bg">'+
                                '<div class="col-2" style="text-align:center;">'+
                                '<i class="mdi mdi-check-circle"></i>'+
                                '</div>'+
                                '<div class="col-10 ">Location: '+result[i].ServiceArea+' Date: '+
                                    moment(result[i].Date, 'YYYY-MM-DD').format('DD-MMM-YYYY')+', '+moment(result[i].Time, 'HH:mm:ss').format('HH:mm')+'<br />'
                                    +result[i].ServiceEvent+'</div>'+
                            '</div>');
                        }
                    });

                    if(result[0].tracking!='DHL'&&result[0].status>4){
                        $('#detail').prepend('<div class="row track-bg">'+
                                '<div class="col-2" style="text-align:center;">'+
                                '<i class="mdi mdi-check-circle"></i>'+
                                '</div>'+
                                '<div class="col-10 "><a href="'+getTrackingURL(result[0].tracking,result[0].awb)+'">Shipment forward to service '+result[0].tracking+'.<br/> Click here to check the status of your shipment.</a></div>');
                    }
                    $('#shipdate').html(date);
                    $('#shipto').html(result[0].recipient);
                    $('#destination').html(result[0].country);
                    $('#awb').html(result[0].awb);
                    console.log(result);
                    $("#loading").hide();
                },
                error: function (e) {
                    $('#detail_area').hide();
                    $('#detail').hide();
                    $('#no-found').show();
                    console.log("ERROR : ", e);
                    $("#loading").hide();
                }
            });
        });

        function retopic(str){
            if(str=='Create'){
                return 'Booking';
            }
            else if(str=='Calculate'){
                return 'SME SHIPPING Pick-Up at sender location';
            }
            else if(str=='Complete'){
                return 'Arrived at sort facility SME SHIPPING Bangkok Thailand';
            }
            else{
                return str;
            }
        }

        function getTrackingURL(tracking,awb){
            if(tracking == "TNT"){
                return "http://www.tnt.com/webtracker/tracking.do?cons="+awb+"&navigation=1";
            } 
            else if(tracking == "FEDEX"){
                return "https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber="+awb+"&cntry_code=th_thai";
            } 
            else if(tracking == "GMB"){
                return "https://ecommerceportal.dhl.com/track/?ref="+awb;
            } 
            else if(tracking == "Aramex"){
                return "https://www.aramex.com/track/results?ShipmentNumber="+awb;
            } else {
                return "https://www.smeshipping.com/tracking?code="+awb;
            }
        }

        if($('#code').val()!=''){
            $('#tracking').submit();
        }
    </script>

    @endsection