@extends('layouts.customer_app_hidden_nav')

@section('title','Customer forget password -')


@section('content')
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<style>
    .row {
        padding-bottom: 10px;
    }
</style>

<div class="row" style="padding-bottom:0px;">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <hr style="border: 1px solid rgb(242, 39, 39);
        background-color: white;
        position: inherit;
        top: 100px;
        z-index: 2;" />
        <div class="card">
            <div class="card-body">
                <H3 style="padding-left:20px">ลืมรหัสผ่าน</H3>

                <br />
                <br />
                <form name="create_customer" id="create_customer" action="{{url('/app/forgetpassword')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-4" style="text-align:right;font-size:16px;font-weight: bold;padding-top: 8px;">
                            โปรดกรอกอีเมล์*</div>
                        <div class="col-7">
                            <input type="email" name="email" class="form-control" placeholder="กรุณากรอกอีเมล์"
                                required="">
                        </div>
                    </div>


                    <div style="text-align:center"><button type="submit"
                            class="btn btn-danger waves-effect waves-light">ส่งข้อมูล</button></div>
                    <br />
            </div>
            <div class="col-sm-3"></div>
        </div>
        </form>
    </div>
    <!--end card-body-->
</div>

</div>


@if($errors->any())
<div class="modal fade bs-example-modal-lg" id="onloadPopup" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">แจ้งเตือน</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="background-color: white;">

                <div class="container">
                    {{$errors->first()}} โปรดตรวจสอบใหม่อีกครั้ง หากท่านยังไม่เคยใช้บริการ SME SHIPPING โปรดกด<a
                        href="{{url('/app/register')}}">สมัครสมาชิก</a>
                </div>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endif

@endsection