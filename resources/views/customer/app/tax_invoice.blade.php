@extends('layouts.customer_app_hidden_nav')
@section('title','Tax Invoice')
@section('content')
<link href="{{ asset('assets/css/tex_invoice.css?1' )}}" rel="stylesheet">
<link href="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.css' )}}" rel="stylesheet" />
<div class="row" style="padding-bottom:0px;">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 toppic fontweight">
                        <h4>แบบฟอร์มขอรับใบกำกับภาษี</h4>
                    </div>
                    <div class="form_s" id="tax_invoice">
                        <div class="col-12 marginbottom fontweight">
                            ต้องการใบกำกับภาษีหรือไม่
                        </div>
                        <div class="row marginbottom">
                            <div class="form-check form-check-inline col-md-5">
                                <input class="form-check-input" type="radio" name="status" onclick="check_tax()"
                                    id="tax" value="รับ" checked>
                                <label class="form-check-label status_tax" for="tax">รับ</label>
                            </div>
                            <div class="form-check form-check-inline col-md-5">
                                <input class="form-check-input" type="radio" name="status" onclick="check_tax()"
                                    id="no_tax" value="ไม่รับ">
                                <label class="form-check-label status_tax" for="no_tax">ไม่รับ</label>
                            </div>
                        </div>
                        <div class="col-12 marginbottom fontweight no_taxinvoice">
                            ออกบิลใบนาม
                        </div>
                        <div class="row marginbottom no_taxinvoice">
                            <div class="form-check form-check-inline col-md-5">
                                <input class="form-check-input" type="radio" name="status_name" onclick="check_()"
                                    id="personal" value="บุคคล" checked>
                                <label class="form-check-label status_tax" for="personal">บุคคล</label>
                            </div>
                            <div class="form-check form-check-inline col-md-5">
                                <input class="form-check-input" type="radio" name="status_name" onclick="check_()"
                                    id="company" value="บริษัท/ห้างหุ้นส่วนจำกัด">
                                <label class="form-check-label status_tax" for="company">บริษัท /
                                    ห้างหุ้นส่วนจำกัด</label>
                            </div>
                        </div>
                        <div class="row edge personal">
                            <div class="col-md-3 marginbottom">
                                <span>ชื่อ-สกุล / บริษัท</span>
                            </div>
                            <div class="col-md-9 marginbottom">
                                <input type="text" id="name" class="personal_input basic_input" name="name" value=""
                                    require>
                            </div>
                            <div class="form-check col-md-4 marginbottom no_taxinvoice">
                                <input class="form-check-input" type="radio" onclick="check_subject()"
                                    name="status_subject" id="main_branch" value="สำนักงานใหญ่" checked>
                                <label class="form-check-label staus_subject manage" for="main_branch">
                                    <span>สำนักงานใหญ่</span>
                                </label>
                            </div>
                            <div class="form-check col-md-8 marginbottom no_taxinvoice">
                                <input class="form-check-input" type="radio" name="status_subject"
                                    onclick="check_subject()" id="branch" value="สาขา">
                                <label class="form-check-label staus_subject_l" for="branch">
                                    <span>สาขา</span>
                                    <input type="text" class="input_subject read_only" name="name_branch"
                                        id="name_branch" value="" disabled>
                                </label>
                            </div>
                            <div class="col-md-3 marginbottom no_taxinvoice">
                                <span>เลขผู้เสียภาษี</span>
                            </div>
                            <div class="col-md-9 marginbottom no_taxinvoice">
                                <input type="text" id="number_tax" class="personal_input basic_input" name="tax_number"
                                    value="">
                            </div>
                            <div class="col-md-3 marginbottom no_taxinvoice">
                                <span>เบอร์โทร</span>
                            </div>
                            <div class="col-md-9 marginbottom no_taxinvoice">
                                <input type="text" id="phone" class="personal_input basic_input" name="phone" value=""
                                    require>
                            </div>
                            <div class="form-check col-md-8 no_taxinvoice">
                                <input class="form-check-input" type="radio" name="status_address" id="address"
                                    value="ที่อยู่ตามหน้าใบกำกับภาษี/ใบเสร็จรับเงิน">
                                <label class="form-check-label address_one"
                                    for="address">ที่อยู่ตามหน้าใบกำกับภาษี/ใบเสร็จรับเงิน</label>
                            </div>
                            <div class="form-check col-md-4 no_taxinvoice">
                                <input class="form-check-input" type="radio" name="status_address" id="address_other"
                                    value="ระบุที่อยู่อื่นๆ" checked>
                                <label class="form-check-label address_two" for="address_other">ระบุที่อยู่อื่นๆ</label>
                            </div>
                            <div class="col-md-12 marginbottom no_taxinvoice">
                                <textarea id="c_address" class="personal_input" name="address" require></textarea>
                            </div>
                            <div class="form-check col-md-4 no_taxinvoice">
                                <input class="form-check-input hidden_checkbox" type="checkbox" onclick="check_mail()"
                                    name="c_email" id="acc_mail" value="">
                                <label class="form-check-label check_email" for="acc_mail">รับบิลทาง Email</label>
                            </div>
                            <div class="col-md-8 no_taxinvoice">
                                <input type="email" class="basic_input read_only" name="email" id="v_email" value=""
                                    disabled>
                            </div>
                        </div>
                        <div class="col-md-12 no_taxinvoice">
                            กรุณาแนบไฟล์รูปภาพ บัตรประชาชน(ไม่สามารถส่งไฟล์อื่นนอกจากรูปได้ค่ะ)
                        </div>
                        <div class="col-md-12 custom-file div_file marginbottom no_taxinvoice">
                            <input type="file" id="file">
                        </div>
                        <div class="col-md-12 no_taxinvoice">
                            การหักภาษี ณ ที่จ่าย (ค่าขนส่งหัก 1% และ ค่าบริการหักภาษี 3%)
                        </div>
                        <div class="row marginbottom edge no_taxinvoice">
                            <div class="form-check form-check-inline col-md-5" id="div_wht">
                                <input class="form-check-input" type="radio" name="wht" id="wht" value="หัก">
                                <label class="form-check-label wht" for="wht">หัก</label>
                            </div>
                            <div class="form-check form-check-inline col-md-5">
                                <input class="form-check-input" type="radio" name="wht" id="no_wht" value="ไม่หัก">
                                <label class="form-check-label wht" for="no_wht">ไม่หัก</label>
                            </div>
                        </div>
                        <div class="col-md-12 submit">
                            <button type="button" onclick="upload_data()" class="btn btn-danger">ยืนยันคำขอ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="color: red;">หมายเหตุ</div>
        <div>
            ใบกำกับภาษี/ใบเสร็จรับเงินตัวจริง จะทำการจัดภายใน 3-5 วันทำการ (หลังจากได้รับใบหักภาษี ณ
            ที่จ่าย)<br>
            สอบถามหรือติดตามใบกำกับภาษี/ใบเสร็จรับเงินได้ที่ โทร 02-1057712-4
        </div>
    </div>
</div>
<!--end card-body-->
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sweet-alert2/sweetalert2.min.js')}}"></script>
@if (session('alert'))
<script>
swal.fire("เสร็จสิ้น", "ยินยันคำการขอใบกำกับภาษี", "success");
</script>
@endif
<script>
$(document).ready(function() {
    $('#no_wht').prop('checked', true);
    $('#div_wht').addClass('display_none');
});

function upload_data() {
    debugger;
    var form_data = new FormData();
    var totalfiles = document.getElementById('file').files.length;
    for (var index = 0; index < totalfiles; index++) {
        form_data.append("files[]", document.getElementById('file').files[index]);
    }
    var wht = $('#wht').is(":checked")?$('#wht').val():$('#no_wht').val();
    var status = $('#tax').is(":checked")?$('#tax').val():$('#no_tax').val();
    var status_name = $('#personal').is(":checked")?$('#personal').val():$('#company').val();
    var status_subject = $('#branch').is(":checked")?$('#branch').val():$('#main_branch').val();
    var status_address = $('#status_address').is(":checked")?$('#status_address').val():$('#address').val();
    
    var name = $('#name').val();
    var name_branch = $('#name_branch').val();
    var tax_number = $('#number_tax').val();
    var phone = $('#phone').val();
    var address = $('#c_address').val();
    var email = $('#v_email').val();
    form_data.append('status', status);
    form_data.append('status_name', status_name);
    form_data.append('wht', wht);
    form_data.append('status_subject', status_subject);

    form_data.append('name', name);
    form_data.append('name_branch', name_branch);
    form_data.append('tax_number', tax_number);
    form_data.append('phone', phone);
    form_data.append('status_address', status_address);
    form_data.append('address', address);
    form_data.append('email', email);
    console.log(form_data);
    if($('#name').val() !== ""){
    $.ajax({
        type: "POST",
        data: form_data,
        url: '/admin/save_taxinvoice',
        processData: false,
        contentType: false,
        success: async function(result) {
            await swal({
            title: "Success",
            text: "ขอบคุณที่ส่งข้อมูลมาให้ค่ะ",
            type: "success"
        }, function() {
            location.reload();
        });
        await window.location.reload();
        },
        error: function(e) {
            console.log("ERROR : ", e);
            Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'หากพบปัญหาสามารถติดต่อได้ที่เบอร์ 02-1057712-4',
        });
        }
    });
    }else{
        Swal.fire({
        icon: 'error',
        title: 'คุณลืมกรอกชื่อ',
        text: 'กรุณากลับไปกรอกข้อมูลให้ครบถ้วนค่ะ',
        });
    }
}

function check_tax() {
    var tax = $('#tax').is(":checked");
    var no_tax = $('#no_tax').is(":checked");
    if (tax) {
        $('#tax_invoice').removeClass('col-md-12');
        $('#tax_invoice').removeClass('form_m');
        $('#tax_invoice').addClass('form_s');
        $('.no_taxinvoice').removeClass('display_none');
        $('input[name="phone"]').attr('require', true);
        $('textarea[name="address"]').attr('require', true);
    } else if (no_tax) {
        $('#tax_invoice').addClass('col-md-12');
        $('#tax_invoice').removeClass('form_s');
        $('#tax_invoice').addClass('form_m');
        $('.no_taxinvoice').addClass('display_none');
        $('input[name="phone"]').removeAttr('require');
        $('textarea[name="address"]').removeAttr('require');
    }
}

function check_mail() {
    var check_mail = $('#acc_mail').is(":checked");
    if (check_mail) {
        $('#v_email').removeAttr('disabled');
        $('#v_email').removeClass('read_only');
        $('#v_email').attr('require', true);
    } else {
        $('#v_email').attr('disabled', true);
        $('#v_email').addClass('read_only');
        $('#v_email').removeClass('require');
    }
};

function check_subject() {
    var c_branch = $('#branch').is(":checked");
    var c_main = $('#main_branch').is(":checked");
    if (c_branch) {
        $('#name_branch').removeAttr('disabled', true);
        $('#name_branch').removeClass('read_only');
    }
    if (c_main) {
        $('#name_branch').attr('disabled', true);
        $('#name_branch').addClass('read_only');
    }
};

function check_() {
    var c_personal = $('#personal').is(":checked");
    var c_company = $('#company').is(":checked");
    if (c_company) {
        $('#div_wht').removeClass('display_none');
    }
    if (c_personal) {
        $('#div_wht').addClass('display_none');
        $('#no_wht').prop('checked', true);
    }
};
</script>
@endsection