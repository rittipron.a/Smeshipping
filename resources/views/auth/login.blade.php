<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Login SME Shipping</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A premium admin dashboard template by Mannatthemes" name="description" />
    <meta content="Mannatthemes" name="author" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('/assets/images/favicon.png') }}">

    <!-- App css -->
    <link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/metisMenu.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/css/style.css') }}" rel="stylesheet" type="text/css" />

</head>

<body class="account-body accountbg">

    <!-- Log In page -->
    <div class="row vh-100 ">
        <div class="col-lg-12  pr-0 align-self-center">
            <div class="row">
                <div class="col-lg-3 mx-auto">
                    <div class="card auth-card shadow-lg">
                        <div class="card-body">
                            <div class="px-3">
                                <div class="auth-logo-box">
                                    <a href="#" class="logo logo-admin"><img
                                            src="{{ asset('/assets/images/logo-sm.png') }}" height="55" alt="logo"
                                            class="auth-logo"></a>
                                </div>
                                <!--end auth-logo-box-->

                                <div class="text-center auth-logo-text">
                                    <h4 class="mt-0 mb-3 mt-5">SME Shipping</h4>
                                    <p class="text-muted mb-0">Sign in to continue to SME Shipping.</p>
                                </div>
                                <!--end auth-logo-text-->


                                <form method="POST" class="form-horizontal auth-form my-4"
                                    action="{{ route('login') }}">
                                    @csrf

                                    <div class="form-group">
                                        <label for="username">Email</label>
                                        <div class="input-group mb-3">
                                            <span class="auth-form-icon">
                                                <i class="dripicons-user"></i>
                                            </span>
                                            <input type="text" class="form-control" id="email" name="email"
                                                placeholder="Enter email">
                                        </div>
                                    </div>
                                    <!--end form-group-->

                                    <div class="form-group">
                                        <label for="userpassword">Password</label>
                                        <div class="input-group mb-3">
                                            <span class="auth-form-icon">
                                                <i class="dripicons-lock"></i>
                                            </span>
                                            <input type="password" class="form-control" id="userpassword"
                                                name="password" placeholder="Enter password">
                                        </div>
                                    </div>
                                    <!--end form-group-->

                                    <div class="form-group row mt-4">
                                        <div class="col-sm-12">
                                            <!--<div class="custom-control custom-switch switch-success">
                                                <input type="checkbox" class="custom-control-input"
                                                    id="customSwitchSuccess">
                                                <label class="custom-control-label text-muted"
                                                    for="customSwitchSuccess">Remember me</label>
                                            </div><span style="color:red">ประกาศ: รหัสผ่านรูปแบบใหม่จะใช้
                                                ตัวอักษรตัวแรกของชื่อจริง(พิมพ์ใหญ่) @วันเดือนปีเกิด 6 หลัก เช่น
                                                Paphatchai
                                                เกิด 02/01/2535 ก็จะใช้รหัสเป็น P@020135 </span>-->

                                        </div>
                                        <!--
                                        <div class="col-sm-6 text-right">
                                            <a href="auth-recover-pw.html" class="text-muted font-13"><i
                                                    class="dripicons-lock"></i> Forgot password?</a>
                                        </div>
                                        -->
                                    </div>
                                    <!--end form-group-->

                                    <div class="form-group mb-0 row">
                                        <div class="col-12 mt-2">
                                            <button class="btn btn-danger btn-round btn-block waves-effect waves-light"
                                                type="submit">Log In <i class="fas fa-sign-in-alt ml-1"></i></button>
                                        </div>
                                        <!--end col-->
                                    </div>
                                    <!--end form-group-->
                                </form>
                                <!--end form-->
                            </div>
                            <!--end /div-->


                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->

                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end col-->
    </div>
    <!--end row-->
    <!-- End Log In page -->


    <!-- jQuery  -->
    <script src="{{ asset('/assets/js/jquery.min.js' )}}"></script>
    <script src="{{ asset('/assets/js/bootstrap.bundle.min.js' )}}"></script>
    <script src="{{ asset('/assets/js/metisMenu.min.js' )}}"></script>
    <script src="{{ asset('/assets/js/waves.min.js' )}}"></script>
    <script src="{{ asset('/assets/js/jquery.slimscroll.min.js' )}}"></script>

    <!-- App js -->
    <script src="{{ asset('/assets/js/app.js' )}}"></script>

</body>

</html>

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class=" card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email"
                                class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
    var OneSignal = window.OneSignal || [];
      OneSignal.push(function() {
        OneSignal.init({
          //appId: "56cca475-ad31-4fc8-8ea1-ff381d4cdd0a",
          appId: "bea7fe0f-eeb0-41bd-aafc-b17393792e40",
        });
      });
</script>
@endsection